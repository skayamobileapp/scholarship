<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class StudentLogin extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_login_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $this->checkStudentLoggedIn();
    }
    

    function checkStudentLoggedIn()
    {
        $isStudentLoggedIn = $this->session->userdata('isStudentLoggedIn');
        
        if(!isset($isStudentLoggedIn) || $isStudentLoggedIn != TRUE)
        {
            $this->load->view('student_login');
        }
        else
        {
            redirect('student/profile');
        }
    }


    public function studentLogin()
    {
        $formData = $this->input->post();
        // echo "<Pre>"; print_r($formData);exit;
        $domain = $this->getDomainName();

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = strtolower($this->security->xss_clean($this->input->post('email')));
            $password = $this->input->post('password');
            
            
            $result = $this->student_login_model->loginStudent($email, $password);

        // echo "<Pre>"; print_r($result);exit;

            
            if(!empty($result))
            {
                if($result->applicant_status == 'Graduated')
                {
                    echo "For Graduated Students, Student Portal is No More Active";exit();
                }
                $lastLogin = $this->student_login_model->studentLastLoginInfo($result->id_student);

                if($lastLogin == '')
                {
                    $student_login = date('Y-m-d h:i:s');
                }
                else
                {
                    $student_login = $lastLogin->created_dt_tm;
                }

                $sessionArray = array('id_student'=>$result->id_student,                    
                                        'student_name'=>$result->student_name,
                                        'email_id'=>$result->email_id,
                                        'nric'=>$result->nric,
                                        'id_intake'=>$result->id_intake,
                                        'id_program'=>$result->id_program,
                                        'id_program_landscape'=>$result->id_program_landscape,
                                        'id_qualification'=>$result->id_degree_type,
                                        'id_applicant' => $result->id_applicant,
                                        'id_program_scheme'=>$result->id_program_scheme,
                                        'student_education_level'=>$result->student_education_level,
                                        'student_last_login'=> $student_login,
                                        'isStudentLoggedIn' => TRUE
                                );
        // echo "<Pre>";print_r($sessionArray);exit();

                $this->session->set_userdata($sessionArray);

                unset($sessionArray['id_student'], $sessionArray['isStudentLoggedIn'], $sessionArray['student_last_login']);

                $loginInfo = array("id_student"=>$result->id_student, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());

                $uniqueId = rand(0000000000,9999999999);
                $this->session->set_userdata("my_student_session_id", md5($uniqueId));


                $this->student_login_model->addStudentLastLogin($loginInfo);

                // echo "Login";exit();
                // echo md5($uniqueId);exit();
                redirect('/student/profile');                
            }
            else
            {
                $this->session->set_flashdata('error', 'Email or password mismatch');
                
                $this->index();
            }
        }
    }
}

?>
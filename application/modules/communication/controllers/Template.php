<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Template extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('template_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('template.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['templateList'] = $this->template_model->templateListSearch($name);
            //echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Scholarship Management System : Communication Template List';
            $this->loadViews("template/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('template.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_scholar_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $subject = $this->security->xss_clean($this->input->post('subject'));
                $message = $this->security->xss_clean($this->input->post('message'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'subject' => $subject,
                    'message' => $message,
                    'status' => $status,
                    'created_by' => $user_id
                );
                $result = $this->template_model->addNewTemplate($data);
                redirect('/communication/template/list');
            }

            $this->global['pageTitle'] = 'Scholarship Management System : Add Communication Template';
            $this->loadViews("template/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('template.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/communication/template/list');
            }
            $id_session = $this->session->my_scholar_session_id;
            $user_id = $this->session->userId;
            
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $subject = $this->security->xss_clean($this->input->post('subject'));
                $message = $this->security->xss_clean($this->input->post('message'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'subject' => $subject,
                    'message' => $message,
                    'status' => $status,
                    'created_by' => $user_id
                );                
                $result = $this->template_model->editTemplate($data,$id);
                redirect('/communication/template/list');
            }

            $data['template'] = $this->template_model->getTemplate($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Communication Template';
            $this->loadViews("template/edit", $this->global, $data, NULL);
        }
    }
}

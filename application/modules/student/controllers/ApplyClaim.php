<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ApplyClaim extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('apply_claim_model');
        $this->isStudentLoggedIn();
    }

    function list()
    {

        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;
        $id_program_scheme = $this->session->id_program_scheme;



        $formData['type'] = $this->security->xss_clean($this->input->post('type'));
        $formData['status'] = $this->security->xss_clean($this->input->post('status'));
        $formData['id_student'] = $id_student;


        $data['searchParam'] = $formData;
        $data['applypplyClaimList'] = $this->apply_claim_model->applyClaimListSearch($formData);


        // echo "<Pre>";print_r($data['applypplyClaimList']);exit();

        $this->global['pageTitle'] = 'Scholarship Student Portal : ApplyClaim List';
        $this->loadViews("apply_claim/list", $this->global, $data, NULL);
    }
    
    function add()
    {
        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;
        $id_program_scheme = $this->session->id_program_scheme;


        if($this->input->post())
        {

        // echo "<Pre>";print_r($this->input->post());exit();


            $type = $this->security->xss_clean($this->input->post('type'));
            $amount = $this->security->xss_clean($this->input->post('amount'));
            $description = $this->security->xss_clean($this->input->post('description'));
            $start_date = $this->security->xss_clean($this->input->post('start_date'));
            $end_date = $this->security->xss_clean($this->input->post('end_date'));
        
            if($start_date)
            {
                $start_date = date('Y-m-d', strtotime($start_date));
            }

            if($end_date)
            {
                $end_date = date('Y-m-d', strtotime($end_date));
            }

            $data = array(
                'id_student' => $id_student,
                'id_program' => $id_program,
                'type' => $type,
                'description' => $description,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'amount' => $amount,
                'status' => 0,
                'created_by' => 0
            );

        // echo "<Pre>";print_r($data);exit();
            $inserted_id = $this->apply_claim_model->addApplyClaim($data);

            // if($inserted_id)
            // {
            //     $moved = $this->apply_claim_model->moveTempToDetails($inserted_id);
            // }

            redirect('/student/applyClaim/list');
        }

        $data['studentDetails'] = $this->apply_claim_model->getStudentByStudentId($id_student);

        // echo "<Pre>";print_r($data['examinerList']);exit();

        $this->global['pageTitle'] = 'Scholarship Student Portal : Add ApplyClaim';
        $this->loadViews("apply_claim/add", $this->global, $data, NULL);
    }


    function edit($id = NULL)
    {

        if ($id == null)
        {
            redirect('/student/applyClaim/list');
        }

        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;
        $id_program_scheme = $this->session->id_program_scheme;


        if($this->input->post())
        {
            $type = $this->security->xss_clean($this->input->post('type'));
            $amount = $this->security->xss_clean($this->input->post('amount'));
            $description = $this->security->xss_clean($this->input->post('description'));
            $start_date = $this->security->xss_clean($this->input->post('start_date'));
            $end_date = $this->security->xss_clean($this->input->post('end_date'));
        
            if($start_date)
            {
                $start_date = date('Y-m-d', strtotime($start_date));
            }

            if($end_date)
            {
                $end_date = date('Y-m-d', strtotime($end_date));
            }

            $data = array(
                'id_student' => $id_student,
                'type' => $type,
                'description' => $description,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'amount' => $amount
            );


            $result = $this->apply_claim_model->editApplyClaim($data,$id);
            redirect('/student/applyClaim/list');
        
        }
        
        $data['applyClaim'] = $this->apply_claim_model->getApplyClaim($id);
        $data['studentDetails'] = $this->apply_claim_model->getStudentByStudentId($id_student);

        // echo "<Pre>";print_r($data['researchApplyClaimHasExaminer']);exit;

        $this->global['pageTitle'] = 'Scholarship Student Portal : Edit ApplyClaim';
        $this->loadViews("apply_claim/edit", $this->global, $data, NULL);
    }


    function view($id = NULL)
    {
        if ($id == null)
        {
            redirect('/student/applyClaim/list');
        }

        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;
        $id_program_scheme = $this->session->id_program_scheme;


        if($this->input->post())
        {
           redirect('/student/applyClaim/list');
        }
        
        $data['applyClaim'] = $this->apply_claim_model->getApplyClaim($id);
        $data['studentDetails'] = $this->apply_claim_model->getStudentByStudentId($id_student);

        // echo "<Pre>";print_r($data['researchApplyClaimHasExaminer']);exit;

        $this->global['pageTitle'] = 'Scholarship Student Portal : View ApplyClaim';
        $this->loadViews("apply_claim/view", $this->global, $data, NULL);
    }



    function approvalList()
    {
        $student_education_level = $this->session->student_education_level;
        if ($student_education_level == 'Master' || $student_education_level == 'P.hd' )
        {

            $id_student = $this->session->id_student;
            $id_intake = $this->session->id_intake;
            $id_program = $this->session->id_program;
            $id_qualification = $this->session->id_qualification;
            $id_program_scheme = $this->session->id_program_scheme;

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['id_research_category'] = $this->security->xss_clean($this->input->post('id_research_category'));
            $formData['id_research_topic'] = $this->security->xss_clean($this->input->post('id_research_topic'));
            $formData['id_student'] = $id_student;
            $formData['status'] = '0';



            $data['searchParam'] = $formData;
            $data['researchApplyClaimList'] = $this->apply_claim_model->researchApplyClaimListSearch($formData);
            $data['researchCategoryList'] = $this->apply_claim_model->researchCategoryListByStatus('1');
            $data['researchTopicList'] = $this->apply_claim_model->researchTopicListByStatus('1');


            // echo "<Pre>";print_r($data['researchApplyClaimList']);exit();

            $this->global['pageTitle'] = 'Scholarship Student Portal : ApplyClaim Approval List';
            $this->loadViews("apply_claim/approval_list", $this->global, $data, NULL);
        }
        else
        {
            $this->loadAccessRestricted();
        }
    }


    function approve($id = NULL)
    {
        $student_education_level = $this->session->student_education_level;
        if ($student_education_level == 'Master' || $student_education_level == 'P.hd' )
        {
            if ($id == null)
            {
                redirect('/research/proposal/approvalList');
            }


            $id_student = $this->session->id_student;
            $id_intake = $this->session->id_intake;
            $id_program = $this->session->id_program;
            $id_qualification = $this->session->id_qualification;
            $id_program_scheme = $this->session->id_program_scheme;
            

            if($this->input->post())
            {
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'reason' => $reason,
                    'status' => $status,
                    'updated_by' => $id_user
                );

                $result = $this->apply_claim_model->editResearchApplyClaimDetails($data,$id);
                redirect('/research/proposal/approvalList');
            }

            $data['researchApplyClaim'] = $this->apply_claim_model->getResearchApplyClaim($id);
            $data['researchApplyClaimHasSupervisor'] = $this->apply_claim_model->getResearchApplyClaimHasSupervisor($id);
            $data['researchApplyClaimHasExaminer'] = $this->apply_claim_model->getResearchApplyClaimHasExaminer($id);

            $data['studentDetails'] = $this->apply_claim_model->getStudentByStudentId($id_student);



            $data['staffList'] = $this->apply_claim_model->staffListByStatus('1');
            $data['researchCategoryList'] = $this->apply_claim_model->researchCategoryListByStatus('1');
            $data['researchTopicList'] = $this->apply_claim_model->researchTopicListByStatus('1');

            $data['supervisorRoleList'] = $this->apply_claim_model->supervisorRoleListByStatus('1');
            $data['examinerList'] = $this->apply_claim_model->examinerListByStatus('1');
            $data['examinerRoleList'] = $this->apply_claim_model->examinerRoleListByStatus('1');

            // echo "<Pre>";print_r($data['researchApplyClaimHasExaminer']);exit;

            $this->global['pageTitle'] = 'Scholarship Student Portal : Approve ApplyClaim';
            $this->loadViews("apply_claim/approve", $this->global, $data, NULL);
        }
        else
        {
            $this->loadAccessRestricted();
        }
    }


    function getIntakeByProgramme($id_programme)
    {
        $intake_data = $this->apply_claim_model->getIntakeByProgrammeId($id_programme);

        // echo "<Pre>"; print_r($intake_data);exit;

        $table="
        <script type='text/javascript'>
            $('select').select2();
        </script>


        <select name='id_intake' id='id_intake' class='form-control' onchange='getStudentByData()'>
        <option value=''>Select</option>
        ";

        for($i=0;$i<count($intake_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $intake_data[$i]->id;
        $name = $intake_data[$i]->name;
        $year = $intake_data[$i]->year;

        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }


    function getStudentByData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // $id_programme = $tempData['id_programme'];
        // $id_program_scheme = $tempData['id_program_scheme'];
        // $id_intake = $tempData['id_intake'];

        $student_data = $this->apply_claim_model->getStudentByData($tempData);

        // echo "<Pre>";print_r($student_data);exit();

        $table="
        <script type='text/javascript'>
            $('select').select2();
        </script>


        <select name='id_student' id='id_student' class='form-control' onchange='getStudentByStudentId(this.value)'>
        <option value=''>Select</option>";

        for($i=0;$i<count($student_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $student_data[$i]->id;
        $full_name = $student_data[$i]->full_name;
        $nric = $student_data[$i]->nric;

        $table.="<option value=".$id.">" . $nric . " - " . $full_name.
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }


    function getStudentByStudentId($id)
    {
         // print_r($id);exit;
            $student_data = $this->apply_claim_model->getStudentByStudentId($id);
            // echo "<Pre>"; print_r($student_data);exit;

            $student_name = $student_data->full_name;
            $student_nric = $student_data->nric;
            $email = $student_data->email_id;
            $nric = $student_data->nric;
            $intake_name = $student_data->intake_name;
            $id_intake = $student_data->id_intake;
            $programme_name = $student_data->programme_name;
            $nationality = $student_data->nationality;

            if($nationality == 'Malaysian')
            {
                $currency = 'MYR';
            }elseif($nationality == 'Other')
            {
                $currency = 'USD';
            }


            $table  = "



             <h4 class='sub-title'>Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>$student_name</dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd>$email</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>$nric</dd>
                            </dl>
                            
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Intake :</dt>
                                <dd>
                                    <input type='hidden' name='id_intake' id='id_intake' value='$id_intake' />
                                    <input type='hidden' name='currency' id='currency' value='$currency' />
                                    $intake_name

                                </dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd>$programme_name</dd>
                            </dl>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd>$nationality</dd>
                            </dl>
                        </div>
    
                    </div>
                </div>";

                
            echo $table;
            exit;
    }




    function tempAddResearchApplyClaimHasSupervisor()
    {
        $id_student = $this->session->id_student;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_student;

        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->apply_claim_model->tempAddResearchApplyClaimHasSupervisor($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;

        $data = $this->displayTempSupervisordata();
        
        echo $data;        
    }

    function displayTempSupervisordata()
    {
        $id_student = $this->session->id_student;
        
        $temp_details = $this->apply_claim_model->getTempResearchApplyClaimHasSupervisorBySession($id_student); 
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Supervisor</th>
                    <th>Supervisor Role</th>
                    <th>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $ic_no = $temp_details[$i]->ic_no;
                    $staff_name = $temp_details[$i]->staff_name;
                    $supervisor_role = $temp_details[$i]->supervisor_role;
                    $status = $temp_details[$i]->status;

                    if($status == 1)
                    {
                        $status = 'Active';
                    }else
                    {
                        $status = 'In-Active';
                    }

                    $j = $i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$ic_no - $staff_name</td>                         
                            <td>$supervisor_role</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempResearchApplyClaimHasSupervisor($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
            // <span onclick='deleteTempApplyClaimHasProgramme($id)'>Delete</a>                            
            $table.= "
            </tbody>
            </table>
            </div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempResearchApplyClaimHasSupervisor($id)
    {
        $inserted_id = $this->apply_claim_model->deleteTempResearchApplyClaimHasSupervisor($id);
        if($inserted_id)
        {
            $data = $this->displayTempSupervisordata();
            echo $data;  
        }
    }



    function tempAddResearchApplyClaimHasExaminer()
    {
        $id_student = $this->session->id_student;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_student;

        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->apply_claim_model->tempAddResearchApplyClaimHasExaminer($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;

        $data = $this->displayTempExaminerdata();
        
        echo $data;        
    }

    function displayTempExaminerdata()
    {
        $id_student = $this->session->id_student;
        
        $temp_details = $this->apply_claim_model->getTempResearchApplyClaimHasExaminerBySession($id_student); 
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Type</th>
                    <th>Examiner</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
                </thead>";


                    // <th>Status</th>
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $type = $temp_details[$i]->type;
                    $ic_no = $temp_details[$i]->ic_no;
                    $staff_name = $temp_details[$i]->staff_name;
                    $full_name = $temp_details[$i]->full_name;
                    $status = $temp_details[$i]->status;
                    $examiner_role = $temp_details[$i]->examiner_role;

                    if($status == 1)
                    {
                        $status = 'Active';
                    }else
                    {
                        $status = 'In-Active';
                    }


                    if($type == 1)
                    {
                        $type = 'Internal';
                        $staff = $ic_no . " - " . $staff_name;
                    }else
                    {
                        $type = 'External';
                        $staff = $full_name;
                    }

                    $j = $i+1;
                            // <td>$status</td>

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$type</td>
                            <td>$staff</td>                         
                            <td>$examiner_role</td>                         
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempResearchApplyClaimHasExaminer($id)'>Delete</a>
                            <td>
                        </tr>";
                    }
            // <span onclick='deleteTempApplyClaimHasProgramme($id)'>Delete</a>                            
            $table.= "
            </tbody>
            </table>
            </div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempResearchApplyClaimHasExaminer($id)
    {
        $inserted_id = $this->apply_claim_model->deleteTempResearchApplyClaimHasExaminer($id);
        if($inserted_id)
        {
            $data = $this->displayTempExaminerdata();
            echo $data;  
        }
    }




    function addResearchApplyClaimHasSupervisor()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->apply_claim_model->addNewResearchApplyClaimHasSupervisors($tempData);
        // echo "<Pre>";print_r($tempData);exit();

        echo "success";exit;
    }

    function deleteResearchApplyClaimHasSupervisor($id)
    {
        $inserted_id = $this->apply_claim_model->deleteResearchApplyClaimHasSupervisor($id);
        echo "Success"; 
    }





    function addResearchApplyClaimHasExaminer()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $inserted_id = $this->apply_claim_model->addNewResearchApplyClaimHasExaminer($tempData);
        // echo "<Pre>";print_r($tempData);exit();

        echo "success";exit;
    }

    function deleteResearchApplyClaimHasExaminer($id)
    {
        $inserted_id = $this->apply_claim_model->deleteResearchApplyClaimHasExaminer($id);
        echo "Success"; 
    }
}

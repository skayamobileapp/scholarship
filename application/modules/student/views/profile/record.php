<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <!-- <h3>Edit Student</h3> -->
        </div>



            <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($getStudentData->full_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $getStudentData->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $getStudentData->email_id; ?></dd>
                            </dl>
                            <dl>
                                <dt>Scholarship :</dt>
                                <dd><?php echo "" ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->mail_address1 ; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->mail_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing City :</dt>
                                <dd><?php echo $getStudentData->mailing_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Zipcode :</dt>
                                <dd><?php echo $getStudentData->mailing_zipcode; ?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $getStudentData->nationality ?></dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd><?php echo $getStudentData->program ?></dd>
                            </dl>
                            <!-- <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $getStudentData->intake; ?></dd>
                            </dl> -->
                            <dl>
                                <dt>Qualification Type :</dt>
                                <dd><?php echo $getStudentData->qualification_code . " - " . $getStudentData->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->permanent_address1; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->permanent_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent City :</dt>
                                <dd><?php echo $getStudentData->permanent_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Zipcode :</dt>
                                <dd><?php echo $getStudentData->permanent_zipcode; ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>
            </div>


        <br>
    <form id="form_profile" action="" method="post" enctype="multipart/form-data">
  
   <!--  <div class="form-container">
        <h4 class="form-group-title">Profile Details</h4>   -->
    
        <div class="m-auto text-center">
           <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
        </div>


        <div class="clearfix">
        <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
            <li role="presentation" class="active"><a href="#tab_one" class="nav-link border rounded text-center"
                    aria-controls="tab_one" aria-selected="true"
                    role="tab" data-toggle="tab">Profile</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Status</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Academic status</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Internship</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Apprenticeship</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Finance</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Application</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Selection</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Document</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Communication</a>
            </li>
        </ul>



        <div class="tab-content offers-tab-content">




            <div role="tabpanel" class="tab-pane active" id="tab_one">

                <div class="col-12 mt-4">
                    <br>


                <div class="form-container">
                    <h4 class="form-group-title">Profile Details</h4>

                </div>


                 
                 </div> <!-- END col-12 -->  
            </div>




            <div role="tabpanel" class="tab-pane" id="tab_two">
                <div class="col-12 mt-4">


                <br>

                    
                <div class="form-container">
                    <h4 class="form-group-title">Coming Soon ..!!!!</h4>

                </div>

                             
                </div> <!-- END col-12 -->  
            </div>


    
        </div>

       </div> <!-- END row-->
   

            <!-- <div class="button-block clearfix">
                <div class="bttn-group pull-right pull-right">
                    <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div> -->
    <!-- </div> -->
    </form>
    <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
    </footer>

    </div>
</div>
<script type="text/javascript">
    $('select').select2();

    $( function()
    {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1920:2019"
    });
    });



    function deleteVisaDetails(id)
    {
        // alert(id);
         $.ajax(
            {
               url: '/student/editProfile/deleteVisaDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something Is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }

    function deleteOtherDocuments(id)
    {
        $.ajax(
            {
               url: '/student/editProfile/deleteOtherDocuments/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something Is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }

    function deleteExamDetails(id)
    {
        $.ajax(
            {
               url: '/student/editProfile/deleteExamDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something Is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }

    function deleteEnglishProficiencyDetails(id)
    {
        $.ajax(
            {
               url: '/student/editProfile/deleteEnglishProficiencyDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something Is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }

    function deleteEmploymentDetails(id)
    {
         $.ajax(
            {
               url: '/student/editProfile/deleteEmploymentDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something Is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }





    $(document).ready(function() {
        $("#form_profile").validate({
            rules: {
                qualification_level: {
                    required: true
                },
                degree_awarded: {
                    required: true
                },
                specialization: {
                    required: true
                },
                class_degree: {
                    required: true
                },
                result: {
                    required: true
                },
                year: {
                    required: true
                },
                medium: {
                    required: true
                },
                college_country: {
                    required: true
                },
                college_name: {
                    required: true
                },
                test: {
                    required: true
                },
                date: {
                    required: true
                },
                score: {
                    required: true
                },
                company_name: {
                    required: true
                },
                company_address: {
                    required: true
                },
                telephone: {
                    required: true
                },
                fax_num: {
                    required: true
                },
                designation: {
                    required: true
                },
                position: {
                    required: true
                },
                service_year: {
                    required: true
                },
                industry: {
                    required: true
                },
                job_desc: {
                    required: true
                },
                salutation: {
                    required: true
                },
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                id_type: {
                    required: true
                },
                passport_number: {
                    required: true
                },
                passport_expiry_date: {
                    required: true
                },
                phone: {
                    required: true
                },
                gender: {
                    required: true
                },
                date_of_birth: {
                    required: true
                },
                martial_status: {
                    required: true
                },
                religion: {
                    required: true
                },
                nationality: {
                    required: true
                },
                id_race: {
                    required: true
                },
                email_id: {
                    required: true
                },
                nric: {
                    required: true
                },
                mail_address1: {
                    required: true
                },
                mail_address2: {
                    required: true
                },
                mailing_country: {
                    required: true
                },
                mailing_state: {
                    required: true
                },
                mailing_city: {
                    required: true
                },
                mailing_zipcode: {
                    required: true
                },
                permanent_address1: {
                    required: true
                },
                permanent_address2: {
                    required: true
                },
                permanent_country: {
                    required: true
                },
                permanent_state: {
                    required: true
                },
                permanent_city: {
                    required: true
                },
                permanent_zipcode: {
                    required: true
                },
                malaysian_visa: {
                    required: true
                },
                visa_expiry_date: {
                    required: true
                },
                visa_status: {
                    required: true
                },
                doc_name: {
                    required: true
                },
                remarks: {
                    required: true
                },
                visa_number: {
                    required: true
                },
                doc_file: {
                    required: true
                }
            },
            messages: {
                qualification_level: {
                    required: "<p class='error-text'>Qualification Level Required</p>",
                },
                degree_awarded: {
                    required: "<p class='error-text'>Degree Awarded Required</p>",
                },
                specialization: {
                    required: "<p class='error-text'>Specialization Required</p>",
                },
                class_degree: {
                    required: "<p class='error-text'>Degree Class Required</p>",
                },
                result: {
                    required: "<p class='error-text'>Result Required</p>",
                },
                year: {
                    required: "<p class='error-text'>Year Requred</p>",
                },
                medium: {
                    required: "<p class='error-text'>Select Medium Of Study</p>",
                },
                college_country: {
                    required: "<p class='error-text'>Country Required</p>",
                },
                college_name: {
                    required: "<p class='error-text'>College Name Required</p>",
                },
                test: {
                    required: "<p class='error-text'>Test Name Required</p>",
                },
                date: {
                    required: "<p class='error-text'>Select Date</p>",
                },
                score: {
                    required: "<p class='error-text'>Score Required</p>",
                },
                company_name: {
                    required: "<p class='error-text'>Company Name Required</p>",
                },
                company_address: {
                    required: "<p class='error-text'>Company Address Required</p>",
                },
                telephone: {
                    required: "<p class='error-text'>Telephone No. Required</p>",
                },
                fax_num: {
                    required: "<p class='error-text'>Fax No. Required</p>",
                },
                designation: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                position: {
                    required: "<p class='error-text'>Position Of Rhe Job Required</p>",
                },
                service_year: {
                    required: "<p class='error-text'>No. Of Service Required</p>",
                },
                industry: {
                    required: "<p class='error-text'>Industry Required</p>",
                },
                job_desc: {
                    required: "<p class='error-text'>Job Description Required</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name Required</p>",
                },
                id_type: {
                    required: "<p class='error-text'>ID Type Required</p>",
                },
                passport_number: {
                    required: "<p class='error-text'>Passport NUmber Required</p>",
                },
                passport_expiry_date: {
                    required: "<p class='error-text'>Select Passport Expire Date</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone Number Required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Select Gender</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                martial_status: {
                    required: "<p class='error-text'>Select Maritual Status</p>",
                },
                religion: {
                    required: "<p class='error-text'>Select Religion</p>",
                },
                nationality: {
                    required: "<p class='error-text'>Select Ntionality</p>",
                },
                id_race: {
                    required: "<p class='error-text'>Select Rece</p>",
                },
                email_id: {
                    required: "<p class='error-text'>Main Id Required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC Required</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Mailing Address 1 Required</p>",
                },
                mail_address2: {
                    required: "<p class='error-text'>Mailing Address 2 Required</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Mailing Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>Select Mailing State</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Mailing City Required</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Mailing Zipcode Required</p>",
                },
                permanent_address1: {
                    required: "<p class='error-text'>Select Permanent Address 2</p>",
                },
                permanent_address2: {
                    required: "<p class='error-text'>Select Permanent Address 2</p>",
                },
                permanent_country: {
                    required: "<p class='error-text'>Select Permanent Country</p>",
                },
                permanent_state: {
                    required: "<p class='error-text'>Select Permanent State</p>",
                },
                permanent_city: {
                    required: "<p class='error-text'>Permanent City Required</p>",
                },
                permanent_zipcode: {
                    required: "<p class='error-text'>Permanent Zipcode Required</p>",
                },
                malaysian_visa: {
                    required: "<p class='error-text'>Select Visa Type</p>",
                },
                visa_expiry_date: {
                    required: "<p class='error-text'>Select Visa Expire Date</p>",
                },
                visa_status: {
                    required: "<p class='error-text'>Visa Status Required</p>",
                },
                doc_name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                remarks: {
                    required: "<p class='error-text'>Remars Required</p>"
                },
                visa_number: {
                    required: "<p class='error-text'>Visa Number Required</p>"
                },
                doc_file: {
                    required: "<p class='error-text'>Document Required</p>"
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

  </script>
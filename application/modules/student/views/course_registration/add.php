<?php $this->load->helper("form"); ?>
<form id="form_pr_entry" action="" method="post">
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Course Registration</h3>
            </div>

            <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>

                <div class='data-list'>
                    <div class='row'>
    
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($getStudentData->full_name); ?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $getStudentData->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $getStudentData->email_id; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->mail_address1 ; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->mail_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing City :</dt>
                                <dd><?php echo $getStudentData->mailing_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Mailing Zipcode :</dt>
                                <dd><?php echo $getStudentData->mailing_zipcode; ?></dd>
                            </dl>
                        </div>        
                        
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $getStudentData->nationality ?></dd>
                            </dl>
                            <dl>
                                <dt>Programme :</dt>
                                <dd><?php echo $getStudentData->program ?></dd>
                            </dl>
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $getStudentData->intake; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Address :</dt>
                                <dd></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->permanent_address1; ?></dd>
                            </dl>
                            <dl>
                                <dt></dt>
                                <dd><?php echo $getStudentData->permanent_address2; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent City :</dt>
                                <dd><?php echo $getStudentData->permanent_city; ?></dd>
                            </dl>
                            <dl>
                                <dt>Permanent Zipcode :</dt>
                                <dd><?php echo $getStudentData->permanent_zipcode; ?></dd>
                            </dl>
                        </div>
    
                    </div>
                </div>

            </div>



        <div class="form-container">
            <h4 class="form-group-title">Course Registration</h4>

            <div class="row">
                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Course <span class='error-text'>*</span></label>
                        <select name="id_course_registered_landscape" id="id_course_registered_landscape" class="form-control" multiple>
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Semester <span class='error-text'>*</span></label>
                        <select name="id_semester" id="id_semester" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($semesterList))
                            {
                                foreach ($semesterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
              
                <div class="col-sm-4">
                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="checkValidation()">Add</button>
                </div>
            </div>

        </div>

            

        </form>





        <div class="form-container">
                <h4 class="form-group-title">Course Registered List</h4>

                <span class='error-text'>Note * : Course Registered With Withdrawl & Completed are Excluded. </span>

            <div class="custom-table">
                <table class="table">
                    <thead>
                        <tr>
                        <th>Sl. No</th>
                         <th>Course </th>
                         <th>Credit Hours </th>
                         <th>Semester </th>
                         <th>Course Registered By </th>
                         <th class="text-center">Type </th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php
                     $total = 0;
                      for($i=0;$i<count($courseRegisteredList);$i++)
                     { ?>
                        <tr>
                        <td><?php echo $i+1;?></td>
                        <td><?php echo $courseRegisteredList[$i]->course_code . " - " . $courseRegisteredList[$i]->course_name;?></td>
                        <td><?php echo $courseRegisteredList[$i]->credit_hours;?></td>
                        <td><?php echo $courseRegisteredList[$i]->semester_code . " - " . $courseRegisteredList[$i]->semester_name;?></td>
                        <td>
                            <?php if($courseRegisteredList[$i]->by_student > 0)
                            {
                                echo $student_name . " ( SELF )";
                            }
                            else
                            {
                                echo $courseRegisteredList[$i]->role; 
                            } 
                        ?>
                                
                        </td>

                        <td class="text-center">
                            <?php echo $courseRegisteredList[$i]->course_type;  ?>
                           <!--  <?php if($courseRegisteredList[$i]->by_student > 0)
                            {
                            ?>
                                <?php $id = $courseRegisteredList[$i]->id; ?>
                                <a class='btn btn-sm btn-edit' onclick='deleteCourseRegistration(<?php echo $id; ?>)'>Delete</a>                                
                            <?php
                            }
                            else
                            {
                                
                            }
                        ?> -->
                        </td>
                      <?php 
                     
                  }
                  ?>
                    </tbody>
                </table>
            </div>
        </div>


       


        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $('select').select2();

    function deleteCourseRegistration(id)
    {
        // alert(id);
        $.ajax(
        {
           url: '/student/courseRegistration/deleteCourseRegistration/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            // alert(result);
            window.location.reload();
           }
        });
    }

    function checkValidation()
    {
        if($('#form_pr_entry').valid())
        {
            // $('#form_pr_entry').submit();

            var tempPR = {};
            tempPR['id_semester'] = $("#id_semester").val();
            tempPR['id_course_registered_landscape'] = $("#id_course_registered_landscape").val();

                    // alert($tempPR['id_tax']);
            // if ($tempPR['id_tax'] != '' && $tempPR['quantity'] != '' && $tempPR['price'] != '')
            // {

                $.ajax(
                {
                   url: '/student/courseRegistration/addCourseRegistration',
                   type: 'POST',
                   data:
                   {
                    tempData: tempPR
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                    // alert(result);
                     window.location.reload();                   
                   }
                });

        }    
    }



  $(document).ready(function() {
        $("#form_pr_entry").validate({
            rules: {
                id_course_registered_landscape: {
                    required: true
                },
                 id_semester: {
                    required: true
                }
            },
            messages: {
                id_course_registered_landscape: {
                    required: "<p class='error-text'>Select Course",
                },
                id_semester: {
                    required: "<p class='error-text'>Sellect Semester",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }


  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1960:2020"
    });
  } );
</script>
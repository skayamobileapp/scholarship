            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="/student/profile" class="user-profile-link">
                        <span>
                            <img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg">
                        </span> <?php echo $student_name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <!-- <li><a href="/sponser/user/profile">Edit Profile</a></li> -->
                            <li><a href="/student/profile/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    <h4>Profile</h4>
                    <ul>
                        <li><a href="/student/editProfile/edit">Edit Profile</a></li>
                    </ul>
                    <h4>Records</h4>
                    <ul>
                        <li><a href="/student/editProfile/record">Student Records</a></li>
                    </ul>
                    <h4>Finance</h4>
                    <ul>
                        <li><a href="/student/applyClaim/list">Apply Claim</a></li>
                    </ul>
                </div>
            </div>
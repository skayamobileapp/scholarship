            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $scholar_name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <!-- <li><a href="/sponser/user/profile">Edit Profile</a></li> -->
                            <li><a href="/program/role/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                  
<h4>Module Setup</h4>
                    <ul>
                        <li><a href="/program/moduleType/list">Module Type Setup</a></li>
                        <li><a href="/program/assesmentMethod/list">Assessment Method Setup</a></li>
                                                <li><a href="/program/modulelisting/list">Module Listing</a></li>


                      

                    </ul>


                    <h4>Program Setup</h4>
                    <ul>
                        <li><a href="/program/award/list">Award Setup</a></li>
                        <li><a href="/program/programme/list">Program Setup</a></li>
                    <li><a href="/program/programSyllabus/list">Program Syllabus</a></li>

                        <li><a href="/program/programAccreditation/list">Program Accreditation</a></li>
                        <li><a href="/program/IndividualEntryRequirement/list">Entry Requirement</a></li>
                        <li><a href="/program/applicantInformation/programList">Application form Set-up</a></li>
                    </ul>


                   
                    

                </div>
            </div>

<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Module Listing</h3>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Module Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
             <th>Sl. No</th>
            <th>Module Code</th>
            <th>Module Name</th>
            <th>Credit Hours</th>
            <th>Scholarship Name</th>
            <th>Programme Name</th>
            <th>Thrust Name</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($moduleList)) {
            $i=1;
            foreach ($moduleList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                  <td><?php echo $record->code ?></td>
                  <td><?php echo $record->name ?></td>
                  <td><?php echo $record->credit_hours ?></td>
                  <td><?php echo $record->scholarship_name ?></td>
                  <td><?php echo $record->program_name ?></td>
                  <td><?php echo $record->thrust_name ?></td>                 
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    function clearSearchForm()
    {
      window.location.reload();
    }
</script>
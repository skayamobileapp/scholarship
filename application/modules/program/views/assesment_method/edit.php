<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Assesment Method</h3>
        </div>
        <form id="form_award" action="" method="post">

         <div class="form-container">
            <h4 class="form-group-title">Assesment Method Details</h4>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $assesmentMethod->name; ?>">
                    </div>
                </div>

                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="type" name="type" value="<?php echo $assesmentMethod->type; ?>">
                    </div>
                </div> -->


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($assesmentMethod->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($assesmentMethod->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>               
            </div>

        </div>
        

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();


    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                description: {
                    required: true
                },
                id_programme: {
                    required: true
                },
                level: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Assesment Method Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                description: {
                    required: "<p class='error-text'>Assesment Method Description required</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                level: {
                    required: "<p class='error-text'>Select Level</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
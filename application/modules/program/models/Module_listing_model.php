<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Module_listing_model extends CI_Model
{
    function moduleList()
    {
        $this->db->select('spshm.*,t.name as thrust_name, t.code as thrust_code, st.scholarship_name, st.scholarship_code, sp.code as program_code, sp.name as program_name');
        $this->db->from('scholarship_program_syllabus_has_module as spshm');

        $this->db->join('scholarship_program_syllabus as fc', 'spshm.id_program_syllabus = fc.id');
        $this->db->join('scholarship_thrust as t', 'fc.id_thrust = t.id');
        $this->db->join('scholarship_sub_thrust as st', 'fc.id_sub_thrust = st.id');
        $this->db->join('scholarship_programme as sp', 'fc.id_program = sp.id');
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }

   
}


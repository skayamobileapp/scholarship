<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ModuleType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('module_type_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('module_type.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['moduleTypeList'] = $this->module_type_model->moduleTypeListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Module Type List';
            $this->loadViews("module_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('module_type.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->module_type_model->addNewModuleType($data);
                redirect('/program/moduleType/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add Module Type';
            $this->loadViews("module_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('module_type.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/program/moduleType/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->module_type_model->editModuleType($data,$id);
                redirect('/program/moduleType/list');
            }
            $data['moduleType'] = $this->module_type_model->getModuleType($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Module Type';
            $this->loadViews("module_type/edit", $this->global, $data, NULL);
        }
    }
}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ApplicantInformation extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('applicant_information_model');
        $this->load->model('programme_model');
        // $this->load->model('intake_model');
        $this->isScholarLoggedIn();
    }

    function programList()
    {
        if ($this->checkScholarAccess('scholarship_programme.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $name = $this->security->xss_clean($this->input->post('name'));

            $data['searchName'] = $name; 

            $data['programmeList'] = $this->programme_model->programmeListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Program List';
            $this->loadViews("applicant_information/programme_list", $this->global, $data, NULL);
        }
    }

    function programmeDetails($id_programme = NULL)
    {
        if ($this->checkScholarAccess('scholarship_programme.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit;

                $is_personel_details  = $this->security->xss_clean($this->input->post('is_personel_details'));
                $is_education_details  = $this->security->xss_clean($this->input->post('is_education_details'));
                $is_family_details  = $this->security->xss_clean($this->input->post('is_family_details'));
                $is_financial_recoverable_details  = $this->security->xss_clean($this->input->post('is_financial_recoverable_details'));
                $is_file_upload_details  = $this->security->xss_clean($this->input->post('is_file_upload_details'));
                $is_employment_details  = $this->security->xss_clean($this->input->post('is_employment_details'));



                $data = array(
                    'is_personel_details' => $is_personel_details,
                    'is_education_details' => $is_education_details,
                    'is_family_details' => $is_family_details,
                    'is_financial_recoverable_details' => $is_financial_recoverable_details,
                    'is_file_upload_details' => $is_file_upload_details,
                    'is_employment_details' => $is_employment_details,
                     );

                $updated = $this->applicant_information_model->updateProgram($data,$id_programme);


                $id_is_personel_details  = $this->security->xss_clean($this->input->post('id_is_personel_details'));
                $is_salutation  = $this->security->xss_clean($this->input->post('is_salutation'));
                $is_first_name  = $this->security->xss_clean($this->input->post('is_first_name'));
                $is_last_name  = $this->security->xss_clean($this->input->post('is_last_name'));
                $is_nric  = $this->security->xss_clean($this->input->post('is_nric'));
                $is_phone  = $this->security->xss_clean($this->input->post('is_phone'));
                $is_email_id  = $this->security->xss_clean($this->input->post('is_email_id'));
                $is_gender  = $this->security->xss_clean($this->input->post('is_gender'));
                $is_date_of_birth  = $this->security->xss_clean($this->input->post('is_date_of_birth'));
                $is_martial_status  = $this->security->xss_clean($this->input->post('is_martial_status'));
                $is_id_race  = $this->security->xss_clean($this->input->post('is_id_race'));
                $is_religion  = $this->security->xss_clean($this->input->post('is_religion'));
                $is_mail_address1  = $this->security->xss_clean($this->input->post('is_mail_address1'));
                $is_mail_address2  = $this->security->xss_clean($this->input->post('is_mail_address2'));
                $is_mailing_country  = $this->security->xss_clean($this->input->post('is_mailing_country'));
                $is_mailing_state  = $this->security->xss_clean($this->input->post('is_mailing_state'));
                $is_mailing_city  = $this->security->xss_clean($this->input->post('is_mailing_city'));
                $is_mailing_zipcode  = $this->security->xss_clean($this->input->post('is_mailing_zipcode'));
                $is_permanent_address1  = $this->security->xss_clean($this->input->post('is_permanent_address1'));
                $is_permanent_address2  = $this->security->xss_clean($this->input->post('is_permanent_address2'));
                $is_permanent_country  = $this->security->xss_clean($this->input->post('is_permanent_country'));
                $is_permanent_state  = $this->security->xss_clean($this->input->post('is_permanent_state'));
                $is_permanent_city  = $this->security->xss_clean($this->input->post('is_permanent_city'));
                $is_permanent_zipcode  = $this->security->xss_clean($this->input->post('is_permanent_zipcode'));

                $data = array(
                        'is_salutation' => $is_salutation,
                        'is_first_name' => $is_first_name,
                        'is_last_name' => $is_last_name,
                        'is_nric' => $is_nric,
                        'is_phone' => $is_phone,
                        'is_email_id' => $is_email_id,
                        'is_gender' => $is_gender,
                        'is_date_of_birth' => $is_date_of_birth,
                        'is_martial_status' => $is_martial_status,
                        'is_id_race' => $is_id_race,
                        'is_religion' => $is_religion,
                        'is_mail_address1' => $is_mail_address1,
                        'is_mail_address2' => $is_mail_address2,
                        'is_mailing_country' => $is_mailing_country,
                        'is_mailing_state' => $is_mailing_state,
                        'is_mailing_city' => $is_mailing_city,
                        'is_mailing_zipcode' => $is_mailing_zipcode,
                        'is_permanent_address1' => $is_permanent_address1,
                        'is_permanent_address2' => $is_permanent_address2,
                        'is_permanent_country' => $is_permanent_country,
                        'is_permanent_state' => $is_permanent_state,
                        'is_permanent_city' => $is_permanent_city,
                        'is_permanent_zipcode' => $is_permanent_zipcode
                     );

                $updated = $this->applicant_information_model->updateProgramIsPersonelDetails($data,$id_is_personel_details);


                $is_qualification_level  = $this->security->xss_clean($this->input->post('is_qualification_level'));
                $id_is_qualification_details  = $this->security->xss_clean($this->input->post('id_is_qualification_details'));
                $is_degree_awarded  = $this->security->xss_clean($this->input->post('is_degree_awarded'));
                $is_specialization  = $this->security->xss_clean($this->input->post('is_specialization'));
                $is_class_degree  = $this->security->xss_clean($this->input->post('is_class_degree'));
                $is_result  = $this->security->xss_clean($this->input->post('is_result'));
                $is_year  = $this->security->xss_clean($this->input->post('is_year'));
                $is_medium  = $this->security->xss_clean($this->input->post('is_medium'));
                $is_college_country  = $this->security->xss_clean($this->input->post('is_college_country'));
                $is_college_name  = $this->security->xss_clean($this->input->post('is_college_name'));
                $is_certificate  = $this->security->xss_clean($this->input->post('is_certificate'));
                $is_transcript  = $this->security->xss_clean($this->input->post('is_transcript'));

                $data = array(
                    'is_qualification_level' => $is_qualification_level,
                    'is_degree_awarded' => $is_degree_awarded,
                    'is_specialization' => $is_specialization,
                    'is_class_degree' => $is_class_degree,
                    'is_result' => $is_result,
                    'is_year' => $is_year,
                    'is_medium' => $is_medium,
                    'is_college_country' => $is_college_country,
                    'is_college_name' => $is_college_name,
                    'is_certificate' => $is_certificate,
                    'is_transcript' => $is_transcript
                     );

                $updated = $this->applicant_information_model->updateProgramIsQualificationDetails($data,$id_is_qualification_details);



                $is_father_name  = $this->security->xss_clean($this->input->post('is_father_name'));
                $id_is_family_details  = $this->security->xss_clean($this->input->post('id_is_family_details'));
                $is_mother_name  = $this->security->xss_clean($this->input->post('is_mother_name'));
                $is_father_deceased  = $this->security->xss_clean($this->input->post('is_father_deceased'));
                $is_father_occupation  = $this->security->xss_clean($this->input->post('is_father_occupation'));
                $is_no_siblings  = $this->security->xss_clean($this->input->post('is_no_siblings'));
                $is_est_fee  = $this->security->xss_clean($this->input->post('is_est_fee'));
                $is_family_annual_income  = $this->security->xss_clean($this->input->post('is_family_annual_income'));

                $data = array(
                'is_father_name' => $is_father_name,
                'is_mother_name' => $is_mother_name,
                'is_father_deceased' => $is_father_deceased,
                'is_father_occupation' => $is_father_occupation,
                'is_no_siblings' => $is_no_siblings,
                'is_est_fee' => $is_est_fee,
                'is_family_annual_income' => $is_family_annual_income,
                );

                $updated = $this->applicant_information_model->updateProgramIsFamilyDetails($data,$id_is_family_details);



                $is_company_name  = $this->security->xss_clean($this->input->post('is_company_name'));
                $id_is_employment_details  = $this->security->xss_clean($this->input->post('id_is_employment_details'));
                $is_company_address  = $this->security->xss_clean($this->input->post('is_company_address'));
                $is_telephone  = $this->security->xss_clean($this->input->post('is_telephone'));
                $is_fax_num  = $this->security->xss_clean($this->input->post('is_fax_num'));
                $is_designation  = $this->security->xss_clean($this->input->post('is_designation'));
                $is_position  = $this->security->xss_clean($this->input->post('is_position'));
                $is_service_year  = $this->security->xss_clean($this->input->post('is_service_year'));
                $is_industry  = $this->security->xss_clean($this->input->post('is_industry'));
                $is_job_desc  = $this->security->xss_clean($this->input->post('is_job_desc'));
                $is_employment_letter  = $this->security->xss_clean($this->input->post('is_employment_letter'));


                $data = array(
                    'is_company_name' => $is_company_name,
                    'is_company_address' => $is_company_address,
                    'is_telephone' => $is_telephone,
                    'is_fax_num' => $is_fax_num,
                    'is_designation' => $is_designation,
                    'is_position' => $is_position,
                    'is_service_year' => $is_service_year,
                    'is_industry' => $is_industry,
                    'is_job_desc' => $is_job_desc,
                    'is_employment_letter' => $is_employment_letter
                     );

                $updated = $this->applicant_information_model->updateProgramIsEmploymentDetails($data,$id_is_employment_details);

                redirect('/program/applicantInformation/programList');
            }

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_scholarship_program'] = $this->security->xss_clean($this->input->post('id_scholarship_program'));
            
            $data['searchParam'] = $formData;

            $data['programme'] = $this->applicant_information_model->getProgramme($id_programme);            
            $data['id_programme'] = $id_programme;

            $data['programmeDetailsList'] = $this->applicant_information_model->programmeApplicantDocsByProgramId($id_programme,$formData);

            $data['isPersonel'] = $this->applicant_information_model->getIsPersonelDetails($id_programme);
            $data['isQualification'] = $this->applicant_information_model->getIsQualificationDetails($id_programme);
            $data['isFamily'] = $this->applicant_information_model->getIsFamilyDetails($id_programme);
            $data['isEmployment'] = $this->applicant_information_model->getIsEmploymentDetails($id_programme);

            
            // echo "<Pre>";print_r($data);exit;

            $this->global['pageTitle'] = 'Scholarship Management System : Program Landscape List';
            $this->loadViews("applicant_information/view", $this->global, $data, NULL);
        }
    }
    
    function add($id_programme = NULL)
    {
        if ($this->checkScholarAccess('applicant_information.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                // $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $min_total_cr_hrs = $this->security->xss_clean($this->input->post('min_total_cr_hrs'));
                $min_repeat_course = $this->security->xss_clean($this->input->post('min_repeat_course'));
                $max_repeat_exams = $this->security->xss_clean($this->input->post('max_repeat_exams'));
                $total_semester = $this->security->xss_clean($this->input->post('total_semester'));
                $total_block = $this->security->xss_clean($this->input->post('total_block'));
                $total_level = $this->security->xss_clean($this->input->post('total_level'));
                $min_total_score = $this->security->xss_clean($this->input->post('min_total_score'));
                $min_pass_subject = $this->security->xss_clean($this->input->post('min_pass_subject'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $result = $this->applicant_information_model->programmeLandscapeDuplicationCheck($id_programme,$id_intake);

         // echo "<Pre>";print_r($result);exit;


                if($result == '1' )
                {

                    // Duplicate Found

                // echo "<script>alert('Programme Landscape Already Added For This Intake And Programme!');
                // </script>";

       
                // redirect('orderManagement/index', 'refresh');
                echo "<Pre>";print_r('Dullicate Entry Not Allowed');exit;
                // redirect('/setup/programmeLandscape/programmeLandscapeList/'.$id_programme);

                }
                else
                {
                    $data = array(
                    'name' => $name,
                    'id_programme' => $id_programme,
                    'id_intake' => $id_intake,
                    'min_total_cr_hrs' => $min_total_cr_hrs,
                    'min_repeat_course' => $min_repeat_course,
                    'max_repeat_exams' => $max_repeat_exams,
                    'total_semester' => $total_semester,
                    'total_block' => $total_block,
                    'total_level' => $total_level,
                    'min_total_score' => $min_total_score,
                    'min_pass_subject' => $min_pass_subject,
                    'status' => $status
                );
            // echo "<pre>"; print_r($min_total_cr_hrs);exit();
                $result = $this->applicant_information_model->addNewProgrammeLandscapeDetails($data);
                redirect('/setup/programmeLandscape/programmeLandscapeList/'.$id_programme);
                }
            }
            $data['programme'] = $this->applicant_information_model->getProgramme($id_programme);

            $data['programmeList'] = $this->programme_model->programmeList();
            $data['intakeList'] = $this->applicant_information_model->intakeListForLandscape($id_programme);
            $this->global['pageTitle'] = 'Scholarship Management System : Add Program Landscape';
            $this->loadViews("applicant_information/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL,$id_programme = NULL)
    {
        if ($this->checkScholarAccess('applicant_information.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // echo "<pre>"; print_r($id);exit();
            if ($id == null)
            {
                redirect('/setup/programmeLandscape/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                // $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $min_total_cr_hrs = $this->security->xss_clean($this->input->post('min_total_cr_hrs'));
                $min_repeat_course = $this->security->xss_clean($this->input->post('min_repeat_course'));
                $max_repeat_exams = $this->security->xss_clean($this->input->post('max_repeat_exams'));
                $total_semester = $this->security->xss_clean($this->input->post('total_semester'));
                $total_block = $this->security->xss_clean($this->input->post('total_block'));
                $total_level = $this->security->xss_clean($this->input->post('total_level'));
                $min_total_score = $this->security->xss_clean($this->input->post('min_total_score'));
                $min_pass_subject = $this->security->xss_clean($this->input->post('min_pass_subject'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $result = $this->applicant_information_model->programmeLandscapeDuplicationCheck($id_programme,$id_intake);

         // echo "<Pre>";print_r($result);exit;


                if($result == '1' )
                {

                    // Duplicate Found
                    echo "<Pre>";print_r('Dullicate Entry Not Allowed');exit;

                }
                else
                {
            
                    $data = array(
                        'name' => $name,
                        'id_programme' => $id_programme,
    					'min_total_cr_hrs' => $min_total_cr_hrs,
    					'min_repeat_course' => $min_repeat_course,
    					'max_repeat_exams' => $max_repeat_exams,
    					'total_semester' => $total_semester,
    					'total_block' => $total_block,
    					'total_level' => $total_level,
    					'min_total_score' => $min_total_score,
    					'min_pass_subject' => $min_pass_subject,
                        'status' => $status
                    );
                    // echo "<pre>"; print_r($data);exit();

                    $result = $this->applicant_information_model->editProgrammeLandscapeDetails($data,$id);
                    redirect('/setup/programmeLandscape/programmeLandscapeList/'.$id_programme);
                }
            }
            $data['programmeList'] = $this->programme_model->programmeList();
            $data['intakeList'] = $this->intake_model->intakeList();
            $data['programmeLandscapeDetails'] = $this->applicant_information_model->getProgrammeLandscapeDetails($id);
            // echo "<pre>"; print_r($data['programmeLandscapeDetails']);exit();
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Program Landscape';
            $this->loadViews("applicant_information/edit", $this->global, $data, NULL);
        }
    }

    function addcourse($id = NULL, $id_programme = NULL, $id_intake = NULL)
    {
        if ($this->checkScholarAccess('applicant_information.addcourse') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/programmeLandscape/list');
            }
            if($this->input->post())
            {
                $id_program_landscape = $id;
                $id_semester = $this->security->xss_clean($this->input->post('id_semester'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                // $id_intake = $this->security->xss_clean($this->input->post('id_intake'));
                $course_type = $this->security->xss_clean($this->input->post('course_type'));
                $pre_requisite = $this->security->xss_clean($this->input->post('pre_requisite'));

                $result = $this->applicant_information_model->programmeLandscapeCourseDuplicationCheck($id_program_landscape,$id_course);

         // echo "<Pre>";print_r($result);exit;


                if($result == '1' )
                {

                    // Duplicate Found
                    echo "<Pre>";print_r('Duplicate Entry Not Allowed');exit;

                }
                else
                {

                    $data = array(
                        'id_program_landscape' => $id_program_landscape,
                        'id_semester' => $id_semester,
                        'id_course' => $id_course,
                        'id_intake' => $id_intake,
                        'id_program' => $id_programme,
                        'course_type' => $course_type,
                        'pre_requisite' => $pre_requisite
                    );
                    // echo "<pre>"; print_r($data);exit();

                    $result = $this->applicant_information_model->addCourseToProgramLandscape($data);
                    // redirect('/setup/programmeLandscape/programmeLandscapeList/'.$id_programme);
                   redirect($_SERVER['HTTP_REFERER']);

                }
            }
            $data['id_programme'] = $id_programme;
            $data['id_intake'] = $id_intake;
            $data['programmeList'] = $this->programme_model->programmeList();
            $data['semesterList'] = $this->semester_model->semesterList();
            $data['courseList'] = $this->course_model->courseList();
            $data['intakeList'] = $this->intake_model->intakeList();

            $data['programmeLandscapeDetails'] = $this->applicant_information_model->getProgrammeLandscapeDetails($id);
            $data['getCompulsoryCourse'] = $this->applicant_information_model->getCompulsoryCourse($id);
            $data['getMajorCourse'] = $this->applicant_information_model->getMajorCourse($id);
            $data['getMinorCourse'] = $this->applicant_information_model->getMinorCourse($id);
            $data['getNotCompulsoryCourse'] = $this->applicant_information_model->getNotCompulsoryCourse($id);
            // echo "<pre>"; print_r($data['programmeLandscapeDetails']);exit();
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Program Landscape';
            $this->loadViews("applicant_information/addcourse", $this->global, $data, NULL);
        }
    }

    function delete_course_program()
    {
        $id = $this->input->get('id');

       $this->applicant_information_model->deleteCourseFromProgramLandscape($id);

       redirect($_SERVER['HTTP_REFERER']);
    }
}

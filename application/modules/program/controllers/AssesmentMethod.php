<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AssesmentMethod extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('assesment_method_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('assesment_method.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            // $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));

            $data['searchParameters'] = $formData;
            $data['assesmentMethodList'] = $this->assesment_method_model->assesmentMethodListSearch($formData);
            $this->global['pageTitle'] = 'Scholarship Management System : Assesment Method List';
            $this->loadViews("assesment_method/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('assesment_method.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_scholar_session_id;

                $type = $this->security->xss_clean($this->input->post('type'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'type' => $type,
                    'name' => $name,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->assesment_method_model->addNewAssesmentMethod($data);
                redirect('/program/assesmentMethod/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add AssesmentMethod';
            $this->loadViews("assesment_method/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('assesment_method.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/program/assesmentMethod/list');
            }
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_scholar_session_id;

                $type = $this->security->xss_clean($this->input->post('type'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'type' => $type,
                    'name' => $name,
                    'status' => $status,
                    'updated_by' => $id_user
                );

                $result = $this->assesment_method_model->editAssesmentMethod($data,$id);
                redirect('/program/assesmentMethod/list');
            }
            $data['assesmentMethod'] = $this->assesment_method_model->getAssesmentMethod($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit AssesmentMethod';
            $this->loadViews("assesment_method/edit", $this->global, $data, NULL);
        }
    }
}

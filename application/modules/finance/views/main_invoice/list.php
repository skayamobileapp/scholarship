<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Invoice</h3>
      <a href="add" class="btn btn-primary">+ Add Invoice</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">

              <div class="row">

                  <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Invoice Number</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" name="invoice_number" value="<?php echo $searchParam['invoice_number']; ?>">
                    </div>
                  </div>
                </div>

                 <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Type </label>
                    <div class="col-sm-8">
                      <select name="type" id="type" class="form-control">
                        <option value="">Select</option>
                        <option value="Partner University" <?php if($searchParam['type']=='Partner University'){ echo "selected"; } ?>>Partner University</option>
                        <option value="Scholar" <?php if($searchParam['type']=='Scholar'){ echo "selected"; } ?>>Scholar</option>
                        <!-- <option value="Sponser" <?php if($searchParam['type']=='Sponser'){ echo "selected"; } ?>>Sponser</option> -->
                      </select>
                    </div>
                  </div>
                </div>


              </div>


              <div class="row">


                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Cohert</label>
                    <div class="col-sm-8">
                      <select name="id_cohert" id="id_cohert" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($schemeList)) {
                          foreach ($schemeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_cohert']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Scholarship</label>
                    <div class="col-sm-8">
                      <select name="id_scholarship" id="id_scholarship" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($scholarshipList)) {
                          foreach ($scholarshipList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_scholarship']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->scholarship_code . " - " . $record->scholarship_name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>



              </div>

              <div class="row">


                 <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Programme </label>
                    <div class="col-sm-8">
                      <select name="id_program" id="id_program" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programList)) {
                          foreach ($programList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_program']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                


                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Status </label>
                    <div class="col-sm-8">
                      <select name="status" id="status" class="form-control">
                        <option value="">Select</option>
                        <option value="0" <?php if($searchParam['status']=='0'){ echo "selected"; } ?>>Pending</option>
                        <option value="1" <?php if($searchParam['status']=='1'){ echo "selected"; } ?>>Approved</option>
                        <option value="2" <?php if($searchParam['status']=='2'){ echo "selected"; } ?>>Cancelled</option>
                      </select>
                    </div>
                  </div>
                </div>               

              </div>
              

              </div>

              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <th>Sl. No</th>
            <th>Invoice Number</th>
            <th>Type</th>
            <th>Invoice For</th>
            <th>Programme</th>
            <th>Cohort</th>
            <th>Scholarship</th>
            <th>Invoice Total</th>
            <th>Total Discount</th>
            <th>Total Payable</th>
            <th>Paid </th>
            <th>Balance </th>
            <th>Remarks</th>
            <th>Invoice Date</th>
            <th>Currency</th>
            <th style="text-align: center;">Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($mainInvoiceList)) {
            $i=1;
            foreach ($mainInvoiceList as $record)
            {
              $total_amount = number_format($record->total_amount, 2, '.', ',');
              $balance_amount = number_format($record->balance_amount, 2, '.', ',');
              $paid_amount = number_format($record->paid_amount, 2, '.', ',');
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->invoice_number ?></td>
                <td><?php echo $record->type ?></td>
                <td><?php echo $record->type; ?></td>
                <td><?php echo $record->program_code . " - " . $record->program_name ?></td>
                <td><?php echo $record->scheme_code . " - " . $record->scheme_name ?></td>
                <td><?php echo $record->scholarship_code . " - " . $record->scholarship_name ?></td>
                <td><?php echo $record->invoice_total ?></td>
                <td><?php echo $record->total_discount ?></td>
                <td><?php echo $total_amount ?></td>
                <td><?php echo $paid_amount ?></td>
                <td><?php echo $balance_amount ?></td>
                <td><?php echo $record->remarks ?></td>
                <td><?php echo date("d-m-Y", strtotime($record->date_time)) ?></td>
                <td><?php echo $record->currency ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Approved";
                }
                else if( $record->status == '0')
                {
                   echo "Pending";
                }
                else if( $record->status == '2')
                {
                  echo "Cancelled";
                } 
                ?></td>
                <td class="text-center"> View



                  <!-- <a href="<?php echo 'edit/' . $record->id; ?>" title="View">View</a> -->
                  
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script type="text/javascript">
  
  $('select').select2();

  function clearSearchForm()
      {
        window.location.reload();
      }
</script>
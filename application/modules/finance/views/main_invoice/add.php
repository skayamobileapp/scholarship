<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Invoice</h3>
        </div>

    <form id="form_main_invoice" action="" method="post">
    
        <div class="form-container">
            <h4 class="form-group-title">Invoice</h4>
            
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Select Type<span class='error-text'>*</span></label>
                        <select name="type" id="type" class="form-control">
                            <option value="">Select</option>
                            <option value="Partner University">Partner University</option>
                            <option value="Scholar">Scholar</option>
                        </select>
                    </div>
                </div> 

                <div class="col-sm-4">
                  <div class="form-group">
                      <label>Cohort <span class='error-text'>*</span></label>
                      <select name="id_cohert" id="id_cohert" class="form-control selitemIcon" onchange="getScholarshipByIdCohort(this.value)">
                          <option value="">Select</option>
                          <?php
                          if (!empty($schemeList))
                          {
                              foreach ($schemeList as $record)
                              {?>
                                  <option value="<?php echo $record->id;?>"
                                  ><?php echo $record->code . " - " . $record->name; ?>
                                  </option>
                              <?php
                              }
                          }
                          ?>

                      </select>
                  </div>
                </div>


                <div class="col-sm-4">
                      <div class="form-group">
                          <label>Scholarship <span class='error-text'>*</span></label>
                          
                          <span id="view_scholarship">
                            <select class="form-control" id='id_scholarship' name='id_scholarship'>
                                <option value=''></option>
                              </select>
                         </span>

                      </div>
                </div>

                                  
            </div>

            <div class="row">

                <div class="col-sm-4">
                      <div class="form-group">
                          <label>Program <span class='error-text'>*</span></label>
                          
                          <span id="view_program">
                            <select class="form-control" id='id_program' name='id_program'>
                                <option value=''></option>
                              </select>
                         </span>

                      </div>
                </div>


                <div class="col-sm-4">
                  <div class="form-group">
                      <label>Student <span class='error-text'>*</span></label>
                      
                      <span id="view_student">
                        <select class="form-control" id='id_student' name='id_student'  onchange="getRefundableAmount()">
                            <option value=''></option>
                          </select>
                     </span>

                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Currency <span class='error-text'>*</span></label>
                        <select name="currency" id="currency" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($currencyList))
                            {
                                foreach ($currencyList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->code . " - " . $record->name; ?>
                                    </option>
                                <?php
                                }
                            }
                            ?>

                        </select>
                    </div>
                </div>


            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="total_amount" name="total_amount"  value="" readonly>
                    </div>
                </div>


                <div  id='view_student_data'>
                </div> 
            </div>
        </div>

        <div id="view_student_details"  style="display: none;">
        </div>

        <!-- <div class="form-container">
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                      <label>Currency <span class='error-text'>*</span></label>
                      <select name="currency" id="currency" class="form-control">
                          <option value="">Select</option>
                          <option value="MYR">MYR</option>
                          <option value="USD">USD</option>
                      </select>
                    </div>
                </div>  

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Total Amount <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="total_amount" name="total_amount" readonly="readonly">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Date Time () <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" value="<?php echo date('d-m-Y'); ?>" readonly="readonly">
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Remarks </label>
                        <input type="text" class="form-control" id="remarks" name="remarks">
                    </div>
                </div>
            </div>
        </div> -->


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>
    
    </form>






    <form id="form_main_invoice_details" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Main Invoice Details</h4>

            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fee Item <span class='error-text'>*</span></label>
                            <span id="view_fee_item">
                                <select class="form-control" id='id_fee_item' name='id_fee_item'>
                                    <option value=''></option>
                                  </select>
                             </span>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount <span class='error-text'>*</span></label>
                            <input type="number"  class="form-control" id="amount" name="amount">
                        </div>
                    </div>
              
                <div class="col-sm-4">
                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                </div>
            </div>

            <div id="view">
            </div>   

        </div>        

            

        </form>

        </div>

    </div>

        </div>

        <footer class="footer-wrapper">
                <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>

<script>

    $('select').select2();



    function getScholarshipByIdCohort(id)
    {
      $.get("/finance/mainInvoice/getScholarshipByIdCohort/"+id, function(data, status)
      {
          $("#view_scholarship").html(data);
          $("#view_scholarship").show();
      });

    }




    function getProgramByScholarshipNCohortId()
    {
        var tempPR = {};
        // tempPR['id_program'] = idprogram;
        tempPR['id_cohert'] = $("#id_cohert").val();
        tempPR['id_scholarship'] = $("#id_scholarship").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_cohert'] != '' && tempPR['id_scholarship'] != '')
        {

            $.ajax(
            {
               url: '/finance/mainInvoice/getProgramByScholarshipNCohortId',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  $("#view_program").html(result);
                  $("#view_program").show();
               }
            });
        }
    }


    function getStudentByData()
    {
        var tempPR = {};
        // tempPR['id_program'] = idprogram;
        tempPR['id_cohert'] = $("#id_cohert").val();
        tempPR['id_scholarship'] = $("#id_scholarship").val();
        tempPR['id_program'] = $("#id_program").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_cohert'] != '' && tempPR['id_scholarship'] != '' && tempPR['id_program'] != '')
        {

            $.ajax(
            {
               url: '/finance/mainInvoice/getStudentByData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  $("#view_student").html(result);
                  $("#view_student").show();
               }
            });
        }


        var tempPR = {};
        // tempPR['id_program'] = idprogram;
        tempPR['id_cohert'] = $("#id_cohert").val();
        tempPR['id_scholarship'] = $("#id_scholarship").val();
        tempPR['id_program'] = $("#id_program").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_cohert'] != '' && tempPR['id_scholarship'] != '' && tempPR['id_program'] != '')
        {

            $.ajax(
            {
               url: '/finance/mainInvoice/getFeeSetupListByData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  $("#view_fee_item").html(result);
                  $("#view_fee_item").show();
               }
            });
        }
    }

    function getStudentDetailsById(id_student)
    {
        // var id_student = $("#id_student").val();

        // alert(id_student);

        if(id_student != '')
        {
            $.get("/finance/mainInvoice/getStudentByStudentId/"+id_student, function(data, status)
            {
              $("#view_student_data").html(data);
              $("#view_student_data").show();
            });
        }
    }


    function saveData()
    {
        if($('#form_main_invoice_details').valid())
        {
        var tempPR = {};
        tempPR['id_fee_item'] = $("#id_fee_item").val();
        tempPR['amount'] = $("#amount").val();
        // tempPR['id'] = $("#id").val();
            $.ajax(
            {
               url: '/finance/mainInvoice/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                var ta = $("#inv-total-amount").val();
                $("#total_amount").val(ta);
                $('#myModal').modal('hide');
               }
            });
        }
    }

    function deleteTempData(id) {
         $.ajax(
            {
               url: '/finance/mainInvoice/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                var ta = $("#inv-total-amount").val();
                $("#total_amount").val(ta);
               }
            });
    }



    function validateDetailsData()
    {
        if($('#form_main_invoice').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam=='')
            {
                alert("Add Invoice Details");
            }
            else
            {
                $('#form_main_invoice').submit();
            }
        }    
    }



    function getStudentByProgramme()
    {
        var tempPR = {};
        tempPR['id_program'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['type'] = $("#type").val();
        // tempPR['id'] = $("#id").val();
        if(tempPR['id_program'] != '' && tempPR['id_intake'] != '' && tempPR['type'] != '')
        {
            $.ajax(
            {
               url: '/finance/mainInvoice/getStudentByProgrammeId',
                type: 'POST',
               data:
               {
                formData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#student").html(result);
               }
            });
        }   
    }


 //    function getStudentByProgramme(id)
 //    {

 //     $.get("/finance/mainInvoice/getStudentByProgrammeId/"+id, function(data, status){
   
 //        $("#student").html(data);
 //        // $("#view_programme_details").html(data);
 //        // $("#view_programme_details").show();
 //    });
 // }

     function getStudentByStudentId(id)
     {
        $.get("/finance/mainInvoice/getStudentByStudentId/"+id, function(data, status){
       
            $("#view_student_details").html(data);
            $("#view_student_details").show();
        });
     }

     function getApplicantByApplicantId(id)
     {
        $.get("/finance/mainInvoice/getApplicantByApplicantId/"+id, function(data, status){
       
            $("#view_student_details").html(data);
            $("#view_student_details").show();
        });
     }

    function opendialog()
    {
        $("#id_fee_item").val('');
        $("#amount").val('');
        // $("#id").val('0');                    
        $('#myModal').modal('show');

    }


    


    function getTempData(id) {
        $.ajax(
            {
               url: '/finance/mainInvoice/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });
    }

    

    $(document).ready(function() {
        $("#form_main_invoice_details").validate({
            rules: {
                id_fee_item: {
                    required: true
                },
                amount: {
                    required: true
                }
            },
            messages: {
                id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                amount: {
                    required: "<p class='error-text'>Amount Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_main_invoice").validate({
            rules: {
                total_amount: {
                    required: true
                },
                id_program: {
                    required: true
                },
                id_student: {
                    required: true
                },
                date_time: {
                    required: true
                },
                type: {
                    required: true
                },
                id_cohert: {
                    required: true
                },
                currency: {
                    required: true
                },
                id_scholarship: {
                    required: true
                }
            },
            messages: {
                total_amount: {
                    required: "<p class='error-text'>Enter Details For Total Amount</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_student: {
                    required: "<p class='error-text'>Select Student</p>",
                },
                date_time: {
                    required: "<p class='error-text'>Select Date </p>",
                },
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_cohert: {
                    required: "<p class='error-text'>Select Cohert </p>",
                },
                currency: {
                    required: "<p class='error-text'>Select Currency </p>",
                },
                id_scholarship: {
                    required: "<p class='error-text'>Select Scholarship </p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
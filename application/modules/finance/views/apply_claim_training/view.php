<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>View Apply Claim Training/Conferences</h3>
            </div>


    <h4 class='sub-title'>Student Details</h4>

        <div class='data-list'>
            <div class='row'>

                <div class='col-sm-6'>
                    <dl>
                        <dt>Student Name :</dt>
                        <dd><?php echo $studentDetails->full_name ?></dd>
                    </dl>
                    <dl>
                        <dt>Student Email :</dt>
                        <dd><?php echo $studentDetails->email_id ?></dd>
                    </dl>
                    <dl>
                        <dt>Student NRIC :</dt>
                        <dd><?php echo $studentDetails->nric ?></dd>
                    </dl>
                    
                </div>        
                
                <div class='col-sm-6'>
                    <!-- <dl>
                        <dt>Intake :</dt>
                        <dd>
                            <?php echo $studentDetails->intake_name ?>
                        </dd>
                    </dl> -->
                    <dl>
                        <dt>Programme :</dt>
                        <dd><?php echo $studentDetails->programme_name ?></dd>
                    </dl>
                    <dl>
                        <dt>Nationality :</dt>
                        <dd><?php echo $studentDetails->nationality ?></dd>
                    </dl>
                </div>

            </div>
        </div>

    <form id="form_programme" action="" method="post">
        <div class="form-container">
            <h4 class="form-group-title">Apply Claim Details</h4>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" value="<?php echo date('d-m-Y', strtotime($applyClaimTraining->start_date)) ?>" readonly>
                    </div>
                </div>                


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="end_date" name="end_date" value="<?php echo date('d-m-Y', strtotime($applyClaimTraining->end_date)) ?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Research Category <span class='error-text'>*</span></label>
                        <select class="form-control" id="type" name="type" disabled>
                            <option value="">Select</option>
                            <option value="Training"
                            <?php
                            if ('Training' == $applyClaimTraining->type)
                            {
                              echo 'selected';
                            } ?>
                            >Training</option>
                            <option value="Conferences"
                            <?php
                            if ('Conferences' == $applyClaimTraining->type)
                            {
                              echo 'selected';
                            } ?>
                            >Conferences</option>
                            <option value="Miscellaneouss"
                            <?php
                            if ('Miscellaneouss' == $applyClaimTraining->type)
                            {
                              echo 'selected';
                            } ?>
                            >Miscellaneouss</option>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $applyClaimTraining->description ?>" readonly>
                    </div>
                </div>

                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Amount Paid <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="amount" name="amount" value="<?php echo $applyClaimTraining->amount ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Supporting File 1 </label>
                        <input type="file" class="form-control" id="file_one" name="file_one" readonly>
                    </div>
                </div>


            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Supporting File 2</label>
                        <input type="file" class="form-control" id="file_two" name="file_two" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Supporting File 3</label>
                        <input type="file" class="form-control" id="file_three" name="file_three" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="date_time" name="date_time" value="<?php
                        if($applyClaimTraining->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($applyClaimTraining->status == '1')
                        {
                            echo "Approved";
                        }
                        elseif($applyClaimTraining->status == '2')
                        {
                            echo "Rejected";
                        }
                         ?>" readonly="readonly">
                    </div>
                </div>

            </div>

        </div>



    <?php
    if($applyClaimTraining->status != 0)
    {
    ?>



        <div class="form-container">
            <h4 class="form-group-title">Approval Details</h4>



            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Paid Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="paid_date" name="paid_date" value="<?php echo date('d-m-Y', strtotime($applyClaimTraining->paid_date)) ?>" readonly>
                    </div>
                </div>   

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Claim Paid <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="claim_amount" name="claim_amount" value="<?php echo $applyClaimTraining->claim_amount ?>" readonly>
                    </div>
                </div>             


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Reference Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reference_number" name="reference_number" value="<?php echo $applyClaimTraining->reference_number ?>" readonly>
                    </div>
                </div>



             </div>

            <div class="row">

                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Remarks <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reason" name="reason" value="<?php echo $applyClaimTraining->reason ?>" readonly>
                    </div>
                </div>


            </div>

        </div>


    <?php
    }
   ?>
    

        <div class="button-block clearfix">
            <div class="bttn-group">
                <!-- <button type="submit" class="btn btn-primary btn-lg" >Save</button> -->
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>


    </form>
    

         
        

    <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
    </footer>

    </div>
</div>

<script>

    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                type: {
                    required: true
                },
                description: {
                    required: true
                },
                amount: {
                    required: true
                },
                id_research_category: {
                    required: true
                }
            },
            messages: {
                start_date: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Select End Date</p>",
                },
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                amount: {
                    required: "<p class='error-text'>Amount Required</p>",
                },
                id_research_category: {
                    required: "<p class='error-text'>Select Research Category</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $('select').select2();


    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "2019:2020"
    });
  } );
</script>
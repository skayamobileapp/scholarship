<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Fee Setup</h3>
        </div>
        <form id="form_fee_setup" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Fee Setup Details</h4>  
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $feeSetup->name;?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name Optional Language </label>
                            <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $feeSetup->code;?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="code" name="code" value="<?php echo $feeSetup->code;?>">
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fee Category <span class='error-text'>*</span></label>
                            <select name="id_fee_category" id="id_fee_category" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($feeCategoryList))
                                {
                                    foreach ($feeCategoryList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $feeSetup->id_fee_category)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->code. " - " .$record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount Calculation Tytpe <span class='error-text'>*</span></label>
                            <select name="id_amount_calculation_type" id="id_amount_calculation_type" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($amountCalculationTypeList))
                                {
                                    foreach ($amountCalculationTypeList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $feeSetup->id_amount_calculation_type)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->code. " - " .$record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Frequency Mode <span class='error-text'>*</span></label>
                            <select name="id_frequency_mode" id="id_frequency_mode" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($frequencyModeList))
                                {
                                    foreach ($frequencyModeList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php 
                                            if($record->id == $feeSetup->id_frequency_mode)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->code. " - " .$record->name;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Account Code <span class='error-text'>*</span></label>
                            <select name="account_code" id="account_code" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($accountCodeList))
                                {
                                    foreach ($accountCodeList as $record)
                                    {?>
                                        <option value="<?php echo $record->code;  ?>"
                                            <?php 
                                            if($record->code == $feeSetup->account_code)
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                            <?php echo $record->code. " - " .$record->type;  ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>GST Tax <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="gst_tax" name="gst_tax" value="<?php echo $feeSetup->gst_tax;?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Effective Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="effective_date" name="effective_date" value="<?php echo date('d-m-Y', strtotime($feeSetup->effective_date));?>" autocomplete="off">
                        </div>
                    </div>

                </div>


                <div class="row">


                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Recoverable <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="is_non_invoice" id="is_non_invoice" value="1" <?php if($feeSetup->is_non_invoice=='1') {
                                    echo "checked=checked";
                                };?>><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="is_non_invoice" id="is_non_invoice" value="0" <?php if($feeSetup->is_non_invoice=='0') {
                                    echo "checked=checked";
                                };?>>
                                <span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                    </div>




                   <!--  <div class="col-sm-4" id='recoverablePeriodDiv'>
                        <div class="form-group">
                            <label>Recoverable Period <span class='error-text'>*</span></label>
                            <select name="recoverable_period" id="recoverable_period" class="form-control">
                                <option value="">Select</option>
                                <option value="Monthly" <?php 
                                            if($feeSetup->recoverable_period == 'Monthly')
                                            {
                                                echo "selected=selected";
                                            } ?>>Monthly</option>
                                <option value="Quaterly" <?php 
                                            if($feeSetup->recoverable_period == 'Quaterly')
                                            {
                                                echo "selected=selected";
                                            } ?>>Quaterly</option>
                                
                            </select>
                        </div>
                    </div> -->

                      <div class="col-sm-4" id='recoverableDayDiv'>
                        <div class="form-group">
                            <label>Recoverable Day <span class='error-text'>*</span></label>
                            <select name="recoverable_day" id="recoverable_day" class="form-control">
                                <option value="">Select</option>
                                <?php for($k=1;$k<=30;$k++) {?>
                                <option value="<?php echo $k;?>"
                                    <?php 
                                            if($feeSetup->recoverable_day == $k)
                                            {
                                                echo "selected=selected";
                                            } ?>

                                            ><?php echo $k;?></option>

                                <?php }?>
                            </select>
                        </div>
                    </div>






                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" <?php if($feeSetup->status=='1') {
                                    echo "checked=checked";
                                };?>><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0" <?php if($feeSetup->status=='0') {
                                    echo "checked=checked";
                                };?>>
                                <span class="check-radio"></span> In-Active
                                </label>                              
                            </div>                         
                    </div>
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>

        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<script>


    function  recoverableDiv (id) {
      $("#recoverableDayDiv").show();
      $("#recoverablePeriodDiv").show();

      if(id=='0') {
              $("#recoverableDayDiv").hide();
      $("#recoverablePeriodDiv").hide();

      }
    }


     $(document).ready(function() {
        $("#form_fee_setup").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                id_fee_category: {
                    required: true
                },
                id_amount_calculation_type: {
                    required: true
                },
                id_frequency_mode:{
                    required: true
                },
                account_code: {
                    required: true
                },
                effective_date: {
                    required: true
                },
                gst_tax: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Fee Code Required</p>",
                },
                id_fee_category: {
                    required: "<p class='error-text'>Select Fee Category</p>",
                },
                id_amount_calculation_type: {   
                    required: "<p class='error-text'>Select Amount Calculation Type</p>",
                },
                id_frequency_mode: {
                    required: "<p class='error-text'>Select Frequency Mode</p>",
                },
                account_code: {
                    required: "<p class='error-text'>Select Account Code</p>",
                },
                effective_date: {
                    required: "<p class='error-text'>Effective Date Required</p>",
                },
                gst_tax: {
                    required: "<p class='error-text'>Enter GST Tax</p>",
                },
                status: {
                    required: "<p class='error-text'>Status required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
 <script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>
<script type="text/javascript">
    $('select').select2();
</script>

<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Statement Of Accounts</h3>
            <a href="list" class="btn btn-link btn-back">‹ Back</a>
        </div>    
        <form id="form_main_invoice" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>   
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd>Azwan Ahmad</dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd>200502025681</dd>
                            </dl>
                           
                             <dl>
                                <dt>Scholarship :</dt>
                                <dd>Peneraju Skil Iltizam </dd>
                            </dl> 
                            <dl>
                                <dt>Program  :</dt>
                                <dd>Peneraju Skil Iltizam Fabrikasi Logam</dd>
                            </dl>                            
                        </div>        
                        
                        
                    </div>
                </div>
            </div>






            <!-- <div style="border: 1px solid;border-radius: 12px;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <h4>Student Name : <?php echo ucwords($studentDetails->full_name); ?></h4>
                        <h4>Student ID : <?php echo $studentDetails->id ?></h4>
                        <h4>Email Id : <?php echo $studentDetails->email_id ?></h4>
                        <h4>NRIC : <?php echo $studentDetails->nric ?></h4>
                    </div>
                    <div class="col-sm-4">
                        <h4>Intake : <?php echo $studentDetails->intake_name ?></h4>
                        <h4>Student Category : </h4>
                        <h4>Program : <?php echo $studentDetails->programme_name ?></h4>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="form-container">
            <h4 class="form-group-title">Account Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#statement_of_accounts" class="nav-link border rounded text-center"
                            aria-controls="statement_of_accounts" aria-selected="true"
                            role="tab" data-toggle="tab">Account Statement</a>
                    </li>
                    <li role="presentation"><a href="#statement_of_accounts" class="nav-link border rounded text-center"
                            aria-controls="statement_of_accounts" role="tab" data-toggle="tab">Account Statement(By Group)</a>
                    </li>
                    <li role="presentation"><a href="#tab_1" class="nav-link border rounded text-center"
                            aria-controls="tab_1" role="tab" data-toggle="tab">Invoice</a>
                    </li>
                    <li role="presentation"><a href="#tab_2" class="nav-link border rounded text-center"
                            aria-controls="tab_2" role="tab" data-toggle="tab">Receipt</a>
                    </li>
                    <li role="presentation"><a href="#tab_5" class="nav-link border rounded text-center"
                            aria-controls="tab_5" role="tab" data-toggle="tab">Advance Payment</a>
                    </li>
                    <li role="presentation"><a href="#tab_3" class="nav-link border rounded text-center"
                            aria-controls="tab_3" role="tab" data-toggle="tab">Credit Note</a>
                    </li>
                    <li role="presentation"><a href="#tab_4" class="nav-link border rounded text-center"
                            aria-controls="tab_4" role="tab" data-toggle="tab">Debit Note</a>
                    </li>
                    <li role="presentation"><a href="#tab_5" class="nav-link border rounded text-center"
                            aria-controls="tab_5" role="tab" data-toggle="tab">Discount Note</a>
                    </li>
                    <li role="presentation"><a href="#tab_5" class="nav-link border rounded text-center"
                            aria-controls="tab_5" role="tab" data-toggle="tab">Refund</a>
                    </li>
                    <li role="presentation"><a href="#tab_6" class="nav-link border rounded text-center"
                            aria-controls="tab_6" role="tab" data-toggle="tab">Reprint Receipt</a>
                    </li>
                </ul>


                <div class="tab-content offers-tab-content">



                    <div role="tabpanel" class="tab-pane active" id="statement_of_accounts">
                        <div class="mt-4">

                            <div class="custom-table" id="printInvoice">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Sl. No</th>
                                        <th>Date</th>
                                        <th>Fee Item</th>
                                        <th>Frequency</th>
                                        <th>Amount</th>
                                        <th>Funding Percentage</th>
                                        <th>Paid to </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                         <tr>
                                            <td>1</td>
                                            <td>25 Dec 2019</td>
                                            <td>Living Allowance</td>
                                            <td>Monthly</td>
                                            <td>500</td>
                                            <td>100%</td>
                                            <td>Credited Scholar Bank</td>
                                          </tr>
                                          <tr>
                                            <td>2</td>
                                            <td>25 Dec 2019</td>
                                            <td>Book Allowance</td>
                                            <td>Monthly</td>
                                            <td>500</td>
                                            <td>100%</td>
                                            <td>Credited Scholar Bank</td>
                                          </tr>

                                          <tr>
                                            <td>3</td>
                                            <td>25 Dec 2019</td>
                                            <td>Accommodation</td>
                                            <td>Monthly</td>
                                            <td>500</td>
                                            <td>100%</td>
                                            <td>Credited Scholar Bank</td>
                                          </tr>

                                          <tr>
                                            <td>4</td>
                                            <td>25 Dec 2019</td>
                                            <td>Meal Allowance</td>
                                            <td>Monthly</td>
                                            <td>600</td>
                                            <td>100%</td>
                                            <td>Credited Scholar Bank</td>
                                          </tr>
                                          <tr>
                                            <td>5</td>
                                            <td>25 Dec 2019</td>
                                            <td>Transport</td>
                                            <td>Monthly</td>
                                            <td>80</td>
                                            <td>100%</td>
                                            <td>Credited Scholar Bank</td>
                                          </tr>
                                          <tr>
                                            <td>6</td>
                                            <td>25 Dec 2019</td>
                                            <td>Insurance</td>
                                            <td>Once</td>
                                            <td>1200</td>
                                            <td>100%</td>
                                            <td>-</td>
                                          </tr>
                                          <tr>
                                            <td>7</td>
                                            <td>25 Dec 2019</td>
                                            <td>Living Allowance</td>
                                            <td>Monthly</td>
                                            <td>500</td>
                                            <td>100%</td>
                                            <td>-</td>
                                          </tr>
                                          
                                    </tbody>
                                    <!-- <tbody>
                                    <?php
                                    if (!empty($getStatementOfAccountByStudentId)) {
                                        $i=1;
                                        foreach ($getStatementOfAccountByStudentId as $record) {
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)); ?></td>
                                            <td><?php echo $record->transaction_type ?></td>
                                            <td><?php echo $record->reference_number ?></td>
                                            <td><?php echo $record->type ?></td>
                                            <td><?php echo $record->remarks ?></td>
                                            <td><?php echo $record->invoice_total ?></td>
                                            <td><?php echo $record->total_payable ?></td>
                                            <td><?php echo $record->paid_amount ?></td>
                                            <td><?php echo $record->balance_amount ?></td>
                                            <td class="">
                                            <a href="#" title="" onclick="printDiv('printInvoice')">Print</a>
                                            </td>
                                        </tr>
                                    <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                                    </tbody> -->
                                </table>
                            </div>


                        </div> 
                    </div>



                    <div role="tabpanel" class="tab-pane" id="receipt">
                        <div class="mt-4">
                            <div class="custom-table" id="printReceipt">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Receipt Number</th>
                                        <th>Receipt Amount</th>
                                        <th>Remarks</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($getReceiptByStudentId)) {
                                        foreach ($getReceiptByStudentId as $record) {
                                    ?>
                                        <tr>
                                            <td><?php echo $record->receipt_number ?></td>
                                            <td><?php if($record->receipt_amount == ""){
                                                echo "0.00";
                                            } else { echo $record->receipt_amount; } ?></td>
                                            <td><?php echo $record->remarks ?></td>
                                            <td class="">
                                            <a href="#" title="" onclick="printDiv('printReceipt')">Print</a>
                                            </td>
                                        </tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>



                    <div role="tabpanel" class="tab-pane" id="tab_1">
                        <div class="mt-4">


                            <div class="custom-table" id="printInvoice">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Sl. No</th>
                                        <th>Invoice Number</th>
                                        <th>Invoice Type</th>
                                        <th>Total</th>
                                        <th>Balance </th>
                                        <th>Remarks</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($getInvoiceByStudentId)) {
                                        $i=1;
                                        foreach ($getInvoiceByStudentId as $record) {
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $record->invoice_number ?></td>
                                            <td><?php echo $record->type ?></td>
                                            <td><?php echo $record->total_amount ?></td>
                                            <td><?php if($record->balance_amount == ""){
                                                echo "0.00";
                                            } else { echo $record->balance_amount; } ?></td>
                                            <td><?php echo $record->remarks ?></td>
                                            <td class="">
                                            <a href="#" title="" onclick="printDiv('printInvoice')">Print</a>
                                            </td>
                                        </tr>
                                    <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>



                        </div> 
                    </div>












                    <div role="tabpanel" class="tab-pane" id="tab_2">
                        <div class="mt-4">
                            <div class="custom-table" id="printReceipt">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Sl. No</th>
                                        <th>Receipt Number</th>
                                        <th>Type</th>
                                        <th>Receipt Amount</th>
                                        <th>Remarks</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($getReceiptByStudentId)) {
                                        $i=1;
                                        foreach ($getReceiptByStudentId as $record) {
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $record->receipt_number ?></td>
                                            <td><?php echo $record->type ?></td>
                                            <td><?php if($record->receipt_amount == ""){
                                                echo "0.00";
                                            } else { echo $record->receipt_amount; } ?></td>
                                            <td><?php echo $record->remarks ?></td>
                                            <td class="">
                                            <a href="#" title="" onclick="printDiv('printReceipt')">Print</a>
                                            </td>
                                        </tr>
                                    <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>









                    <div role="tabpanel" class="tab-pane" id="tab_3">
                        <div class="mt-4">


                                <div class="custom-table" id="printReceipt">
                                    <table class="table" id="list-table">
                                        <thead>
                                        <tr>
                                            <th>Sl. No</th>
                                            <th>Credit Note Number</th>
                                            <th>Credit Type</th>
                                            <th>Type</th>
                                            <th>Invoice</th>
                                            <th>Credit Note Date</th>
                                            <th>Credit Note Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if (!empty($creditNoteByStudentId)) {
                                            $i=1;
                                            foreach ($creditNoteByStudentId as $record) {
                                        ?>
                                            <tr>
                                                <td><?php echo $i ?></td>
                                                <td><?php echo $record->reference_number ?></td>
                                                <td><?php echo $record->credit_code . " - " . $record->credit_name ?></td>
                                                <td><?php echo $record->type ?></td>
                                                <td><?php echo $record->invoice_number ?></td>
                                                <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)) ?></td>                                                <td><?php echo $record->amount ?></td>
                                            </tr>
                                        <?php
                                        $i++;
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>



                        </div>
                    </div>











                    <div role="tabpanel" class="tab-pane" id="tab_4">
                        <div class="mt-4">


                            <div class="custom-table" id="printReceipt">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Sl. No</th>
                                        <th>Debit Note Number</th>
                                        <th>Debit Type</th>
                                        <th>Type</th>
                                        <th>Invoice</th>
                                        <th>Debit Note Date</th>
                                        <th>Debit Note Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($debitNoteByStudentId)) {
                                        $i=1;
                                        foreach ($debitNoteByStudentId as $record) {
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $record->reference_number ?></td>
                                            <td><?php echo $record->debit_code . " - " . $record->debit_name ?></td>
                                            <td><?php echo $record->type ?></td>
                                            <td><?php echo $record->invoice_number ?></td>
                                            <td><?php echo date('d-m-Y', strtotime($record->created_dt_tm)) ?></td>                                                <td><?php echo $record->amount ?></td>
                                        </tr>
                                    <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>




                        </div>
                    </div>







                    <div role="tabpanel" class="tab-pane" id="tab_5">
                        <div class="mt-4">


                            <div class="custom-table" id="printReceipt">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Coming Soon... !</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>




                        </div>
                    </div>










                    <div role="tabpanel" class="tab-pane" id="tab_6">
                        <div class="mt-4">
                            <div class="custom-table" id="printReceipt">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Sl. No</th>
                                        <th>Receipt Number</th>
                                        <th>Type</th>
                                        <th>Receipt Amount</th>
                                        <th>Remarks</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($getReceiptByStudentId)) {
                                        $i=1;
                                        foreach ($getReceiptByStudentId as $record) {
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $record->receipt_number ?></td>
                                            <td><?php echo $record->type ?></td>
                                            <td><?php if($record->receipt_amount == ""){
                                                echo "0.00";
                                            } else { echo $record->receipt_amount; } ?></td>
                                            <td><?php echo $record->remarks ?></td>
                                            <td class="">
                                            <a href="#" title="" onclick="printDiv('printReceipt')">Print</a>
                                            </td>
                                        </tr>
                                    <?php
                                    $i++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

        </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>


</form>
<script type="text/javascript">
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}
</script>
<script>

    function getStudentByProgramme(id)
    {

     $.get("/finance/mainInvoice/getStudentByProgrammeId/"+id, function(data, status){
   
        $("#student").html(data);
        // $("#view_programme_details").html(data);
        // $("#view_programme_details").show();
    });
 }

 function getStudentByStudentId(id)
 {

     $.get("/finance/mainInvoice/getStudentByStudentId/"+id, function(data, status){
   
        $("#view_student_details").html(data);
        $("#view_student_details").show();
    });
 }



    function opendialog()
    {
        $("#id_fee_item").val('');
        $("#amount").val('');
        $("#id").val('0');                    
        $('#myModal').modal('show');

    }
    function saveData() {


        var tempPR = {};
        tempPR['id_fee_item'] = $("#id_fee_item").val();
        tempPR['amount'] = $("#amount").val();
        tempPR['id'] = $("#id").val();
            $.ajax(
            {
               url: '/finance/mainInvoice/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                var ta = $("#inv-total-amount").val();
                $("#total_amount").val(ta);
                $('#myModal').modal('hide');
               }
            });
        
    }

    function deleteTempData(id) {
         $.ajax(
            {
               url: '/finance/mainInvoice/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
               }
            });
    }


    function getTempData(id) {
        $.ajax(
            {
               url: '/finance/mainInvoice/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });

    }


    $(document).ready(function() {
        $("#form_main_invoice").validate({
            rules: {
                total_amount: {
                    required: true
                },
                id_student: {
                    required: true
                },
                date_time: {
                    required: true
                }
            },
            messages: {
                total_amount: {
                    required: "Enter Total Amount",
                },
                id_student: {
                    required: "Select Student",
                },
                date_time: {
                    required: "Select Date ",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
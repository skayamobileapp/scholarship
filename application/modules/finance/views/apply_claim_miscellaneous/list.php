<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Apply Claim Miscellaneous</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Apply Claim</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">
                <div class="row">

                  <div class="col-sm-6"> 
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Student Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>


                  <div class="col-sm-6"> 
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Student NRIC</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="nric" value="<?php echo $searchParam['nric']; ?>">
                      </div>
                    </div>
                  </div>


                </div>


                <div class="row">

                  
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Program</label>
                      <div class="col-sm-8">
                        <select name="id_program" id="id_program" class="form-control selitemIcon">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                              <?php
                              if($record->id ==$searchParam['id_program'])
                              {
                                echo 'selected';
                              }
                              ?>
                              >
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                  </div>
                
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Type</label>
                      <div class="col-sm-8">
                        <select name="type" id="type" class="form-control">                          
                          <option value="<?php echo 'Miscellaneouss';  ?>"
                            <?php
                            if ('Miscellaneouss' == $searchParam['type'])
                            {
                              echo 'selected';
                            } ?>
                            >
                            <?php echo 'Miscellaneouss';  ?>
                          </option>
                        </select>
                      </div>
                    </div>
                  </div>


              </div>



                <div class="row">


                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Status</label>
                      <div class="col-sm-8">
                        <select name="status" id="status" class="form-control">
                          <option value="">Select</option>
                          <option value="<?php echo '1';  ?>"
                            <?php
                            if ('1' == $searchParam['status'])
                            {
                              echo 'selected';
                            } ?>
                            >
                            <?php echo 'Approved';  ?>
                          </option>

                          <option value="<?php echo '0';  ?>"
                            <?php
                            if ('0' == $searchParam['status'])
                            {
                              echo 'selected';
                            } ?>
                            >
                            <?php echo 'Pending';  ?>
                          </option>


                          <option value="<?php echo '2';  ?>"
                            <?php
                            if ('2' == $searchParam['status'])
                            {
                              echo 'selected';
                            } ?>
                            >
                            <?php echo 'Rejected';  ?>
                          </option>

                        </select>
                      </div>
                    </div>
                  </div>
                
                </div>

              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href='list' class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    </div>

    <div class="custom-table">
      <table class="table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Student</th>
            <th>Program</th>
            <th>Type</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Amount Paid</th>
            <th style="text-align: center;">Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($applyClaimMiscellaneousList)) {
            $i=1;
            foreach ($applyClaimMiscellaneousList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->nric . " - " . $record->student_name ?></td>
                <td><?php echo $record->program_code . " - " . $record->program_name ?></td>
                <td><?php echo $record->type ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->start_date)) ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->end_date)) ?></td>
                <td><?php echo $record->amount ?></td>
                <td style="text-align: center;"><?php
                 if($record->status == 0)
                  {
                      echo 'Pending';
                  }elseif($record->status == 1)
                  {
                      echo 'Approved';
                  }if($record->status == 2)
                  {
                      echo 'Rejected';
                  }
                 ?>
                </td>
                <td style="text-align: center;">
                  <?php
                 if($record->status == 0)
                  {
                    ?>
                  <a href="<?php echo 'edit/' . $record->id; ?>">Approve</a>

                  <?php 
                  }else
                  {
                    ?>
                  <a href="<?php echo 'view/' . $record->id; ?>">View</a>

                    <?php
                  }
                ?>
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script>
    $('select').select2();
</script>
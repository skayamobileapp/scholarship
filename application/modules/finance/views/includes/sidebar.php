            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $scholar_name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <!-- <li><a href="/sponser/user/profile">Edit Profile</a></li> -->
                            <li><a href="/scholarship/role/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    <h4>Finance Setup</h4>
                    <ul>
                        <li><a href="/finance/currency/list">Currency Setup</a></li>
                        <li><a href="/finance/currencyRateSetup/list">Currency Rate Setup</a></li>
                        <li><a href="/finance/accountCode/list">Account Code</a></li>
                        <li><a href="/finance/bankRegistration/list">Bank Registration</a></li>
                        <li><a href="/finance/amountCalculationType/list">Amount Calculation Type</a></li>
                        <li><a href="/finance/frequencyMode/list">Frequency Mode</a></li>
                        <li><a href="/finance/paymentType/list">Payment Type</a></li>
                        <li><a href="/finance/feeCategory/list">Fee Category</a></li>
                        <li><a href="/finance/feeSetup/list">Fee Setup</a></li>
                        <li><a href="/finance/feeStructure/list">Financial Affidavit</a></li>
                    </ul>
                    <h4>Claims</h4>
                    <ul>
                        <li><a href="/finance/applyClaimTraining/list">Training / Conference Claims</a></li>
                        <li><a href="/finance/applyClaimMiscellaneous/list">Miscellaneous Claims</a></li>
                    </ul>
                    <h4>Repayment Schedule</h4>
                    <ul>
                        <li><a href="/finance/repayment/list">Repayment Schedule</a></li>
                        <!--<li><a href="/finance/repayment/extensionList">Repayment Extension</a></li>-->
                    </ul>
                    <h4>Invoice</h4>
                    <ul>
                        <li><a href="/finance/mainInvoice/list">Invoice Generation</a></li>
                        <!--<li><a href="/finance/repayment/extensionList">Repayment Extension</a></li>-->
                    </ul>
                    <h4>Statement of Account</h4>
                    <ul>
                        <li><a href="/finance/StatementOfAccount/list">Statement of Account</a></li>
                        <!--<li><a href="/finance/repayment/extensionList">Repayment Extension</a></li>-->
                    </ul>
                </div>
            </div>
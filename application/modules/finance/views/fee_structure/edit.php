<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Fee Structure</h3>
        </div>

        
        <form id="form_payment_type" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Fee Structure Details</h4>  


                <div class="row">

                    <div class="col-sm-4">
                      <div class="form-group">
                          <label>Cohort <span class='error-text'>*</span></label>
                          <select name="id_cohert" id="id_cohert" class="form-control selitemIcon" onchange="getScholarshipByIdCohort(this.value)">
                              <option value="">Select</option>
                              <?php
                              if (!empty($schemeList))
                              {
                                  foreach ($schemeList as $record)
                                  {?>
                                      <option value="<?php echo $record->id;?>"
                                        <?php if($record->id == $feeStructure->id_cohert)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                      ><?php echo $record->code . " - " . $record->name; ?>
                                      </option>
                                  <?php
                                  }
                              }
                              ?>

                          </select>
                      </div>
                    </div>


                    <div class="col-sm-4">
                          <div class="form-group">
                              <label>Scholarship <span class='error-text'>*</span></label>
                              
                              <span id="view_scholarship">
                                <select class="form-control" id='id_scholarship' name='id_scholarship'>
                                    <option value=''></option>
                                  </select>
                             </span>

                          </div>
                    </div>


                    <div class="col-sm-4">
                          <div class="form-group">
                              <label>Program <span class='error-text'>*</span></label>
                              
                              <span id="view_program">
                                <select class="form-control" id='id_program' name='id_program'>
                                    <option value=''></option>
                                  </select>
                             </span>

                          </div>
                    </div>


                </div>


                <div class="row">



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Currency <span class='error-text'>*</span></label>
                            <select name="currency" id="currency" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($currencyList))
                                {
                                    foreach ($currencyList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        <?php if($record->id == $feeStructure->currency)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                        ><?php echo $record->code . " - " . $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

<!-- 

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $repayment->amount; ?>" readonly>
                        </div>
                    </div>
                     -->


                     <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" <?php if($feeStructure->status=='1') {
                                    echo "checked=checked";
                                };?>><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0" <?php if($feeStructure->status=='0') {
                                    echo "checked=checked";
                                };?>>
                                <span class="check-radio"></span> In-Active
                                </label>                              
                            </div>                         
                    </div>



                </div>

            </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>


   
  $('select').select2();


  $( function()
    {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1920:2019"
    });
    });


    function getScholarshipByIdCohort(id)
    {
      $.get("/finance/repayment/getScholarshipByIdCohort/"+id, function(data, status)
      {
          $("#view_scholarship").html(data);
          $("#view_scholarship").show();
      });

    }




    function getProgramByScholarshipNCohortId()
    {
        var tempPR = {};
        // tempPR['id_program'] = idprogram;
        tempPR['id_cohert'] = $("#id_cohert").val();
        tempPR['id_scholarship'] = $("#id_scholarship").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_cohert'] != '' && tempPR['id_scholarship'] != '')
        {

            $.ajax(
            {
               url: '/finance/repayment/getProgramByScholarshipNCohortId',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  $("#view_program").html(result);
                  $("#view_program").show();
               }
            });
        }
    }



    $(document).ready(function()
    {
        var id_cohert = <?php echo $feeStructure->id_cohert; ?>;
        var id_scholarship = <?php echo $feeStructure->id_scholarship; ?>;
        var id_program = <?php echo $feeStructure->id_program; ?>;



        if(id_scholarship != '')
        {
            $.get("/finance/feeStructure/getScholarshipByIdCohort/"+id_cohert, function(data, status)
            {
                // alert(data);

                $("#view_scholarship").html(data);
                $("#view_scholarship").show();

                $("#id_scholarship").find('option[value="'+id_scholarship+'"]').attr('selected',true);
                $('select').select2();
            });
        }


        if(id_scholarship != '' && id_cohert != '')
        {
            var tempPR = {};
            tempPR['id_cohert'] = id_cohert;
            tempPR['id_scholarship'] = id_scholarship;
            // alert(tempPR['id_program']);

            if(tempPR['id_cohert'] != '' && tempPR['id_scholarship'] != '')
            {

                $.ajax(
                {
                   url: '/finance/feeStructure/getProgramByScholarshipNCohortId',
                    type: 'POST',
                   data:
                   {
                    tempData: tempPR
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                        $("#view_program").html(result);
                        $("#view_program").show();

                        $("#id_program").find('option[value="'+id_program+'"]').attr('selected',true);
                        $('select').select2();
                   }
                });
            }        
        }





        $("#form_payment_type").validate(
        {
            rules:
            {
                id_cohert:
                {
                    required: true
                },
                id_scholarship:
                {
                    required: true
                },
                id_program:
                {
                    required: true
                },
                currency:
                {
                    required: true
                },
                currency:
                {
                    required: true
                },
                payment_type:
                {
                    required: true
                },
                day:
                {
                    required: true
                },
                installment_amount:
                {
                    required: true
                },
                start_date:
                {
                    required: true
                },
                end_date:
                {
                    required: true
                }
            },
            messages:
            {
                id_cohert:
                {
                    required: "<p class='error-text'>Select Cohort</p>",
                },
                id_scholarship:
                {
                    required: "<p class='error-text'>Select Scholarhip</p>",
                },
                id_program:
                {
                    required: "<p class='error-text'>Select Program</p>",
                },
                currency:
                {
                    required: "<p class='error-text'>Select Currency</p>",
                },
                currency:
                {
                    required: "<p class='error-text'>Select Currency</p>",
                },
                payment_type:
                {
                    required: "<p class='error-text'>Select Repayment Type</p>",
                },
                day:
                {
                    required: "<p class='error-text'>Select Monthly Payment Day</p>",
                },
                installment_amount:
                {
                    required: "<p class='error-text'>Quarterly Installment Amount Required</p>",
                },
                start_date:
                {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date:
                {
                    required: "<p class='error-text'>Select End Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element)
            {
                error.appendTo(element.parent());
            }

        });
    });

</script>
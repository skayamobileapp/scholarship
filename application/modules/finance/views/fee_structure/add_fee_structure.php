<?php $this->load->helper("form"); ?>

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
                <h3>Add Fee Financial Affidavit </h3>
            <a href="<?php echo '../list'; ?>" class="btn btn-link"> < Back</a>
        </div>


          <div class="form-container">
                <h4 class="form-group-title">Sub Thrust Details</h4> 

            <div class="row">

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Thrust Name</label>
                          <input type="text" class="form-control" id="thrust_name" name="thrust_name" value="<?php echo $subThrust->thrust_code . " - " . $subThrust->thrust_name; ?>" readonly="readonly">
                      </div>
                  </div>    

                  

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Scholarship Name</label>
                          <input type="text" class="form-control" id="scholarship_name" name="scholarship_name" value="<?php echo $subThrust->scholarship_name; ?>" readonly="readonly">
                      </div>
                  </div>     


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Scholarship Short Name</label>
                          <input type="text" class="form-control" id="scholarship_short_name" name="scholarship_short_name" value="<?php echo $subThrust->scholarship_short_name; ?>" readonly="readonly">
                      </div>
                  </div>


                </div>

                <div class="row">


                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Scholarship Code</label>
                          <input type="text" class="form-control" id="scholarship_code" name="scholarship_code" value="<?php echo $subThrust->scholarship_code; ?>" readonly="readonly">
                      </div>
                  </div>

            </div>

          </div>














        <br>


        <div class="form-container">
            <h4 class="form-group-title">Fee Structure Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Fee structure Details</a>
                    </li>    
                  <!--   <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                            aria-controls="tab_two" role="tab" data-toggle="tab">International Students</a>
                    </li>   -->
                    <!-- <li role="presentation"><a href="#tab_three" class="nav-link border rounded text-center"
                            aria-controls="tab_three" role="tab" data-toggle="tab">Partner University</a>
                    </li>    -->             
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">








                        <form id="form_one" action="" method="post">

                          <div class="form-container">
                                  <h4 class="form-group-title">Fee Structure (Malaysian) Details</h4> 

                              <div class="row">

                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Fee Item <span class='error-text'>*</span></label>
                                              <select name="one_id_fee_item" id="one_id_fee_item" class="form-control">
                                                  <option value="">Select</option>
                                                  <?php
                                                  if (!empty($feeSetupList))
                                                  {
                                                      foreach ($feeSetupList as $record)
                                                      {?>
                                                          <option value="<?php echo $record->id;?>"
                                                          ><?php echo $record->name.'  -'.$record->frequency_mode;?>
                                                          </option>
                                                  <?php
                                                      }
                                                  }
                                                  ?>
                                              </select>
                                          </div>
                                      </div>



                                      <div class="col-sm-2">
                                          <div class="form-group">
                                              <label>Amount (MYR) <span class='error-text'>*</span></label>
                                              <input type="number" class="form-control" id="one_amount" name="one_amount">
                                          </div>
                                      </div>

                                      <div class="col-sm-2">
                                          <div class="form-group">
                                              <label>Funding Percentage (%)<span class='error-text'>*</span></label>
                                               <input type="text" class="form-control" id="funding_percentage" name="funding_percentage">
                                          </div>
                                      </div>
                                       <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Recoverable Amount (MYR) <span class='error-text'>*</span></label>
                                               <input type="text" class="form-control" id="recoverable_amount" name="recoverable_amount">
                                          </div>
                                      </div>

                                      <div class="col-sm-2">
                                          <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                                      </div>


                                  </div>


                                  <div class="row">
                                
                                      

                                  </div>

                            </div>


                        </form>



                        <div class="form-container">
                            <h4 class="form-group-title">Fee Structure List</h4> 


                        

                        <div class="custom-table">
                          <table class="table" id="list-table">
                            <thead>
                              <tr>
                                <th>Sl. No</th>
                                <th>Fee Item</th>
                                <th>Frequency Mode</th>
                                <th>Amount Calculation</th>
                                <th>Funding Percentage (%)</th>
                                <th>Recoverable Amount(MYR)</th>
                                <th>Amount</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              if (!empty($feeStructureLocalList))
                              {
                                $i = 1;
                                $total_amount = 0;
                                foreach ($feeStructureLocalList as $record)
                                {
                              ?>
                                  <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->fee_structure ?></td>
                                    <td><?php echo $record->frequency_code . " - " . $record->frequency_mode ?></td>
                                    <td><?php echo $record->amount_calculatoin_code ?></td>
                                     <td><?php echo $record->funding_percentage ?></td>
                                      <td><?php echo $record->recoverable_amount ?></td>
                                    <td><?php echo $record->amount ?></td>
                                    <!-- <td class="text-center"> -->
                                      <td>
                                    <a onclick="deleteFeeStructure(<?php echo $record->id; ?>)" title="Delete">Delete</a>
                                    </td>
                                  </tr>
                              <?php
                              $total_amount = $total_amount + $record->amount;
                              $i++;
                                }
                                 $total_amount = number_format($total_amount, 2, '.', ',');
                                ?>

                                <tr >
                                    <td bgcolor="" colspan="3"></td>
                                    <td bgcolor="" style="text-align: center;"><b>Total Amount :</b></td>
                                    <td bgcolor=""><b><?php echo $total_amount ?></b></td>
                                    <!-- <td class="text-center"> -->
                                    <td bgcolor=""></td>
                                  </tr>
                                <?php
                              }
                              ?>
                            </tbody>
                          </table>
                        </div>

                      </div>


                        </div> 
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="col-12">




                        <form id="form_two" action="" method="post">


                            <div class="form-container">
                                  <h4 class="form-group-title">Fee Structure (International) Details</h4> 

                              <div class="row">

                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Fee Item <span class='error-text'>*</span></label>
                                              <select name="two_id_fee_item" id="two_id_fee_item" class="form-control">
                                                  <option value="">Select</option>
                                                  <?php
                                                  if (!empty($feeSetupList))
                                                  {
                                                      foreach ($feeSetupList as $record)
                                                      {?>
                                                          <option value="<?php echo $record->id;?>"
                                                          ><?php echo $record->name;?>
                                                          </option>
                                                  <?php
                                                      }
                                                  }
                                                  ?>
                                              </select>
                                          </div>
                                      </div>

                                      <div class="col-sm-2">
                                          <div class="form-group">
                                              <label>Amount (USD) <span class='error-text'>*</span></label>
                                              <input type="number" class="form-control" id="two_amount" name="two_amount">
                                          </div>
                                      </div>


                                         <div class="col-sm-2">
                                          <div class="form-group">
                                              <label>Funding Percentage <span class='error-text'>*</span></label>
                                               <input type="text" class="form-control" id="two_funding_percentage" name="two_funding_percentage">
                                          </div>
                                      </div>
                                       <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Recoverable Amount (USD) <span class='error-text'>*</span></label>
                                               <input type="text" class="form-control" id="two_recoverable_amount" name="two_recoverable_amount">
                                          </div>
                                      </div>


                                     
                                
                                      <div class="col-sm-2">
                                          <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveDataUSD()">Add</button>
                                      </div>
                                  </div>

                            </div>

                           


                        </form>


                      

                        <div class="form-container">
                              <h4 class="form-group-title">Fee Structure List</h4> 


                          

                          <div class="custom-table">
                            <table class="table" id="list-table">
                              <thead>
                                <tr>
                                  <th>Sl. No</th>
                                  <th>Fee Item</th>
                                  <th>Frequency Mode</th>
                                  <th>Amount Calculation</th>
                                  <th>Funding Percentage(%)</th>
                                  <th>Recoverable Amount(USD)</th>
                                  <th>Amount</th>
                                  <th>Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                if (!empty($feeStructureInternationalList))
                                {
                                  $i = 1;
                                  $total_amount = 0;
                                  foreach ($feeStructureInternationalList as $record)
                                  {
                                ?>
                                    <tr>
                                      <td><?php echo $i ?></td>
                                      <td><?php echo $record->fee_structure ?></td>
                                      <td><?php echo $record->frequency_code . " - " . $record->frequency_mode ?></td>
                                      <td><?php echo $record->amount_calculatoin_code ?></td>
                                      <td><?php echo $record->amount ?></td>
                                      <td><?php echo $record->funding_percentage ?></td>
                                      <td><?php echo $record->recoverable_amount ?></td>
                                      <!-- <td class="text-center"> -->
                                        <td>
                                      <a onclick="deleteFeeStructure(<?php echo $record->id; ?>)" title="Edit">Delete</a>
                                      </td>
                                    </tr>
                                <?php
                                $total_amount = $total_amount + $record->amount;
                                $i++;
                                  }
                                   $total_amount = number_format($total_amount, 2, '.', ',');
                                  ?>

                                  <tr >
                                      <td bgcolor="" colspan="3"></td>
                                      <td bgcolor="" style="text-align: center;"><b>Total Amount :</b></td>
                                      <td bgcolor=""><b><?php echo $total_amount ?></b></td>
                                      <!-- <td class="text-center"> -->
                                      <td bgcolor=""></td>
                                    </tr>
                                  <?php
                                }
                                ?>
                              </tbody>
                            </table>
                          </div>

                        </div>


                        </div> 
                    </div>

















                    <!-- <div role="tabpanel" class="tab-pane" id="tab_three">
                        <div class="col-12">




                        <form id="form_three" action="" method="post">


                            <div class="form-container">
                                  <h4 class="form-group-title">Fee Structure (Partner University) Details</h4> 

                              <div class="row">

                                    


                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Partner University <span class='error-text'>*</span></label>
                                            <select name="three_id_training_center" id="three_id_training_center" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($partnerUniversityList))
                                                {
                                                    foreach ($partnerUniversityList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->code . " - " . $record->name;?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Fee Type <span class='error-text'>*</span></label>
                                            <select name="three_is_installment" id="three_is_installment" class="form-control" onchange="showInstallments(this.value)">
                                                <option value="">Select</option>
                                                <option value="0">Per Semester</option>
                                                <option value="1">Installment</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3" id="view_fee_item" style="display: none">
                                        <div class="form-group">
                                            <label>Fee Item <span class='error-text'>*</span></label>
                                            <select name="three_id_fee_item" id="three_id_fee_item" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($feeSetupList))
                                                {
                                                    foreach ($feeSetupList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"
                                                        ><?php echo $record->name;?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3" id="view_amount">
                                        <div class="form-group">
                                            <label>Amount <span class='error-text'>*</span></label>
                                            <input type="number" class="form-control" id="three_amount" name="three_amount">
                                        </div>
                                    </div>



                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Currency <span class='error-text'>*</span></label>
                                              <select name="three_currency" id="three_currency" class="form-control">
                                                  <option value="">Select</option>
                                                  <?php
                                                  if (!empty($currencyList))
                                                  {
                                                      foreach ($currencyList as $record)
                                                      {?>
                                                          <option value="<?php echo $record->id;?>"
                                                          ><?php echo $record->code . " - " . $record->name;?>
                                                          </option>
                                                  <?php
                                                      }
                                                  }
                                                  ?>
                                              </select>
                                          </div>
                                      </div>  


                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>No. Of Semesters / Installments To Charge <span class='error-text'>*</span></label>
                                              <input type="number" class="form-control" id="three_installments" name="three_installments">
                                          </div>
                                      </div>                                  

                                    
                                
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveDataPartnerUSD()">Add</button>
                                    </div>

                                  </div>

                            </div>

                           


                        </form>


                        <div class="form-container">
                              <h4 class="form-group-title">Fee Structure List</h4> 


                          

                          <div class="custom-table">
                            <table class="table" id="list-table">
                              <thead>
                                <tr>
                                  <th>Sl. No</th>
                                  <th>Partner University Code</th>
                                  <th>Partner University Name</th>
                                  <th>Fee Item</th>
                                  <th>Fee Type</th>
                                  <th>Semesters / Installments To Charge</th>
                                  <th>Currency</th>
                                  <th>Amount</th>
                                  <th class="text-center">Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                if (!empty($feeStructurePInternationalList))
                                {
                                  $i = 1;
                                  $total_amount = 0;
                                  foreach ($feeStructurePInternationalList as $record)
                                  {
                                ?>
                                    <tr>
                                      <td><?php echo $i ?></td>
                                      <td><?php echo $record->code ?></td>
                                      <td><?php echo $record->name ?></td>
                                      <td><?php echo $record->fee_code . " - " . $record->fee_name ?></td>
                                      <td><?php
                                      if($record->is_installment == 1)
                                      {
                                        echo 'Installment'; 
                                      }
                                      elseif($record->is_installment == 0)
                                      {
                                        echo 'Per Semester';
                                      }?></td>
                                      <td><?php echo $record->installments ?></td>
                                      <td><?php echo $record->currency_code . " - " . $record->currency ?></td>
                                      <td><?php echo $record->amount ?></td>
                                      <td class="text-center">

                                      <a onclick="deleteDataByTrainingCenter(<?php echo $record->id_fee_structure ; ?>)" title="Delete">Delete</a>

                                      <?php if($record->is_installment == 1)
                                      {
                                        ?>
                                      |
                                      
                                      <a onclick="viewFeeByTrainingCenter(<?php echo $record->id_fee_structure; ?>)" title="View">View</a>

                                      <?php
                                      }
                                      ?>                                      
                                      </td>
                                    </tr>
                                <?php
                               
                                $i++;
                                  }
                                  ?>

    
                                  <?php
                                }
                                ?>
                              </tbody>
                            </table>
                          </div>

                        </div>


                        </div> 
                    </div> -->


                </div>

            </div>
        </div> 







        

           
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();




    function saveData()
    {

      if($('#form_one').valid())
        {

        var tempPR = {};

        tempPR['id_fee_item'] = $("#one_id_fee_item").val();
        tempPR['currency'] = 'MYR';
        tempPR['amount'] = $("#one_amount").val();
        tempPR['funding_percentage'] = $("#funding_percentage").val();
        tempPR['recoverable_amount'] = $("#recoverable_amount").val();
        tempPR['id_sub_thrust'] = <?php echo $subThrust->id; ?>;
        tempPR['status'] = <?php echo '1'; ?>;

            $.ajax(
            {
               url: '/finance/feeStructure/addNewFeeStructure',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                window.location.reload();
               }
            });

          }
        
    }


    function saveDataUSD()
    {

      if($('#form_two').valid())
        {


        var tempPR = {};

        tempPR['id_fee_item'] = $("#two_id_fee_item").val();
        tempPR['currency'] = 'USD';
        tempPR['amount'] = $("#two_amount").val();
        tempPR['funding_percentage'] = $("#two_funding_percentage").val();
        tempPR['recoverable_amount'] = $("#two_recoverable_amount").val();


        tempPR['id_sub_thrust'] = <?php echo $subThrust->id; ?>;
        tempPR['status'] = <?php echo '1'; ?>

            $.ajax(
            {
               url: '/finance/feeStructure/addNewFeeStructure',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                window.location.reload();
               }
            });

          }
        
    }


    function deleteFeeStructure(id)
    {
        $.ajax(
            {
               url: '/finance/feeStructure/deleteFeeStructure/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                alert('Deleted Successfully');
                window.location.reload();
               }
            });
    }



    $(document).ready(function()
    {
        $("#form_one").validate({
            rules: {
                one_id_fee_item: {
                    required: true
                },
                one_amount: {
                    required: true
                },
                one_id_frequency_mode: {
                    required: true
                }
            },
            messages: {
                one_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                one_amount: {
                    required: "<p class='error-text'>Enter Amount</p>",
                },
                one_id_frequency_mode: {
                    required: "<p class='error-text'>Select Frequency Mode</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    $(document).ready(function()
    {
        $("#form_two").validate({
            rules: {
                two_id_fee_item: {
                    required: true
                },
                two_amount: {
                    required: true
                },
                two_id_frequency_mode: {
                    required: true
                }
            },
            messages: {
                two_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                two_amount: {
                    required: "<p class='error-text'>Enter Amount</p>",
                },
                two_id_frequency_mode: {
                    required: "<p class='error-text'>Select Frequency Mode</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function() {
        $("#form_three").validate({
            rules: {
                three_id_fee_item: {
                    required: true
                },
                three_amount: {
                    required: true
                },
                three_id_frequency_mode: {
                    required: true
                },
                three_id_training_center: {
                    required: true
                },
                three_is_installment: {
                    required: true
                },
                three_currency: {
                    required: true
                },
                three_installments: {
                  required: true
                }
            },
            messages: {
                three_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                three_amount: {
                    required: "<p class='error-text'>Enter Amount</p>",
                },
                three_id_frequency_mode: {
                    required: "<p class='error-text'>Select Frequency Mode</p>",
                },
                three_id_training_center: {
                    required: "<p class='error-text'>Select Partner University</p>",
                },
                three_is_installment: {
                    required: "<p class='error-text'>Select Installment Status</p>",
                },
                three_currency: {
                    required: "<p class='error-text'>Select Currency</p>",
                },
                three_installments: {
                    required: "<p class='error-text'>Installments Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });








    $(document).ready(function() {
        $("#form_four").validate({
            rules: {
                trigger_id_semester: {
                    required: true
                },
                trigger_id_fee_item: {
                  required: true
                }
            },
            messages: {
                trigger_id_semester: {
                    required: "<p class='error-text'>Select Semester</p>",
                },
                trigger_id_fee_item: {
                    required: "<p class='error-text'>Select Fee Item</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>

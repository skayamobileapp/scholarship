<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Fee Structure List</h3>
      <a href="add" class="btn btn-primary">+ Add Fee Structure</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
               Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Cohert</label>
                      <div class="col-sm-8">
                        <select name="id_cohert" id="id_cohert" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($schemeList)) {
                            foreach ($schemeList as $record)
                            {
                              ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php  if ($record->id == $searchParam['id_cohert']) {
                                echo 'selected';
                              }?>
                                >
                                <?php echo  $record->code ." - " . $record->name;  ?>
                              </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Scholarship</label>
                      <div class="col-sm-8">
                        <select name="id_scholarship" id="id_scholarship" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($scholarshipList)) {
                            foreach ($scholarshipList as $record)
                            {
                              ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php  if ($record->id == $searchParam['id_scholarship']) {
                                echo 'selected';
                              }?>
                                >
                                <?php echo  $record->scholarship_code ." - " . $record->scholarship_name;  ?>
                              </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>

                <div class="row">
                  
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Program</label>
                      <div class="col-sm-8">
                        <select name="id_program" id="id_program" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($programList)) {
                            foreach ($programList as $record)
                            {
                              ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php  if ($record->id == $searchParam['id_program']) {
                                echo 'selected';
                              }?>
                                >
                                <?php echo  $record->code ." - " . $record->name;  ?>
                              </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>

                </div>


                
              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Cohort</th>
            <th>Scholarship</th>
            <th>Programme</th>
            <th>Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($feeStructureList))
          {
            $i=1;
            foreach ($feeStructureList as $record)
            {
             ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->scheme_code . " - " . $record->scheme_name ?></td>
                <td><?php echo $record->scholarship_code . " - " . $record->scholarship_name ?></td>
                <td><?php echo $record->programme_code . " - " . $record->programme_name ?></td>
                <td><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <!-- <td><?php echo date("d-m-Y", strtotime($record->created_dt_tm)) ?></td> -->
                <td class="text-center">

                  <?php if( $record->status == '1')
                  {
                    ?>
                        <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a> | 
                        <a href="<?php echo 'addFeeStructureDetails/' . $record->id; ?>" title="Add">Add Fee</a>
                    <?php
                  }
                  else
                  {
                    ?>
                        <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                    <?php
                  } 
                  ?>



                  
                    <i class="fa fa-trash"></i>
                  </a>

                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
    $('select').select2();
  
    function clearSearchForm()
    {
      window.location.reload();
    }
</script>
<?php $this->load->helper("form"); ?>
<form id="form_fee_structure" action="" method="post">

<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
                <h3>Add Fee Structure </h3>
            <a href="<?php echo '../../../showProgramScheme/' . $intake->id . '/' . $programme->id; ?>" class="btn btn-link">Back</a>
        </div>


            
            <!-- <div class="page-title clearfix"> -->
                <!-- <a href="<?php echo '../../show_intake/' . $programme->id; ?>" class="btn btn-link">Back</a> -->
            <!-- </div> -->


          <div class="form-container">
                <h4 class="form-group-title">Program Details</h4> 

            <div class="row">
                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Programme Name</label>
                          <input type="text" class="form-control" id="name" name="name" value="<?php echo $programme->name; ?>" readonly="readonly">
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Programme Code</label>
                          <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $programme->code; ?>" readonly="readonly">
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Name Optional Language</label>
                          <input type="text" class="form-control" id="code" name="code" value="<?php echo $programme->name_optional_language; ?>" readonly="readonly">
                      </div>
                  </div>
            </div>
          </div>


          <div class="form-container">
                <h4 class="form-group-title">Intake Details</h4> 

            <div class="row">
                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Intake Name</label>
                          <input type="text" class="form-control" id="name" name="name" value="<?php echo $intake->name; ?>" readonly="readonly">
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Year</label>
                          <input type="text" class="form-control" id="year" name="year" value="<?php echo $intake->year; ?>" readonly="readonly">
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Start Date</label>
                          <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo date("d-m-Y", strtotime($intake->start_date)); ?>" readonly="readonly" >
                      </div>
                  </div>
            </div>

          </div>


          <div class="form-container">
                <h4 class="form-group-title">Program Scheme Details</h4> 

            <div class="row">
                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Mode Of Program</label>
                          <input type="text" class="form-control" id="name" name="name" value="<?php echo $programScheme->mode_of_program; ?>" readonly="readonly">
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Mode Of Study</label>
                          <input type="text" class="form-control" id="year" name="year" value="<?php echo $programScheme->mode_of_study; ?>" readonly="readonly">
                      </div>
                  </div>

                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>Min. Duration (Months)</label>
                          <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $programScheme->min_duration; ?>" readonly="readonly" >
                      </div>
                  </div>
            </div>

            <div class="row">

              <div class="col-sm-4">
                      <div class="form-group">
                          <label>Max. Duration (Months)</label>
                          <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $programScheme->max_duration; ?>" readonly="readonly" >
                      </div>
                  </div>


            </div>

          </div>





              <br>

            <div class="page-title clearfix">
                <h3>Add Fee Structure</h3>
            </div>

          <div class="form-container">
                <h4 class="form-group-title">Fee Structure Details</h4> 

            <div class="row">

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Fee Item <span class='error-text'>*</span></label>
                            <select name="id_fee_item" id="id_fee_item" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($feeSetupList))
                                {
                                    foreach ($feeSetupList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                     <div class="col-sm-3">
                        <div class="form-group">
                            <label>Frequency Mode <span class='error-text'>*</span></label>
                            <select name="id_frequency_mode" id="id_frequency_mode" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($frequencyModeList))
                                {
                                    foreach ($frequencyModeList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->code . " - " . $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Amount <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="amount" name="amount">
                        </div>
                    </div>
              
                <div class="col-sm-3">
                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                </div>
            </div>

          </div>


           <div class="button-block clearfix">
              <div class="bttn-group">
                  <!-- <button type="submit" class="btn btn-primary btn-lg">Submit</button> -->
                  <!-- <a href="<?php echo '../../show_intake/' . $programme->id; ?>" class="btn btn-link">Back</a> -->
              </div>
          </div>


            <!-- <div class="row">
                <div id="view"></div>
            </div> -->
          <br>

          <div class="page-title clearfix">
                <h3>Fee Structure List</h3>
          </div>

          <div class="form-container">
                <h4 class="form-group-title">Fee Structure List</h4> 


            

            <div class="custom-table">
              <table class="table" id="list-table">
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Frequency Mode</th>
                    <th>Amount</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if (!empty($feeStructureList))
                  {
                    $i = 1;
                    $total_amount = 0;
                    foreach ($feeStructureList as $record)
                    {
                  ?>
                      <tr>
                        <td><?php echo $i ?></td>
                        <td><?php echo $record->fee_structure ?></td>
                        <td><?php echo $record->frequency_code . " - " . $record->frequency_mode ?></td>
                        <td><?php echo $record->amount ?></td>
                        <!-- <td class="text-center"> -->
                          <td>
                        <a onclick="tempDelete(<?php echo $record->id; ?>)" title="Edit">Delete</a>
                        </td>
                      </tr>
                  <?php
                  $total_amount = $total_amount + $record->amount;
                  $i++;
                    }
                     $total_amount = number_format($total_amount, 2, '.', ',');
                    ?>

                    <tr >
                        <td bgcolor=""></td>
                        <td bgcolor=""></td>
                        <td bgcolor="" style="text-align: center;"><b>Total Amount :</b></td>
                        <td bgcolor=""><b><?php echo $total_amount ?></b></td>
                        <!-- <td class="text-center"> -->
                        <td bgcolor=""></td>
                      </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>

          </div>

        

           
        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>



</form>
<script>

    $('select').select2();
  

    function opendialog()
    {
        $("#id_fee_item").val('');
        $("#amount").val('');
        $("#id_frequency_mode").val('');
        $("#id").val('0');                    
        $('#myModal').modal('show');

    }
    function saveData() {


        var tempPR = {};
        var id_programme = "<?php echo $programme->id; ?>";
        var id_intake = "<?php echo $intake->id; ?>";
        var id_program_scheme = "<?php echo $id_program_scheme; ?>";
        tempPR['id_programme'] = id_programme;
        tempPR['id_program_scheme'] = id_program_scheme;
        tempPR['id_intake'] = id_intake;
        tempPR['id_fee_item'] = $("#id_fee_item").val();
        tempPR['amount'] = $("#amount").val();
        tempPR['id_frequency_mode'] = $("#id_frequency_mode").val();
        tempPR['id'] = $("#id").val();
            $.ajax(
            {
               url: '/finance/feeStructure/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                // $("#view").html(result);
                // $('#myModal').modal('hide');
                window.location.reload();
               }
            });
        
    }

    function tempDelete(id) {
      // alert(id);
         $.ajax(
            {
               url: '/finance/feeStructure/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                alert('Deleted Successfully');
                window.location.reload();
                    // $("#view").html(result);
               }
            });
    }

 //    function tempDelete(id){

 //     $.get("/finance/feeStructure/tempDelete/"+id, function(data, status){
   
 //        // $("#selectsubcategory").html(data);
 //    });
 // }


    function getTempData(id) {
        $.ajax(
            {
               url: '/finance/feeStructure/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });

    }


    // $(document).ready(function() {
    //     $("#form_fee_structure").validate({
    //         rules: {
    //             id_fee_item: {
    //                 required: true
    //             },
    //             amount: {
    //                 required: true
    //             },
    //             id_frequency_mode: {
    //                 required: true
    //             }
    //         },
    //         messages: {
    //             id_fee_item: {
    //                 required: "<p class='error-text'>Select Fee Item</p>",
    //             },
    //             amount: {
    //                 required: "<p class='error-text'>Enter Amount</p>",
    //             },
    //             id_frequency_mode: {
    //                 required: "<p class='error-text'>Select Frequency Mode</p>",
    //             }
    //         },
    //         errorElement: "span",
    //         errorPlacement: function(error, element) {
    //             error.appendTo(element.parent());
    //         }

    //     });
    // });



</script>

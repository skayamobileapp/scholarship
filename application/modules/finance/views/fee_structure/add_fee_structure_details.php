<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Fee Structure Details</h3>
            <a href="../list" class="btn btn-link">< Back</a>

        </div>


            <div class="form-container">
                <h4 class="form-group-title">Fee Structure Details</h4>  


              <div class="row">

                    <div class="col-sm-4">
                      <div class="form-group">
                          <label>Cohort <span class='error-text'>*</span></label>
                          <select name="id_cohert" id="id_cohert" class="form-control selitemIcon" disabled>
                              <option value="">Select</option>
                              <?php
                              if (!empty($schemeList))
                              {
                                  foreach ($schemeList as $record)
                                  {?>
                                      <option value="<?php echo $record->id;?>"
                                        <?php if($record->id == $feeStructure->id_cohert)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                      ><?php echo $record->code . " - " . $record->name; ?>
                                      </option>
                                  <?php
                                  }
                              }
                              ?>

                          </select>
                      </div>
                    </div>



                    <div class="col-sm-4">
                      <div class="form-group">
                          <label>Scholarship <span class='error-text'>*</span></label>
                          <select name="id_scholarship" id="id_scholarship" class="form-control selitemIcon" disabled>
                              <option value="">Select</option>
                              <?php
                              if (!empty($scholarshipList))
                              {
                                  foreach ($scholarshipList as $record)
                                  {?>
                                      <option value="<?php echo $record->id;?>"
                                        <?php if($record->id == $feeStructure->id_scholarship)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                      ><?php echo $record->scholarship_code . " - " . $record->scholarship_name; ?>
                                      </option>
                                  <?php
                                  }
                              }
                              ?>

                          </select>
                      </div>
                    </div>


                    <div class="col-sm-4">
                      <div class="form-group">
                          <label>Program <span class='error-text'>*</span></label>
                          <select name="id_program" id="id_program" class="form-control selitemIcon" disabled>
                              <option value="">Select</option>
                              <?php
                              if (!empty($programList))
                              {
                                  foreach ($programList as $record)
                                  {?>
                                      <option value="<?php echo $record->id;?>"
                                        <?php if($record->id == $feeStructure->id_program)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                      ><?php echo $record->code . " - " . $record->name; ?>
                                      </option>
                                  <?php
                                  }
                              }
                              ?>

                          </select>
                      </div>
                    </div>


                   
                </div>
                <div class="row">



                  <div class="col-sm-4">
                        <div class="form-group">
                            <label>Currency <span class='error-text'>*</span></label>
                            <select name="currency" id="currency" class="form-control" disabled>
                                <option value="">Select</option>
                                <?php
                                if (!empty($currencyList))
                                {
                                    foreach ($currencyList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        <?php if($record->id == $feeStructure->currency)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                        ><?php echo $record->code . " - " . $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div> 

                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $feeStructure->amount; ?>" readonly>
                        </div>
                    </div> -->


                     <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" <?php if($feeStructure->status=='1') {
                                    echo "checked=checked";
                                };?>><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0" <?php if($feeStructure->status=='0') {
                                    echo "checked=checked";
                                };?>>
                                <span class="check-radio"></span> In-Active
                                </label>                              
                            </div>                         
                    </div>



                </div>

            </div>


            <!-- <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div> -->












        <br>


        <div class="form-container">
            <h4 class="form-group-title">Fee Structure Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Fee structure Details</a>
                    </li>
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">








                        <form id="form_one" action="" method="post">

                          <div class="form-container">
                                  <h4 class="form-group-title">Fee Structure (Malaysian) Details</h4> 

                              <div class="row">

                                      <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Fee Item <span class='error-text'>*</span></label>
                                              <select name="one_id_fee_item" id="one_id_fee_item" class="form-control">
                                                  <option value="">Select</option>
                                                  <?php
                                                  if (!empty($feeSetupList))
                                                  {
                                                      foreach ($feeSetupList as $record)
                                                      {?>
                                                          <option value="<?php echo $record->id;?>"
                                                          ><?php echo $record->name.'  -'.$record->frequency_mode;?>
                                                          </option>
                                                  <?php
                                                      }
                                                  }
                                                  ?>
                                              </select>
                                          </div>
                                      </div>



                                      <div class="col-sm-2">
                                          <div class="form-group">
                                              <label>Amount (MYR) <span class='error-text'>*</span></label>
                                              <input type="number" class="form-control" id="one_amount" name="one_amount">
                                          </div>
                                      </div>

                                      <div class="col-sm-2">
                                          <div class="form-group">
                                              <label>Funding Percentage (%)<span class='error-text'>*</span></label>
                                               <input type="text" class="form-control" id="funding_percentage" name="funding_percentage">
                                          </div>
                                      </div>
                                       <div class="col-sm-3">
                                          <div class="form-group">
                                              <label>Recoverable Amount (MYR) <span class='error-text'>*</span></label>
                                               <input type="text" class="form-control" id="recoverable_amount" name="recoverable_amount">
                                          </div>
                                      </div>

                                      <div class="col-sm-2">
                                          <button type="submit" class="btn btn-primary btn-lg">Add</button>
                                      </div>


                                  </div>

                            </div>


                           <!--  <div class="button-block clearfix">
                                <div class="bttn-group">
                                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                    <a href="../list" class="btn btn-link">Back</a>
                                </div>
                            </div> -->




                        </form>



                        

                  <?php
                     
                     if (!empty($feeStructureDetailsByMasterId))
                      {

                        ?>



                        <div class="custom-table">
                          <table class="table" id="list-table">
                            <thead>
                              <tr>
                                <th>Sl. No</th>
                                <th>Fee Item</th>
                                <th>Frequency Mode</th>
                                <th>Amount Calculation</th>
                                <th>Funding Percentage (%)</th>
                                <th>Recoverable Amount(MYR)</th>
                                <th>Amount</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              if (!empty($feeStructureDetailsByMasterId))
                              {
                                $i = 1;
                                $total_amount = 0;
                                foreach ($feeStructureDetailsByMasterId as $record)
                                {
                              ?>
                                  <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->fee_structure ?></td>
                                    <td><?php echo $record->frequency_code . " - " . $record->frequency_mode ?></td>
                                    <td><?php echo $record->amount_calculatoin_code ?></td>
                                     <td><?php echo $record->funding_percentage ?></td>
                                      <td><?php echo $record->recoverable_amount ?></td>
                                    <td><?php echo $record->amount ?></td>
                                    <!-- <td class="text-center"> -->
                                      <td>
                                    <a onclick="deleteFeeStructure(<?php echo $record->id; ?>)" title="Delete">Delete</a>
                                    </td>
                                  </tr>
                              <?php
                              $total_amount = $total_amount + $record->amount;
                              $i++;
                                }
                                 $total_amount = number_format($total_amount, 2, '.', ',');
                                ?>

                                <tr >
                                    <td bgcolor="" colspan="5"></td>
                                    <td bgcolor="" style="text-align: center;"><b>Total Amount :</b></td>
                                    <td bgcolor=""><b><?php echo $total_amount ?></b></td>
                                    <!-- <td class="text-center"> -->
                                    <td bgcolor=""></td>
                                  </tr>
                                <?php
                              }
                              ?>
                            </tbody>
                          </table>
                        </div>


                        <?php
                        }
                        ?>

                        </div> 
                    </div>


                </div>

            </div>
        </div> 



        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>


   
  $('select').select2();


  $( function()
    {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1920:2019"
    });
    });



    function deleteFeeStructure(id)
    {
        $.ajax(
            {
               url: '/finance/feeStructure/deleteFeeStructure/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                alert('Deleted Successfully');
                window.location.reload();
               }
            });
    }

    // function getScholarshipByIdCohort(id)
    // {
    //   $.get("/finance/repayment/getScholarshipByIdCohort/"+id, function(data, status)
    //   {
    //       $("#view_scholarship").html(data);
    //       $("#view_scholarship").show();
    //   });

    // }




    // function getProgramByScholarshipNCohortId()
    // {
    //     var tempPR = {};
    //     // tempPR['id_program'] = idprogram;
    //     tempPR['id_cohert'] = $("#id_cohert").val();
    //     tempPR['id_scholarship'] = $("#id_scholarship").val();
    //     // alert(tempPR['id_program']);

    //     if(tempPR['id_cohert'] != '' && tempPR['id_scholarship'] != '')
    //     {

    //         $.ajax(
    //         {
    //            url: '/finance/repayment/getProgramByScholarshipNCohortId',
    //             type: 'POST',
    //            data:
    //            {
    //             tempData: tempPR
    //            },
    //            error: function()
    //            {
    //             alert('Something is wrong');
    //            },
    //            success: function(result)
    //            {
    //               $("#view_program").html(result);
    //               $("#view_program").show();
    //            }
    //         });
    //     }
    // }



    $(document).ready(function()
    {
        $("#form_one").validate(
        {
            rules:
            {
                one_id_fee_item:
                {
                    required: true
                },
                one_amount:
                {
                    required: true
                },
                funding_percentage:
                {
                    required: true
                },
                recoverable_amount:
                {
                    required: true
                }
            },
            messages:
            {
                one_id_fee_item:
                {
                    required: "<p class='error-text'>Select Fee Item</p>",
                },
                one_amount:
                {
                    required: "<p class='error-text'>Amount Required</p>",
                },
                funding_percentage:
                {
                    required: "<p class='error-text'>Funding Percentage Required</p>",
                },
                recoverable_amount:
                {
                    required: "<p class='error-text'>Recoverable Amount Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element)
            {
                error.appendTo(element.parent());
            }

        });
    });

</script>
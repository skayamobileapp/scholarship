<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Repayment Extension</h3>
            <a href="../extensionList" class="btn btn-link">  < Back </a>

        </div>


          <div class="form-container">
            <h4 class="form-group-title">Student Details</h4>
            <div class='data-list'>
               <div class='row'>
                  <div class='col-sm-6'>
                     <dl>
                        <dt>Student Name :</dt>
                        <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                     </dl>
                     <dl>
                        <dt>Student NRIC :</dt>
                        <dd><?php echo $studentDetails->nric ?></dd>
                     </dl>
                     <dl>
                        <dt>Student Email :</dt>
                        <dd><?php echo $studentDetails->email_id; ?></dd>
                     </dl>
                                     
                  </div>
                  <div class='col-sm-6'>
                     <dl>
                        <dt>Program :</dt>
                        <dd><?php echo $studentDetails->programme_code . " - " . $studentDetails->programme_name; ?></dd>
                     </dl>
                     <dl>
                        <dt>Nationality :</dt>
                        <dd><?php echo $studentDetails->nationality ?></dd>
                     </dl>
                     <dl>
                        <dt>Learning Mode :</dt>
                        <dd><?php echo $studentDetails->program_scheme ?></dd>
                     </dl>     
                  </div>
               </div>
            </div>
         </div>


         <br>
          

          <div class="form-container">
                <h4 class="form-group-title">Repayment Details</h4>  


                <div class="row">

                    <div class="col-sm-4">
                      <div class="form-group">
                          <label>Cohort <span class='error-text'>*</span></label>
                          <select name="id_cohert" id="id_cohert" class="form-control selitemIcon" onchange="getScholarshipByIdCohort(this.value)">
                              <option value="">Select</option>
                              <?php
                              if (!empty($schemeList))
                              {
                                  foreach ($schemeList as $record)
                                  {?>
                                      <option value="<?php echo $record->id;?>"
                                        <?php if($record->id == $repayment->id_cohert)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                      ><?php echo $record->code . " - " . $record->name; ?>
                                      </option>
                                  <?php
                                  }
                              }
                              ?>

                          </select>
                      </div>
                    </div>


                    <div class="col-sm-4">
                          <div class="form-group">
                              <label>Scholarship <span class='error-text'>*</span></label>
                              
                              <span id="view_scholarship">
                                <select class="form-control" id='id_scholarship' name='id_scholarship'>
                                    <option value=''></option>
                                  </select>
                             </span>

                          </div>
                    </div>


                    <div class="col-sm-4">
                          <div class="form-group">
                              <label>Program <span class='error-text'>*</span></label>
                              
                              <span id="view_program">
                                <select class="form-control" id='id_program' name='id_program'>
                                    <option value=''></option>
                                  </select>
                             </span>

                          </div>
                    </div>


                </div>


                <div class="row">



                    <div class="col-sm-4">
                      <div class="form-group">
                          <label>Student <span class='error-text'>*</span></label>
                          
                          <span id="view_student">
                            <select class="form-control" id='id_student' name='id_student'>
                                <option value=''></option>
                              </select>
                         </span>

                        </div>
                  </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Currency <span class='error-text'>*</span></label>
                            <select name="currency" id="currency" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($currencyList))
                                {
                                    foreach ($currencyList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        <?php if($record->id == $repayment->currency)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                        ><?php echo $record->code . " - " . $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $repayment->amount; ?>" readonly>
                        </div>
                    </div>


                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Start Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="start_date" name="start_date" value="<?php echo date('d-m-Y', strtotime($repayment->start_date)); ?>" readonly>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="end_date" name="end_date" value="<?php echo date('d-m-Y', strtotime($repayment->end_date)); ?>" readonly>
                        </div>
                    </div>




                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Repayment Type <span class='error-text'>*</span></label>
                            <select name="payment_type" id="payment_type" class="form-control" onchange="showPaymentType(this.value)" disabled>
                                <option value="">Select</option>
                                <option value="Monthly"
                                <?php if('Monthly' == $repayment->payment_type)
                                {
                                    echo 'selected';
                                }
                                ?>
                                >Monthly</option>
                                <option value="Quaterly"
                                <?php if('Quaterly' == $repayment->payment_type)
                                {
                                    echo 'selected';
                                }
                                ?>
                                >Quaterly</option>
                            </select>
                        </div>
                    </div>



                </div>

                <div class="row">




                    <div class="col-sm-4" id='view_monthly' style="display: none">
                            <div class="form-group">
                                <p>Monthly payment Day <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="day" id="day" value="1" checked="checked"><span class="check-radio"></span> First-Day
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="day" id="day" value="0"><span class="check-radio"></span> Last-Dat
                                </label>                              
                            </div>                         
                    </div>


                    <div class="col-sm-4" id="view_quaterly" style="display: none">
                        <div class="form-group">
                            <label>Amount <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="installment_amount" name="installment_amount" value="<?php echo $repayment->amount; ?>" readonly>
                        </div>
                    </div>




                </div>
            </div>


            <!-- <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../extensionList" class="btn btn-link">Back</a>
                </div>
            </div> -->
        




        <br>





        <form id="form_payment_type" action="" method="post" enctype="multipart/form-data">

          <div class="form-container">
                <h4 class="form-group-title">Extension Details</h4>  


                <div class="row">

                <input type="hidden" class="form-control" id="id_scholar" name="id_scholar" value="<?php echo $repayment->id_student; ?>" >
                <input type="hidden" class="form-control datepicker" id="old_end_date" name="old_end_date" value="<?php echo $repayment->end_date; ?>" readonly>



                  <div class="col-sm-4" id="view_quaterly">
                      <div class="form-group">
                          <label>Additional Months Required <span class='error-text'>*</span></label>
                          <input type="number" class="form-control" id="additionnal_months" name="additionnal_months">
                      </div>
                  </div>


                  <div class="col-sm-4" id="view_quaterly">
                      <div class="form-group">
                          <label>Reason <span class='error-text'>*</span></label>
                          <input type="text" class="form-control" id="reason" name="reason">
                      </div>
                  </div>



                  <div class="col-sm-4">
                      <div class="form-group">
                          <label>End Date <span class='error-text'>*</span></label>
                          <input type="text" class="form-control datepicker" id="new_end_date" name="new_end_date">
                      </div>
                  </div>


                </div>

                <div class="row">


                  <div class="col-sm-4" id="view_quaterly">
                      <div class="form-group">
                          <label>File 1 <span class='error-text'>*</span></label>
                          <input type="file" class="form-control" id="file_one" name="file_one">
                      </div>
                  </div>


                  <div class="col-sm-4" id="view_quaterly">
                      <div class="form-group">
                          <label>File 2</label>
                          <input type="file" class="form-control" id="file_two" name="file_two">
                      </div>
                  </div>



                  <div class="col-sm-4" id="view_quaterly">
                      <div class="form-group">
                          <label>File 2 <span class='error-text'>*</span></label>
                          <input type="file" class="form-control" id="file_three" name="file_three">
                      </div>
                  </div>



                </div>


          </div>


          <div class="button-block clearfix">
              <div class="bttn-group">
                  <button type="submit" class="btn btn-primary btn-lg">Save</button>
              </div>
          </div>



        </form>








        <?php
        if (!empty($repaymentExtensionByRepayment))
        {

          ?>


            <div class="form-container">
                <h4 class="form-group-title">Extension Details</h4>

                    <div class="custom-table">
                      <table class="table" id="list-table">
                        <thead>
                          <tr>
                            <th>Sl. No</th>
                            <th>Addtional Months</th>
                            <th>Reason</th>
                            <th>Old End Date</th>
                            <th>Old End Date</th>
                            <th class="text-center">File One</th>
                            <th class="text-center">File Two</th>
                            <th class="text-center">File Three</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if (!empty($repaymentExtensionByRepayment))
                          {
                            $i=1;
                            foreach ($repaymentExtensionByRepayment as $record)
                            {
                          ?>
                              <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $record->additionnal_months ?></td>
                                <td><?php echo $record->reason ?></td>
                                <td><?php echo date('d-m-Y', strtotime($record->old_end_date)) ?></td>
                                <td><?php echo date('d-m-Y', strtotime($record->new_end_date)) ?></td>
                                <td class="text-center">

                                    <?php
                                    if (!empty($record->file_one))
                                    {
                                      ?>

                                    <a href="<?php echo '/assets/images/' . $record->file_one; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->file_one; ?>)" title="View"> View
                                        <!-- <span style='font-size:18px;'>&#128065;</span> -->
                                    </a>

                                    <?php
                                    }
                                    else
                                    {
                                      echo 'No File';
                                    }
                                  ?>

                                </td>
                                <td class="text-center">

                                    <?php
                                    if (!empty($record->file_two))
                                    {
                                      ?>


                                    <a href="<?php echo '/assets/images/' . $record->file_two; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->file_two; ?>)" title="View"> View
                                    <!-- <span style='font-size:18px;'>&#128065;</span> -->
                                    </a>

                                    <?php
                                    }
                                    else
                                    {
                                      echo 'No File';
                                    }
                                  ?>


                                </td>
                                <td class="text-center">

                                  <?php
                                    if (!empty($record->file_three))
                                    {
                                    ?>

                                    <a href="<?php echo '/assets/images/' . $record->file_three; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->file_three; ?>)" title="View"> View
                                    <!-- <span style='font-size:18px;'>&#128065;</span> -->
                                    </a>


                                    <?php
                                    }
                                    else
                                    {
                                      echo 'No File';
                                    }
                                  ?>


                                </td>
                              </tr>
                          <?php
                          $i++;
                            }
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>

                </div>



          <?php
          }
          ?>




















        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>


   
  $('select').select2();


  $( function()
    {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1920:2019"
    });
    });


    function getScholarshipByIdCohort(id)
    {
      $.get("/finance/repayment/getScholarshipByIdCohort/"+id, function(data, status)
      {
          $("#view_scholarship").html(data);
          $("#view_scholarship").show();
      });

    }




    function getProgramByScholarshipNCohortId()
    {
        var tempPR = {};
        // tempPR['id_program'] = idprogram;
        tempPR['id_cohert'] = $("#id_cohert").val();
        tempPR['id_scholarship'] = $("#id_scholarship").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_cohert'] != '' && tempPR['id_scholarship'] != '')
        {

            $.ajax(
            {
               url: '/finance/repayment/getProgramByScholarshipNCohortId',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  $("#view_program").html(result);
                  $("#view_program").show();
               }
            });
        }
    }


    function getStudentByData()
    {
        var tempPR = {};
        // tempPR['id_program'] = idprogram;
        tempPR['id_cohert'] = $("#id_cohert").val();
        tempPR['id_scholarship'] = $("#id_scholarship").val();
        tempPR['id_program'] = $("#id_program").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_cohert'] != '' && tempPR['id_scholarship'] != '' && tempPR['id_program'] != '')
        {

            $.ajax(
            {
               url: '/finance/repayment/getStudentByData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  $("#view_student").html(result);
                  $("#view_student").show();
               }
            });
        }
    }


    function showPaymentType(type)
    {
        if(type == 'Monthly')
        {
            $("#view_monthly").show();
            $("#view_quaterly").hide();
        }
        else if(type == 'Quaterly')
        {
            $("#view_quaterly").show();
            $("#view_monthly").hide();

        }
    }



    $(document).ready(function()
    {
        var payment_type = '<?php echo $repayment->payment_type; ?>';
        var id_cohert = <?php echo $repayment->id_cohert; ?>;
        var id_scholarship = <?php echo $repayment->id_scholarship; ?>;
        var id_program = <?php echo $repayment->id_program; ?>;
        var id_student = <?php echo $repayment->id_student; ?>;

        

        // alert(payment_type);
        if(payment_type != '')
        {
            showPaymentType(payment_type);
        }



        if(id_scholarship != '')
        {
            $.get("/finance/repayment/getScholarshipByIdCohort/"+id_cohert, function(data, status)
            {
                // alert(data);

                $("#view_scholarship").html(data);
                $("#view_scholarship").show();

                $("#id_scholarship").find('option[value="'+id_scholarship+'"]').attr('selected',true);
                $('select').select2();
            });
        }


        if(id_scholarship != '' && id_cohert != '')
        {
            var tempPR = {};
            tempPR['id_cohert'] = id_cohert;
            tempPR['id_scholarship'] = id_scholarship;
            // alert(tempPR['id_program']);

            if(tempPR['id_cohert'] != '' && tempPR['id_scholarship'] != '')
            {

                $.ajax(
                {
                   url: '/finance/repayment/getProgramByScholarshipNCohortId',
                    type: 'POST',
                   data:
                   {
                    tempData: tempPR
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                        $("#view_program").html(result);
                        $("#view_program").show();

                        $("#id_program").find('option[value="'+id_program+'"]').attr('selected',true);
                        $('select').select2();
                   }
                });
            }        
        }





        if(id_scholarship != '' && id_cohert != '' && id_program != '')
        {
            var tempPR = {};
            tempPR['id_cohert'] = id_cohert;
            tempPR['id_scholarship'] = id_scholarship;
            tempPR['id_program'] = id_program;
            // alert(tempPR['id_program']);

            if(tempPR['id_cohert'] != '' && tempPR['id_scholarship'] != '' && tempPR['id_program'] != '')
            {

                $.ajax(
                {
                   url: '/finance/repayment/getStudentByData',
                    type: 'POST',
                   data:
                   {
                    tempData: tempPR
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                        $("#view_student").html(result);
                        $("#view_student").show();

                        $("#id_student").find('option[value="'+id_student+'"]').attr('selected',true);
                        $('select').select2();
                   }
                });
            }
        }






        $("#form_payment_type").validate(
        {
            rules:
            {
                additionnal_months:
                {
                    required: true
                },
                reason:
                {
                    required: true
                },
                new_end_date:
                {
                    required: true
                },
                file_one:
                {
                    required: true
                }
            },
            messages:
            {
                additionnal_months:
                {
                    required: "<p class='error-text'>Additional Months Required</p>",
                },
                reason:
                {
                    required: "<p class='error-text'>Reason Required</p>",
                },
                new_end_date:
                {
                    required: "<p class='error-text'>Select New End Date</p>",
                },
                file_one:
                {
                    required: "<p class='error-text'>Select File 1</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element)
            {
                error.appendTo(element.parent());
            }

        });
    });

</script>
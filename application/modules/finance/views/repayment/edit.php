<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit / View Repayment</h3>
        </div>

        
        <form id="form_payment_type" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Repayment Details</h4>  


                <div class="row">

                    <div class="col-sm-4">
                      <div class="form-group">
                          <label>Cohort <span class='error-text'>*</span></label>
                          <select name="id_cohert" id="id_cohert" class="form-control selitemIcon" onchange="getScholarshipByIdCohort(this.value)">
                              <option value="">Select</option>
                              <?php
                              if (!empty($schemeList))
                              {
                                  foreach ($schemeList as $record)
                                  {?>
                                      <option value="<?php echo $record->id;?>"
                                        <?php if($record->id == $repayment->id_cohert)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                      ><?php echo $record->code . " - " . $record->name; ?>
                                      </option>
                                  <?php
                                  }
                              }
                              ?>

                          </select>
                      </div>
                    </div>


                    <div class="col-sm-4">
                          <div class="form-group">
                              <label>Scholarship <span class='error-text'>*</span></label>
                              
                              <span id="view_scholarship">
                                <select class="form-control" id='id_scholarship' name='id_scholarship'>
                                    <option value=''></option>
                                  </select>
                             </span>

                          </div>
                    </div>


                    <div class="col-sm-4">
                          <div class="form-group">
                              <label>Program <span class='error-text'>*</span></label>
                              
                              <span id="view_program">
                                <select class="form-control" id='id_program' name='id_program'>
                                    <option value=''></option>
                                  </select>
                             </span>

                          </div>
                    </div>


                </div>


                <div class="row">



                    <div class="col-sm-4">
                      <div class="form-group">
                          <label>Student <span class='error-text'>*</span></label>
                          
                          <span id="view_student">
                            <select class="form-control" id='id_student' name='id_student'>
                                <option value=''></option>
                              </select>
                         </span>

                        </div>
                  </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Currency <span class='error-text'>*</span></label>
                            <select name="currency" id="currency" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($currencyList))
                                {
                                    foreach ($currencyList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        <?php if($record->id == $repayment->currency)
                                        {
                                            echo 'selected';
                                        }
                                        ?>
                                        ><?php echo $record->code . " - " . $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Amount <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="amount" name="amount" value="<?php echo $repayment->amount; ?>" readonly>
                        </div>
                    </div>
                    


                </div>

                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Start Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="start_date" name="start_date" value="<?php echo date('d-m-Y', strtotime($repayment->start_date)); ?>" readonly>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="end_date" name="end_date" value="<?php echo date('d-m-Y', strtotime($repayment->end_date)); ?>" readonly>
                        </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Repayment Type <span class='error-text'>*</span></label>
                            <select name="payment_type" id="payment_type" class="form-control" onchange="showPaymentType(this.value)" disabled>
                                <option value="">Select</option>
                                <option value="Monthly"
                                <?php if('Monthly' == $repayment->payment_type)
                                {
                                    echo 'selected';
                                }
                                ?>
                                >Monthly</option>
                                <option value="Quaterly"
                                <?php if('Quaterly' == $repayment->payment_type)
                                {
                                    echo 'selected';
                                }
                                ?>
                                >Quaterly</option>
                            </select>
                        </div>
                    </div>


                </div>

                <div class="row">



                    <div class="col-sm-4" id='view_monthly' style="display: none">
                            <div class="form-group">
                                <p>Monthly payment Day <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="day" id="day" value="1" checked="checked"><span class="check-radio"></span> First-Day
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="day" id="day" value="0"><span class="check-radio"></span> Last-Dat
                                </label>                              
                            </div>                         
                    </div>


                    <div class="col-sm-4" id="view_quaterly" style="display: none">
                        <div class="form-group">
                            <label>Quarterly Amount <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="installment_amount" name="installment_amount" value="<?php echo $repayment->installment_amount; ?>">
                        </div>
                    </div>




                </div>
            </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <!-- <button type="submit" class="btn btn-primary btn-lg">Save</button> -->
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>


   
  $('select').select2();


  $( function()
    {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1920:2019"
    });
    });


    function getScholarshipByIdCohort(id)
    {
      $.get("/finance/repayment/getScholarshipByIdCohort/"+id, function(data, status)
      {
          $("#view_scholarship").html(data);
          $("#view_scholarship").show();
      });

    }




    function getProgramByScholarshipNCohortId()
    {
        var tempPR = {};
        // tempPR['id_program'] = idprogram;
        tempPR['id_cohert'] = $("#id_cohert").val();
        tempPR['id_scholarship'] = $("#id_scholarship").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_cohert'] != '' && tempPR['id_scholarship'] != '')
        {

            $.ajax(
            {
               url: '/finance/repayment/getProgramByScholarshipNCohortId',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  $("#view_program").html(result);
                  $("#view_program").show();
               }
            });
        }
    }


    function getStudentByData()
    {
        var tempPR = {};
        // tempPR['id_program'] = idprogram;
        tempPR['id_cohert'] = $("#id_cohert").val();
        tempPR['id_scholarship'] = $("#id_scholarship").val();
        tempPR['id_program'] = $("#id_program").val();
        // alert(tempPR['id_program']);

        if(tempPR['id_cohert'] != '' && tempPR['id_scholarship'] != '' && tempPR['id_program'] != '')
        {

            $.ajax(
            {
               url: '/finance/repayment/getStudentByData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                  $("#view_student").html(result);
                  $("#view_student").show();
               }
            });
        }
    }


    function showPaymentType(type)
    {
        if(type == 'Monthly')
        {
            $("#view_monthly").show();
            $("#view_quaterly").hide();
        }
        else if(type == 'Quaterly')
        {
            $("#view_quaterly").show();
            $("#view_monthly").hide();

        }
    }



    $(document).ready(function()
    {
        var payment_type = '<?php echo $repayment->payment_type; ?>';
        var id_cohert = <?php echo $repayment->id_cohert; ?>;
        var id_scholarship = <?php echo $repayment->id_scholarship; ?>;
        var id_program = <?php echo $repayment->id_program; ?>;
        var id_student = <?php echo $repayment->id_student; ?>;

        

        // alert(payment_type);
        if(payment_type != '')
        {
            showPaymentType(payment_type);
        }



        if(id_scholarship != '')
        {
            $.get("/finance/repayment/getScholarshipByIdCohort/"+id_cohert, function(data, status)
            {
                // alert(data);

                $("#view_scholarship").html(data);
                $("#view_scholarship").show();

                $("#id_scholarship").find('option[value="'+id_scholarship+'"]').attr('selected',true);
                $('select').select2();
            });
        }


        if(id_scholarship != '' && id_cohert != '')
        {
            var tempPR = {};
            tempPR['id_cohert'] = id_cohert;
            tempPR['id_scholarship'] = id_scholarship;
            // alert(tempPR['id_program']);

            if(tempPR['id_cohert'] != '' && tempPR['id_scholarship'] != '')
            {

                $.ajax(
                {
                   url: '/finance/repayment/getProgramByScholarshipNCohortId',
                    type: 'POST',
                   data:
                   {
                    tempData: tempPR
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                        $("#view_program").html(result);
                        $("#view_program").show();

                        $("#id_program").find('option[value="'+id_program+'"]').attr('selected',true);
                        $('select').select2();
                   }
                });
            }        
        }





        if(id_scholarship != '' && id_cohert != '' && id_program != '')
        {
            var tempPR = {};
            tempPR['id_cohert'] = id_cohert;
            tempPR['id_scholarship'] = id_scholarship;
            tempPR['id_program'] = id_program;
            // alert(tempPR['id_program']);

            if(tempPR['id_cohert'] != '' && tempPR['id_scholarship'] != '' && tempPR['id_program'] != '')
            {

                $.ajax(
                {
                   url: '/finance/repayment/getStudentByData',
                    type: 'POST',
                   data:
                   {
                    tempData: tempPR
                   },
                   error: function()
                   {
                    alert('Something is wrong');
                   },
                   success: function(result)
                   {
                        $("#view_student").html(result);
                        $("#view_student").show();

                        $("#id_student").find('option[value="'+id_student+'"]').attr('selected',true);
                        $('select').select2();
                   }
                });
            }
        }






        $("#form_payment_type").validate(
        {
            rules:
            {
                id_cohert:
                {
                    required: true
                },
                id_scholarship:
                {
                    required: true
                },
                id_program:
                {
                    required: true
                },
                id_student:
                {
                    required: true
                },
                currency:
                {
                    required: true
                },
                payment_type:
                {
                    required: true
                },
                day:
                {
                    required: true
                },
                installment_amount:
                {
                    required: true
                },
                start_date:
                {
                    required: true
                },
                end_date:
                {
                    required: true
                }
            },
            messages:
            {
                id_cohert:
                {
                    required: "<p class='error-text'>Select Cohort</p>",
                },
                id_scholarship:
                {
                    required: "<p class='error-text'>Select Scholarhip</p>",
                },
                id_program:
                {
                    required: "<p class='error-text'>Select Program</p>",
                },
                id_student:
                {
                    required: "<p class='error-text'>Select Student</p>",
                },
                currency:
                {
                    required: "<p class='error-text'>Select Currency</p>",
                },
                payment_type:
                {
                    required: "<p class='error-text'>Select Repayment Type</p>",
                },
                day:
                {
                    required: "<p class='error-text'>Select Monthly Payment Day</p>",
                },
                installment_amount:
                {
                    required: "<p class='error-text'>Quarterly Installment Amount Required</p>",
                },
                start_date:
                {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                end_date:
                {
                    required: "<p class='error-text'>Select End Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element)
            {
                error.appendTo(element.parent());
            }

        });
    });

</script>
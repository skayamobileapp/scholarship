<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Repayment Extension</h3>
      <!-- <a href="add" class="btn btn-primary">+ Add Repayment</a> -->
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Student Name</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" value="<?php echo $searchParam['name']; ?>">
                      </div>
                    </div>
                  </div>


                

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Student NRIC</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="nric" value="<?php echo $searchParam['nric']; ?>">
                      </div>
                    </div>
                  </div>


                </div>

                <div class="row">


                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Cohert </label>
                      <div class="col-sm-8">
                        <select name="id_cohert" id="id_cohert" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($schemeList)) {
                            foreach ($schemeList as $record)
                            {
                              $selected = '';
                              if ($record->id == $searchParam['id_cohert']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->code ."-".$record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>



                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Scholarship </label>
                      <div class="col-sm-8">
                        <select name="id_scholarship" id="id_scholarship" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($scholarshipList)) {
                            foreach ($scholarshipList as $record)
                            {
                              $selected = '';
                              if ($record->id == $searchParam['id_scholarship']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->scholarship_code ."-".$record->scholarship_name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>


                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Programme </label>
                      <div class="col-sm-8">
                        <select name="id_programme" id="id_programme" class="form-control">
                          <option value="">Select</option>
                          <?php
                          if (!empty($programList)) {
                            foreach ($programList as $record)
                            {
                              $selected = '';
                              if ($record->id == $searchParam['id_programme']) {
                                $selected = 'selected';
                              }
                          ?>
                              <option value="<?php echo $record->id;  ?>"
                                <?php echo $selected;  ?>>
                                <?php echo  $record->code ."-".$record->name;  ?>
                                </option>
                          <?php
                            }
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>                       


              </div>


              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>

            <th>Sl. No</th>
            <th>Student</th>
            <th>Cohort</th>
            <th>Scholarship</th>
            <th>Programme</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
              <!-- <tr>
                <td>1</td>
                <td>IC2131 - Ibrahim Luthia</td>
                <td>Cohert One</td>
                <td>National Scholarship</td>
                <td>PIG786 - Program Batchelors</td>
                <td>12-10-2020</td>
                <td>12-1-2021</td>
                <td class="text-center">
                  <a href="<?php echo 'edit/1'; ?>" title="Edit">Edit</a>
                  
                </td>
              </tr> -->

          <?php
          if (!empty($repaymentList)) {
            $i=1;
            foreach ($repaymentList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->nric . " - " . $record->full_name ?></td>
                <td><?php echo $record->scheme_code . " - " . $record->scheme_name ?></td>
                <td><?php echo $record->scholarship_code . " - " . $record->scholarship_name ?></td>
                <td><?php echo $record->programme_code . " - " . $record->programme_name ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->start_date)); ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->end_date)) ?></td>
                <td class="text-center">
                  <a href="<?php echo 'extend/' . $record->id; ?>" title="Extend End Date">Extend</a>
                  
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>

  $('select').select2();

  
  function clearSearchForm()
      {
        window.location.reload();
      }
</script>
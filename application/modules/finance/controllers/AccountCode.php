<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AccountCode extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('account_code_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('account_code.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['accountCodeList'] = $this->account_code_model->accountCodeListSearch($name);

            $this->global['pageTitle'] = 'Scholarship Management System : Account Code List';
            $this->loadViews("account_code/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('account_code.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_scholar_session_id;
            $user_id = $this->session->userId;   

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'type' => $type,
                    'status' => $status,
                    'created_by' => $user_id
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->account_code_model->addNewAccountCode($data);
                redirect('/finance/accountCode/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add AccountCode';
            $this->loadViews("account_code/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('account_code.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_scholar_session_id;
            $user_id = $this->session->userId; 

            if ($id == null)
            {
                redirect('/setup/accountCode/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'type' => $type,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                $result = $this->account_code_model->editAccountCode($data,$id);
                redirect('/finance/accountCode/list');
            }
            $data['accountCode'] = $this->account_code_model->getAccountCode($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Account Code';
            $this->loadViews("account_code/edit", $this->global, $data, NULL);
        }
    }
}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FeeStructure extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('fee_setup_model');
        $this->load->model('fee_structure_model');
        $this->load->model('fee_category_model');
        $this->load->model('frequency_mode_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('fee_setup.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
        	$formData['id_cohert'] = $this->security->xss_clean($this->input->post('id_cohert'));
            $formData['id_scholarship'] = $this->security->xss_clean($this->input->post('id_scholarship'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
 
            $data['searchParam'] = $formData;
            // $data['subThrustList'] = $this->fee_structure_model->subThrustListSearch($formData);
            $data['feeStructureList'] = $this->fee_structure_model->feeStructureListSearch($formData);
            $data['schemeList'] = $this->fee_structure_model->schemeListByStatus('1');
            $data['scholarshipList'] = $this->fee_structure_model->scholarshipListByStatus('1');
            $data['programList'] = $this->fee_structure_model->programListByStatus('1');

            //print_r($subjectDetails);exit;


            $this->global['pageTitle'] = 'Scholarship Management System : Fee Structure List';
            $this->loadViews("fee_structure/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkScholarAccess('fee_structure.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {           

            if($this->input->post())
            {
                $id_cohert = $this->security->xss_clean($this->input->post('id_cohert'));
                $id_scholarship = $this->security->xss_clean($this->input->post('id_scholarship'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $currency = $this->security->xss_clean($this->input->post('currency'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $data = array(
                    'id_cohert' => $id_cohert,
                    'id_scholarship' => $id_scholarship,
                    'id_program' => $id_program,
                    'amount' => $amount,
                    'currency' => $currency,
                    'status' => $status
                );

                $result = $this->fee_structure_model->addNewFeeStructureMaster($data);
                redirect('/finance/feeStructure/addFeeStructureDetails/'.$result);
            }        

            // $data['schemeList'] = $this->fee_structure_model->schemeListByStatusForAdd('1');
            $data['schemeList'] = $this->fee_structure_model->schemeListByStatus('1');
            $data['currencyList'] = $this->fee_structure_model->currencyListByStatus('1');

        // echo "<Pre>";  print_r($data['subThrust']);exit;
            $this->global['pageTitle'] = 'Scholarship Management System : Add Fee Structure';
            $this->loadViews("fee_structure/add_master", $this->global, $data, NULL);
            // }
        }
    }

    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('fee_structure.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/feeStructure/list');
            }
            if($this->input->post())
            {
                $id_cohert = $this->security->xss_clean($this->input->post('id_cohert'));
                $id_scholarship = $this->security->xss_clean($this->input->post('id_scholarship'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $currency = $this->security->xss_clean($this->input->post('currency'));
                $status = $this->security->xss_clean($this->input->post('status'));


                $data = array(
                    'id_cohert' => $id_cohert,
                    'id_scholarship' => $id_scholarship,
                    'id_program' => $id_program,
                    'amount' => $amount,
                    'currency' => $currency,
                    'status' => $status
                );

                // echo "<Pre>";print_r($id);exit;

                $result = $this->fee_structure_model->editFeeStructureMaster($data,$id);
                redirect('/finance/feeStructure/list');
            }
            // $data['studentList'] = $this->fee_category_model->studentList();

            $data['feeStructure'] = $this->fee_structure_model->getFeeStructure($id);
            $data['schemeList'] = $this->fee_structure_model->schemeListByStatus('1');
            $data['currencyList'] = $this->fee_structure_model->currencyListByStatus('1');

            $this->global['pageTitle'] = 'Scholarship Management System : Edit Fee Category';
            $this->loadViews("fee_structure/edit", $this->global, $data, NULL);
        }
    }


    function addFeeStructureDetails($id = NULL)
    {
        if ($this->checkScholarAccess('fee_structure.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/feeStructure/list');
            }
            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit;

                $id_fee_item = $this->security->xss_clean($this->input->post('one_id_fee_item'));
                $amount = $this->security->xss_clean($this->input->post('one_amount'));
                $funding_percentage = $this->security->xss_clean($this->input->post('funding_percentage'));
                $recoverable_amount = $this->security->xss_clean($this->input->post('recoverable_amount'));


                $data = array(
                    'id_fee_structure' => $id,
                    'id_fee_item' => $id_fee_item,
                    'amount' => $amount,
                    'funding_percentage' => $funding_percentage,
                    'recoverable_amount' => $recoverable_amount
                );

                // echo "<Pre>";print_r($id);exit;
                
                $result = $this->fee_structure_model->addNewFeeStructure($data);
                redirect($_SERVER['HTTP_REFERER']);
            }
            // $data['studentList'] = $this->fee_category_model->studentList();

            
            $data['schemeList'] = $this->fee_structure_model->schemeListByStatus('1');
            $data['scholarshipList'] = $this->fee_structure_model->scholarshipListByStatus('1');
            $data['programList'] = $this->fee_structure_model->programListByStatus('1');
            $data['currencyList'] = $this->fee_structure_model->currencyListByStatus('1');
            $data['feeSetupList'] = $this->fee_structure_model->feeSetupListByStatus('1');


            $data['feeStructure'] = $this->fee_structure_model->getFeeStructure($id);
            $data['feeStructureDetailsByMasterId'] = $this->fee_structure_model->feeStructureDetailsByMasterId($id);

            $this->global['pageTitle'] = 'Scholarship Management System : Edit Fee Category';
            $this->loadViews("fee_structure/add_fee_structure_details", $this->global, $data, NULL);
        }
    }



    function addFeeStructure($id_sub_thrust)
    {
        if ($this->checkScholarAccess('fee_structure.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {           

            if($this->input->post())
            {
                redirect('/finance/feeStructure/programmeLandscapeList/'.$id_programme);
            }        

            $data['feeSetupList'] = $this->fee_structure_model->feeSetupListByStatus('1');



            $data['feeStructureLocalList'] = $this->fee_structure_model->getFeeStructureByThrustNCurrency($id_sub_thrust,'MYR');
            $data['feeStructureInternationalList'] = $this->fee_structure_model->getFeeStructureByThrustNCurrency($id_sub_thrust,'USD');

            $data['subThrust'] = $this->fee_structure_model->getSubThrust($id_sub_thrust);


            $data['id_sub_thrust'] = $id_sub_thrust;

        // echo "<Pre>";  print_r($data['subThrust']);exit;



            $this->global['pageTitle'] = 'Scholarship Management System : Add Fee Structure';
            $this->loadViews("fee_structure/add_fee_structure", $this->global, $data, NULL);
            // }
        }
    }

     function addNewFeeStructure()
    {
        // $id_session = $this->session->my_scholar_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>";  print_r($tempData);exit;
        // $check_duplication = $this->fee_structure_model->checkDuplicationTrainingCenterFeeStructure($tempData);

        // if($check_duplication)
        // {
        //     echo 0;exit;
        // }else
        // {
            $inserted_id = $this->fee_structure_model->addNewFeeStructure($tempData);
            echo $inserted_id;exit;
        // }   
    }

    function deleteFeeStructure($id)
    {
        $this->fee_structure_model->deleteFeeStructure($id);
        echo 'Success';exit(); 
    }


    function getScholarshipByIdCohort($id_cohort)
    {
        // echo "<Pre>"; print_r($id_cohort);exit;
        
        $intake_data = $this->fee_structure_model->getScholarshipByIdCohort($id_cohort);
        

        // echo "<Pre>"; print_r($intake_data);exit;

         $table="
        <script type='text/javascript'>

        $('select').select2();
                        
        </script>


        <select name='id_scholarship' id='id_scholarship' class='form-control' onchange='getProgramByScholarshipNCohortId()'>";
        $table.="<option value=''>Select</option>";

        for($i=0;$i<count($intake_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $intake_data[$i]->id;
        $name = $intake_data[$i]->scholarship_name;
        $code = $intake_data[$i]->scholarship_code;

        $table.="<option value=".$id.">". $code . " - " .  $name .
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }


    function getProgramByScholarshipNCohortId()
    {
        $data = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($data);exit;
        
        $intake_data = $this->fee_structure_model->getProgramByScholarshipNCohortId($data);
        

        // echo "<Pre>"; print_r($intake_data);exit;

         $table="
        <script type='text/javascript'>

        $('select').select2();
                        
        </script>


        <select name='id_program' id='id_program' class='form-control'>";
        $table.="<option value=''>Select</option>";

        for($i=0;$i<count($intake_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $intake_data[$i]->id;
        $name = $intake_data[$i]->name;
        $code = $intake_data[$i]->code;

        $table.="<option value=".$id.">". $code . " - " .  $name .
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }


}

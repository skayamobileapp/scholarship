<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PaymentType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_type_model');
        $this->isScholarLoggedIn();
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Campus Management System : 404 - Page Not Found';
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    function list()
    {
        if ($this->checkScholarAccess('payment_type.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['paymentTypeList'] = $this->payment_type_model->paymentTypeList();
            $this->global['pageTitle'] = 'Campus Management System : Payment Type List';
            $this->loadViews("payment_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('payment_type.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
                            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->payment_type_model->addNewPaymentType($data);
                redirect('/finance/paymentType/list');
            }
            //print_r($data['stateList']);exit;
            $this->global['pageTitle'] = 'Campus Management System : Add Payment Type';
            $this->loadViews("payment_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('payment_type.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/paymentType/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
                            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                
                $result = $this->payment_type_model->editPaymentType($data,$id);
                redirect('/finance/paymentType/list');
            }
            $data['paymentTypeDetails'] = $this->payment_type_model->getPaymentType($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit Payment Type';
            $this->loadViews("payment_type/edit", $this->global, $data, NULL);
        }
    }
}

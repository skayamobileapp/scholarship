<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Repayment extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('repayment_model');
        $this->isScholarLoggedIn();
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Scholarship Management System : 404 - Page Not Found';
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    function list()
    {
        if ($this->checkScholarAccess('repayment.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_scholarship'] = $this->security->xss_clean($this->input->post('id_scholarship'));
            $formData['id_cohert'] = $this->security->xss_clean($this->input->post('id_cohert'));
            // $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['status'] = '';

            $data['searchParam'] = $formData;

            $data['repaymentList'] = $this->repayment_model->repaymentListSearch($formData);

            $data['schemeList'] = $this->repayment_model->schemeListByStatus('1');
            $data['scholarshipList'] = $this->repayment_model->scholarshipListByStatus('1');
            $data['programList'] = $this->repayment_model->programListByStatus('1');



            // echo "<Pre>"; print_r($data['repaymentList']);exit;


            $this->global['pageTitle'] = 'Scholarship Management System : Repayment List';
            $this->loadViews("repayment/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('repayment.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_student = $this->security->xss_clean($this->input->post('id_student'));
                $id_cohert = $this->security->xss_clean($this->input->post('id_cohert'));
                $id_scholarship = $this->security->xss_clean($this->input->post('id_scholarship'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $currency = $this->security->xss_clean($this->input->post('currency'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $installment_amount = $this->security->xss_clean($this->input->post('installment_amount'));;
                $day = $this->security->xss_clean($this->input->post('day'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));





                $data = array(
                    'id_student' => $id_student,
                    'id_cohert' => $id_cohert,
                    'id_scholarship' => $id_scholarship,
                    'id_program' => $id_program,
                    'amount' => $amount,
                    'currency' => $currency,
                    'payment_type' => $type,
                    'installment_amount' => $installment_amount,
                    'day' => $day,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'status' => 1
                );

                $result = $this->repayment_model->addNewRepayment($data);
                redirect('/finance/repayment/list');
            }

            $data['schemeList'] = $this->repayment_model->schemeListByStatus('1');
            $data['currencyList'] = $this->repayment_model->currencyListByStatus('1');


            // echo "<Pre>"; print_r($data['schemeList']);exit;

            $this->global['pageTitle'] = 'Scholarship Management System : Add Repayment';
            $this->loadViews("repayment/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('repayment.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/repayment/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
                            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                
                $result = $this->repayment_model->editPaymentType($data,$id);
                redirect('/finance/repayment/list');
            }

            
            $data['repayment'] = $this->repayment_model->getRepayment($id);
            $data['schemeList'] = $this->repayment_model->schemeListByStatus('1');
            $data['currencyList'] = $this->repayment_model->currencyListByStatus('1');

            // echo "<Pre>"; print_r($data['repayment']);exit;

            $this->global['pageTitle'] = 'Scholarship Management System : Edit Repayment';
            $this->loadViews("repayment/edit", $this->global, $data, NULL);
        }
    }

    function extensionList()
    {
        if ($this->checkScholarAccess('repayment_extension.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_scholarship'] = $this->security->xss_clean($this->input->post('id_scholarship'));
            $formData['id_cohert'] = $this->security->xss_clean($this->input->post('id_cohert'));
            // $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['status'] = '';

            $data['searchParam'] = $formData;

            $data['repaymentList'] = $this->repayment_model->repaymentListSearch($formData);

            $data['schemeList'] = $this->repayment_model->schemeListByStatus('1');
            $data['scholarshipList'] = $this->repayment_model->scholarshipListByStatus('1');
            $data['programList'] = $this->repayment_model->programListByStatus('1');



            // echo "<Pre>"; print_r($data['repaymentList']);exit;


            $this->global['pageTitle'] = 'Scholarship Management System : Repayment Extension List';
            $this->loadViews("repayment/extension_list", $this->global, $data, NULL);
        }
    }

    function extend($id = NULL)
    {
        if ($this->checkScholarAccess('repayment_extension.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/repayment/extensionList');
            }
            if($this->input->post())
            {
                // echo "<Pre>"; print_r($this->input->post());exit;
                // echo "<Pre>"; print_r($_FILES);exit;

                // $id_repayment = $this->security->xss_clean($this->input->post('id_repayment'));
                $id_student = $this->security->xss_clean($this->input->post('id_scholar'));
                $additionnal_months = $this->security->xss_clean($this->input->post('additionnal_months'));
                $reason = $this->security->xss_clean($this->input->post('reason'));
                $old_end_date = $this->security->xss_clean($this->input->post('old_end_date'));
                $new_end_date = $this->security->xss_clean($this->input->post('new_end_date'));
                $installment_amount = $this->security->xss_clean($this->input->post('installment_amount'));
                // $file_one = $this->security->xss_clean($this->input->post('file_one'));
                // $file_two = $this->security->xss_clean($this->input->post('file_two'));
                // $file_three   = $this->security->xss_clean($this->input->post('file_three'));

                $repayment_data['end_date'] = date('Y-m-d', strtotime($new_end_date));

                $updated_repayment = $this->repayment_model->editRepayment($repayment_data,$id);

                if($updated_repayment)
                {


                          
                    $data = array(
                        'id_repayment' => $id,
                        'id_student' => $id_student,
                        'additionnal_months' => $additionnal_months,
                        'reason' => $reason,
                        'old_end_date' => $old_end_date,
                        'new_end_date' => date('Y-m-d', strtotime($new_end_date)),
                        'installment_amount' => $installment_amount,
                    );


                    if (!empty($_FILES['file_one']['name']))
                    {

                      // $certificate_name = $_FILES['certificate']['name'];
                      
                      $file_one_name = $_FILES['file_one']['name'];
                      $file_one_size =$_FILES['file_one']['size'];
                      $file_one_tmp =$_FILES['file_one']['tmp_name'];
                      $file_one_type=$_FILES['file_one']['type'];
                      // $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
                      $file_one_ext=explode('.',$file_one_name);
                      $file_one_ext=end($file_one_ext);
                      $file_one_ext=strtolower($file_one_ext);

                      

                      // For file validation from 36 to 44 , file size validation

                      $this->fileFormatNSizeValidation($file_one_ext,$file_one_size,'File One');

                      $data['file_one'] = $this->uploadFile($file_one_name,$file_one_tmp,'File One');
                    }



                    if (!empty($_FILES['file_two']['name']))
                    {

                      $file_two_name = $_FILES['file_two']['name'];
                      $file_two_size = $_FILES['file_two']['size'];
                      $file_two_tmp =$_FILES['file_two']['tmp_name'];

                      $file_two_ext=explode('.',$file_two_name);
                      $file_two_ext=end($file_two_ext);
                      $file_two_ext=strtolower($file_two_ext);


                      $this->fileFormatNSizeValidation($file_two_ext,$file_two_size,'Transcript');
                      $data['file_two'] = $this->uploadFile($file_two_name,$file_two_tmp,'file_two');


                    }


                    if (!empty($_FILES['file_three']['name']))
                    {

                      $file_three_name = $_FILES['file_three']['name'];
                      $file_three_size = $_FILES['file_three']['size'];
                      $file_three_tmp =$_FILES['file_three']['tmp_name'];

                      $file_three_ext=explode('.',$file_three_name);
                      $file_three_ext=end($file_three_ext);
                      $file_three_ext=strtolower($file_three_ext);


                      $this->fileFormatNSizeValidation($file_three_ext,$file_three_size,'Transcript');
                      $data['file_three'] = $this->uploadFile($file_three_name,$file_three_tmp,'transcript');


                    }

                        // echo "<Pre>"; print_r($data);exit;


                        // 'file_one' => $file_one,
                        // 'file_two' => $file_two,
                        // 'file_three' => $file_three
                        
                        $result = $this->repayment_model->addRepaymentExtension($data);


                }

                redirect($_SERVER['HTTP_REFERER']);
            }

            
            $data['repayment'] = $this->repayment_model->getRepayment($id);
            $data['schemeList'] = $this->repayment_model->schemeListByStatus('1');
            $data['currencyList'] = $this->repayment_model->currencyListByStatus('1');

            $data['studentDetails'] = $this->repayment_model->getStudentByStudentId($data['repayment']->id_student);


            $data['repaymentExtensionByRepayment'] = $this->repayment_model->repaymentExtensionByRepayment($id);


            // echo "<Pre>"; print_r($data['repaymentExtensionByRepayment']);exit;

            $this->global['pageTitle'] = 'Scholarship Management System : Add Repayment Extension';
            $this->loadViews("repayment/extension_add", $this->global, $data, NULL);
        }
    }


    function getScholarshipByIdCohort($id_cohort)
    {
        // echo "<Pre>"; print_r($id_cohort);exit;
        
        $intake_data = $this->repayment_model->getScholarshipByIdCohort($id_cohort);
        

        // echo "<Pre>"; print_r($intake_data);exit;

         $table="
        <script type='text/javascript'>

        $('select').select2();
                        
        </script>


        <select name='id_scholarship' id='id_scholarship' class='form-control' onchange='getProgramByScholarshipNCohortId()'>";
        $table.="<option value=''>Select</option>";

        for($i=0;$i<count($intake_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $intake_data[$i]->id;
        $name = $intake_data[$i]->scholarship_name;
        $code = $intake_data[$i]->scholarship_code;

        $table.="<option value=".$id.">". $code . " - " .  $name .
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }


    function getProgramByScholarshipNCohortId()
    {
        $data = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($data);exit;
        
        $intake_data = $this->repayment_model->getProgramByScholarshipNCohortId($data);
        

        // echo "<Pre>"; print_r($intake_data);exit;

         $table="
        <script type='text/javascript'>

        $('select').select2();
                        
        </script>


        <select name='id_program' id='id_program' class='form-control' onchange='getStudentByData()'>";
        $table.="<option value=''>Select</option>";

        for($i=0;$i<count($intake_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $intake_data[$i]->id;
        $name = $intake_data[$i]->name;
        $code = $intake_data[$i]->code;

        $table.="<option value=".$id.">". $code . " - " .  $name .
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }


    function getStudentByData()
    {
        $data = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($data);exit;
        
        $intake_data = $this->repayment_model->getStudentByData($data);
        

        // echo "<Pre>"; print_r($intake_data);exit;

         $table="
        <script type='text/javascript'>

        $('select').select2();
                        
        </script>


        <select name='id_student' id='id_student' class='form-control'>";
        $table.="<option value=''>Select</option>";

        for($i=0;$i<count($intake_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $intake_data[$i]->id;
        $full_name = $intake_data[$i]->full_name;
        $nric = $intake_data[$i]->nric;

        $table.="<option value=".$id.">". $nric . " - " .  $full_name .
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }
}

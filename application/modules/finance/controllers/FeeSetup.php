<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FeeSetup extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('fee_setup_model');
        $this->load->model('fee_category_model');
        $this->load->model('amount_calculation_type_model');
        $this->load->model('frequency_mode_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('fee_setup.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            //echo "<Pre>"; print_r($data);exit;
            $data['feeSetupList'] = $this->fee_setup_model->feeSetupList();
            $this->global['pageTitle'] = 'Scholarship Management System : Sponser List';
            $this->loadViews("fee_setup/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('fee_setup.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {

                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $id_fee_category = $this->security->xss_clean($this->input->post('id_fee_category'));
                $id_amount_calculation_type = $this->security->xss_clean($this->input->post('id_amount_calculation_type'));
                $id_frequency_mode = $this->security->xss_clean($this->input->post('id_frequency_mode'));
                $account_code = $this->security->xss_clean($this->input->post('account_code'));
                $is_refundable = $this->security->xss_clean($this->input->post('is_refundable'));
                $is_non_invoice = $this->security->xss_clean($this->input->post('is_non_invoice'));
                $is_gst = $this->security->xss_clean($this->input->post('is_gst'));
                $gst_tax = $this->security->xss_clean($this->input->post('gst_tax'));
                $effective_date = $this->security->xss_clean($this->input->post('effective_date'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $recoverable_day = $this->security->xss_clean($this->input->post('recoverable_day'));
                $recoverable_period = $this->security->xss_clean($this->input->post('recoverable_period'));

            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'recoverable_day' => $recoverable_day,
                    'recoverable_period' => $recoverable_period,  
                    'name_optional_language' => $name_optional_language,
                    'id_fee_category' => $id_fee_category,
                    'id_amount_calculation_type' => $id_amount_calculation_type,
                    'id_frequency_mode' => $id_frequency_mode,
                    'account_code' => $account_code,
                    'is_refundable' => $is_refundable,
                    'is_non_invoice' => $is_non_invoice,
                    'is_gst' => $is_gst,
                    'gst_tax' => $gst_tax,
                    'effective_date' => date('Y-m-d',strtotime($effective_date)),
                    'status' => $status
                );
                //echo "<Pre>"; print_r($data);exit;

                $inserted_id = $this->fee_setup_model->addNewFeeSetup($data);
                redirect('/finance/feeSetup/list');
            }
            
            $data['accountCodeList'] = $this->fee_setup_model->financialAccountCodeListByStatus('1');
            $data['feeCategoryList'] = $this->fee_category_model->feeCategoryList();
            $data['amountCalculationTypeList'] = $this->amount_calculation_type_model->amountCalculationTypeList();
            $data['frequencyModeList'] = $this->frequency_mode_model->frequencyModeList();
            $this->global['pageTitle'] = 'Scholarship Management System : Add Sponsor';
            $this->loadViews("fee_setup/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('fee_setup.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/feeSetup/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $id_fee_category = $this->security->xss_clean($this->input->post('id_fee_category'));
                $id_amount_calculation_type = $this->security->xss_clean($this->input->post('id_amount_calculation_type'));
                $id_frequency_mode = $this->security->xss_clean($this->input->post('id_frequency_mode'));
                $account_code = $this->security->xss_clean($this->input->post('account_code'));
                $is_refundable = $this->security->xss_clean($this->input->post('is_refundable'));
                $is_non_invoice = $this->security->xss_clean($this->input->post('is_non_invoice'));
                $is_gst = $this->security->xss_clean($this->input->post('is_gst'));
                $gst_tax = $this->security->xss_clean($this->input->post('gst_tax'));
                $effective_date = $this->security->xss_clean($this->input->post('effective_date'));
                $status = $this->security->xss_clean($this->input->post('status'));

                            $recoverable_day = $this->security->xss_clean($this->input->post('recoverable_day'));
                $recoverable_period = $this->security->xss_clean($this->input->post('recoverable_period'));

                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'recoverable_day' => $recoverable_day,
                    'recoverable_period' => $recoverable_period,                    
                    'name_optional_language' => $name_optional_language,
                    'id_fee_category' => $id_fee_category,
                    'id_amount_calculation_type' => $id_amount_calculation_type,
                    'id_frequency_mode' => $id_frequency_mode,
                    'account_code' => $account_code,
                    'is_refundable' => $is_refundable,
                    'is_non_invoice' => $is_non_invoice,
                    'is_gst' => $is_gst,
                    'gst_tax' => $gst_tax,
                    'effective_date' => date('Y-m-d',strtotime($effective_date)),
                    'status' => $status
                );

                //echo "<Pre>"; print_r($data);exit;
                $result = $this->fee_setup_model->editFeeSetup($data,$id);
                redirect('/finance/feeSetup/list');
            }
            $data['accountCodeList'] = $this->fee_setup_model->financialAccountCodeListByStatus('1');
            $data['feeCategoryList'] = $this->fee_category_model->feeCategoryList();
            $data['amountCalculationTypeList'] = $this->amount_calculation_type_model->amountCalculationTypeList();
            $data['frequencyModeList'] = $this->frequency_mode_model->frequencyModeList();
            $data['feeSetup'] = $this->fee_setup_model->getFeeSetup($id);
            //echo "<Pre>"; print_r($data['feeSetup']);exit;
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Sponsor';
            $this->loadViews("fee_setup/edit", $this->global, $data, NULL);
        }
    }
}

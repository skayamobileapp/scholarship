<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ApplyClaimTraining extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('apply_claim_training_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('apply_claim_training.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
        	$formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));
 
            $data['searchParam'] = $formData;

            $data['applyClaimTrainingList'] = $this->apply_claim_training_model->applyClaimTrainingListSearch($formData);
            $data['programList'] = $this->apply_claim_training_model->programListByStatus('1');

            // echo "<Pre>"; print_r($data['applyClaimTrainingList']);exit;

            $this->global['pageTitle'] = 'Scholarship Management System : Apply Claim List';
            $this->loadViews("apply_claim_training/list", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('apply_claim_training.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {


            if ($id == null)
            {
                redirect('/student/applyClaimTraining/list');
            }

            $id_scholar = $this->session->id_scholar;


            if($this->input->post())
            {

            // echo "<Pre>";print_r($this->input->post());exit;

                $reason = $this->security->xss_clean($this->input->post('reason'));
                $claim_amount = $this->security->xss_clean($this->input->post('claim_amount'));
                $reference_number = $this->security->xss_clean($this->input->post('reference_number'));
                $paid_date = $this->security->xss_clean($this->input->post('paid_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                if($paid_date)
                {
                    $paid_date = date('Y-m-d', strtotime($paid_date));
                }


                $data = array(
                    'reason' => $reason,
                    'claim_amount' => $claim_amount,
                    'reference_number' => $reference_number,
                    'paid_date' => $paid_date,
                    'reason' => $reason,
                    'status' => $status,
                    'updated_by' => $id_scholar
                );

            // echo "<Pre>";print_r($data);exit;

                $result = $this->apply_claim_training_model->editApplyClaim($data,$id);
                redirect('/finance/applyClaimTraining/list');
            
            }
            
            $data['applyClaimTraining'] = $this->apply_claim_training_model->getApplyClaim($id);
            $data['studentDetails'] = $this->apply_claim_training_model->getStudentByStudentId($data['applyClaimTraining']->id_student);

            // echo "<Pre>";print_r($data['applyClaimTraining']);exit;

            $this->global['pageTitle'] = 'Scholarship Student Portal : Edit ApplyClaim';
            $this->loadViews("apply_claim_training/edit", $this->global, $data, NULL);

        }
    }


    function view($id = NULL)
    {
        if ($this->checkScholarAccess('apply_claim_training.approve') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if ($id == null)
            {
                redirect('/student/applyClaimTraining/list');
            }

            $id_student = $this->session->id_student;
            $id_intake = $this->session->id_intake;
            $id_program = $this->session->id_program;
            $id_qualification = $this->session->id_qualification;
            $id_program_scheme = $this->session->id_program_scheme;


            if($this->input->post())
            {
               redirect('/student/applyClaimTraining/list');
            }
            
            $data['applyClaimTraining'] = $this->apply_claim_training_model->getApplyClaim($id);
                $data['studentDetails'] = $this->apply_claim_training_model->getStudentByStudentId($data['applyClaimTraining']->id_student);

            // echo "<Pre>";print_r($data['researchApplyClaimHasExaminer']);exit;

            $this->global['pageTitle'] = 'Scholarship Student Portal : View ApplyClaim';
            $this->loadViews("apply_claim_training/view", $this->global, $data, NULL);

        }
    }

}

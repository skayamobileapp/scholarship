<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Repayment_model extends CI_Model
{
    function schemeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_scheme');
        $this->db->where('status', $status);
        $this->db->where('date(to_dt) >=', date('Y-m-d'));   
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function currencyListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('currency_setup');
        $this->db->where('status', $status); 
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function scholarshipListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_sub_thrust');
        $this->db->where('status', $status);
        $this->db->order_by("scholarship_name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }


    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function repaymentListSearch($data)
    {
        $this->db->select('sr.*,s.nric, s.full_name, p.code as programme_code, p.name as programme_name, ssr.scholarship_name, ssr.scholarship_code, sch.code as scheme_code, sch.name as scheme_name ');
        $this->db->from('repayment as sr');
        $this->db->join('student as s', 'sr.id_student = s.id');
        $this->db->join('scholarship_programme as p', 'sr.id_program = p.id');
        $this->db->join('scholarship_sub_thrust as ssr', 'sr.id_scholarship = ssr.id');
        $this->db->join('scholarship_scheme as sch', 'sr.id_cohert = sch.id');
         if ($data['name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['nric'] != '')
        {
            $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_programme'] != '')
        {
            $this->db->where('sr.id_program', $data['id_programme']);
        }
        if ($data['id_cohert'] != '')
        {
            $this->db->where('sr.id_cohert', $data['id_cohert']);
        }
        if ($data['id_scholarship'] != '')
        {
            $this->db->where('sr.id_scholarship', $data['id_scholarship']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('mi.status', $data['status']);
        }
        // $this->db->where('a.applicant_status !=', 'Graduated');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    

    function repaymentList()
    {
        $this->db->select('p.*');
        $this->db->from('repayment as p');
        $this->db->order_by("p.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getRepayment($id)
    {
        $this->db->select('p.*');
        $this->db->from('repayment as p');
        $this->db->where('p.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewRepayment($data)
    {
        $this->db->trans_start();
        $this->db->insert('repayment', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editRepayment($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('repayment', $data);
        return TRUE;
    }



    function getScholarshipByIdCohort($id_scheme)
    {
        $this->db->select('DISTINCT(icr.id_sub_thrust) as id_sub_thrust');
        $this->db->from('scheme_has_program as icr');
        $this->db->join('scholarship_programme as p', 'icr.id_program = p.id','left');
        $this->db->join('scholarship_sub_thrust as ichp', 'icr.id_sub_thrust = ichp.id','left');
        $this->db->where('icr.id_scholarship_scheme', $id_scheme);
        $this->db->where('icr.id_scholarship_scheme', $id_scheme);
        // $this->db->where('icr.status', '1');
        // $this->db->where('icr.to_dt <', date('Y-m-d'));
        $query = $this->db->get();
        $results = $query->result();

        $details = array();
        foreach ($results as $result)
        {
            $id_sub_thrust = $result->id_sub_thrust;
            $sub_thrust = $this->getSubThrust($id_sub_thrust);
            
            array_push($details, $sub_thrust);
        }

        return $details;
    }


    function getProgramByScholarshipNCohortId($data)
    {
        $this->db->select('DISTINCT(icr.id_program) as id_program');
        $this->db->from('scheme_has_program as icr');
        $this->db->join('scholarship_programme as p', 'icr.id_program = p.id','left');
        $this->db->join('scholarship_sub_thrust as ichp', 'icr.id_sub_thrust = ichp.id','left');
        $this->db->where('icr.id_scholarship_scheme', $data['id_cohert']);
        $this->db->where('icr.id_sub_thrust', $data['id_scholarship']);
        // $this->db->where('icr.status', '1');
        // $this->db->where('icr.to_dt <', date('Y-m-d'));
        $query = $this->db->get();
        $results = $query->result();

        $details = array();
        foreach ($results as $result)
        {
            $id_program = $result->id_program;
            $program = $this->getProgramData($id_program);
            
            array_push($details, $program);
        }

        return $details;
    }


    function getSubThrust($id)
    {
        $this->db->select('sa.*');
        // $this->db->join('scholarship_programme as sp', 'sa.id_program = sp.id');
        $this->db->from('scholarship_sub_thrust as sa');
        $this->db->where('sa.id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function getProgramData($id)
    {
        $this->db->select('sa.*');
        // $this->db->join('scholarship_programme as sp', 'sa.id_program = sp.id');
        $this->db->from('scholarship_programme as sa');
        $this->db->where('sa.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getStudentByData($data)
    {
        $this->db->select('sa.*');
        $this->db->from('student as sa');
        $this->db->where('sa.id_cohert', $data['id_cohert']);
        $this->db->where('sa.id_scholarship', $data['id_scholarship']);
        $this->db->where('sa.id_program', $data['id_program']);
        $query = $this->db->get();
        return $query->result();
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.code as programme_code, p.name as programme_name, ms.name as mailing_state, mc.name as mailing_country, ps.name as permanent_state, pc.name as permanent_country, rs.name as race, rels.name as religion, salut.name as salutation');
        $this->db->from('student as s');
        $this->db->join('scholarship_programme as p', 's.id_program = p.id','left'); 
        $this->db->join('salutation_setup as salut', 's.salutation = salut.id','left');
        $this->db->join('scholarship_state as ms', 's.mailing_state = ms.id','left'); 
        $this->db->join('scholarship_country as mc', 's.mailing_country = mc.id','left');
        $this->db->join('scholarship_state as ps', 's.permanent_state = ps.id','left'); 
        $this->db->join('scholarship_country as pc', 's.permanent_country = pc.id','left'); 
        $this->db->join('scholarship_race_setup as rs', 's.id_race = rs.id','left'); 
        $this->db->join('scholarship_religion_setup as rels', 's.religion = rels.id','left');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function addRepaymentExtension($data)
    {
        $this->db->trans_start();
        $this->db->insert('repayment_extension', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function repaymentExtensionByRepayment($id_repayment)
    {
        $this->db->select('p.*');
        $this->db->from('repayment_extension as p');
        $this->db->where('p.id_repayment', $id_repayment);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
}
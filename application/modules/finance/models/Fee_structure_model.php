<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Fee_structure_model extends CI_Model
{

    function schemeListByStatusForAdd($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_scheme');
        $this->db->where('status', $status);
        $this->db->where('date(to_dt) >=', date('Y-m-d'));   
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function currencyListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('currency_setup');
        $this->db->where('status', $status); 
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function thrustListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_thrust');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function schemeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_scheme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }


    function scholarshipListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_sub_thrust');
        $this->db->where('status', $status);
        $this->db->order_by("scholarship_name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }


    function feeStructureListSearch($data)
    {
        $this->db->select('sr.*, p.code as programme_code, p.name as programme_name, ssr.scholarship_name, ssr.scholarship_code, sch.code as scheme_code, sch.name as scheme_name ');
        $this->db->from('fee_structure_master as sr');
        $this->db->join('scholarship_programme as p', 'sr.id_program = p.id');
        $this->db->join('scholarship_sub_thrust as ssr', 'sr.id_scholarship = ssr.id');
        $this->db->join('scholarship_scheme as sch', 'sr.id_cohert = sch.id');
        //  if ($data['name'] != '')
        // {
        //     $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        // if ($data['nric'] != '')
        // {
        //     $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        if ($data['id_program'] != '')
        {
            $this->db->where('sr.id_program', $data['id_program']);
        }
        if ($data['id_cohert'] != '')
        {
            $this->db->where('sr.id_cohert', $data['id_cohert']);
        }
        if ($data['id_scholarship'] != '')
        {
            $this->db->where('sr.id_scholarship', $data['id_scholarship']);
        }
        // if ($data['status'] != '')
        // {
        //     $this->db->where('mi.status', $data['status']);
        // }
        // $this->db->where('a.applicant_status !=', 'Graduated');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

  function feeSetupListByStatus($status)
    {
        $this->db->select('fs.*, fm.name as frequency_mode');
        $this->db->from('fee_setup as fs');
        $this->db->join('scholarship_frequency_mode as fm', 'fs.id_frequency_mode = fm.id');        
        $this->db->where('fs.status', $status);
        $this->db->order_by("fs.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
    function subThrustListSearch($search)
    {
        $this->db->select('sst.*, c.code as thrust_code, c.name as thrust_name');
        $this->db->from('scholarship_sub_thrust as sst');
        $this->db->join('scholarship_thrust as c','sst.id_thrust = c.id');
        if ($search['name'] != '')
        {
            $likeCriteria = "(sst.scholarship_name  LIKE '%" . $search['name'] . "%' or sst.scholarship_code  LIKE '%" . $search['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($search['id_thrust'] != '')
        {
            $this->db->where('sst.id_thrust', $search['id_thrust']);
        }
        $this->db->order_by("sst.scholarship_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getFeeStructureByThrustNCurrency($id_sub_thrust,$currency)
    {
        $this->db->select('p.*, fs.name as fee_structure,fm.code as frequency_code, fm.name as frequency_mode, amt.code as amount_calculatoin_code');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id');   
        $this->db->join('scholarship_frequency_mode as fm', 'fs.id_frequency_mode = fm.id');
        $this->db->join('scholarship_amount_calculation_type as amt', 'fs.id_amount_calculation_type = amt.id');
        $this->db->where('p.id_sub_thrust', $id_sub_thrust);
        $this->db->where('p.currency', $currency);
        $this->db->order_by("fs.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getSubThrust($id)
    {
        $this->db->select('sst.*, c.code as thrust_code, c.name as thrust_name');
        $this->db->from('scholarship_sub_thrust as sst');
        $this->db->join('scholarship_thrust as c','sst.id_thrust = c.id');
        $this->db->where('sst.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function addNewFeeStructure($data)
    {
        // echo "<Pre>";  print_r($data);exit;

        $this->db->trans_start();
        $this->db->insert('fee_structure', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addNewFeeStructureMaster($data)
    {
        $this->db->trans_start();
        $this->db->insert('fee_structure_master', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function deleteFeeStructure($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('fee_structure');
        return TRUE;
    }


    function getScholarshipByIdCohort($id_scheme)
    {
        $this->db->select('DISTINCT(icr.id_sub_thrust) as id_sub_thrust');
        $this->db->from('scheme_has_program as icr');
        $this->db->join('scholarship_programme as p', 'icr.id_program = p.id','left');
        $this->db->join('scholarship_sub_thrust as ichp', 'icr.id_sub_thrust = ichp.id','left');
        $this->db->where('icr.id_scholarship_scheme', $id_scheme);
        $this->db->where('icr.id_scholarship_scheme', $id_scheme);
        // $this->db->where('icr.status', '1');
        // $this->db->where('icr.to_dt <', date('Y-m-d'));
        $query = $this->db->get();
        $results = $query->result();

        $details = array();
        foreach ($results as $result)
        {
            $id_sub_thrust = $result->id_sub_thrust;
            $sub_thrust = $this->getSubThrustData($id_sub_thrust);
            
            array_push($details, $sub_thrust);
        }

        return $details;
    }


    function getProgramByScholarshipNCohortId($data)
    {
        $this->db->select('DISTINCT(icr.id_program) as id_program');
        $this->db->from('scheme_has_program as icr');
        $this->db->join('scholarship_programme as p', 'icr.id_program = p.id','left');
        $this->db->join('scholarship_sub_thrust as ichp', 'icr.id_sub_thrust = ichp.id','left');
        $this->db->where('icr.id_scholarship_scheme', $data['id_cohert']);
        $this->db->where('icr.id_sub_thrust', $data['id_scholarship']);
        // $this->db->where('icr.status', '1');
        // $this->db->where('icr.to_dt <', date('Y-m-d'));
        $query = $this->db->get();
        $results = $query->result();

        $details = array();
        foreach ($results as $result)
        {
            $id_program = $result->id_program;
            $program = $this->getProgramData($id_program);
            
            array_push($details, $program);
        }

        return $details;
    }


    function getSubThrustData($id)
    {
        $this->db->select('sa.*');
        // $this->db->join('scholarship_programme as sp', 'sa.id_program = sp.id');
        $this->db->from('scholarship_sub_thrust as sa');
        $this->db->where('sa.id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function getProgramData($id)
    {
        $this->db->select('sa.*');
        // $this->db->join('scholarship_programme as sp', 'sa.id_program = sp.id');
        $this->db->from('scholarship_programme as sa');
        $this->db->where('sa.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getFeeStructure($id)
    {
        $this->db->select('sa.*');
        // $this->db->join('scholarship_programme as sp', 'sa.id_program = sp.id');
        $this->db->from('fee_structure_master as sa');
        $this->db->where('sa.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function editFeeStructureMaster($data,$id)
    {
        // echo "<Pre>";print_r($data);exit;

        $this->db->where('id', $id);
        $this->db->update('fee_structure_master',$data);
        return TRUE;
    }

    function feeStructureDetailsByMasterId($id)
    {
        $this->db->select('p.*, fs.name as fee_structure,fm.code as frequency_code, fm.name as frequency_mode, amt.code as amount_calculatoin_code');
        $this->db->from('fee_structure as p');
        $this->db->join('fee_setup as fs', 'p.id_fee_item = fs.id');   
        $this->db->join('scholarship_frequency_mode as fm', 'fs.id_frequency_mode = fm.id');
        $this->db->join('scholarship_amount_calculation_type as amt', 'fs.id_amount_calculation_type = amt.id');
        $this->db->where('p.id_fee_structure', $id);
        $this->db->order_by("fs.name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }
}
<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Apply_claim_miscellaneous_model extends CI_Model
{
    function applyClaimMiscellaneousListSearch($data)
    {
        $this->db->select('accl.*, s.full_name as student_name, s.nric, p.name as program_name, p.code as program_code');
        $this->db->from('apply_claim as accl');
        $this->db->join('student as s', 'accl.id_student = s.id');
        $this->db->join('scholarship_programme as p', 'accl.id_program = p.id','left');
        if($data['name'] != '')
        {
            $likeCriteria = "(accl.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['nric'] != '')
        {
            $likeCriteria = "(accl.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['type'] != '')
        {
            $this->db->where("accl.type", $data['type']);
        }
        if($data['id_program'] != '')
        {
            $this->db->where("accl.id_program", $data['id_program']);
        }

        if($data['status'] != '')
        {
            $this->db->where("accl.status", $data['status']);
        }
        $this->db->where("accl.type", 'Miscellaneouss');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
        $this->db->where("status", $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }


    function applyClaimListSearch($data)
    {
        $this->db->select('i.*');
        $this->db->from('apply_claim as i');
        // if($data['name'] != '')
        // {
        //     $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        // if($data['nric'] != '')
        // {
        //     $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        if($data['type'] != '')
        {
            $this->db->where("i.type", $data['type']);
        }
        if($data['id_student'] != '')
        {
            $this->db->where("i.id_student", $data['id_student']);
        }
        if($data['status'] != '')
        {
            $this->db->where("i.status", $data['status']);
        }
        // $this->db->order_by("i.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name');
        $this->db->from('student as s');
        $this->db->join('scholarship_programme as p', 's.id_program = p.id','left');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function addApplyClaim($data)
    {
        $this->db->trans_start();
        $this->db->insert('apply_claim', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getApplyClaim($id)
    {
         $this->db->select('i.*');
        $this->db->from('apply_claim as i');
        $this->db->where('i.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function editApplyClaim($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('apply_claim', $data);
        return TRUE;
    }
}
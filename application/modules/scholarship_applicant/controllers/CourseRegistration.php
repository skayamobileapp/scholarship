<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CourseRegistration extends BaseController
{
    public function __construct()
    {
        // $test = new BaseController();
        // $test->isStudentLoggedIn();
        parent::__construct();
        $this->load->model('course_registration_model');
        $this->load->model('edit_profile_model');
        $this->isStudentLoggedIn();
    }
    
    function add()
    {

        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_program = $this->session->id_program;
        $id_qualification = $this->session->id_qualification;

        if($this->input->post())
        {
            $id_course = $this->security->xss_clean($this->input->post('id_course'));

            $data = array(
                'id_course' => $id_course,
                'id_intake' => $id_intake,
                'id_programme' => $id_program,
                'id_student' => $id_student,
                'by_student' => 1
            );
                 // echo "<Pre>";print_r($data);exit();
            $insert_id = $this->course_registration_model->addCoureRegistration($data);
            redirect($_SERVER['HTTP_REFERER']);
        }

                 // echo "<Pre>";print_r($id_qualification);exit();
        $data['courseList'] = $this->course_registration_model->getPerogramLandscape($id_intake,$id_program);
        $data['getStudentData'] = $this->edit_profile_model->getStudentData($id_student);
        $data['courseRegisteredList'] = $this->course_registration_model->getCourseRegisteredList($id_student,$id_intake,$id_program,$id_qualification);
        // if(!empty($data['courseRegisteredList']))
        // {
        //     $$data['courseRegisteredList']->
        // }
        // echo "<Pre>";print_r($data['courseRegisteredList']);exit();

        $this->global['pageTitle'] = 'Student Portal : Add Course Registration';
        $this->loadViews("course_registration/add", $this->global, $data, NULL);
    }

     function deleteCourseRegistration($id_course_registration)
    {
        $student_list_data = $this->course_registration_model->deleteCourseRegistration($id_course_registration);
        echo $id_course_registration;
        exit();
    }

    function courseWithdraw()
    {
        $id_student = $this->session->id_student;
        $id_intake = $this->session->id_intake;
        $id_programme = $this->session->id_program;
        $user_id = $this->session->userId;

        if($this->input->post())
        {
            // $id_course = $this->security->xss_clean($this->input->post('id_course'));
            $reason = $this->security->xss_clean($this->input->post('reason'));
            $id_exam_register = $this->security->xss_clean($this->input->post('id_exam_register'));
            // $reason = $this->security->xss_clean($this->input->post('reason'));

                $exam_registration_data = $this->course_registration_model->getExamRegistration($id_exam_register);
            // echo "<Pre>";print_r($exam_registration_data);exit();

                 $data = array(
                        'id_exam_register' => $id_exam_register,
                        'id_exam_center' => $exam_registration_data->id_exam_center,
                        'id_course' => $exam_registration_data->id_course,
                        'id_semester' => $exam_registration_data->id_semester,
                        'id_student' => $exam_registration_data->id_student,
                        'id_intake' => $exam_registration_data->id_intake,
                        'id_programme' => $exam_registration_data->id_programme,
                        'reason' => $reason,
                        'status' => '0',
                        'created_by' => $user_id
                    );
                     // echo "<Pre>";print_r($data);exit();
                $insert_id = $this->course_registration_model->addBulkWithdraw($data);
                if ($insert_id)
                {
                    $update = array(
                        'is_bulk_withdraw' => $insert_id
                    );
                    $updated = $this->course_registration_model->updateExamRegistration($update,$id_exam_register);                       
                }
            redirect($_SERVER['HTTP_REFERER']);
        }

        $data['getStudentData'] = $this->edit_profile_model->getStudentData($id_student);
        $data['courseRegistrationList'] = $this->course_registration_model->getCourseFromExamRegister($id_intake,$id_programme,$id_student);
        $data['bulkWithdrawList'] = $this->course_registration_model->bulkWithdrawList($id_student);
        
                 // echo "<Pre>";print_r($data['courseRegistrationList']);exit();

        $this->global['pageTitle'] = 'Campus Management System : Add Student To Exam Center';
        $this->loadViews("course_registration/bulk_withdraw", $this->global, $data, NULL);
    }
}


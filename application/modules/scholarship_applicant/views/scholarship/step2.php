<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Scholaship Applicant Details</h3>
        </div>


    <div class="form-container">
            <h4 class="form-group-title">Applicant Details</h4>
            <div class='data-list'>
                <div class='row'> 
                    <div class='col-sm-6'>
                        <dl>
                            <dt>Applicant Name :</dt>
                            <dd>
                            <?php
                           
                                echo ucwords($profileDetails->full_name);
                            ?>
                                
                            </dd>
                        </dl>
                        <dl>
                            <dt>Applicant NRIC :</dt>
                            <dd>
                                <?php
                           
                                echo ucwords($profileDetails->nric);
                            ?>
                            
                            </dd>
                        </dl>
                                                
                    </div>        
                    
                    <div class='col-sm-6'> 
                         <dl>
                            <dt>Applicant Email :</dt>
                            <dd>
                            <?php
                           
                                echo ucwords($profileDetails->email_id);
                            ?>
                            </dd>
                        </dl>     
                        <dl>
                            <dt>Applicant Phone :</dt>
                            <dd>
                            <?php
                           
                                echo ucwords($profileDetails->phone);
                            ?>
                        </dd>
                        </dl>                         
                        
                    </div>
                </div>
            </div>
        </div>

        <form id="form_profile" action="" method="post" enctype="multipart/form-data">

            <div class="clearfix">
                <div class="page-title clearfix">
                    <h3>Student Registration</h3>
                </div>
                <div id="wizard" class="wizard">
                    <div class="wizard__content">
                      <header class="wizard__header">
                        <div class="wizard__steps">
                          <nav class="steps">
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="<?php echo '../step1/'. $id_application ?>" class="step__text">Profile Details</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -start"></div>
    
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="<?php echo '../step2/'. $id_application ?>" class="step__text">Contact Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="<?php echo '../step3/'. $id_application ?>" class="step__text">Education Details</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="<?php echo '../step4/'. $id_application ?>" class="step__text">Family Information</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="<?php echo '../step5/'. $id_application ?>" class="step__text">Financial Recoverable</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>

                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="<?php echo '../step6/'. $id_application ?>" class="step__text">File Upload</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>


                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="<?php echo '../step7/'. $id_application ?>" class="step__text">Employment Details</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>

    
                            <div class="step">
                              <div class="step__content">
                                <p class="step__number"></p>
                                <a href="<?php echo '../step8/'. $id_application ?>" class="step__text">Declaration Form</a>
                                <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
                                  <circle class="checkmark__circle" cx="26" cy="26" r="25" fill="none" />
                                  <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                </svg>
    
                                <div class="lines">
                                  <div class="line -background"></div>
    
                                  <div class="line -progress"></div>
                                </div>
                              </div>
                            </div>
                          </nav>
                        </div>
                      </header>
    
                      <div class="panels">
                        <div class="paneld">







                      <br>


                        <div class="form-container">
                            <h4 class="form-group-title">Mailing Address</h4>

                            <div class="row">



                                <div class="col-sm-4" id="is_mail_address1_view">
                                    <div class="form-group">
                                        <label>Mailing Address 1 <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="mail_address1" name="mail_address1" value="<?php 
                                        if(!empty($studentApplicatoinDetails->mail_address1))
                                        { 
                                            echo $studentApplicatoinDetails->mail_address1;
                                        } ?>">

                                        <input type="hidden" class="form-control" id="is_profile_update" name="is_profile_update" value="<?php 
                                        if(empty($studentApplicatoinDetails->first_name))
                                        { 
                                            echo 0; 
                                        }
                                        else
                                        {
                                            echo $studentApplicatoinDetails->id;
                                        } 
                                        ?>">

                                    
                                    </div>
                                </div>

                                <div class="col-sm-4" id="is_mail_address2_view">
                                    <div class="form-group">
                                        <label>Mailing Address 2 <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="mail_address2" name="mail_address2" value="<?php 
                                        if(!empty($studentApplicatoinDetails->mail_address2))
                                        { 
                                            echo $studentApplicatoinDetails->mail_address2;
                                        } ?>">
                                    </div>
                                </div>

                                <div class="col-sm-4" id="is_mailing_country_view">
                                    <div class="form-group">
                                        <label>Mailing Country <span class='error-text'>*</span></label>
                                        <select name="mailing_country" id="mailing_country" class="form-control" style="width: 405px;">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($countryList) && !empty($studentApplicatoinDetails))
                                            {
                                                foreach ($countryList as $record)
                                                {?>
                                             <option value="<?php echo $record->id;  ?>" <?php if($studentApplicatoinDetails->mailing_country==$record->id){ echo "selected"; } ?>>
                                                <?php echo $record->name;?>
                                             </option>
                                            <?php
                                                }
                                            }
                                            else
                                            if (!empty($countryList))
                                            {
                                                foreach ($countryList as $record)
                                                {?>
                                                    <option value="<?php echo $record->id;?>"
                                                    ><?php echo $record->name; ?>
                                                    </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                           <!--  </div>

                            <div class="row"> -->

                                <div class="col-sm-4" id="is_mailing_state_view">
                                    <div class="form-group">
                                        <label>Mailing State <span class='error-text'>*</span></label>
                                        <select name="mailing_state" id="mailing_state" class="form-control" style="width: 405px;">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($stateList) && !empty($studentApplicatoinDetails))
                                            {
                                                foreach ($stateList as $record)
                                                {?>
                                             <option value="<?php echo $record->id;  ?>" <?php if($studentApplicatoinDetails->mailing_state==$record->id){ echo "selected"; } ?>>
                                                <?php echo $record->name;?>
                                             </option>
                                            <?php
                                                }
                                            }
                                            else
                                            if (!empty($stateList))
                                            {
                                                foreach ($stateList as $record)
                                                {?>
                                                    <option value="<?php echo $record->id;?>"
                                                    ><?php echo $record->name; ?>
                                                    </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4" id="is_mailing_city_view">
                                    <div class="form-group">
                                        <label>Mailing City <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="mailing_city" name="mailing_city" value="<?php 
                                        if(!empty($studentApplicatoinDetails->mailing_city))
                                        { 
                                            echo $studentApplicatoinDetails->mailing_city;
                                        } ?>">
                                    </div>
                                </div>

                                <div class="col-sm-4" id="is_mailing_zipcode_view">
                                    <div class="form-group">
                                        <label>Mailing Zipcode <span class='error-text'>*</span></label>
                                        <input type="number" class="form-control" id="mailing_zipcode" name="mailing_zipcode" value="<?php 
                                        if(!empty($studentApplicatoinDetails->mailing_zipcode))
                                        { 
                                            echo $studentApplicatoinDetails->mailing_zipcode;
                                        } ?>">
                                    </div>
                                </div>
                            </div>

                        </div>

                        
                        <br>

                        <div class="form-container">
                            <h4 class="form-group-title">Permanent Address</h4>
                            
                            <div class="row">

                                <div class="col-sm-4" id="is_permanent_address1_view">
                                    <div class="form-group">
                                        <label>Permanent Address 1 <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="permanent_address1" name="permanent_address1" value="<?php 
                                        if(!empty($studentApplicatoinDetails->permanent_address1))
                                        { 
                                            echo $studentApplicatoinDetails->permanent_address1;
                                        } ?>">
                                    </div>
                                </div><div class="col-sm-4" id="is_permanent_address2_view">
                                    <div class="form-group">
                                        <label>Permanent Address 2 <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="permanent_address2" name="permanent_address2" value="<?php 
                                        if(!empty($studentApplicatoinDetails->permanent_address2))
                                        { 
                                            echo $studentApplicatoinDetails->permanent_address2;
                                        } ?>">
                                    </div>
                                </div>
                                <div class="col-sm-4" id="is_permanent_country_view">
                                    <div class="form-group">
                                        <label>Permanent Country <span class='error-text'>*</span></label>
                                        <select name="permanent_country" id="permanent_country" class="form-control" style="width: 405px;">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($countryList) && !empty($studentApplicatoinDetails))
                                            {
                                                foreach ($countryList as $record)
                                                {?>
                                             <option value="<?php echo $record->id;  ?>" <?php if($studentApplicatoinDetails->permanent_country==$record->id){ echo "selected"; } ?>>
                                                <?php echo $record->name;?>
                                             </option>
                                            <?php
                                                }
                                            }
                                            else
                                            if (!empty($countryList))
                                            {
                                                foreach ($countryList as $record)
                                                {?>
                                                    <option value="<?php echo $record->id;?>"
                                                    ><?php echo $record->name; ?>
                                                    </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                           <!--  </div>

                            <div class="row"> -->

                                <div class="col-sm-4" id="is_permanent_state_view">
                                    <div class="form-group">
                                        <label>Permanent State <span class='error-text'>*</span></label>
                                        <select name="permanent_state" id="permanent_state" class="form-control" style="width: 405px;">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($stateList) && !empty($studentApplicatoinDetails))
                                            {
                                                foreach ($stateList as $record)
                                                {?>
                                             <option value="<?php echo $record->id;  ?>" <?php if($studentApplicatoinDetails->permanent_state==$record->id){ echo "selected"; } ?>>
                                                <?php echo $record->name;?>
                                             </option>
                                            <?php
                                                }
                                            }
                                            else
                                            if (!empty($stateList))
                                            {
                                                foreach ($stateList as $record)
                                                {?>
                                                    <option value="<?php echo $record->id;?>"
                                                    ><?php echo $record->name; ?>
                                                    </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4" id="is_permanent_city_view">
                                    <div class="form-group">
                                        <label>Permanent City <span class='error-text'>*</span></label>
                                        <input type="text" class="form-control" id="permanent_city" name="permanent_city" value="<?php 
                                        if(!empty($studentApplicatoinDetails->permanent_city))
                                        { 
                                            echo $studentApplicatoinDetails->permanent_city;
                                        } ?>">
                                    </div>
                                </div>

                                <div class="col-sm-4" id="is_permanent_zipcode_view">
                                    <div class="form-group">
                                        <label>Permanent Zipcode <span class='error-text'>*</span></label>
                                        <input type="number" class="form-control" id="permanent_zipcode" name="permanent_zipcode" value="<?php 
                                        if(!empty($studentApplicatoinDetails->permanent_zipcode))
                                        { 
                                            echo $studentApplicatoinDetails->permanent_zipcode;
                                        } ?>">
                                    </div>
                                </div>
                            </div>

                        </div>








                        </div>
                      </div>




                       <div class="wizard__footer">
                        <a href="list" class="btn btn-link mr-3">Cancel</a>
                        <button class="btn btn-primary next" type="submit">Save & Continue</button>
                      </div>

    
                     
                    </div>
    
                    <h2 class="wizard__congrats-message">
                      Congratulations!!
                    </h2>
                  </div>



                
               

                <footer class="footer-wrapper">
                    <p>&copy; 2019 All rights, reserved</p>
                </footer>
                
            </div>        
            
    </form>
      

    </div>
</div>




<script type="text/javascript">

    function getRequirementsByProgramId(id_scholarship_program)
    {
        // alert(id_scholarship_program);
        if(id_scholarship_program != '')
        {
            $.get("/scholarship_applicant/scholarshipApplication/getRequirementsByIdProgram/"+id_scholarship_program, function(data, status){
           
                $("#view_data").html(data);
                // $("#view_data").show();

                var is_financial_recoverable_details = $("#financial_recoverable_details").val();
                var is_education_details = $("#education_details").val();
                var is_family_details = $("#family_details").val();
                var is_file_upload_details = $("#file_upload_details").val();
                var is_employment_details = $("#employment_details").val();



                var is_salutation = $("#is_salutation").val();
                var is_first_name = $("#is_first_name").val();
                var is_last_name = $("#is_last_name").val();
                var is_nric = $("#is_nric").val();
                var is_passport = $("#is_passport").val();
                var is_phone = $("#is_phone").val();
                var is_email_id = $("#is_email_id").val();
                var is_passport_expiry_date = $("#is_passport_expiry_date").val();
                var is_gender = $("#is_gender").val();
                var is_date_of_birth = $("#is_date_of_birth").val();
                var is_martial_status = $("#is_martial_status").val();
                var is_religion = $("#is_religion").val();
                var is_nationality = $("#is_nationality").val();
                var is_id_race = $("#is_id_race").val();
                var is_mail_address1 = $("#is_mail_address1").val();
                var is_mail_address2 = $("#is_mail_address2").val();
                var is_mailing_country = $("#is_mailing_country").val();
                var is_mailing_state = $("#is_mailing_state").val();
                var is_mailing_city = $("#is_mailing_city").val();
                var is_mailing_zipcode = $("#is_mailing_zipcode").val();
                var is_permanent_address1 = $("#is_permanent_address1").val();
                var is_permanent_address2 = $("#is_permanent_address2").val();
                var is_permanent_country = $("#is_permanent_country").val();
                var is_permanent_state = $("#is_permanent_state").val();
                var is_permanent_city = $("#is_permanent_city").val();
                var is_permanent_zipcode = $("#is_permanent_zipcode").val();
                var is_id_type = $("#is_id_type").val();
                var is_passport_number = $("#is_passport_number").val();

                var is_qualification_level = $("#is_qualification_level").val();
                var is_degree_awarded = $("#is_degree_awarded").val();
                var is_specialization = $("#is_specialization").val();
                var is_class_degree = $("#is_class_degree").val();
                var is_result = $("#is_result").val();
                var is_year = $("#is_year").val();
                var is_medium = $("#is_medium").val();
                var is_college_country = $("#is_college_country").val();
                var is_college_name = $("#is_college_name").val();
                var is_certificate = $("#is_certificate").val();
                var is_transcript = $("#is_transcript").val();

                var is_father_name = $("#is_father_name").val();
                var is_mother_name = $("#is_mother_name").val();
                var is_father_deceased = $("#is_father_deceased").val();
                var is_father_occupation = $("#is_father_occupation").val();
                var is_no_siblings = $("#is_no_siblings").val();
                var is_est_fee = $("#is_est_fee").val();
                var is_family_annual_income = $("#is_family_annual_income").val();

                var is_company_name = $("#is_company_name").val();
                var is_company_address = $("#is_company_address").val();
                var is_telephone = $("#is_telephone").val();
                var is_fax_num = $("#is_fax_num").val();
                var is_designation = $("#is_designation").val();
                var is_position = $("#is_position").val();
                var is_service_year = $("#is_service_year").val();
                var is_industry = $("#is_industry").val();
                var is_job_desc = $("#is_job_desc").val();
                var is_employment_letter = $("#is_employment_letter").val();

                // alert(is_first_name);

                // alert(is_financial_recoverable_details);
                if(is_financial_recoverable_details == 0)
                {
                    $("#is_financial_recoverable_details").hide();
                }
                else if(is_financial_recoverable_details == 1)
                {
                    $("#is_financial_recoverable_details").show();
                }


                if(is_education_details == 0)
                {
                    $("#is_education_details").hide();
                }else
                if(is_education_details == 1)
                {
                    $("#is_education_details").show();
                }


                if(is_family_details == 0)
                {
                    $("#is_family_details").hide();
                }
                else if(is_family_details == 1)
                {
                    $("#is_family_details").show();
                }


                if(is_file_upload_details == 0)
                {
                    $("#is_file_upload_details").hide();
                }
                else if(is_file_upload_details == 1)
                {
                    $("#is_file_upload_details").show();
                }


                if(is_employment_details == 0)
                {
                    $("#is_employment_details").hide();
                }
                else if(is_employment_details == 1)
                {
                    $("#is_employment_details").show();
                }


                if(is_salutation == 0)
                {
                    $("#is_salutation_view").hide();
                }
                else if(is_salutation == 1)
                {
                    $("#is_salutation_view").show();
                }


                if(is_first_name == 0)
                {
                    $("#is_first_name_view").hide();
                }
                else if(is_first_name == 1)
                {
                    $("#is_first_name_view").show();
                }


                if(is_last_name == 0)
                {
                    $("#is_last_name_view").hide();
                }
                else if(is_last_name == 1)
                {
                    $("#is_last_name_view").show();
                }


                if(is_nric == 0)
                {
                    $("#is_nric_view").hide();
                }
                else if(is_nric == 1)
                {
                    $("#is_nric_view").show();
                }


                if(is_passport == 0)
                {
                    $("#is_passport_view").hide();
                }
                else if(is_passport == 1)
                {
                    $("#is_passport_view").show();
                }


                if(is_phone == 0)
                {
                    $("#is_phone_view").hide();
                }
                else if(is_phone == 1)
                {
                    $("#is_phone_view").show();
                }


                if(is_email_id == 0)
                {
                    $("#is_email_id_view").hide();
                }
                else if(is_email_id == 1)
                {
                    $("#is_email_id_view").show();
                }


                if(is_passport_expiry_date == 0)
                {
                    $("#is_passport_expiry_date_view").hide();
                }
                else if(is_passport_expiry_date == 1)
                {
                    $("#is_passport_expiry_date_view").show();
                }


                if(is_gender == 0)
                {
                    $("#is_gender_view").hide();
                }
                else if(is_gender == 1)
                {
                    $("#is_gender_view").show();
                }


                if(is_date_of_birth == 0)
                {
                    $("#is_date_of_birth_view").hide();
                }
                else if(is_date_of_birth == 1)
                {
                    $("#is_date_of_birth_view").show();
                }


                if(is_martial_status == 0)
                {
                    $("#is_martial_status_view").hide();
                }
                else if(is_martial_status == 1)
                {
                    $("#is_martial_status_view").show();
                }


                if(is_religion == 0)
                {
                    $("#is_religion_view").hide();
                }
                else if(is_religion == 1)
                {
                    $("#is_religion_view").show();
                }


                if(is_nationality == 0)
                {
                    $("#is_nationality_view").hide();
                }
                else if(is_nationality == 1)
                {
                    $("#is_nationality_view").show();
                }


                if(is_id_race == 0)
                {
                    $("#is_id_race_view").hide();
                }
                else if(is_id_race == 1)
                {
                    $("#is_id_race_view").show();
                }


                if(is_mail_address1 == 0)
                {
                    $("#is_mail_address1_view").hide();
                }
                else if(is_mail_address1 == 1)
                {
                    $("#is_mail_address1_view").show();
                }


                if(is_mail_address2 == 0)
                {
                    $("#is_mail_address2_view").hide();
                }
                else if(is_mail_address2 == 1)
                {
                    $("#is_mail_address2_view").show();
                }


                if(is_mailing_country == 0)
                {
                    $("#is_mailing_country_view").hide();
                }
                else if(is_mailing_country == 1)
                {
                    $("#is_mailing_country_view").show();
                }


                if(is_mailing_state == 0)
                {
                    $("#is_mailing_state_view").hide();
                }
                else if(is_mailing_state == 1)
                {
                    $("#is_mailing_state_view").show();
                }


                if(is_mailing_city == 0)
                {
                    $("#is_mailing_city_view").hide();
                }
                else if(is_mailing_city == 1)
                {
                    $("#is_mailing_city_view").show();
                }


                if(is_mailing_zipcode == 0)
                {
                    $("#is_mailing_zipcode_view").hide();
                }
                else if(is_mailing_zipcode == 1)
                {
                    $("#is_mailing_zipcode_view").show();
                }


                if(is_permanent_address1 == 0)
                {
                    $("#is_permanent_address1_view").hide();
                }
                else if(is_permanent_address1 == 1)
                {
                    $("#is_permanent_address1_view").show();
                }


                if(is_permanent_address2 == 0)
                {
                    $("#is_permanent_address2_view").hide();
                }
                else if(is_permanent_address2 == 1)
                {
                    $("#is_permanent_address2_view").show();
                }


                if(is_permanent_country == 0)
                {
                    $("#is_permanent_country_view").hide();
                }
                else if(is_permanent_country == 1)
                {
                    $("#is_permanent_country_view").show();
                }


                if(is_permanent_state == 0)
                {
                    $("#is_permanent_state_view").hide();
                }
                else if(is_permanent_state == 1)
                {
                    $("#is_permanent_state_view").show();
                }


                if(is_permanent_city == 0)
                {
                    $("#is_permanent_city_view").hide();
                }
                else if(is_permanent_city == 1)
                {
                    $("#is_permanent_city_view").show();
                }


                if(is_permanent_zipcode == 0)
                {
                    $("#is_permanent_zipcode_view").hide();
                }
                else if(is_permanent_zipcode == 1)
                {
                    $("#is_permanent_zipcode_view").show();
                }


                if(is_id_type == 0)
                {
                    $("#is_id_type_view").hide();
                }
                else if(is_id_type == 1)
                {
                    $("#is_id_type_view").show();
                }


                if(is_passport_number == 0)
                {
                    $("#is_passport_number_view").hide();
                }
                else if(is_passport_number == 1)
                {
                    $("#is_passport_number_view").show();
                }


                if(is_qualification_level == 0)
                {
                    $("#is_qualification_level_view").hide();
                }
                else if(is_qualification_level == 1)
                {
                    $("#is_qualification_level_view").show();
                }


                if(is_degree_awarded == 0)
                {
                    $("#is_degree_awarded_view").hide();
                }
                else if(is_degree_awarded == 1)
                {
                    $("#is_degree_awarded_view").show();
                }



                if(is_specialization == 0)
                {
                    $("#is_specialization_view").hide();
                }
                else if(is_specialization == 1)
                {
                    $("#is_specialization_view").show();
                }


                if(is_class_degree == 0)
                {
                    $("#is_class_degree_view").hide();
                }
                else if(is_class_degree == 1)
                {
                    $("#is_class_degree_view").show();
                }


                if(is_result == 0)
                {
                    $("#is_result_view").hide();
                }
                else if(is_result == 1)
                {
                    $("#is_result_view").show();
                }


                if(is_year == 0)
                {
                    $("#is_year_view").hide();
                }
                else if(is_year == 1)
                {
                    $("#is_year_view").show();
                }


                if(is_medium == 0)
                {
                    $("#is_medium_view").hide();
                }
                else if(is_medium == 1)
                {
                    $("#is_medium_view").show();
                }


                if(is_college_country == 0)
                {
                    $("#is_college_country_view").hide();
                }
                else if(is_college_country == 1)
                {
                    $("#is_college_country_view").show();
                }


                if(is_college_name == 0)
                {
                    $("#is_college_name_view").hide();
                }
                else if(is_college_name == 1)
                {
                    $("#is_college_name_view").show();
                }


                if(is_certificate == 0)
                {
                    $("#is_certificate_view").hide();
                }
                else if(is_certificate == 1)
                {
                    $("#is_certificate_view").show();
                }


                if(is_transcript == 0)
                {
                    $("#is_transcript_view").hide();
                }
                else if(is_transcript == 1)
                {
                    $("#is_transcript_view").show();
                }


                if(is_father_name == 0)
                {
                    $("#is_father_name_view").hide();
                }
                else if(is_father_name == 1)
                {
                    $("#is_father_name_view").show();
                }


                if(is_mother_name == 0)
                {
                    $("#is_mother_name_view").hide();
                }
                else if(is_mother_name == 1)
                {
                    $("#is_mother_name_view").show();
                }


                if(is_father_deceased == 0)
                {
                    $("#is_father_deceased_view").hide();
                }
                else if(is_father_deceased == 1)
                {
                    $("#is_father_deceased_view").show();
                }


                if(is_father_occupation == 0)
                {
                    $("#is_father_occupation_view").hide();
                }
                else if(is_father_occupation == 1)
                {
                    $("#is_father_occupation_view").show();
                }


                if(is_no_siblings == 0)
                {
                    $("#is_no_siblings_view").hide();
                }
                else if(is_no_siblings == 1)
                {
                    $("#is_no_siblings_view").show();
                }


                if(is_est_fee == 0)
                {
                    $("#is_est_fee_view").hide();
                }
                else if(is_est_fee == 1)
                {
                    $("#is_est_fee_view").show();
                }


                if(is_family_annual_income == 0)
                {
                    $("#is_family_annual_income_view").hide();
                }
                else if(is_family_annual_income == 1)
                {
                    $("#is_family_annual_income_view").show();
                }


                if(is_company_name == 0)
                {
                    $("#is_company_name_view").hide();
                }
                else if(is_company_name == 1)
                {
                    $("#is_company_name_view").show();
                }


                if(is_company_address == 0)
                {
                    $("#is_company_address_view").hide();
                }
                else if(is_company_address == 1)
                {
                    $("#is_company_address_view").show();
                }


                if(is_telephone == 0)
                {
                    $("#is_telephone_view").hide();
                }
                else if(is_telephone == 1)
                {
                    $("#is_telephone_view").show();
                }


                if(is_fax_num == 0)
                {
                    $("#is_fax_num_view").hide();
                }
                else if(is_fax_num == 1)
                {
                    $("#is_fax_num_view").show();
                }


                if(is_designation == 0)
                {
                    $("#is_designation_view").hide();
                }
                else if(is_designation == 1)
                {
                    $("#is_designation_view").show();
                }


                if(is_position == 0)
                {
                    $("#is_position_view").hide();
                }
                else if(is_position == 1)
                {
                    $("#is_position_view").show();
                }


                if(is_service_year == 0)
                {
                    $("#is_service_year_view").hide();
                }
                else if(is_service_year == 1)
                {
                    $("#is_service_year_view").show();
                }


                if(is_industry == 0)
                {
                    $("#is_industry_view").hide();
                }
                else if(is_industry == 1)
                {
                    $("#is_industry_view").show();
                }


                if(is_job_desc == 0)
                {
                    $("#is_job_desc_view").hide();
                }
                else if(is_job_desc == 1)
                {
                    $("#is_job_desc_view").show();
                }


                if(is_employment_letter == 0)
                {
                    $("#is_employment_letter_view").hide();
                }
                else if(is_employment_letter == 1)
                {
                    $("#is_employment_letter_view").show();
                }

                // hideShow();
            });
        }
    }



    function hideShow()
    {

    }



    $('select').select2();

    $( function()
    {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1920:2019"
    });
    });


    function deleteEducationDetails(id)
    {
        $.ajax(
        {
           url: '/scholarship_applicant/editProfile/deleteEducationDetails/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            // alert(result);
                window.location.reload();
           }
        });
    }


    function deleteEmploymentDetails(id)
    {
         $.ajax(
        {
           url: '/scholarship_applicant/editProfile/deleteEmploymentDetails/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            // alert(result);
                window.location.reload();
           }
        });
    }

    function submitApp()
    {
        if($('#form_profile').valid())
        {    
            $('#id_program').prop('disabled', false);
            $('#form_profile').submit();
        }
    }




    $(document).ready(function()
    {
        $("#form_profile").validate({
            rules: {
                qualification_level: {
                    required: true
                },
                degree_awarded: {
                    required: true
                },
                specialization: {
                    required: true
                },
                class_degree: {
                    required: true
                },
                result: {
                    required: true
                },
                year: {
                    required: true
                },
                medium: {
                    required: true
                },
                college_country: {
                    required: true
                },
                college_name: {
                    required: true
                },
                test: {
                    required: true
                },
                date: {
                    required: true
                },
                score: {
                    required: true
                },
                company_name: {
                    required: true
                },
                company_address: {
                    required: true
                },
                telephone: {
                    required: true
                },
                fax_num: {
                    required: true
                },
                designation: {
                    required: true
                },
                position: {
                    required: true
                },
                service_year: {
                    required: true
                },
                industry: {
                    required: true
                },
                job_desc: {
                    required: true
                },
                salutation: {
                    required: true
                },
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                id_type: {
                    required: true
                },
                passport_number: {
                    required: true
                },
                passport_expiry_date: {
                    required: true
                },
                phone: {
                    required: true
                },
                gender: {
                    required: true
                },
                date_of_birth: {
                    required: true
                },
                martial_status: {
                    required: true
                },
                religion: {
                    required: true
                },
                nationality: {
                    required: true
                },
                id_race: {
                    required: true
                },
                email_id: {
                    required: true
                },
                nric: {
                    required: true
                },
                mail_address1: {
                    required: true
                },
                mail_address2: {
                    required: true
                },
                mailing_country: {
                    required: true
                },
                mailing_state: {
                    required: true
                },
                mailing_city: {
                    required: true
                },
                mailing_zipcode: {
                    required: true
                },
                permanent_address1: {
                    required: true
                },
                permanent_address2: {
                    required: true
                },
                permanent_country: {
                    required: true
                },
                permanent_state: {
                    required: true
                },
                permanent_city: {
                    required: true
                },
                permanent_zipcode: {
                    required: true
                },
                malaysian_visa: {
                    required: true
                },
                visa_expiry_date: {
                    required: true
                },
                visa_status: {
                    required: true
                },
                doc_name: {
                    required: true
                },
                remarks: {
                    required: true
                },
                id_program: {
                    Required: true
                },
                certificate: {
                    Required: true
                }
            },
            messages: {
                qualification_level: {
                    required: "<p class='error-text'>Qualification Level Required</p>",
                },
                degree_awarded: {
                    required: "<p class='error-text'>Degree Awarded Required</p>",
                },
                specialization: {
                    required: "<p class='error-text'>Specialization Required</p>",
                },
                class_degree: {
                    required: "<p class='error-text'>Degree Class Required</p>",
                },
                result: {
                    required: "<p class='error-text'>Result Required</p>",
                },
                year: {
                    required: "<p class='error-text'>Year Requred</p>",
                },
                medium: {
                    required: "<p class='error-text'>Select Medium Of Study</p>",
                },
                college_country: {
                    required: "<p class='error-text'>Country Required</p>",
                },
                college_name: {
                    required: "<p class='error-text'>College Name Required</p>",
                },
                test: {
                    required: "<p class='error-text'>Test Name Required</p>",
                },
                date: {
                    required: "<p class='error-text'>Select Date</p>",
                },
                score: {
                    required: "<p class='error-text'>Score Required</p>",
                },
                company_name: {
                    required: "<p class='error-text'>Company Name Required</p>",
                },
                company_address: {
                    required: "<p class='error-text'>Company Address Required</p>",
                },
                telephone: {
                    required: "<p class='error-text'>Telephone No. Required</p>",
                },
                fax_num: {
                    required: "<p class='error-text'>Fax No. Required</p>",
                },
                designation: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                position: {
                    required: "<p class='error-text'>Position Of Rhe Job Required</p>",
                },
                service_year: {
                    required: "<p class='error-text'>No. Of Service Required</p>",
                },
                industry: {
                    required: "<p class='error-text'>Industry Required</p>",
                },
                job_desc: {
                    required: "<p class='error-text'>Job Description Required</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name Required</p>",
                },
                id_type: {
                    required: "<p class='error-text'>ID Type Required</p>",
                },
                passport_number: {
                    required: "<p class='error-text'>Passport NUmber Required</p>",
                },
                passport_expiry_date: {
                    required: "<p class='error-text'>Select Passport Expire Date</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone Number Required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Select Gender</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                martial_status: {
                    required: "<p class='error-text'>Select Maritual Status</p>",
                },
                religion: {
                    required: "<p class='error-text'>Select Religion</p>",
                },
                nationality: {
                    required: "<p class='error-text'>Select Ntionality</p>",
                },
                id_race: {
                    required: "<p class='error-text'>Select Rece</p>",
                },
                email_id: {
                    required: "<p class='error-text'>Main Id Required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC Required</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Mailing Address 1 Required</p>",
                },
                mail_address2: {
                    required: "<p class='error-text'>Mailing Address 2 Required</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Mailing Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>Select Mailing State</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Mailing City Required</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Mailing Zipcode Required</p>",
                },
                permanent_address1: {
                    required: "<p class='error-text'>Select Permanent Address 2</p>",
                },
                permanent_address2: {
                    required: "<p class='error-text'>Select Permanent Address 2</p>",
                },
                permanent_country: {
                    required: "<p class='error-text'>Select Permanent Country</p>",
                },
                permanent_state: {
                    required: "<p class='error-text'>Select Permanent State</p>",
                },
                permanent_city: {
                    required: "<p class='error-text'>Permanent City Required</p>",
                },
                permanent_zipcode: {
                    required: "<p class='error-text'>Permanent Zipcode Required</p>",
                },
                malaysian_visa: {
                    required: "<p class='error-text'>Select Visa Type</p>",
                },
                visa_expiry_date: {
                    required: "<p class='error-text'>Select Visa Expire Date</p>",
                },
                visa_status: {
                    required: "<p class='error-text'>Visa Status Required</p>",
                },
                doc_name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                remarks: {
                    required: "<p class='error-text'>Remars Required</p>"
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>"
                },
                certificate: {
                    required: "<p class='error-text'>Select Certificate</p>"
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    

  </script>
            <link rel="stylesheet" href="<?php echo BASE_PATH; ?>/assets/css/wizard.css">

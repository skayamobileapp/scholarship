<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Scholarship Application </h3>
            <a href="../list" class="btn btn-link">Back</a>
        </div>

<form id="form_profile" action="" method="post">


    <div class="form-container">
            <h4 class="form-group-title">Applicant Details</h4>
            <div class='data-list'>
                <div class='row'> 
                    <div class='col-sm-6'>
                        <dl>
                            <dt>Applicant Name :</dt>
                            <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                        </dl>
                        <dl>
                            <dt>Applicant NRIC :</dt>
                            <dd><?php echo $studentDetails->nric ?></dd>
                        </dl>
                        
                                                
                    </div>        
                    
                    <div class='col-sm-6'> 
                        <dl>
                            <dt>Applicant Email :</dt>
                            <dd><?php echo $studentDetails->email_id; ?></dd>
                        </dl>
                        <dl>
                            <dt>Mobile Number :</dt>
                            <dd><?php echo $studentDetails->phone ?></dd>
                        </dl>                         
                        <!-- <dl>
                            <dt>Intake :</dt>
                            <dd><?php echo $studentDetails->intake_name ?></dd>
                        </dl>
                        
                        <dl>
                            <dt>Qualification Type :</dt>
                            <dd><?php echo $studentDetails->qualification_code . " - " . $studentDetails->qualification_name; ?></dd>
                        </dl>
                        <dl>
                            <dt>Scholarship :</dt>
                            <dd><?php echo ""; ?></dd>
                        </dl> -->
                    </div>
                </div>
            </div>
        </div>

        <br>


        <div class="form-container">
            <h4 class="form-group-title">Application Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Cohert Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $scheme->code; ?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Cohert Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $scheme->name; ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Scheme End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="to_dt" name="to_dt" value="<?php echo date('d-m-Y',strtotime($scheme->to_dt)) ?>" readonly>
                    </div>
                </div>


            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Application Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="application_number" name="application_number" min="1" max="100" value="<?php echo $scholarship->application_number; ?>" readonly>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Application Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" value="<?php echo date('d-m-Y',strtotime($scholarship->created_dt_tm)) ?>" readonly>
                    </div>
                </div>

                

               <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php
                        if($studentApplicatoinDetails->is_submitted == '0')
                        {
                            echo "Draft";
                        }
                        elseif($studentApplicatoinDetails->status == '0')
                        {
                            echo "Pending";
                        }
                        elseif($studentApplicatoinDetails->status > 0)
                        {
                            echo $studentApplicatoinDetails->status_;
                        }
                        // elseif($studentApplicatoinDetails->status == '2')
                        // {
                        //     echo "Rejected";
                        // }
                         ?>" readonly="readonly">
                    </div>
                </div>
                
            </div>

            <div class="row">

                <?php
                if($scholarship->status == '2')
                {
                 ?>


                   <!--  <div class="col-sm-4" id="view_reject">
                        <div class="form-group">
                            <label>Reject Reason <span class='error-text'>*</span></label>
                            <input type="text" id="reason" name="reason" class="form-control" value="<?php echo $scholarship->reason; ?>" readonly>
                        </div>
                    </div> -->

                <?php
                }
                ?>

            </div>

                

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group pull-right">
            <!-- <a href="../schemeList" class="btn btn-link">Back</a> -->
            <!-- <button type="submit" class="btn btn-primary btn-lg" >Submit Application</button> -->
                <!-- <button type="submit" class="btn btn-primary btn-lg" >Save</button> -->
            </div>
        </div>




    <br>
    
    
  
    
        <div class="m-auto text-center">
           <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
         </div>
        <div class="clearfix">
        <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
            <li role="presentation" class="active"><a href="#education" class="nav-link border rounded text-center"
                    aria-controls="education" aria-selected="true"
                    role="tab" data-toggle="tab">Personal Details</a>
            </li>
            <li role="presentation"><a href="#contact_details" class="nav-link border rounded text-center"
                    aria-controls="contact_details" role="tab" data-toggle="tab">Contact Details</a>
            </li>
            <li role="presentation"><a href="#proficiency" class="nav-link border rounded text-center"
                    aria-controls="proficiency" role="tab" data-toggle="tab">Education Details</a>
            </li>
            <li role="presentation"><a href="#employment" class="nav-link border rounded text-center"
                    aria-controls="employment" role="tab" data-toggle="tab">Family Information</a>
            </li>
            <li role="presentation"><a href="#profile" class="nav-link border rounded text-center"
                    aria-controls="profile" role="tab" data-toggle="tab">Financial Recoverable</a>
            </li>
            <li role="presentation"><a href="#visa" class="nav-link border rounded text-center"
                    aria-controls="visa" role="tab" data-toggle="tab">File Upload</a>
            </li>
            <li role="presentation"><a href="#other" class="nav-link border rounded text-center"
                    aria-controls="other" role="tab" data-toggle="tab">Employment Details</a>
            </li>
            <li role="presentation"><a href="#decleration" class="nav-link border rounded text-center"
                    aria-controls="decleration" role="tab" data-toggle="tab">Applicant Decleration</a>
            </li>
            <li role="presentation"><a href="#approval_details" class="nav-link border rounded text-center"
                    aria-controls="approval_details" role="tab" data-toggle="tab">Approval Details</a>
            </li>
        </ul>



        <div class="tab-content offers-tab-content">


            <div role="tabpanel" class="tab-pane active" id="education">
                <div class="col-12 mt-4">
                    <br>

                 <div class="form-container">
                    <h4 class="form-group-title">Profile Details</h4>

                        <div class="row">

                            <div class="col-sm-4">
                            <div class="form-group">
                                <label>Salutation <span class='error-text'>*</span></label>
                                <select name="salutation" id="salutation" class="form-control" style="width: 405px;" disabled>
                                    <option value="">Select</option>
                                    <option value="Miss" <?php if($profileDetails->salutation=='Miss'){ echo "selected"; } ?>>Miss</option>
                                    <option value="Mr" <?php if($profileDetails->salutation=='Mr'){ echo "selected"; } ?>>Mr</option>
                                    <option value="Mrs" <?php if($profileDetails->salutation=='Mrs'){ echo "selected"; } ?>>Mrs</option>
                                    <option value="Dr" <?php if($profileDetails->salutation=='Dr'){ echo "selected"; } ?>>Dr</option>
                                </select>
                            </div>
                            </div>



                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>First Name <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" value="<?php if(!empty($profileDetails->first_name)){ echo $profileDetails->first_name; } ?>" readonly>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Last Name <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="last_name" name="last_name" value="<?php if(!empty($profileDetails->last_name)){ echo $profileDetails->last_name; } ?>" readonly>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>ID Type <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="id_type" name="id_type" value="<?php if(!empty($profileDetails->id_type)){ echo $profileDetails->id_type; } ?>" readonly>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>MyKad Passport National ID Military ID/Police ID MyPR ID Number <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="passport_number" name="passport_number" value="<?php if(!empty($profileDetails->passport_number)){ echo $profileDetails->passport_number; } ?>" readonly>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Passport Expiry Date <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control datepicker" autocomplete="off" id="passport_expiry_date" name="passport_expiry_date" value="<?php if(!empty($profileDetails->passport_expiry_date)){ echo date('d-m-Y', strtotime($profileDetails->passport_expiry_date)); } ?>" readonly>
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Phone Number <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $profileDetails->phone ?>" readonly>
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Gender <span class='error-text'>*</span></label>
                                    <select class="form-control" id="gender" name="gender" style="width: 405px;" disabled>
                                        <option value="">SELECT</option>
                                        <option value="Male" <?php if($profileDetails->gender == "Male"){ echo "selected=selected"; } ?>>MALE</option>
                                        <option value="Female" <?php if($profileDetails->gender == "Female"){ echo "selected=selected"; } ?>>FEMALE</option>
                                        <!-- <option value="Others" <?php if($profileDetails->gender == "Others"){ echo "selected=selected"; } ?>>OTHERS</option> -->
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Of Birth <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" value="<?php if(!empty($profileDetails->date_of_birth)){ echo date('d-m-Y', strtotime($profileDetails->date_of_birth)); } ?>" autocomplete="off" readonly>
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Martial Status <span class='error-text'>*</span></label>
                                    <select id="martial_status" name="martial_status" class="form-control" style="width: 405px;" disabled>
                                        <option value="">SELECT</option>
                                        <option value="Single" <?php if($profileDetails->martial_status=='Single'){ echo "selected"; } ?>>SINGLE</option>
                                        <option value="Married" <?php if($profileDetails->martial_status=='Married'){ echo "selected"; } ?>>MARRIED</option>
                                        <option value="Divorced" <?php if($profileDetails->martial_status=='Divorced'){ echo "selected"; } ?>>DIVORCED</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Religion <span class='error-text'>*</span></label>
                                    <select name="religion" id="religion" class="form-control" style="width: 405px;" disabled>
                                        <option value="">Select</option>
                                        <?php
                                        if (!empty($religionList))
                                        {
                                            foreach ($religionList as $record)
                                            {?>
                                                <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $profileDetails->religion)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?>
                                                </option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>



                            <div class="col-sm-4">
                                <div class="form-group">
                                <label>Type Of Nationality <span class='error-text'>*</span></label>

                                 <select name="nationality" id="nationality" class="form-control" style="width: 405px;" disabled>
                                    <option value="">Select</option>
                                    <option value="<?php echo 'Malaysian';?>"
                                        <?php 
                                        if ($profileDetails->nationality == 'Malaysian')
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                                <?php echo "Malaysian";  ?>
                                    </option>

                                    <option value="<?php echo 'Other';?>"
                                        <?php 
                                        if ($profileDetails->nationality == 'Other')
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                                <?php echo "Other";  ?>
                                    </option>
                                </select>


                                </div>
                            </div>


                        </div>


                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Race <span class='error-text'>*</span></label>
                                    <select name="id_race" id="id_race" class="form-control" style="width: 405px;" disabled>
                                        <option value="">Select</option>
                                        <?php
                                        if (!empty($raceList))
                                        {
                                            foreach ($raceList as $record)
                                            {?>
                                        <option value="<?php echo $record->id;  ?>" <?php if($profileDetails->id_race==$record->id){ echo "selected"; } ?>>
                                            <?php echo $record->name;?>
                                        </option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                        


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Email ID <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="email_id" name="email_id" value="<?php if(!empty($profileDetails->email_id)){ echo $profileDetails->email_id; } ?>" readonly>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>NRIC <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="nric" name="nric" value="<?php echo $profileDetails->nric ?>" readonly>
                                </div>
                            </div>


                    </div>

                    <div class="row">

                        <div class="col-sm-4">
                              <div class="form-group">
                                  <label>Scholarship <span class='error-text'>*</span></label>
                                  <select name="id_scholarship" id="id_scholarship" class="form-control selitemIcon" disabled>
                                      <option value="">Select</option>
                                      <?php
                                      if (!empty($scholarshipList))
                                      {
                                          foreach ($scholarshipList as $record)
                                          {?>
                                              <option value="<?php echo $record->id;?>"
                                              <?php  
                                              if($record->id == $studentApplicatoinDetails->id_scholarship)
                                              {
                                                echo 'selected';
                                              }
                                              ?>
                                              >
                                              <?php echo $record->scholarship_code . " - " . $record->scholarship_name; ?>
                                              </option>
                                          <?php
                                          }
                                      }
                                      ?>

                                  </select>
                              </div>
                          </div>



                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Program <span class='error-text'>*</span></label>
                                <select name="id_program" id="id_program" class="form-control selitemIcon" disabled>
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($programList))
                                    {
                                        foreach ($programList as $record)
                                        {?>
                                            <option value="<?php echo $record->id;  ?>"
                                                <?php 
                                                if($record->id == $profileDetails->id_program)
                                                {
                                                    echo "selected=selected";
                                                } ?>>
                                                <?php echo $record->code . " - " .$record->name;  ?>
                                            </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>

                        
                        <!-- <?php
                            if($profileDetails->id_program > 0)
                            {
                            ?> -->

                                <!-- <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Program <span class='error-text'>*</span></label>  
                                         <a href="editProgram" class="btn btn-link">
                                            <span style='font-size:18px;'>&#9998;</span></a>
                                        <select name="id_program" id="id_program" class="form-control" disabled="true" style="width: 408px">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($programList))
                                            {
                                                foreach ($programList as $record)
                                                {?>
                                                    <option value="<?php echo $record->id;  ?>"
                                                        <?php 
                                                        if($record->id == $profileDetails->id_program)
                                                        {
                                                            echo "selected=selected";
                                                        } ?>>
                                                        <?php echo $record->code . " - " .$record->name;  ?>
                                                    </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div> -->

                           <!--  <?php 
                            }
                            else
                            {
                            ?> -->


                                

                           <!--  <?php
                            }
                            ?> -->

                    <!-- </div> -->

                </div>

                
                 
                 </div> <!-- END col-12 -->  
            </div>






            <div role="tabpanel" class="tab-pane" id="proficiency">
                <div class="col-12 mt-4">

                <div class="form-container">
                <h4 class="form-group-title">Education Qualification Details</h4>

                    <div class="custom-table">
                      <table class="table" id="list-table">
                        <thead>
                          <tr>
                            <th>Sl. No</th>
                            <th>Qualification Level</th>
                            <th>Degree Awarded</th>
                            <th>Result / CGPA</th>
                            <th>Year Of Graduation</th>
                            <th>College Name</th>
                            <th class="text-center">Certificate</th>
                            <th class="text-center">Transcript</th>
                            <!-- <th class="text-center">Action</th> -->
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if (!empty($educationDetails))
                          {
                            $i=1;
                            foreach ($educationDetails as $record)
                            {
                          ?>
                              <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $record->qualification_level ?></td>
                                <td><?php echo $record->degree_awarded ?></td>
                                <td><?php echo $record->result ?></td>
                                <td><?php echo $record->year ?></td>
                                <td><?php echo $record->college_name ?></td>
                                <td class="text-center">
                                    <a href="<?php echo '/assets/images/' . $record->certificate; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->certificate; ?>)" title="View"> View
                                        <!-- <span style='font-size:18px;'>&#128065;</span> -->
                                    </a>

                                </td>
                                <td class="text-center">

                                    <a href="<?php echo '/assets/images/' . $record->transcript; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->transcript; ?>)" title="View"> View
                                        <!-- <span style='font-size:18px;'>&#128065;</span> -->
                                    </a>
                                </td>
                                <!-- <td class="text-center">
                                    <a onclick="deleteEducationDetails(<?php echo $record->id; ?>)" title="Delete">DELETE</a>
                                </td> -->
                              </tr>
                          <?php
                          $i++;
                            }
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>

                </div>

                
                             
                </div> <!-- END col-12 -->  
            </div>






            <div role="tabpanel" class="tab-pane" id="employment">
                <div class="col-12 mt-4">
                    <br>

                    <div class="form-container">
                    <h4 class="form-group-title">Family Details <span class='error-text'>*</span></h4>

                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Father Name <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="father_name" name="father_name" value="<?php echo $familyDetails->father_name;?>" readonly>
                                </div>
                            </div>    


                             <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Mother Name <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="mother_name" name="mother_name" value="<?php echo $familyDetails->mother_name;?>" readonly>
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>No Of Siblings <span class='error-text'>*</span></label>
                                    <input type="number" class="form-control" id="no_siblings" name="no_siblings" value="<?php echo $familyDetails->no_siblings;?>" readonly>
                                </div>
                            </div>     

                        </div>

                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Father Deceased <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="father_deceased" id="sd1" value="1" <?php if($familyDetails->father_deceased=='1'){ echo "checked";}?> disabled><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="father_deceased" id="sd2" value="0" <?php if($familyDetails->father_deceased=='0'){ echo "checked";}?> disabled><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>



                             <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Father Occupation <span class='error-text'>*</span></label>
                                    <select name="father_occupation" id="father_occupation" class="form-control" style="width: 408px" disabled>
                                        <option value="">Select</option>
                                        <option value="Employee"
                                            <?php 
                                            if ($familyDetails->father_occupation == 'Employee')
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                                    <?php echo "Employee";  ?>
                                        </option>

                                        <option value="Government Servent"
                                            <?php 
                                            if ($familyDetails->father_occupation == 'Government Servent')
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                                    <?php echo "Government Servent";  ?>
                                        </option>
                                        <option value="Business"
                                            <?php 
                                            if ($familyDetails->father_occupation == 'Business')
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                                    <?php echo "Business";  ?>
                                        </option>
                                        <option value="Other"
                                            <?php 
                                            if ($familyDetails->father_occupation == 'Other')
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                                    <?php echo "Other";  ?>
                                        </option>
                                    </select>
                                </div>
                            </div>

            


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Estimated Fee Amount(RM) <span class='error-text'>*</span></label>
                                    <input type="number" class="form-control" id="est_fee" name="est_fee" min="1" value="<?php echo $familyDetails->est_fee;?>" readonly>
                                </div>
                            </div>  



                        </div>

                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Family Annual Income(RM) <span class='error-text'>*</span></label>
                                    <input type="number" class="form-control" id="family_annual_income" name="family_annual_income" value="<?php echo $familyDetails->family_annual_income;?>" min="1" readonly>
                                </div>
                            </div>  

                        </div>
                    </div>
                                        
                         
                </div> <!-- END col-12 -->  
            </div>





        
            <div role="tabpanel" class="tab-pane" id="profile">
                <div class="col-12 mt-4">
                    <br>

                    <!-- <div class="form-container">
                        <h4 class="form-group-title">Profile Details</h4>

                        <div class="row">

                            <div class="col-sm-4">
                            <div class="form-group">
                                <label>Salutation <span class='error-text'>*</span></label>
                                <select name="salutation" id="salutation" class="form-control" style="width: 405px;">
                                    <option value="">Select</option>
                                    <option value="Miss" <?php if($profileDetails->salutation=='Miss'){ echo "selected"; } ?>>Miss</option>
                                    <option value="Mr" <?php if($profileDetails->salutation=='Mr'){ echo "selected"; } ?>>Mr</option>
                                    <option value="Mrs" <?php if($profileDetails->salutation=='Mrs'){ echo "selected"; } ?>>Mrs</option>
                                    <option value="Dr" <?php if($profileDetails->salutation=='Dr'){ echo "selected"; } ?>>Dr</option>
                                </select>
                            </div>
                            </div>



                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>First Name <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" value="<?php if(!empty($profileDetails->first_name)){ echo $profileDetails->first_name; } ?>" readonly>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Last Name <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="last_name" name="last_name" value="<?php if(!empty($profileDetails->last_name)){ echo $profileDetails->last_name; } ?>" readonly>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>ID Type <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="id_type" name="id_type" value="<?php if(!empty($profileDetails->id_type)){ echo $profileDetails->id_type; } ?>">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>MyKad Passport National ID Military ID/Police ID MyPR ID Number <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="passport_number" name="passport_number" value="<?php if(!empty($profileDetails->passport_number)){ echo $profileDetails->passport_number; } ?>">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Passport Expiry Date <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control datepicker" autocomplete="off" id="passport_expiry_date" name="passport_expiry_date" value="<?php if(!empty($profileDetails->passport_expiry_date)){ echo date('d-m-Y', strtotime($profileDetails->passport_expiry_date)); } ?>">
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Phone Number <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $profileDetails->phone ?>">
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Gender <span class='error-text'>*</span></label>
                                    <select class="form-control" id="gender" name="gender" style="width: 405px;">
                                        <option value="">SELECT</option>
                                        <option value="Male" <?php if($profileDetails->gender == "Male"){ echo "selected=selected"; } ?>>MALE</option>
                                        <option value="Female" <?php if($profileDetails->gender == "Female"){ echo "selected=selected"; } ?>>FEMALE</option>
                                        <option value="Others" <?php if($profileDetails->gender == "Others"){ echo "selected=selected"; } ?>>OTHERS</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Of Birth <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" value="<?php if(!empty($profileDetails->date_of_birth)){ echo date('d-m-Y', strtotime($profileDetails->date_of_birth)); } ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Martial Status <span class='error-text'>*</span></label>
                                    <select id="martial_status" name="martial_status" class="form-control" style="width: 405px;">
                                        <option value="">SELECT</option>
                                        <option value="Single" <?php if($profileDetails->martial_status=='Single'){ echo "selected"; } ?>>SINGLE</option>
                                        <option value="Married" <?php if($profileDetails->martial_status=='Married'){ echo "selected"; } ?>>MARRIED</option>
                                        <option value="Divorced" <?php if($profileDetails->martial_status=='Divorced'){ echo "selected"; } ?>>DIVORCED</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Religion <span class='error-text'>*</span></label>
                                    <select name="religion" id="religion" class="form-control" style="width: 405px;">
                                        <option value="">Select</option>
                                        <?php
                                        if (!empty($religionList))
                                        {
                                            foreach ($religionList as $record)
                                            {?>
                                                <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $profileDetails->religion)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?>
                                                </option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>



                            <div class="col-sm-4">
                                <div class="form-group">
                                <label>Type Of Nationality <span class='error-text'>*</span></label>

                                 <select name="nationality" id="nationality" class="form-control" style="width: 405px;">
                                    <option value="">Select</option>
                                    <option value="<?php echo 'Malaysian';?>"
                                        <?php 
                                        if ($profileDetails->nationality == 'Malaysian')
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                                <?php echo "Malaysian";  ?>
                                    </option>

                                    <option value="<?php echo 'Other';?>"
                                        <?php 
                                        if ($profileDetails->nationality == 'Other')
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                                <?php echo "Other";  ?>
                                    </option>
                                </select>


                                </div>
                            </div>

                        </div>


                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Race <span class='error-text'>*</span></label>
                                    <select name="id_race" id="id_race" class="form-control" style="width: 405px;">
                                        <option value="">Select</option>
                                        <?php
                                        if (!empty($raceList))
                                        {
                                            foreach ($raceList as $record)
                                            {?>
                                        <option value="<?php echo $record->id;  ?>" <?php if($profileDetails->id_race==$record->id){ echo "selected"; } ?>>
                                            <?php echo $record->name;?>
                                        </option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                           

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Email ID <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="email_id" name="email_id" value="<?php if(!empty($profileDetails->email_id)){ echo $profileDetails->email_id; } ?>" readonly>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>NRIC <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="nric" name="nric" value="<?php echo $profileDetails->nric ?>" readonly>
                                </div>
                            </div>

                        </div>

                     <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Upload Employment Letter <span class='error-text'>*</span></label>
                                <input type="file" class="form-control" id="employment_letter" name="employment_letter">
                            </div>
                        </div>

                     </div>

                    </div> -->


                

                    <!-- <div class="button-block clearfix">
                        <div class="bttn-group pull-right">
                        <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                        </div>
                    </div> -->
                 
                </div> <!-- END col-12 -->  
            </div>







            <div role="tabpanel" class="tab-pane" id="visa">
                <div class="col-12 mt-4">
                    <br>

                <div class="form-container">
                    <h4 class="form-group-title">Upload File Details</h4>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Upload Employment Letter <span class='error-text'>*</span></label>
                                <input type="file" class="form-control" id="employment_letter" name="employment_letter"  >
                            </div>
                        </div>

                     </div>                  

                </div>

                 
                </div> <!-- END col-12 -->  
            </div>





            <div role="tabpanel" class="tab-pane" id="other">

                <div class="col-12 mt-4">
                    <br>
                    

                    <div class="form-container">
                        <h4 class="form-group-title">Employment Details</h4>  

                        <div class="custom-table">
                          <table class="table" id="list-table">
                            <thead>
                              <tr>
                                <th>Sl. No</th>
                                <th>Company Name</th>
                                <th>Company Address</th>
                                <th>Designation</th>
                                <th>Position</th>
                                <th>Year Of Service</th>
                                <th class="text-center">Employment Letter</th>
                                <!-- <th class="text-center">Action</th> -->
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              if (!empty($employmentDetails))
                              {
                                $i=1;
                                foreach ($employmentDetails as $record) {
                              ?>
                                  <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->company_name ?></td>
                                    <td><?php echo $record->company_address ?></td>
                                    <td><?php echo $record->designation ?></td>
                                    <td><?php echo $record->position ?></td>
                                    <td><?php if($record->service_year == ""){ echo "0";} else {
                                        echo $record->service_year; } ?></td>
                                    <td class="text-center">

                                        <a href="<?php echo '/assets/images/' . $record->employment_letter; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->employment_letter; ?>)" title="View"> View
                                            <!-- <span style='font-size:18px;'>&#128065;</span> -->
                                        </a>

                                    </td>

                                    <!-- <td class="text-center">
                                        <a onclick="deleteEmploymentDetails(<?php echo $record->id; ?>)" title="Delete">DELETE</a>
                                    </td> -->
                                  </tr>
                              <?php
                              $i++;
                                }
                              }
                              ?>
                            </tbody>
                          </table>
                        </div>

                    </div>


                 
                </div> <!-- END col-12 -->  
            </div>




            <div role="tabpanel" class="tab-pane" id="decleration">

                <div class="col-12 mt-4">
                    <br>





                    <div class="form-container">
                     <h4 class="form-group-title">Decleration</h4>
                     <br>
                     <h4 class="modal-title">Agree To Terms & Condition To Submit The Application</h4>
                     <br>
                     <p>
                        1. These terms and conditions represent an agreement between the University of Edinburgh ("University") and you, a prospective student. By accepting the University's offer of a place on a programme, you accept these terms and conditions in full, which along with your offer and the University's rules, regulations, policies and procedures and the most recently published prospectus (as applicable), form the contract between you and the University in relation to your studies at the University as amended from time to time pursuant to Clause 1.3 (the "Contract"). 
                     </p>
                     <p>
                        2.    If you have any questions or concerns about these terms and conditions, please contact the University's Student Recruitment and Admission Office:
                     </p>
                     <p>Requirements
                     </p>
                     <br>
                     &emsp;<input type="checkbox" 
                      <?php if($studentApplicatoinDetails->is_submitted > 0)
                        {
                            echo 'checked="true"';
                        }
                        ?> id="is_submitted" 
                      name="is_submitted" value="1" disabled="true">&emsp;
                     By Checking This Button I accept the <u><a onclick="showModel()">Terms and Conditions</a> <span class='error-text'>*</span></u>
                    </div>
                    

                


                 
                </div> <!-- END col-12 -->  
            </div>



            <div role="tabpanel" class="tab-pane" id="contact_details">

                <div class="col-12 mt-4">
                    
                <br>


                <div class="form-container">
                    <h4 class="form-group-title">Mailing Address</h4>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Mailing Address 1 <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="mail_address1" name="mail_address1" value="<?php echo $profileDetails->mail_address1 ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Mailing Address 2 <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="mail_address2" name="mail_address2" value="<?php echo $profileDetails->mail_address2 ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Mailing Country <span class='error-text'>*</span></label>
                                <select name="mailing_country" id="mailing_country" class="form-control" style="width: 405px;" disabled>
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($countryList))
                                    {
                                        foreach ($countryList as $record)
                                        {?>
                                     <option value="<?php echo $record->id;  ?>" <?php if($profileDetails->mailing_country==$record->id){ echo "selected"; } ?>>
                                        <?php echo $record->name;?>
                                     </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Mailing State <span class='error-text'>*</span></label>
                                <select name="mailing_state" id="mailing_state" class="form-control" style="width: 405px;" disabled>
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($stateList))
                                    {
                                        foreach ($stateList as $record)
                                        {?>
                                     <option value="<?php echo $record->id;  ?>" <?php if($profileDetails->mailing_state==$record->id){ echo "selected"; } ?>>
                                        <?php echo $record->name;?>
                                     </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Mailing City <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="mailing_city" name="mailing_city" value="<?php echo $profileDetails->mailing_city ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Mailing Zipcode <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="mailing_zipcode" name="mailing_zipcode" value="<?php echo $profileDetails->mailing_zipcode ?>" readonly>
                            </div>
                        </div>
                    </div>

                </div>

                
                <br>

                <div class="form-container">
                    <h4 class="form-group-title">Permanent Address</h4>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Permanent Address 1 <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="permanent_address1" name="permanent_address1" value="<?php echo $profileDetails->permanent_address1 ?>" readonly>
                            </div>
                        </div><div class="col-sm-4">
                            <div class="form-group">
                                <label>Permanent Address 2 <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="permanent_address2" name="permanent_address2" value="<?php echo $profileDetails->permanent_address2 ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Permanent Country <span class='error-text'>*</span></label>
                                <select name="permanent_country" id="permanent_country" class="form-control" style="width: 405px;" disabled>
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($countryList))
                                    {
                                        foreach ($countryList as $record)
                                        {?>
                                     <option value="<?php echo $record->id;  ?>" <?php if($profileDetails->permanent_country==$record->id){ echo "selected"; } ?>>
                                        <?php echo $record->name;?>
                                     </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Permanent State <span class='error-text'>*</span></label>
                                <select name="permanent_state" id="permanent_state" class="form-control" style="width: 405px;" disabled>
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($stateList))
                                    {
                                        foreach ($stateList as $record)
                                        {?>
                                     <option value="<?php echo $record->id;  ?>" <?php if($profileDetails->permanent_state==$record->id){ echo "selected"; } ?>>
                                        <?php echo $record->name;?>
                                     </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Permanent City <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="permanent_city" name="permanent_city" value="<?php echo $profileDetails->permanent_city ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Permanent Zipcode <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="permanent_zipcode" name="permanent_zipcode" value="<?php echo $profileDetails->permanent_zipcode ?>" readonly>
                            </div>
                        </div>
                    </div>

                </div>

                


                 
                </div> <!-- END col-12 -->  
            </div>






                <div role="tabpanel" class="tab-pane" id="approval_details">
                <div class="col-12 mt-4">

                    <div class="form-container">
                    <h4 class="form-group-title">Application Status Details</h4>

                    <div class="custom-table">
                      <table class="table" id="list-table">
                        <thead>
                          <tr>
                            <th>Sl. No</th>
                            <th>Updated By</th>
                            <th>Status</th>
                            <th>Date Time</th>
                            <!-- <th class="text-center">Action</th> -->
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if (!empty($getStatusHistoryByIdApplication))
                          {
                            $i=1;
                            foreach ($getStatusHistoryByIdApplication as $record)
                            {
                          ?>
                              <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $record->user_name ?></td>
                                <td><?php echo $record->status ?></td>
                                <td><?php echo date('d-m-Y H:i:s', strtotime($record->created_dt_tm)) ?></td>
                                <!-- <td class="text-center">
                                    <a onclick="deleteEducationDetails(<?php echo $record->id; ?>)" title="Delete">DELETE</a>
                                </td> -->
                              </tr>
                          <?php
                          $i++;
                            }
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>

                            </div>

                </div>

            </div>



          </div>
        </div>

       </div> <!-- END row-->
   
    </form>
    <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
    </footer>

    </div>
</div>
<script type="text/javascript">

    $('select').select2();

    $( function()
    {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1920:2019"
    });
    });


    function deleteEducationDetails(id)
    {
        $.ajax(
        {
           url: '/scholarship_applicant/editProfile/deleteEducationDetails/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            // alert(result);
                window.location.reload();
           }
        });
    }


    function deleteEmploymentDetails(id)
    {
         $.ajax(
        {
           url: '/scholarship_applicant/editProfile/deleteEmploymentDetails/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            // alert(result);
                window.location.reload();
           }
        });
    }

    function submitApp()
    {
        if($('#form_profile').valid())
        {    
            $('#id_program').prop('disabled', false);
            $('#form_profile').submit();
        }
    }




    $(document).ready(function() {
        $("#form_profile").validate({
            rules: {
                qualification_level: {
                    required: true
                },
                degree_awarded: {
                    required: true
                },
                specialization: {
                    required: true
                },
                class_degree: {
                    required: true
                },
                result: {
                    required: true
                },
                year: {
                    required: true
                },
                medium: {
                    required: true
                },
                college_country: {
                    required: true
                },
                college_name: {
                    required: true
                },
                test: {
                    required: true
                },
                date: {
                    required: true
                },
                score: {
                    required: true
                },
                company_name: {
                    required: true
                },
                company_address: {
                    required: true
                },
                telephone: {
                    required: true
                },
                fax_num: {
                    required: true
                },
                designation: {
                    required: true
                },
                position: {
                    required: true
                },
                service_year: {
                    required: true
                },
                industry: {
                    required: true
                },
                job_desc: {
                    required: true
                },
                salutation: {
                    required: true
                },
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                id_type: {
                    required: true
                },
                passport_number: {
                    required: true
                },
                passport_expiry_date: {
                    required: true
                },
                phone: {
                    required: true
                },
                gender: {
                    required: true
                },
                date_of_birth: {
                    required: true
                },
                martial_status: {
                    required: true
                },
                religion: {
                    required: true
                },
                nationality: {
                    required: true
                },
                id_race: {
                    required: true
                },
                email_id: {
                    required: true
                },
                nric: {
                    required: true
                },
                mail_address1: {
                    required: true
                },
                mail_address2: {
                    required: true
                },
                mailing_country: {
                    required: true
                },
                mailing_state: {
                    required: true
                },
                mailing_city: {
                    required: true
                },
                mailing_zipcode: {
                    required: true
                },
                permanent_address1: {
                    required: true
                },
                permanent_address2: {
                    required: true
                },
                permanent_country: {
                    required: true
                },
                permanent_state: {
                    required: true
                },
                permanent_city: {
                    required: true
                },
                permanent_zipcode: {
                    required: true
                },
                malaysian_visa: {
                    required: true
                },
                visa_expiry_date: {
                    required: true
                },
                visa_status: {
                    required: true
                },
                doc_name: {
                    required: true
                },
                remarks: {
                    required: true
                },
                id_program: {
                    Required: true
                }
            },
            messages: {
                qualification_level: {
                    required: "<p class='error-text'>Qualification Level Required</p>",
                },
                degree_awarded: {
                    required: "<p class='error-text'>Degree Awarded Required</p>",
                },
                specialization: {
                    required: "<p class='error-text'>Specialization Required</p>",
                },
                class_degree: {
                    required: "<p class='error-text'>Degree Class Required</p>",
                },
                result: {
                    required: "<p class='error-text'>Result Required</p>",
                },
                year: {
                    required: "<p class='error-text'>Year Requred</p>",
                },
                medium: {
                    required: "<p class='error-text'>Select Medium Of Study</p>",
                },
                college_country: {
                    required: "<p class='error-text'>Country Required</p>",
                },
                college_name: {
                    required: "<p class='error-text'>College Name Required</p>",
                },
                test: {
                    required: "<p class='error-text'>Test Name Required</p>",
                },
                date: {
                    required: "<p class='error-text'>Select Date</p>",
                },
                score: {
                    required: "<p class='error-text'>Score Required</p>",
                },
                company_name: {
                    required: "<p class='error-text'>Company Name Required</p>",
                },
                company_address: {
                    required: "<p class='error-text'>Company Address Required</p>",
                },
                telephone: {
                    required: "<p class='error-text'>Telephone No. Required</p>",
                },
                fax_num: {
                    required: "<p class='error-text'>Fax No. Required</p>",
                },
                designation: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                position: {
                    required: "<p class='error-text'>Position Of Rhe Job Required</p>",
                },
                service_year: {
                    required: "<p class='error-text'>No. Of Service Required</p>",
                },
                industry: {
                    required: "<p class='error-text'>Industry Required</p>",
                },
                job_desc: {
                    required: "<p class='error-text'>Job Description Required</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name Required</p>",
                },
                id_type: {
                    required: "<p class='error-text'>ID Type Required</p>",
                },
                passport_number: {
                    required: "<p class='error-text'>Passport NUmber Required</p>",
                },
                passport_expiry_date: {
                    required: "<p class='error-text'>Select Passport Expire Date</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone Number Required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Select Gender</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                martial_status: {
                    required: "<p class='error-text'>Select Maritual Status</p>",
                },
                religion: {
                    required: "<p class='error-text'>Select Religion</p>",
                },
                nationality: {
                    required: "<p class='error-text'>Select Ntionality</p>",
                },
                id_race: {
                    required: "<p class='error-text'>Select Rece</p>",
                },
                email_id: {
                    required: "<p class='error-text'>Main Id Required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC Required</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Mailing Address 1 Required</p>",
                },
                mail_address2: {
                    required: "<p class='error-text'>Mailing Address 2 Required</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Mailing Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>Select Mailing State</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Mailing City Required</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Mailing Zipcode Required</p>",
                },
                permanent_address1: {
                    required: "<p class='error-text'>Select Permanent Address 2</p>",
                },
                permanent_address2: {
                    required: "<p class='error-text'>Select Permanent Address 2</p>",
                },
                permanent_country: {
                    required: "<p class='error-text'>Select Permanent Country</p>",
                },
                permanent_state: {
                    required: "<p class='error-text'>Select Permanent State</p>",
                },
                permanent_city: {
                    required: "<p class='error-text'>Permanent City Required</p>",
                },
                permanent_zipcode: {
                    required: "<p class='error-text'>Permanent Zipcode Required</p>",
                },
                malaysian_visa: {
                    required: "<p class='error-text'>Select Visa Type</p>",
                },
                visa_expiry_date: {
                    required: "<p class='error-text'>Select Visa Expire Date</p>",
                },
                visa_status: {
                    required: "<p class='error-text'>Visa Status Required</p>",
                },
                doc_name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                remarks: {
                    required: "<p class='error-text'>Remars Required</p>"
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>"
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

  </script>
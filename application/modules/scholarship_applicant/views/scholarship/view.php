<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Scholaship Applicant Details</h3>
        </div>


    <div class="form-container">
            <h4 class="form-group-title">Applicant Details</h4>
            <div class='data-list'>
                <div class='row'> 
                    <div class='col-sm-6'>
                        <dl>
                            <dt>Applicant Name :</dt>
                            <dd>
                            <?php
                           
                                echo ucwords($profileDetails->full_name);
                            ?>
                                
                            </dd>
                        </dl>
                        <dl>
                            <dt>Applicant NRIC :</dt>
                            <dd>
                                <?php
                           
                                echo ucwords($profileDetails->nric);
                            ?>
                            
                            </dd>
                        </dl>
                                                
                    </div>        
                    
                    <div class='col-sm-6'> 
                         <dl>
                            <dt>Applicant Email :</dt>
                            <dd>
                            <?php
                           
                                echo ucwords($profileDetails->email_id);
                            ?>
                            </dd>
                        </dl>     
                        <dl>
                            <dt>Applicant Phone :</dt>
                            <dd>
                            <?php
                           
                                echo ucwords($profileDetails->phone);
                            ?>
                        </dd>
                        </dl>                         
                        
                    </div>
                </div>
            </div>
        </div>



    <form id="form_profile" action="" method="post" enctype="multipart/form-data">



    <div class="form-container">
            <h4 class="form-group-title">Cohert Details</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $scheme->name; ?>" readonly>
                        <input type="hidden" class="form-control" id="id_programme" name="id_programme" value="<?php echo $studentDetails->id_program; ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $scheme->code; ?>" readonly>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Scholrship Percentage <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="percentage" name="percentage" min="1" max="100" value="<?php echo $scheme->percentage; ?>" readonly>
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" value="<?php echo date('d-m-Y',strtotime($scheme->from_dt)) ?>" readonly>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="to_dt" name="to_dt" value="<?php echo date('d-m-Y',strtotime($scheme->to_dt)) ?>" readonly>
                    </div>
                </div>



                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($scheme->status=='1') {
                                 echo "checked=checked";
                              };?> disabled>
                              <span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($scheme->status=='0') {
                                 echo "checked=checked";
                              };?> disabled>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>               
                
            </div>

                

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group pull-right">
            <a href="../schemeList" class="btn btn-link">Back</a>
        <?php
        if(!empty($studentApplicatoinDetails) && !empty($familyDetails))
        {
            ?>

            <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="1">Submit Application</button>

        <?php

        }
        ?>
                <!-- <button type="submit" class="btn btn-primary btn-lg" >Save</button> -->
            </div>
        </div>



    <br>
    
  
    
        <div class="m-auto text-center">
           <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto">
               
           </div>
         </div>
        <div class="clearfix">

            <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
            <li role="presentation" class="active" id="is_personel_details"><a href="#education" class="nav-link border rounded text-center"
                    aria-controls="education" aria-selected="true"
                    role="tab" data-toggle="tab">Personal Details</a>
            </li>
            <li role="presentation" id="is_education_details"><a href="#proficiency" class="nav-link border rounded text-center"
                    aria-controls="proficiency" role="tab" data-toggle="tab">Education Details</a>
            </li>
            <li role="presentation" id="is_family_details"><a href="#employment" class="nav-link border rounded text-center"
                    aria-controls="employment" role="tab" data-toggle="tab">Family Information</a>
            </li>
            <li role="presentation" id="is_financial_recoverable_details"><a href="#profile" class="nav-link border rounded text-center"
                    aria-controls="profile" role="tab" data-toggle="tab">Financial Recoverable</a>
            </li>
            <li role="presentation" id="is_file_upload_details"><a href="#visa" class="nav-link border rounded text-center"
                    aria-controls="visa" role="tab" data-toggle="tab">File Upload</a>
            </li>
            <li role="presentation" id="is_employment_details"><a href="#other" class="nav-link border rounded text-center"
                    aria-controls="other" role="tab" data-toggle="tab">Employment Details</a>
            </li>
            </ul>



        <div class="tab-content offers-tab-content">

            <div role="tabpanel" class="tab-pane active" id="education">
                <div class="col-12 mt-4">
                    <br>

                 <div class="form-container">
                    <h4 class="form-group-title">Profile Details</h4>

                        <div class="row">

                            <div class="col-sm-4" id="is_salutation_view">
                            <div class="form-group">
                                <label>Salutation <span class='error-text'>*</span></label>
                                <select name="salutation" id="salutation" class="form-control" style="width: 405px;">
                                    <option value="">Select</option>

                                    <?php
                                    if(empty($studentApplicatoinDetails))
                                    {
                                        ?>

                                        <option value="Miss" <?php if($profileDetails->salutation=='Miss'){ echo "selected"; } ?>>Miss</option>
                                        <option value="Mr" <?php if($profileDetails->salutation=='Mr'){ echo "selected"; } ?>>Mr</option>
                                        <option value="Mrs" <?php if($profileDetails->salutation=='Mrs'){ echo "selected"; } ?>>Mrs</option>
                                        <option value="Dr" <?php if($profileDetails->salutation=='Dr'){ echo "selected"; } ?>>Dr</option>

                                        <?php
                                    }
                                    else
                                    {
                                        ?>

                                        <option value="Miss" <?php if($studentApplicatoinDetails->salutation=='Miss'){ echo "selected"; } ?>>Miss</option>
                                        <option value="Mr" <?php if($studentApplicatoinDetails->salutation=='Mr'){ echo "selected"; } ?>>Mr</option>
                                        <option value="Mrs" <?php if($studentApplicatoinDetails->salutation=='Mrs'){ echo "selected"; } ?>>Mrs</option>
                                        <option value="Dr" <?php if($studentApplicatoinDetails->salutation=='Dr'){ echo "selected"; } ?>>Dr</option>


                                       <?php
                                    }
                                    ?>

                                    
                                </select>
                            </div>
                            </div>



                            <div class="col-sm-4" id="is_first_name_view">
                                <div class="form-group">
                                    <label>First Name <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" value="<?php 
                                    if(empty($studentApplicatoinDetails->first_name))
                                    { 
                                        echo $profileDetails->first_name; 
                                    }
                                    else
                                    {
                                        echo $studentApplicatoinDetails->first_name; 
                                    } 
                                    ?>">

                                    <input type="hidden" class="form-control" id="is_profile_update" name="is_profile_update" value="<?php 
                                    if(empty($studentApplicatoinDetails->first_name))
                                    { 
                                        echo 0; 
                                    }
                                    else
                                    {
                                        echo $studentApplicatoinDetails->id;
                                    } 
                                    ?>">
                                </div>
                            </div>

                            <div class="col-sm-4" id="is_last_name_view">
                                <div class="form-group">
                                    <label>Last Name <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="last_name" name="last_name" value="<?php
                                     if(empty($studentApplicatoinDetails->last_name))
                                    { 
                                        echo $profileDetails->last_name;
                                    }
                                    else
                                    {
                                        echo $studentApplicatoinDetails->last_name;
                                    } ?>">
                                </div>
                            </div>



                            <div class="col-sm-4" id="is_id_type_view">
                                <div class="form-group">
                                    <label>ID Type <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="id_type" name="id_type" value="<?php if(!empty($studentApplicatoinDetails->id_type)){ echo $studentApplicatoinDetails->id_type; } ?>">
                                </div>
                            </div>

                            <div class="col-sm-4" id="is_passport_number_view">
                                <div class="form-group">
                                    <label>MyKad Passport National ID Military ID/Police ID MyPR ID Number <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="passport_number" name="passport_number" value="<?php if(!empty($studentApplicatoinDetails->passport_number)){ echo $studentApplicatoinDetails->passport_number; } ?>">
                                </div>
                            </div>

                            <div class="col-sm-4" id="is_passport_expiry_date_view">
                                <div class="form-group">
                                    <label>Passport Expiry Date <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control datepicker" autocomplete="off" id="passport_expiry_date" name="passport_expiry_date" value="<?php if(!empty($studentApplicatoinDetails->passport_expiry_date)){ echo date('d-m-Y', strtotime($studentApplicatoinDetails->passport_expiry_date)); } ?>">
                                </div>
                            </div>


                       

                            <div class="col-sm-4" id="is_phone_view">
                                <div class="form-group">
                                    <label>Phone Number <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="phone" name="phone" value="<?php
                                     if(empty($studentApplicatoinDetails->phone))
                                    { 
                                        echo $profileDetails->phone;
                                    }
                                    else
                                    {
                                        echo $studentApplicatoinDetails->phone;
                                    } ?>">
                                </div>
                            </div>


                            <div class="col-sm-4" id="is_gender_view">
                                <div class="form-group">
                                    <label>Gender <span class='error-text'>*</span></label>
                                    <select class="form-control" id="gender" name="gender" style="width: 405px;">
                                        <option value="">SELECT</option>
                                        <?php
                                        if(!empty($studentApplicatoinDetails))
                                        {
                                            ?>
                                        <option value="Male" <?php if($studentApplicatoinDetails->gender == "Male"){ echo "selected=selected"; } ?>>MALE</option>
                                        <option value="Female" <?php if($studentApplicatoinDetails->gender == "Female"){ echo "selected=selected"; } ?>>FEMALE</option>
                                        <?php
                                        }
                                        else
                                        {
                                            ?>

                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>


                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4" id="is_date_of_birth_view">
                                <div class="form-group">
                                    <label>Date Of Birth <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" value="<?php 
                                    if(!empty($studentApplicatoinDetails->date_of_birth))
                                    { 
                                        echo date('d-m-Y', strtotime($studentApplicatoinDetails->date_of_birth));
                                    } ?>
                                    " autocomplete="off">
                                </div>
                            </div>


                        


                            <div class="col-sm-4" id="is_martial_status_view">
                                <div class="form-group">
                                    <label>Martial Status <span class='error-text'>*</span></label>
                                    <select id="martial_status" name="martial_status" class="form-control" style="width: 405px;">
                                        <option value="">SELECT</option>

                                    <?php
                                        if(!empty($studentApplicatoinDetails))
                                        {
                                            ?>


                                        <option value="Single" <?php if($studentApplicatoinDetails->martial_status=='Single'){ echo "selected"; } ?>>SINGLE</option>
                                        <option value="Married" <?php if($studentApplicatoinDetails->martial_status=='Married'){ echo "selected"; } ?>>MARRIED</option>
                                        <option value="Divorced" <?php if($studentApplicatoinDetails->martial_status=='Divorced'){ echo "selected"; } ?>>DIVORCED</option>

                                        <?php
                                        }
                                        else
                                        {
                                            ?>

                                        <option value="Single">SINGLE</option>
                                        <option value="Married">MARRIED</option>
                                        <option value="Divorced">DIVORCED</option>


                                            <?php
                                        }
                                        ?>

                                    </select>
                                </div>
                            </div>


                            <div class="col-sm-4" id="is_religion_view">
                                <div class="form-group">
                                    <label>Religion <span class='error-text'>*</span></label>
                                    <select name="religion" id="religion" class="form-control" style="width: 405px;">
                                        <option value="">Select</option>
                                        <?php
                                        if (!empty($religionList) && !empty($studentApplicatoinDetails))
                                        {
                                            foreach ($religionList as $record)
                                            {?>
                                                <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $studentApplicatoinDetails->religion)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?>
                                                </option>
                                        <?php
                                            }
                                        }
                                        else
                                        if (!empty($religionList))
                                        {
                                            foreach ($religionList as $record)
                                            {?>
                                                <option value="<?php echo $record->id;?>"
                                                ><?php echo $record->name; ?>
                                                </option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>



                            <div class="col-sm-4" id="is_nationality_view">
                                <div class="form-group">
                                <label>Type Of Nationality <span class='error-text'>*</span></label>

                                 <select name="nationality" id="nationality" class="form-control" style="width: 405px;">
                                    <option value="">Select</option>

                                <?php

                                if(!empty($studentApplicatoinDetails))
                                    {
                                    ?>

                                    <option value="<?php echo 'Malaysian';?>"
                                        <?php 
                                        if ($studentApplicatoinDetails->nationality == 'Malaysian')
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                                <?php echo "Malaysian";  ?>
                                    </option>

                                    <option value="<?php echo 'Other';?>"
                                        <?php 
                                        if ($studentApplicatoinDetails->nationality == 'Other')
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                                <?php echo "Other";  ?>
                                    </option>
                                    <?php 
                                    }
                                    else
                                    {
                                        ?>
                                    <option value="Malaysian">Malaysian</option>
                                    <option value="Other">Other</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                </div>
                            </div>


                       




                            <div class="col-sm-4" id="is_id_race_view">
                                <div class="form-group">
                                    <label>Race <span class='error-text'>*</span></label>
                                    <select name="id_race" id="id_race" class="form-control" style="width: 405px;">
                                        <option value="">Select</option>
                                        <?php
                                        if (!empty($raceList) && !empty($studentApplicatoinDetails))
                                        {
                                            foreach ($raceList as $record)
                                            {?>
                                        <option value="<?php echo $record->id;  ?>" <?php if($studentApplicatoinDetails->id_race==$record->id){ echo "selected"; } ?>>
                                            <?php echo $record->name;?>
                                        </option>
                                        <?php
                                            }
                                        }
                                        else
                                        if (!empty($raceList))
                                        {
                                            foreach ($raceList as $record)
                                            {?>
                                                <option value="<?php echo $record->id;?>"
                                                ><?php echo $record->name; ?>
                                                </option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                        


                            <div class="col-sm-4" id="is_email_id_view">
                                <div class="form-group">
                                    <label>Email ID <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="email_id" name="email_id" value="<?php
                                     if(empty($studentApplicatoinDetails->email_id))
                                    { 
                                        echo $profileDetails->email_id;
                                    }
                                    else
                                    {
                                        echo $studentApplicatoinDetails->email_id;
                                    } ?>" readonly>
                                </div>
                            </div>

                            <div class="col-sm-4" id="is_nric">
                                <div class="form-group">
                                    <label>NRIC <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="nric" name="nric" value="<?php
                                     if(empty($studentApplicatoinDetails->nric))
                                    { 
                                        echo $profileDetails->nric;
                                    }
                                    else
                                    {
                                        echo $studentApplicatoinDetails->nric;
                                    } ?>" readonly>
                                </div>
                            </div>



                    


                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Program <span class='error-text'>*</span></label>
                                <select name="id_program" id="id_program" class="form-control selitemIcon" onchange="getRequirementsByProgramId()">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($programList) && !empty($studentApplicatoinDetails))
                                    {
                                        foreach ($programList as $record)
                                        {?>
                                            <option value="<?php echo $record->id;  ?>"
                                                <?php 
                                                if($record->id == $studentApplicatoinDetails->id_program)
                                                {
                                                    echo "selected=selected";
                                                } ?>>
                                                <?php echo $record->code . " - " .$record->name;  ?>
                                            </option>
                                    <?php
                                        }
                                    }
                                    else
                                    if (!empty($programList))
                                    {
                                        foreach ($programList as $record)
                                        {?>
                                            <option value="<?php echo $record->id;?>"
                                            ><?php echo $record->code . " - " . $record->name; ?>
                                            </option>
                                        <?php
                                        }
                                    }
                                    ?>

                                </select>
                            </div>
                        </div>

                    </div>


                    <div class="row" id="view_data" style="display: none;">
                        
                    </div>

                        
                        <!-- <?php
                            if($profileDetails->id_program > 0)
                            {
                            ?> -->

                                <!-- <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Program <span class='error-text'>*</span></label>  
                                         <a href="editProgram" class="btn btn-link">
                                            <span style='font-size:18px;'>&#9998;</span></a>
                                        <select name="id_program" id="id_program" class="form-control" disabled="true" style="width: 408px">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($programList))
                                            {
                                                foreach ($programList as $record)
                                                {?>
                                                    <option value="<?php echo $record->id;  ?>"
                                                        <?php 
                                                        if($record->id == $profileDetails->id_program)
                                                        {
                                                            echo "selected=selected";
                                                        } ?>>
                                                        <?php echo $record->code . " - " .$record->name;  ?>
                                                    </option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div> -->

                           <!--  <?php 
                            }
                            else
                            {
                            ?> -->


                                

                           <!--  <?php
                            }
                            ?> -->

                    <!-- </div> -->

                </div>

                <br>


                <div class="form-container">
                    <h4 class="form-group-title">Mailing Address</h4>

                    <div class="row">

                        <div class="col-sm-4" id="is_mail_address1_view">
                            <div class="form-group">
                                <label>Mailing Address 1 <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="mail_address1" name="mail_address1" value="<?php 
                                if(!empty($studentApplicatoinDetails->mail_address1))
                                { 
                                    echo $studentApplicatoinDetails->mail_address1;
                                } ?>">
                            </div>
                        </div>

                        <div class="col-sm-4" id="is_mail_address2_view">
                            <div class="form-group">
                                <label>Mailing Address 2 <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="mail_address2" name="mail_address2" value="<?php 
                                if(!empty($studentApplicatoinDetails->mail_address2))
                                { 
                                    echo $studentApplicatoinDetails->mail_address2;
                                } ?>">
                            </div>
                        </div>

                        <div class="col-sm-4" id="is_mailing_country_view">
                            <div class="form-group">
                                <label>Mailing Country <span class='error-text'>*</span></label>
                                <select name="mailing_country" id="mailing_country" class="form-control" style="width: 405px;">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($countryList) && !empty($studentApplicatoinDetails))
                                    {
                                        foreach ($countryList as $record)
                                        {?>
                                     <option value="<?php echo $record->id;  ?>" <?php if($studentApplicatoinDetails->mailing_country==$record->id){ echo "selected"; } ?>>
                                        <?php echo $record->name;?>
                                     </option>
                                    <?php
                                        }
                                    }
                                    else
                                    if (!empty($countryList))
                                    {
                                        foreach ($countryList as $record)
                                        {?>
                                            <option value="<?php echo $record->id;?>"
                                            ><?php echo $record->name; ?>
                                            </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                   <!--  </div>

                    <div class="row"> -->

                        <div class="col-sm-4" id="is_mailing_state_view">
                            <div class="form-group">
                                <label>Mailing State <span class='error-text'>*</span></label>
                                <select name="mailing_state" id="mailing_state" class="form-control" style="width: 405px;">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($stateList) && !empty($studentApplicatoinDetails))
                                    {
                                        foreach ($stateList as $record)
                                        {?>
                                     <option value="<?php echo $record->id;  ?>" <?php if($studentApplicatoinDetails->mailing_state==$record->id){ echo "selected"; } ?>>
                                        <?php echo $record->name;?>
                                     </option>
                                    <?php
                                        }
                                    }
                                    else
                                    if (!empty($stateList))
                                    {
                                        foreach ($stateList as $record)
                                        {?>
                                            <option value="<?php echo $record->id;?>"
                                            ><?php echo $record->name; ?>
                                            </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4" id="is_mailing_city_view">
                            <div class="form-group">
                                <label>Mailing City <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="mailing_city" name="mailing_city" value="<?php 
                                if(!empty($studentApplicatoinDetails->mailing_city))
                                { 
                                    echo $studentApplicatoinDetails->mailing_city;
                                } ?>">
                            </div>
                        </div>

                        <div class="col-sm-4" id="is_mailing_zipcode_view">
                            <div class="form-group">
                                <label>Mailing Zipcode <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="mailing_zipcode" name="mailing_zipcode" value="<?php 
                                if(!empty($studentApplicatoinDetails->mailing_zipcode))
                                { 
                                    echo $studentApplicatoinDetails->mailing_zipcode;
                                } ?>">
                            </div>
                        </div>
                    </div>

                </div>

                
                <br>

                <div class="form-container">
                    <h4 class="form-group-title">Permanent Address</h4>
                    
                    <div class="row">

                        <div class="col-sm-4" id="is_permanent_address1_view">
                            <div class="form-group">
                                <label>Permanent Address 1 <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="permanent_address1" name="permanent_address1" value="<?php 
                                if(!empty($studentApplicatoinDetails->permanent_address1))
                                { 
                                    echo $studentApplicatoinDetails->permanent_address1;
                                } ?>">
                            </div>
                        </div><div class="col-sm-4" id="is_permanent_address2_view">
                            <div class="form-group">
                                <label>Permanent Address 2 <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="permanent_address2" name="permanent_address2" value="<?php 
                                if(!empty($studentApplicatoinDetails->permanent_address2))
                                { 
                                    echo $studentApplicatoinDetails->permanent_address2;
                                } ?>">
                            </div>
                        </div>
                        <div class="col-sm-4" id="is_permanent_country_view">
                            <div class="form-group">
                                <label>Permanent Country <span class='error-text'>*</span></label>
                                <select name="permanent_country" id="permanent_country" class="form-control" style="width: 405px;">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($countryList) && !empty($studentApplicatoinDetails))
                                    {
                                        foreach ($countryList as $record)
                                        {?>
                                     <option value="<?php echo $record->id;  ?>" <?php if($studentApplicatoinDetails->permanent_country==$record->id){ echo "selected"; } ?>>
                                        <?php echo $record->name;?>
                                     </option>
                                    <?php
                                        }
                                    }
                                    else
                                    if (!empty($countryList))
                                    {
                                        foreach ($countryList as $record)
                                        {?>
                                            <option value="<?php echo $record->id;?>"
                                            ><?php echo $record->name; ?>
                                            </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                   <!--  </div>

                    <div class="row"> -->

                        <div class="col-sm-4" id="is_permanent_state_view">
                            <div class="form-group">
                                <label>Permanent State <span class='error-text'>*</span></label>
                                <select name="permanent_state" id="permanent_state" class="form-control" style="width: 405px;">
                                    <option value="">Select</option>
                                    <?php
                                    if (!empty($stateList) && !empty($studentApplicatoinDetails))
                                    {
                                        foreach ($stateList as $record)
                                        {?>
                                     <option value="<?php echo $record->id;  ?>" <?php if($studentApplicatoinDetails->permanent_state==$record->id){ echo "selected"; } ?>>
                                        <?php echo $record->name;?>
                                     </option>
                                    <?php
                                        }
                                    }
                                    else
                                    if (!empty($stateList))
                                    {
                                        foreach ($stateList as $record)
                                        {?>
                                            <option value="<?php echo $record->id;?>"
                                            ><?php echo $record->name; ?>
                                            </option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4" id="is_permanent_city_view">
                            <div class="form-group">
                                <label>Permanent City <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="permanent_city" name="permanent_city" value="<?php 
                                if(!empty($studentApplicatoinDetails->permanent_city))
                                { 
                                    echo $studentApplicatoinDetails->permanent_city;
                                } ?>">
                            </div>
                        </div>

                        <div class="col-sm-4" id="is_permanent_zipcode_view">
                            <div class="form-group">
                                <label>Permanent Zipcode <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="permanent_zipcode" name="permanent_zipcode" value="<?php 
                                if(!empty($studentApplicatoinDetails->permanent_zipcode))
                                { 
                                    echo $studentApplicatoinDetails->permanent_zipcode;
                                } ?>">
                            </div>
                        </div>
                    </div>

                </div>


                <div class="button-block clearfix">
                    <div class="bttn-group pull-right">
                        <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="0">Save</button>
                    </div>
                </div>

                 
                 </div> <!-- END col-12 -->  
            </div>






            <div role="tabpanel" class="tab-pane" id="proficiency">
                <div class="col-12 mt-4">


                    
                <div class="form-container">
                <h4 class="form-group-title">Education Details</h4>

                    <div class="row">

                        <div class="col-sm-4" id="is_qualification_level_view">
                            <div class="form-group">
                                <label>Qualification Level <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="qualification_level" name="qualification_level">
                            </div>
                        </div>
                        <div class="col-sm-4" id="is_degree_awarded_view">
                            <div class="form-group">
                                <label>Degree Awarded <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="degree_awarded" name="degree_awarded" >
                            </div>
                        </div>
                        <div class="col-sm-4" id="is_specialization_view">
                            <div class="form-group">
                                <label>Major / Specialization <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="specialization" name="specialization">
                            </div>
                        </div>


                    <!-- </div>

                    <div class="row"> -->


                        <div class="col-sm-4" id="is_class_degree_view">
                            <div class="form-group">
                                <label>Class Degree <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="class_degree" name="class_degree"  >
                            </div>
                        </div>
                        <div class="col-sm-4" id="is_result_view">
                            <div class="form-group">
                                <label>Result / CGPA <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="result" name="result" >
                            </div>
                        </div>
                        <div class="col-sm-4" id="is_year_view">
                            <div class="form-group">
                                <label>Year Of Graduation <span class='error-text'>*</span></label>
                                <input type="number" class="form-control" id="year" name="year" >
                            </div>
                        </div>


                    <!-- </div>

                    <div class="row"> -->


                        <div class="col-sm-4" id="is_medium_view">
                            <div class="form-group">
                                <label>Medium Of Instruction <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="medium" name="medium" >
                            </div>
                        </div>
                        <div class="col-sm-4" id="is_college_country_view">
                            <div class="form-group">
                                <label>Country of University/College <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="college_country" name="college_country"  >
                            </div>
                        </div>
                        <div class="col-sm-4" id="is_college_name_view">
                            <div class="form-group">
                                <label>Name of University/College <span class='error-text'>*</span></label>
                                <input type="text" class="form-control" id="college_name" name="college_name" >
                            </div>
                        </div>


                    <!-- </div>

                    <div class="row"> -->

                        <!-- <div class="col-sm-4">
                            <div class="form-group">
                                <label>FILE <span class='error-text'>*</span></label>
                                <input type="file" name="image" />
                            </div>                    
                        </div> -->


                        <div class="col-sm-4" id="is_certificate_view">
                            <div class="form-group">
                                <label>Upload Certificate <span class='error-text'>*</span></label>
                                <input type="file" class="form-control" id="certificate" name="certificate" >
                            </div>
                        </div>
                        <div class="col-sm-4" id="is_transcript_view">
                            <div class="form-group">
                                <label>Upload Transcript <span class='error-text'>*</span></label>
                                <input type="file" class="form-control" id="transcript" name="transcript" >
                            </div>
                        </div>
                    </div>

                </div>



                <div class="button-block clearfix">
                    <div class="bttn-group pull-right">
                        <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="0">Save</button>
                        <!-- <button type="button" class="btn btn-primary btn-lg" onclick="submitApp()">Save</button> -->
                    </div>
                </div>



                <div class="form-container">
                <h4 class="form-group-title">Education Qualification Details</h4>

                    <div class="custom-table">
                      <table class="table" id="list-table">
                        <thead>
                          <tr>
                            <th>Sl. No</th>
                            <th>Qualification Level</th>
                            <th>Degree Awarded</th>
                            <th>Result / CGPA</th>
                            <th>Year Of Graduation</th>
                            <th>College Name</th>
                            <th class="text-center">Certificate</th>
                            <th class="text-center">Transcript</th>
                            <th class="text-center">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if (!empty($educationDetails))
                          {
                            $i=1;
                            foreach ($educationDetails as $record)
                            {
                          ?>
                              <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $record->qualification_level ?></td>
                                <td><?php echo $record->degree_awarded ?></td>
                                <td><?php echo $record->result ?></td>
                                <td><?php echo $record->year ?></td>
                                <td><?php echo $record->college_name ?></td>
                                <td class="text-center">

                                    <a href="<?php echo '/assets/images/' . $record->certificate; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->certificate; ?>)" title="View">
                                        <span style='font-size:18px;'>&#128065;</span>
                                    </a>

                                </td>
                                <td class="text-center">

                                    <a href="<?php echo '/assets/images/' . $record->transcript; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->transcript; ?>)" title="View">
                                    <span style='font-size:18px;'>&#128065;</span>
                                </a>

                                </td>
                                <td class="text-center">
                                    <a onclick="deleteEducationDetails(<?php echo $record->id; ?>)" title="Delete">
                                    <span style='font-size:18px;'>&#128465;</span>
                                    </a>
                                </td>
                              </tr>
                          <?php
                          $i++;
                            }
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>

                </div>

                
                             
                </div> <!-- END col-12 -->  
            </div>






            <div role="tabpanel" class="tab-pane" id="employment">
                <div class="col-12 mt-4">
                    <br>

                    <div class="form-container">
                    <h4 class="form-group-title">Family Details <span class='error-text'>*</span></h4>

                        <div class="row">

                            <div class="col-sm-4" id="is_father_name_view">
                                <div class="form-group">
                                    <label>Father Name <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="father_name" name="father_name" value="<?php 
                                    if(!empty($familyDetails->father_name))
                                    { 
                                        echo $familyDetails->father_name;
                                    } ?>">
                                    <input type="hidden" class="form-control" id="is_family_update" name="is_family_update" value="<?php 
                                    if(empty($familyDetails->father_name))
                                    { 
                                        echo 0;
                                    }
                                    else
                                    {
                                        echo $familyDetails->id; 
                                    } 
                                    ?>">
                                </div>
                            </div>    


                             <div class="col-sm-4" id="is_mother_name_view">
                                <div class="form-group">
                                    <label>Mother Name <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="mother_name" name="mother_name" value="<?php 
                                    if(!empty($familyDetails->mother_name))
                                    { 
                                        echo $familyDetails->mother_name;
                                    } ?>">
                                </div>
                            </div>


                            <div class="col-sm-4" id="is_no_siblings_view">
                                <div class="form-group">
                                    <label>No Of Siblings <span class='error-text'>*</span></label>
                                    <input type="number" class="form-control" id="no_siblings" name="no_siblings" value="<?php 
                                    if(!empty($familyDetails->no_siblings))
                                    { 
                                        echo $familyDetails->no_siblings;
                                    } ?>">
                                </div>
                            </div>     

                        <!-- </div>

                        <div class="row"> -->

                            <div class="col-sm-4" id="is_father_deceased_view">
                                <div class="form-group">
                                    <p>Father Deceased <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                    <?php
                                    if(!empty($familyDetails->father_deceased))
                                    { 
                                    ?>
                                      <input type="radio" name="father_deceased" id="sd1" value="1" 
                                      <?php
                                       if($familyDetails->father_deceased=='1')
                                        {
                                         echo "checked";
                                        }
                                        ?>
                                    ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="father_deceased" id="sd2" value="0" <?php if($familyDetails->father_deceased=='0'){ echo "checked";}?>><span class="check-radio"></span> No
                                    </label>  
                                    <?php
                                    }else
                                    {
                                        ?>
                                        <label class="radio-inline">
                                            <input type="radio" name="father_deceased" id="sd1" value="1"><span class="check-radio"></span> Yes
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="father_deceased" id="sd2" value="0" checked="checked"><span class="check-radio"></span> No
                                        </label>

                                        <?php
                                    } 
                                    ?>                            
                                </div>                         
                            </div>



                             <div class="col-sm-4" id="is_father_occupation_view">
                                <div class="form-group">
                                    <label>Father Occupation <span class='error-text'>*</span></label>
                                    <select name="father_occupation" id="father_occupation" class="form-control" style="width: 408px">
                                        <option value="">Select</option>

                                        <?php

                                if(!empty($familyDetails->father_occupation))
                                    {
                                    ?>

                                        <option value="Employee"
                                            <?php 
                                            if ($familyDetails->father_occupation == 'Employee')
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                                    <?php echo "Employee";  ?>
                                        </option>

                                        <option value="Government Servent"
                                            <?php 
                                            if ($familyDetails->father_occupation == 'Government Servent')
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                                    <?php echo "Government Servent";  ?>
                                        </option>
                                        <option value="Business"
                                            <?php 
                                            if ($familyDetails->father_occupation == 'Business')
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                                    <?php echo "Business";  ?>
                                        </option>
                                        <option value="Other"
                                            <?php 
                                            if ($familyDetails->father_occupation == 'Other')
                                            {
                                                echo "selected=selected";
                                            } ?>>
                                                    <?php echo "Other";  ?>
                                        </option>

                                    <?php
                                    }else
                                    {
                                        ?>
                                        <option value="Employee">Employee</option>
                                        <option value="Government Servent">Government Servent</option>
                                        <option value="Business">Business</option>
                                        <option value="Other">Other</option>
                                        
                                        <?php
                                    } 
                                    ?>          
                                    </select>
                                </div>
                            </div>

            


                            <div class="col-sm-4" id="is_est_fee_view">
                                <div class="form-group">
                                    <label>Estimated Fee Amount(RM) <span class='error-text'>*</span></label>
                                    <input type="number" class="form-control" id="est_fee" name="est_fee" min="1" value="<?php 
                                    if(!empty($familyDetails->est_fee))
                                    { 
                                        echo $familyDetails->est_fee;
                                    } ?>">
                                </div>
                            </div>  



                       <!--  </div>

                        <div class="row"> -->

                            <div class="col-sm-4" id="is_family_annual_income_view">
                                <div class="form-group">
                                    <label>Family Annual Income(RM) <span class='error-text'>*</span></label>
                                    <input type="number" class="form-control" id="family_annual_income" name="family_annual_income" value="<?php 
                                    if(!empty($familyDetails->family_annual_income))
                                    { 
                                        echo $familyDetails->family_annual_income;
                                    } ?>" min="1">

                                </div>
                            </div>  

                        </div>
                    </div>
                        
                    <div class="button-block clearfix">
                        <div class="bttn-group pull-right">
                        <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="0">Save</button>
                            <!-- <button type="button" class="btn btn-primary btn-lg" onclick="submitApp()">Save</button> -->
                        </div>
                    </div>

                    
                         
                </div> <!-- END col-12 -->  
            </div>





        
            <div role="tabpanel" class="tab-pane" id="profile">
                <div class="col-12 mt-4">
                    <br>

                    <!-- <div class="form-container">
                        <h4 class="form-group-title">Profile Details</h4>

                        <div class="row">

                            <div class="col-sm-4">
                            <div class="form-group">
                                <label>Salutation <span class='error-text'>*</span></label>
                                <select name="salutation" id="salutation" class="form-control" style="width: 405px;">
                                    <option value="">Select</option>
                                    <option value="Miss" <?php if($profileDetails->salutation=='Miss'){ echo "selected"; } ?>>Miss</option>
                                    <option value="Mr" <?php if($profileDetails->salutation=='Mr'){ echo "selected"; } ?>>Mr</option>
                                    <option value="Mrs" <?php if($profileDetails->salutation=='Mrs'){ echo "selected"; } ?>>Mrs</option>
                                    <option value="Dr" <?php if($profileDetails->salutation=='Dr'){ echo "selected"; } ?>>Dr</option>
                                </select>
                            </div>
                            </div>



                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>First Name <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" value="<?php if(!empty($profileDetails->first_name)){ echo $profileDetails->first_name; } ?>" readonly>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Last Name <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="last_name" name="last_name" value="<?php if(!empty($profileDetails->last_name)){ echo $profileDetails->last_name; } ?>" readonly>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>ID Type <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="id_type" name="id_type" value="<?php if(!empty($profileDetails->id_type)){ echo $profileDetails->id_type; } ?>">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>MyKad Passport National ID Military ID/Police ID MyPR ID Number <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="passport_number" name="passport_number" value="<?php if(!empty($profileDetails->passport_number)){ echo $profileDetails->passport_number; } ?>">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Passport Expiry Date <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control datepicker" autocomplete="off" id="passport_expiry_date" name="passport_expiry_date" value="<?php if(!empty($profileDetails->passport_expiry_date)){ echo date('d-m-Y', strtotime($profileDetails->passport_expiry_date)); } ?>">
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Phone Number <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $profileDetails->phone ?>">
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Gender <span class='error-text'>*</span></label>
                                    <select class="form-control" id="gender" name="gender" style="width: 405px;">
                                        <option value="">SELECT</option>
                                        <option value="Male" <?php if($profileDetails->gender == "Male"){ echo "selected=selected"; } ?>>MALE</option>
                                        <option value="Female" <?php if($profileDetails->gender == "Female"){ echo "selected=selected"; } ?>>FEMALE</option>
                                        <option value="Others" <?php if($profileDetails->gender == "Others"){ echo "selected=selected"; } ?>>OTHERS</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Date Of Birth <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" value="<?php if(!empty($profileDetails->date_of_birth)){ echo date('d-m-Y', strtotime($profileDetails->date_of_birth)); } ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Martial Status <span class='error-text'>*</span></label>
                                    <select id="martial_status" name="martial_status" class="form-control" style="width: 405px;">
                                        <option value="">SELECT</option>
                                        <option value="Single" <?php if($profileDetails->martial_status=='Single'){ echo "selected"; } ?>>SINGLE</option>
                                        <option value="Married" <?php if($profileDetails->martial_status=='Married'){ echo "selected"; } ?>>MARRIED</option>
                                        <option value="Divorced" <?php if($profileDetails->martial_status=='Divorced'){ echo "selected"; } ?>>DIVORCED</option>
                                    </select>
                                </div>
                            </div>


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Religion <span class='error-text'>*</span></label>
                                    <select name="religion" id="religion" class="form-control" style="width: 405px;">
                                        <option value="">Select</option>
                                        <?php
                                        if (!empty($religionList))
                                        {
                                            foreach ($religionList as $record)
                                            {?>
                                                <option value="<?php echo $record->id;  ?>"
                                                    <?php 
                                                    if($record->id == $profileDetails->religion)
                                                    {
                                                        echo "selected=selected";
                                                    } ?>>
                                                    <?php echo $record->name;  ?>
                                                </option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>



                            <div class="col-sm-4">
                                <div class="form-group">
                                <label>Type Of Nationality <span class='error-text'>*</span></label>

                                 <select name="nationality" id="nationality" class="form-control" style="width: 405px;">
                                    <option value="">Select</option>
                                    <option value="<?php echo 'Malaysian';?>"
                                        <?php 
                                        if ($profileDetails->nationality == 'Malaysian')
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                                <?php echo "Malaysian";  ?>
                                    </option>

                                    <option value="<?php echo 'Other';?>"
                                        <?php 
                                        if ($profileDetails->nationality == 'Other')
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                                <?php echo "Other";  ?>
                                    </option>
                                </select>


                                </div>
                            </div>

                        </div>


                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Race <span class='error-text'>*</span></label>
                                    <select name="id_race" id="id_race" class="form-control" style="width: 405px;">
                                        <option value="">Select</option>
                                        <?php
                                        if (!empty($raceList))
                                        {
                                            foreach ($raceList as $record)
                                            {?>
                                        <option value="<?php echo $record->id;  ?>" <?php if($profileDetails->id_race==$record->id){ echo "selected"; } ?>>
                                            <?php echo $record->name;?>
                                        </option>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                           

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Email ID <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="email_id" name="email_id" value="<?php if(!empty($profileDetails->email_id)){ echo $profileDetails->email_id; } ?>" readonly>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>NRIC <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="nric" name="nric" value="<?php echo $profileDetails->nric ?>" readonly>
                                </div>
                            </div>

                        </div>

                     <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Upload Employment Letter <span class='error-text'>*</span></label>
                                <input type="file" class="form-control" id="employment_letter" name="employment_letter">
                            </div>
                        </div>

                     </div>

                    </div> -->


                

                    <div class="button-block clearfix">
                        <div class="bttn-group pull-right">
                        <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="0">Save</button>
                            <!-- <button type="button" class="btn btn-primary btn-lg" onclick="submitApp()">Save</button> -->
                        </div>
                    </div>
                 
                </div> <!-- END col-12 -->  
            </div>







            <div role="tabpanel" class="tab-pane" id="visa">
                <div class="col-12 mt-4">
                    <br>

                <div class="form-container">
                    <h4 class="form-group-title">Upload File Details</h4>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Upload Employment Letter <span class='error-text'>*</span></label>
                                <input type="file" class="form-control" id="employment_letter" name="employment_letter"  >
                            </div>
                        </div>

                     </div>                  

                </div>


                <div class="button-block clearfix">
                    <div class="bttn-group pull-right">
                        <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="0">Save</button>
                        <!-- <button type="button" class="btn btn-primary btn-lg" onclick="submitApp()">Save</button> -->
                    </div>
                </div>
                 
                </div> <!-- END col-12 -->  
            </div>





            <div role="tabpanel" class="tab-pane" id="other">

                <div class="col-12 mt-4">
                    <br>

                    <div class="form-container">
                    
                    <h4 class="form-group-title">Employmet Details <span class='error-text'>*</span></h4>

                        <div class="row">

                            <div class="col-sm-4" id="is_company_name_view">
                                <div class="form-group">
                                    <label>Company Name <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="company_name" name="company_name">
                                </div>
                            </div>
                            <div class="col-sm-4" id="is_company_address_view">
                                <div class="form-group">
                                    <label>Company Address <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="company_address" name="company_address" >
                                </div>
                            </div>
                            <div class="col-sm-4" id="is_telephone_view">
                                <div class="form-group">
                                    <label>Telephone Number <span class='error-text'>*</span></label>
                                    <input type="number" class="form-control" id="telephone" name="telephone">
                                </div>
                            </div>

                       <!--  </div>

                        <div class="row"> -->

                            <div class="col-sm-4" id="is_fax_num_view">
                                <div class="form-group">
                                    <label>Fax Number <span class='error-text'>*</span></label>
                                    <input type="number" class="form-control" id="fax_num" name="fax_num">
                                </div>
                            </div>
                            <div class="col-sm-4" id="is_designation_view">
                                <div class="form-group">
                                    <label>Designation <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="designation" name="designation" >
                                </div>
                            </div>
                            <div class="col-sm-4" id="is_position_view">
                                <div class="form-group">
                                    <label>Position Level <span class='error-text'>*</span></label>
                                    <select class="form-control" id="position" name="position" style="width: 405px;">
                                        <option value="">SELECT</option>
                                        <option value="Senior Manager">Senior Manager</option>
                                        <option value="Manager">Manager</option>
                                        <option value="Senior Executive">Senior Executive</option>
                                        <option value="Junior Executive">Junior Executive</option>
                                        <option value="Non-Executive">Non-Executive</option>
                                        <option value="Fresh Entry">Fresh Entry</option>
                                    </select>
                                </div>
                            </div>

                      <!--   </div>

                        <div class="row"> -->

                            <div class="col-sm-4" id="is_service_year_view">
                                <div class="form-group">
                                    <label>Year Of Service <span class='error-text'>*</span></label>
                                    <input type="number" class="form-control" id="service_year" name="service_year">
                                </div>
                            </div>
                            <div class="col-sm-4" id="is_industry_view">
                                <div class="form-group">
                                    <label>Industry <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="industry" name="industry" >
                                </div>
                            </div>
                            <div class="col-sm-4" id="is_job_desc_view">
                                <div class="form-group">
                                    <label>Job Description <span class='error-text'>*</span></label>
                                    <input type="text" class="form-control" id="job_desc" name="job_desc">
                                </div>
                            </div>

                        <!-- </div>

                        <div class="row"> -->

                            <div class="col-sm-4" id="is_employment_letter_view">
                                <div class="form-group">
                                    <label>Upload Employment Letter <span class='error-text'>*</span></label>
                                    <input type="file" class="form-control" id="employment_letter" name="employment_letter"  >
                                </div>
                            </div>
                        </div>
                    
                    </div>
                        
                    
                    <div class="button-block clearfix">
                        <div class="bttn-group pull-right">
                        <button type="submit" class="btn btn-primary btn-lg" name="btn_submit" value="0">Save</button>
                            <!-- <button type="submit" class="btn btn-primary btn-lg" >Save</button> -->
                        </div>
                    </div>

                    <div class="form-container">
                        <h4 class="form-group-title">Employment Details</h4>  

                        <div class="custom-table">
                          <table class="table" id="list-table">
                            <thead>
                              <tr>
                                <th>Sl. No</th>
                                <th>Company Name</th>
                                <th>Company Address</th>
                                <th>Designation</th>
                                <th>Position</th>
                                <th>Year Of Service</th>
                                <th class="text-center">Employment Letter</th>
                                <th class="text-center">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              if (!empty($employmentDetails))
                              {
                                $i=1;
                                foreach ($employmentDetails as $record) {
                              ?>
                                  <tr>
                                    <td><?php echo $i ?></td>
                                    <td><?php echo $record->company_name ?></td>
                                    <td><?php echo $record->company_address ?></td>
                                    <td><?php echo $record->designation ?></td>
                                    <td><?php echo $record->position ?></td>
                                    <td><?php if($record->service_year == ""){ echo "0";} else {
                                        echo $record->service_year; } ?></td>

                                    <td class="text-center">

                                        <a href="<?php echo '/assets/images/' . $record->employment_letter; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->employment_letter; ?>)" title="View">
                                        <span style='font-size:18px;'>&#128065;</span>
                                    </a>

                                    </td>


                                    <td class="text-center">
                                        <a onclick="deleteEmploymentDetails(<?php echo $record->id; ?>)" title="Delete">
                                        <span style='font-size:18px;'>&#128465;</span>
                                        </a>
                                    </td>
                                  </tr>
                              <?php
                              $i++;
                                }
                              }
                              ?>
                            </tbody>
                          </table>
                        </div>

                    </div>


                 
                </div> <!-- END col-12 -->  
            </div>
            


        </div>


       </div> <!-- END row-->
   
    </form>



    <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
    </footer>

    </div>
</div>
<script type="text/javascript">

    function getRequirementsByProgramId()
    {
        var id_scholarship_program;
        id_scholarship_program = $("#id_program").val();

        // alert(id_scholarship_program);
        if(id_scholarship_program != '')
        {
            $.get("/scholarship_applicant/scholarshipApplication/getRequirementsByIdProgram/"+id_scholarship_program, function(data, status){
           
                $("#view_data").html(data);
                // $("#view_data").show();

                var is_financial_recoverable_details = $("#financial_recoverable_details").val();
                var is_education_details = $("#education_details").val();
                var is_family_details = $("#family_details").val();
                var is_file_upload_details = $("#file_upload_details").val();
                var is_employment_details = $("#employment_details").val();



                var is_salutation = $("#is_salutation").val();
                var is_first_name = $("#is_first_name").val();
                var is_last_name = $("#is_last_name").val();
                var is_nric = $("#is_nric").val();
                var is_passport = $("#is_passport").val();
                var is_phone = $("#is_phone").val();
                var is_email_id = $("#is_email_id").val();
                var is_passport_expiry_date = $("#is_passport_expiry_date").val();
                var is_gender = $("#is_gender").val();
                var is_date_of_birth = $("#is_date_of_birth").val();
                var is_martial_status = $("#is_martial_status").val();
                var is_religion = $("#is_religion").val();
                var is_nationality = $("#is_nationality").val();
                var is_id_race = $("#is_id_race").val();
                var is_mail_address1 = $("#is_mail_address1").val();
                var is_mail_address2 = $("#is_mail_address2").val();
                var is_mailing_country = $("#is_mailing_country").val();
                var is_mailing_state = $("#is_mailing_state").val();
                var is_mailing_city = $("#is_mailing_city").val();
                var is_mailing_zipcode = $("#is_mailing_zipcode").val();
                var is_permanent_address1 = $("#is_permanent_address1").val();
                var is_permanent_address2 = $("#is_permanent_address2").val();
                var is_permanent_country = $("#is_permanent_country").val();
                var is_permanent_state = $("#is_permanent_state").val();
                var is_permanent_city = $("#is_permanent_city").val();
                var is_permanent_zipcode = $("#is_permanent_zipcode").val();
                var is_id_type = $("#is_id_type").val();
                var is_passport_number = $("#is_passport_number").val();

                var is_qualification_level = $("#is_qualification_level").val();
                var is_degree_awarded = $("#is_degree_awarded").val();
                var is_specialization = $("#is_specialization").val();
                var is_class_degree = $("#is_class_degree").val();
                var is_result = $("#is_result").val();
                var is_year = $("#is_year").val();
                var is_medium = $("#is_medium").val();
                var is_college_country = $("#is_college_country").val();
                var is_college_name = $("#is_college_name").val();
                var is_certificate = $("#is_certificate").val();
                var is_transcript = $("#is_transcript").val();

                var is_father_name = $("#is_father_name").val();
                var is_mother_name = $("#is_mother_name").val();
                var is_father_deceased = $("#is_father_deceased").val();
                var is_father_occupation = $("#is_father_occupation").val();
                var is_no_siblings = $("#is_no_siblings").val();
                var is_est_fee = $("#is_est_fee").val();
                var is_family_annual_income = $("#is_family_annual_income").val();

                var is_company_name = $("#is_company_name").val();
                var is_company_address = $("#is_company_address").val();
                var is_telephone = $("#is_telephone").val();
                var is_fax_num = $("#is_fax_num").val();
                var is_designation = $("#is_designation").val();
                var is_position = $("#is_position").val();
                var is_service_year = $("#is_service_year").val();
                var is_industry = $("#is_industry").val();
                var is_job_desc = $("#is_job_desc").val();
                var is_employment_letter = $("#is_employment_letter").val();

                // alert(is_first_name);

                // alert(is_financial_recoverable_details);
                if(is_financial_recoverable_details == 0)
                {
                    $("#is_financial_recoverable_details").hide();
                }
                else if(is_financial_recoverable_details == 1)
                {
                    $("#is_financial_recoverable_details").show();
                }


                if(is_education_details == 0)
                {
                    $("#is_education_details").hide();
                }else
                if(is_education_details == 1)
                {
                    $("#is_education_details").show();
                }


                if(is_family_details == 0)
                {
                    $("#is_family_details").hide();
                }
                else if(is_family_details == 1)
                {
                    $("#is_family_details").show();
                }


                if(is_file_upload_details == 0)
                {
                    $("#is_file_upload_details").hide();
                }
                else if(is_file_upload_details == 1)
                {
                    $("#is_file_upload_details").show();
                }


                if(is_employment_details == 0)
                {
                    $("#is_employment_details").hide();
                }
                else if(is_employment_details == 1)
                {
                    $("#is_employment_details").show();
                }


                if(is_salutation == 0)
                {
                    $("#is_salutation_view").hide();
                }
                else if(is_salutation == 1)
                {
                    $("#is_salutation_view").show();
                }


                if(is_first_name == 0)
                {
                    $("#is_first_name_view").hide();
                }
                else if(is_first_name == 1)
                {
                    $("#is_first_name_view").show();
                }


                if(is_last_name == 0)
                {
                    $("#is_last_name_view").hide();
                }
                else if(is_last_name == 1)
                {
                    $("#is_last_name_view").show();
                }


                if(is_nric == 0)
                {
                    $("#is_nric_view").hide();
                }
                else if(is_nric == 1)
                {
                    $("#is_nric_view").show();
                }


                if(is_passport == 0)
                {
                    $("#is_passport_view").hide();
                }
                else if(is_passport == 1)
                {
                    $("#is_passport_view").show();
                }


                if(is_phone == 0)
                {
                    $("#is_phone_view").hide();
                }
                else if(is_phone == 1)
                {
                    $("#is_phone_view").show();
                }


                if(is_email_id == 0)
                {
                    $("#is_email_id_view").hide();
                }
                else if(is_email_id == 1)
                {
                    $("#is_email_id_view").show();
                }


                if(is_passport_expiry_date == 0)
                {
                    $("#is_passport_expiry_date_view").hide();
                }
                else if(is_passport_expiry_date == 1)
                {
                    $("#is_passport_expiry_date_view").show();
                }


                if(is_gender == 0)
                {
                    $("#is_gender_view").hide();
                }
                else if(is_gender == 1)
                {
                    $("#is_gender_view").show();
                }


                if(is_date_of_birth == 0)
                {
                    $("#is_date_of_birth_view").hide();
                }
                else if(is_date_of_birth == 1)
                {
                    $("#is_date_of_birth_view").show();
                }


                if(is_martial_status == 0)
                {
                    $("#is_martial_status_view").hide();
                }
                else if(is_martial_status == 1)
                {
                    $("#is_martial_status_view").show();
                }


                if(is_religion == 0)
                {
                    $("#is_religion_view").hide();
                }
                else if(is_religion == 1)
                {
                    $("#is_religion_view").show();
                }


                if(is_nationality == 0)
                {
                    $("#is_nationality_view").hide();
                }
                else if(is_nationality == 1)
                {
                    $("#is_nationality_view").show();
                }


                if(is_id_race == 0)
                {
                    $("#is_id_race_view").hide();
                }
                else if(is_id_race == 1)
                {
                    $("#is_id_race_view").show();
                }


                if(is_mail_address1 == 0)
                {
                    $("#is_mail_address1_view").hide();
                }
                else if(is_mail_address1 == 1)
                {
                    $("#is_mail_address1_view").show();
                }


                if(is_mail_address2 == 0)
                {
                    $("#is_mail_address2_view").hide();
                }
                else if(is_mail_address2 == 1)
                {
                    $("#is_mail_address2_view").show();
                }


                if(is_mailing_country == 0)
                {
                    $("#is_mailing_country_view").hide();
                }
                else if(is_mailing_country == 1)
                {
                    $("#is_mailing_country_view").show();
                }


                if(is_mailing_state == 0)
                {
                    $("#is_mailing_state_view").hide();
                }
                else if(is_mailing_state == 1)
                {
                    $("#is_mailing_state_view").show();
                }


                if(is_mailing_city == 0)
                {
                    $("#is_mailing_city_view").hide();
                }
                else if(is_mailing_city == 1)
                {
                    $("#is_mailing_city_view").show();
                }


                if(is_mailing_zipcode == 0)
                {
                    $("#is_mailing_zipcode_view").hide();
                }
                else if(is_mailing_zipcode == 1)
                {
                    $("#is_mailing_zipcode_view").show();
                }


                if(is_permanent_address1 == 0)
                {
                    $("#is_permanent_address1_view").hide();
                }
                else if(is_permanent_address1 == 1)
                {
                    $("#is_permanent_address1_view").show();
                }


                if(is_permanent_address2 == 0)
                {
                    $("#is_permanent_address2_view").hide();
                }
                else if(is_permanent_address2 == 1)
                {
                    $("#is_permanent_address2_view").show();
                }


                if(is_permanent_country == 0)
                {
                    $("#is_permanent_country_view").hide();
                }
                else if(is_permanent_country == 1)
                {
                    $("#is_permanent_country_view").show();
                }


                if(is_permanent_state == 0)
                {
                    $("#is_permanent_state_view").hide();
                }
                else if(is_permanent_state == 1)
                {
                    $("#is_permanent_state_view").show();
                }


                if(is_permanent_city == 0)
                {
                    $("#is_permanent_city_view").hide();
                }
                else if(is_permanent_city == 1)
                {
                    $("#is_permanent_city_view").show();
                }


                if(is_permanent_zipcode == 0)
                {
                    $("#is_permanent_zipcode_view").hide();
                }
                else if(is_permanent_zipcode == 1)
                {
                    $("#is_permanent_zipcode_view").show();
                }


                if(is_id_type == 0)
                {
                    $("#is_id_type_view").hide();
                }
                else if(is_id_type == 1)
                {
                    $("#is_id_type_view").show();
                }


                if(is_passport_number == 0)
                {
                    $("#is_passport_number_view").hide();
                }
                else if(is_passport_number == 1)
                {
                    $("#is_passport_number_view").show();
                }


                if(is_qualification_level == 0)
                {
                    $("#is_qualification_level_view").hide();
                }
                else if(is_qualification_level == 1)
                {
                    $("#is_qualification_level_view").show();
                }


                if(is_degree_awarded == 0)
                {
                    $("#is_degree_awarded_view").hide();
                }
                else if(is_degree_awarded == 1)
                {
                    $("#is_degree_awarded_view").show();
                }



                if(is_specialization == 0)
                {
                    $("#is_specialization_view").hide();
                }
                else if(is_specialization == 1)
                {
                    $("#is_specialization_view").show();
                }


                if(is_class_degree == 0)
                {
                    $("#is_class_degree_view").hide();
                }
                else if(is_class_degree == 1)
                {
                    $("#is_class_degree_view").show();
                }


                if(is_result == 0)
                {
                    $("#is_result_view").hide();
                }
                else if(is_result == 1)
                {
                    $("#is_result_view").show();
                }


                if(is_year == 0)
                {
                    $("#is_year_view").hide();
                }
                else if(is_year == 1)
                {
                    $("#is_year_view").show();
                }


                if(is_medium == 0)
                {
                    $("#is_medium_view").hide();
                }
                else if(is_medium == 1)
                {
                    $("#is_medium_view").show();
                }


                if(is_college_country == 0)
                {
                    $("#is_college_country_view").hide();
                }
                else if(is_college_country == 1)
                {
                    $("#is_college_country_view").show();
                }


                if(is_college_name == 0)
                {
                    $("#is_college_name_view").hide();
                }
                else if(is_college_name == 1)
                {
                    $("#is_college_name_view").show();
                }


                if(is_certificate == 0)
                {
                    $("#is_certificate_view").hide();
                }
                else if(is_certificate == 1)
                {
                    $("#is_certificate_view").show();
                }


                if(is_transcript == 0)
                {
                    $("#is_transcript_view").hide();
                }
                else if(is_transcript == 1)
                {
                    $("#is_transcript_view").show();
                }


                if(is_father_name == 0)
                {
                    $("#is_father_name_view").hide();
                }
                else if(is_father_name == 1)
                {
                    $("#is_father_name_view").show();
                }


                if(is_mother_name == 0)
                {
                    $("#is_mother_name_view").hide();
                }
                else if(is_mother_name == 1)
                {
                    $("#is_mother_name_view").show();
                }


                if(is_father_deceased == 0)
                {
                    $("#is_father_deceased_view").hide();
                }
                else if(is_father_deceased == 1)
                {
                    $("#is_father_deceased_view").show();
                }


                if(is_father_occupation == 0)
                {
                    $("#is_father_occupation_view").hide();
                }
                else if(is_father_occupation == 1)
                {
                    $("#is_father_occupation_view").show();
                }


                if(is_no_siblings == 0)
                {
                    $("#is_no_siblings_view").hide();
                }
                else if(is_no_siblings == 1)
                {
                    $("#is_no_siblings_view").show();
                }


                if(is_est_fee == 0)
                {
                    $("#is_est_fee_view").hide();
                }
                else if(is_est_fee == 1)
                {
                    $("#is_est_fee_view").show();
                }


                if(is_family_annual_income == 0)
                {
                    $("#is_family_annual_income_view").hide();
                }
                else if(is_family_annual_income == 1)
                {
                    $("#is_family_annual_income_view").show();
                }


                if(is_company_name == 0)
                {
                    $("#is_company_name_view").hide();
                }
                else if(is_company_name == 1)
                {
                    $("#is_company_name_view").show();
                }


                if(is_company_address == 0)
                {
                    $("#is_company_address_view").hide();
                }
                else if(is_company_address == 1)
                {
                    $("#is_company_address_view").show();
                }


                if(is_telephone == 0)
                {
                    $("#is_telephone_view").hide();
                }
                else if(is_telephone == 1)
                {
                    $("#is_telephone_view").show();
                }


                if(is_fax_num == 0)
                {
                    $("#is_fax_num_view").hide();
                }
                else if(is_fax_num == 1)
                {
                    $("#is_fax_num_view").show();
                }


                if(is_designation == 0)
                {
                    $("#is_designation_view").hide();
                }
                else if(is_designation == 1)
                {
                    $("#is_designation_view").show();
                }


                if(is_position == 0)
                {
                    $("#is_position_view").hide();
                }
                else if(is_position == 1)
                {
                    $("#is_position_view").show();
                }


                if(is_service_year == 0)
                {
                    $("#is_service_year_view").hide();
                }
                else if(is_service_year == 1)
                {
                    $("#is_service_year_view").show();
                }


                if(is_industry == 0)
                {
                    $("#is_industry_view").hide();
                }
                else if(is_industry == 1)
                {
                    $("#is_industry_view").show();
                }


                if(is_job_desc == 0)
                {
                    $("#is_job_desc_view").hide();
                }
                else if(is_job_desc == 1)
                {
                    $("#is_job_desc_view").show();
                }


                if(is_employment_letter == 0)
                {
                    $("#is_employment_letter_view").hide();
                }
                else if(is_employment_letter == 1)
                {
                    $("#is_employment_letter_view").show();
                }

                // hideShow();
            });
        }
    }



    function hideShow()
    {

    }



    $('select').select2();

    $( function()
    {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "1920:2019"
    });
    });


    function deleteEducationDetails(id)
    {
        $.ajax(
        {
           url: '/scholarship_applicant/editProfile/deleteEducationDetails/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            // alert(result);
                window.location.reload();
           }
        });
    }


    function deleteEmploymentDetails(id)
    {
         $.ajax(
        {
           url: '/scholarship_applicant/editProfile/deleteEmploymentDetails/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            // alert(result);
                window.location.reload();
           }
        });
    }

    function submitApp()
    {
        if($('#form_profile').valid())
        {    
            $('#id_program').prop('disabled', false);
            $('#form_profile').submit();
        }
    }




    $(document).ready(function() {
        $("#form_profile").validate({
            rules: {
                qualification_level: {
                    required: true
                },
                degree_awarded: {
                    required: true
                },
                specialization: {
                    required: true
                },
                class_degree: {
                    required: true
                },
                result: {
                    required: true
                },
                year: {
                    required: true
                },
                medium: {
                    required: true
                },
                college_country: {
                    required: true
                },
                college_name: {
                    required: true
                },
                test: {
                    required: true
                },
                date: {
                    required: true
                },
                score: {
                    required: true
                },
                company_name: {
                    required: true
                },
                company_address: {
                    required: true
                },
                telephone: {
                    required: true
                },
                fax_num: {
                    required: true
                },
                designation: {
                    required: true
                },
                position: {
                    required: true
                },
                service_year: {
                    required: true
                },
                industry: {
                    required: true
                },
                job_desc: {
                    required: true
                },
                salutation: {
                    required: true
                },
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                id_type: {
                    required: true
                },
                passport_number: {
                    required: true
                },
                passport_expiry_date: {
                    required: true
                },
                phone: {
                    required: true
                },
                gender: {
                    required: true
                },
                date_of_birth: {
                    required: true
                },
                martial_status: {
                    required: true
                },
                religion: {
                    required: true
                },
                nationality: {
                    required: true
                },
                id_race: {
                    required: true
                },
                email_id: {
                    required: true
                },
                nric: {
                    required: true
                },
                mail_address1: {
                    required: true
                },
                mail_address2: {
                    required: true
                },
                mailing_country: {
                    required: true
                },
                mailing_state: {
                    required: true
                },
                mailing_city: {
                    required: true
                },
                mailing_zipcode: {
                    required: true
                },
                permanent_address1: {
                    required: true
                },
                permanent_address2: {
                    required: true
                },
                permanent_country: {
                    required: true
                },
                permanent_state: {
                    required: true
                },
                permanent_city: {
                    required: true
                },
                permanent_zipcode: {
                    required: true
                },
                malaysian_visa: {
                    required: true
                },
                visa_expiry_date: {
                    required: true
                },
                visa_status: {
                    required: true
                },
                doc_name: {
                    required: true
                },
                remarks: {
                    required: true
                },
                id_program: {
                    Required: true
                },
                certificate: {
                    Required: true
                }
            },
            messages: {
                qualification_level: {
                    required: "<p class='error-text'>Qualification Level Required</p>",
                },
                degree_awarded: {
                    required: "<p class='error-text'>Degree Awarded Required</p>",
                },
                specialization: {
                    required: "<p class='error-text'>Specialization Required</p>",
                },
                class_degree: {
                    required: "<p class='error-text'>Degree Class Required</p>",
                },
                result: {
                    required: "<p class='error-text'>Result Required</p>",
                },
                year: {
                    required: "<p class='error-text'>Year Requred</p>",
                },
                medium: {
                    required: "<p class='error-text'>Select Medium Of Study</p>",
                },
                college_country: {
                    required: "<p class='error-text'>Country Required</p>",
                },
                college_name: {
                    required: "<p class='error-text'>College Name Required</p>",
                },
                test: {
                    required: "<p class='error-text'>Test Name Required</p>",
                },
                date: {
                    required: "<p class='error-text'>Select Date</p>",
                },
                score: {
                    required: "<p class='error-text'>Score Required</p>",
                },
                company_name: {
                    required: "<p class='error-text'>Company Name Required</p>",
                },
                company_address: {
                    required: "<p class='error-text'>Company Address Required</p>",
                },
                telephone: {
                    required: "<p class='error-text'>Telephone No. Required</p>",
                },
                fax_num: {
                    required: "<p class='error-text'>Fax No. Required</p>",
                },
                designation: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                position: {
                    required: "<p class='error-text'>Position Of Rhe Job Required</p>",
                },
                service_year: {
                    required: "<p class='error-text'>No. Of Service Required</p>",
                },
                industry: {
                    required: "<p class='error-text'>Industry Required</p>",
                },
                job_desc: {
                    required: "<p class='error-text'>Job Description Required</p>",
                },
                first_name: {
                    required: "<p class='error-text'>First Name</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name Required</p>",
                },
                id_type: {
                    required: "<p class='error-text'>ID Type Required</p>",
                },
                passport_number: {
                    required: "<p class='error-text'>Passport NUmber Required</p>",
                },
                passport_expiry_date: {
                    required: "<p class='error-text'>Select Passport Expire Date</p>",
                },
                phone: {
                    required: "<p class='error-text'>Phone Number Required</p>",
                },
                gender: {
                    required: "<p class='error-text'>Select Gender</p>",
                },
                date_of_birth: {
                    required: "<p class='error-text'>Select Date Of Birth</p>",
                },
                martial_status: {
                    required: "<p class='error-text'>Select Maritual Status</p>",
                },
                religion: {
                    required: "<p class='error-text'>Select Religion</p>",
                },
                nationality: {
                    required: "<p class='error-text'>Select Ntionality</p>",
                },
                id_race: {
                    required: "<p class='error-text'>Select Rece</p>",
                },
                email_id: {
                    required: "<p class='error-text'>Main Id Required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC Required</p>",
                },
                mail_address1: {
                    required: "<p class='error-text'>Mailing Address 1 Required</p>",
                },
                mail_address2: {
                    required: "<p class='error-text'>Mailing Address 2 Required</p>",
                },
                mailing_country: {
                    required: "<p class='error-text'>Select Mailing Country</p>",
                },
                mailing_state: {
                    required: "<p class='error-text'>Select Mailing State</p>",
                },
                mailing_city: {
                    required: "<p class='error-text'>Mailing City Required</p>",
                },
                mailing_zipcode: {
                    required: "<p class='error-text'>Mailing Zipcode Required</p>",
                },
                permanent_address1: {
                    required: "<p class='error-text'>Select Permanent Address 2</p>",
                },
                permanent_address2: {
                    required: "<p class='error-text'>Select Permanent Address 2</p>",
                },
                permanent_country: {
                    required: "<p class='error-text'>Select Permanent Country</p>",
                },
                permanent_state: {
                    required: "<p class='error-text'>Select Permanent State</p>",
                },
                permanent_city: {
                    required: "<p class='error-text'>Permanent City Required</p>",
                },
                permanent_zipcode: {
                    required: "<p class='error-text'>Permanent Zipcode Required</p>",
                },
                malaysian_visa: {
                    required: "<p class='error-text'>Select Visa Type</p>",
                },
                visa_expiry_date: {
                    required: "<p class='error-text'>Select Visa Expire Date</p>",
                },
                visa_status: {
                    required: "<p class='error-text'>Visa Status Required</p>",
                },
                doc_name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                remarks: {
                    required: "<p class='error-text'>Remars Required</p>"
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>"
                },
                certificate: {
                    required: "<p class='error-text'>Select Certificate</p>"
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    

  </script>
<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>View Invoice</h3>
            <!-- <a href="../list" class="btn btn-link btn-back">‹ Back</a> -->
        </div>    
        <form id="form_main_invoice" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Student Details</h4>
                <div class='data-list'>
                    <div class='row'> 
                        <div class='col-sm-6'>
                            <dl>
                                <dt>Student Name :</dt>
                                <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                            </dl>
                            <dl>
                                <dt>Student NRIC :</dt>
                                <dd><?php echo $studentDetails->nric ?></dd>
                            </dl>
                            <dl>
                                <dt>Student Email :</dt>
                                <dd><?php echo $studentDetails->email_id; ?></dd>
                            </dl>
                             <dl>
                                <dt>Nationality :</dt>
                                <dd><?php echo $studentDetails->nationality ?></dd>
                            </dl>                            
                        </div>        
                        
                        <div class='col-sm-6'>                           
                            <dl>
                                <dt>Intake :</dt>
                                <dd><?php echo $studentDetails->intake_name ?></dd>
                            </dl>
                            <dl>
                                <dt>Program :</dt>
                                <dd><?php echo $studentDetails->programme_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Qualification Type :</dt>
                                <dd><?php echo $studentDetails->qualification_code . " - " . $studentDetails->qualification_name; ?></dd>
                            </dl>
                            <dl>
                                <dt>Scholarship :</dt>
                                <dd><?php echo ""; ?></dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>






            <!-- <div style="border: 1px solid;border-radius: 12px;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <h4>Student Name : <?php echo ucwords($studentDetails->full_name); ?></h4>
                        <h4>Student ID : <?php echo $studentDetails->id ?></h4>
                        <h4>Email Id : <?php echo $studentDetails->email_id ?></h4>
                        <h4>NRIC : <?php echo $studentDetails->nric ?></h4>
                    </div>
                    <div class="col-sm-4">
                        <h4>Intake : <?php echo $studentDetails->intake_name ?></h4>
                        <h4>Student Category : </h4>
                        <h4>Program : <?php echo $studentDetails->programme_name ?></h4>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="form-container">
            <h4 class="form-group-title">Invoice Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Invoice</a>
                    </li>
                   <!--  <li role="presentation"><a href="#receipt" class="nav-link border rounded text-center"
                            aria-controls="receipt" role="tab" data-toggle="tab">Receipt</a>
                    </li>
                    <li role="presentation"><a href="#major" class="nav-link border rounded text-center"
                            aria-controls="major" role="tab" data-toggle="tab">Account Summary</a>
                    </li> -->
                </ul>

                
                <div class="tab-content offers-tab-content">
                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="mt-4">
                            <div class="custom-table" id="printInvoice">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Sl. No</th>
                                        <th>Invoice Number</th>
                                        <th>Invoice Type</th>
                                        <th>Invoice Total</th>
                                        <th>Total Payable</th>
                                        <th>Total Discount</th>
                                        <th>Balance </th>
                                        <th>Remarks</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($getInvoiceByStudentId))
                                    {
                                         $i=1;
                                        foreach ($getInvoiceByStudentId as $record)
                                        {
                                    ?>
                                        <tr>
                                            <td><?php echo $i ?></td>
                                            <td><?php echo $record->invoice_number ?></td>
                                            <td><?php echo $record->type ?></td>
                                            <td><?php echo $record->invoice_total ?></td>
                                            <td><?php echo $record->total_amount ?></td>
                                            <td><?php echo $record->total_discount ?></td>
                                            <td><?php if($record->balance_amount == ""){
                                                echo "0.00";
                                            } else { echo $record->balance_amount; } ?></td>
                                            <td><?php echo $record->remarks ?></td>

                                            <td><?php 
                                            if($record->status == "0")
                                            {
                                                echo "Pending";
                                            }
                                            elseif($record->status == "1")
                                            { 
                                                echo "Approved"; 
                                            }elseif($record->status == "2")
                                            { 
                                                echo "Rejected"; 
                                            } ?></td>

                                            <td class="">
                                            <a href="#" title="" onclick="printDiv('printInvoice')">Print</a> | <a href="<?php echo 'showInvoice/' . $record->id . '/invoice'; ?>" title="">View</a>
                                            </td>
                                        </tr>
                                    <?php
                                        $i++;
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div> 
                    </div>

                    <!-- <div role="tabpanel" class="tab-pane" id="receipt">
                        <div class="mt-4">
                            <div class="custom-table" id="printReceipt">
                                <table class="table" id="list-table">
                                    <thead>
                                    <tr>
                                        <th>Receipt Number</th>
                                        <th>Receipt Amount</th>
                                        <th>Remarks</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if (!empty($getReceiptByStudentId)) {
                                        foreach ($getReceiptByStudentId as $record) {
                                    ?>
                                        <tr>
                                            <td><?php echo $record->receipt_number ?></td>
                                            <td><?php if($record->receipt_amount == ""){
                                                echo "0.00";
                                            } else { echo $record->receipt_amount; } ?></td>
                                            <td><?php echo $record->remarks ?></td>
                                            <td class="">
                                            <a href="#" title="" onclick="printDiv('printReceipt')">Print</a>
                                            </td>
                                        </tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>

        </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>


</form>
<script type="text/javascript">
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}
</script>
<script>
    
    $('select').select2();

    function getStudentByProgramme(id)
    {

     $.get("/finance/mainInvoice/getStudentByProgrammeId/"+id, function(data, status){
   
        $("#student").html(data);
        // $("#view_programme_details").html(data);
        // $("#view_programme_details").show();
    });
 }

 function getStudentByStudentId(id)
 {

     $.get("/finance/mainInvoice/getStudentByStudentId/"+id, function(data, status){
   
        $("#view_student_details").html(data);
        $("#view_student_details").show();
    });
 }



    function opendialog()
    {
        $("#id_fee_item").val('');
        $("#amount").val('');
        $("#id").val('0');                    
        $('#myModal').modal('show');

    }
    function saveData() {


        var tempPR = {};
        tempPR['id_fee_item'] = $("#id_fee_item").val();
        tempPR['amount'] = $("#amount").val();
        tempPR['id'] = $("#id").val();
            $.ajax(
            {
               url: '/finance/mainInvoice/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
                var ta = $("#inv-total-amount").val();
                $("#total_amount").val(ta);
                $('#myModal').modal('hide');
               }
            });
        
    }

    function deleteTempData(id) {
         $.ajax(
            {
               url: '/finance/mainInvoice/tempDelete/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
               }
            });
    }


    function getTempData(id) {
        $.ajax(
            {
               url: '/finance/mainInvoice/tempedit/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(resultfromjson)
               {
                    result = JSON.parse(resultfromjson);
                    $("#dt_fund").val(result['dt_fund']);
                    $("#dt_department").val(result['dt_department']);
                    $("#id").val(id);
                    $('#myModal').modal('show');
               }
            });

    }


    $(document).ready(function() {
        $("#form_main_invoice").validate({
            rules: {
                total_amount: {
                    required: true
                },
                id_student: {
                    required: true
                },
                date_time: {
                    required: true
                }
            },
            messages: {
                total_amount: {
                    required: "Enter Total Amount",
                },
                id_student: {
                    required: "Select Student",
                },
                date_time: {
                    required: "Select Date ",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
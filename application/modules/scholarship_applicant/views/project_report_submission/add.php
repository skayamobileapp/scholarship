<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Project Report Submission</h3>
        </div>
        <form id="form_internship" action="" method="post">

            <div class="form-container">
                <h4 class="form-group-title">Project Report Submission Details</h4>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Description <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="description" name="description">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Duration (Months) <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="duration" name="duration" min="1" max="12">
                        </div>
                    </div>
                </div>

                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Start Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" autocomplete="off">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="to_dt" name="to_dt" autocomplete="off">
                        </div>
                    </div>

                   <!--  <div class="col-sm-4">
                        <div class="form-group">
                            <label>Zipcode <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="zipcode" name="zipcode">
                        </div>
                    </div>-->  

                </div>

            </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="list" class="btn btn-link">Cancel</a>
                </div>
            </div>

        </form>


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


<script>

    $('select').select2();

    function getCompanyByCompanyType(id)
    {
        if(id != '')
        {
        $.get("/student/internshipApplication/getCompanyByCompanyType/"+id,
            function(data, status)
            {
                $("#view_company").html(data);
                // $("#view_programme_details").html(data);
                // $("#view_programme_details").show();
            });
        }
    }



    $(document).ready(function() {
        $("#form_internship").validate({
            rules: {
                duration: {
                    required: true
                },
                 description: {
                    required: true
                },
                 name: {
                    required: true
                },
                 from_dt: {
                    required: true
                },
                 to_dt: {
                    required: true
                }
            },
            messages: {
                duration: {
                    required: "<p class='error-text'>Duration Reequired</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                from_dt: {
                    required: "<p class='error-text'>Select Start Date</p>",
                },
                to_dt: {
                    required: "<p class='error-text'>Select End Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(function(){
        $( ".datepicker" ).datepicker();
      });

</script>

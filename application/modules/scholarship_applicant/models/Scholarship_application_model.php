<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Scholarship_application_model extends CI_Model
{
    function schemeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_scheme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }


    function scholarshipListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_sub_thrust');
        $this->db->where('status', $status);
        $this->db->order_by("scholarship_name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function schemeListByStatusForApplicant($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_scheme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $results = $query->result();

        $details = array();
        foreach ($results as $result)
        {
            $result->id_application = 0;
            $result->is_applied = 0;

            $id_scholar_applicant = $this->session->id_scholar_applicant;
            $id_scheme = $result->id;

            $application = $this->checkSchemeAppliedByStudent($id_scheme, $id_scholar_applicant);
            if($application)
            {
                $result->id_application = $application->id;
                $result->is_applied = 1;

                if($application->is_submitted > 0)
                {
                    $result->is_applied = 2;
                }

                if($application->is_migrated > 0)
                {
                    $result->is_applied = 3;
                }

            }
            array_push($details, $result);    
            // echo "<Pre>"; print_r($application);exit();

        }    
        return $details;

    }

    function getSchemeById($id)
    {
        $this->db->select('sa.*');
        $this->db->from('scholarship_scheme as sa');
        // $this->db->join('scholarship_programme as sp', 'sa.id_program = sp.id');
        $this->db->where('sa.id', $id);
        $query = $this->db->get();
        $result = $query->row();      
        return $result;
    }

     function getStudentDetails($id)
    {
        $this->db->select('sa.*');
        $this->db->from('scholar_applicant as sa');
        // $this->db->join('scholarship_programme as sp', 'sa.id_program = sp.id');
        $this->db->where('sa.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getSchemesByProgramId($id_program)
    {
        $this->db->select('DISTINCT(icr.id) as id, icr.*');
        $this->db->from('scholarship_scheme as icr');
        $this->db->join('scheme_has_program as ichp', 'icr.id = ichp.id_scholarship_scheme');
        $this->db->where('ichp.id_program', $id_program);
        $this->db->where('icr.status', '1');
        // $this->db->where('icr.to_dt <', date('Y-m-d'));
        $query = $this->db->get();
        return $query->result();
    }

    function scholarshipListByIdscheme($id_scheme)
    {
        $this->db->select('DISTINCT(icr.id_sub_thrust) as id_sub_thrust');
        $this->db->from('scheme_has_program as icr');
        $this->db->join('scholarship_programme as p', 'icr.id_program = p.id','left');
        $this->db->join('scholarship_sub_thrust as ichp', 'icr.id_sub_thrust = ichp.id','left');
        $this->db->where('icr.id_scholarship_scheme', $id_scheme);
        $this->db->where('icr.id_scholarship_scheme', $id_scheme);
        // $this->db->where('icr.status', '1');
        // $this->db->where('icr.to_dt <', date('Y-m-d'));
        $query = $this->db->get();
        $results = $query->result();

        $details = array();
        foreach ($results as $result)
        {
            $id_sub_thrust = $result->id_sub_thrust;
            $sub_thrust = $this->getSubThrust($id_sub_thrust);
            
            array_push($details, $sub_thrust);
        }

        return $details;
    }


    function getProgramByScholarshipNCohortId($data)
    {
        $this->db->select('DISTINCT(icr.id_program) as id_program');
        $this->db->from('scheme_has_program as icr');
        $this->db->join('scholarship_programme as p', 'icr.id_program = p.id','left');
        $this->db->join('scholarship_sub_thrust as ichp', 'icr.id_sub_thrust = ichp.id','left');
        $this->db->where('icr.id_scholarship_scheme', $data['id_scheme']);
        $this->db->where('icr.id_sub_thrust', $data['id_scholarship']);
        // $this->db->where('icr.status', '1');
        // $this->db->where('icr.to_dt <', date('Y-m-d'));
        $query = $this->db->get();
        $results = $query->result();

        $details = array();
        foreach ($results as $result)
        {
            $id_program = $result->id_program;
            $program = $this->getProgramData($id_program);
            
            array_push($details, $program);
        }

        return $details;
    }


    function getSubThrust($id)
    {
        $this->db->select('sa.*');
        // $this->db->join('scholarship_programme as sp', 'sa.id_program = sp.id');
        $this->db->from('scholarship_sub_thrust as sa');
        $this->db->where('sa.id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function getProgramData($id)
    {
        $this->db->select('sa.*');
        // $this->db->join('scholarship_programme as sp', 'sa.id_program = sp.id');
        $this->db->from('scholarship_programme as sa');
        $this->db->where('sa.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getProgramBySchemeId($id_scheme)
    {
        $this->db->select('DISTINCT(sp.id) as id, sp.*');
        $this->db->from('scheme_has_program as icr');
        $this->db->join('scholarship_programme as sp', 'icr.id_program = sp.id');
        $this->db->where('icr.id_scholarship_scheme', $id_scheme);
        $this->db->where('sp.status', '1');
        // $this->db->where('icr.to_dt <', date('Y-m-d'));
        $query = $this->db->get();
        return $query->result();
    }

    
    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.name as programme_name, i.name as intake_name, qs.name as qualification_name, qs.code as qualification_code');
        $this->db->from('student as s');
        $this->db->join('programme as p', 's.id_program = p.id'); 
        $this->db->join('intake as i', 's.id_intake = i.id'); 
        $this->db->join('qualification_setup as qs', 's.id_degree_type = qs.id');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }

    function getScholarship($id)
    {
        $this->db->select('ia.*, ss.name as scholarship_name, ss.code as scholarship_code');
        $this->db->from('scholarship_application as ia');
        $this->db->join('scholarship_scheme as ss', 'ia.id_scholarship_scheme = ss.id');
        $this->db->where('ia.id', $id);
        $query = $this->db->get();
         return $query->row();
    }


	function getScholarshipApplicationListByStudentId($id_student)
	{
        $this->db->select('ia.*, ss.name as scholarship_name, ss.code as scholarship_code, st.name as status_, st.migration');
        $this->db->from('scholarship_application as ia');
        $this->db->join('status_list as st', 'ia.status = st.id','left');
        $this->db->join('scholarship_scheme as ss', 'ia.id_scholarship_scheme = ss.id','left');
        $this->db->where('ia.id_student', $id_student);
        $query = $this->db->get();
         return $query->result();
    }

    function getStatusHistoryByIdApplication($id_application)
    {
        $this->db->select('s.*, sch.name as user_name, st.name as status');
        $this->db->from('applicantion_status_change_history as s');
        $this->db->join('scholar as sch', 's.created_by = sch.id'); 
        $this->db->join('status_list as st', 's.id_status = st.id','left');
        $this->db->where('s.id_application', $id_application);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }


    function getScholarshipApplicationById($id)
    {
        $this->db->select('ia.*, ss.name as scholarship_name, ss.code as scholarship_code');
        $this->db->from('scholarship_application as ia');
        $this->db->join('scholarship_scheme as ss', 'ia.id_scholarship_scheme = ss.id');
        $this->db->where('ia.id', $id);
        $query = $this->db->get();
         return $query->row();
    }


    function getScholarshipApplicationByIdForView($id)
    {
        $this->db->select('ia.*, ss.name as scholarship_name, ss.code as scholarship_code, sl.name as status_');
        $this->db->from('scholarship_application as ia');
        $this->db->join('scholarship_scheme as ss', 'ia.id_scholarship_scheme = ss.id');
        $this->db->join('status_list as sl', 'ia.status = ia.id','left');
        $this->db->where('ia.id', $id);
        $query = $this->db->get();
         return $query->row();
    }

    

    function companyTypeList()
    {
        $this->db->select('*');
        $this->db->from('internship_company_type');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();      
        return $result;
    }

    function companyRegistrationListByStatus($status)
    {
        $this->db->select('invt.*');
        $this->db->from('internship_company_registration as invt');
        $this->db->where('invt.status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function applicantApprovalListByStatus($status)
    {
        $this->db->select('invt.*');
        $this->db->from('status_list as invt');
        $this->db->where('invt.status', $status);
        $this->db->where('invt.id_status_master', 1);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function companyRegistrationList()
    {
        $this->db->select('invt.*');
        $this->db->from('internship_company_registration as invt');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    

    function addScholarshipApplication($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_application', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getScholarshipApplication($id)
    {
        $this->db->select('ia.*, ss.name as scholarship_name, ss.code as scholarship_code');
        $this->db->from('scholarship_application as ia');
        $this->db->join('scholarship_scheme as ss', 'ia.id_scholarship_scheme = ss.id');
         $this->db->where('ia.id', $id);
        $query = $this->db->get();
          $result = $query->row();
         return $result; 
    }

    function getMaxLimit()
    {
        $this->db->select('ia.*');
        $this->db->from('internship_student_limit as ia');
        $this->db->order_by("id", "DESC");
        $query = $this->db->get();
          $result = $query->row();
         return $result; 
    }


    function generateScholarshipApplicationNumber()
    {
        $year = date('y');
        $Year = date('Y');
            $this->db->select('*');
            $this->db->from('scholarship_application');
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            $result = $query->num_rows();

     
            $count= $result + 1;
            $generated_number = "SCH" .(sprintf("%'06d", $count)). "/" . $Year;
           // echo "<Pre>";print_r($generated_number);exit();
           return $generated_number;
    }

    function checkSchemeAppliedByStudent($id_scheme,$id_scholar_applicant)
    {
        $this->db->select('ia.*');
        $this->db->from('scholarship_application as ia');
        $this->db->where('ia.id_student', $id_scholar_applicant);
        // $this->db->where('ia.id_program', $data['id_program']);
        $this->db->where('ia.id_scholarship_scheme', $id_scheme);
        // $this->db->where('ia.is_submitted', 0);
        $this->db->order_by("ia.id", "desc");
        $query = $this->db->get();
        $result = $query->row();

       return $result;
        // if($result)
        // {
        // }else
        // {
        //     return 0;
        // }
    }

    function checkStudentScholarshipApplication($data)
    {
        $year = date('Y');
        $this->db->select('ia.*');
        $this->db->from('scholarship_application as ia');
        $this->db->where('ia.id_student', $data['id_student']);
        $this->db->where('ia.id_program', $data['id_program']);
        $this->db->where('ia.id_intake', $data['id_intake']);
        $this->db->where('ia.year', $year);
        $this->db->where('ia.status', '1');
        $this->db->or_where('ia.status', '0');
        $this->db->order_by("ia.id", "desc");
        $query = $this->db->get();
        $result = $query->num_rows();

        // $max_limit = $this->getMaxLimit()->max_limit;

        // if($result >= $max_limit )
        // {
        //     $check = 1;
        // }
        return $result;
        // echo "<Pre>";print_r($result . " - " . $max_limit);exit();
    }


    function getScholarshipApplicationPersonalDetails($id_scholar_applicant,$id_scheme)
    {
        $this->db->select('ia.*');
        $this->db->from('scholarship_applicant as ia');
        $this->db->where('ia.id_scholar_applicant', $id_scholar_applicant);
        $this->db->where('ia.year', date('Y'));
        $this->db->where('ia.id_scholarship_scheme', $id_scheme);
        $this->db->order_by("ia.id", "desc");
        $query = $this->db->get();
        $result = $query->row();

        return $result;

    }




    function addExamDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_examination_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getExamDetails($id_scholar_applicant,$id_scholarship_scheme,$id_application)
    {
        $this->db->select('*');
        $this->db->from('scholarship_examination_details');
        $this->db->where('id_scholar_applicant', $id_scholar_applicant);
        $this->db->where('id_scholarship_scheme', $id_scholarship_scheme);
        $this->db->where('id_application', $id_application);
        $this->db->where('year', date('Y'));
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }


    function getScholarshipPersonelDetailsByIdApplication($id_application)
    {
        $this->db->select('*');
        $this->db->from('scholarship_applicant');
        $this->db->where('id_application', $id_application);
        $query = $this->db->get();
         $result = $query->row();
        return $result;
    }

     function deleteEducationDetails($id)
    {
        // echo "<Pre>";print_r($id);exit();
        $this->db->where('id', $id);
        $this->db->delete('scholarship_examination_details');
         return TRUE;
    }


    function updateScholarshipApplication($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_application', $data);
        return TRUE;
    }


    function updateFamilyDetails($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholar_applicant_family_details', $data);

        return TRUE;
    }


    function getFamilyDetails($id_scholar_applicant,$id_scholarship_scheme,$id_application)
    {
        $this->db->select('*');
        $this->db->from('scholar_applicant_family_details');
        $this->db->where('id_scholar_applicant', $id_scholar_applicant);
        $this->db->where('id_scholarship_scheme', $id_scholarship_scheme);
        $this->db->where('id_application', $id_application);
        // $this->db->where('year', date('Y'));
        $query = $this->db->get();
         $result = $query->row();
        return $result;
    }

    function getEmploymentDetails($id_scholar_applicant,$id_scholarship_scheme)
    {
        $this->db->select('*');
        $this->db->from('scholar_applicant_employment_status');
        $this->db->where('id_scholar_applicant', $id_scholar_applicant);
        $this->db->where('id_scholarship_scheme', $id_scholarship_scheme);
        // $this->db->where('year', date('Y'));
        // $this->db->where('id_application', 0);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }


    function addEmploymentDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholar_applicant_employment_status', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    
    function deleteEmploymentDetails($id)
    {
        // echo "<Pre>";print_r($id);exit();
        $this->db->where('id', $id);
        $this->db->delete('scholar_applicant_employment_status');
         return TRUE;
    }

    function updatePersonalData($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_applicant', $data);
        return TRUE;
    }

    function updatePersonalDataByIdApplication($data,$id)
    {
        $this->db->where('id_application', $id);
        $this->db->update('scholarship_applicant', $data);
        return TRUE;

    }

    function addPersonalData($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_applicant', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function addFamilyDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholar_applicant_family_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function updateApplicationIdToPersonalDetails($data)
    {
        $id_scholar_applicant = $data['id_scholar_applicant'];
        $id_application = $data['id_application'];
        $id_scholarship_scheme = $data['id_scholarship_scheme'];
        $year = $data['year'];
        $previous_application = $data['previous_application'];
        unset($data['previous_application']);

        $this->db->where('id_scholar_applicant', $id_scholar_applicant);
        $this->db->where('id_scholarship_scheme', $id_scholarship_scheme);
        $this->db->where('year', $year);
        $this->db->where('id_application', $previous_application);
        $this->db->update('scholarship_applicant', $data);
        return TRUE;
    }

    function updateApplicationIdToFamilyDetails($data)
    {
        $id_scholar_applicant = $data['id_scholar_applicant'];
        $id_application = $data['id_application'];
        $id_scholarship_scheme = $data['id_scholarship_scheme'];
        $year = $data['year'];
        $previous_application = $data['previous_application'];
        unset($data['previous_application']);

        $this->db->where('id_scholar_applicant', $id_scholar_applicant);
        $this->db->where('id_scholarship_scheme', $id_scholarship_scheme);
        $this->db->where('year', $year);
        $this->db->where('id_application', $previous_application);
        $this->db->update('scholar_applicant_family_details', $data);
        return TRUE;
    }


    function updateApplicationIdToQualificationDetails($data)
    {
        $id_scholar_applicant = $data['id_scholar_applicant'];
        $id_application = $data['id_application'];
        $id_scholarship_scheme = $data['id_scholarship_scheme'];
        $year = $data['year'];
        $previous_application = $data['previous_application'];
        unset($data['previous_application']);

        $this->db->where('id_scholar_applicant', $id_scholar_applicant);
        $this->db->where('id_scholarship_scheme', $id_scholarship_scheme);
        $this->db->where('year', $year);
        $this->db->where('id_application', $previous_application);
        $this->db->update('scholarship_examination_details', $data);
        return TRUE;
    }


    function updateApplicationIdToEmploymentDetails($data)
    {
        $id_scholar_applicant = $data['id_scholar_applicant'];
        $id_application = $data['id_application'];
        $id_scholarship_scheme = $data['id_scholarship_scheme'];
        $year = $data['year'];
        $previous_application = $data['previous_application'];
        unset($data['previous_application']);

        $this->db->where('id_scholar_applicant', $id_scholar_applicant);
        $this->db->where('id_scholarship_scheme', $id_scholarship_scheme);
        $this->db->where('year', $year);
        $this->db->where('id_application', $previous_application);
        $this->db->update('scholar_applicant_employment_status', $data);
        return TRUE;
    }


    function getApplicationPersonalDetailsByApplicationId($id_scholar_applicant,$id_scheme,$id_application)
    {
        $this->db->select('ia.*');
        $this->db->from('scholarship_applicant as ia');
        $this->db->where('ia.id_scholar_applicant', $id_scholar_applicant);
        $this->db->where('id_application', $id_application);
        $this->db->where('ia.id_scholarship_scheme', $id_scheme);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }



    function getExamDetailsByApplicationId($id_scholar_applicant,$id_scholarship_scheme,$id_application)
    {
        $this->db->select('*');
        $this->db->from('scholarship_examination_details');
        $this->db->where('id_scholar_applicant', $id_scholar_applicant);
        $this->db->where('id_scholarship_scheme', $id_scholarship_scheme);
        $this->db->where('id_application', $id_application);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }
    
    function getFamilyDetailsByApplicationId($id_scholar_applicant,$id_scholarship_scheme,$id_application)
    {
        $this->db->select('*');
        $this->db->from('scholar_applicant_family_details');
        $this->db->where('id_scholar_applicant', $id_scholar_applicant);
        $this->db->where('id_scholarship_scheme', $id_scholarship_scheme);
        $this->db->where('id_application', $id_application);
        $query = $this->db->get();
         $result = $query->row();
        return $result;
    }
    
    function getEmploymentDetailsByApplicationId($id_scholar_applicant,$id_scholarship_scheme,$id_application)
    {
        $this->db->select('*');
        $this->db->from('scholar_applicant_employment_status');
        $this->db->where('id_scholar_applicant', $id_scholar_applicant);
        $this->db->where('id_scholarship_scheme', $id_scholarship_scheme);
        $this->db->where('id_application', $id_application);
        $query = $this->db->get();
         $result = $query->result();
        return $result;
    }



    function getProgram($id_scholarship_program)
    {
        $this->db->select('id, is_personel_details, is_education_details, is_family_details, is_financial_recoverable_details, is_file_upload_details,is_employment_details');
        $this->db->from('scholarship_programme');
        $this->db->where('id', $id_scholarship_program);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
        
    }

    function getIsPersonelDetails($id_scholarship_program)
    {
        $this->db->select('*');
        $this->db->from('is_scholarship_applicant_personel_details as ihs');
        $this->db->where('ihs.id_scholarship_program', $id_scholarship_program);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
        
    }

    function getIsQualificationDetails($id_scholarship_program)
    {
        $this->db->select('*');
        $this->db->from('is_scholarship_applicant_examination_details as ihs');
        $this->db->where('ihs.id_scholarship_program', $id_scholarship_program);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
        
    }

    function getIsFamilyDetails($id_scholarship_program)
    {
        $this->db->select('*');
        $this->db->from('is_scholar_applicant_family_details as ihs');
        $this->db->where('ihs.id_scholarship_program', $id_scholarship_program);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
        
    }

    function getIsEmploymentDetails($id_scholarship_program)
    {
        $this->db->select('*');
        $this->db->from('is_scholar_applicant_employment_status as ihs');
        $this->db->where('ihs.id_scholarship_program', $id_scholarship_program);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }


    function getFeeStructure($data)
    {
        $this->db->select('sa.*');
        // $this->db->join('scholarship_programme as sp', 'sa.id_program = sp.id');
        $this->db->from('fee_structure_master as sa');
        $this->db->where('sa.id_cohert', $data['id_cohert']);
        $this->db->where('sa.id_scholarship', $data['id_scholarship']);
        $this->db->where('sa.id_program', $data['id_program']);
        $query = $this->db->get();
        return $query->row();
    }


    

















    


    

    function getRole($id)
    {
        $this->db->select('*');
        $this->db->from('roles');
        $this->db->where('id', $id);
        $query = $this->db->get();
            // echo "<pre>";print_r($query);die;
        
        return $query->row();
    }
}
<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Student_record_model extends CI_Model
{
    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }


    function applicantList($data)
    {
        $this->db->select('a.*, p.code as programme_code, p.name as programme_name');
        $this->db->from('student as a');
        // $this->db->join('intake as i', 'a.id_intake = i.id');
        $this->db->join('scholarship_programme as p', 'a.id_program = p.id','left');
         if ($data['name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['nric'] != '')
        {
            $likeCriteria = "(s.nric  LIKE '%" . $data['nric'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_programme'] != '')
        {
            $this->db->where('s.id_program', $data['id_programme']);
        }
        if ($data['status'] != '')
        {
            $this->db->where('mi.status', $data['status']);
        }
        $this->db->where('a.applicant_status !=', 'Graduated');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }


    function getStudentByStudentId($id_student)
    {
        $this->db->select('s.*, p.code as programme_code, p.name as programme_name, ms.name as mailing_state, mc.name as mailing_country, ps.name as permanent_state, pc.name as permanent_country, rs.name as race, rels.name as religion, salut.name as salutation');
        $this->db->from('student as s');
        $this->db->join('scholarship_programme as p', 's.id_program = p.id','left'); 
        $this->db->join('salutation_setup as salut', 's.salutation = salut.id','left');
        $this->db->join('scholarship_state as ms', 's.mailing_state = ms.id','left'); 
        $this->db->join('scholarship_country as mc', 's.mailing_country = mc.id','left');
        $this->db->join('scholarship_state as ps', 's.permanent_state = ps.id','left'); 
        $this->db->join('scholarship_country as pc', 's.permanent_country = pc.id','left'); 
        $this->db->join('scholarship_race_setup as rs', 's.id_race = rs.id','left'); 
        $this->db->join('scholarship_religion_setup as rels', 's.religion = rels.id','left');
        $this->db->where('s.id', $id_student);
        $query = $this->db->get();
        $result = $query->row(); 

        return$result;
    }


    function getStatusHistoryByIdApplication($id_application)
    {
        $this->db->select('s.*, sch.name as user_name, st.name as status');
        $this->db->from('applicantion_status_change_history as s');
        $this->db->join('scholar as sch', 's.created_by = sch.id'); 
        $this->db->join('status_list as st', 's.id_status = st.id','left');
        $this->db->where('s.id_application', $id_application);
        $query = $this->db->get();
        $result = $query->result(); 

        return$result;
    }

















    function editMainInvoiceList($array)
    {
        $status = ['status'=>'1'];
      $this->db->where_in('id', $array);
      $this->db->update('main_invoice', $status);
    }

    function getMainInvoice($id)
    {
        $this->db->select('mi.*, s.full_name as student');
        $this->db->from('main_invoice as mi');
        $this->db->join('student as s', 'mi.id_student = s.id');
        $this->db->where('mi.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewMainInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }
}
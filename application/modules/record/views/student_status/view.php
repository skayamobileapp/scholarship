<?php $this->load->helper("form"); ?>
<?php 
   // require('ckeditor/ckeditor.php');
    ?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <div class="page-title clearfix">
         <h3>Student Record</h3>
         <a href="../list" class="btn btn-link btn-back">‹ Back</a>
      </div>


         <div class="form-container">
            <h4 class="form-group-title">Student Details</h4>
            <div class='data-list'>
               <div class='row'>
                  <div class='col-sm-6'>
                     <dl>
                        <dt>Student Name :</dt>
                        <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                     </dl>
                     <dl>
                        <dt>Student NRIC :</dt>
                        <dd><?php echo $studentDetails->nric ?></dd>
                     </dl>
                     <dl>
                        <dt>Student Email :</dt>
                        <dd><?php echo $studentDetails->email_id; ?></dd>
                     </dl>
                                     
                  </div>
                  <div class='col-sm-6'>
                     <dl>
                        <dt>Program :</dt>
                        <dd><?php echo $studentDetails->programme_code . " - " . $studentDetails->programme_name; ?></dd>
                     </dl>
                     <dl>
                        <dt>Nationality :</dt>
                        <dd><?php echo $studentDetails->nationality ?></dd>
                     </dl>
                     <dl>
                        <dt>Learning Mode :</dt>
                        <dd><?php echo $studentDetails->program_scheme ?></dd>
                     </dl>     
                  </div>
               </div>
            </div>
         </div>










        <form id="form_detail" action="" method="post" enctype="multipart/form-data">
            <div class="form-container">
                <h4 class="form-group-title">Staus Details</h4>  


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>New Profile Status <span class='error-text'>*</span></label>
                            <select name="id_status" id="id_status" class="form-control" onchange="subStatusListBystatusId(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($statusList))
                                {
                                    foreach ($statusList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->name;?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-sm-4">
                          <div class="form-group">
                              <label>New Sub Status <span class='error-text'>*</span></label>
                              <span id="view_sub_status">
                                <select class="form-control" id='id_sub_status' name='id_sub_status'>
                                    <option value=''></option>
                                  </select>
                             </span>

                          </div>
                    </div>



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Effective Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="effective_date" name="effective_date">
                        </div>
                    </div>



                </div>


                <div class="row">



                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Remarks <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="remarks" name="remarks">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Attachments <span class='error-text'>*</span></label>
                            <input type="file" class="form-control" id="file" name="file">
                        </div>
                    </div>

                </div>
            </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>

        </form>


         <?php
        if (!empty($getStudentStatusHistory))
        {

          ?>


            <div class="form-container">
                <h4 class="form-group-title">Status History Details</h4>

                    <div class="custom-table">
                      <table class="table" id="list-table">
                        <thead>
                          <tr>
                            <th>Sl. No</th>
                            <th>Profile Status</th>
                            <th>Sub Status</th>
                            <th>Effective Date</th>
                            <th>Remarks</th>
                            <th style="text-align: center;">Attachment</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if (!empty($getStudentStatusHistory))
                          {
                            $i=1;
                            foreach ($getStudentStatusHistory as $record)
                            {
                          ?>
                              <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $record->status ?></td>
                                <td><?php echo $record->sub_status ?></td>
                                <td><?php echo date('d-m-Y', strtotime($record->effective_date)) ?></td>
                                <td><?php echo $record->remarks ?></td>
                                <td class="text-center">

                                    <?php
                                    if (!empty($record->file))
                                    {
                                      ?>

                                    <a href="<?php echo '/assets/images/' . $record->file; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $record->file; ?>)" title="View"> View
                                        <!-- <span style='font-size:18px;'>&#128065;</span> -->
                                    </a>

                                    <?php
                                    }
                                    else
                                    {
                                      echo 'No File';
                                    }
                                  ?>

                                </td>
                              </tr>
                          <?php
                          $i++;
                            }
                          }
                          ?>
                        </tbody>
                      </table>
                    </div>

                </div>



          <?php
          }
          ?>









   <footer class="footer-wrapper">
      <p>&copy; 2019 All rights, reserved</p>
   </footer>
</div>
</form>
<script type="text/javascript">
   function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;
       document.body.innerHTML = printContents;
       window.print();
       document.body.innerHTML = originalContents;
   }
</script>
<script>

  $('select').select2();

  $( function()
  {
  $( ".datepicker" ).datepicker({
      changeYear: true,
      changeMonth: true,
      yearRange: "1920:2019"
  });
  });



    function subStatusListBystatusId(id)
    {
      $.get("/record/studentStatus/subStatusListBystatusId/"+id, function(data, status)
      {
          $("#view_sub_status").html(data);
          $("#view_sub_status").show();
      });

    }
   
   
   
    $(document).ready(function(){
       $("#form_detail").validate(
       {
           rules:
           {
               id_status:
               {
                   required: true
               },
               id_sub_status:
               {
                   required: true
               },
               effective_date:
               {
                   required: true
               },
               remarks:
               {
                   required: true
               },
               file:
               {
                   required: true
               }
           },
           messages:
           {
               id_status:
               {
                   required: "<p class='error-text'>Select Profile Status</p>",
               },
               id_sub_status:
               {
                   required: "<p class='error-text'>Select Sub Status</p>",
               },
               effective_date:
               {
                   required: "<p class='error-text'>Select Effective Date</p>",
               },
               remarks:
               {
                   required: "<p class='error-text'>Remarks Required</p>",
               },
               file:
               {
                   required: "<p class='error-text'>File Required</p>",
               }
           },
           errorElement: "span",
           errorPlacement: function(error, element) {
               error.appendTo(element.parent());
           }
   
       });
   });
   






   
</script>
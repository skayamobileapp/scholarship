<?php $this->load->helper("form"); ?>
<?php 
   // require('ckeditor/ckeditor.php');
    ?>
<div class="container-fluid page-wrapper">
   <div class="main-container clearfix">
      <div class="page-title clearfix">
         <h3>Student Record</h3>
         <a href="../list" class="btn btn-link btn-back">‹ Back</a>
      </div>


      <form id="form_main_invoice" action="" method="post">


         <div class="form-container">
            <h4 class="form-group-title">Student Details</h4>
            <div class='data-list'>
               <div class='row'>
                  <div class='col-sm-6'>
                     <dl>
                        <dt>Student Name :</dt>
                        <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                     </dl>
                     <dl>
                        <dt>Student NRIC :</dt>
                        <dd><?php echo $studentDetails->nric ?></dd>
                     </dl>
                     <dl>
                        <dt>Student Email :</dt>
                        <dd><?php echo $studentDetails->email_id; ?></dd>
                     </dl>
                                     
                  </div>
                  <div class='col-sm-6'>
                     <dl>
                        <dt>Program :</dt>
                        <dd><?php echo $studentDetails->programme_code . " - " . $studentDetails->programme_name; ?></dd>
                     </dl>
                     <dl>
                        <dt>Nationality :</dt>
                        <dd><?php echo $studentDetails->nationality ?></dd>
                     </dl>
                     <dl>
                        <dt>Learning Mode :</dt>
                        <dd><?php echo $studentDetails->program_scheme ?></dd>
                     </dl>     
                  </div>
               </div>
            </div>
         </div>

         
         <div class="form-container">
            <h4 class="form-group-title">Student Records Details</h4>



            
            <div class="m-auto text-center">
           <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
        </div>


        <div class="clearfix">
        <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
            <li role="presentation" class="active"><a href="#tab_one" class="nav-link border rounded text-center"
                    aria-controls="tab_one" aria-selected="true"
                    role="tab" data-toggle="tab">Profile</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Status</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Academic status</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Internship</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Apprenticeship</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Finance</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Application</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Selection</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Document</a>
            </li>
            <li role="presentation"><a href="#tab_two" class="nav-link border rounded text-center"
                    aria-controls="tab_two" role="tab" data-toggle="tab">Communication</a>
            </li>
        </ul>



        <div class="tab-content offers-tab-content">




            <div role="tabpanel" class="tab-pane active" id="tab_one">

                <div class="col-12 mt-4">
                    <br>


                <div class="form-container">
                    <h4 class="form-group-title">Profile Details</h4>

                </div>

                 
                 </div> <!-- END col-12 -->  
            </div>




            <div role="tabpanel" class="tab-pane" id="tab_two">
                <div class="col-12 mt-4">

                <br>
                    
                <div class="form-container">
                    <h4 class="form-group-title">Coming Soon ..!!!!</h4>

                </div>

                             
                </div> <!-- END col-12 -->  
            </div>


    
        </div>

       </div> <!-- END row-->




   <footer class="footer-wrapper">
      <p>&copy; 2019 All rights, reserved</p>
   </footer>
</div>
</form>
<script type="text/javascript">
   function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;
       document.body.innerHTML = printContents;
       window.print();
       document.body.innerHTML = originalContents;
   }
</script>
<script>

   
   
   function deleteStudentNote(id)
   {
   
       var tempPR = {};
       tempPR['id'] = id;
       tempPR['id_student'] = <?php echo $studentDetails->id ?>;
           $.ajax(
           {
              url: '/records/studentRecord/deleteStudentNote',
               type: 'POST',
              data:
              {
               tempData: tempPR
              },
              error: function()
              {
               alert('Something is wrong');
              },
              success: function(result)
              {
               // alert(result);
               // window.location.reload();
               $("#view_student_note").html(result);
               // $('#myModal').modal('hide');
              }
           });
   }
   
   
   
    $(document).ready(function(){
       $("#form_detail").validate(
       {
           rules:
           {
               note:
               {
                   required: true
               }         
           },
           messages:
           {
               note:
               {
                   required: "<p class='error-text'>Note Required</p>",
               }
           },
           errorElement: "span",
           errorPlacement: function(error, element) {
               error.appendTo(element.parent());
           }
   
       });
   });
   
   
</script>
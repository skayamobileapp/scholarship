<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Attendence extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('attendence_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('account_code.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

                        $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_thrust'] = $this->security->xss_clean($this->input->post('id_thrust'));
            $formData['id_sub_thrust'] = $this->security->xss_clean($this->input->post('id_sub_thrust'));
 
            $data['searchParam'] = $formData;

            $data['programSyllabusList'] = $this->attendence_model->programSyllabusList($formData);

            $this->global['pageTitle'] = 'Campus Management System : List Students';
            $this->loadViews("attendence/list", $this->global, $data, NULL);

        }
    }
    
    
    
    function module($id)
    {
        if ($this->checkScholarAccess('account_code.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['programSyllabusList'] = $this->attendence_model->syllanusModuleList($id);
            $this->global['pageTitle'] = 'Campus Management System : List Students';
            $this->loadViews("attendence/modules", $this->global, $data, NULL);

        }
    }


    function attendence($id)
    {
        if ($this->checkScholarAccess('account_code.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['programSyllabusList'] = $this->attendence_model->syllanusModuleList($id);
            $this->global['pageTitle'] = 'Campus Management System : List Students';
            $this->loadViews("attendence/attendence", $this->global, $data, NULL);

        }
    }


}
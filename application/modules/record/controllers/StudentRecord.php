<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentRecord extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_record_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('student_record.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['status'] = '';

            $data['searchParam'] = $formData;

            $data['mainInvoiceList'] = $this->student_record_model->applicantList($formData);
                // echo "<Pre>";print_r($data['mainInvoiceList']);exit;
        // print_r($data);exit();
            $data['programmeList']= $this->student_record_model->programmeListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : List Students';
            $this->loadViews("student_record/list", $this->global, $data, NULL);

        }
    }
    
    
    function view($id)
    {
        if ($this->checkScholarAccess('student_record.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            // $id_applicant = 
            $data['studentDetails'] = $this->student_record_model->getStudentByStudentId($id);
            // $data['status'] = $this->student_record_model->getStudentByStudentId($id);

            // $data['getStatusHistoryByIdApplication'] = $this->student_record_model->getStatusHistoryByIdApplication($id_application);


            // echo "<Pre>";print_r($data['studentDetails']);exit;

            $this->global['pageTitle'] = 'Campus Management System : View Student Account Statements';
            $this->loadViews("student_record/view", $this->global, $data, NULL);

        }
    }

    function addStudentNote()
    {
        $user_id = $this->session->userId; 

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;

        if($tempData['note'] != '')
        {
            $tempData['created_by'] = $user_id;
            $inserted_id = $this->student_record_model->addStudentNote($tempData);
            $data = $this->displayStudentNote($tempData['id_student']);
        }else
        {
        // echo "<Pre>";print_r($tempData);exit;
            $data = $this->displayStudentNote($tempData['id_student']);
        }
        echo $data;exit();       
    }

    function displayStudentNote($id_student)
    {        
        $temp_details = $this->student_record_model->getStudentNote($id_student); 
        // echo "<Pre>";print_r($id_student);exit;
        if(!empty($temp_details))
        {

        $table = "
        <div class='row'>

                                <div class='col-sm-2'>
                                    <div class='form-group'>
                                    </div>
                                </div>


                                <div class='col-sm-8'>";




                           
    $table.= "

    <div class='form-container'>
        <h4 class='form-group-title'>Student Note Details</h4>

        <table  class='table' id='list-table'>
                    <tr>
                    <th>Sl. No</th>
                    <th class='text-center'>Note</th>
                    <th class='text-center'>Action</th>
                    </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $note = $temp_details[$i]->note;
                    $user = $temp_details[$i]->user;
                    $created_dt_tm = date('h:i:s A ( d-m-Y )', strtotime($temp_details[$i]->created_dt_tm));

                    $j = $i+1;
                    
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$note
                            <br>
                            <br>
                            <p style='text-align: left;'>
                            Created By - <b> $user </b>, Created On - <b> $created_dt_tm </b></p>
                            </td>                          
                            <td style='text-align: center;'>
                                <a onclick='deleteStudentNote($id)'>Delete</a>
                            </td>
                        </tr>";
                    }

                    $table .= "";

        $table.= "</table>

            </div>";

         $table.= "   
                                </div>


                                <div class='col-sm-2'>
                                    <div class='form-group'>
                                    </div>
                                </div>



                            </div>";


        }
        else
        {
            $table= "";
        }
        return $table;
    }

    function deleteStudentNote()
    {
        // echo "<Pre>";  print_r($id);exit;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id = $tempData['id'];
        $id_student = $tempData['id_student'];

        $inserted_id = $this->student_record_model->deleteStudentNote($id);
        $data = $this->displayStudentNote($id_student);

        echo $data;exit(); 
    }
}
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentStatus extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_status_model');
        $this->isScholarLoggedIn();
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Scholarship Management System : 404 - Page Not Found';
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    function list()
    {
        if ($this->checkScholarAccess('student_status.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_programme'] = $this->security->xss_clean($this->input->post('id_programme'));
            $formData['id_intake'] = $this->security->xss_clean($this->input->post('id_intake'));
            $formData['status'] = '';

            $data['searchParam'] = $formData;

            $data['mainInvoiceList'] = $this->student_status_model->applicantList($formData);
                // echo "<Pre>";print_r($data['mainInvoiceList']);exit;
        // print_r($data);exit();
            $data['programmeList']= $this->student_status_model->programmeListByStatus('1');

            $this->global['pageTitle'] = 'Scholarship Management System : List Students';
            $this->loadViews("student_status/list", $this->global, $data, NULL);

        }
    }
    
    
    function view($id)
    {
        if ($this->checkScholarAccess('student_status.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit;
                // echo "<Pre>";print_r($_FILES);exit;

                $id_status = $this->security->xss_clean($this->input->post('id_status'));
                $id_sub_status = $this->security->xss_clean($this->input->post('id_sub_status'));
                $effective_date = $this->security->xss_clean($this->input->post('effective_date'));
                $remarks = $this->security->xss_clean($this->input->post('remarks'));
                            
                $data = array(
                    'id_student' => $id,
                    'id_status' => $id_status,
                    'id_sub_status' => $id_sub_status,
                    'effective_date' => date('Y-m-d', strtotime($effective_date)),
                    'remarks' => $remarks
                );


                if (!empty($_FILES['file']['name']))
                {

                  // $certificate_name = $_FILES['certificate']['name'];
                  
                  $file_one_name = $_FILES['file']['name'];
                  $file_one_size =$_FILES['file']['size'];
                  $file_one_tmp =$_FILES['file']['tmp_name'];
                  $file_one_type=$_FILES['file']['type'];
                  // $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
                  $file_one_ext=explode('.',$file_one_name);
                  $file_one_ext=end($file_one_ext);
                  $file_one_ext=strtolower($file_one_ext);

                  

                  // For file validation from 36 to 44 , file size validation

                  $this->fileFormatNSizeValidation($file_one_ext,$file_one_size,'Attachment');

                  $data['file'] = $this->uploadFile($file_one_name,$file_one_tmp,'Attachment');
                }

                // echo "<Pre>";print_r($data);exit;
                
                $result = $this->student_status_model->addStudentStatus($data);
                redirect($_SERVER['HTTP_REFERER']);

            }


            // $id_applicant = 
            $data['studentDetails'] = $this->student_status_model->getStudentByStudentId($id);
            $data['getStudentStatusHistory'] = $this->student_status_model->getStudentStatusChangeHistory($id);

            // $data['status'] = $this->student_status_model->getStudentByStudentId($id);
            $data['statusList']= $this->student_status_model->statusListByStatus('1');



            // echo "<Pre>";print_r($data['getStudentStatusHistory']);exit;

            $this->global['pageTitle'] = 'Scholarship Management System : View Student Account Statements';
            $this->loadViews("student_status/view", $this->global, $data, NULL);

        }
    }

    function subStatusListBystatusId($id_status)
    {
        // echo "<Pre>"; print_r($id_cohort);exit;
        
        $intake_data = $this->student_status_model->subStatusListBystatusId($id_status);
        

        // echo "<Pre>"; print_r($intake_data);exit;

         $table="
        <script type='text/javascript'>

        $('select').select2();
                        
        </script>


        <select name='id_sub_status' id='id_sub_status' class='form-control'>";
        $table.="<option value=''>Select</option>";

        for($i=0;$i<count($intake_data);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $intake_data[$i]->id;
        $name = $intake_data[$i]->name;
        // $code = $intake_data[$i]->scholarship_code;

        $table.="<option value=".$id.">" .  $name .
                "</option>";

        }
        $table.="</select>";

        echo $table;
    }
}

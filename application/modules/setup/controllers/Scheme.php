<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Scheme extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('scheme_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('scheme.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            $data['schemeList'] = $this->scheme_model->schemeListSearch($name);
            // echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Campus Management System : Scholarship Cohert List';
            $this->loadViews("scheme/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('scheme.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_scholar_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $form = $this->input->post();
            // echo "<Pre>"; print_r($form);exit;

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $percentage = $this->security->xss_clean($this->input->post('percentage'));
                $from_dt = $this->security->xss_clean($this->input->post('from_dt'));
                $to_dt = $this->security->xss_clean($this->input->post('to_dt'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'percentage' => $percentage,
                    'from_dt' => date('Y-m-d',strtotime($from_dt)),
                    'to_dt' => date('Y-m-d',strtotime($to_dt)),
                    'status' => $status,
                    'created_by' => $user_id
                );

                $inserted_id = $this->scheme_model->addNewScheme($data);
                if($inserted_id)
                {
                    $details_moved =  $this->scheme_model->moveTempDataToDetails($inserted_id);
                }
                redirect('/scholarship/scheme/list');
            }
            else
            {
                $temp_deleted =  $this->scheme_model->deleteTempSchemeHasProgramBySession($id_session);
            }
            $data['programList'] = $this->scheme_model->programListByStatus('1');
            $data['subThrustList'] = $this->scheme_model->subThrustListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add Scholarship Cohert';
            $this->loadViews("scheme/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('scheme.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/scheme/list');
            }
            $id_session = $this->session->my_scholar_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $percentage = $this->security->xss_clean($this->input->post('percentage'));
                $from_dt = $this->security->xss_clean($this->input->post('from_dt'));
                $to_dt = $this->security->xss_clean($this->input->post('to_dt'));
                $status = $this->security->xss_clean($this->input->post('status'));

            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'percentage' => $percentage,
                    'from_dt' => date('Y-m-d',strtotime($from_dt)),
                    'to_dt' => date('Y-m-d',strtotime($to_dt)),
                    'status' => $status,
                    'created_by' => $user_id
                );
                
                $result = $this->scheme_model->editScheme($data,$id);
                redirect('/scholarship/scheme/list');
            }

            $data['scheme'] = $this->scheme_model->getScheme($id);
            $data['schemeHasProgramList'] = $this->scheme_model->getSchemeHasProgramByMasterId($id);
            $data['idScheme'] = $id;
            $data['programList'] = $this->scheme_model->programListByStatus('1');
            $data['subThrustList'] = $this->scheme_model->subThrustListByStatus('1');
            // echo "<Pre>"; print_r($data['subThrustList']);exit;
            $this->global['pageTitle'] = 'Campus Management System : Edit Scholarship Cohert';
            $this->loadViews("scheme/edit", $this->global, $data, NULL);
        }
    }


    function tempadd()
    {
        $id_session = $this->session->my_scholar_session_id;
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['id_session'] = $id_session;
        // echo "<Pre>";print_r($tempData);exit;
        
        $inserted_id = $this->scheme_model->addTempDetails($tempData);

        $data = $this->displaytempdata();
        
        echo $data;        
    }

    function displaytempdata()
    {
        $id_session = $this->session->my_scholar_session_id;
        
        $temp_details = $this->scheme_model->getTempSchemeHasProgramBySession($id_session); 
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Scholarship</th>
                    <th>Program</th>
                    <th>Quota</th>
                    <th>Budget</th>
                    <th>Action</th>
                </tr>
                </thead>";
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $program_name = $temp_details[$i]->program_name;
                    $program_code = $temp_details[$i]->program_code;
                    $scholarship_name = $temp_details[$i]->scholarship_name;
                    $scholarship_code = $temp_details[$i]->scholarship_code;
                    $quota = $temp_details[$i]->quota;
                    $budget = $temp_details[$i]->budget;

                    $j = $i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$scholarship_name - $scholarship_code</td>
                            <td>$program_code - $program_name</td>
                            <td>$quota</td>
                            <td>$budget</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempSchemeHasProgramme($id)'>Delete</a>
                            <td>
                        </tr>";
                    }


                                // <span onclick='deleteTempSchemeHasProgramme($id)'>Delete</a>
                            

            $table.= "
            </tbody>
            </table></div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempSchemeHasProgramme($id)
    {
        $inserted_id = $this->scheme_model->deleteTempSchemeHasProgramme($id);
        if($inserted_id)
        {
            $data = $this->displaytempdata();
            echo $data;  
        }
        
        // echo "Success";
    }

    function deleteSchemeHasProgram($id_program_has_scheme)
    {
        $inserted_id = $this->scheme_model->deleteSchemeHasProgram($id_program_has_scheme);
        echo "Success"; 
    }

    function directAdd()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit();
        
        $inserted_id = $this->scheme_model->addNewSchemeHasProgramDetails($tempData);
        
        echo "Success";        
    }

    function getProgramBySubThrustId($id_sub_thrust)
    {
        $results = $this->scheme_model->getProgramBySubThrustId($id_sub_thrust);

            // echo "<Pre>"; print_r($results);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>";

            $table.="
            <select name='id_program' id='id_program' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $program_code = $results[$i]->program_code;
            $program_name = $results[$i]->program_name;
            $table.="<option value=".$id.">".$program_code . " - " . $program_name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }
}

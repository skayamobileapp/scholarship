<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FeeStructure extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('fee_setup_model');
        $this->load->model('fee_structure_model');
        $this->load->model('fee_category_model');
        $this->load->model('frequency_mode_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('fee_setup.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
        	$name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;

            //echo "<Pre>"; print_r($data);exit;
            $data['programmeList'] = $this->fee_structure_model->programmeListSearch($name);
            $this->global['pageTitle'] = 'Campus Management System : Fee Structure List';
            $this->loadViews("fee_structure/list", $this->global, $data, NULL);
        }
    }

    function show_intake($id = NULL)
    {
        if ($this->checkScholarAccess('fee_setup.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

        	if ($id == null)
            {
                redirect('/finance/feeStructure/list');
            }

            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['programme_id'] = $id;

            $data['intakeList'] = $this->fee_structure_model->intakeListByProgrammeId($id,$name);
            $data['programme'] = $this->fee_structure_model->getProgrammeById($id);
            // echo "<Pre>"; print_r($data);exit;
            $this->global['pageTitle'] = 'Campus Management System :Fee Structure';
            $this->loadViews("fee_structure/intake_list", $this->global, $data, NULL);
        }
    }
    
    function add($id_intake,$id_programme)
    {
        if ($this->checkScholarAccess('fee_setup.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
                if($this->input->post())
                {
                    redirect('/finance/feeStructure/show_intake/'.$id_programme);
                }           

                
                $data['intake'] = $this->fee_structure_model->getIntakeById($id_intake);
                $data['programme'] = $this->fee_structure_model->getProgrammeById($id_programme);
                $data['frequencyModeList'] = $this->frequency_mode_model->frequencyModeListByStatus('1');
                $data['feeSetupList'] = $this->fee_setup_model->feeSetupListByStatus('1');
                $data['feeStructureList'] = $this->fee_structure_model->getFeeStructureByIdProgrammeNIdIntake($id_programme,$id_intake);


                $this->global['pageTitle'] = 'Campus Management System : Add Fee Structure';
                $this->loadViews("fee_structure/add", $this->global, $data, NULL);
            // }
        }
    }

     function tempadd()
    {
        $id_session = $this->session->my_scholar_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        unset($tempData['id']);
        $id_programme = $tempData['id_programme'];
        $id_intake = $tempData['id_intake'];
        
        unset($tempData['id']);
        $inserted_id = $this->fee_structure_model->addNewFeeStructure($tempData);
        // echo "<Pre>";  print_r($inserted_id);exit;
         redirect('/finance/feeStructure/add/'.$id_intake.'/'.$id_programme);
        // header("Refresh:0; url=". $_SERVER['HTTP_REFERER']);
        // echo $_SERVER['HTTP_REFERER'];
        // redirect($_SERVER['HTTP_REFERER']);       
    }

    function displaytempdata($id_programme,$id_intake)
    {
        $id_session = $this->session->my_scholar_session_id;
        
        $temp_details = $this->fee_structure_model->getFeeStructureByIdProgrammeNIdIntake($id_programme,$id_intake); 
        $table = "<table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Frequency Mode</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>";
                $total_amount = 0;
                    for($i=0;$i<count($temp_details);$i++)
                    {
        echo "<Pre>";print_r($temp_details);exit;
                    $id = $temp_details[$i]->id;
                    $fee_structure = $temp_details[$i]->fee_structure;
                    $frequency_mode = $temp_details[$i]->frequency_mode;
                    $frequency_code = $temp_details[$i]->frequency_code;
                    $frequency = $frequency_code . " - " . $frequency_mode;
                    $amount = $temp_details[$i]->amount;
                    $j = $i+1;
                        $table .= "
                        <tr>
                            <td>$j</td>
                            <td>$fee_structure</td>
                            <td>$frequency</td>
                            <td>$amount</td>                           
                            <td>
                                <span onclick='deleteTempData($id)'>Delete</a>
                            <td>
                        </tr>";
                        $total_amount = $total_amount + $amount;
                    }

                    $table .= "
                        <tr>
                            <td></td>
                            <td></td>
                            <td>Total : </td>
                            <td>$total_amount</td>                           
                            <td>
                            <td>
                        </tr>";
        $table.= "</table>";
        return $table;
    }

    function tempDelete($id)
    {
        // echo "<Pre>";  print_r($id);exit;
        $id_session = $this->session->my_scholar_session_id;
        // $inserted_id = $this->fee_structure_model->deleteTempData($id);
        $this->fee_structure_model->deleteFeeStructureById($id);
        // $data = $this->displaytempdata();
        // echo $inserted_id; 
    }

     function tempaddExisting()
    {
        $id_session = $this->session->my_scholar_session_id;

        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";  print_r($tempData);exit;
        $tempData['id_session'] = $id_session;
        $inserted_id = $this->fee_structure_model->addTempDetails($tempData);
        $data = $this->displaytempdata();
        echo $data;        
    }
}

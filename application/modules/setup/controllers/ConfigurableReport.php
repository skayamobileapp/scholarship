<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ConfigurableReport extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('race_model');
        $this->isScholarLoggedIn();
    }

    function add()
    {
            $this->loadViews("configurable_report/add", $this->global, NULL, NULL);
        
    }


    function list()
    {
            $this->loadViews("configurable_report/list", $this->global, NULL, NULL);
        
    }




}

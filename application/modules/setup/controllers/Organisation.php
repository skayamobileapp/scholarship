<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Organisation extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('organisation_model');
        $this->isScholarLoggedIn();
    }

    function edit()
    {
        if ($this->checkScholarAccess('organisation.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                //END OF FILES/////
                $id_user = $this->session->userId;
                $id_session = $this->session->my_scholar_session_id;

                 $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                // $filename = $_FILES['image']['name']; //Command to assign the value
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $short_name = $this->security->xss_clean($this->input->post('short_name'));
                $name_in_malay = $this->security->xss_clean($this->input->post('name_in_malay'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_registrar = $this->security->xss_clean($this->input->post('id_registrar'));
                $date_time = $this->security->xss_clean($this->input->post('date_time'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $address1 = $this->security->xss_clean($this->input->post('address1'));
                $address2 = $this->security->xss_clean($this->input->post('address2'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $fax_number = $this->security->xss_clean($this->input->post('fax_number'));
                $id_organisation = $this->security->xss_clean($this->input->post('id_organisation'));

            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'short_name' => $short_name,
                    'name_in_malay' => $name_in_malay,
                    'url' => $url,
                    'id_country' => $id_country,
                    'id_registrar' => $id_registrar,
                    'date_time' => date('Y-m-d', strtotime($date_time)),
                    'contact_number' => $contact_number,
                    'fax_number' => $fax_number,
                    'address1' => $address1,
                    'address2' => $address2,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'email' => $email,
                    'status' => $status,
                    'created_by' => $id_user
                );

            // echo "<Pre>";print_r($data);exit();
                $result = $this->organisation_model->editOrganisation($data,$id_organisation);
                redirect('/scholarship/organisation/edit');
            }
            $data['organisation'] = $this->organisation_model->getOrganisation();
            // echo "<Pre>";print_r($data);exit();
            $data['id_organisation'] = $data['organisation']->id;
            $data['stateList'] = $this->organisation_model->stateListByStatus('1');
            $data['countryList'] = $this->organisation_model->countryListByActivity('1');
            $data['staffList'] = $this->organisation_model->staffListByActivity('1');
            $data['departmentList'] = $this->organisation_model->departmentListByStatus('1');
            $data['organisationComiteeList'] = $this->organisation_model->organisationComiteeList($data['organisation']->id);
            $data['organisationDepartmentList'] = $this->organisation_model->organisationDepartmentList($data['organisation']->id);


            // echo "<Pre>";print_r($data['organisation']);exit();
            
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Organisation';
            $this->loadViews("organisation/edit", $this->global, $data, NULL);
        }
    }

    function addOrganisationComitee()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $tempData['effective_date'] = date('Y-m-d', strtotime($tempData['effective_date']));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->organisation_model->addNewOrganisationComitee($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }

    function deleteOrganisationConitee($id)
    {
            // echo "<Pre>"; print_r($id);exit();
      
      $inserted_id = $this->organisation_model->deleteOrganisationComitee($id);
        
        echo "Success"; 
    }

    function addOrganisationDepartment()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // $tempData['effective_date'] = date('Y-m-d', strtotime($tempData['effective_date']));
            // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->organisation_model->addNewOrganisationDepartment($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }

    function deleteOrganisationDepartment($id)
    {
            // echo "<Pre>"; print_r($id);exit();
      
      $inserted_id = $this->organisation_model->deleteOrganisationDepartment($id);
        
        echo "Success"; 
    }
}

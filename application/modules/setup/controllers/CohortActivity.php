<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CohortActivity extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('cohort_model');
        $this->load->model('programme_model');

        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('cohort.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['cohortList'] = $this->cohort_model->cohortListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Cohort List';
            $this->loadViews("cohort_activity/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('cohort.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {


                $id_cohort = $this->security->xss_clean($this->input->post('id_cohort'));
                $id_scholarship_sub_thrust = $this->security->xss_clean($this->input->post('id_scholarship_sub_thrust'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $activity_name = $this->security->xss_clean($this->input->post('activity_name'));
                $id_communication_template = $this->security->xss_clean($this->input->post('id_communication_template'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_cohort' => $id_cohort,
                    'id_scholarship_sub_thrust' => $id_scholarship_sub_thrust,
                    'id_programme' => $id_programme,
                    'activity_name' => $activity_name,
                    'id_communication_template' => $id_communication_template,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->cohort_model->add($data);
                redirect('/scholarship/cohortActivity/list');
            }
            $data['communicationList'] = $this->cohort_model->communicationTemplate();
            $data['cohortList'] = $this->cohort_model->cohortList();
            $data['programmeList'] = $this->cohort_model->programmeList();
            $data['scholarshipList'] = $this->cohort_model->scholarshipList();

            $this->global['pageTitle'] = 'Scholarship Management System : Add Cohort';
            $this->loadViews("cohort_activity/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('cohort.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
               if($this->input->post())
            {

                $id_cohort = $this->security->xss_clean($this->input->post('id_cohort'));
                $id_scholarship_sub_thrust = $this->security->xss_clean($this->input->post('id_scholarship_sub_thrust'));
                $id_programme = $this->security->xss_clean($this->input->post('id_programme'));
                $activity_name = $this->security->xss_clean($this->input->post('activity_name'));
                $id_communication_template = $this->security->xss_clean($this->input->post('id_communication_template'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_cohort' => $id_cohort,
                    'id_scholarship_sub_thrust' => $id_scholarship_sub_thrust,
                    'id_programme' => $id_programme,
                    'activity_name' => $activity_name,
                    'id_communication_template' => $id_communication_template,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'status' => $status
                );

                $result = $this->cohort_model->edit($data,$id);
                redirect('/scholarship/cohortActivity/list');
            }
            
            $data['communicationList'] = $this->cohort_model->communicationTemplate();
            $data['cohortList'] = $this->cohort_model->cohortList();
            $data['programmeList'] = $this->cohort_model->programmeList();
            $data['scholarshipList'] = $this->cohort_model->scholarshipList();
            $data['cohortActivityDetails'] = $this->cohort_model->getCohort($id);

            $this->global['pageTitle'] = 'Scholarship Management System : Edit Cohort';
            $this->loadViews("cohort_activity/edit", $this->global, $data, NULL);
        }
    }
}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Race extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('race_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('race.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['raceList'] = $this->race_model->raceListSearch($name);
            $this->global['pageTitle'] = 'Scholarship Management System : Race List';
            $this->loadViews("race/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('race.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->race_model->addNewRace($data);
                redirect('/scholarship/race/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add Race';
            $this->loadViews("race/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('race.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/race/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->race_model->editRace($data,$id);
                redirect('/scholarship/race/list');
            }
            $data['race'] = $this->race_model->getRace($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Race';
            $this->loadViews("race/edit", $this->global, $data, NULL);
        }
    }
}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class PartnerUniversity extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('partner_university_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('partner_university.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_country'] = $this->security->xss_clean($this->input->post('id_country'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['url'] = $this->security->xss_clean($this->input->post('url'));
            $formData['email'] = $this->security->xss_clean($this->input->post('email'));
            $formData['contact_number'] = $this->security->xss_clean($this->input->post('contact_number'));
 
            $data['searchParam'] = $formData;
            $data['partnerUniversityList'] = $this->partner_university_model->partnerUniversityListSearch($formData);
            $data['countryList'] = $this->partner_university_model->countryListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Partner University List';
            //print_r($subjectDetails);exit;
            $this->loadViews("partner_university/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('partner_university.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_scholar_session_id;

                 $formData = $this->input->post();

                // $certificate_name = $_FILES['certificate']['name'];
                // $certificate_size = $_FILES['certificate']['size'];
                // $certificate_tmp =$_FILES['certificate']['tmp_name'];

                // $certificate_ext=explode('.',$certificate_name);
                // $certificate_ext=end($certificate_ext);
                // $certificate_ext=strtolower($certificate_ext);


                // $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'Certificate');

                // $certificate = $this->uploadFile($certificate_name,$certificate_tmp,'Certificate');


                    // echo "<Pre>";print_r($certificate);exit;



                // echo "<Pre>"; print_r($formData);exit;

                $name = $this->security->xss_clean($this->input->post('name'));
                $short_name = $this->security->xss_clean($this->input->post('short_name'));
                $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $fax = $this->security->xss_clean($this->input->post('fax'));
                $url = $this->security->xss_clean($this->input->post('url'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $address1 = $this->security->xss_clean($this->input->post('address1'));
                $address2 = $this->security->xss_clean($this->input->post('address2'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $login_id = $this->security->xss_clean($this->input->post('login_id'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $id_contact_person = $this->security->xss_clean($this->input->post('id_contact_person'));
                $location = $this->security->xss_clean($this->input->post('location'));


                $check_login_id = $this->partner_university_model->checkDuplicationLoginId($login_id,$email);
                if($check_login_id)
                {
                    echo "<Pre>";print_r('Login Id or Email-Id Already Exist, Select Another One');exit();
                }

            
                $data = array(
                    'name' => $name,
                    'short_name' => $short_name,
                    'name_optional_language' => $name_optional_language,
                    'code' => $code,
                    'fax' => $fax,
                    'url' => $url,
                    'id_country' => $id_country,
                    'contact_number' => $contact_number,
                    'address1' => $address1,
                    'address2' => $address2,
                    'location' => $location,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'email' => $email,
                    'id_contact_person' => $id_contact_person,
                    'login_id' => $login_id,
                    'password' => $password,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->partner_university_model->addNewPartnerUniversity($data);
                redirect('/setup/partnerUniversity/edit/'.$result);
            }
            //print_r($data['stateList']);exit;
            //$this->load->model('partner_university_model');
            $data['countryList'] = $this->partner_university_model->countryListByStatus('1');
            // $data['partnerCategoryList'] = $this->partner_university_model->partnerCategoryListByStatus('1');
            $data['partnerUniversityList'] = $this->partner_university_model->partnerUniversityListByStatus('1');
            $data['staffList'] = $this->partner_university_model->staffListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add Partner University';
            $this->loadViews("partner_university/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('partner_university.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/partnerUniversity/list');
            }
            if($this->input->post())
            {

            $resultprint = $this->input->post();

                // echo "<Pre>"; print_r($resultprint);exit();
            if($resultprint)
            {
             switch ($resultprint['btn_submit'])
             {

                case '1':
                    $id_user = $this->session->userId;
                    $id_session = $this->session->my_scholar_session_id;

                     $formData = $this->input->post();



                    $name = $this->security->xss_clean($this->input->post('name'));
                    $short_name = $this->security->xss_clean($this->input->post('short_name'));
                    $name_optional_language = $this->security->xss_clean($this->input->post('name_optional_language'));
                    $code = $this->security->xss_clean($this->input->post('code'));
                    $fax = $this->security->xss_clean($this->input->post('fax'));
                    $url = $this->security->xss_clean($this->input->post('url'));
                    $id_country = $this->security->xss_clean($this->input->post('id_country'));
                    $status = $this->security->xss_clean($this->input->post('status'));
                    $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                    $address1 = $this->security->xss_clean($this->input->post('address1'));
                    $address2 = $this->security->xss_clean($this->input->post('address2'));
                    $id_country = $this->security->xss_clean($this->input->post('id_country'));
                    $id_state = $this->security->xss_clean($this->input->post('id_state'));
                    $city = $this->security->xss_clean($this->input->post('city'));
                    $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                    $email = $this->security->xss_clean($this->input->post('email'));
                    $start_date = $this->security->xss_clean($this->input->post('start_date'));
                    $end_date = $this->security->xss_clean($this->input->post('end_date'));
                    $old_cert = $this->security->xss_clean($this->input->post('cert'));
                    $login_id = $this->security->xss_clean($this->input->post('login_id'));
                    $password = $this->security->xss_clean($this->input->post('password'));
                    $id_contact_person = $this->security->xss_clean($this->input->post('id_contact_person'));
                    $location = $this->security->xss_clean($this->input->post('location'));


                    $check_login_id = $this->partner_university_model->checkDuplicationLoginIdExist($login_id,$email,$id);
                    if($check_login_id)
                    {
                        echo "<Pre>";print_r('Login Id or Email-Id Already Exist, Select Another One');exit();
                    }


                
                    $data = array(
                        'name' => $name,
                        'short_name' => $short_name,
                        'name_optional_language' => $name_optional_language,
                        'code' => $code,
                        'fax' => $fax,
                        'url' => $url,
                        'id_country' => $id_country,
                        'contact_number' => $contact_number,
                        'start_date' => date('Y-m-d', strtotime($start_date)),
                        'end_date' => date('Y-m-d', strtotime($end_date)),
                        // 'certificate' => $certificate,
                        'address1' => $address1,
                        'address2' => $address2,
                        'location' => $location,
                        'id_country' => $id_country,
                        'id_state' => $id_state,
                        'city' => $city,
                        'zipcode' => $zipcode,
                        'id_contact_person' => $id_contact_person,
                        'email' => $email,
                        'login_id' => $login_id,
                        'password' => $password,
                        'status' => $status,
                        'updated_by' => $id_user
                    );

        
                    // echo "<Pre>"; print_r($data);exit;
                    
                    $result = $this->partner_university_model->editPartnerUniversity($data,$id);

                    redirect('/setup/partnerUniversity/list');

                break;



                case '4':

                // echo "<Pre>"; print_r($resultprint['btn_submit']);exit();
                // echo "<Pre>"; print_r($resultprint);exit();

                $certificate_name = $_FILES['moa_file']['name'];
                $certificate_size = $_FILES['moa_file']['size'];
                $certificate_tmp =$_FILES['moa_file']['tmp_name'];
                
                // echo "<Pre>"; print_r($certificate_tmp);exit();

                $certificate_ext=explode('.',$certificate_name);
                $certificate_ext=end($certificate_ext);
                $certificate_ext=strtolower($certificate_ext);


                $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'MOA File');

                $moa_file = $this->uploadFile($certificate_name,$certificate_tmp,'MOA File');


                $start_date = $this->security->xss_clean($this->input->post('moa_start_date'));
                $end_date = $this->security->xss_clean($this->input->post('moa_end_date'));

                $moa = array(
                        'start_date' => date('Y-m-d', strtotime($start_date)),
                        'end_date' => date('Y-m-d', strtotime($end_date)),
                        'id_partner_university' => $id
                    );


                if($moa_file != '')
                {
                    $moa['file'] = $moa_file;
                }

                // echo "<Pre>"; print_r($moa_file);exit();

                $result = $this->partner_university_model->addNewAggrement($moa);

                
                redirect($_SERVER['HTTP_REFERER']);;
                    
                    break;

                }
            }


                
            }
            $data['partnerUniversity'] = $this->partner_university_model->getPartnerUniversity($id);
            $data['trainingCenterList'] = $this->partner_university_model->trainingCenterList($id);
            $data['countryList'] = $this->partner_university_model->countryListByStatus('1');
            $data['stateList'] = $this->partner_university_model->stateListByStatus('1');
            $data['staffList'] = $this->partner_university_model->staffListByStatus('1');
            $data['programList'] = $this->partner_university_model->programListByStatus('1');
            $data['moduleTypeList'] = $this->partner_university_model->moduleTypeListByStatus('1');


            $data['getPartnerUniversityAggrementList'] = $this->partner_university_model->getPartnerUniversityAggrementList($id);
            $data['partnerProgramDetails'] = $this->partner_university_model->partnerProgramDetails($id);
            if($data['partnerProgramDetails'])
            {
                $data['partnerProgramStudyModeDetails'] = $this->partner_university_model->partnerProgramStudyModeDetails($id,$data['partnerProgramDetails']->id);
                $data['partnerProgramApprenticeshipDetails'] = $this->partner_university_model->partnerProgramApprenticeshipDetails($id,$data['partnerProgramDetails']->id);
                $data['partnerProgramInternshipDetails'] = $this->partner_university_model->partnerProgramInternshipDetails($id,$data['partnerProgramDetails']->id);
                $data['partnerProgramSyllabusDetails'] = $this->partner_university_model->partnerProgramSyllabusDetails($id,$data['partnerProgramDetails']->id);
            }

            
                // echo "<Pre>"; print_r($data['partnerProgramDetails']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Edit Partner University';
            $this->loadViews("partner_university/edit", $this->global, $data, NULL);
        }
    }


    function getPartnerUniversity()
    {
        $id_partner_category = $this->security->xss_clean($this->input->post('id_partner_category'));

        $partner_category = $this->partner_university_model->getPartnerUniversityCategory($id_partner_category);

        $name = $partner_category->name;
        $code = $partner_category->code;
        $value = 0;

        if($name == 'Franchise')
        {
            $value= 1;
        }
        echo $value;exit();
    }

    function getStateByCountry($id_country)
    {
            $results = $this->partner_university_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($programme_data);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }


    function getStateByCountryForTraining($id_country)
    {
            $results = $this->partner_university_model->getStateByCountryId($id_country);

            // echo "<Pre>"; print_r($results);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_training_state' id='id_training_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }



    function deleteMoaAggrement($id)
    {
            // echo "<Pre>"; print_r($id);exit();
      
      $inserted_id = $this->partner_university_model->deleteMoaAggrement($id);
        
        echo "Success"; 
    }

     function addTrainingCenter()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->partner_university_model->addTrainingCenter($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }

    function deleteTrainingCenter($id)
    {
            // echo "<Pre>"; print_r($id);exit();
      
      $inserted_id = $this->partner_university_model->deleteTrainingCenter($id);
        
        echo "Success"; 
    }

    function saveProgramDetailsData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $id = $tempData['id'];
        unset($tempData['id']);

        if($id > 0)
        {
            $inserted_id = $this->partner_university_model->editPartnerUniversityProgram($tempData,$id);
        }
        else
        {
            $inserted_id = $this->partner_university_model->addNewPartnerUniversityProgram($tempData);
        }

        if($inserted_id)
        {
            echo "success";
        }
        // echo "<Pre>"; print_r($tempData);exit();
    }


    function addProgramStudyModeData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['start_date'] = date('Y-m-d', strtotime($tempData['start_date']));
        $tempData['end_date'] = date('Y-m-d', strtotime($tempData['end_date']));

        $inserted_id = $this->partner_university_model->addNewPartnerProgramStudyMode($tempData);
        // echo "<Pre>"; print_r($inserted_id);exit();

        if($inserted_id)
        {
            echo "success";
        }
    }

    function addProgramInternshipData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();

        $tempData['start_date'] = date('Y-m-d', strtotime($tempData['start_date']));
        $tempData['end_date'] = date('Y-m-d', strtotime($tempData['end_date']));

        $inserted_id = $this->partner_university_model->addNewPartnerProgramInternship($tempData);
        // echo "<Pre>"; print_r($inserted_id);exit();

        if($inserted_id)
        {
            echo "success";
        }
    }

    function addProgramApprenticeData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['start_date'] = date('Y-m-d', strtotime($tempData['start_date']));
        $tempData['end_date'] = date('Y-m-d', strtotime($tempData['end_date']));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->partner_university_model->addNewPartnerProgramApprenticeship($tempData);

        if($inserted_id)
        {
            echo "success";
        }
    }

    function addProgramSyllabusData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->partner_university_model->addNewPartnerProgramSyllabus($tempData);

        if($inserted_id)
        {
            echo "success";
        }
    }

    function deleteProgramModeOfStudy($id)
    {
        $deleted = $this->partner_university_model->deletePartnerProgramStudyMode($id);
        echo "Success";
    }

    function deleteProgramInternship($id)
    {
        $deleted = $this->partner_university_model->deletePartnerProgramInternship($id);
        echo "Success";
    }

    function deleteProgramApprenticeship($id)
    {
        $deleted = $this->partner_university_model->deletePartnerProgramApprenticeship($id);
        echo "Success";
    }

    function deleteProgramSyllabus($id)
    {
        $deleted = $this->partner_university_model->deletePartnerProgramSyllabus($id);
        echo "Success";
    }
}

<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Scholar_status_model extends CI_Model
{
    function list()
    {
        $this->db->select('*');
        $this->db->from('status_list');
        $this->db->where('id_status_master=2');

        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


     function get($id)
    {
        $this->db->select('*');
        $this->db->from('status_list');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    
    function addNew($data)
    {
        $this->db->trans_start();
        $this->db->insert('status_list', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function edit($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('status_list', $data);
        return TRUE;
    }
}


<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Documents_scholar_model extends CI_Model
{
    
    function ScholarListByStatus()
    {
        $this->db->select('*');
        $this->db->from('scholarship_sub_thrust');
        $query = $this->db->get();
        return $query->result();
    }
    
    function addNewDocumentsProgram($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_documents_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editDocumentsProgram($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_documents_program', $data);
        return TRUE;
    }

    function addTempDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_scholarship_documents_program_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteTempData($id) {
        $this->db->where('id', $id);
        $this->db->delete('temp_scholarship_documents_program_details');
        return TRUE;
    }


    function deleteTempDataBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
        $this->db->delete('temp_scholarship_documents_program_details');
        return TRUE;
    }


    function getTempDetailsBySession($sessionid)
    {
        $this->db->select('tpe.*, p.scholarship_name');
        $this->db->from('scholarship_documents_scholarship_details as tpe');
        $this->db->join('scholarship_sub_thrust as p','tpe.id_scholarship_sub_thrust = p.id');
        $this->db->where('tpe.id_documents', $sessionid);
        $query = $this->db->get();
        return $query->result();
    }

    function getTempDetailsBySessionId($sessionid)
    {
        $this->db->select('tpe.*');
        $this->db->from('temp_scholarship_documents_program_details as tpe');
        $this->db->join('scholarship_programme as p','tpe.id_program = p.id');
        $this->db->where('tpe.id_session', $sessionid);
        $query = $this->db->get();
        return $query->result();
    }

    function addNewDocumentsProgramDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_documents_scholarship_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

}


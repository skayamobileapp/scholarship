<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Award_model extends CI_Model
{
    function awardList()
    {
        $this->db->select('a.*');
        $this->db->from('scholarship_award_setup as a');
        $this->db->order_by("id", "DESC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function awardListSearch($formData)
    {
        $this->db->select('a.*');
        $this->db->from('scholarship_award_setup as a');
        if (!empty($formData['name']))
        {
            $likeCriteria = "(a.name  LIKE '%" . $formData['name'] . "%' or a.code  LIKE '%" . $formData['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAward($id)
    {
        $this->db->select('*');
        $this->db->from('scholarship_award_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewAward($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_award_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editAward($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_award_setup', $data);
        return TRUE;
    }
}


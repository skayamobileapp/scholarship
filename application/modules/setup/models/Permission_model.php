<?php if(!defined('BASEPATH')) exit('No direct script access allowed');


class Permission_model extends CI_Model
{
    
    function permissionListingCount()
    {
        $this->db->select('BaseTbl.*');
        $this->db->from('scholar_permissions as BaseTbl');
         $query = $this->db->get();
        
        return $query->num_rows();
    }
    
    function permissionListSearch($data)
    {
        $this->db->select('BaseTbl.*');
        $this->db->from('scholar_permissions as BaseTbl');
        if ($data['name'] != '')
        {
            $likeCriteria = "(code  LIKE '%" . $data['name'] . "%' or description  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if($data['status'] != '')
        {
            $this->db->where('BaseTbl.status', $data['status']);
        }
        $this->db->order_by("id", "ASC");

         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }
    
    
    function permissionListing()
    {
        $this->db->select('BaseTbl.*');
        $this->db->from('scholar_permissions as BaseTbl');
         $query = $this->db->get();
         $result = $query->result();        
         return $result;
    }
    
    function addNewPermission($permissionInfo)
    {
        $this->db->trans_start();
        $this->db->insert('scholar_permissions', $permissionInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }
    
   
    function getPermission($permissionId)
    {
        $this->db->select('*');
        $this->db->from('scholar_permissions');
        $this->db->where('id', $permissionId);
        $query = $this->db->get();
        
        return $query->row();
    }
    
    function editPermission($permissionInfo, $permissionId)
    {
        $this->db->where('id', $permissionId);
        $this->db->update('scholar_permissions', $permissionInfo);
        
        return TRUE;
    }
    
    
    function deletePermission($permissionId, $permissionInfo)
    {
        $this->db->where('id', $permissionId);
        $this->db->update('scholar_permissions', $permissionInfo);
        
        return $this->db->affected_rows();
    }

    function rolePermissionListByPermissionId($id_permission)
    {
        
        $this->db->select('pupd.*, c.role');
        $this->db->from('scholar_role_permissions as pupd');
        $this->db->join('scholar_roles as c','pupd.id_role = c.id');
        $this->db->where('pupd.id_permission', $id_permission);
        $query = $this->db->get();
        return $query->result();
    }

    function rolesList()
    {
        $this->db->select('*');
        $this->db->from('scholar_roles');
        $this->db->order_by("role", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addRolesToPermission($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholar_role_permissions', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteRoleFromPermission($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholar_role_permissions');
        return TRUE;
    }

}

  
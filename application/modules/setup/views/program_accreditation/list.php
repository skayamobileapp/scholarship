<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Program Accreditation</h3>
      <a href="add" class="btn btn-primary">+ Add Program Accreditation</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                    <label class="col-sm-4 control-label">Accreditation Category</label>
                    <div class="col-sm-8">
                      <select name="id_accreditation_category" id="id_accreditation_category" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($accreditationCategoryList)) {
                          foreach ($accreditationCategoryList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_accreditation_category']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                    <label class="col-sm-4 control-label">Accreditation Type</label>
                    <div class="col-sm-8">
                      <select name="id_accreditation_type" id="id_accreditation_type" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($accreditationTypeList)) {
                          foreach ($accreditationTypeList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_accreditation_type']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>


                </div>


                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                    <label class="col-sm-4 control-label">Program</label>
                    <div class="col-sm-8">
                      <select name="id_program" id="id_program" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programList)) {
                          foreach ($programList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_program']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>


                </div>


              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Accreditation<br>Category</th>
            <th>Accreditation<br>Type</th>
            <th>Program</th>
            <th>Accreditation<br>Reference</th>
            <th>Accreditation<br>Date</th>
            <th>Accreditation<br>Number</th>
            <th>Valid From</th>
            <th>Valid To</th>
            <th>Approval Date</th>
            <!-- <th class="text-center">Status</th> -->
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($programAccreditationList))
          {
            $i=1;
            foreach ($programAccreditationList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->category_code . " - " . $record->category_name ?></td>
                <td><?php echo $record->type_code . " - " . $record->type_name ?></td>
                <td><?php echo $record->program_code . " - " . $record->program_name ?></td>
                <td><?php echo $record->reference_number ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->date_time)); ?></td>
                <td><?php echo $record->accreditation_number ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->valid_from)); ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->valid_to)); ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->approval_date)); ?></td>
                <!-- <td class="text-center"><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td> -->
                <!-- <td><?php echo date("d-m-Y", strtotime($record->created_dt_tm)) ?></td> -->
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a> | 
                  <a onclick="deleteProgramAccreditation(<?php echo $record->id; ?>)">Delete</a>
                  <!--  -->
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>

  function deleteProgramAccreditation(id)
    {
        $.get("/scholarship/programAccreditation/deleteProgramAccreditation/"+id,
            function(data, status)
            {
                window.location.reload();
            }
            );
    }


  
  $('select').select2();


  function clearSearchForm()
      {
        window.location.reload();
      }
</script>
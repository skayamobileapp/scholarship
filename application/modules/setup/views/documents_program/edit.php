<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Documents For Program</h3>
        </div>

            <div class="form-container">
                <h4 class="form-group-title">Documents For Program Details</h4>

               

  <div class="row">

              
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $documentDetails->name; ?>" readonly="readonly">
                    </div>
                </div>

               


                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Type <span class='error-text'>*</span></label>
                         <input type="text" class="form-control" id="name" name="name" value="<?php echo $documentDetails->file_type; ?>" readonly="readonly">

                          
                    </div>
                </div>

                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max File Size (MB) <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="file_size" name="file_size" value="<?php echo $documentDetails->file_size; ?>" readonly="readonly">
                    </div>
                </div>
            </div>

       
   </div>
        <form id="form_intake" action="" method="post">

      
            <div class="form-container">
                <h4 class="form-group-title">Documents For Programme Details</h4>


                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Program <span class='error-text'>*</span></label>
                            <select name="id_program" id="id_program" class="form-control">
                                <option value="">Select</option>
                                <?php
                                if (!empty($programList))
                                {
                                    foreach ($programList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->name; ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                     <div class="col-sm-4">
                        <div class="form-group">
                            <p>Mandatory <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="mandatory" id="mandatory" value="1" ><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="mandatory" id="mandatory" value="0" >
                              <span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                </div>      
                  
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Add</button>
                    </div>
                </div>

                <div class="row">
                    <div class='custom-table'>
        <table  class='table' id='list-tabled'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Program</th>
                    <th>Mandatory</th>
                    <th>Action</th>
                </tr>
                </thead>
                    <?php for($i=0;$i<count($temp_details);$i++)
                    {

                    $id = $temp_details[$i]->id;
                    $program_name = $temp_details[$i]->program_name;
                    $program_code = $temp_details[$i]->program_code;
                    if($temp_details[$i]->mandatory=='1') {
                        $mandatory ='Yes';
                    } else {
                        $mandatory =' No';
                    }

                    $j = $i+1;?>
                        <tbody>
                        <tr>
                            <td><?php echo $j;?> </td>
                            <td><?php echo $program_code.'-'.$program_name;?> </td>
                            <td><?php echo $mandatory;?>                         
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempDocumentsProgram($id)'>Delete</a>
                            <td>
                        </tr>
                    <?php } ?>
                                              
            </tbody>
            </table>
        </div>"; 
                </div>
                
            </div>
        </form>


           

      </div>
    </div>

   </div> <!-- END row-->

           
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

        

    function deleteDocumentsProgram(id) {
                    // alert(id);

            $.ajax(
            {
               url: '/scholarship/documentsProgram/deleteDocumentsProgram/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // $("#view").html(result);
                    // var ta = $("#inv-total-amount").val();
                // $("#total_amount").val(ta);
                    // alert(result);
                    window.location.reload();
               }
            });
    }


    function validateDetailsData()
    {
       
                $('#form_intake').submit();
            
    }

    $(document).ready(function() {
        $("#form_programme_intake").validate({
            rules: {
                id_program: {
                    required: true
                }
            },
            messages: {
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>
<script type="text/javascript">
    $('select').select2();
</script>
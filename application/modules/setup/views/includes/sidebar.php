            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $scholar_name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <!-- <li><a href="/sponser/user/profile">Edit Profile</a></li> -->
                            <li><a href="/setup/role/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    <h4>User Management</h4>
                    <ul>
                        <li><a href="/setup/role/list">Roles</a></li>
                        <li><a href="/setup/permission/list">Permissions</a></li>
                        <li><a href="/setup/user/list">User</a></li>

                        <li><a href="/setup/salutation/list">Salutation</a></li>
                        <li><a href="/setup/accreditationCategory/list">Accreditation Category</a></li>
                        <li><a href="/setup/accreditationType/list">Accreditation Type</a></li>
                        <li><a href="/setup/salutation/list">Salutation</a></li>
                    </ul>
                    <h4>Demographic Setup</h4>
                    <ul>
                        <li><a href="/setup/race/list">Race</a></li>
                        <li><a href="/setup/religion/list">Religion</a></li>
                        <li><a href="/setup/maritalStatus/list">Marital Status</a></li>

                    </ul>
                    <h4>Organisation Setup</h4>
                    <ul>
                        <li><a href="/setup/department/list">Department</a></li>
                        <li><a href="/setup/organisation/edit">Organisation Setup</a></li>
                       
                    </ul>
                     <h4>Documents Required</h4>
                    <ul>
                        <li><a href="/setup/documents/list">Documents</a></li>
                        <li><a href="/setup/documentsProgram/list">Documents For Program</a></li>
                    <li><a href="/setup/documentsScholar/list">Documents For Scholarship</a></li>

                    </ul>

                     <h4>Status Management</h4>
                    <ul>
                        <li><a href="/setup/applicantStatus/list">Application Status</a></li>
                         <li><a href="/setup/scholarStatus/list">Scholar Status</a></li>
                            <li><a href="/setup/aluminiStatus/list">Alumni Status</a></li>
                    </ul>


                   <!--  <h4>Program Setup</h4>
                    <ul>
                        <li><a href="/setup/award/list">Award Setup</a></li>
                        <li><a href="/setup/programme/list">Program Setup</a></li>
                    <li><a href="/setup/programSyllabus/list">Program Syllabus</a></li>

                        <li><a href="/setup/programPartner/list">Program Partner</a></li>
                        <li><a href="/setup/programAccreditation/list">Program Accreditation</a></li>
                        <li><a href="/setup/IndividualEntryRequirement/list">Entry Requirement</a></li>
                        <li><a href="/setup/applicantInformation/programList">Application form Set-up</a></li>
                    </ul>


                    <h4>Program Partner</h4>
                    <ul>
                        
                        <li><a href="/setup/programPartner/list">Program Partner</a></li>
                         
                    </ul>

                    <h4>Scholarship Setup</h4>
                    <ul>
                        <li><a href="/setup/educationLevel/list">Education Level</a></li>
                        <li><a href="/setup/selectionType/list">Selection Type</a></li>
                        <li><a href="/setup/thrust/list">Scholarship Thrust</a></li>
                        <li><a href="/setup/subThrust/list">Scholarship Sub-Thrust</a></li>
                        <li><a href="/setup/workSpecialisation/list">Work Specialisation</a></li>

                    </ul>
                   
                    <h4>Cohort</h4>
                    <ul>
                        <li><a href="/setup/scheme/list">Cohort</a></li>
                        <li><a href="/setup/cohortActivity/list">Calendar of Activities</a></li>

                    </ul>
                    <h4>Selection</h4>
                    <ul>
                        <li><a href="/setup/scholarshipApplication/approvalList">Application Approval</a></li>
                        <li><a href="/setup/scholarshipApplication/list">Application Summary</a></li>
                    </ul>
                     <h4>Report</h4>
                    <ul>
                        <li><a href="/setup/configurableReport/add">Configurable Report</a></li>
                        <li><a href="/setup/configurableReport/list">Finance  Report Status</a></li>
                    </ul> -->

                </div>
            </div>
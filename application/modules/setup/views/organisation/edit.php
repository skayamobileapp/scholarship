<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Organisation</h3>
        </div>
        <form id="form_award" action="" method="post" enctype="multipart/form-data">

         <div class="form-container">
            <h4 class="form-group-title">University Details</h4>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $organisation->code; ?>">
                        <input type="hidden" class="form-control datepicker" id="id_organisation" name="id_organisation" value="<?php echo $id_organisation; ?>" autocomplete="off">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $organisation->name; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Short Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="short_name" name="short_name" value="<?php echo $organisation->short_name; ?>">
                    </div>
                </div>

                
                
            </div>

             <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name In Other Language </label>
                        <input type="text" class="form-control" id="name_in_malay" name="name_in_malay" value="<?php echo $organisation->name_in_malay; ?>">
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Website <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="url" name="url" value="<?php echo $organisation->url; ?>">
                    </div>
                </div>



                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($organisation->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($organisation->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>  


               <!--  <div class="col-sm-4">
                    <div class="form-group">
                        <label>FILE <span class='error-text'>*</span></label>
                        <input type="file" name="image" />
                    </div>                    
                </div> -->

                             
            </div>

        </div>


        <!-- <div class="form-container">
            <h4 class="form-group-title">Registrar Details</h4>

              <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Registrar <span class='error-text'>*</span></label>
                        <select name="id_registrar" id="id_registrar" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($staffList))
                            {
                                foreach ($staffList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $organisation->id_registrar)
                                        {
                                            echo "selected=selected";
                                        } ?>>
                                        <?php echo $record->ic_no . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Joining Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" value="<?php echo date('d-m-Y', strtotime($organisation->date_time)); ?>" autocomplete="off">

                        
                    </div>
                </div>



            </div>

        </div> -->

        <div class="form-container">
                <h4 class="form-group-title">Contact Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contace Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="contact_number" name="contact_number" value="<?php echo $organisation->contact_number; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Contace Email <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="email" name="email" value="<?php echo $organisation->email; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fax Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="fax_number" name="fax_number" value="<?php echo $organisation->fax_number; ?>">
                    </div>
                </div>


                

            </div>


            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 1 <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="address1" name="address1" value="<?php echo $organisation->address1 ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Address 2</label>
                        <input type="text" class="form-control" id="address2" name="address2" value="<?php echo $organisation->address2 ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Country <span class='error-text'>*</span></label>
                        <select name="id_country" id="id_country" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($countryList))
                            {
                                foreach ($countryList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>" <?php if($organisation->id_country==$record->id){ echo "selected"; } ?>>
                                <?php echo $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                
            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>State <span class='error-text'>*</span></label>
                        <select name="id_state" id="id_state" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($stateList))
                            {
                                foreach ($stateList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>" <?php if($organisation->id_state==$record->id){ echo "selected"; } ?>>
                                <?php echo $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>City <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="city" name="city" value="<?php echo $organisation->city ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Zipcode <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?php echo $organisation->zipcode ?>">
                    </div>
                </div>
            </div>
        </div>

        

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <!-- <a href="../list" class="btn btn-link">Cancel</a> -->
            </div>
        </div>


        </form>



        <div class="form-container">
            <h4 class="form-group-title"> Organisation Comitee Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Comitee Details</a>
                    </li>
                    <li role="presentation"><a href="#tab_department" class="nav-link border rounded text-center"
                            aria-controls="tab_department" role="tab" data-toggle="tab">Department Details</a>
                    </li>
                   <!--  <li role="presentation">
                        <a href="#tab_department" class="nav-link border rounded text-center"
                            aria-controls="tab_department" role="tab" data-toggle="tab">Department Details</a>
                    </li>   -->           
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <form id="form_comitee" action="" method="post">


                                <br>

                                <div class="row">

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Select Role <span class='error-text'>*</span></label>
                                            <select name="role" id="role" class="form-control">
                                                <option value="">Select</option>
                                                <option value="CEO">CEO</option>
                                                <option value="COO">COO</option>
                                                <option value="HOD PSDM">HOD PSDM</option>
                                                <option value="HOD Finance">HOD Finance</option>
                                                <option value="HOD Support Services">HOD Support Services   </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Name <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="com_name" name="com_name">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Email <span class='error-text'>*</span></label>
                                            <!-- <label>NRIC <span class='error-text'>*</span></label> -->
                                            <input type="email" class="form-control" id="com_nric" name="com_nric">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="forintake_has_programmem-group">
                                            <label>Effective Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="effective_date" name="effective_date" autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                                    </div>
                                
                                </div>


                        </form>


                        <?php

                            if(!empty($organisationComiteeList))
                            {
                                ?>

                                <div class="form-container">
                                        <h4 class="form-group-title">Comitee Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Role</th>
                                                 <th>Name</th>
                                                 <!-- <th>NRIC</th> -->
                                                 <th>Email</th>
                                                 <th>Effective Date</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($organisationComiteeList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $organisationComiteeList[$i]->role;?></td>
                                                <td><?php echo $organisationComiteeList[$i]->name;?></td>
                                                <td><?php echo $organisationComiteeList[$i]->nric;?></td>
                                                <td><?php echo date('d-m-Y', strtotime($organisationComiteeList[$i]->effective_date));?></td>
                                                <td>
                                                <a onclick="deleteOrganisationConitee(<?php echo $organisationComiteeList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div> 
                    </div>










                    <div role="tabpanel" class="tab-pane" id="tab_department">
                        <div class="col-12 mt-4">




                        <form id="form_department" action="" method="post">


                                <br>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Department <span class='error-text'>*</span></label>
                                            <select name="id_department" id="id_department" class="form-control" style="width: 350px">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($departmentList))
                                                {
                                                    foreach ($departmentList as $record)
                                                    {?>
                                                <option value="<?php echo $record->id;  ?>">
                                                    <?php echo $record->code . " - " . $record->name;?>
                                                </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveOrganisationHasDepartment()">Add</button>
                                    </div>
                                
                                </div>


                        </form>


                        <?php

                            if(!empty($organisationDepartmentList))
                            {
                                ?>

                                <div class="form-container">
                                        <h4 class="form-group-title">Department Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Department</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($organisationDepartmentList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $organisationDepartmentList[$i]->department_code . " - " . $organisationDepartmentList[$i]->department_name;?></td>
                                                <td>
                                                <a onclick="deleteOrganisationDepartment(<?php echo $organisationDepartmentList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div> 
                    </div>



                </div>

            </div>
        </div> 


        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    function saveData()
    {
        if($('#form_comitee').valid())
        {

        var tempPR = {};
        tempPR['role'] = $("#role").val();
        tempPR['name'] = $("#com_name").val();
        tempPR['nric'] = $("#com_nric").val();
        tempPR['effective_date'] = $("#effective_date").val();
        tempPR['id_organisation'] = <?php echo $organisation->id;?>;
            $.ajax(
            {
               url: '/scholarship/organisation/addOrganisationComitee',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                location.reload();
                // $("#view").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }



    function deleteOrganisationConitee(id)
    {
        $.ajax(
            {
               url: '/scholarship/organisation/deleteOrganisationConitee/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert(id);
                    location.reload();
               }
            });
    }


    $(document).ready(function() {
        $("#form_comitee").validate({
            rules: {
                role: {
                    required: true
                },
                com_name: {
                    required: true
                },
                com_nric: {
                    required: true
                },
                effective_date: {
                    required: true
                }
            },
            messages: {
                role: {
                    required: "<p class='error-text'>Select Role</p>",
                },
                com_name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                com_nric: {
                    required: "<p class='error-text'>Email Required</p>",
                    // required: "<p class='error-text'>NRIC Required</p>",
                },
                effective_date: {
                    required: "<p class='error-text'>Select Effective Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    function saveOrganisationHasDepartment()
    {
        if($('#form_department').valid())
        {

        var tempPR = {};
        tempPR['id_department'] = $("#id_department").val();
        tempPR['id_organisation'] = <?php echo $organisation->id;?>;
            $.ajax(
            {
               url: '/scholarship/organisation/addOrganisationDepartment',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                location.reload();
                // $("#view").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }

    function deleteOrganisationDepartment(id)
    {
        $.ajax(
            {
               url: '/scholarship/organisation/deleteOrganisationDepartment/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert(id);
                    location.reload();
               }
            });
    }


    $(document).ready(function() {
        $("#form_department").validate({
            rules: {
                id_department: {
                    required: true
                }
            },
            messages: {
                id_department: {
                    required: "<p class='error-text'>Select Department</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });







    $(document).ready(function() {
        $("#form_award").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                short_name: {
                    required: true
                },
                url: {
                    required: true
                },
                id_country: {
                    required: true
                },
                contact_number: {
                    required: true
                },
                address1: {
                    required: true
                },
                id_registrar: {
                    required: true
                },
                date_time: {
                    required: true
                },
                email: {
                    required: true
                },
                status: {
                    required: true
                },
                id_state: {
                    required: true
                },
                city: {
                    required: true
                },
                zipcode: {
                    required: true
                },
                fax_number: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>University Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                short_name: {
                    required: "<p class='error-text'>University Short Name Required</p>",
                },
                url: {
                    required: "<p class='error-text'>Website Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Select Country</p>",
                },
                contact_number: {
                    required: "<p class='error-text'>Contact Number Required</p>",
                },
                permanent_address: {
                    required: "<p class='error-text'>Address1 Required</p>",
                },
                id_registrar: {
                    required: "<p class='error-text'>Select Registrar</p>",
                },
                date_time: {
                    required: "<p class='error-text'>Select Joining Date</p>",
                },
                email: {
                    required: "<p class='error-text'>Contact Email Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Status Required</p>",
                },
                id_state: {
                    required: "<p class='error-text'>Select State</p>",
                },
                city: {
                    required: "<p class='error-text'>City Required</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                fax_number: {
                    required: "<p class='error-text'>Fax Number Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $('select').select2();

  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true
    });
  } );
</script>
 <link rel="stylesheet" href="<?php echo BASE_PATH; ?>/assets/css/query-builder.default.min.css">
<script src="http://boiteaimages.com/demo/querybuilder/query-builder.js"></script>

<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Configurable Report</h3>
        </div>
        <form id="form_unit" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Configurable Report Details</h4>

            <div class="row">
              <div id='builder-basic'></div>
                
              
            </div>

        </div>




        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

  $(document).ready(function(){
    var rules_basic = {
  condition: 'AND',
  rules: [{
    id: 'price',
    operator: 'less',
    value: 10.25
  }, {
    condition: 'OR',
    rules: [{
      id: 'category',
      operator: 'equal',
      value: 2
    }]
  }]
};

$('#builder-basic').queryBuilder({
  plugins: ['bt-tooltip-errors'],
  
  filters: [{
    id: 'name',
    label: 'Name',
    type: 'string'
  }, {
    id: 'category',
    label: 'Application Status',
    type: 'integer',
    input: 'select',
    values: {
      1: 'Draft',
      2: 'Entry',
      3: 'Approval'
    },
    operators: ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null']
  }, {
    id: 'in_stock',
     label: 'Scholar Name',
    type: 'string'
  }, {
    id: 'price',
    label: 'Payment Amount',
    type: 'double',
    validation: {
      min: 0,
      step: 0.01
    }
  }],

  rules: rules_basic
});

$('#btn-reset').on('click', function() {
  $('#builder-basic').queryBuilder('reset');
});

$('#btn-set').on('click', function() {
  $('#builder-basic').queryBuilder('setRules', rules_basic);
});

$('#btn-get').on('click', function() {
  var result = $('#builder-basic').queryBuilder('getRules');
  
  if (!$.isEmptyObject(result)) {
    alert(JSON.stringify(result, null, 2));
  }
});
  })


</script>

<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Cohort</h3>
        </div>
        <form id="form_sponser" action="" method="post">

            <div class="form-container">
                    <h4 class="form-group-title">Cohort Details</h4>  
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="code" name="code">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Start Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" autocomplete="off">
                        </div>
                    </div>

                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label>Max. Scholrship Percentage <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="percentage" name="percentage" min="1" max="100">
                        </div>
                    </div> -->

                </div>



                <div class="row">

                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <label>Message <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="message" name="message">
                        </div>
                    </div> -->


                    


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>End Date <span class='error-text'>*</span></label>
                            <input type="text" class="form-control datepicker" id="to_dt" name="to_dt" autocomplete="off">
                        </div>
                    </div>

                    
                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                                </label>                              
                            </div>                         
                    </div>

                </div>
            </div>
        </form>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="validateProgram()">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        <form id="form_programme_intake" action="" method="post">
        <div class="form-container">
        <h4 class="form-group-title">Scholarship Offered</h4>

            <div class="row">

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Select Scholarship <span class='error-text'>*</span></label>
                        <select name="id_sub_thrust" id="id_sub_thrust" class="form-control" onchange="getProgramBySubThrustId(this.value)">
                            <option value="">Select</option>
                            <?php
                            if (!empty($subThrustList))
                            {
                                foreach ($subThrustList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->scholarship_code . " - " . $record->scholarship_name; ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <span id='view_program'></span>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Quota <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="quota" name="quota">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Budget <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="budget" name="budget">
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-3">
                    <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                </div>

            </div>


            <div class="row">
                <div id="view"></div>
            </div>

        </div>
    </form>



            
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    function getProgramBySubThrustId(id)
    {
        // $("#view_program").html(id);
        // alert(id);
        $.get("/scholarship/scheme/getProgramBySubThrustId/"+id,

            function(data, status)
            {
                $("#view_program").html(data);
                // $("#view_programme_details").html(data);
                // $("#view_programme_details").show();
        });
    }



    function saveData()
    {
        if($('#form_programme_intake').valid())
        {

        var tempPR = {};
        tempPR['id_program'] = $("#id_program").val();
        tempPR['id_sub_thrust'] = $("#id_sub_thrust").val();
        tempPR['quota'] = $("#quota").val();
        tempPR['budget'] = $("#budget").val();
            $.ajax(
            {
               url: '/scholarship/scheme/tempadd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                $("#view").html(result);
               }
            });
        }
    }

    function deleteTempSchemeHasProgramme(id) {
        // alert(id);
         $.ajax(
            {
               url: '/scholarship/scheme/deleteTempSchemeHasProgramme/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    $("#view").html(result);
               }
            });
    }

    function validateProgram()
    {

    if($('#form_sponser').valid())
      {
         console.log($("#view").html());
         var addedProgam = $("#view").html();
         if(addedProgam=='')
         {
            alert("Add Scholarship Offered to the Cohort");
         }
         else
         {
            $('#form_sponser').submit();
         }
      }     
    }


    $(document).ready(function() {
        $("#form_programme_intake").validate({
            rules: {
                id_sub_thrust: {
                    required: true
                },
                id_programme: {
                    required: true
                },
                quota: {
                    required: true
                }
            },
            messages: {
                id_sub_thrust: {
                    required: "<p class='error-text'>Select Scholarship</p>",
                },
                id_programme: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                quota: {
                    required: "<p class='error-text'>Quota Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $(function(){
    $(".datepicker").datepicker(
    {
        changeYear: true,
        changeMonth: true,
    });
  });


    $(document).ready(function() {
        $("#form_sponser").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                percentage: {
                    required: true
                },
                from_dt: {
                    required: true
                },
                to_dt: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                percentage: {
                    required: "<p class='error-text'>Percentage Required</p>",
                },
                from_dt: {
                    required: "<p class='error-text'>Select From Date</p>",
                },
                to_dt: {
                    required: "<p class='error-text'>Select To Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $('select').select2();

</script>
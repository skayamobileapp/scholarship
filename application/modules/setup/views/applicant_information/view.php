<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Scholarship Application form Set-up </h3>
            <!-- <a href="" class="btn btn-link">Back</a> -->
        </div>



    <div class="form-container">
        <h4 class="form-group-title">Program Details</h4>
        <div class='data-list'>
            <div class='row'> 
                <div class='col-sm-6'>
                    <dl>
                        <dt>Program Name :</dt>
                        <dd><?php echo ucwords($programme->name);?></dd>
                    </dl>
                    <dl>
                        <dt>Program Name Optional Language :</dt>
                        <dd><?php echo $programme->name_optional_language ?></dd>
                    </dl>
                                            
                </div>        
                
                <div class='col-sm-6'> 
                    <dl>
                        <dt>Program Code :</dt>
                        <dd><?php echo $programme->code; ?></dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>

        <br>



    <form id="form_profile" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Scholarship Requirement Details</h4>


                 <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Personel Details <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_personel_details" id="sd1" value="1" <?php if($programme->is_personel_details=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_personel_details" id="sd2" value="0" <?php if($programme->is_personel_details=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Education Details <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_education_details" id="sd1" value="1" <?php if($programme->is_education_details=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_education_details" id="sd2" value="0" <?php if($programme->is_education_details=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Family Details <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_family_details" id="sd1" value="1" <?php if($programme->is_family_details=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_family_details" id="sd2" value="0" <?php if($programme->is_family_details=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                    </div>



                </div>


                <div class="row">


                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Financial Recoverable Details <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_financial_recoverable_details" id="sd1" value="1" <?php if($programme->is_financial_recoverable_details=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_financial_recoverable_details" id="sd2" value="0" <?php if($programme->is_financial_recoverable_details=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>File Upload Details <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_file_upload_details" id="sd1" value="1" <?php if($programme->is_file_upload_details=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_file_upload_details" id="sd2" value="0" <?php if($programme->is_file_upload_details=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <p>Employment Details <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="is_employment_details" id="sd1" value="1" <?php if($programme->is_employment_details=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="is_employment_details" id="sd2" value="0" <?php if($programme->is_employment_details=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                            </label>                              
                        </div>                         
                    </div>



                </div>
        </div>



        <div class="button-block clearfix">
            <div class="bttn-group pull-right">
             <a href="../programList" class="btn btn-link">Back</a>
            <button type="submit" class="btn btn-primary btn-lg" >Save</button>
            </div>
        </div>

        <br>
    
    
  
    
        <div class="m-auto text-center">
           <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
         </div>
        <div class="clearfix">
        <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
            <li role="presentation" class="active"><a href="#education" class="nav-link border rounded text-center"
                    aria-controls="education" aria-selected="true"
                    role="tab" data-toggle="tab">Personal Details</a>
            </li>
            <li role="presentation"><a href="#proficiency" class="nav-link border rounded text-center"
                    aria-controls="proficiency" role="tab" data-toggle="tab">Education Details</a>
            </li>
            <li role="presentation"><a href="#employment" class="nav-link border rounded text-center"
                    aria-controls="employment" role="tab" data-toggle="tab">Family Information</a>
            </li>
            <li role="presentation"><a href="#profile" class="nav-link border rounded text-center"
                    aria-controls="profile" role="tab" data-toggle="tab">Financial Recoverable</a>
            </li>
            <li role="presentation"><a href="#visa" class="nav-link border rounded text-center"
                    aria-controls="visa" role="tab" data-toggle="tab">File Upload</a>
            </li>
            <li role="presentation"><a href="#other" class="nav-link border rounded text-center"
                    aria-controls="other" role="tab" data-toggle="tab">Employment Details</a>
            </li>
        </ul>



        <div class="tab-content offers-tab-content">


            <div role="tabpanel" class="tab-pane active" id="education">
                <div class="col-12 mt-4">
                    <br>

                 <div class="form-container">
                    <h4 class="form-group-title">Profile Details</h4>



                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Salutation <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_salutation" id="sd1" value="1" <?php if($isPersonel->is_salutation=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_salutation" id="sd2" value="0" <?php if($isPersonel->is_salutation=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>  
                                    <input type="hidden" class="form-control" id="id_is_personel_details" name="id_is_personel_details" value="<?php echo $isPersonel->id; ?>">                            
                                </div>                         
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>First Name <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_first_name" id="sd1" value="1" <?php if($isPersonel->is_first_name=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_first_name" id="sd2" value="0" <?php if($isPersonel->is_first_name=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>           

                                </div>                         
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Last Name <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_last_name" id="sd1" value="1" <?php if($isPersonel->is_last_name=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_last_name" id="sd2" value="0" <?php if($isPersonel->is_last_name=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>                            

                        </div>


                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>NRIC <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_nric" id="sd1" value="1" <?php if($isPersonel->is_nric=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_nric" id="sd2" value="0" <?php if($isPersonel->is_nric=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Phone <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_phone" id="sd1" value="1" <?php if($isPersonel->is_phone=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_phone" id="sd2" value="0" <?php if($isPersonel->is_phone=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Email <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_email_id" id="sd1" value="1" <?php if($isPersonel->is_email_id=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_email_id" id="sd2" value="0" <?php if($isPersonel->is_email_id=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>                            

                        </div>


                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Gender <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_gender" id="sd1" value="1" <?php if($isPersonel->is_gender=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_gender" id="sd2" value="0" <?php if($isPersonel->is_gender=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Gender <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_date_of_birth" id="sd1" value="1" <?php if($isPersonel->is_date_of_birth=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_date_of_birth" id="sd2" value="0" <?php if($isPersonel->is_date_of_birth=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Maritual Status <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_martial_status" id="sd1" value="1" <?php if($isPersonel->is_martial_status=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_martial_status" id="sd2" value="0" <?php if($isPersonel->is_martial_status=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>                            

                        </div>

                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Race <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_id_race" id="sd1" value="1" <?php if($isPersonel->is_id_race=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_id_race" id="sd2" value="0" <?php if($isPersonel->is_id_race=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Religion <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_religion" id="sd1" value="1" <?php if($isPersonel->is_religion=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_religion" id="sd2" value="0" <?php if($isPersonel->is_religion=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>       

                        </div>
                </div>

                <br>


                <div class="form-container">
                    <h4 class="form-group-title">Mailing Address</h4>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Mail Address 1 <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_mail_address1" id="sd1" value="1" <?php if($isPersonel->is_mail_address1=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_mail_address1" id="sd2" value="0" <?php if($isPersonel->is_mail_address1=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Address 2 <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_mail_address2" id="sd1" value="1" <?php if($isPersonel->is_mail_address2=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_mail_address2" id="sd2" value="0" <?php if($isPersonel->is_mail_address2=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>
                  

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Country <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_mailing_country" id="sd1" value="1" <?php if($isPersonel->is_mailing_country=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_mailing_country" id="sd2" value="0" <?php if($isPersonel->is_mailing_country=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>

                    </div>



                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>State <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_mailing_state" id="sd1" value="1" <?php if($isPersonel->is_mailing_state=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_mailing_state" id="sd2" value="0" <?php if($isPersonel->is_mailing_state=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>City <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_mailing_city" id="sd1" value="1" <?php if($isPersonel->is_mailing_city=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_mailing_city" id="sd2" value="0" <?php if($isPersonel->is_mailing_city=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>                 



                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Zipcode <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_mailing_zipcode" id="sd1" value="1" <?php if($isPersonel->is_mailing_zipcode=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_mailing_zipcode" id="sd2" value="0" <?php if($isPersonel->is_mailing_zipcode=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>                 


                    </div>

                </div>

                
                <br>

                <div class="form-container">
                    <h4 class="form-group-title">Permanent Address</h4>
                    
                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Mail Address 1 <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_permanent_address1" id="sd1" value="1" <?php if($isPersonel->is_permanent_address1=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_permanent_address1" id="sd2" value="0" <?php if($isPersonel->is_permanent_address1=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Address 2 <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_permanent_address2" id="sd1" value="1" <?php if($isPersonel->is_permanent_address2=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_permanent_address2" id="sd2" value="0" <?php if($isPersonel->is_permanent_address2=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>
                  

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Country <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_permanent_country" id="sd1" value="1" <?php if($isPersonel->is_permanent_country=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_permanent_country" id="sd2" value="0" <?php if($isPersonel->is_permanent_country=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>

                    </div>



                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>State <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_permanent_state" id="sd1" value="1" <?php if($isPersonel->is_permanent_state=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_permanent_state" id="sd2" value="0" <?php if($isPersonel->is_permanent_state=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>City <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_permanent_city" id="sd1" value="1" <?php if($isPersonel->is_permanent_city=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_permanent_city" id="sd2" value="0" <?php if($isPersonel->is_permanent_city=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>                 



                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Zipcode <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_permanent_zipcode" id="sd1" value="1" <?php if($isPersonel->is_permanent_zipcode=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_permanent_zipcode" id="sd2" value="0" <?php if($isPersonel->is_permanent_zipcode=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>                 


                    </div>

                </div>



                <div class="button-block clearfix">
                    <div class="bttn-group pull-right">
                    <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                    </div>
                </div>
                 
                 </div> <!-- END col-12 -->  
            </div>






            <div role="tabpanel" class="tab-pane" id="proficiency">
                <div class="col-12 mt-4">

                <div class="form-container">
                <h4 class="form-group-title">Education Qualification Requirements</h4>

                    <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Qualification Level <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_qualification_level" id="sd1" value="1" <?php if($isQualification->is_qualification_level=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_qualification_level" id="sd2" value="0" <?php if($isQualification->is_qualification_level=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>         
                                <input type="hidden" class="form-control" id="id_is_qualification_details" name="id_is_qualification_details" value="<?php echo $isQualification->id; ?>">                     
                            </div>                         
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Degree Awarded <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_degree_awarded" id="sd1" value="1" <?php if($isQualification->is_degree_awarded=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_degree_awarded" id="sd2" value="0" <?php if($isQualification->is_degree_awarded=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Specialization <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_specialization" id="sd1" value="1" <?php if($isQualification->is_specialization=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_specialization" id="sd2" value="0" <?php if($isQualification->is_specialization=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>

                            

                    </div>



                    <div class="row">

                         <div class="col-sm-4">
                            <div class="form-group">
                                <p>Classs Degree <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_class_degree" id="sd1" value="1" <?php if($isQualification->is_class_degree=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_class_degree" id="sd2" value="0" <?php if($isQualification->is_class_degree=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>                       

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Result <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_result" id="sd1" value="1" <?php if($isQualification->is_result=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_result" id="sd2" value="0" <?php if($isQualification->is_result=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>



                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Year Of raduation <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_year" id="sd1" value="1" <?php if($isQualification->is_year=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_year" id="sd2" value="0" <?php if($isQualification->is_year=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>

                    </div>



                    <div class="row">

                        

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Medium Of Institution <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_medium" id="sd1" value="1" <?php if($isQualification->is_medium=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_medium" id="sd2" value="0" <?php if($isQualification->is_medium=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>           


                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Country of University/College <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_college_country" id="sd1" value="1" <?php if($isQualification->is_college_country=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_college_country" id="sd2" value="0" <?php if($isQualification->is_college_country=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>           


                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Name of University/College <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_college_name" id="sd1" value="1" <?php if($isQualification->is_college_name=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_college_name" id="sd2" value="0" <?php if($isQualification->is_college_name=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>                            

                    </div>



                    <div class="row">

                        

                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Certificate Upload <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_certificate" id="sd1" value="1" <?php if($isQualification->is_certificate=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_certificate" id="sd2" value="0" <?php if($isQualification->is_certificate=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>           


                        <div class="col-sm-4">
                            <div class="form-group">
                                <p>Transcript Upload <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                  <input type="radio" name="is_transcript" id="sd1" value="1" <?php if($isQualification->is_transcript=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="is_transcript" id="sd2" value="0" <?php if($isQualification->is_transcript=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                </label>                              
                            </div>                         
                        </div>           

                    </div>

                </div>



                <div class="button-block clearfix">
                    <div class="bttn-group pull-right">
                    <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                    </div>
                </div>



                
                             
                </div> <!-- END col-12 -->  
            </div>






            <div role="tabpanel" class="tab-pane" id="employment">
                <div class="col-12 mt-4">
                    <br>

                    <div class="form-container">
                    <h4 class="form-group-title">Family Details <span class='error-text'>*</span></h4>

                        
                        <div class="row">

                        

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Father Name <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_father_name" id="sd1" value="1" <?php if($isFamily->is_father_name=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_father_name" id="sd2" value="0" <?php if($isFamily->is_father_name=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>            
                                    <input type="hidden" class="form-control" id="id_is_family_details" name="id_is_family_details" value="<?php echo $isFamily->id; ?>">                  
                                </div>                         
                            </div>           


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>MOther Name <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_mother_name" id="sd1" value="1" <?php if($isFamily->is_mother_name=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_mother_name" id="sd2" value="0" <?php if($isFamily->is_mother_name=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>           


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Father Deceased <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_father_deceased" id="sd1" value="1" <?php if($isFamily->is_father_deceased=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_father_deceased" id="sd2" value="0" <?php if($isFamily->is_father_deceased=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>                            

                        </div>





                        <div class="row">

                        

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Father Occupation <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_father_occupation" id="sd1" value="1" <?php if($isFamily->is_father_occupation=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_father_occupation" id="sd2" value="0" <?php if($isFamily->is_father_occupation=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>           


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>No. Of Sibllings <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_no_siblings" id="sd1" value="1" <?php if($isFamily->is_no_siblings=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_no_siblings" id="sd2" value="0" <?php if($isFamily->is_no_siblings=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>           


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Estimated Fee Amount <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_est_fee" id="sd1" value="1" <?php if($isFamily->is_est_fee=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_est_fee" id="sd2" value="0" <?php if($isFamily->is_est_fee=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>                            

                        </div>

                        <div class="row">

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Family Annual Income <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_family_annual_income" id="sd1" value="1" <?php if($isFamily->is_family_annual_income=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_family_annual_income" id="sd2" value="0" <?php if($isFamily->is_family_annual_income=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div> 

                        </div>

                    </div>



                    <div class="button-block clearfix">
                        <div class="bttn-group pull-right">
                        <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                        </div>
                    </div>
                                        
                         
                </div> <!-- END col-12 -->  
            </div>





        
            <div role="tabpanel" class="tab-pane" id="profile">
                <div class="col-12 mt-4">
                    <br>



                

                    <div class="button-block clearfix">
                        <div class="bttn-group pull-right">
                        <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                        </div>
                    </div>



                 
                </div> <!-- END col-12 -->  
            </div>







            <div role="tabpanel" class="tab-pane" id="visa">
                <div class="col-12 mt-4">
                    <br>

                <div class="form-container">
                    <h4 class="form-group-title">Upload File Details</h4>

                    <!-- <div class="row">

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Upload Employment Letter <span class='error-text'>*</span></label>
                                <input type="file" class="form-control" id="employment_letter" name="employment_letter"  >
                            </div>
                        </div>

                     </div>          -->         

                </div>


                <div class="button-block clearfix">
                    <div class="bttn-group pull-right">
                    <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                    </div>
                </div>

                 
                </div> <!-- END col-12 -->  
            </div>





            <div role="tabpanel" class="tab-pane" id="other">

                <div class="col-12 mt-4">
                    <br>
                    

                    <div class="form-container">
                        <h4 class="form-group-title">Employment Details</h4>  


                        <div class="row">

                        

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Company Name <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_company_name" id="sd1" value="1" <?php if($isEmployment->is_company_name=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_company_name" id="sd2" value="0" <?php if($isEmployment->is_company_name=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                    
                                    <input type="hidden" class="form-control" id="id_is_employment_details" name="id_is_employment_details" value="<?php echo $isEmployment->id; ?>">          
                                </div>
                            </div>          



                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Company Address <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_company_address" id="sd1" value="1" <?php if($isEmployment->is_company_address=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_company_address" id="sd2" value="0" <?php if($isEmployment->is_company_address=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>          



                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Telephone <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_telephone" id="sd1" value="1" <?php if($isEmployment->is_telephone=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_telephone" id="sd2" value="0" <?php if($isEmployment->is_telephone=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>          

                        </div>




                        <div class="row">


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Fax Number <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_fax_num" id="sd1" value="1" <?php if($isEmployment->is_fax_num=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_fax_num" id="sd2" value="0" <?php if($isEmployment->is_fax_num=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>          



                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Designation <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_designation" id="sd1" value="1" <?php if($isEmployment->is_designation=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_designation" id="sd2" value="0" <?php if($isEmployment->is_designation=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>          



                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Position <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_position" id="sd1" value="1" <?php if($isEmployment->is_position=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_position" id="sd2" value="0" <?php if($isEmployment->is_position=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>          

                        </div>




                        <div class="row">


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Year Of Service<span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_service_year" id="sd1" value="1" <?php if($isEmployment->is_service_year=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_service_year" id="sd2" value="0" <?php if($isEmployment->is_service_year=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>          



                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Industry <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_industry" id="sd1" value="1" <?php if($isEmployment->is_industry=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_industry" id="sd2" value="0" <?php if($isEmployment->is_industry=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>          



                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Job Description <span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_job_desc" id="sd1" value="1" <?php if($isEmployment->is_job_desc=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_job_desc" id="sd2" value="0" <?php if($isEmployment->is_job_desc=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>          

                        </div>


                        <div class="row">


                            <div class="col-sm-4">
                                <div class="form-group">
                                    <p>Upload Employment Letter<span class='error-text'>*</span></p>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_employment_letter" id="sd1" value="1" <?php if($isEmployment->is_employment_letter=='1'){ echo "checked";}?> ><span class="check-radio"></span> Yes
                                    </label>
                                    <label class="radio-inline">
                                      <input type="radio" name="is_employment_letter" id="sd2" value="0" <?php if($isEmployment->is_employment_letter=='0'){ echo "checked";}?> ><span class="check-radio"></span> No
                                    </label>                              
                                </div>                         
                            </div>          


                        </div>

                    </div>



                    <div class="button-block clearfix">
                        <div class="bttn-group pull-right">
                        <button type="submit" class="btn btn-primary btn-lg" >Save</button>
                        </div>
                    </div>


                 
                </div> <!-- END col-12 -->  
            </div>




          </div>
        </div>

       </div> <!-- END row-->



    </form>


   
    
    <footer class="footer-wrapper">
        <p>&copy; 2019 All rights, reserved</p>
    </footer>

    </div>
</div>
<script type="text/javascript">

    $('select').select2();

    $(document).ready(function() {
        $("#form_profile").validate({
            rules: {
                status: {
                    required: true
                },
                 reason: {
                    required: true
                }
            },
            messages: {
                status: {
                    required: "<p class='error-text'>Select Status",
                },
                reason: {
                    required: "<p class='error-text'>Reason Required",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function showRejectField(){
            $("#view_reject").show();
    }

    function hideRejectField(){
            $("#view_reject").hide();
    }

  </script>
<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Budget Thrust Allocation</h3>
        </div>
        <form id="form_academic_year" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Budget Thrust Allocation</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Academic Year <span class='error-text'>*</span></label>
                        <select name="id_academic_year" id="id_academic_year" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($academicYearList))
                            {
                                foreach ($academicYearList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Trust <span class='error-text'>*</span></label>
                         <select name="id_thrust" id="id_thrust" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($thrustList))
                            {
                                foreach ($thrustList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"
                                    ><?php echo $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Amount <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="amount" name="amount" autocomplete="off">
                    </div>
                </div>

            </div>


            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div>

            </div>

        </div>

        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="list" class="btn btn-link">Cancel</a>
            </div>
        </div>

        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
    $('select').select2();

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });


    $(document).ready(function() {
        $("#form_academic_year").validate({
            rules: {
                name: {
                    required: true
                },
                start_date:
                {
                    required: true
                },
                end_date:
                {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Academic Year Required</p>",
                },
                start_date:
                {
                    required: "<p class='error-text'>Start Date Required</p>",
                },
                end_date:
                {
                    required: "<p class='error-text'>End Date Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>

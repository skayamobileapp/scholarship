<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Cohort</h3>
        </div>
        <form id="form_grade" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Cohort Details</h4>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $scheme->name; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $scheme->code; ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="from_dt" name="from_dt" value="<?php echo date('d-m-Y',strtotime($scheme->from_dt)) ?>" autocomplete="off">
                    </div>
                </div>

                <!-- <div class="col-sm-4">
                    <div class="form-group">
                        <label>Max. Scholrship Percentage <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="percentage" name="percentage" min="1" max="100" value="<?php echo $scheme->percentage; ?>">
                    </div>
                </div> -->

            </div>


            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="to_dt" name="to_dt" value="<?php echo date('d-m-Y',strtotime($scheme->to_dt)) ?>" autocomplete="off">
                    </div>
                </div>

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($scheme->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($scheme->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>               
                
            </div>

        </div>


        </form>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="button" class="btn btn-primary btn-lg" onclick="validateDetailsData()">Save</button>
                <a href="../list" class="btn btn-link">Back</a>
            </div>
        </div>



        <form id="form_programme_intake" action="" method="post">


            <div class="form-container">
                <h4 class="form-group-title">Scholarship Offered</h4>


                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Select Scholarship <span class='error-text'>*</span></label>
                            <select name="id_sub_thrust" id="id_sub_thrust" class="form-control" onchange="getProgramBySubThrustId(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($subThrustList))
                                {
                                    foreach ($subThrustList as $record)
                                    {?>
                                        <option value="<?php echo $record->id;?>"
                                        ><?php echo $record->scholarship_code . " - " . $record->scholarship_name; ?>
                                        </option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Program <span class='error-text'>*</span></label>
                            <span id='view_program'></span>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Quota <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="quota" name="quota">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Budget <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="budget" name="budget">
                        </div>
                    </div>


                  
                    <div class="col-sm-2">
                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveData()">Add</button>
                    </div>
                </div>
            </div>

            </form>

           


        <!-- <div class="form-container">
            <h4 class="form-group-title">Program Tagging Company List</h4>


                 <div class="row">
                    <div id="view"></div>
                </div>

            </div> -->

            <div class="form-container">
            <h4 class="form-group-title">Scheme Has Program Details</h4>

                <div class="custom-table">
                  <table class="table" id="list-table">
                    <thead>
                      <tr>
                        <th>Sl. No</th>
                        <th>Scholarship</th>
                        <th>Program</th>
                        <th>Quota</th>
                        <th>Budget</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if (!empty($schemeHasProgramList))
                      {
                        $i=1;
                        foreach ($schemeHasProgramList as $record)
                        {
                      ?>
                          <tr>
                            <td><?php echo $i ?></td>
                            <td><?php echo $record->scholarship_code . " - " . $record->scholarship_name ?></td>
                            <td><?php echo $record->program_code . " - " . $record->program_name ?></td>
                            <td><?php echo $record->quota ?></td>
                            <td><?php echo $record->budget ?></td>
                            <td class="text-center">
                                <a onclick="deleteSchemeHasProgram(<?php echo $record->id; ?>)" title="Edit">DELETE</a>
                            </td>

                            <!-- <td class="text-center"><?php echo anchor('admission/student/delete_exam?id='.$record->id, 'DELETE', 'id="$record->id"'); ?></td> -->
                          </tr>
                      <?php
                      $i++;
                        }
                      }
                      ?>
                    </tbody>
                  </table>
                </div>

            </div>


        



            
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    function getProgramBySubThrustId(id)
    {
        // $("#view_program").html(id);
        // alert(id);
        $.get("/scholarship/scheme/getProgramBySubThrustId/"+id,

            function(data, status)
            {
                $("#view_program").html(data);
                // $("#view_programme_details").html(data);
                // $("#view_programme_details").show();
        });
    }


     function saveData()
    {
        if($('#form_programme_intake').valid())
        {
        var tempPR = {};
        tempPR['id_program'] = $("#id_program").val();
        tempPR['id_sub_thrust'] = $("#id_sub_thrust").val();
        tempPR['quota'] = $("#quota").val();
        tempPR['budget'] = $("#budget").val();
        tempPR['id_scholarship_scheme'] =  <?php echo $idScheme;?>;
            $.ajax(
            {
               url: '/scholarship/scheme/directAdd',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // $("#view").html(result);
                window.location.reload();
               }
            });
        }
    }


    function deleteSchemeHasProgram(id)
    {
            // alert(id);
            $.ajax(
            {
               url: '/scholarship/scheme/deleteSchemeHasProgram/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
               }
            });
    }

    function validateDetailsData()
    {
        if($('#form_grade').valid())
        {
            console.log($("#view").html());
            var addedProgam = $("#view").html();
            if(addedProgam=='')
            {
                alert("Add Scholarship Offered Cohort");
            }
            else
            {
                $('#form_grade').submit();
            }
        }    
    }


    $('select').select2();

    $(document).ready(function() {
        // saveData();
        $("#form_programme_intake").validate({
            rules: {
                id_sub_thrust: {
                    required: true
                },
                id_program: {
                    required: true
                },
                quota: {
                    required: true
                }
            },
            messages: {
                id_sub_thrust: {
                    required: "<p class='error-text'>Select Scholarship</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                },
                quota: {
                    required: "<p class='error-text'>Quota Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );

    
     $(document).ready(function() {
        // saveData();
        $("#form_grade").validate({
            rules: {
                name: {
                    required: true
                },
                 code: {
                    required: true
                },
                 percentage: {
                    required: true
                },
                 from_dt: {
                    required: true
                },
                 to_dt: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                percentage: {
                    required: "<p class='error-text'>Percentage Required</p>",
                },
                from_dt: {
                    required: "<p class='error-text'>Select From Date</p>",
                },
                to_dt: {
                    required: "<p class='error-text'>Select To Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
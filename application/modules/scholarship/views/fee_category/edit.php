<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Fee Category</h3>
        </div>
        <form id="form_fee_category" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Fee Category Details</h4>  
                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Fee Category Code <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="code" name="code" value="<?php echo $feeCategory->code;?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $feeCategory->name;?>">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Name In Malay <span class='error-text'>*</span></label>
                            <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $feeCategory->name_optional_language;?>">
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Select Fee Group <span class='error-text'>*</span></label>
                            <select name="fee_group" id="fee_group" class="form-control">
                                <option value="">Select</option>
                                <option value="<?php echo "Rental";  ?>"
                                    <?php 
                                    if ($feeCategory->fee_group == 'Rental')
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                    <?php echo "Rental";  ?>
                                </option>

                                <option value="<?php echo "Statement Of Account";?>"
                                    <?php 
                                    if ($feeCategory->fee_group == 'Statement Of Account')
                                    {
                                        echo "selected=selected";
                                    } ?>>
                                    <?php echo "Statement Of Account";  ?>
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Sequence <span class='error-text'>*</span></label>
                            <input type="number" class="form-control" id="sequence" name="sequence" value="<?php echo $feeCategory->sequence;?>">
                        </div>
                    </div>



                    <div class="col-sm-4">
                            <div class="form-group">
                                <p>Status <span class='error-text'>*</span></p>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="1" <?php if($feeCategory->status=='1') {
                                    echo "checked=checked";
                                };?>><span class="check-radio"></span> Active
                                </label>
                                <label class="radio-inline">
                                <input type="radio" name="status" id="status" value="0" <?php if($feeCategory->status=='0') {
                                    echo "checked=checked";
                                };?>>
                                <span class="check-radio"></span> In-Active
                                </label>                              
                            </div>                         
                    </div>
                </div>
            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>
     $(document).ready(function() {
        $("#form_fee_category").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                },
                name_optional_language: {
                    required: true
                },
                fee_group: {
                    required: true
                },
                sequence: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Category Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                name_optional_language: {
                    required: "<p class='error-text'>Name In Malay Required</p>",
                },
                fee_group: {
                    required: "<p class='error-text'>Select Fee Group</p>",
                },
                sequence: {
                    required: "<p class='error-text'>Enter Sequence</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $('select').select2();

</script>
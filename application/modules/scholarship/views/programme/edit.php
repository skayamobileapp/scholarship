<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>Edit Program</h3>
            </div>





    <div class="form-container">
            <h4 class="form-group-title"> Program Setup Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Program Details</a>
                    </li>
                    <li role="presentation"><a href="#tab_department" class="nav-link border rounded text-center"
                            aria-controls="tab_department" role="tab" data-toggle="tab">Certification Details</a>
                    </li>
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">



                        <form id="form_programme" action="" method="post">
        
                            <div class="form-container">
                                <h4 class="form-group-title">Program Details</h4>
                                    

                                    <div class="row">

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Code <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control" id="code" name="code" value="<?php echo $programmeDetails->code; ?>">
                                            </div>
                                        </div>



                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Name <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control" id="name" name="name" value="<?php echo $programmeDetails->name; ?>">
                                            </div>
                                        </div>

                                         <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Name Other Language</label>
                                                <input type="text" class="form-control" id="name_optional_language" name="name_optional_language" value="<?php echo $programmeDetails->name_optional_language; ?>">
                                            </div>
                                        </div>

                                        
                                    </div>

                                    <div class="row">


                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Select Award <span class='error-text'>*</span></label>
                                                <select name="id_award" id="id_award" class="form-control">
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($awardList))
                                                    {
                                                        foreach ($awardList as $record)
                                                        {?>
                                                            <option value="<?php echo $record->id;  ?>"
                                                                <?php 
                                                                if($record->id == $programmeDetails->id_award)
                                                                {
                                                                    echo "selected=selected";
                                                                } ?>>
                                                                <?php echo $record->code . " - " . $record->name;  ?>
                                                            </option>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                         <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Program Manager <span class='error-text'>*</span></label>
                                            <select name="id_user" id="id_user" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($userList))
                                                {
                                                    foreach ($userList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;?>"

                                                            <?php 
                                                                if($record->id == $programmeDetails->id_user)
                                                                {
                                                                    echo "selected=selected";
                                                                } ?>


                                                        ><?php echo $record->name;?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Start Date <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control datepicker" id="start_date" name="start_date" value="<?php echo date('d-m-Y', strtotime($programmeDetails->start_date)); ?>" autocomplete="off">
                                            </div>
                                        </div>



                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>End Date <span class='error-text'>*</span></label>
                                                <input type="text" class="form-control datepicker" id="end_date" name="end_date" value="<?php echo date('d-m-Y', strtotime($programmeDetails->end_date)); ?>" autocomplete="off">
                                            </div>
                                        </div>


                                    </div>


                                    <div class="row">



                                        <div class="col-sm-4">
                                                <div class="form-group">
                                                    <p>Status <span class='error-text'>*</span></p>
                                                    <label class="radio-inline">
                                                      <input type="radio" name="status" id="status" value="1" <?php if($programmeDetails->status=='1') {
                                                         echo "checked=checked";
                                                      };?>><span class="check-radio"></span> Active
                                                    </label>
                                                    <label class="radio-inline">
                                                      <input type="radio" name="status" id="status" value="0" <?php if($programmeDetails->status=='0') {
                                                         echo "checked=checked";
                                                      };?>>
                                                      <span class="check-radio"></span> In-Active
                                                    </label>                              
                                                </div>                         
                                        </div>

                                    </div>

                                </div>
                            


                                <div class="button-block clearfix">
                                    <div class="bttn-group">
                                        <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                        <a href="../list" class="btn btn-link">Back</a>
                                    </div>
                                </div>


                        </form>


                        
                        


                        </div> 
                    </div>










                    <div role="tabpanel" class="tab-pane" id="tab_department">
                        <div class="col-12 mt-4">




                        <form id="form_department" action="" method="post">


                                <br>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Certification Title <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="certification_name" name="certification_name">
                                        </div>
                                    </div>


                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="saveCertificationData()">Add</button>
                                    </div>
                                
                                </div>


                        </form>


                        <?php

                            if(!empty($programCertificationList))
                            {
                                ?>

                                <div class="form-container">
                                        <h4 class="form-group-title">Certification Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Department</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($programCertificationList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $programCertificationList[$i]->name;?></td>
                                                <td>
                                                <a onclick="deleteCertificationData(<?php echo $programCertificationList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div> 
                    </div>



                </div>

            </div>
        </div>


    

        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>

<script>



    function saveCertificationData()
    {
        if($('#form_department').valid())
        {

        var tempPR = {};
        tempPR['name'] = $("#certification_name").val();
        tempPR['id_program'] = <?php echo $programmeDetails->id;?>;
            $.ajax(
            {
               url: '/scholarship/programme/saveCertificationData',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    location.reload();
               }
            });
        }
    }

    function deleteCertificationData(id)
    {
        $.ajax(
            {
               url: '/scholarship/programme/deleteCertificationData/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    location.reload();
               }
            });
    }


    $(document).ready(function() {
        $("#form_department").validate({
            rules: {
                certification_name: {
                    required: true
                }
            },
            messages: {
                certification_name: {
                    required: "<p class='error-text'>Certification Title Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    


    $(document).ready(function() {
        $("#form_programme").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                graduate_studies: {
                    required: true
                },
                foundation: {
                    required: true
                },
                total_cr_hrs: {
                    required: true
                },
                id_award: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Program Name Required</p>",
                },
                code: {
                    required: "<p class='error-text'>Program Code Required</p>",
                },
                graduate_studies: {
                    required: "<p class='error-text'>Graduate Studies Required</p>",
                },
                foundation: {
                    required: "<p class='error-text'>Foundation Required</p>",
                },
                total_cr_hrs: {
                    required: "<p class='error-text'>Total Credit Hours required</p>",
                },
                id_award: {
                    required: "<p class='error-text'>Select Award</p>",
                },
                status: {
                    required: "<p class='error-text'>Select status</p>",
                },
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

</script>
<script type="text/javascript">
    $('select').select2();
</script>

<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>
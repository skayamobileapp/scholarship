            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $scholar_name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <!-- <li><a href="/sponser/user/profile">Edit Profile</a></li> -->
                            <li><a href="/scholarship/role/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                    
                    <h4>Scholarship Setup</h4>
                    <ul>
                        <li><a href="/scholarship/academicYear/list">Academic Year</a></li>


                        <li><a href="/scholarship/educationLevel/list">Education Level</a></li>
                        <li><a href="/scholarship/selectionType/list">Selection Type</a></li>
                        <li><a href="/scholarship/thrust/list">Scholarship Thrust</a></li>
                        <li><a href="/scholarship/subThrust/list">Scholarship Sub-Thrust</a></li>


                        <li><a href="/scholarship/workSpecialisation/list">Work Specialisation</a></li>

                    </ul>

                     <h4>Budget Allocation</h4>
                    <ul>
                        <li><a href="/scholarship/budgetAllocation/list">Scholarship Thrust Budget Allocation</a></li>
                        <li><a href="/scholarship/budgetAllocationDetails/list">Distribute Budget</a></li>

                    </ul>

                   
                    <h4>Cohort</h4>
                    <ul>
                        <li><a href="/scholarship/scheme/list">Cohort</a></li>
                        <li><a href="/scholarship/cohortActivity/list">Calendar of Activities</a></li>

                    </ul>
                    <h4>Selection</h4>
                    <ul>
                        <li><a href="/scholarship/scholarshipApplication/approvalList">Application Approval</a></li>
                        <li><a href="/scholarship/scholarshipApplication/list">Application Summary</a></li>
                    </ul>
                    

                </div>
            </div>
<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Program Partner</h3>
        </div>
        <form id="form_bank" action="" method="post">
            <div class="form-container">
                <h4 class="form-group-title">Program Partner Details</h4> 

                <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Accreditation Category <span class='error-text'>*</span></label>
                        <select name="id_accreditation_category" id="id_accreditation_category" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($accreditationCategoryList))
                            {
                                foreach ($accreditationCategoryList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php
                                        if($record->id == $programAccreditation->id_accreditation_category)
                                            {
                                                echo "selected=selected";
                                            } ?>
                                            >
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Accreditation Type <span class='error-text'>*</span></label>
                        <select name="id_accreditation_type" id="id_accreditation_type" class="form-control" style="width: 408px" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($accreditationTypeList))
                            {
                                foreach ($accreditationTypeList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"
                                 <?php
                                if($record->id == $programAccreditation->id_accreditation_type)
                                {
                                    echo "selected=selected";
                                } ?>
                                >
                                <?php echo $record->code . " -  ". $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Program <span class='error-text'>*</span></label>
                        <select name="id_program" id="id_program" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programList))
                            {
                                foreach ($programList as $record)
                                {?>
                            <option value="<?php echo $record->id;  ?>"
                                <?php
                                if($record->id == $programAccreditation->id_program)
                                {
                                    echo "selected=selected";
                                } ?>
                                >
                                <?php echo $record->code . " -  ". $record->name;?>
                            </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


            </div>

            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Accreditation Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="date_time" name="date_time" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($programAccreditation->date_time)); ?>">
                    </div>
                </div>

            

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Accreditation Number <span class='error-text'>*</span></label>
                        <input type="number" class="form-control" id="accreditation_number" name="accreditation_number" value="<?php echo $programAccreditation->accreditation_number; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Valid From <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="valid_from" name="valid_from" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($programAccreditation->valid_from)); ?>">
                    </div>
                </div>


            </div>

            <div class="row">



                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Valid To <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="valid_to" name="valid_to" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($programAccreditation->valid_to)); ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Approval Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="approval_date" name="approval_date" autocomplete="off" value="<?php echo date('d-m-Y',strtotime($programAccreditation->approval_date)); ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Accreditation Reference <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="reference_number" name="reference_number" value="<?php echo $programAccreditation->reference_number; ?>">
                    </div>
                </div>

<!-- 
                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                            </label>                              
                        </div>                         
                </div> -->

            </div>


            </div>

            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
            
        </form>

    </div>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
</div>
<script>

    $(document).ready(function() {
        $("#form_bank").validate({
            rules: {
                id_accreditation_category: {
                    required: true
                },
                id_accreditation_type: {
                    required: true
                },
                date_time: {
                    required: true
                },
                accreditation_number: {
                    required: true
                },
                valid_from: {
                    required: true
                },
                valid_to: {
                    required: true
                },
                approval_date: {
                    required: true
                },
                reference_number: {
                    required: true
                },
                id_program: {
                    required: true
                }
            },
            messages: {
                id_accreditation_category: {
                    required: "<p class='error-text'>Select Accreditation Category</p>",
                },
                id_accreditation_type: {
                    required: "<p class='error-text'>Select Accreditation Type</p>",
                },
                date_time: {
                    required: "<p class='error-text'>Select Accreditation Date Time</p>",
                },
                accreditation_number: {
                    required: "<p class='error-text'>Select Accreditation Number</p>",
                },
                valid_from: {
                    required: "<p class='error-text'>Select Valid From</p>",
                },
                valid_to: {
                    required: "<p class='error-text'>Select Valid To</p>",
                },
                approval_date: {
                    required: "<p class='error-text'>Select Approval Date</p>",
                },
                reference_number: {
                    required: "<p class='error-text'>Reference Number Required</p>",
                },
                id_program: {
                    required: "<p class='error-text'>Select Program</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    $('select').select2();

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true
    });
  } );

</script>

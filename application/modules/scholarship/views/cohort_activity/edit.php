<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Add Cohort Activity</h3>
        </div>
        <form id="form_unit" action="" method="post">

        <div class="form-container">
            <h4 class="form-group-title">Cohort Activity Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Cohort <span class='error-text'>*</span></label>


                        <select class="form-control" id="id_cohort" name="id_cohort">

                            <option value="">Select</option>
                            <?php
                            if (!empty($cohortList))
                            {
                                foreach ($cohortList as $record)
                                { ?>

                                   


                                    <option value="<?php echo $record->id;?>"
                                    
                                      <?php 
                                            if($record->id == $cohortActivityDetails->id_cohort)
                                            {
                                                echo "selected=selected";
                                            } ?>

                                    ><?php echo $record->code . " - " . $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>


                        </select>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Scholarship <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_scholarship_sub_thrust" name="id_scholarship_sub_thrust">
                             <option value="">Select</option>
                            <?php
                            if (!empty($scholarshipList))
                            {
                                foreach ($scholarshipList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"

                                      <?php 
                                            if($record->id == $cohortActivityDetails->id_scholarship_sub_thrust)
                                            {
                                                echo "selected=selected";
                                            } ?>

                                    ><?php echo $record->scholarship_name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Programme <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_programme" name="id_programme">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"


                                      <?php 
                                            if($record->id == $cohortActivityDetails->id_programme)
                                            {
                                                echo "selected=selected";
                                            } ?>


                                    ><?php echo $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                    <div class="col-sm-4">
                    <div class="form-group">
                        <label>Activity Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="activity_name" name="activity_name" value="<?php echo $cohortActivityDetails->activity_name;?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Communication Template <span class='error-text'>*</span></label>
                        <select class="form-control" id="id_communication_template" name="id_communication_template">
                            <option value="">Select</option>
                            <?php
                            if (!empty($communicationList))
                            {
                                foreach ($communicationList as $record)
                                {?>
                                    <option value="<?php echo $record->id;?>"

                                      <?php 
                                            if($record->id == $cohortActivityDetails->id_communication_template)
                                            {
                                                echo "selected=selected";
                                            } ?>

                                    ><?php echo $record->name;?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>



                 <div class="col-sm-4">
                    <div class="form-group">
                        <label>Trigger <span class='error-text'>*</span></label>
                        <select class="form-control" id="variable" name="variable">
                            <option value="">Select</option>
                             <option value="Invoice" <?php if($cohortActivityDetails->variable=='Invoice'){ echo "selected=selected";} ?>>Invoice</option>
                             <option value="Receipt" <?php if($cohortActivityDetails->variable=='Receipt'){ echo "selected=selected";} ?>>Receipt</option>
                        </select>
                    </div>
                </div>


                    <div class="col-sm-4">
                    <div class="form-group">
                        <label>Start Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="start_date" name="start_date" autocomplete="off" value="<?php echo $cohortActivityDetails->start_date;?>">
                    </div>
                </div>


                    <div class="col-sm-4">
                    <div class="form-group">
                        <label>End Date <span class='error-text'>*</span></label>
                        <input type="text" class="form-control datepicker" id="end_date" name="end_date" autocomplete="off" value="<?php echo $cohortActivityDetails->end_date;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <p>Status <span class='error-text'>*</span></p>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="1" checked="checked"><span class="check-radio"></span> Active
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="status" id="status" value="0"><span class="check-radio"></span> Inactive
                        </label>                              
                    </div>                         
                </div>
            </div>

        </div>


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                <a href="../list" class="btn btn-link">Cancel</a>
            </div>
        </div>


        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>
      $('select').select2();

    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true
    });
  } );



</script>

<script>
    $(document).ready(function() {
        $("#form_unit").validate({
            rules: {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                code: {
                    required: "<p class='error-text'>Code required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>

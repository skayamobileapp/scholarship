<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Education Level</h3>
        </div>
        <form id="form_selectionType" action="" method="post">

         <div class="form-container">
            <h4 class="form-group-title">Education Level Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $selectionType->name; ?>">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Calculation Method <span class='error-text'>*</span></label>
                        <select name="calculation_method" id="calculation_method" class="form-control" style="width: 308px">
                            <option value="">Select</option>
                            <option value="Pass/Fail" <?php if($selectionType->calculation_method=='Pass/Fail'){ echo "selected"; } ?>>Pass/Fail</option>
                            <option value="Mark" <?php if($selectionType->calculation_method=='Mark'){ echo "selected"; } ?>>Mark</option>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($selectionType->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($selectionType->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
            </div>

        </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Cancel</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();
    
    $(document).ready(function() {
        $("#form_selectionType").validate({
            rules: {
                name: {
                    required: true
                },
                calculation_method: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                calculation_method: {
                    required: "<p class='error-text'>Select calculation Method</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>

<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Department</h3>
        </div>
        <form id="form_department" action="" method="post">

        <div class="form-container">
        <h4 class="form-group-title">Department Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Code <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="code" name="code" value="<?php echo $departmentDetails->code;?>">
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $departmentDetails->name;?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Description <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="description" name="description" value="<?php echo $departmentDetails->description;?>">
                    </div>
                </div>

                 <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($departmentDetails->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($departmentDetails->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>
                
            </div>

        </div>


            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>





        <div class="form-container">
            <h4 class="form-group-title"> Head Of Department Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Head Of Department Details</a>
                    </li>          
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <form id="form_comitee" action="" method="post">


                                <br>

                                <div class="row">

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Head Of Department <span class='error-text'>*</span></label>
                                            <select name="id_staff" id="id_staff" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($staffList))
                                                {
                                                    foreach ($staffList as $record)
                                                    {?>
                                                        <option value="<?php echo $record->id;  ?>">
                                                            <?php echo $record->ic_no . " - " . $record->name;  ?>
                                                        </option>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="forintake_has_programmem-group">
                                            <label>Effective Date <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control datepicker" id="effective_date" name="effective_date" autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-primary btn-lg form-row-btn" onclick="addNewStaffToDepartment()">Add</button>
                                    </div>
                                
                                </div>


                        </form>


                        <?php

                            if(!empty($departmentHasStaffList))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Head Of Department Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Staff</th>
                                                 <th>Email Id</th>
                                                 <th>Effective Date</th>
                                                 <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($departmentHasStaffList);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $departmentHasStaffList[$i]->ic_no . " - " . $departmentHasStaffList[$i]->staff_name;?></td>
                                                <td><?php echo $departmentHasStaffList[$i]->email;?></td>
                                                <td><?php echo date('d-m-Y', strtotime($departmentHasStaffList[$i]->effective_date));?></td>
                                                <td>
                                                <a onclick="deleteDepartmentHasStaff(<?php echo $departmentHasStaffList[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div> 
                    </div>


                </div>



            </div>
            </div>








        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>

<script>

    $('select').select2();


    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true
    });
  } );


    function addNewStaffToDepartment()
    {
        if($('#form_comitee').valid())
        {

        var tempPR = {};
        tempPR['id_staff'] = $("#id_staff").val();
        tempPR['effective_date'] = $("#effective_date").val();
        tempPR['id_department'] = <?php echo $departmentDetails->id; ?>;
            $.ajax(
            {
               url: '/scholarship/department/addNewStaffToDepartment',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                location.reload();
                // $("#view").html(result);
                // $('#myModal').modal('hide');
               }
            });
        }
    }



    function deleteDepartmentHasStaff(id)
    {
        $.ajax(
            {
               url: '/scholarship/department/deleteDepartmentHasStaff/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // $("#view").html(result);
                    // alert(id);
                    location.reload();
               }
            });
    }


    $(document).ready(function(){
        $("#form_comitee").validate(
        {
            rules:
            {
                id_staff:
                {
                    required: true
                },
                effective_date:
                {
                    required: true
                }              
            },
            messages:
            {
                id_staff:
                {
                    required: "<p class='error-text'>Select Staff</p>",
                },
                effective_date:
                {
                    required: "<p class='error-text'>Effective Date Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




     $(document).ready(function(){
        $("#form_department").validate(
        {
            rules:
            {
                name:
                {
                    required: true
                },
                code:
                {
                    required: true
                },
                description:
                {
                    required: true
                }                
            },
            messages:
            {
                name:
                {
                    required: "<p class='error-text'>Name Required</p>",
                },
                code:
                {
                    required: "<p class='error-text'>Code Required</p>",
                },
                description:
                {
                    required: "<p class='error-text'>Description Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


</script>
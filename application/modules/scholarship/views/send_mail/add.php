<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Send Communication Mail</h3>
        </div>
        <form id="form_sponser_has_students" action="" method="post">
<!-- 
        <div class="page-title clearfix">
            <h4>Sponser Details</h4>
        </div> -->


        <div class="form-container">
            <h4 class="form-group-title">Mail Details</h4>

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                      <label>Type </label>
                        <select name="type" id="type" class="form-control" onchange="showType(this.value)">
                            <option value="">Select</option>
                            <option value="Applicant">Applicant</option>
                            <option value="Student">Student</option>
                            <option value="Staff">Staff</option>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                      <label>Template </label>
                        <select name="id_template" id="id_template" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($templateList))
                            {
                                foreach ($templateList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                
            </div>
        </div>


        <br>



        <div class="form-container" id="view_recepient_display" style="display: none;">
        <!-- <div class="form-container" id="view_type_display" style="display: none;"> -->
            <h4 class="form-group-title">Recepients Search</h4>


            <div class="row" id="view_student_display" style="display: none;">

                <div class="col-sm-3">
                    <div class="form-group">
                      <label>Program </label>
                        <select name="id_programme" id="id_programme" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($programmeList))
                            {
                                foreach ($programmeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code .". ".$record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                      <label>Intake </label>
                        <select name="id_intake" id="id_intake" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->year .". ".$record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Name / NRIC </label>
                        <input type="text" class="form-control" id="student" name="student">
                    </div>
                </div>

            </div>            



            <div class="row" id="view_staff_display" style="display: none;">

                <!-- <div class="col-sm-4">
                    <div class="form-group">
                      <label>Intake </label>
                        <select name="id_intake" id="id_intake" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($intakeList))
                            {
                                foreach ($intakeList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->year .". ".$record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div> -->

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Staff Name</label>
                        <input type="text" class="form-control" id="staff_name" name="staff_name">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Staff IC No.</label>
                        <input type="text" class="form-control" id="staff_ic_no" name="staff_ic_no">
                    </div>
                </div>

            </div>            

        </div>

        <div id="view_search_btn_visible" style="display: none;">
            <table border="0px" style="width: 100%">
                <tr>
                    <td style="text-align: right;" colspan="6">
                        <button type="button" id="btn_add_detail" onclick="showstudentlist()" class="btn btn-primary btn-light btn-lg">Search</button>
                    </td>
                </tr>

            </table>
            <br>
        </div>



        <div class="form-container" id="view_visible" style="display: none;">
            <h4 class="form-group-title">Recepients Details</h4>

            <div id="view">
            </div>

        </div>




     


        <div class="button-block clearfix">
            <div class="bttn-group">
                <button type="submit" class="btn btn-primary btn-lg">Send Mail</button>
                <!-- <a href="../list" class="btn btn-link">Cancel</a> -->
            </div>
        </div>


        </form>

        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    // $('select').select2();

    function showType(type)
    {
        // alert(type);
        if(type == 'Applicant')
        {

            $("#view_recepient_display").show();
            $("#view_student_display").show();
            $("#view_search_btn_visible").show();
        }
        else if(type == 'Student')
        {
            $("#view_recepient_display").show();
            $("#view_student_display").show();
            $("#view_search_btn_visible").show();
        }
        else if(type == 'Staff')
        {
            $("#view_recepient_display").show();
            $("#view_student_display").hide();
            $("#view_staff_display").show();
            $("#view_search_btn_visible").show();
        }
    }



     function showstudentlist()
    // $("button").click(function()
    {
        var type = $("#type").val();

        if(type == 'Staff')
        {
            getStaffList();
        }
        else
        {
            getStudentListByType();
        }
    }


    function getStaffList()
    {
        var tempPR = {};
        tempPR['type'] = $("#type").val();
        tempPR['staff_name'] = $("#staff_name").val();
        tempPR['staff_ic_no'] = $("#staff_ic_no").val();

        $.ajax(
        {
           url: '/communication/sendMail/getStaffList',
           type: 'POST',
           data:
           {
            data: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            // alert(result);
             //location.reload();
             $("#view").html(result);
             $("#view_visible").show();
           }
        });
    }


    function getStudentListByType()
    {
        var tempPR = {};
        tempPR['type'] = $("#type").val();
        tempPR['id_programme'] = $("#id_programme").val();
        tempPR['id_intake'] = $("#id_intake").val();
        tempPR['student'] = $("#student").val();

        $.ajax(
        {
           url: '/communication/sendMail/getStudentListByType',
           type: 'POST',
           data:
           {
            data: tempPR
           },
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
             //location.reload();
             $("#view").html(result);
             $("#view_visible").show();
           }
        });
    }
    

    // $('select').select2();


    $(function ()
    {
         // alert("asdf");
        $("#checkAll").click(function () {
            if ($("#checkAll").is(':checked')) {
                $(".check").prop("checked", true);
            } else {
                $(".check").prop("checked", false);
            }
        });
    });


    $(document).ready(function() {
        $("#form_sponser_has_students").validate({
            rules: {
                type: {
                    required: true
                },
                id_template: {
                    required: true
                }
            },
            messages: {
                type: {
                    required: "<p class='error-text'>Select Type</p>",
                },
                id_template: {
                    required: "<p class='error-text'>Select Template</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>

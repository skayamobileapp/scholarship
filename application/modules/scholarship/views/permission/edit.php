<?php

$menuarray = array('Roles','User','Salutation','Accredation Category','Accreditation Type','Salutation');
?>

<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit Permission</h3>
        </div>
        <form id="form_religion" action="" method="post">

         <div class="form-container">
            <h4 class="form-group-title">Permission Details</h4>

            <div class="row">
                 <div class="clearfix">
                    <ul class="menu-tree">
                                          
                        <li>
                            <span class="icon folder collapsed" data-toggle="collapse" href="#menu99" aria-expanded="false" aria-controls="menu99"></span>
                            <div class="checkbox">
                                <label>
                                  <input type="checkbox">
                                  <span class="check-radio"></span>
                                  User Management
                                </label>
                            </div> 
                              
                            <div class="collapse" id="menu99">
                                <ul>

                                    <?php for($i=0;$i<count($menuarray);$i++) {?>
                                    <li>
                                        <span class="icon folder collapsed" data-toggle="collapse" href="#menu<?php echo $i;?>" aria-expanded="false" aria-controls="menu<?php echo $i;?>"></span>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">
                                              <span class="check-radio"></span>
                                              <?php echo $menuarray[$i];?>
                                            </label>
                                        </div> 
                                          
                                        <div class="collapse" id="menu<?php echo $i;?>">
                                            <ul>
                                                <li>
                                                    <span class="icon page"></span>
                                                    <div class="checkbox">
                                                        <label>
                                                          <input type="checkbox">
                                                          <span class="check-radio"></span>
                                                          Add
                                                        </label>
                                                    </div>                                         
                                                </li>
                                                <li>
                                                    <span class="icon page"></span>
                                                    <div class="checkbox">
                                                        <label>
                                                          <input type="checkbox">
                                                          <span class="check-radio"></span>
                                                          Edit
                                                        </label>
                                                    </div>                                         
                                                </li> 
                                                <li>
                                                    <span class="icon page"></span>
                                                    <div class="checkbox">
                                                        <label>
                                                          <input type="checkbox">
                                                          <span class="check-radio"></span>
                                                          List
                                                        </label>
                                                    </div>                                         
                                                </li>
                                            </ul>
                                        </div>                              
                                    </li>
                                <?php }?>
                                   <!--  <li>
                                        <span class="icon folder collapsed" data-toggle="collapse" href="#menu21" aria-expanded="false" aria-controls="menu21"></span>
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox">
                                              <span class="check-radio"></span>
                                              Admin Setup
                                            </label>
                                        </div> 
                                          
                                        <div class="collapse" id="menu21">
                                            <ul>
                                                <li>
                                                    <span class="icon page"></span>
                                                    <div class="checkbox">
                                                        <label>
                                                          <input type="checkbox">
                                                          <span class="check-radio"></span>
                                                          Scheme
                                                        </label>
                                                    </div>                                         
                                                </li>
                                                <li>
                                                    <span class="icon page"></span>
                                                    <div class="checkbox">
                                                        <label>
                                                          <input type="checkbox">
                                                          <span class="check-radio"></span>
                                                          Award Level
                                                        </label>
                                                    </div>                                         
                                                </li> 
                                                <li>
                                                    <span class="icon page"></span>
                                                    <div class="checkbox">
                                                        <label>
                                                          <input type="checkbox">
                                                          <span class="check-radio"></span>
                                                          Program
                                                        </label>
                                                    </div>                                         
                                                </li>
                                                <li>
                                                    <span class="icon page"></span>
                                                    <div class="checkbox">
                                                        <label>
                                                          <input type="checkbox">
                                                          <span class="check-radio"></span>
                                                          Intake
                                                        </label>
                                                    </div>                                         
                                                </li>                                                                                                  
                                            </ul>
                                        </div>                              
                                    </li> -->

                                </ul>
                            </div>                              
                        </li>
                    </ul>
                </div>       

            </div>

        </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>

</div>
</div>

    
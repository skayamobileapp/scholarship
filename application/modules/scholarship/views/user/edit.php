<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
        <div class="page-title clearfix">
            <h3>Edit User</h3>
        </div>
        <form id="form" method="post">

            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Salutation <span class='error-text'>*</span></label>
                        <select name="salutation" id="salutation" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($salutationList)) {
                                foreach ($salutationList as $record) {
                            ?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php if($record->id == $userInfo->salutation)
                                        echo "selected=selected";
                                         ?> >
                                        <?php echo $record->name;  ?>        
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>First Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $userInfo->first_name; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Last Name <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $userInfo->last_name; ?>">
                    </div>
                </div>

                
            </div>


            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Email (Login) <span class='error-text'>*</span></label>
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $userInfo->email; ?>">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Mobile Number <span class='error-text'>*</span></label>
                        <input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo $userInfo->mobile; ?>">
                    </div>
                </div>


                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Role <span class='error-text'>*</span></label>
                        <select name="role" id="role" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($roleList)) {
                                foreach ($roleList as $record) {
                            ?>
                                    <option value="<?php echo $record->id;  ?>" <?php if($record->id == $userInfo->id_role) {echo "selected=selected";} ?>><?php echo $record->role;  ?></option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                        <div class="form-group">
                            <p>Status <span class='error-text'>*</span></p>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="1" <?php if($userInfo->status=='1') {
                                 echo "checked=checked";
                              };?>><span class="check-radio"></span> Active
                            </label>
                            <label class="radio-inline">
                              <input type="radio" name="status" id="status" value="0" <?php if($userInfo->status=='0') {
                                 echo "checked=checked";
                              };?>>
                              <span class="check-radio"></span> In-Active
                            </label>                              
                        </div>                         
                </div>


               <!--  <div class="col-sm-4">
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" id="password" name="password" value="<?php echo md5($userInfo->password); ?>">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" class="form-control" id="cpassword" name="cpassword" value="<?php echo md5($userInfo->password); ?>">
                    </div>
                </div> -->
            </div>
            <div class="button-block clearfix">
                <div class="bttn-group">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    <a href="../list" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>
<script>

    $('select').select2();
    
    $(document).ready(function() {
        $("#form").validate({
            rules: {
                salutaion: {
                    required: true
                },
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                mobile: {
                    required: true,
                    number: true
                },
                role: {
                    required: true,
                },
                password: {
                    required: true,
                },
                cpassword: {
                    required: true,
                    equalTo : "#password"
                }
            },
            messages: {
                salutaion: {
                    required: "<p class='error-text'>Select Salutation</p>",
                },
                first_name: {
                    required: "<p class='error-text'>Full Name Required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name Required</p>",
                },
                email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                mobile: {
                    required: "<p class='error-text'>Mobile Required</p>",
                    number: "<p class='error-text'>Please enter a valid phone number without +9</p>1"
                },
                role: {
                    required: "<p class='error-text'>Role Required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password Required</p>",
                },
                cpassword: {
                    required: "<p class='error-text'>Confirm Password Required</p>",
                    equalTo: "<p class='error-text'>Password Mismatc</p>h"
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
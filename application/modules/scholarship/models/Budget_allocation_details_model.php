<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Budget_allocation_details_model extends CI_Model
{
    function List()
    {
        $this->db->select('ba.id,ay.name as academicyear,st.name as scholarname,ba.amount,ba.status');
        $this->db->from('budget_allocation as ba');
        $this->db->join('academic_year as ay', 'ba.id_academic_year = ay.id');
        $this->db->join('scholarship_thrust as st', 'ba.id_thrust = st.id');
       
        $this->db->order_by("ay.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }


     function subThrustListSearch($id)
    {
        $this->db->select('sst.*');
        $this->db->from('scholarship_sub_thrust as sst');
        $this->db->where('sst.id_thrust', $id);
        
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getSubthrustAmount($idsubthrust,$id)
    {
        $this->db->select('sst.amount');
        $this->db->from('budget_allocation_details as sst');
        $this->db->where('sst.id_budget_allocation', $id);
        $this->db->where('sst.id_sub_thrust', $idsubthrust);
        
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

 function delete($idsubthrust,$id)
    {
        $this->db->where('id_budget_allocation', $id);
        $this->db->where('id_sub_thrust', $idsubthrust);
        $this->db->delete('budget_allocation_details');
        return TRUE;
    }



    function get($id)
    {
        $this->db->select('*');
        $this->db->from('budget_allocation_details');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function add($data)
    {
        $this->db->trans_start();
        $this->db->insert('budget_allocation_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    
    
}


<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Scheme_model extends CI_Model
{
    function schemeList()
    {
        $this->db->select('sp.*');
        $this->db->from('scholarship_scheme as sp');
        $this->db->order_by("sp.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function countryList()
    {
        $this->db->select('c.id, c.name');
        $this->db->from('country as c');
        $this->db->where('c.status', '1');
        $this->db->order_by("c.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function schemeListSearch($search)
    {
        $this->db->select('sp.*');
        $this->db->from('scholarship_scheme as sp');
        if (!empty($search))
        {
            $likeCriteria = "(sp.name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("sp.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }

    function getScheme($id)
    {
        $this->db->select('*');
        $this->db->from('scholarship_scheme');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewScheme($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_scheme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editScheme($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_scheme', $data);
        return TRUE;
    }

    function addTempDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('temp_scheme_has_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewSchemeHasProgramDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('scheme_has_program', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTempSchemeHasProgramBySession($id_session)
    {
        $this->db->select('tsp.*, p.name as program_name, p.code as program_code, sst.scholarship_name, sst.scholarship_code');
        $this->db->from('temp_scheme_has_program as tsp');
        $this->db->join('scholarship_programme as p', 'tsp.id_program = p.id');
        $this->db->join('scholarship_sub_thrust as sst', 'tsp.id_sub_thrust = sst.id');
        $this->db->where('tsp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function getTempSchemeHasProgram($id)
    {
        $this->db->select('tsp.*, p.name as program_name, p.code as program_code');
        $this->db->from('temp_scheme_has_program as tsp');
        $this->db->join('scholarship_programme as p', 'tsp.id_program = p.id');
        $this->db->where('tsp.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getTempSchemeProgram($id_session)
    {
        $this->db->select('tsp.*');
        $this->db->from('temp_scheme_has_program as tsp');
        $this->db->where('tsp.id_session', $id_session);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteTempSchemeHasProgramme($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('temp_scheme_has_program');
       return TRUE;
    }

    function moveTempDataToDetails($id_scheme)
    {
        $id_session = $this->session->my_scholar_session_id;
        $temp_details = $this->getTempSchemeProgram($id_session);
        foreach ($temp_details as $temp_detail)
        {
            $temp_detail->id_scholarship_scheme = $id_scheme;
            unset($temp_detail->id);
            unset($temp_detail->id_session);

            $inserted_detail = $this->addNewSchemeHasProgramDetails($temp_detail);
        }
        
        $inserted_detail = $this->deleteTempSchemeHasProgramBySession($id_session);
        return TRUE;
    }

    function deleteTempSchemeHasProgramBySession($id_session)
    {
        $this->db->where('id_session', $id_session);
       $this->db->delete('temp_scheme_has_program');
       return TRUE;
    }

    function getSchemeHasProgramByMasterId($id_scholarship_scheme)
    {
        $this->db->select('tsp.*, p.name as program_name, p.code as program_code, sst.scholarship_name, sst.scholarship_code');
        $this->db->from('scheme_has_program as tsp');
        $this->db->join('scholarship_programme as p', 'tsp.id_program = p.id');
        $this->db->join('scholarship_sub_thrust as sst', 'tsp.id_sub_thrust = sst.id');
        $this->db->where('tsp.id_scholarship_scheme', $id_scholarship_scheme);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteSchemeHasProgram($id)
    {
        $this->db->where('id', $id);
       $this->db->delete('scheme_has_program');
       return TRUE;
    }

    function getProgramBySubThrustId($id_sub_thrust)
    {
        $this->db->select('DISTINCT(sst.id) as id, sst.code as program_code, sst.code as program_code, sst.name as program_name');
        $this->db->from('scholarship_programme as sst');
        $this->db->join('scholarship_sub_thrust_has_program as sel','sst.id = sel.id_program');
        $this->db->where('sel.id_sub_thrust', $id_sub_thrust);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function subThrustListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_sub_thrust');
        $this->db->where('status', $status);
        $this->db->order_by("scholarship_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         //echo "<Pre>"; print_r($result);exit;
         return $result;
    }
}


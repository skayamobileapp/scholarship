<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Budget_allocation_model extends CI_Model
{
    function List()
    {
        $this->db->select('ba.id,ay.name as academicyear,st.name as scholarname,ba.amount,ba.status');
        $this->db->from('budget_allocation as ba');
        $this->db->join('academic_year as ay', 'ba.id_academic_year = ay.id');
        $this->db->join('scholarship_thrust as st', 'ba.id_thrust = st.id');
       
        $this->db->order_by("ay.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();   
         //print_r($result);exit();     
         return $result;
    }

    function get($id)
    {
        $this->db->select('*');
        $this->db->from('budget_allocation');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function add($data)
    {
        $this->db->trans_start();
        $this->db->insert('budget_allocation', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function edit($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('budget_allocation', $data);
        return TRUE;
    }
    
    
}


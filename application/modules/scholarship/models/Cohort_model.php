<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Cohort_model extends CI_Model
{
    function cohortListSearch()
    {
        $this->db->select('ca.*,ss.name as cohortname');
        $this->db->from('cohort_activity as ca');

        $this->db->join('scholarship_scheme as ss', 'ca.id_cohort = ss.id');

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


     function getCohort($id)
    {
        $this->db->select('*');
        $this->db->from('cohort_activity');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    
    function add($data)
    {
        $this->db->trans_start();
        $this->db->insert('cohort_activity', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function edit($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('cohort_activity', $data);
        return TRUE;
    }



    function communicationTemplate()
    {
        $this->db->select('*');
        $this->db->from('communication_template');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function scholarshipList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_sub_thrust');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function cohortList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_scheme');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function programmeList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

}


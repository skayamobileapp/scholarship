<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Programme_model extends CI_Model
{
    function programmeList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function userList($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_users');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function awardListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_award_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function programmeListSearch($search)
    {
        $this->db->select('sp.*, sd.code as award_code, sd.name as award_name');
        $this->db->from('scholarship_programme as sp');
        $this->db->join('scholarship_award_setup as sd','sp.id_award = sd.id');
        if (!empty($search))
        {
            $likeCriteria = "(sp.name  LIKE '%" . $search . "%' or sp.name_optional_language  LIKE '%" . $search . "%' or sp.code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("sp.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProgrammeDetails($id)
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProgrammeDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_programme', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editProgrammeDetails($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_programme', $data);
        return TRUE;
    }

    function addNewProgrammeDocumentRequirements($id_scholarship_program)
    {
        $data['id_scholarship_program'] = $id_scholarship_program;
        $this->db->trans_start();
        $this->db->insert('is_scholarship_applicant_personel_details', $data);
        $this->db->insert('is_scholarship_applicant_examination_details', $data);
        $this->db->insert('is_scholar_applicant_family_details', $data);
        $this->db->insert('is_scholar_applicant_employment_status', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function programCertificationList($id_program)
    {
        $this->db->select('*');
        $this->db->from('program_has_certification');
        $this->db->where('id_program', $id_program);
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function saveCertificationData($data)
    {
        $this->db->trans_start();
        $this->db->insert('program_has_certification', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function deleteCertificationData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('program_has_certification');
        return TRUE;
    }
}
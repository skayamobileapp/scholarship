<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Education_level_model extends CI_Model
{
    function educationLevelList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_education_level');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function educationLevelListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('scholarship_education_level');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or short_name  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getEducationLevel($id)
    {
        $this->db->select('*');
        $this->db->from('scholarship_education_level');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewEducationLevel($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_education_level', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editEducationLevel($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_education_level', $data);
        return TRUE;
    }
}
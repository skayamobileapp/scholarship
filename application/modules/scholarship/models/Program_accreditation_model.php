<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Program_accreditation_model extends CI_Model
{
    function programListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function programAccreditationList($data)
    {
        $this->db->select('fc.*, sat.name as category_name, sat.code as category_code, sac.name as type_name, sac.code as type_code, p.code as program_code, p.name as program_name');
        $this->db->from('scholarship_program_accreditation as fc');
        $this->db->join('scholarship_accreditation_category as sat', 'fc.id_accreditation_category = sat.id');
        $this->db->join('scholarship_accreditation_type as sac', 'fc.id_accreditation_type = sac.id');
        $this->db->join('scholarship_programme as p', 'fc.id_program = p.id');
        if($data['id_accreditation_category'] != '')
        {
            $this->db->where('fc.id_accreditation_category', $data['id_accreditation_category']);
        }
        if($data['id_accreditation_type'] != '')
        {
            $this->db->where('fc.id_accreditation_type', $data['id_accreditation_type']);
        }
        if($data['id_program'] != '')
        {
            $this->db->where('fc.id_program', $data['id_program']);
        }
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getProgramAccreditation($id)
    {
        $this->db->select('fc.*, sat.name as category_name, sat.code as category_code, sac.name as type_name, sac.code as type_code');
        $this->db->from('scholarship_program_accreditation as fc');
        $this->db->join('scholarship_accreditation_category as sat', 'fc.id_accreditation_category = sat.id');
        $this->db->join('scholarship_accreditation_type as sac', 'fc.id_accreditation_type = sac.id');
        $this->db->where('fc.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewProgramAccreditation($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_program_accreditation', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editProgramAccreditation($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_program_accreditation', $data);
        return TRUE;
    }

    function accreditationCategoryByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_accreditation_category');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function accreditationTypeByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_accreditation_type');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function deleteProgramAccreditation($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('scholarship_program_accreditation');
        return TRUE;
    }
}
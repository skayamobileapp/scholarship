<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Accreditation_category_model extends CI_Model
{
    function accreditationCategoryList()
    {
        $this->db->select('*');
        $this->db->from('scholarship_accreditation_category');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function accreditationCategoryListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('scholarship_accreditation_category');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or code  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAccreditationCategory($id)
    {
        $this->db->select('*');
        $this->db->from('scholarship_accreditation_category');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewAccreditationCategory($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholarship_accreditation_category', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editAccreditationCategory($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('scholarship_accreditation_category', $data);
        return TRUE;
    }
}


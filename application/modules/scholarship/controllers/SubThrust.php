<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SubThrust extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('sub_thrust_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('sub_thrust.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_thrust'] = $this->security->xss_clean($this->input->post('id_thrust'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
 
            $data['searchParam'] = $formData;
            $data['subThrustList'] = $this->sub_thrust_model->subThrustListSearch($formData);
            $data['thrustList'] = $this->sub_thrust_model->thrustListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Partner University List';
            //print_r($subjectDetails);exit;
            $this->loadViews("sub_thrust/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('sub_thrust.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_scholar_session_id;

                $formData = $this->input->post();

                // echo "<Pre>"; print_r($formData);exit;

                $id_thrust = $this->security->xss_clean($this->input->post('id_thrust'));
                $scholarship_code = $this->security->xss_clean($this->input->post('scholarship_code'));
                $scholarship_name = $this->security->xss_clean($this->input->post('scholarship_name'));
                $scholarship_short_name = $this->security->xss_clean($this->input->post('scholarship_short_name'));
                $status = $this->security->xss_clean($this->input->post('status'));


                // $check_login_id = $this->sub_thrust_model->checkDuplicationLoginId($login_id,$email);
                // if($check_login_id)
                // {
                //     echo "<Pre>";print_r('Login Id or Email-Id Already Exist, Select Another One');exit();
                // }

            
                $data = array(
                    'id_thrust' => $id_thrust,
                    'scholarship_code' => $scholarship_code,
                    'scholarship_name' => $scholarship_name,
                    'scholarship_short_name' => $scholarship_short_name,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->sub_thrust_model->addNewSubThrust($data);
                redirect('/scholarship/subThrust/edit/'.$result);
            }
            //print_r($data['stateList']);exit;
            //$this->load->model('sub_thrust_model');
            $data['thrustList'] = $this->sub_thrust_model->thrustListByStatus('1');

            $this->global['pageTitle'] = 'Campus Management System : Add Partner University';
            $this->loadViews("sub_thrust/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('sub_thrust.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/subThrust/list');
            }
            if($this->input->post())
            {

                $resultprint = $this->input->post();

                // echo "<Pre>"; print_r($resultprint);exit();
                if($resultprint)
                {
                    $id_user = $this->session->userId;
                    $id_session = $this->session->my_scholar_session_id;

                    $formData = $this->input->post();


                    $id_thrust = $this->security->xss_clean($this->input->post('id_thrust'));
                    $scholarship_code = $this->security->xss_clean($this->input->post('scholarship_code'));
                    $scholarship_name = $this->security->xss_clean($this->input->post('scholarship_name'));
                    $scholarship_short_name = $this->security->xss_clean($this->input->post('scholarship_short_name'));
                    $status = $this->security->xss_clean($this->input->post('status'));


                    // $check_login_id = $this->sub_thrust_model->checkDuplicationLoginId($login_id,$email);
                    // if($check_login_id)
                    // {
                    //     echo "<Pre>";print_r('Login Id or Email-Id Already Exist, Select Another One');exit();
                    // }

                
                    $data = array(
                        'id_thrust' => $id_thrust,
                        'scholarship_code' => $scholarship_code,
                        'scholarship_name' => $scholarship_name,
                        'scholarship_short_name' => $scholarship_short_name,
                        'status' => $status,
                        'updated_by' => $id_user
                    );

                    $result = $this->sub_thrust_model->editSubThrust($data,$id);
                    redirect('/scholarship/subThrust/list');                
                }
            }

            $data['subThrust'] = $this->sub_thrust_model->getSubThrust($id);
            $data['raceList'] = $this->sub_thrust_model->raceListByStatus('1');
            $data['thrustList'] = $this->sub_thrust_model->thrustListByStatus('1');
            $data['programList'] = $this->sub_thrust_model->programListByStatus('1');
            $data['qualificationList'] = $this->sub_thrust_model->qualificationListByStatus('1');
            $data['workSpecialisationList'] = $this->sub_thrust_model->workSpecialisationListByStatus('1');
            if($data['subThrust'])
            {

                $data['programEntryRequirementList'] = $this->sub_thrust_model->getScholarshipSubThrustEntryRequirement($id);

                for($i=0;$i<count($data['programEntryRequirementList']);$i++) {
                    $idraces =  $data['programEntryRequirementList'][$i]->id_race;

                    $racenames = $this->sub_thrust_model->getScholarshipSubThrustEntryRequirementRace($idraces);

                    $rnames = '';
                    for($r=0;$r<count($racenames);$r++) {
                      $rnames = $rnames.', '.$racenames[$r]->name;
                    }

                    $data['programEntryRequirementList'][$i]->race_name = $rnames;
                    
                }
                $data['scholarshipProgramList'] = $this->sub_thrust_model->getScholarshipProgramBySubThrustId($id);

            }
            // echo "<Pre>"; print_r($data['programEntryRequirementList']);exit;

            $this->global['pageTitle'] = 'Campus Management System : Edit Partner University';
            $this->loadViews("sub_thrust/edit", $this->global, $data, NULL);
        }
    }


    function addProgramRequirement()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();

        $programs = $tempData['id_program'];

        if(count($programs) > 0)
        {
            foreach ($programs as $program)
            {
                
                // echo "<Pre>"; print_r($program);exit();
                $data['id_sub_thrust'] = $tempData['id_sub_thrust'];
                $data['id_program'] = $program;
                $data['requirement_entry'] = $tempData['requirement_entry'];
                $data['status'] = $tempData['status'];

                $requirement_entry = $tempData['requirement_entry'];

                $inserted_id = $this->sub_thrust_model->addSubThrustRequirement($data);
                
                // echo "<Pre>"; print_r($inserted_id);exit();

                $tempData['id_requirement'] = $inserted_id;
                $tempData['id_program'] = $program;

                switch ($requirement_entry)
                {
                    case 'Age':
                        $required_inserted_id = $this->AddAgeRequirement($tempData);
                        break;

                    case 'Education':
                        $required_inserted_id = $this->AddEducationRequirement($tempData);
                        // echo "<Pre>"; print_r($required_inserted_id);exit();
                        break;

                    case 'Work Experience':
                        $required_inserted_id = $this->AddWorkExperienceRequirement($tempData);
                        break;

                    case 'Other Requirement':
                        $required_inserted_id = $this->AddOtherRequirement($tempData);
                        break;
                }
            }
        }

        if($required_inserted_id)
        {
            echo "success";
        }
    }

    function AddAgeRequirement($data)
    {
        $age_data['id_sub_thrust'] = $data['id_sub_thrust'];
        $age_data['id_requirement'] = $data['id_requirement'];
        $age_data['id_program'] = $data['id_program'];
        $age_data['min_age'] = $data['min_age'];
        $age_data['max_age'] = $data['max_age'];

        // echo "<Pre>"; print_r($age_data);exit();
        $inserted_id = $this->sub_thrust_model->AddAgeRequirement($age_data);

        return $inserted_id;
    }

    function AddEducationRequirement($data)
    {
        $education_data['id_sub_thrust'] = $data['id_sub_thrust'];
        $education_data['id_requirement'] = $data['id_requirement'];
        $education_data['id_program'] = $data['id_program'];
        $education_data['id_qualification'] = $data['id_qualification'];
        $education_data['education_descrption'] = $data['education_descrption'];

        $inserted_id = $this->sub_thrust_model->AddEducationRequirement($education_data);
        // echo "<Pre>"; print_r($inserted_id);exit();

        return $inserted_id;
    }

    function AddWorkExperienceRequirement($data)
    {
        $experience_data['id_sub_thrust'] = $data['id_sub_thrust'];
        $experience_data['id_requirement'] = $data['id_requirement'];
        $experience_data['id_program'] = $data['id_program'];
        $experience_data['min_experience'] = $data['min_experience'];
        $experience_data['id_specialisation'] = $data['id_specialisation'];
        $experience_data['experience_descrption'] = $data['experience_descrption'];

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->sub_thrust_model->AddWorkExperienceRequirement($experience_data);

        return $inserted_id;
    }

    function AddOtherRequirement($data)
    {
        $other_data['id_sub_thrust'] = $data['id_sub_thrust'];
        $other_data['id_requirement'] = $data['id_requirement'];
        $other_data['id_program'] = $data['id_program'];
        $other_data['other_descrption'] = $data['other_descrption'];

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->sub_thrust_model->AddOtherRequirement($other_data);

        return $inserted_id;
    }

    function viewRequirementData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        $id_sub_thrust = $tempData['id_sub_thrust'];
        $id_program = $tempData['id_program'];

        $age_details = $this->sub_thrust_model->getAgeRequirementsBySubThrustIdNProgramId($id_sub_thrust,$id_program);
        $education_details = $this->sub_thrust_model->getEducationRequirementsBySubThrustIdNProgramId($id_sub_thrust,$id_program);
        $work_experience_details = $this->sub_thrust_model->getWorkExperienceRequirementsBySubThrustIdNProgramId($id_sub_thrust,$id_program);
        $other_details = $this->sub_thrust_model->getOtherRequirementsBySubThrustIdNProgramId($id_sub_thrust,$id_program);

        // echo "<Pre>"; print_r($work_experience_details);exit();

        $table = "
        <h5> Age Requirements</h5>
        ";

        if(!empty($age_details))
        {
            
        // echo "<Pre>";print_r($details);exit;
        $table .= "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Age Requirement</th>
                    <th>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($age_details);$i++)
                    {
                        $id = $age_details[$i]->id;
                        $min_age = $age_details[$i]->min_age;
                        $max_age = $age_details[$i]->max_age;
                        $j=$i+1;

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>Min $min_age Year, Max $max_age Year</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteAgeRequirement($id)'>Delete</a>
                            <td>
                        </tr>";
                                // <span onclick='deleteTempData($id)'>Delete</a>
                    }

                     $table .= "
                    </tbody>
                    </table>
        </div>
        <br>";
        }
        else
        {
            $table .= "
            <h6 class='text-center'>No Date Available</h6>
            <br>
            ";

        }



        $table .= "
        <h5> Education Requirements</h5>
        ";

        if(!empty($education_details))
        {
            
        // echo "<Pre>";print_r($details);exit;
        $table .= "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Qualification Code</th>
                    <th>Qualification Name</th>
                    <th>Qualification Description</th>
                    <th>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($education_details);$i++)
                    {


                        $id = $education_details[$i]->id;
                        $id_qualification = $education_details[$i]->id_qualification;
                        $education_level_short_name = $education_details[$i]->education_level_short_name;
                        $education_level_name = $education_details[$i]->education_level_name;
                        $education_descrption = $education_details[$i]->education_descrption;
                        $j=$i+1;

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$education_level_short_name</td>
                            <td>$education_level_name</td>
                            <td>$education_descrption</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteEducationRequirement($id)'>Delete</a>
                            <td>
                        </tr>";
                                // <span onclick='deleteTempData($id)'>Delete</a>
                    }

                     $table .= "
                    </tbody>
                    </table>
        </div>
        <br>";
        }
        else
        {
            $table .= "
            <h6 class='text-center'>No Date Available</h6>
            <br>
            ";

        }




        $table .= "
        <h5> Work Experience Requirements</h5>
        ";

        if(!empty($work_experience_details))
        {
            
        // echo "<Pre>";print_r($details);exit;
        $table .= "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Min Years</th>
                    <th>Specialisation</th>
                    <th>Description</th>
                    <th>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($work_experience_details);$i++)
                    {
                        $id = $work_experience_details[$i]->id;
                        $min_experience = $work_experience_details[$i]->min_experience;
                        $id_specialisation = $work_experience_details[$i]->id_specialisation;
                        $work_code = $work_experience_details[$i]->work_code;
                        $work_name = $work_experience_details[$i]->work_name;
                        $experience_descrption = $work_experience_details[$i]->experience_descrption;
                        $j=$i+1;

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$min_experience</td>
                            <td>$work_code - $work_name</td>
                            <td>$experience_descrption</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteWorkExperienceRequirement($id)'>Delete</a>
                            <td>
                        </tr>";
                                // <span onclick='deleteTempData($id)'>Delete</a>
                    }

                     $table .= "
                    </tbody>
                    </table>
        </div>
        <br>";
        }
        else
        {
            $table .= "
            <h6 class='text-center'>No Date Available</h6>
            <br>
            ";

        }



        $table .= "
        <h5> Other Requirements</h5>
        ";

        if(!empty($other_details))
        {
            
        // echo "<Pre>";print_r($details);exit;
        $table .= "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Description</th>
                    <th>Action</th>
                    </tr>
                </thead>";
                    $total_detail = 0;
                    for($i=0;$i<count($other_details);$i++)
                    {
                        $id = $other_details[$i]->id;
                        $other_descrption = $other_details[$i]->other_descrption;
                        $j=$i+1;

                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$other_descrption</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteOtherRequirement($id)'>Delete</a>
                            <td>
                        </tr>";
                                // <span onclick='deleteTempData($id)'>Delete</a>
                    }

                     $table .= "
                    </tbody>
                    </table>
        </div>
        <br>";
        }
        else
        {
            $table .= "
            <h6 class='text-center'>No Date Available</h6>
            <br>
            ";

        }


        echo $table;exit;
    }

    function deleteAgeRequirement($id)
    {
      $deleted = $this->sub_thrust_model->deleteAgeRequirement($id);   
        echo "Success"; 
    }

    function deleteEducationRequirement($id)
    {
        $deleted = $this->sub_thrust_model->deleteEducationRequirement($id);
        echo "Success";
    }

    function deleteWorkExperienceRequirement($id)
    {
        $deleted = $this->sub_thrust_model->deleteWorkExperienceRequirement($id);
        echo "Success";
    }

    function deleteOtherRequirement($id)
    {
        $deleted = $this->sub_thrust_model->deleteOtherRequirement($id);
        echo "Success";
    }

    function getPartnerProgram($id)
    {
            // echo "<Pre>"; print_r($id);exit;
         $results = $this->sub_thrust_model->getPartnerProgram($id);

            // echo "<Pre>"; print_r($results);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>";

            $table.="
            <select name='program_id_partner' id='program_id_partner' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $partner_code = $results[$i]->partner_code;
            $partner_name = $results[$i]->partner_name;
            $table.="<option value=".$id.">".$partner_code . " - " . $partner_name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function addScholarshipProgram()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->sub_thrust_model->addScholarshipProgram($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }

    function deleteScholarshipProgram($id)
    {
        $deleted = $this->sub_thrust_model->deleteScholarshipProgram($id);   
        echo "Success"; 
    }

    function addProgramEntryRequirement()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $tempData['age'] = 0;
        $tempData['education'] = 0;
        $tempData['income'] = 0;
        $tempData['other'] = 0;

        if($tempData['min_age'] != '' || $tempData['max_age'] != '')
        {
            $tempData['age'] = 1;
        }

        if($tempData['id_education_qualification'] != '' || $tempData['education_description'] != '')
        {
            $tempData['education'] = 1;
        }

        if($tempData['min_income'] != '' || $tempData['max_income'] != '' || $tempData['income_type'] != '')
        {
            $tempData['income'] = 1;
        }

        if($tempData['other_description'] != '')
        {
            $tempData['other'] = 1;
        }

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->sub_thrust_model->addProgramEntryRequirement($tempData);

        if($inserted_id)
        {
            echo "success";
        }
    }

    function deleteScholarshipSubThrustEntryRequirement($id)
    {
        $deleted = $this->sub_thrust_model->deleteScholarshipSubThrustEntryRequirement($id);   
        echo "Success"; 
    }
}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class BudgetAllocationDetails extends BaseController
{
    public function __construct()
    {
        error_reporting(0);
        parent::__construct();
                $this->load->model('sub_thrust_model');
                $this->load->model('thrust_model');
        $this->load->model('academic_year_model');
        $this->load->model('budget_allocation_model');
        $this->load->model('budget_allocation_details_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('budget_allocation_details.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $name;

            $data['budgetAllocationDetails'] = $this->budget_allocation_details_model->List();

            $this->global['pageTitle'] = 'Campus Management System : Academic Year';
            //print_r($subjectDetails);exit;
            $this->loadViews("budget_allocation_details/list", $this->global, $data, NULL);
        }
    }
    
    

    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('budget_allocation_details.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_scholar_session_id;
            $user_id = $this->session->userId;

            if ($id == null)
            {
                redirect('/scholarship/budgetAllocation/list');
            }
            if($this->input->post())
            {

                $formData = $this->input->post();

                for($i=0;$i<count($formData['amountdetails']);$i++) {

                        $amount = $formData['amountdetails'][$i];
                        $id_sub_thrust = $formData['subthrustids'][$i];

$result = $this->budget_allocation_details_model->delete($id_sub_thrust,$id);

                     $data = array(
                    'id_budget_allocation' => $id,
                    'id_sub_thrust' => $id_sub_thrust,
                    'amount' => $amount
                );

                $result = $this->budget_allocation_details_model->add($data);



                }
                
                redirect('/scholarship/budgetAllocationDetails/list');
            }
            $name='';
           $data['academicYearList'] = $this->academic_year_model->academicYearList($name);
        $data['thrustList'] = $this->thrust_model->thrustListSearch($name);



            $data['budgetAllocationDetails'] = $this->budget_allocation_model->get($id);

            $data['budgetAllocationsubDetails'] = $this->budget_allocation_details_model->get($id);

            $idthrust = $data['budgetAllocationDetails']->id_thrust;

            $data['subThrustList'] = $this->budget_allocation_details_model->subThrustListSearch($idthrust);



            for($k=0;$k<count($data['subThrustList']);$k++) {
                $idsubthrust = $data['subThrustList'][$k]->id;

               $data['subThrustList'][$k]->amount = 0;
                $amoutobject = $this->budget_allocation_details_model->getSubthrustAmount($idsubthrust,$id);

               $data['subThrustList'][$k]->amount = $amoutobject->amount;
            }




            $this->global['pageTitle'] = 'Campus Management System : Edit Academic Year';
            $this->loadViews("budget_allocation_details/edit", $this->global, $data, NULL);
        }
    }
}

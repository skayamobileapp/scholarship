<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Documents extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('documents_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('documents.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParameters'] = $formData;
            $data['documentsList'] = $this->documents_model->documentsListSearch($formData);
            $this->global['pageTitle'] = 'Scholarship Management System : Documents List';
            $this->loadViews("documents/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('documents.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_user = $this->session->userId;
                $id_session = $this->session->my_scholar_session_id;

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $file_size = $this->security->xss_clean($this->input->post('file_size'));
 $file_type = $this->security->xss_clean($this->input->post('file_type'));                
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
                    'file_size' => $file_size,
                    'file_type' => $file_type,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->documents_model->addNewDocuments($data);
                redirect('/scholarship/documents/list');
            }
            $this->global['pageTitle'] = 'Scholarship Management System : Add Documents';
            $this->loadViews("documents/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('documents.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/documents/list');
            }
            if($this->input->post())
            {

                $filetypes = '';
                for($k=0;$k<count($this->input->post('file_type'));$k++) {
                $filetypes = $filetypes.','.$this->input->post('file_type')[$k];
                }

                $id_user = $this->session->userId;
                $id_session = $this->session->my_scholar_session_id;

                $name = $this->security->xss_clean($this->input->post('name'));
                $description = $this->security->xss_clean($this->input->post('description'));
                $code = $this->security->xss_clean($this->input->post('code'));
                                $file_size = $this->security->xss_clean($this->input->post('file_size'));
 $file_type = $filetypes;                


                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'description' => $description,
  'file_size' => $file_size,
                    'file_type' => $file_type,                    
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $id_user
                );

                $result = $this->documents_model->editDocuments($data,$id);
                redirect('/scholarship/documents/list');
            }
            $data['documentDetails'] = $this->documents_model->getDocuments($id);
            $this->global['pageTitle'] = 'Scholarship Management System : Edit Documents';
            $this->loadViews("documents/edit", $this->global, $data, NULL);
        }
    }
}

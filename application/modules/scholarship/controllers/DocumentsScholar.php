<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class DocumentsScholar extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('documents_scholar_model');
        $this->load->model('documents_model');

        $this->isScholarLoggedIn();
    }

    function list()
    {

         if ($this->checkScholarAccess('documents.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParameters'] = $formData;
            $data['documentsList'] = $this->documents_model->documentsListSearch($formData);
            $this->global['pageTitle'] = 'Scholarship Management System : Documents List';
            $this->loadViews("documents_scholar/list", $this->global, $data, NULL);
        }

        
    }
   

    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('documents_program.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/documentsProgram/list');
            }
            if($this->input->post())
            {

                $id_user = $this->session->userId;
                $id_session = $this->session->my_scholar_session_id;

               
            
                 $data['id_scholarship_sub_thrust'] =  $this->security->xss_clean($this->input->post('id_scholarship_sub_thrust'));
                $data['mandatory'] = $this->security->xss_clean($this->input->post('mandatory'));
                $data['id_documents'] =  $id;

                $inserted_id = $this->documents_scholar_model->addNewDocumentsProgramDetails($data);

        
            }

            $data['documentDetails'] = $this->documents_model->getDocuments($id);


              $data['temp_details'] = $this->documents_scholar_model->getTempDetailsBySession($id); 

            $data['subthrustlist'] = $this->documents_scholar_model->ScholarListByStatus();


            $this->global['pageTitle'] = 'Scholarship Management System : Edit Documents For Program';
            $this->loadViews("documents_scholar/edit", $this->global, $data, NULL);
        }
    }

    
}

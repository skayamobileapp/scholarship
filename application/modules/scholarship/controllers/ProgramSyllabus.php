<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProgramSyllabus extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('program_syllabus_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('program_syllabus.list') == 1)
        // if ($this->checkScholarAccess('program_syllabus.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_thrust'] = $this->security->xss_clean($this->input->post('id_thrust'));
            $formData['id_sub_thrust'] = $this->security->xss_clean($this->input->post('id_sub_thrust'));
 
            $data['searchParam'] = $formData;

            $data['programSyllabusList'] = $this->program_syllabus_model->programSyllabusList($formData);

            $data['thrustList'] = $this->program_syllabus_model->thrustListByStatus('1');
            $data['subThrustList'] = $this->program_syllabus_model->subThrustListByStatus('1');
            $data['programList'] = $this->program_syllabus_model->programListByStatus('1');


            $this->global['pageTitle'] = 'scholarship Management System : Program Syllabus List';
            $this->loadViews("program_syllabus/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('program_syllabus.add') == 1)
        // if ($this->checkScholarAccess('program_syllabus.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_scholar_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $formData = $this->input->post();

               // echo "<Pre>"; print_r($formData);exit;
            	

                $id_thrust = $this->security->xss_clean($this->input->post('id_thrust'));
                $id_sub_thrust = $this->security->xss_clean($this->input->post('id_sub_thrust'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                $data = array(
					'id_thrust' => $id_thrust,
					'id_sub_thrust' => $id_sub_thrust,
					'id_program' => $id_program,
					'status' => $status,
					'created_by' => $user_id
                );

                $inserted_id = $this->program_syllabus_model->addNewProgramSyllabus($data);
                redirect('/scholarship/programSyllabus/edit/'.$inserted_id);
            }

            $data['thrustList'] = $this->program_syllabus_model->thrustListByStatus('1');
            $data['programList'] = $this->program_syllabus_model->programListByStatus('1');
               // echo "<Pre>"; print_r($data);exit;

            
            $this->global['pageTitle'] = 'scholarship Management System : Add Program Syllabus';
            $this->loadViews("program_syllabus/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('program_syllabus.edit') == 1)
        // if ($this->checkScholarAccess('program_syllabus.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/programSyllabus/list');
            }
            if($this->input->post())
            {
                $formData = $this->input->post();

               // echo "<Pre>"; print_r($formData);exit;

	            $id_session = $this->session->my_scholar_session_id;
                $user_id = $this->session->userId;

                $id_thrust = $this->security->xss_clean($this->input->post('id_thrust'));
                $id_sub_thrust = $this->security->xss_clean($this->input->post('id_sub_thrust'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                $data = array(
                    'id_thrust' => $id_thrust,
                    'id_sub_thrust' => $id_sub_thrust,
                    'id_program' => $id_program,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                //print_r($data);exit;
                $result = $this->program_syllabus_model->editProgramSyllabus($data,$id);
                redirect('/scholarship/programSyllabus/list');
            }
            // $data['studentList'] = $this->program_syllabus_model->studentList();
            $data['programSyllabus'] = $this->program_syllabus_model->getProgramSyllabus($id);
            $data['syllanusModuleList'] = $this->program_syllabus_model->syllanusModuleList($id);
            $data['thrustList'] = $this->program_syllabus_model->thrustListByStatus('1');
            $data['subThrustList'] = $this->program_syllabus_model->subThrustListByStatus('1');
            $data['programList'] = $this->program_syllabus_model->programListByStatus('1');
            
               // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'scholarship Management System : Edit Program Syllabus';
            $this->loadViews("program_syllabus/edit", $this->global, $data, NULL);
        }
    }

    function getSubThrustByThrustId($id_thrust)
    {
        $results = $this->program_syllabus_model->getSubThrustByThrustId($id_thrust);

        // echo "<Pre>"; print_r($programme_data);exit;
     //    $table="   
     //        <script type='text/javascript'>
     //             $('select').select2();
     //         </script>
     // ";

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_sub_thrust' id='id_sub_thrust' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->scholarship_name;
        $code = $results[$i]->scholarship_code;
        $table.="<option value=".$id.">" . $code . " - " . $name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }

    function addSyllabusModule()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->program_syllabus_model->addSyllabusModule($tempData);

        if($inserted_id)
        {
            echo "success";
        }

    }

    function deleteSyllabusModule($id)
    {
        $deleted = $this->program_syllabus_model->deleteSyllabusModule($id);   
        echo "Success"; 
    }
}

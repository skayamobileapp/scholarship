<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class BudgetAllocation extends BaseController
{
    public function __construct()
    {
        parent::__construct();
                $this->load->model('thrust_model');
        $this->load->model('academic_year_model');
        $this->load->model('budget_allocation_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('budget_allocation.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $name;

            $data['budgetAllocationDetails'] = $this->budget_allocation_model->List();

            $this->global['pageTitle'] = 'Campus Management System : Academic Year';
            //print_r($subjectDetails);exit;
            $this->loadViews("budget_allocation/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('budget_allocation.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_scholar_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $id_thrust = $this->security->xss_clean($this->input->post('id_thrust'));
                $id_academic_year = $this->security->xss_clean($this->input->post('id_academic_year'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_thrust' => $id_thrust,
                    'id_academic_year' => $id_academic_year,
                    'amount' => $amount,
                    'status' => $status
                );
            
                $result = $this->budget_allocation_model->add($data);
                redirect('/scholarship/budgetAllocation/list');
            }
            $name='';
                        $data['academicYearList'] = $this->academic_year_model->academicYearList($name);
                $data['thrustList'] = $this->thrust_model->thrustListSearch($name);

            $this->load->model('budget_allocation_model');
            $this->global['pageTitle'] = 'Campus Management System : Add Academic Year';
            $this->loadViews("budget_allocation/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('budget_allocation.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_scholar_session_id;
            $user_id = $this->session->userId;

            if ($id == null)
            {
                redirect('/scholarship/budgetAllocation/list');
            }
            if($this->input->post())
            {
                $id_thrust = $this->security->xss_clean($this->input->post('id_thrust'));
                $id_academic_year = $this->security->xss_clean($this->input->post('id_academic_year'));
                $amount = $this->security->xss_clean($this->input->post('amount'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_thrust' => $id_thrust,
                    'id_academic_year' => $id_academic_year,
                    'amount' => $amount,
                    'status' => $status
                );
                $result = $this->budget_allocation_model->edit($data,$id);
                redirect('/scholarship/budgetAllocation/list');
            }
            $name='';
                        $data['academicYearList'] = $this->academic_year_model->academicYearList($name);
                $data['thrustList'] = $this->thrust_model->thrustListSearch($name);

            $data['budgetAllocationDetails'] = $this->budget_allocation_model->get($id);

            $this->global['pageTitle'] = 'Campus Management System : Edit Academic Year';
            $this->loadViews("budget_allocation/edit", $this->global, $data, NULL);
        }
    }
}

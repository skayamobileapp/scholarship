<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Modulelisting extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('module_listing_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('role.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['status'] = $this->security->xss_clean($this->input->post('status'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
 
            $data['searchParam'] = $formData;
            $data['moduleList'] = $this->module_listing_model->moduleList();


            $this->global['pageTitle'] = 'Scholarship Management System : Role List';
            $this->loadViews("module_listing/list", $this->global, $data, NULL);
        }
    }
    
    
}

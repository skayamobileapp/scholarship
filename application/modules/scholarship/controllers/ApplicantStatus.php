<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ApplicantStatus extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('applicant_status_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('status.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['list'] = $this->applicant_status_model->list();
            $this->global['pageTitle'] = 'Scholarship Management System : Race List';
            $this->loadViews("applicant_status/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('status.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $id_status_master = $this->security->xss_clean($this->input->post('id_status_master'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'id_status_master' => $id_status_master,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->applicant_status_model->addNew($data);
                redirect('/scholarship/applicantStatus/list');
            }
            $this->loadViews("applicant_status/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('race.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/scholarship/applicantStatus/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $id_status_master = $this->security->xss_clean($this->input->post('id_status_master'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
            
                $data = array(
                   'name' => $name,
                    'status' => $status
                );

                $result = $this->applicant_status_model->edit($data,$id);
                redirect('/scholarship/applicantStatus/list');
            }
            $data['edit'] = $this->applicant_status_model->get($id);
            $this->loadViews("applicant_status/edit", $this->global, $data, NULL);
        }
    }
}

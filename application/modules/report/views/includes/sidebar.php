            <div class="sidebar">
                <div class="user-profile clearfix">
                    <a href="#" class="user-profile-link">
                        <span><img src="<?php echo BASE_PATH; ?>assets/img/user_profile.jpg"></span> <?php echo $scholar_name; ?>
                    </a>
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                            <!-- <li><a href="/sponser/user/profile">Edit Profile</a></li> -->
                            <li><a href="/report/role/logout">Logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar-nav">
                     <h4>Report</h4>
                    <ul>
                        <li><a href="/report/configurableReport/add">Configurable Report</a></li>
                        <li><a href="/report/configurableReport/list">Finance  Report Status</a></li>
                         <li><a href="/report/configurableReport/bank">Bank Report</a></li>
                          <li><a href="/report/configurableReport/trainingcenter">Training Partner Report</a></li>
                    </ul>
                </div>
            </div>
<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List of training Partner</h3>
    </div>


    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Training Partner</th>
            <th>Scholarship</th>
            <th>Programme</th>
            <th>Total Student</th>
            <th>Total Amount (MYR)</th>
            <th>Period</th>
            <th>View Student</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Standford University</td>
            <td>Peneraju Profesional Data Scientist</td>
            <td>Financial Risk Manager </td>
            <td>3</td>
            <td>3,000</td>
                        <td>01-09-2020 to 30-09-2020</td>

            <td><a href="studentamount" class="btn btn-primary" style="color:white;">View Student </a></td>
          </tr>
          <tr>
            <td>2</td>
            <td>Standford University</td>
            <td>Peneraju Skil </td>
            <td>Chartered Financial Analyst</td>
            <td>2</td>
            <td>4,000</td>
                        <td>01-09-2020 to 30-09-2020</td>

            <td><a href="#" class="btn btn-primary" style="color:white;">View Student </a></td>
          </tr>
          <tr>
            <td>3</td>
            <td>System & Skills Training Concept Sdn Bhd</td>
            <td>Peneraju Profesional Data Scientist</td>
            <td>Chartered Financial Analyst</td>
            <td>1</td>
            <td>1,500</td>
                        <td>01-09-2020 to 30-09-2020</td>

            <td><a href="#" class="btn btn-primary" style="color:white;">View Student </a></td>
          </tr>
          <tr>
            <td>4</td>
            <td>System & Skills Training Concept Sdn Bhd</td>
            <td>Peneraju Skil </td>
            <td>Peneraju Skil Iltizam Fabrikasi Logam </td>
            <td>5</td>
            <td>2,500</td>
                        <td>01-09-2020 to 30-09-2020</td>

            <td><a href="#" class="btn btn-primary" style="color:white;">View Student </a></td>
          </tr>
          
         
        </tbody>
      </table>
    </div>

    
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  
    $('select').select2();

    function clearSearchForm()
    {
      window.location.reload();
    }
</script>
<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List of Finance report status</h3>
    </div>


    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Process</th>
            <th>Frequency</th>
            <th>Status</th>
            <th>Manual Update</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Account Code</td>
            <td>Every Day</td>
            <td>Last Updated on <br/>
            September 18th 2020</td>
            <td><a href="#" class="btn btn-primary" style="color:white;">Run </a></td>
          </tr>
          <tr>
            <td>2</td>
            <td>Scholar Allowance</td>
            <td>Monthly</td>
            <td>Last Updated on <br/>
            August 31st 2020</td>
            <td><a href="#" class="btn btn-primary" style="color:white;">Run </a></td>
          </tr>
          
         
        </tbody>
      </table>
    </div>

    
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  
    $('select').select2();

    function clearSearchForm()
    {
      window.location.reload();
    }
</script>
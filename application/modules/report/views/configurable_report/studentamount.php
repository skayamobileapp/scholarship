<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Bank Report for the month of September 2020</h3>
      <a href="trainingcenter" class="btn btn-link"> &lt; Back</a>
    </div>


    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Student name</th>
            <th>Student Ic number</th>
            
          </tr>

        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Testing One</td>
            <td>746637383</td>
           

          </tr>
          
           <tr>
            <td>2</td>
            <td>Testing Two</td>
            <td>48484747474</td>
           
          </tr>
           <tr>
            <td>3</td>
            <td>Abdul Razak</td>
            <td>8937704848</td>
           
          </tr>

       
         
        </tbody>
      </table>
    </div>

    
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  
    $('select').select2();

    function clearSearchForm()
    {
      window.location.reload();
    }
</script>
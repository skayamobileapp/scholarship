<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>Bank Report for the month of September 2020</h3>
            <a href="downloadbankreport" class="btn btn-link" target="_blank">Download</a>

    </div>


    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Bank Name</th>
            <th>Bank Code</th>
            <th>Purpose of Payment</th>

            <th>Amount</th>
            <th>Time Period</th>
            <th>Action</th>
          </tr>

        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>MayBank</td>
            <td>MB002</td>
            <td>Living Allowance</td>
            <td>15,000 </td>
            <td>01-09-2020 to 30-09-2020</td>
            <td><a href="student" class="btn btn-primary" style="color:white;">View Student</a></td>

          </tr>
          <tr>
            <td>2</td>
            <td>MayBank</td>
            <td>MB002</td>
            <td>Meal Allowance</td>
            <td>35,000 </td>
            <td>01-09-2020 to 30-09-2020</td>
            <td><a href="#" class="btn btn-primary" style="color:white;">View Student</a></td>

          </tr>
          <tr>
            <td>3</td>
            <td>MayBank</td>
            <td>MB002</td>
            <td>Transport Allowance</td>
            <td>800 </td>
            <td>01-09-2020 to 30-09-2020</td>
            <td><a href="#" class="btn btn-primary" style="color:white;">View Student</a></td>

          </tr>

       
         
        </tbody>
      </table>
    </div>

    
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  
    $('select').select2();

    function clearSearchForm()
    {
      window.location.reload();
    }
</script>
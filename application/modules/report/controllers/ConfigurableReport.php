<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ConfigurableReport extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isScholarLoggedIn();
    }

    function add()
    {
            $this->loadViews("configurable_report/add", $this->global, NULL, NULL);
        
    }


    function list()
    {
            $this->loadViews("configurable_report/list", $this->global, NULL, NULL);
        
    }


    function bank()
    {
            $this->loadViews("configurable_report/bank", $this->global, NULL, NULL);
        
    }

     function student()
    {
            $this->loadViews("configurable_report/student", $this->global, NULL, NULL);
        
    }



    function trainingcenter()
    {
            $this->loadViews("configurable_report/training_center", $this->global, NULL, NULL);
        
    }

    function studentamount()
    {
            $this->loadViews("configurable_report/studentamount", $this->global, NULL, NULL);
        
    }


    function downloadbankreport(){
          header("Content-type: application/csv");
            header("Content-Disposition: attachment; filename=" . date('Ymd') . "_CommunicationHistoryReport.csv");
            header("Pragma: no-cache");
            header("Expires: 0");

            $fp = fopen('php://output', 'w');

                $dtheader = array();

                           


                array_push($dtheader, "BANK NAME");
                array_push($dtheader, "BANK CODE");
                array_push($dtheader, "PURPOSE OF PAYMENT");
                array_push($dtheader, "AMOUNT");
                array_push($dtheader, "TIME PERIOD");
               

                fputcsv($fp, $dtheader);

                



                    $csvData = array();
                    array_push($csvData,'MayBank');
                    array_push($csvData,'MB002');
                    array_push($csvData,'Living Allowance');
                    array_push($csvData,'15,000');
                    array_push($csvData,'01-09-2020 to 30-09-2020');

                    fputcsv($fp, $csvData);


                     $csvData = array();
                    array_push($csvData,'MayBank');
                    array_push($csvData,'MB002');
                    array_push($csvData,'Meal Allowance ');
                    array_push($csvData,'35,000');
                    array_push($csvData,'01-09-2020 to 30-09-2020');
                    
                    fputcsv($fp, $csvData);



                     $csvData = array();
                    array_push($csvData,'MayBank');
                    array_push($csvData,'MB002');
                    array_push($csvData,'Transport Allowance');
                    array_push($csvData,'8,000');
                    array_push($csvData,'01-09-2020 to 30-09-2020');
                    
                    fputcsv($fp, $csvData);

               
        
            fclose($fp);
            exit;
    }




}

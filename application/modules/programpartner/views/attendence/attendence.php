<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">
            <div class="page-title clearfix">
                <h3>MArk Attendence</h3>
            </div>





    <div class="form-container">
            <h4 class="form-group-title"> Mark Attendence Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Mark Attendence</a>
                    </li>
                    <li role="presentation"><a href="#tab_department" class="nav-link border rounded text-center"
                            aria-controls="tab_department" role="tab" data-toggle="tab">Student Attendence Details</a>
                    </li>
                </ul>

                
                <div class="tab-content offers-tab-content">
                   <div role="tabpanel"  class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">
                            <form id="form_department" action="" method="post">
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-sm-4">
                                         <div class="form-group">
                                            <label>STudent NAme <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="code" name="code">
                                        </div>
                                    </div>
                                     <div class="col-sm-4">
                                         <div class="form-group">
                                            <label>STudent NRIC <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="code" name="code">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                         <div class="form-group">
                                                <button type="submit" class="btn btn-primary btn-lg" style="margin-top:15px;">Search</button>            </div>
                                    </div>


                                </div> 
                                <div class="row">
                                       <div class="custom-table">
                                          <table class="table"  width="98%">
                                            <thead>
                                              <tr>
                                                <th>Sl. No</th>
                                                <th>Student Name</th>
                                                <th>Student NRIC</th>
                                                <th>Attendence Date</th>
                                                <th>Attendence Status</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>First user</td>
                                                    <td>6753839837</td>
                                                    <td><input type='text' class="form-control datepicker"></td>
                                                    <td><input type='radio' name='status' id='status' />Present <br/>
<input type='radio' name='status' id='status' />Absent
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Second User</td>
                                                    <td>9735335367</td>

                                                    <td><input type='text' class="form-control datepicker"></td>
 <td><input type='radio' name='status' id='status' />Present <br/>
<input type='radio' name='status' id='status' />Absent
                                                    </td>                                                </tr>
                                              
                                            </tbody>
                                          </table>
                                        </div>
                                </div> 

                            </form>
                        </div>


                                       

                    </div>

                    <div role="tabpanel" class="tab-pane" id="tab_department">
                        <div class="col-12 mt-4">
                                <div class="row">
                                       <div class="custom-table">
                                          <table class="table"  width="98%">
                                            <thead>
                                              <tr>
                                                <th>Sl. No</th>
                                                <th>Student Name</th>
                                                <th>Attendence Date</th>
                                                <th>Attendence Status</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Testing One</td>
                                                    <td>2020-09-13</td>
                                                    <td>Present</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Testing Two</td>
                                                    <td>2020-09-13</td>
                                                    <td>Absent</td>
                                                </tr>
                                              
                                            </tbody>
                                          </table>
                                        </div>
                                </div> 
                        </div>
                    </div>

            </div>
        </div>


    

        
        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>

    </div>
</div>


<script type="text/javascript">
    $('select').select2();
</script>

<script>
  $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
  } );
</script>
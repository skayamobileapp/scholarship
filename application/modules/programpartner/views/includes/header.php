<!DOCTYPE html>
<html lang="en"> 
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $pageTitle; ?></title>
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/css/datatable.min.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/select2/css/select2.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH; ?>assets/select2/css/select2-bootstrap.min.css">

    
</head>
<body>
     <header class="navbar navbar-default navbar-fixed-top main-header">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">SCHOLARSHIP MANAGEMENT SYSTEM</a>
            </div>

            <nav class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/setup/role/list">SYSTEM SETUP</a></li>
                    <li><a href="/program/moduleType/list">programme</a></li>
                    <li class="active"><a href="/programpartner/partnerUniversity/list">programme partner</a></li>
                    <li><a href="/scholarship/educationLevel/list">scholarship</a></li>
                    <li><a href="/record/studentRecord/list">scholars record</a></li>
                    <li><a href="/scholarship/role/list">alumni records</a></li>
                    <li><a href="/finance/bankRegistration/list">Finance</a></li>
                    <li><a href="/communication/template/list">Communication</a></li>
                    <li><a href="/report/configurableReport/add">Reports</a></li>
                </ul>
                
            </nav>
        </div>
    </header>
</body>

     
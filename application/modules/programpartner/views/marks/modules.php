<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Modules</h3>
    </div>



    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Name</th>
            <th>Code</th>
            <th>Credit_hours</th>
            <th>Course Type</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($programSyllabusList))
          {
            $i=1;
            foreach ($programSyllabusList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->code ?></td>
                <td><?php echo $record->credit_hours ?></td>
                <td><?php echo $record->type ?></td>
              
                <td class="text-center">
                  <a href="<?php echo '../attendence/' . $record->id; ?>" title="Edit">Add Marks</a>
                  <!--  -->
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  
  $('select').select2();


  function clearSearchForm()
      {
        window.location.reload();
      }
</script>
<div class="container-fluid page-wrapper">

  <div class="main-container clearfix">
    <div class="page-title clearfix">
      <h3>List Program Partner</h3>
      <a href="add" class="btn btn-primary">+ Add Program Partner</a>
    </div>

    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Advanced Search
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">
          <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
              <div class="form-horizontal">


                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                    <label class="col-sm-4 control-label">Thrust</label>
                    <div class="col-sm-8">
                      <select name="id_thrust" id="id_thrust" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($thrustList)) {
                          foreach ($thrustList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_thrust']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                    <label class="col-sm-4 control-label">Sub-Thrust</label>
                    <div class="col-sm-8">
                      <select name="id_sub_thrust" id="id_sub_thrust" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($subThrustList)) {
                          foreach ($subThrustList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_sub_thrust']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->scholarship_code ."-".$record->scholarship_name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>


                </div>

                <div class="row">

                  <div class="col-sm-6">
                    <div class="form-group">
                    <label class="col-sm-4 control-label">Program</label>
                    <div class="col-sm-8">
                      <select name="id_program" id="id_program" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($programList)) {
                          foreach ($programList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_program']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                    <label class="col-sm-4 control-label">Partner</label>
                    <div class="col-sm-8">
                      <select name="id_partner_university" id="id_partner_university" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($partnerUniversityList)) {
                          foreach ($partnerUniversityList as $record)
                          {
                            $selected = '';
                            if ($record->id == $searchParam['id_partner_university']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>"
                              <?php echo $selected;  ?>>
                              <?php echo  $record->code ."-".$record->name;  ?>
                              </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                    </div>
                  </div>


                </div>


              </div>
              <div class="app-btn-group">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Program</th>
            <th>Thrust</th>
            <th>Sub-Thrust</th>
            <th>Partner</th>
            <th>Location</th>
            <th class="text-center">Status</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($programPartnerList))
          {
            $i=1;
            foreach ($programPartnerList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->program_code . " - " . $record->program_name ?></td>
                <td><?php echo $record->thrust_code . " - " . $record->thrust_name ?></td>
                <td><?php echo $record->scholarship_code . " - " . $record->scholarship_name ?></td>
                <td><?php echo $record->partner_code . " - " . $record->partner_name ?></td>
                <td><?php echo $record->location ?></td>
                <td class="text-center"><?php if( $record->status == '1')
                {
                  echo "Active";
                }
                else
                {
                  echo "In-Active";
                } 
                ?></td>
                <!-- <td><?php echo date("d-m-Y", strtotime($record->created_dt_tm)) ?></td> -->
                <td class="text-center">
                  <a href="<?php echo 'edit/' . $record->id; ?>" title="Edit">Edit</a>
                  <!--  -->
                </td>
              </tr>
          <?php
          $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <footer class="footer-wrapper">
    <p>&copy; 2019 All rights, reserved</p>
  </footer>
</div>
<script>
  
  $('select').select2();


  function clearSearchForm()
      {
        window.location.reload();
      }
</script>
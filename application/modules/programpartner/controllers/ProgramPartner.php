<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ProgramPartner extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('program_partner_model');
        $this->isScholarLoggedIn();
    }

    function list()
    {
        if ($this->checkScholarAccess('program_partner.list') == 1)
        // if ($this->checkScholarAccess('program_partner.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['id_program'] = $this->security->xss_clean($this->input->post('id_program'));
            $formData['id_thrust'] = $this->security->xss_clean($this->input->post('id_thrust'));
            $formData['id_sub_thrust'] = $this->security->xss_clean($this->input->post('id_sub_thrust'));
            $formData['id_partner_university'] = $this->security->xss_clean($this->input->post('id_partner_university'));
            $formData['id_location'] = $this->security->xss_clean($this->input->post('id_location'));
 
            $data['searchParam'] = $formData;

            $data['programPartnerList'] = $this->program_partner_model->programPartnerList($formData);

            $data['stateList'] = $this->program_partner_model->stateListByStatus('1');
            $data['thrustList'] = $this->program_partner_model->thrustListByStatus('1');
            $data['subThrustList'] = $this->program_partner_model->subThrustListByStatus('1');
            $data['programList'] = $this->program_partner_model->programListByStatus('1');
            $data['partnerUniversityList'] = $this->program_partner_model->partnerListByStatus('1');



            $this->global['pageTitle'] = 'Campus Management System : Program Partner List';
            $this->loadViews("program_partner/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkScholarAccess('program_partner.add') == 1)
        // if ($this->checkScholarAccess('program_partner.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_scholar_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $formData = $this->input->post();

               // echo "<Pre>"; print_r($formData);exit;
            	

                $id_thrust = $this->security->xss_clean($this->input->post('id_thrust'));
                $id_sub_thrust = $this->security->xss_clean($this->input->post('id_sub_thrust'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));
                $id_location = $this->security->xss_clean($this->input->post('id_location'));
                $internship = $this->security->xss_clean($this->input->post('internship'));
                $apprenticeship = $this->security->xss_clean($this->input->post('apprenticeship'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                $data = array(
					'id_thrust' => $id_thrust,
					'id_sub_thrust' => $id_sub_thrust,
					'id_program' => $id_program,
					'id_partner_university' => $id_partner_university,
					'id_location' => $id_location,
                    'internship' => $internship,
                    'apprenticeship' => $apprenticeship,
					'status' => $status,
					'created_by' => $user_id
                );

                $inserted_id = $this->program_partner_model->addNewProgramPartner($data);
                redirect('/program/programPartner/list');
            }
            $data['stateList'] = $this->program_partner_model->stateListByStatus('1');
            $data['thrustList'] = $this->program_partner_model->thrustListByStatus('1');
            $data['programList'] = $this->program_partner_model->programListByStatus('1');
            // $data['accountCodeList'] = $this->program_partner_model->getAccountCodeList();
               // echo "<Pre>"; print_r($data);exit;

            
            $this->global['pageTitle'] = 'Campus Management System : Add Program Partner';
            $this->loadViews("program_partner/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkScholarAccess('program_partner.edit') == 1)
        // if ($this->checkScholarAccess('program_partner.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/program/programPartner/list');
            }
            if($this->input->post())
            {
                $formData = $this->input->post();

               // echo "<Pre>"; print_r($formData);exit;

	            $id_session = $this->session->my_scholar_session_id;
                $user_id = $this->session->userId;

                $id_thrust = $this->security->xss_clean($this->input->post('id_thrust'));
                $id_sub_thrust = $this->security->xss_clean($this->input->post('id_sub_thrust'));
                $id_program = $this->security->xss_clean($this->input->post('id_program'));
                $id_partner_university = $this->security->xss_clean($this->input->post('id_partner_university'));
                $id_location = $this->security->xss_clean($this->input->post('id_location'));
                $internship = $this->security->xss_clean($this->input->post('internship'));
                $apprenticeship = $this->security->xss_clean($this->input->post('apprenticeship'));
                $status = $this->security->xss_clean($this->input->post('status'));
                
                $data = array(
                    'id_thrust' => $id_thrust,
                    'id_sub_thrust' => $id_sub_thrust,
                    'id_program' => $id_program,
                    'id_partner_university' => $id_partner_university,
                    'id_location' => $id_location,
                    'internship' => $internship,
                    'apprenticeship' => $apprenticeship,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                //print_r($data);exit;
                $result = $this->program_partner_model->editProgramPartner($data,$id);
                redirect('/program/programPartner/list');
            }
            // $data['studentList'] = $this->program_partner_model->studentList();
            $data['programPartner'] = $this->program_partner_model->getProgramPartner($id);
            $data['stateList'] = $this->program_partner_model->stateListByStatus('1');
            $data['thrustList'] = $this->program_partner_model->thrustListByStatus('1');
            $data['subThrustList'] = $this->program_partner_model->subThrustListByStatus('1');
            $data['programList'] = $this->program_partner_model->programListByStatus('1');
            $data['partnerUniversityList'] = $this->program_partner_model->partnerListByStatus('1');
            
               // echo "<Pre>"; print_r($data);exit;

            $this->global['pageTitle'] = 'Campus Management System : Edit Program Partner';
            $this->loadViews("program_partner/edit", $this->global, $data, NULL);
        }
    }

    function getSubThrustByThrustId($id_thrust)
    {
        $results = $this->program_partner_model->getSubThrustByThrustId($id_thrust);

        // echo "<Pre>"; print_r($programme_data);exit;
     //    $table="   
     //        <script type='text/javascript'>
     //             $('select').select2();
     //         </script>
     // ";

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_sub_thrust' id='id_sub_thrust' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->scholarship_name;
        $code = $results[$i]->scholarship_code;
        $table.="<option value=".$id.">" . $code . " - " . $name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }

    function getPartnerProgram($id)
    {
            // echo "<Pre>"; print_r($id);exit;
         $results = $this->program_partner_model->getPartnerProgram($id);

            // echo "<Pre>"; print_r($results);exit;
            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>";

            $table.="
            <select name='id_partner_university' id='id_partner_university' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $partner_code = $results[$i]->partner_code;
            $partner_name = $results[$i]->partner_name;
            $table.="<option value=".$id.">".$partner_code . " - " . $partner_name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }
}

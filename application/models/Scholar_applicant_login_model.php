<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login_model (Login Model)
 * Login model class to get to authenticate user credentials 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Scholar_applicant_login_model extends CI_Model
{
    
    function loginScholarApplicant($email, $password)
    {
        $this->db->select('stu.id as id_scholar_applicant, stu.full_name as scholar_applicant_name, stu.email_id, stu.password, stu.email_verified, stu.applicant_status');
        $this->db->from('scholar_applicant as stu');
        $this->db->where('stu.email_id', $email);
        // $this->db->where('stu.email_verified', '1');
        // $this->db->where('stu.isDeleted', 0);
        $query = $this->db->get();
        
        $user = $query->row();
        
        if(!empty($user))
        {
        // echo "<Pre>";print_r($user->password);exit();
            if(md5($password) == $user->password)
            {
        // echo "<Pre>";print_r($user);exit();
                return $user;
            }else
            {
                return array();
            }
        }
        else
        {
            return array();
        }
    }

    function scholarApplicantLastLoginInfo($id_scholar_applicant)
    {
        $this->db->select('BaseTbl.created_dt_tm');
        $this->db->where('BaseTbl.id_scholar_applicant', $id_scholar_applicant);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('scholar_applicant_last_login as BaseTbl');

        return $query->row();
    }


    function checkScholarApplicantEmailExist($email)
    {
        $this->db->select('userId');
        $this->db->where('email', $email);
        $query = $this->db->get('scholar_applicant');

        if ($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    function resetPasswordUser($data)
    {
        $result = $this->db->insert('tbl_reset_password', $data);

        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function getScholarApplicantInfoByEmail($email)
    {
        $this->db->select('id, email_id, full_name');
        $this->db->from('scholar_applicant');
        $this->db->where('email_id', $email);
        $query = $this->db->get();

        return $query->row();
    }

    function checkActivationDetails($email, $activation_id)
    {
        $this->db->select('id');
        $this->db->from('tbl_reset_password');
        $this->db->where('email', $email);
        $this->db->where('activation_id', $activation_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function createPasswordUser($email, $password)
    {
        $this->db->where('email', $email);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_users', array('password'=>getHashedPassword($password)));
        $this->db->delete('tbl_reset_password', array('email'=>$email));
    }

    function addScholarApplicantLastLogin($loginInfo)
    {
        $this->db->trans_start();
        $this->db->insert('scholar_applicant_last_login', $loginInfo);
        $this->db->trans_complete();
    }

    function addNewScholarApplicantRegistration($data)
    {
        $this->db->trans_start();
        $this->db->insert('scholar_applicant', $data);
        $insert_id = $this->db->insert_id();
        $details['id_scholar_applicant'] = $insert_id;
        $this->db->insert('scholar_applicant_family_details', $details);
        $this->db->trans_complete();
        return $insert_id;
    }

    function checkDuplicateScholarApplicantRegistration($data)
    {
        $this->db->select('id, email_id, full_name');
        $this->db->from('scholar_applicant');
        $this->db->where('email_id', $data['email_id']);
        $this->db->or_where('phone', $data['phone']);
        $this->db->or_where('nric', $data['nric']);
        $query = $this->db->get();
        return $query->row();
    }

    function programmeListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('scholarship_programme');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }
}

?>
<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Partner_university_login_model extends CI_Model
{
    
    function loginPartnerUniversity($email, $password)
    {
        $this->db->select('stu.*');
        $this->db->from('partner_university as stu');
        $this->db->where('stu.login_id', $email);
        // $this->db->where('stu.applicant_status', 'Approved');
        // $this->db->where('stu.isDeleted', 0);
        $query = $this->db->get();
        
        $user = $query->row();

        if($user)
        {
            if($user->password == md5($password))
            {
                return $user;
            }
            else
            {
                return array();
            }
        }
    
       return array();
    }

    function partnerUniversityLastLoginInfo($id_partner_university)
    {
        $this->db->select('pll.created_dt_tm');
        $this->db->where('pll.id_partner_university', $id_partner_university);
        $this->db->order_by('pll.id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('partner_university_last_login as pll');

        return $query->row();
    }


    function checkPartnerUniversityUserExist($user_id)
    {
        $this->db->select('id');
        $this->db->where('login_id', $user_id);
        $this->db->where('status', 1);
        $query = $this->db->get('partner_university');

        if ($query->num_rows() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function resetPasswordUser($data)
    {
        $result = $this->db->insert('tbl_reset_password', $data);

        if($result)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function getPartnerUniversityInfoByEmail($email)
    {
        $this->db->select('id, user_id, name');
        $this->db->from('partner_university');
        // $this->db->where('isDeleted', 0);
        $this->db->where('user_id', $email);
        $query = $this->db->get();

        return $query->row();
    }

    function addPartnerUniversityLastLogin($loginInfo)
    {
        $this->db->trans_start();
        $this->db->insert('partner_university_last_login', $loginInfo);
        $this->db->trans_complete();
    }
}
?>
-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 20, 2020 at 12:08 PM
-- Server version: 10.2.32-MariaDB-log
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `camsedu_college`
--

-- --------------------------------------------------------

--
-- Table structure for table `academic_year`
--

CREATE TABLE `academic_year` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `from_year` varchar(20) DEFAULT '',
  `to_year` varchar(20) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `academic_year`
--

INSERT INTO `academic_year` (`id`, `name`, `from_year`, `to_year`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, '2020', '2020', '2020', 1, 1, '2020-07-10 23:37:30', NULL, '2020-07-10 23:37:30');

-- --------------------------------------------------------

--
-- Table structure for table `account_code`
--

CREATE TABLE `account_code` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `level` int(2) DEFAULT 0,
  `id_parent` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_code`
--

INSERT INTO `account_code` (`id`, `name`, `name_optional_language`, `code`, `level`, `id_parent`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'C', 'C', 'CC01', 0, 0, 1, NULL, '2020-02-24 02:31:57', NULL, '2020-02-24 02:31:57'),
(2, '1 - Level 1', '', '1L1', 1, 0, NULL, NULL, '2020-05-22 11:19:51', NULL, '2020-05-22 11:19:51'),
(3, '2- Level 1', '', '2L1', 1, 0, NULL, NULL, '2020-05-22 11:20:14', NULL, '2020-05-22 11:20:14'),
(4, '3-Level1', '', '3L1', 1, 0, NULL, NULL, '2020-05-22 11:20:32', NULL, '2020-05-22 11:20:32'),
(5, '1-Level2', '', '1L11L11', 2, 2, NULL, NULL, '2020-05-22 11:21:34', NULL, '2020-05-22 11:21:34'),
(6, '1-Level2', '', '1L11L12', 2, 2, NULL, NULL, '2020-05-22 11:21:55', NULL, '2020-05-22 11:21:55'),
(7, 'AC', '', '1L11L11AC`', 3, 5, NULL, NULL, '2020-05-22 11:22:21', NULL, '2020-05-22 11:22:21'),
(8, 'AC', '', '1L11L122AC', 3, 6, NULL, NULL, '2020-05-22 11:22:34', NULL, '2020-05-22 11:22:34'),
(9, '1- Level 2 ', '', '2L12L11', 2, 3, NULL, NULL, '2020-05-22 11:23:11', NULL, '2020-05-22 11:23:11'),
(10, 'AC', '', '2L12L11AC', 3, 9, NULL, NULL, '2020-05-22 11:23:29', NULL, '2020-05-22 11:23:29'),
(11, '3L2v2l 1', '', '3L13L11', 2, 4, NULL, NULL, '2020-05-22 11:24:00', NULL, '2020-05-22 11:24:00'),
(12, 'AC 3', '', '3L13L11AC', 3, 11, NULL, NULL, '2020-05-22 11:24:15', NULL, '2020-05-22 11:24:15');

-- --------------------------------------------------------

--
-- Table structure for table `activity_code`
--

CREATE TABLE `activity_code` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `level` int(2) DEFAULT 0,
  `id_parent` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `activity_details`
--

CREATE TABLE `activity_details` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `description` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_details`
--

INSERT INTO `activity_details` (`id`, `name`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Act', 'Act', 1, 1, '2020-08-17 14:30:13', NULL, '2020-08-17 14:30:13');

-- --------------------------------------------------------

--
-- Table structure for table `add_course_to_program_landscape`
--

CREATE TABLE `add_course_to_program_landscape` (
  `id` int(20) NOT NULL,
  `id_program_landscape` int(20) DEFAULT NULL,
  `id_semester` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT 0,
  `id_qualification` int(20) DEFAULT 0,
  `id_intake` int(10) DEFAULT NULL,
  `id_program_major` int(20) DEFAULT 0,
  `id_program_minor` int(20) DEFAULT 0,
  `pre_requisite` varchar(50) DEFAULT '',
  `course_type` varchar(120) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `add_course_to_program_landscape`
--

INSERT INTO `add_course_to_program_landscape` (`id`, `id_program_landscape`, `id_semester`, `id_course`, `id_program`, `id_qualification`, `id_intake`, `id_program_major`, `id_program_minor`, `pre_requisite`, `course_type`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(5, 1, 0, 4, 1, 0, 1, 0, 0, 'No', 'Compulsory', NULL, NULL, '2020-07-15 03:30:24', NULL, '2020-07-15 03:30:24'),
(6, 1, 0, 1, 1, 0, 1, 0, 0, '', 'Compulsory', NULL, NULL, '2020-07-19 00:30:40', NULL, '2020-07-19 00:30:40'),
(10, 1, 1, 3, 1, 0, 1, 0, 0, '', 'Compulsory', NULL, NULL, '2020-08-10 01:37:30', NULL, '2020-08-10 01:37:30'),
(11, 1, 1, 2, 1, 0, 1, 1, 0, '', 'Major', NULL, NULL, '2020-08-10 14:22:45', NULL, '2020-08-10 14:22:45'),
(12, 5, 1, 6, 3, 0, 1, 0, 0, '', 'Compulsory', NULL, NULL, '2020-08-20 11:31:26', NULL, '2020-08-20 11:31:26'),
(13, 5, 1, 1, 3, 0, 1, 0, 0, '', 'Compulsory', NULL, NULL, '2020-08-20 11:31:42', NULL, '2020-08-20 11:31:42'),
(14, 5, 1, 2, 3, 0, 1, 0, 0, '', 'Compulsory', NULL, NULL, '2020-08-20 11:31:58', NULL, '2020-08-20 11:31:58'),
(15, 5, 1, 3, 3, 0, 1, 0, 0, '', 'Compulsory', NULL, NULL, '2020-08-20 11:32:05', NULL, '2020-08-20 11:32:05'),
(16, 5, 1, 8, 3, 0, 1, 0, 0, '', 'Compulsory', NULL, NULL, '2020-08-20 11:32:17', NULL, '2020-08-20 11:32:17'),
(17, 5, 1, 7, 3, 0, 1, 0, 0, '', 'Compulsory', NULL, NULL, '2020-08-20 11:32:28', NULL, '2020-08-20 11:32:28'),
(18, 5, 1, 5, 3, 0, 1, 0, 0, '', 'Compulsory', NULL, NULL, '2020-08-20 11:32:42', NULL, '2020-08-20 11:32:42'),
(19, 5, 1, 4, 3, 0, 1, 0, 0, '', 'Compulsory', NULL, NULL, '2020-08-20 11:32:51', NULL, '2020-08-20 11:32:51'),
(20, 5, 1, 9, 3, 0, 1, 0, 0, '', 'Compulsory', NULL, NULL, '2020-08-20 11:33:00', NULL, '2020-08-20 11:33:00');

-- --------------------------------------------------------

--
-- Table structure for table `add_temp_receipt_paid_amount`
--

CREATE TABLE `add_temp_receipt_paid_amount` (
  `id` int(20) NOT NULL,
  `id_session` varchar(580) DEFAULT '',
  `payable_amount` float(20,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `advisor_tagging`
--

CREATE TABLE `advisor_tagging` (
  `id` int(20) NOT NULL,
  `id_advisor` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `advisor_tagging`
--

INSERT INTO `advisor_tagging` (`id`, `id_advisor`, `id_student`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 4, NULL, 1, '2020-08-06 09:26:54', NULL, '2020-08-06 09:26:54'),
(2, 2, 4, NULL, 1, '2020-08-06 09:27:09', NULL, '2020-08-06 09:27:09'),
(3, 1, 5, NULL, 1, '2020-08-07 14:26:58', NULL, '2020-08-07 14:26:58'),
(4, 2, 6, NULL, 1, '2020-08-10 13:15:39', NULL, '2020-08-10 13:15:39'),
(5, 1, 7, NULL, 1, '2020-08-10 14:31:51', NULL, '2020-08-10 14:31:51');

-- --------------------------------------------------------

--
-- Table structure for table `alumni_discount`
--

CREATE TABLE `alumni_discount` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `currency` varchar(200) DEFAULT '',
  `amount` varchar(520) DEFAULT '',
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `sibbling_status` varchar(50) DEFAULT 'Pending',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alumni_discount`
--

INSERT INTO `alumni_discount` (`id`, `name`, `currency`, `amount`, `start_date`, `end_date`, `sibbling_status`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Amount', 'MYR', '100', '2020-08-01 00:00:00', '2020-08-31 00:00:00', 'Pending', 1, NULL, '2020-08-17 12:15:17', NULL, '2020-08-17 12:15:17'),
(2, 'Alumni', 'USD', '18', '2020-08-01 00:00:00', '2020-08-31 00:00:00', 'Pending', 1, NULL, '2020-08-20 04:33:38', NULL, '2020-08-20 04:33:38');

-- --------------------------------------------------------

--
-- Table structure for table `amount_calculation_type`
--

CREATE TABLE `amount_calculation_type` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `amount_calculation_type`
--

INSERT INTO `amount_calculation_type` (`id`, `name`, `name_optional_language`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'FIX AMOUNT', 'FIX AMOUNT', 'FIX AMOUNT', 1, NULL, '2020-07-11 00:24:37', NULL, '2020-07-11 00:24:37'),
(2, 'CREDIT HOUR MULTIPLICATION', 'CREDIT HOUR MULTIPLICATION', 'CREDIT HOUR MULTIPLICATION', 1, NULL, '2020-07-11 00:24:55', NULL, '2020-07-11 00:24:55'),
(3, 'SUBJECT MULTIPLICATION', 'SUBJECT MULTIPLICATION', 'SUBJECT MULTIPLICATION', 1, NULL, '2020-08-20 09:17:42', NULL, '2020-08-20 09:17:42'),
(4, 'REGISTRATION', 'REGISTRATION', 'REGISTRATION', 1, NULL, '2020-08-20 09:17:53', NULL, '2020-08-20 09:17:53');

-- --------------------------------------------------------

--
-- Table structure for table `apex_status`
--

CREATE TABLE `apex_status` (
  `id` int(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `apex_status`
--

INSERT INTO `apex_status` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'REGISTERED OAT', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(2, 'COMPLETED OAT', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(3, 'PASS OAT', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(4, 'FAILED OAT', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(5, 'COMPLETED FORTPOLIO', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(6, 'COMPLETED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(7, 'RELEASED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(8, 'CERTIFIED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45');

-- --------------------------------------------------------

--
-- Table structure for table `applicant`
--

CREATE TABLE `applicant` (
  `id` int(20) NOT NULL,
  `id_intake` int(11) DEFAULT NULL,
  `id_program` int(11) DEFAULT NULL,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `passport` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `contact_email` varchar(500) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `is_hostel` int(20) DEFAULT 0,
  `id_degree_type` int(20) DEFAULT 0,
  `id_race` varchar(120) DEFAULT '',
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `program_scheme` varchar(200) DEFAULT '',
  `id_program_scheme` int(20) DEFAULT 0,
  `id_program_requirement` int(20) DEFAULT 0,
  `id_branch` int(20) DEFAULT 0,
  `mailing_zipcode` varchar(120) DEFAULT '',
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `sibbling_discount` varchar(50) DEFAULT '',
  `employee_discount` varchar(50) DEFAULT '',
  `alumni_discount` varchar(50) DEFAULT '',
  `applicant_status` varchar(50) DEFAULT 'Draft',
  `is_sibbling_discount` int(20) DEFAULT 3,
  `is_employee_discount` int(20) DEFAULT 3,
  `is_alumni_discount` int(2) DEFAULT 3,
  `is_apeal_applied` int(2) DEFAULT 3,
  `id_apeal_status` int(20) DEFAULT 0,
  `apel_reject_reason` varchar(2048) DEFAULT '',
  `pathway` varchar(200) DEFAULT '',
  `email_verified` int(2) DEFAULT 0,
  `is_submitted` int(2) DEFAULT 0,
  `submitted_date` datetime DEFAULT NULL,
  `is_updated` int(2) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `approved_by` int(20) DEFAULT 0,
  `approved_dt_tm` datetime DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicant`
--

INSERT INTO `applicant` (`id`, `id_intake`, `id_program`, `full_name`, `salutation`, `first_name`, `last_name`, `nric`, `passport`, `phone`, `email_id`, `contact_email`, `password`, `passport_expiry_date`, `gender`, `date_of_birth`, `martial_status`, `religion`, `nationality`, `is_hostel`, `id_degree_type`, `id_race`, `mail_address1`, `mail_address2`, `mailing_country`, `mailing_state`, `mailing_city`, `program_scheme`, `id_program_scheme`, `id_program_requirement`, `id_branch`, `mailing_zipcode`, `permanent_address1`, `permanent_address2`, `permanent_country`, `permanent_state`, `permanent_city`, `permanent_zipcode`, `sibbling_discount`, `employee_discount`, `alumni_discount`, `applicant_status`, `is_sibbling_discount`, `is_employee_discount`, `is_alumni_discount`, `is_apeal_applied`, `id_apeal_status`, `apel_reject_reason`, `pathway`, `email_verified`, `is_submitted`, `submitted_date`, `is_updated`, `reason`, `status`, `approved_by`, `approved_dt_tm`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(28, 1, 3, 'DATIN. Student One', '2', 'Student', 'One', '1234', '', '1234', 's1@cms.com', '', '202cb962ac59075b964b07152d234b70', '', 'Male', '2000-08-08', 'Single', '2', 'Malaysian', 1, 1, '1', 'KL', 'KL', 1, 1, 'KL', 'Blended - Part Time', 8, 1, 3, '123445', 'KL', 'KL', 1, 1, 'KL', '12344', '', '', '', 'Approved', 3, 3, 3, 1, 8, '', 'APEL', 1, 1, '2020-08-20 19:38:01', 1, '', NULL, 1, NULL, NULL, '2020-08-20 07:03:42', 1, '2020-08-20 07:03:42'),
(29, 1, 3, 'AG. Student Two', '1', 'Student', 'Two', '99887765', '', '98712388', 's2@cms.com', '', '202cb962ac59075b964b07152d234b70', '', 'Male', '2020-08-01', 'Single', '', 'Malaysian', 1, 1, '2', 'KL', 'KL', 1, 1, 'KL', 'Blended - Part Time', 8, 1, 3, '123456', 'KL', 'KL', 1, 1, 'KL', '123456', '', '', '', 'Approved', 3, 3, 3, 1, 8, '', 'APEL', 1, 1, '2020-08-21 00:26:37', 1, '', NULL, 1, NULL, NULL, '2020-08-20 11:53:59', 1, '2020-08-20 11:53:59');

-- --------------------------------------------------------

--
-- Table structure for table `applicant_has_alumni_discount`
--

CREATE TABLE `applicant_has_alumni_discount` (
  `id` int(20) NOT NULL,
  `id_applicant` int(20) DEFAULT NULL,
  `alumni_name` varchar(520) DEFAULT '',
  `alumni_nric` varchar(520) DEFAULT '',
  `alumni_email` varchar(520) DEFAULT '',
  `alumni_status` varchar(50) DEFAULT 'Pending',
  `reason` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `rejected_by` int(20) DEFAULT 0,
  `rejected_on` datetime DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_has_document`
--

CREATE TABLE `applicant_has_document` (
  `id` int(20) NOT NULL,
  `id_applicant` int(20) DEFAULT NULL,
  `id_document` varchar(520) DEFAULT '',
  `file_name` varchar(520) DEFAULT '',
  `reason` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicant_has_document`
--

INSERT INTO `applicant_has_document` (`id`, `id_applicant`, `id_document`, `file_name`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 26, '2', '7 days lock down order.pdf', '', NULL, NULL, '2020-08-19 22:01:31', NULL, '2020-08-19 22:01:31');

-- --------------------------------------------------------

--
-- Table structure for table `applicant_has_employee_discount`
--

CREATE TABLE `applicant_has_employee_discount` (
  `id` int(20) NOT NULL,
  `id_applicant` int(20) DEFAULT NULL,
  `employee_name` varchar(520) DEFAULT '',
  `employee_nric` varchar(520) DEFAULT '',
  `employee_designation` varchar(520) DEFAULT '',
  `employee_status` varchar(50) DEFAULT 'Pending',
  `reason` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `rejected_by` int(20) DEFAULT 0,
  `rejected_on` datetime DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_has_sibbling_discount`
--

CREATE TABLE `applicant_has_sibbling_discount` (
  `id` int(20) NOT NULL,
  `id_applicant` int(20) DEFAULT NULL,
  `sibbling_name` varchar(520) DEFAULT '',
  `sibbling_nric` varchar(520) DEFAULT '',
  `sibbling_status` varchar(50) DEFAULT 'Pending',
  `reason` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `rejected_by` int(20) DEFAULT 0,
  `rejected_on` datetime DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `applicant_last_login`
--

CREATE TABLE `applicant_last_login` (
  `id` bigint(20) NOT NULL,
  `id_applicant` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `applicant_last_login`
--

INSERT INTO `applicant_last_login` (`id`, `id_applicant`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(46, 24, '{\"applicant_name\":\"Mr. Kiran kumar\",\"email_id\":\"a@a.com\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-19 09:18:51'),
(47, 25, '{\"applicant_name\":\"Mr. kir as\",\"email_id\":\"n@n.com\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-19 21:01:45'),
(48, 26, '{\"applicant_name\":\"Miss. yati baizur\",\"email_id\":\"yati@oum.com\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-19 21:56:25'),
(49, 27, '{\"applicant_name\":\"Mr. Albert Einstein\",\"email_id\":\"alb@cms.com\"}', '157.49.93.131', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-20 04:26:39'),
(50, 28, '{\"applicant_name\":\"Mr. Student One\",\"email_id\":\"s1@cms.com\"}', '157.49.93.131', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-20 07:03:56'),
(51, 29, '{\"applicant_name\":\"Mr. Student Two\",\"email_id\":\"s2@cms.com\"}', '157.49.65.159', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-20 11:54:32');

-- --------------------------------------------------------

--
-- Table structure for table `applicant_status`
--

CREATE TABLE `applicant_status` (
  `id` int(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicant_status`
--

INSERT INTO `applicant_status` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'NEW/LEAD', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(2, 'SUBMITTED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(3, 'INCOMPLETE', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(4, 'NOTQUALIFIED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(5, 'OFFERED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(6, 'OFFERED CONDITIONALLY', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(7, 'REGISTERED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(8, 'REGISTERED CONDITIONALLY', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(9, 'COMPLETED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(10, 'GRADUATED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(11, 'DESEASED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(12, 'WITHDRAW', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45'),
(13, 'DISMISSED/SUSPENDED', '', '', 0, NULL, '2020-08-18 12:37:45', NULL, '2020-08-18 12:37:45');

-- --------------------------------------------------------

--
-- Table structure for table `apply_change_programme`
--

CREATE TABLE `apply_change_programme` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT NULL,
  `reason` varchar(580) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `id_intake` int(20) DEFAULT NULL,
  `id_new_programme` int(20) DEFAULT NULL,
  `id_new_intake` int(20) DEFAULT NULL,
  `id_new_semester` int(20) DEFAULT NULL,
  `id_new_program_scheme` int(20) DEFAULT 0,
  `fee` float(20,2) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `apply_change_programme`
--

INSERT INTO `apply_change_programme` (`id`, `id_student`, `reason`, `id_programme`, `id_intake`, `id_new_programme`, `id_new_intake`, `id_new_semester`, `id_new_program_scheme`, `fee`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 5, 'Not Interested', 1, 1, 2, 1, 1, 7, 9639.00, 1, 1, '2020-08-17 12:37:44', NULL, '2020-08-17 12:37:44'),
(2, 11, 'Chaning', 2, 1, 1, 1, 1, 4, 400.00, 1, 1, '2020-08-20 06:01:26', NULL, '2020-08-20 06:01:26');

-- --------------------------------------------------------

--
-- Table structure for table `apply_change_scheme`
--

CREATE TABLE `apply_change_scheme` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT NULL,
  `reason` varchar(580) DEFAULT '',
  `id_intake` int(20) DEFAULT 0,
  `id_programme` int(20) DEFAULT 0,
  `id_program_scheme` int(20) DEFAULT 0,
  `id_new_semester` int(20) DEFAULT 0,
  `id_new_program_scheme` int(20) DEFAULT 0,
  `fee` float(20,2) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `apply_change_scheme`
--

INSERT INTO `apply_change_scheme` (`id`, `id_student`, `reason`, `id_intake`, `id_programme`, `id_program_scheme`, `id_new_semester`, `id_new_program_scheme`, `fee`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 5, '123', 1, 2, 7, 1, 6, 1000.00, 1, 1, '2020-08-17 12:50:20', NULL, '2020-08-17 12:50:20'),
(2, 11, 'Changing Scheme', 1, 2, 7, 1, 6, 700.00, 1, 1, '2020-08-20 05:45:55', NULL, '2020-08-20 05:45:55');

-- --------------------------------------------------------

--
-- Table structure for table `apply_change_status`
--

CREATE TABLE `apply_change_status` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT NULL,
  `id_change_status` int(20) DEFAULT NULL,
  `id_semester` int(20) DEFAULT NULL,
  `reason` varchar(580) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `apply_change_status`
--

INSERT INTO `apply_change_status` (`id`, `id_student`, `id_change_status`, `id_semester`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 4, 1, 1, 'Droping The Course', 1, 1, '2020-08-17 08:35:57', NULL, '2020-08-17 08:35:57'),
(2, 4, 1, 1, 'Dropping', 1, 1, '2020-08-17 12:25:49', NULL, '2020-08-17 12:25:49'),
(3, 4, 1, 2, 'Reason', 1, 1, '2020-08-17 12:26:56', NULL, '2020-08-17 12:26:56');

-- --------------------------------------------------------

--
-- Table structure for table `asset_category`
--

CREATE TABLE `asset_category` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `description` varchar(580) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asset_disposal`
--

CREATE TABLE `asset_disposal` (
  `id` int(20) NOT NULL,
  `id_disposal_type` int(20) DEFAULT NULL,
  `date_time` datetime DEFAULT current_timestamp(),
  `reason` varchar(580) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asset_disposal_detail`
--

CREATE TABLE `asset_disposal_detail` (
  `id` int(20) NOT NULL,
  `id_asset_disposal` int(20) NOT NULL,
  `id_asset` int(20) DEFAULT NULL,
  `asset_code` varchar(50) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asset_item`
--

CREATE TABLE `asset_item` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `description` varchar(580) DEFAULT '',
  `id_asset_category` int(20) DEFAULT NULL,
  `id_asset_sub_category` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asset_order`
--

CREATE TABLE `asset_order` (
  `id` int(20) NOT NULL,
  `id_grn` int(20) DEFAULT 0,
  `id_po` int(20) DEFAULT NULL,
  `id_po_detail` int(20) DEFAULT NULL,
  `asset_order_number` varchar(50) DEFAULT '',
  `asset_type` varchar(50) DEFAULT '',
  `id_category` int(20) DEFAULT NULL,
  `id_sub_category` int(20) DEFAULT NULL,
  `id_item` int(20) DEFAULT NULL,
  `ordered_qty` int(20) DEFAULT NULL,
  `received_qty` int(20) DEFAULT NULL,
  `balance_qty` int(20) DEFAULT NULL,
  `amount` float(20,2) DEFAULT 0.00,
  `amount_per_item` float(20,2) DEFAULT 0.00,
  `id_pre_register` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asset_pre_registration`
--

CREATE TABLE `asset_pre_registration` (
  `id` int(20) NOT NULL,
  `id_asset_order` int(20) DEFAULT NULL,
  `id_category` int(20) DEFAULT NULL,
  `id_sub_category` int(20) DEFAULT NULL,
  `id_item` int(20) DEFAULT NULL,
  `id_grn` int(20) DEFAULT NULL,
  `name` varchar(500) DEFAULT '',
  `brand` varchar(500) DEFAULT '',
  `company` varchar(500) DEFAULT '',
  `price` float(20,2) DEFAULT NULL,
  `tax` float(20,2) DEFAULT NULL,
  `total_amount` float(20,2) DEFAULT NULL,
  `depriciation_code` int(20) DEFAULT NULL,
  `depriciation_value` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asset_registration`
--

CREATE TABLE `asset_registration` (
  `id` int(20) NOT NULL,
  `id_asset_order` int(20) DEFAULT NULL,
  `id_category` int(20) DEFAULT NULL,
  `id_sub_category` int(20) DEFAULT NULL,
  `id_item` int(20) DEFAULT NULL,
  `id_grn` int(20) DEFAULT NULL,
  `name` varchar(500) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `brand` varchar(500) DEFAULT '',
  `company` varchar(500) DEFAULT '',
  `price` float(20,2) DEFAULT NULL,
  `tax` float(20,2) DEFAULT NULL,
  `total_amount` float(20,2) DEFAULT NULL,
  `depriciation_code` int(20) DEFAULT NULL,
  `depriciation_value` int(20) DEFAULT NULL,
  `id_pre_registration` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `asset_sub_category`
--

CREATE TABLE `asset_sub_category` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `description` varchar(580) DEFAULT '',
  `id_asset_category` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `assign_student_to_exam_center`
--

CREATE TABLE `assign_student_to_exam_center` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT NULL,
  `id_exam_center` int(20) DEFAULT NULL,
  `status` varchar(50) DEFAULT 'Assigne',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attendence_setup`
--

CREATE TABLE `attendence_setup` (
  `id` int(20) NOT NULL,
  `date_time` datetime DEFAULT NULL,
  `id_program` int(20) DEFAULT 0,
  `program_scheme` varchar(200) DEFAULT '',
  `registration_item` varchar(200) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `min_requirement` int(20) DEFAULT 0,
  `is_barring_applicable` int(2) DEFAULT 0,
  `min_warning` int(20) DEFAULT 0,
  `max_warning` int(20) DEFAULT 0,
  `min_barring` int(20) DEFAULT 0,
  `max_barring` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendence_setup`
--

INSERT INTO `attendence_setup` (`id`, `date_time`, `id_program`, `program_scheme`, `registration_item`, `type`, `min_requirement`, `is_barring_applicable`, `min_warning`, `max_warning`, `min_barring`, `max_barring`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '2020-08-01 00:00:00', 1, 'Full Time', '', '', 0, 1, 80, 70, 65, 60, 1, 1, '2020-08-05 12:41:59', NULL, '2020-08-05 12:41:59');

-- --------------------------------------------------------

--
-- Table structure for table `award`
--

CREATE TABLE `award` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `level` int(20) DEFAULT 0,
  `description` varchar(500) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `award`
--

INSERT INTO `award` (`id`, `name`, `level`, `description`, `code`, `id_programme`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Foundation', 1, 'Foundation', 'FDN', NULL, 1, 1, '2020-07-10 23:44:20', NULL, '2020-07-10 23:44:20'),
(2, 'Diploma', 2, 'Diploma', 'DIP', NULL, 1, 1, '2020-08-09 05:03:14', NULL, '2020-08-09 05:03:14'),
(3, 'Degree', 3, 'Degree', 'DEG', NULL, 1, 1, '2020-08-09 05:03:46', NULL, '2020-08-09 05:03:46'),
(4, 'Master', 4, 'Master', 'MAS', NULL, 1, 1, '2020-08-09 05:04:15', NULL, '2020-08-09 05:04:15'),
(5, 'PHD', 5, 'PHD', 'PHD', NULL, 1, 1, '2020-08-09 05:05:13', NULL, '2020-08-09 05:05:13'),
(6, 'Graduate Diploma', 4, 'Graduate Diploma', 'GDIP', NULL, 1, 1, '2020-08-09 05:06:39', NULL, '2020-08-09 05:06:39');

-- --------------------------------------------------------

--
-- Table structure for table `award_level`
--

CREATE TABLE `award_level` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `description` varchar(500) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `award_level`
--

INSERT INTO `award_level` (`id`, `name`, `description`, `code`, `id_programme`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Degree', 'Degree', 'Degree', 1, 1, 1, '2020-07-11 11:58:57', NULL, '2020-07-11 11:58:57');

-- --------------------------------------------------------

--
-- Table structure for table `bank_details`
--

CREATE TABLE `bank_details` (
  `id` int(20) NOT NULL,
  `id_vendor` varchar(10) DEFAULT NULL,
  `id_bank` varchar(120) DEFAULT '',
  `address` varchar(520) DEFAULT '',
  `country` varchar(120) DEFAULT '',
  `state` varchar(120) DEFAULT '',
  `city` varchar(120) DEFAULT '',
  `zipcode` varchar(120) DEFAULT '',
  `branch_no` varchar(120) DEFAULT '',
  `acc_no` varchar(120) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bank_registration`
--

CREATE TABLE `bank_registration` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `bank_id` varchar(50) DEFAULT '',
  `address` varchar(580) DEFAULT '',
  `landmark` varchar(580) DEFAULT '',
  `city` varchar(580) DEFAULT '',
  `id_state` int(20) DEFAULT NULL,
  `id_country` int(20) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `cr_fund` varchar(50) DEFAULT '',
  `cr_department` varchar(50) DEFAULT '',
  `cr_activity` varchar(50) DEFAULT '',
  `cr_account` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_registration`
--

INSERT INTO `bank_registration` (`id`, `name`, `code`, `bank_id`, `address`, `landmark`, `city`, `id_state`, `id_country`, `zipcode`, `cr_fund`, `cr_department`, `cr_activity`, `cr_account`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'CIMB Bank', 'CIMB', '1001', 'Jalan Raja Laut', 'HQ', 'Kuala LUmpur', 1, 1, 50480, '', '', '', '', 1, 1, '2020-08-20 10:05:54', NULL, '2020-08-20 10:05:54');

-- --------------------------------------------------------

--
-- Table structure for table `barring`
--

CREATE TABLE `barring` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT NULL,
  `id_barring_type` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_semester` int(20) DEFAULT NULL,
  `reason` varchar(580) DEFAULT '',
  `barring_date` datetime DEFAULT current_timestamp(),
  `barring_to_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barring`
--

INSERT INTO `barring` (`id`, `id_student`, `id_barring_type`, `id_program`, `id_intake`, `id_semester`, `reason`, `barring_date`, `barring_to_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 6, 1, 1, 1, 0, 'Irregular', '2020-08-01 00:00:00', '2020-08-15 00:00:00', 1, 1, '2020-08-10 13:52:31', NULL, '2020-08-10 13:52:31'),
(2, 7, 1, 1, 1, 0, 'Barring ', '2020-08-14 00:00:00', '2020-08-21 00:00:00', 1, 1, '2020-08-10 14:37:16', NULL, '2020-08-10 14:37:16');

-- --------------------------------------------------------

--
-- Table structure for table `barring_type`
--

CREATE TABLE `barring_type` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barring_type`
--

INSERT INTO `barring_type` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'IRREGULAR To Class', 'IRREGULAR', 1, 1, '2020-08-10 13:51:58', NULL, '2020-08-10 13:51:58');

-- --------------------------------------------------------

--
-- Table structure for table `bulk_withdraw`
--

CREATE TABLE `bulk_withdraw` (
  `id` int(20) NOT NULL,
  `id_exam_register` int(20) DEFAULT NULL,
  `id_course_registered` int(20) DEFAULT 0,
  `id_exam_center` int(20) DEFAULT 0 COMMENT 'it''s id_exam_event not changed due to change in flow',
  `id_course` int(20) DEFAULT 0,
  `id_semester` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_programme` int(20) DEFAULT 0,
  `reason` varchar(580) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bulk_withdraw`
--

INSERT INTO `bulk_withdraw` (`id`, `id_exam_register`, `id_course_registered`, `id_exam_center`, `id_course`, `id_semester`, `id_student`, `id_intake`, `id_programme`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 3, 0, 1, 2, 1, 1, 1, 1, 'Reason', 0, 1, '2020-07-11 11:22:29', NULL, '2020-07-11 11:22:29'),
(2, 5, 0, 1, 2, 1, 2, 1, 1, 'Difficult FRM', 0, 1, '2020-07-11 11:55:46', NULL, '2020-07-11 11:55:46'),
(3, 6, 0, 1, 2, 1, 3, 1, 1, 'asdffsdsdfs', 0, 1, '2020-07-11 12:57:54', NULL, '2020-07-11 12:57:54'),
(4, 8, 10, 1, 1, NULL, 5, 1, 1, 'sadasd', 0, 1, '2020-08-09 05:18:13', NULL, '2020-08-09 05:18:13'),
(5, 12, 12, 1, 3, NULL, 6, 1, 1, 'Not Studied', 0, 1, '2020-08-10 13:12:58', NULL, '2020-08-10 13:12:58'),
(6, 13, 16, 1, 3, NULL, 7, 1, 1, 'Need To Change Course', 0, 1, '2020-08-10 14:33:15', NULL, '2020-08-10 14:33:15');

-- --------------------------------------------------------

--
-- Table structure for table `category_type`
--

CREATE TABLE `category_type` (
  `id` int(20) NOT NULL,
  `category_name` varchar(120) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `change_branch`
--

CREATE TABLE `change_branch` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT NULL,
  `reason` varchar(580) DEFAULT '',
  `id_intake` int(20) DEFAULT 0,
  `id_programme` int(20) DEFAULT 0,
  `id_program_scheme` int(20) DEFAULT 0,
  `id_new_semester` int(20) DEFAULT 0,
  `id_branch` int(20) DEFAULT 0,
  `id_new_branch` int(20) DEFAULT 0,
  `fee` float(20,2) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `change_branch`
--

INSERT INTO `change_branch` (`id`, `id_student`, `reason`, `id_intake`, `id_programme`, `id_program_scheme`, `id_new_semester`, `id_branch`, `id_new_branch`, `fee`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 10, 'Transfered My Location', 1, 2, 0, 1, 1, 2, NULL, 1, 1, '2020-08-19 13:14:37', NULL, '2020-08-19 13:14:37');

-- --------------------------------------------------------

--
-- Table structure for table `change_branch_history`
--

CREATE TABLE `change_branch_history` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT NULL,
  `id_change_branch` int(20) DEFAULT 0,
  `id_branch` int(20) DEFAULT 0,
  `id_new_branch` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `change_branch_history`
--

INSERT INTO `change_branch_history` (`id`, `id_student`, `id_change_branch`, `id_branch`, `id_new_branch`, `id_program`, `id_intake`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, NULL, 0, 0, 0, 0, 0, 1, NULL, '2020-08-19 13:18:55', NULL, '2020-08-19 13:18:55'),
(2, 10, 1, 1, 2, NULL, NULL, 1, 1, '2020-08-19 13:18:55', NULL, '2020-08-19 13:18:55'),
(3, NULL, 0, 0, 0, 0, 0, 1, NULL, '2020-08-19 13:20:35', NULL, '2020-08-19 13:20:35'),
(4, 10, 1, 1, 2, 2, 1, 1, 1, '2020-08-19 13:20:35', NULL, '2020-08-19 13:20:35'),
(5, NULL, 0, 0, 0, 0, 0, 1, NULL, '2020-08-19 13:20:45', NULL, '2020-08-19 13:20:45'),
(6, 10, 1, 1, 2, 2, 1, 1, 1, '2020-08-19 13:20:45', NULL, '2020-08-19 13:20:45'),
(7, 10, 1, 1, 2, 2, 1, 1, 1, '2020-08-19 13:22:10', NULL, '2020-08-19 13:22:10');

-- --------------------------------------------------------

--
-- Table structure for table `change_status`
--

CREATE TABLE `change_status` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `change_status`
--

INSERT INTO `change_status` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Inactive', 'IN', 1, 1, '2020-08-17 08:35:33', NULL, '2020-08-17 08:35:33');

-- --------------------------------------------------------

--
-- Table structure for table `class_details`
--

CREATE TABLE `class_details` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `communication_group`
--

CREATE TABLE `communication_group` (
  `id` int(20) NOT NULL,
  `id_template` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_recepients`
--

CREATE TABLE `communication_group_recepients` (
  `id` int(20) NOT NULL,
  `id_group` int(20) DEFAULT 0,
  `id_recepient` int(20) DEFAULT 0,
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `communication_template`
--

CREATE TABLE `communication_template` (
  `id` int(20) NOT NULL,
  `name` text DEFAULT NULL,
  `subject` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_template`
--

INSERT INTO `communication_template` (`id`, `name`, `subject`, `message`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Offer Letter', 'Offer Letter', '<p>Dear @studentname,</p>\r\n\r\n<p><strong>Congratulations, you have been selected for the @program to study at our university.</strong></p>\r\n\r\n<p>For your information this offer of admission is subjected to the below following terms and condition.</p>\r\n\r\n<ul>\r\n <li>Adherence to the mode of study, rules and regulations of the university.</li>\r\n <li>Settlement of the fees based on the scheduled sent by the university.</li>\r\n</ul>\r\n', 1, 1, '2020-08-10 06:37:43', NULL, '2020-08-10 06:37:43'),
(2, 'Joining letter', 'Joining Letter', '<p>Dear @studentname</p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n', 1, 1, '2020-08-10 07:23:22', NULL, '2020-08-10 07:23:22');

-- --------------------------------------------------------

--
-- Table structure for table `company_details`
--

CREATE TABLE `company_details` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `email` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `tax_no` varchar(120) DEFAULT '',
  `address_one` varchar(520) DEFAULT '',
  `address_two` varchar(520) DEFAULT '',
  `country` varchar(120) DEFAULT '',
  `state` varchar(120) DEFAULT '',
  `zipcode` varchar(120) DEFAULT '',
  `id_vendor` varchar(20) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `convocation`
--

CREATE TABLE `convocation` (
  `id` int(20) NOT NULL,
  `convocation_session` varchar(580) DEFAULT '',
  `session_capacity` int(10) DEFAULT NULL,
  `number_of_guest` int(10) DEFAULT NULL,
  `year` int(20) DEFAULT 0,
  `weblink` varchar(580) DEFAULT '',
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `confirm_attendence_end_date` datetime DEFAULT current_timestamp(),
  `record_verification_end_date` datetime DEFAULT current_timestamp(),
  `graduation_guest_application_end_date` datetime DEFAULT current_timestamp(),
  `student_portal_end_date` datetime DEFAULT current_timestamp(),
  `award` int(10) DEFAULT 1,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `convocation`
--

INSERT INTO `convocation` (`id`, `convocation_session`, `session_capacity`, `number_of_guest`, `year`, `weblink`, `start_date`, `end_date`, `confirm_attendence_end_date`, `record_verification_end_date`, `graduation_guest_application_end_date`, `student_portal_end_date`, `award`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'HONS Completion', 10, 10, 0, '10', '2020-07-12 00:00:00', '2020-07-12 00:00:00', '2020-07-12 00:00:00', '2020-07-12 00:00:00', '2020-07-12 00:00:00', '2020-07-12 00:00:00', 0, 1, 1, '2020-07-11 11:59:19', NULL, '2020-07-11 11:59:19'),
(2, 'Graduation set 2', 10, 101, 0, '100', '2020-07-12 00:00:00', '2020-07-12 00:00:00', '2020-07-12 00:00:00', '2020-07-12 00:00:00', '2020-07-12 00:00:00', '2020-07-12 00:00:00', 0, 1, 1, '2020-07-11 12:59:10', NULL, '2020-07-11 12:59:10');

-- --------------------------------------------------------

--
-- Table structure for table `convocation_details`
--

CREATE TABLE `convocation_details` (
  `id` int(20) NOT NULL,
  `id_convocation` int(20) DEFAULT 0,
  `fee_item` varchar(1048) DEFAULT '',
  `fee_amount` float(20,2) DEFAULT 0.00,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(2) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(2) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Malaysia', 1, 0, '2020-07-10 23:22:08', 0, '2020-07-10 23:22:08'),
(2, 'Australia', 1, 0, '2020-07-10 23:22:19', 0, '2020-07-10 23:22:19');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `id_staff_coordinator` int(10) DEFAULT NULL,
  `name_in_malay` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `credit_hours` varchar(20) DEFAULT '',
  `id_department` int(10) DEFAULT NULL,
  `id_faculty_program` int(20) DEFAULT 0,
  `id_course_type` int(20) DEFAULT 0,
  `class_total_hr` int(20) DEFAULT 0,
  `class_recurrence` varchar(50) DEFAULT '',
  `class_recurrence_period` varchar(50) DEFAULT '',
  `tutorial_total_hr` int(20) DEFAULT 0,
  `tutorial_recurrence` varchar(50) DEFAULT '',
  `tutorial_recurrence_period` varchar(50) DEFAULT '',
  `lab_total_hr` int(20) DEFAULT 0,
  `lab_recurrence` varchar(50) DEFAULT '',
  `lab_recurrence_period` varchar(50) DEFAULT '',
  `exam_total_hr` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `name`, `id_staff_coordinator`, `name_in_malay`, `code`, `credit_hours`, `id_department`, `id_faculty_program`, `id_course_type`, `class_total_hr`, `class_recurrence`, `class_recurrence_period`, `tutorial_total_hr`, `tutorial_recurrence`, `tutorial_recurrence_period`, `lab_total_hr`, `lab_recurrence`, `lab_recurrence_period`, `exam_total_hr`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'COMPARATIVE ETHICS', 0, 'COMPARATIVE ETHICS', 'MPU3312', '2', 1, 1, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2020-07-10 23:39:49', NULL, '2020-07-10 23:39:49'),
(2, 'EDUCATION PSYCHOLOGY AND TEACHING', 0, 'EDUCATION PSYCHOLOGY AND TEACHING', 'EPT112', '3', 1, 1, 1, 30, 'Weekly', 'Trice', 20, 'Weekly', 'Twice', 10, 'Weekly', 'Once', 3, 1, NULL, '2020-07-10 23:40:03', NULL, '2020-07-10 23:40:03'),
(3, 'HUBUNGAN ETNIK', 0, 'HUBUNGAN ETNIK', 'MPU1113', '3', 1, 1, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2020-07-10 23:40:23', NULL, '2020-07-10 23:40:23'),
(4, 'TAMADUN ISLAM DAN ASIA', 0, 'TAMADUN ISLAM DAN ASIA', 'MPU3123', '3', 1, 1, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2020-07-11 12:50:23', NULL, '2020-07-11 12:50:23'),
(5, 'SOCIAL RESPONSIBILITY PROJECT', 0, 'SOCIAL RESPONSIBILITY PROJECT', 'MPU3412', '2', 1, 1, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2020-08-20 11:27:54', NULL, '2020-08-20 11:27:54'),
(6, 'ACADEMIC WRITING', 0, 'ACADEMIC WRITING', 'MPU3222', '2', 1, 1, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2020-08-20 11:28:35', NULL, '2020-08-20 11:28:35'),
(7, 'REFLECTIVE PRACTISE', 0, 'REFLECTIVE PRACTISE', 'ERP212', '3', 1, 1, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2020-08-20 11:30:01', NULL, '2020-08-20 11:30:01'),
(8, 'MICRO TEACHING', 0, 'MICRO TEACHING', 'EMT211', '3', 1, 1, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2020-08-20 11:30:35', NULL, '2020-08-20 11:30:35'),
(9, 'TRENDS IN ONLINE LEARNIG', 0, 'TRENDS IN ONLINE LEARNIG', 'EPP222', '3', 1, 1, 1, 0, '', '', 0, '', '', 0, '', '', 0, 1, NULL, '2020-08-20 11:31:03', NULL, '2020-08-20 11:31:03');

-- --------------------------------------------------------

--
-- Table structure for table `courses_from_programme_landscape`
--

CREATE TABLE `courses_from_programme_landscape` (
  `id` int(20) NOT NULL,
  `id_course_registration` int(20) DEFAULT NULL,
  `id_course_programme` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `course_grade`
--

CREATE TABLE `course_grade` (
  `id` int(20) NOT NULL,
  `id_program` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT NULL,
  `id_grade` int(20) DEFAULT NULL,
  `minmarks` varchar(120) DEFAULT '',
  `maxmarks` varchar(120) DEFAULT '',
  `result` varchar(50) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_grade`
--

INSERT INTO `course_grade` (`id`, `id_program`, `id_course`, `id_grade`, `minmarks`, `maxmarks`, `result`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 1, 1, 1, '80', '100', 'pass', NULL, NULL, '2020-07-11 11:25:59', NULL, '2020-07-11 11:25:59');

-- --------------------------------------------------------

--
-- Table structure for table `course_registration`
--

CREATE TABLE `course_registration` (
  `id` bigint(20) NOT NULL,
  `id_intake` int(20) DEFAULT NULL,
  `id_programme` int(20) DEFAULT NULL,
  `id_student` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT NULL,
  `id_course_registered_landscape` int(20) DEFAULT 0,
  `id_semester` int(20) DEFAULT 0,
  `is_exam_registered` int(20) DEFAULT 0,
  `is_result_announced` int(2) DEFAULT 0,
  `is_bulk_withdraw` int(20) DEFAULT 0,
  `by_student` int(2) DEFAULT 0,
  `total_course_marks` int(20) DEFAULT 0,
  `total_obtained_marks` int(20) DEFAULT 0,
  `total_min_pass_marks` int(20) DEFAULT 0,
  `total_result` varchar(200) DEFAULT '',
  `grade` varchar(512) DEFAULT '',
  `created_date` datetime DEFAULT current_timestamp(),
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `created_by` int(20) DEFAULT 0,
  `updated_by` int(20) DEFAULT 0,
  `status` int(5) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_registration`
--

INSERT INTO `course_registration` (`id`, `id_intake`, `id_programme`, `id_student`, `id_course`, `id_course_registered_landscape`, `id_semester`, `is_exam_registered`, `is_result_announced`, `is_bulk_withdraw`, `by_student`, `total_course_marks`, `total_obtained_marks`, `total_min_pass_marks`, `total_result`, `grade`, `created_date`, `created_dt_tm`, `created_by`, `updated_by`, `status`) VALUES
(21, 1, 1, 6, 2, 11, 0, 16, 3, 0, 0, 300, 155, 120, 'Pass', 'A+', '2020-08-13 10:42:24', '2020-08-13 10:42:24', 1, 1, 0),
(22, 1, 1, 6, 4, 5, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '2020-08-13 12:01:40', '2020-08-13 12:01:40', 1, 0, 0),
(23, 1, 1, 8, 3, 10, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '2020-08-19 05:52:09', '2020-08-19 05:52:09', 1, 0, 0),
(24, 1, 1, 8, 4, 5, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '2020-08-19 05:52:09', '2020-08-19 05:52:09', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `course_requisite`
--

CREATE TABLE `course_requisite` (
  `id` int(20) NOT NULL,
  `id_master_course` int(10) DEFAULT NULL,
  `id_course` int(10) DEFAULT NULL,
  `requisite_type` varchar(520) DEFAULT '',
  `min_pass_grade` varchar(20) DEFAULT '',
  `min_total_credit` varchar(10) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_requisite`
--

INSERT INTO `course_requisite` (`id`, `id_master_course`, `id_course`, `requisite_type`, `min_pass_grade`, `min_total_credit`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 1, 3, 'Co-requisite', 'Excellent', '0', NULL, NULL, '2020-07-15 00:55:13', NULL, '2020-07-15 00:55:13'),
(2, 3, 1, 'Co-requisite', '', '0', NULL, NULL, '2020-07-30 04:30:04', NULL, '2020-07-30 04:30:04');

-- --------------------------------------------------------

--
-- Table structure for table `course_type`
--

CREATE TABLE `course_type` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_type`
--

INSERT INTO `course_type` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Coursework', 'COU', 1, NULL, '2020-07-15 00:43:10', NULL, '2020-07-15 00:43:10'),
(2, 'Project', 'PRO', 1, NULL, '2020-07-15 00:43:23', NULL, '2020-07-15 00:43:23'),
(3, 'Research', 'RES', 1, NULL, '2020-08-09 19:44:18', NULL, '2020-08-09 19:44:18'),
(4, 'Audit', 'AUD', 1, NULL, '2020-08-09 19:44:39', NULL, '2020-08-09 19:44:39');

-- --------------------------------------------------------

--
-- Table structure for table `course_withdraw`
--

CREATE TABLE `course_withdraw` (
  `id` int(20) NOT NULL,
  `semester_type` varchar(50) DEFAULT '',
  `effective_date_from` datetime DEFAULT current_timestamp(),
  `min_days` int(20) DEFAULT NULL,
  `max_days` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `credit_note`
--

CREATE TABLE `credit_note` (
  `id` int(20) NOT NULL,
  `type` varchar(520) DEFAULT '',
  `id_invoice` int(20) DEFAULT NULL,
  `reference_number` varchar(520) DEFAULT '',
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `id_sponser` int(20) DEFAULT 0,
  `id_type` int(20) DEFAULT 0,
  `ratio` varchar(500) DEFAULT '',
  `amount` float(20,2) DEFAULT 0.00,
  `total_amount` float(20,2) DEFAULT 0.00,
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_note`
--

INSERT INTO `credit_note` (`id`, `type`, `id_invoice`, `reference_number`, `id_program`, `id_intake`, `id_student`, `id_sponser`, `id_type`, `ratio`, `amount`, `total_amount`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Applicant', 2, 'CR000001/2020', 1, 1, 2, 0, 0, 'Amount', 10.00, 100.00, '10 MYR Returned', 1, NULL, '2020-08-09 14:51:55', NULL, '2020-08-09 14:51:55'),
(2, 'Sponsor', 9, 'CR000002/2020', 0, 0, 5, 1, 0, 'Amount', 10.00, 100.00, '', 1, NULL, '2020-08-09 14:54:06', NULL, '2020-08-09 14:54:06'),
(3, 'Sponsor', 11, 'CR000003/2020', 0, 0, 6, 1, 1, 'Amount', 10.00, 100.00, 'Extra Amount Rapayment', 1, NULL, '2020-08-10 13:29:02', NULL, '2020-08-10 13:29:02'),
(4, 'Student', 13, 'CR000004/2020', 1, 1, 7, 0, 1, 'Amount', 18.00, 200.00, 'Amount Refunded', 1, NULL, '2020-08-10 14:35:09', NULL, '2020-08-10 14:35:09');

-- --------------------------------------------------------

--
-- Table structure for table `credit_note_details`
--

CREATE TABLE `credit_note_details` (
  `id` int(20) NOT NULL,
  `id_credit_note` int(10) DEFAULT NULL,
  `id_main_invoice` int(20) DEFAULT 0,
  `invoice_amount` varchar(20) DEFAULT '',
  `credit_note_amount` varchar(20) DEFAULT '',
  `remarks` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `credit_note_type`
--

CREATE TABLE `credit_note_type` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_note_type`
--

INSERT INTO `credit_note_type` (`id`, `name`, `name_optional_language`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Fee Rapayable Amount', '', 'FRA', 1, NULL, '2020-08-10 13:28:15', NULL, '2020-08-10 13:28:15');

-- --------------------------------------------------------

--
-- Table structure for table `credit_transfer`
--

CREATE TABLE `credit_transfer` (
  `id` int(20) NOT NULL,
  `institution_type` varchar(2048) DEFAULT '',
  `application_id` varchar(200) DEFAULT '',
  `application_type` varchar(200) DEFAULT '',
  `id_semester` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `applied_by` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `reason` varchar(1024) DEFAULT '',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_transfer`
--

INSERT INTO `credit_transfer` (`id`, `institution_type`, `application_id`, `application_type`, `id_semester`, `id_student`, `applied_by`, `status`, `reason`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Internal', 'CT000001/2020', 'Credit Transfer', 1, 5, '', 1, '', 1, '2020-08-06 02:27:12', 1, '2020-08-06 02:27:12'),
(2, 'Internal', 'CT000002/2020', 'Credit Transfer', 1, 6, '', 0, '', 1, '2020-08-10 14:06:21', NULL, '2020-08-10 14:06:21'),
(3, 'Internal', 'CT000003/2020', 'Credit Transfer', 1, 7, '', 0, '', 1, '2020-08-10 14:32:39', NULL, '2020-08-10 14:32:39');

-- --------------------------------------------------------

--
-- Table structure for table `credit_transfer_details`
--

CREATE TABLE `credit_transfer_details` (
  `id` int(20) NOT NULL,
  `id_credit_transfer` int(20) DEFAULT 0,
  `id_institution` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_grade` int(20) DEFAULT 0,
  `institution_type` varchar(200) DEFAULT '',
  `subject_course` varchar(2048) DEFAULT '',
  `id_equivalent_course` int(20) DEFAULT 0,
  `credit_hours` int(20) DEFAULT 0,
  `remarks` varchar(2048) DEFAULT '',
  `processing_fee` int(20) DEFAULT 0,
  `document_upload` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credit_transfer_details`
--

INSERT INTO `credit_transfer_details` (`id`, `id_credit_transfer`, `id_institution`, `id_course`, `id_grade`, `institution_type`, `subject_course`, `id_equivalent_course`, `credit_hours`, `remarks`, `processing_fee`, `document_upload`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 0, 3, 2, 'Internal', '', 4, 35, 'Remark', 0, '', NULL, NULL, '2020-08-06 02:26:54', NULL, '2020-08-06 02:26:54'),
(3, 2, 0, 3, 4, 'Internal', '', 4, 35, 'Exempted', 0, '', NULL, NULL, '2020-08-10 14:06:18', NULL, '2020-08-10 14:06:18'),
(4, 3, 0, 3, 1, 'Internal', '', 4, 35, 'Adding Excemption', 0, '', NULL, NULL, '2020-08-10 14:32:38', NULL, '2020-08-10 14:32:38');

-- --------------------------------------------------------

--
-- Table structure for table `currency_rate_setup`
--

CREATE TABLE `currency_rate_setup` (
  `id` int(20) NOT NULL,
  `id_currency` int(20) DEFAULT 0,
  `exchange_rate` varchar(200) DEFAULT '',
  `min_rate` varchar(200) DEFAULT '',
  `max_rate` varchar(200) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency_rate_setup`
--

INSERT INTO `currency_rate_setup` (`id`, `id_currency`, `exchange_rate`, `min_rate`, `max_rate`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 1, '1', '1', '1', '2020-08-04 00:00:00', 1, 1, '2020-08-09 01:59:24', NULL, '2020-08-09 01:59:24');

-- --------------------------------------------------------

--
-- Table structure for table `currency_setup`
--

CREATE TABLE `currency_setup` (
  `id` int(20) NOT NULL,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `prefix` varchar(200) DEFAULT '',
  `suffix` varchar(200) DEFAULT '',
  `decimal_place` int(20) DEFAULT 0,
  `default` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency_setup`
--

INSERT INTO `currency_setup` (`id`, `code`, `name`, `name_optional_language`, `prefix`, `suffix`, `decimal_place`, `default`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'MYR', 'MYR', 'Ringgit Malaysia', 'MYR', 'MYR', 2, 1, 1, 1, '2020-08-09 01:58:47', NULL, '2020-08-09 01:58:47'),
(2, 'USD', 'USD ', 'US Dollar', 'USD', 'USD', 2, 0, 1, 1, '2020-08-20 09:24:33', NULL, '2020-08-20 09:24:33');

-- --------------------------------------------------------

--
-- Table structure for table `debit_note`
--

CREATE TABLE `debit_note` (
  `id` int(20) NOT NULL,
  `type` varchar(520) DEFAULT '',
  `id_invoice` int(20) DEFAULT NULL,
  `reference_number` varchar(520) DEFAULT '',
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `id_sponser` int(20) DEFAULT 0,
  `id_type` int(20) DEFAULT 0,
  `ratio` varchar(500) DEFAULT '',
  `amount` float(20,2) DEFAULT 0.00,
  `total_amount` float(20,2) DEFAULT 0.00,
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `debit_note`
--

INSERT INTO `debit_note` (`id`, `type`, `id_invoice`, `reference_number`, `id_program`, `id_intake`, `id_student`, `id_sponser`, `id_type`, `ratio`, `amount`, `total_amount`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Sponsor', 11, 'DB000001/2020', 0, 0, 6, 1, 1, 'Amount', 1.00, 100.00, 'Desc', 1, NULL, '2020-08-10 13:43:07', NULL, '2020-08-10 13:43:07'),
(2, 'Student', 13, 'DB000002/2020', 1, 1, 7, 0, 1, 'Amount', 30.00, 200.00, 'Debbited', 1, NULL, '2020-08-10 14:35:55', NULL, '2020-08-10 14:35:55');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(20) NOT NULL,
  `name` varchar(200) DEFAULT '',
  `code` varchar(120) DEFAULT '',
  `description` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`, `code`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'School of Management', 'SOM', 'School of Management', 1, NULL, '2020-07-10 23:27:51', NULL, '2020-07-10 23:27:51');

-- --------------------------------------------------------

--
-- Table structure for table `department_code`
--

CREATE TABLE `department_code` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `department_has_staff`
--

CREATE TABLE `department_has_staff` (
  `id` int(20) NOT NULL,
  `id_staff` int(20) DEFAULT 0,
  `id_department` int(20) DEFAULT 0,
  `role` varchar(1024) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department_has_staff`
--

INSERT INTO `department_has_staff` (`id`, `id_staff`, `id_department`, `role`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 1, 1, 'DEAN', '2020-08-10 00:00:00', NULL, NULL, '2020-08-10 01:36:44', NULL, '2020-08-10 01:36:44');

-- --------------------------------------------------------

--
-- Table structure for table `disposal_type`
--

CREATE TABLE `disposal_type` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `description` varchar(500) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `name`, `description`, `code`, `id_programme`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'NRIC', '', 'NRIC', NULL, 1, 1, '2020-07-11 00:16:40', NULL, '2020-07-11 00:16:40'),
(2, 'English certificate', '', 'English certificate', NULL, 1, 1, '2020-07-11 00:17:34', NULL, '2020-07-11 00:17:34');

-- --------------------------------------------------------

--
-- Table structure for table `documents_program`
--

CREATE TABLE `documents_program` (
  `id` int(20) NOT NULL,
  `id_document` int(20) DEFAULT 0,
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `documents_program`
--

INSERT INTO `documents_program` (`id`, `id_document`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'English', 1, 1, '2020-07-11 00:18:03', NULL, '2020-07-11 00:18:03');

-- --------------------------------------------------------

--
-- Table structure for table `documents_program_details`
--

CREATE TABLE `documents_program_details` (
  `id` int(20) NOT NULL,
  `id_documents_program` int(20) DEFAULT 0,
  `id_document` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `documents_program_details`
--

INSERT INTO `documents_program_details` (`id`, `id_documents_program`, `id_document`, `id_program`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 0, 1, NULL, NULL, '2020-07-11 00:17:45', NULL, '2020-07-11 00:17:45'),
(2, 1, 0, 0, NULL, NULL, '2020-07-11 00:18:08', NULL, '2020-07-11 00:18:08'),
(3, 0, 2, 1, NULL, NULL, '2020-08-17 11:08:25', NULL, '2020-08-17 11:08:25');

-- --------------------------------------------------------

--
-- Table structure for table `document_checklist`
--

CREATE TABLE `document_checklist` (
  `id` int(20) NOT NULL,
  `document_name` varchar(520) DEFAULT '',
  `id_category` int(20) DEFAULT NULL,
  `mandetory` varchar(50) DEFAULT '',
  `id_program` int(20) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee_discount`
--

CREATE TABLE `employee_discount` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `amount` varchar(520) DEFAULT '',
  `currency` varchar(200) DEFAULT '',
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `employee_status` varchar(50) DEFAULT 'Pending',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_discount`
--

INSERT INTO `employee_discount` (`id`, `name`, `amount`, `currency`, `start_date`, `end_date`, `employee_status`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Employee Discount', '50', 'MYR', '2020-11-07 00:00:00', '2020-11-07 00:00:00', 'Pending', 1, NULL, '2020-07-11 00:39:46', NULL, '2020-07-11 00:39:46'),
(2, 'Name', '10', 'USD', '2020-08-01 00:00:00', '2020-08-31 00:00:00', 'Pending', 1, NULL, '2020-08-20 04:32:58', NULL, '2020-08-20 04:32:58');

-- --------------------------------------------------------

--
-- Table structure for table `employment_status`
--

CREATE TABLE `employment_status` (
  `id` int(20) NOT NULL,
  `id_student` int(10) DEFAULT NULL,
  `company_name` varchar(120) DEFAULT '',
  `company_address` varchar(120) DEFAULT '',
  `telephone` varchar(120) DEFAULT '',
  `fax_num` varchar(120) DEFAULT '',
  `designation` varchar(120) DEFAULT '',
  `position` varchar(120) DEFAULT '',
  `service_year` varchar(120) DEFAULT '',
  `industry` varchar(120) DEFAULT '',
  `job_desc` varchar(520) DEFAULT '',
  `employment_letter` varchar(520) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employment_status`
--

INSERT INTO `employment_status` (`id`, `id_student`, `company_name`, `company_address`, `telephone`, `fax_num`, `designation`, `position`, `service_year`, `industry`, `job_desc`, `employment_letter`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 6, 'Skaya Technology', 'KL', '232323232', '323232323', 'Developer', 'Senior Executive', '2018', 'Software', 'Software Engineer', '', NULL, NULL, '2020-08-12 10:06:04', NULL, '2020-08-12 10:06:04');

-- --------------------------------------------------------

--
-- Table structure for table `english_proficiency_details`
--

CREATE TABLE `english_proficiency_details` (
  `id` int(20) NOT NULL,
  `id_student` int(10) DEFAULT NULL,
  `test` varchar(120) DEFAULT '',
  `date` date DEFAULT NULL,
  `score` varchar(120) DEFAULT '',
  `file` varchar(520) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `english_proficiency_details`
--

INSERT INTO `english_proficiency_details` (`id`, `id_student`, `test`, `date`, `score`, `file`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 6, 'English Speech Test', '2020-08-07', 'English', 'scholarship_programme partner_programme.png', NULL, NULL, '2020-08-12 10:05:08', NULL, '2020-08-12 10:05:08');

-- --------------------------------------------------------

--
-- Table structure for table `entry_requirement_other_requirements`
--

CREATE TABLE `entry_requirement_other_requirements` (
  `id` int(20) NOT NULL,
  `id_requirement_entry` int(20) DEFAULT 0,
  `id_group` int(20) DEFAULT 0,
  `condition` varchar(2048) DEFAULT '',
  `type` varchar(2048) DEFAULT '',
  `result_value` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `entry_requirement_requirements`
--

CREATE TABLE `entry_requirement_requirements` (
  `id` int(20) NOT NULL,
  `id_requirement_entry` int(20) DEFAULT 0,
  `id_group` int(20) DEFAULT 0,
  `id_qualification` int(20) DEFAULT 0,
  `id_specialization` int(20) DEFAULT 0,
  `validity` int(20) DEFAULT 0,
  `result_item` varchar(2048) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `condition` varchar(2048) DEFAULT '',
  `result_value` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entry_requirement_requirements`
--

INSERT INTO `entry_requirement_requirements` (`id`, `id_requirement_entry`, `id_group`, `id_qualification`, `id_specialization`, `validity`, `result_item`, `description`, `condition`, `result_value`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 1, 1, 0, 0, 0, '', 'DEsc', '', '', NULL, NULL, '2020-07-19 19:58:54', NULL, '2020-07-19 19:58:54'),
(3, 2, 1, 0, 0, 0, '', 'Dess', '', '', NULL, NULL, '2020-07-19 20:00:38', NULL, '2020-07-19 20:00:38');

-- --------------------------------------------------------

--
-- Table structure for table `equivalent_course`
--

CREATE TABLE `equivalent_course` (
  `id` int(20) NOT NULL,
  `id_course` int(10) DEFAULT NULL,
  `id_equivalent_course` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `equivalent_course_details`
--

CREATE TABLE `equivalent_course_details` (
  `id` int(20) NOT NULL,
  `id_equivalent_course` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT NULL,
  `id_session` varchar(100) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `examination`
--

CREATE TABLE `examination` (
  `id` int(20) NOT NULL,
  `id_intake` int(20) DEFAULT 0,
  `id_programme` int(20) DEFAULT 0,
  `id_exam_event` int(20) DEFAULT 0,
  `id_course_registered_landscape` int(20) DEFAULT 0,
  `total_capacity` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examination`
--

INSERT INTO `examination` (`id`, `id_intake`, `id_programme`, `id_exam_event`, `id_course_registered_landscape`, `total_capacity`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(4, 1, 1, 1, 5, 8, 1, 1, '2020-08-10 12:55:08', NULL, '2020-08-10 12:55:08'),
(5, 1, 1, 1, 10, 1, 1, 1, '2020-08-10 13:06:17', NULL, '2020-08-10 13:06:17'),
(6, 1, 1, 1, 10, 1, 1, 1, '2020-08-10 14:31:19', NULL, '2020-08-10 14:31:19'),
(7, 1, 1, 1, 10, 1, 1, 1, '2020-08-13 10:12:05', NULL, '2020-08-13 10:12:05'),
(8, 1, 1, 1, 10, 1, 1, 1, '2020-08-13 10:38:34', NULL, '2020-08-13 10:38:34'),
(9, 1, 1, 1, 11, 1, 1, 1, '2020-08-13 10:42:46', NULL, '2020-08-13 10:42:46');

-- --------------------------------------------------------

--
-- Table structure for table `examination_assesment_type`
--

CREATE TABLE `examination_assesment_type` (
  `id` int(20) NOT NULL,
  `type_id` varchar(500) DEFAULT '',
  `type` varchar(500) DEFAULT '',
  `exam` varchar(500) DEFAULT '',
  `id_semester` int(20) DEFAULT NULL,
  `order` int(50) DEFAULT 0,
  `description` varchar(5096) DEFAULT '',
  `description_optional_language` varchar(5096) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `examination_components`
--

CREATE TABLE `examination_components` (
  `id` int(20) NOT NULL,
  `name` varchar(1048) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `name_optional_language` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examination_components`
--

INSERT INTO `examination_components` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Internals', 'Int', '', 1, NULL, '2020-08-07 09:34:56', NULL, '2020-08-07 09:34:56'),
(2, 'Practicals', 'Pra', '', 1, NULL, '2020-08-07 09:34:56', NULL, '2020-08-07 09:34:56'),
(3, 'Thorey Examination', 'The', '', 1, NULL, '2020-08-07 09:34:56', NULL, '2020-08-07 09:34:56');

-- --------------------------------------------------------

--
-- Table structure for table `examination_details`
--

CREATE TABLE `examination_details` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT NULL,
  `qualification_level` varchar(120) DEFAULT '',
  `degree_awarded` varchar(120) DEFAULT '',
  `specialization` varchar(120) DEFAULT '',
  `class_degree` varchar(120) DEFAULT '',
  `result` varchar(120) DEFAULT '',
  `year` varchar(20) DEFAULT '',
  `medium` varchar(120) DEFAULT '',
  `college_country` varchar(120) DEFAULT '',
  `college_name` varchar(120) DEFAULT '',
  `certificate` varchar(520) DEFAULT '',
  `transcript` varchar(520) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examination_details`
--

INSERT INTO `examination_details` (`id`, `id_student`, `qualification_level`, `degree_awarded`, `specialization`, `class_degree`, `result`, `year`, `medium`, `college_country`, `college_name`, `certificate`, `transcript`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 6, 'PHD', 'PHD', 'Computer Science', 'Phd', 'Pass', '2019', 'Malay', 'Malaysia', 'AOE Umiversity', '', '', NULL, NULL, '2020-08-12 10:04:46', NULL, '2020-08-12 10:04:46');

-- --------------------------------------------------------

--
-- Table structure for table `exam_attendence`
--

CREATE TABLE `exam_attendence` (
  `id` bigint(20) NOT NULL,
  `id_intake` int(20) DEFAULT NULL,
  `room` varchar(1024) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT NULL,
  `id_location` int(20) DEFAULT NULL,
  `id_exam_event` int(20) DEFAULT NULL,
  `id_semester` int(20) DEFAULT NULL,
  `total` int(20) DEFAULT 0,
  `present` int(20) DEFAULT 0,
  `absent` int(20) DEFAULT 0,
  `status` int(5) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_attendence`
--

INSERT INTO `exam_attendence` (`id`, `id_intake`, `room`, `id_programme`, `id_course`, `id_location`, `id_exam_event`, `id_semester`, `total`, `present`, `absent`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, NULL, '', NULL, NULL, 1, NULL, NULL, 0, 0, 0, 1, 1, '2020-07-11 11:23:24', NULL, '2020-07-11 11:23:24');

-- --------------------------------------------------------

--
-- Table structure for table `exam_attendence_details`
--

CREATE TABLE `exam_attendence_details` (
  `id` bigint(20) NOT NULL,
  `id_exam_attendence` int(20) DEFAULT NULL,
  `id_exam_register` int(20) DEFAULT NULL,
  `id_student` int(20) DEFAULT NULL,
  `id_exam_event` int(20) DEFAULT NULL,
  `id_semester` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT NULL,
  `status` int(5) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_attendence_details`
--

INSERT INTO `exam_attendence_details` (`id`, `id_exam_attendence`, `id_exam_register`, `id_student`, `id_exam_event`, `id_semester`, `id_course`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, NULL, 1, NULL, 1, 1, '2020-07-11 11:23:24', NULL, '2020-07-11 11:23:24');

-- --------------------------------------------------------

--
-- Table structure for table `exam_calender`
--

CREATE TABLE `exam_calender` (
  `id` int(20) NOT NULL,
  `id_semester` int(20) DEFAULT 0,
  `activity` varchar(200) DEFAULT '0',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_calender`
--

INSERT INTO `exam_calender` (`id`, `id_semester`, `activity`, `start_date`, `end_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Update Exam Center', '2020-08-01 00:00:00', '2021-08-31 00:00:00', 1, 1, '2020-08-06 13:32:39', 1, '2020-08-06 13:32:39');

-- --------------------------------------------------------

--
-- Table structure for table `exam_center`
--

CREATE TABLE `exam_center` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `id_country` int(20) DEFAULT NULL,
  `id_state` int(20) DEFAULT NULL,
  `city` varchar(250) DEFAULT '',
  `id_location` int(20) DEFAULT 0,
  `zipcode` varchar(50) DEFAULT '',
  `address` varchar(580) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_center`
--

INSERT INTO `exam_center` (`id`, `name`, `id_country`, `id_state`, `city`, `id_location`, `zipcode`, `address`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'OUM', 1, 1, 'KL', 1, '47301', 'Menara OUM, Block C, Kelana Centre Point, Jalan SS 7/19,  Petaling Jaya,', 1, 1, '2020-07-11 01:26:27', NULL, '2020-07-11 01:26:27');

-- --------------------------------------------------------

--
-- Table structure for table `exam_center_location`
--

CREATE TABLE `exam_center_location` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_center_location`
--

INSERT INTO `exam_center_location` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Kuala Lumpur', '', 1, NULL, '2020-07-11 01:25:46', NULL, '2020-07-11 01:25:46');

-- --------------------------------------------------------

--
-- Table structure for table `exam_center_to_semester`
--

CREATE TABLE `exam_center_to_semester` (
  `id` int(20) NOT NULL,
  `id_intake` int(20) DEFAULT NULL,
  `id_semester` int(20) DEFAULT NULL,
  `id_exam_center` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exam_configuration`
--

CREATE TABLE `exam_configuration` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `max_count` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_configuration`
--

INSERT INTO `exam_configuration` (`id`, `name`, `max_count`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Highest', '2', 1, 1, '2020-08-14 14:54:34', NULL, '2020-08-14 14:54:34');

-- --------------------------------------------------------

--
-- Table structure for table `exam_event`
--

CREATE TABLE `exam_event` (
  `id` int(20) NOT NULL,
  `id_location` int(20) DEFAULT 0,
  `id_exam_center` int(20) DEFAULT 0,
  `max_count` int(20) DEFAULT 0,
  `name` varchar(250) DEFAULT '',
  `type` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `from_dt` datetime DEFAULT current_timestamp(),
  `to_dt` datetime DEFAULT current_timestamp(),
  `from_tm` varchar(50) DEFAULT '',
  `to_tm` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_event`
--

INSERT INTO `exam_event` (`id`, `id_location`, `id_exam_center`, `max_count`, `name`, `type`, `code`, `from_dt`, `to_dt`, `from_tm`, `to_tm`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 10, 'Examination Online', 'Exam', '', '2020-07-11 00:00:00', '2020-07-11 01:27:07', '9:58 pm', '9:57 pm', 1, 1, '2020-07-11 01:27:07', NULL, '2020-07-11 01:27:07');

-- --------------------------------------------------------

--
-- Table structure for table `exam_register`
--

CREATE TABLE `exam_register` (
  `id` bigint(20) NOT NULL,
  `id_intake` int(20) DEFAULT NULL,
  `id_programme` int(20) DEFAULT NULL,
  `id_student` int(20) DEFAULT NULL,
  `is_marks_entered` int(2) DEFAULT 0,
  `is_attended` int(2) DEFAULT 0,
  `id_course` int(20) DEFAULT NULL,
  `id_location` int(20) DEFAULT 0,
  `id_semester` int(20) DEFAULT NULL,
  `status` int(5) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_register`
--

INSERT INTO `exam_register` (`id`, `id_intake`, `id_programme`, `id_student`, `is_marks_entered`, `is_attended`, `id_course`, `id_location`, `id_semester`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, 0, 1, NULL, 1, 1, 1, 1, '2020-07-11 11:21:56', NULL, '2020-07-11 11:21:56'),
(2, 1, 1, 2, 0, 0, NULL, 1, 1, 1, 1, '2020-07-11 11:55:19', NULL, '2020-07-11 11:55:19'),
(3, 1, 1, 3, 0, 0, NULL, 1, 1, 1, 1, '2020-07-11 12:57:36', NULL, '2020-07-11 12:57:36');

-- --------------------------------------------------------

--
-- Table structure for table `exam_registration`
--

CREATE TABLE `exam_registration` (
  `id` bigint(20) NOT NULL,
  `id_exam` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT NULL,
  `id_programme` int(20) DEFAULT NULL,
  `id_location` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT NULL,
  `is_bulk_withdraw` int(20) DEFAULT 0,
  `id_course_registered_landscape` int(20) DEFAULT 0,
  `id_course_registered` int(20) DEFAULT 0,
  `is_semester_result` int(20) DEFAULT 0,
  `created_date` datetime DEFAULT current_timestamp(),
  `created_by` int(20) DEFAULT NULL,
  `status` int(5) DEFAULT 0,
  `id_exam_center` int(20) DEFAULT NULL,
  `id_semester` int(20) DEFAULT NULL,
  `total_capacity` int(20) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT NULL,
  `id_exam_event` int(20) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_registration`
--

INSERT INTO `exam_registration` (`id`, `id_exam`, `id_intake`, `id_programme`, `id_location`, `id_student`, `id_course`, `is_bulk_withdraw`, `id_course_registered_landscape`, `id_course_registered`, `is_semester_result`, `created_date`, `created_by`, `status`, `id_exam_center`, `id_semester`, `total_capacity`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `id_exam_event`) VALUES
(8, 1, 1, 1, 0, 5, 1, 4, 6, 10, 0, '2020-08-09 05:17:47', 1, 0, NULL, NULL, 123, '2020-08-09 05:17:47', 0, NULL, 1),
(9, 2, 1, 1, 0, 5, 1, 0, 6, 11, 0, '2020-08-09 05:24:21', 1, 0, NULL, NULL, 120, '2020-08-09 05:24:21', 0, NULL, 1),
(11, 4, 1, 1, 0, 6, 4, 0, 5, 13, 0, '2020-08-10 12:55:08', 1, 0, NULL, NULL, 8, '2020-08-10 12:55:08', 0, NULL, 1),
(12, 5, 1, 1, 0, 6, 3, 5, 10, 12, 0, '2020-08-10 13:06:17', 1, 0, NULL, NULL, 1, '2020-08-10 13:06:17', 0, NULL, 1),
(13, 6, 1, 1, 0, 7, 3, 6, 10, 16, 0, '2020-08-10 14:31:19', 1, 0, NULL, NULL, 1, '2020-08-10 14:31:19', 0, NULL, 1),
(14, 7, 1, 1, 0, 3, 3, 0, 10, 19, 0, '2020-08-13 10:12:05', 1, 0, NULL, NULL, 1, '2020-08-13 10:12:05', 0, NULL, 1),
(15, 8, 1, 1, 0, 6, 3, 0, 10, 20, 0, '2020-08-13 10:38:34', 1, 0, NULL, NULL, 1, '2020-08-13 10:38:34', 0, NULL, 1),
(16, 9, 1, 1, 0, 6, 2, 0, 11, 21, 0, '2020-08-13 10:42:46', 1, 0, NULL, NULL, 1, '2020-08-13 10:42:46', 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `faculty_program`
--

CREATE TABLE `faculty_program` (
  `id` int(20) NOT NULL,
  `name` varchar(200) DEFAULT '',
  `code` varchar(120) DEFAULT '',
  `description` varchar(520) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `id_partner_university` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty_program`
--

INSERT INTO `faculty_program` (`id`, `name`, `code`, `description`, `type`, `id_partner_university`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'School of Management', 'SOM', 'School of Management', '', 0, 1, NULL, '2020-07-13 09:05:46', NULL, '2020-07-13 09:05:46'),
(2, 'School of Foundation Studies', 'SOF', 'School of Foundation Studies', 'Internal', 0, 1, NULL, '2020-08-09 20:07:09', NULL, '2020-08-09 20:07:09'),
(3, 'School of Education & Cognitive Science', 'SOE', 'School of Education & Cognitive Science', '', 0, 1, NULL, '2020-08-09 20:08:01', NULL, '2020-08-09 20:08:01'),
(4, 'School of Science & Technology', 'SOS', 'School of Science & Technology', '', 0, 1, NULL, '2020-08-09 20:08:41', NULL, '2020-08-09 20:08:41'),
(5, 'School of Graduate Studies', 'SOG', 'School of Graduate Studies', '', 0, 1, NULL, '2020-08-09 20:09:18', NULL, '2020-08-09 20:09:18'),
(6, 'School of Arts, Humanities & Social Sciences', 'SOA', 'School of Arts, Humanities & Social Sciences', 'External', 4, 1, NULL, '2020-08-09 20:10:15', NULL, '2020-08-09 20:10:15');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_program_has_staff`
--

CREATE TABLE `faculty_program_has_staff` (
  `id` int(20) NOT NULL,
  `id_faculty_program` int(20) DEFAULT 0,
  `id_staff` int(20) DEFAULT 0,
  `role` varchar(1024) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty_program_has_staff`
--

INSERT INTO `faculty_program_has_staff` (`id`, `id_faculty_program`, `id_staff`, `role`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 6, 1, 'DEAN', '2020-08-15 00:00:00', NULL, NULL, '2020-08-14 03:06:01', NULL, '2020-08-14 03:06:01');

-- --------------------------------------------------------

--
-- Table structure for table `fee_category`
--

CREATE TABLE `fee_category` (
  `id` int(20) NOT NULL,
  `code` varchar(520) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `fee_group` varchar(200) DEFAULT '',
  `sequence` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_category`
--

INSERT INTO `fee_category` (`id`, `code`, `name`, `name_optional_language`, `fee_group`, `sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'TUTFEE', 'Tuition Fee', 'Tuition Fee', 'Statement Of Account', 1, 1, NULL, '2020-07-11 00:20:37', NULL, '2020-07-11 00:20:37'),
(2, 'MISC FEE', 'Miscellaneous Fee', 'MISC FEE', 'Statement Of Account', 2, 1, NULL, '2020-07-11 00:21:07', NULL, '2020-07-11 00:21:07');

-- --------------------------------------------------------

--
-- Table structure for table `fee_setup`
--

CREATE TABLE `fee_setup` (
  `id` int(20) NOT NULL,
  `code` varchar(50) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `id_fee_category` int(10) DEFAULT NULL,
  `id_amount_calculation_type` int(10) DEFAULT NULL,
  `id_frequency_mode` int(10) DEFAULT NULL,
  `account_code` varchar(50) DEFAULT '',
  `is_refundable` int(2) DEFAULT NULL,
  `is_non_invoice` int(2) DEFAULT NULL,
  `is_gst` int(2) DEFAULT NULL,
  `gst_tax` varchar(50) DEFAULT '',
  `effective_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_setup`
--

INSERT INTO `fee_setup` (`id`, `code`, `name`, `name_optional_language`, `id_fee_category`, `id_amount_calculation_type`, `id_frequency_mode`, `account_code`, `is_refundable`, `is_non_invoice`, `is_gst`, `gst_tax`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(6, 'AF', 'Assessment Fee ', 'Assessment Fee ', 1, 3, 3, '4003', 0, 0, 0, '123', '2020-08-01 00:00:00', 1, NULL, '2020-08-20 06:31:16', NULL, '2020-08-20 06:31:16'),
(7, 'TAF', 'Recurring Semester Fee', 'Recurring Semester Fee', 1, 1, 3, '4004', 0, 0, 0, '123', '2020-08-01 00:00:00', 1, NULL, '2020-08-20 06:31:44', NULL, '2020-08-20 06:31:44'),
(8, 'TRF', 'Registration Fee', 'Registration Fee', 1, 4, 5, '4003', 0, 0, 0, '123', '2020-08-01 00:00:00', 1, NULL, '2020-08-20 06:32:07', NULL, '2020-08-20 06:32:07'),
(9, 'Tuition Fee - Credit hour multiplication', 'Tuition Fee - Credit hour multiplication', 'Tuition Fee - Credit hour multiplication', 1, 2, 3, '4004', 0, 0, 0, '10', '2020-08-20 00:00:00', 1, NULL, '2020-08-20 10:55:32', NULL, '2020-08-20 10:55:32');

-- --------------------------------------------------------

--
-- Table structure for table `fee_structure`
--

CREATE TABLE `fee_structure` (
  `id` int(20) NOT NULL,
  `id_programme` int(20) DEFAULT NULL,
  `id_intake` int(20) DEFAULT NULL,
  `id_program_scheme` int(20) DEFAULT 0,
  `id_program_landscape` int(20) DEFAULT 0,
  `currency` varchar(200) DEFAULT '',
  `id_fee_item` int(20) DEFAULT NULL,
  `id_frequency_mode` int(20) DEFAULT NULL,
  `amount` float(20,2) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_structure`
--

INSERT INTO `fee_structure` (`id`, `id_programme`, `id_intake`, `id_program_scheme`, `id_program_landscape`, `currency`, `id_fee_item`, `id_frequency_mode`, `amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(53, 3, 1, 8, 5, 'MYR', 8, NULL, 300.00, 1, NULL, '2020-08-20 10:56:49', NULL, '2020-08-20 10:56:49'),
(54, 3, 1, 8, 5, 'MYR', 9, NULL, 150.00, 1, NULL, '2020-08-20 10:57:03', NULL, '2020-08-20 10:57:03'),
(55, 3, 1, 8, 5, 'MYR', 7, NULL, 120.00, 1, NULL, '2020-08-20 10:57:16', NULL, '2020-08-20 10:57:16'),
(56, 3, 1, 8, 5, 'MYR', 6, NULL, 45.00, 1, NULL, '2020-08-20 10:57:34', NULL, '2020-08-20 10:57:34');

-- --------------------------------------------------------

--
-- Table structure for table `fee_structure_activity`
--

CREATE TABLE `fee_structure_activity` (
  `id` int(20) NOT NULL,
  `id_program` int(20) DEFAULT 0,
  `id_activity` int(20) DEFAULT 0,
  `id_fee_setup` int(20) DEFAULT 0,
  `trigger` varchar(1024) DEFAULT '',
  `performa` int(2) DEFAULT NULL,
  `amount` float(20,2) DEFAULT 0.00,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_structure_activity`
--

INSERT INTO `fee_structure_activity` (`id`, `id_program`, `id_activity`, `id_fee_setup`, `trigger`, `performa`, `amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 2, 'Approval Level', 0, 170.00, 1, NULL, '2020-08-17 14:30:46', NULL, '2020-08-17 14:30:46');

-- --------------------------------------------------------

--
-- Table structure for table `fee_structure_details`
--

CREATE TABLE `fee_structure_details` (
  `id` int(20) NOT NULL,
  `id_fee_structure` int(20) DEFAULT NULL,
  `id_fee_item` int(20) DEFAULT NULL,
  `id_frequency_mode` int(20) DEFAULT NULL,
  `amount` float(20,2) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `financial_account_code`
--

CREATE TABLE `financial_account_code` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `short_code` varchar(50) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `level` int(2) DEFAULT 0,
  `id_parent` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `financial_account_code`
--

INSERT INTO `financial_account_code` (`id`, `name`, `name_optional_language`, `short_code`, `code`, `type`, `level`, `id_parent`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '', '', '', '4001', 'Tuition Fees', 0, 0, 1, 1, '2020-08-08 07:27:44', 1, '2020-08-08 07:27:44'),
(2, '', '', '', '4002', 'Assessment Fees', 0, 0, 1, 1, '2020-08-08 07:27:46', 1, '2020-08-08 07:27:46'),
(3, '', '', '', '4003', 'Registration Fees', 0, 0, 1, 1, '2020-08-20 09:50:05', NULL, '2020-08-20 09:50:05'),
(4, '', '', '', '4004', 'Recurring Fees', 0, 0, 1, 1, '2020-08-20 09:50:27', NULL, '2020-08-20 09:50:27'),
(5, '', '', '', '4005', 'Remarking Fees', 0, 0, 1, 1, '2020-08-20 09:52:05', NULL, '2020-08-20 09:52:05'),
(6, '', '', '', '4006', 'Application Fees', 0, 0, 1, 1, '2020-08-20 09:53:58', NULL, '2020-08-20 09:53:58'),
(7, '', '', '', '4007', 'Credit Transfer Fees', 0, 0, 1, 1, '2020-08-20 09:54:23', NULL, '2020-08-20 09:54:23'),
(8, '', '', '', '4008', 'Exemption Fees', 0, 0, 1, 1, '2020-08-20 09:54:40', NULL, '2020-08-20 09:54:40'),
(9, '', '', '', '4010', 'Penalty Fees', 0, 0, 1, 1, '2020-08-20 09:54:54', NULL, '2020-08-20 09:54:54'),
(10, '', '', '', '6009', 'GST', 0, 0, 1, 1, '2020-08-20 09:55:31', NULL, '2020-08-20 09:55:31'),
(11, '', '', '', '6008', 'Sibling Discount', 0, 0, 1, 1, '2020-08-20 09:56:37', NULL, '2020-08-20 09:56:37'),
(12, '', '', '', '6007', 'Alumni Discount', 0, 0, 1, 1, '2020-08-20 09:56:53', NULL, '2020-08-20 09:56:53'),
(13, '', '', '', '6006', 'Employee Discount', 0, 0, 1, 1, '2020-08-20 09:57:13', NULL, '2020-08-20 09:57:13'),
(14, '', '', '', '4011', 'Convocation Fees', 0, 0, 1, 1, '2020-08-20 09:57:36', NULL, '2020-08-20 09:57:36'),
(15, '', '', '', '2009', 'Student Deposit', 0, 0, 1, 1, '2020-08-20 09:58:36', NULL, '2020-08-20 09:58:36');

-- --------------------------------------------------------

--
-- Table structure for table `financial_year`
--

CREATE TABLE `financial_year` (
  `id` int(20) NOT NULL,
  `year` varchar(20) DEFAULT '',
  `name` varchar(120) DEFAULT '',
  `to_year` varchar(20) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `frequency_mode`
--

CREATE TABLE `frequency_mode` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `frequency_mode`
--

INSERT INTO `frequency_mode` (`id`, `name`, `name_optional_language`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'APPLICATION', 'APPLICATION', 'APPLICATION', 1, NULL, '2020-07-11 00:22:32', NULL, '2020-07-11 00:22:32'),
(3, 'PER SEMESTER', 'PER SEMESTER', 'PER SEMESTER', 1, NULL, '2020-07-11 00:23:20', NULL, '2020-07-11 00:23:20'),
(5, 'ONE TIME', 'ONE TIME', 'ONE TIME', 1, NULL, '2020-08-20 10:03:14', NULL, '2020-08-20 10:03:14');

-- --------------------------------------------------------

--
-- Table structure for table `fund_code`
--

CREATE TABLE `fund_code` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gpa_cgpa_setup`
--

CREATE TABLE `gpa_cgpa_setup` (
  `id` int(20) NOT NULL,
  `setup_by` varchar(50) DEFAULT '',
  `id_semester` int(20) DEFAULT NULL,
  `id_award` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT NULL,
  `academic_status` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gpa_cgpa_setup`
--

INSERT INTO `gpa_cgpa_setup` (`id`, `setup_by`, `id_semester`, `id_award`, `id_program`, `academic_status`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Award', 1, 1, 0, 'CGPA', 1, 1, '2020-08-06 13:30:18', NULL, '2020-08-06 13:30:18');

-- --------------------------------------------------------

--
-- Table structure for table `gpa_cgpa_setup_details`
--

CREATE TABLE `gpa_cgpa_setup_details` (
  `id` int(20) NOT NULL,
  `id_gpa_cgpa` int(20) DEFAULT NULL,
  `min_grade_point` int(20) DEFAULT NULL,
  `max_grade_point` int(20) DEFAULT NULL,
  `description` varchar(5096) DEFAULT '',
  `status_optional_language` varchar(5096) DEFAULT '',
  `probation` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gpa_cgpa_setup_details`
--

INSERT INTO `gpa_cgpa_setup_details` (`id`, `id_gpa_cgpa`, `min_grade_point`, `max_grade_point`, `description`, `status_optional_language`, `probation`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 80, 90, '1st Grade Course', '', 1, NULL, NULL, '2020-08-06 13:30:13', NULL, '2020-08-06 13:30:13');

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

CREATE TABLE `grade` (
  `id` int(20) NOT NULL,
  `name` varchar(220) DEFAULT '',
  `description` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade`
--

INSERT INTO `grade` (`id`, `name`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'A', 'First Class', 1, NULL, '2020-07-11 11:24:11', NULL, '2020-07-11 11:24:11'),
(2, 'A+', 'High Level', 1, NULL, '2020-07-11 11:24:26', NULL, '2020-07-11 11:24:26'),
(3, 'B', 'Medium Level', 1, NULL, '2020-07-11 11:24:36', NULL, '2020-07-11 11:24:36'),
(4, 'C', 'Pass', 1, NULL, '2020-07-11 11:24:47', NULL, '2020-07-11 11:24:47');

-- --------------------------------------------------------

--
-- Table structure for table `grade_setup`
--

CREATE TABLE `grade_setup` (
  `id` int(20) NOT NULL,
  `based_on` varchar(200) DEFAULT '',
  `id_intake` int(20) DEFAULT 0,
  `id_semester` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_award` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `description` varchar(1024) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade_setup`
--

INSERT INTO `grade_setup` (`id`, `based_on`, `id_intake`, `id_semester`, `id_program`, `id_course`, `id_award`, `name`, `description`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Award', 0, 1, 0, 0, 1, '', '', NULL, 1, 1, '2020-07-14 22:16:46', NULL, '2020-07-14 22:16:46'),
(2, 'Program', 0, 1, 1, 0, 0, '', '', NULL, 1, 1, '2020-07-14 22:19:00', NULL, '2020-07-14 22:19:00'),
(3, 'Program & Subject', 0, 1, 1, 1, 0, '', '', NULL, 1, 1, '2020-07-14 22:21:57', NULL, '2020-07-14 22:21:57'),
(4, 'Program & Subject', 0, 1, 1, 1, 1, '', '', NULL, 1, 1, '2020-07-14 22:34:40', NULL, '2020-07-14 22:34:40');

-- --------------------------------------------------------

--
-- Table structure for table `grade_setup_details`
--

CREATE TABLE `grade_setup_details` (
  `id` int(20) NOT NULL,
  `id_grade_setup` int(20) DEFAULT 0,
  `id_grade` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `description` varchar(1024) DEFAULT '',
  `description_optional_language` varchar(1024) DEFAULT '',
  `point` int(20) DEFAULT 0,
  `min_marks` int(20) DEFAULT 0,
  `max_marks` int(20) DEFAULT 0,
  `pass` varchar(20) DEFAULT '',
  `rank` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade_setup_details`
--

INSERT INTO `grade_setup_details` (`id`, `id_grade_setup`, `id_grade`, `name`, `description`, `description_optional_language`, `point`, `min_marks`, `max_marks`, `pass`, `rank`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, '', 'dESC', '', 3, 50, 100, 'No', 1, NULL, NULL, '2020-07-14 22:16:43', NULL, '2020-07-14 22:16:43'),
(2, 2, 2, '', 'Desc', '', 1, 90, 100, 'No', 1, NULL, NULL, '2020-07-14 22:18:59', NULL, '2020-07-14 22:18:59'),
(3, 3, 4, '', 'Desc', '', 7, 40, 100, 'No', 9, NULL, NULL, '2020-07-14 22:21:55', NULL, '2020-07-14 22:21:55'),
(5, 4, 1, '', 'First class with A', 'First class with A', 3, 50, 75, 'No', 2, NULL, NULL, '2020-07-19 00:31:30', NULL, '2020-07-19 00:31:30');

-- --------------------------------------------------------

--
-- Table structure for table `graduation_details`
--

CREATE TABLE `graduation_details` (
  `id` int(20) NOT NULL,
  `id_graduation` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `graduation_details`
--

INSERT INTO `graduation_details` (`id`, `id_graduation`, `id_student`, `status`, `created_by`, `reason`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 2, 1, 1, '', '2020-07-11 11:59:42', NULL, '2020-07-11 11:59:42'),
(2, 2, 3, 1, 1, '', '2020-07-11 12:59:47', NULL, '2020-07-11 12:59:47');

-- --------------------------------------------------------

--
-- Table structure for table `graduation_list`
--

CREATE TABLE `graduation_list` (
  `id` int(20) NOT NULL,
  `id_student` int(10) DEFAULT NULL,
  `id_convocation` int(10) DEFAULT NULL,
  `date_tm` datetime DEFAULT NULL,
  `message` varchar(2048) DEFAULT '',
  `reason` varchar(2048) DEFAULT '',
  `status` varchar(50) DEFAULT 'Graduated',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `graduation_list`
--

INSERT INTO `graduation_list` (`id`, `id_student`, `id_convocation`, `date_tm`, `message`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, NULL, 1, '2020-07-12 00:00:00', 'Convoation', '', '1', 1, '2020-07-11 11:59:42', NULL, '2020-07-11 11:59:42'),
(2, NULL, 2, '2020-07-12 00:00:00', 'asfsd', '', '1', 1, '2020-07-11 12:59:47', NULL, '2020-07-11 12:59:47');

-- --------------------------------------------------------

--
-- Table structure for table `grn`
--

CREATE TABLE `grn` (
  `id` int(20) NOT NULL,
  `id_financial_year` int(20) DEFAULT 0,
  `id_po` int(20) DEFAULT 0,
  `reference_number` varchar(50) DEFAULT '',
  `date_time` datetime DEFAULT current_timestamp(),
  `expire_date` datetime DEFAULT current_timestamp(),
  `id_vendor` int(20) DEFAULT 0,
  `description` varchar(500) DEFAULT '',
  `id_department` int(20) DEFAULT 0,
  `total_amount` float(20,2) DEFAULT 0.00,
  `rating` varchar(20) DEFAULT '',
  `id_pre_register` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `group_setup`
--

CREATE TABLE `group_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_setup`
--

INSERT INTO `group_setup` (`id`, `name`, `name_in_malay`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'STPM', '', 'STPM', 1, NULL, '2020-07-19 02:51:08', NULL, '2020-07-19 02:51:08');

-- --------------------------------------------------------

--
-- Table structure for table `guest`
--

CREATE TABLE `guest` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `email` varchar(580) DEFAULT '',
  `description` varchar(580) DEFAULT '',
  `nric` varchar(580) DEFAULT '',
  `mobile` int(20) DEFAULT NULL,
  `id_convocation` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hostel_inventory_registration`
--

CREATE TABLE `hostel_inventory_registration` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hostel_inventory_registration`
--

INSERT INTO `hostel_inventory_registration` (`id`, `name`, `name_in_malay`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Bed', 'Bed', 'Bed', 1, 1, '2020-07-11 12:05:59', NULL, '2020-07-11 12:05:59'),
(2, 'WM', 'Washing Machine', 'Washing Machine', 1, 1, '2020-07-11 12:06:17', NULL, '2020-07-11 12:06:17');

-- --------------------------------------------------------

--
-- Table structure for table `hostel_item_registration`
--

CREATE TABLE `hostel_item_registration` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hostel_registration`
--

CREATE TABLE `hostel_registration` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `type` varchar(50) DEFAULT '',
  `id_staff` int(20) DEFAULT 0,
  `contact_number` int(20) DEFAULT 0,
  `max_capacity` int(20) DEFAULT 0,
  `address` varchar(580) DEFAULT '',
  `landmark` varchar(580) DEFAULT '',
  `city` varchar(580) DEFAULT '',
  `id_state` int(20) DEFAULT NULL,
  `id_country` int(20) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hostel_registration`
--

INSERT INTO `hostel_registration` (`id`, `name`, `code`, `type`, `id_staff`, `contact_number`, `max_capacity`, `address`, `landmark`, `city`, `id_state`, `id_country`, `zipcode`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'OUM Hostel', 'OH', 'Male', 1, 2147483647, 100, 'address ', 'landmark', 'city', 2, 1, 34243, 1, 1, '2020-07-11 12:06:58', NULL, '2020-07-11 12:06:58');

-- --------------------------------------------------------

--
-- Table structure for table `hostel_room`
--

CREATE TABLE `hostel_room` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `short_code` varchar(50) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `level` int(2) DEFAULT 0,
  `id_parent` int(20) DEFAULT 0,
  `id_hostel` int(20) DEFAULT 0,
  `id_room_type` int(20) DEFAULT 0,
  `max_capacity` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hostel_room`
--

INSERT INTO `hostel_room` (`id`, `name`, `name_optional_language`, `short_code`, `code`, `level`, `id_parent`, `id_hostel`, `id_room_type`, `max_capacity`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Appartment1', '', 'Appartment1', 'Appartment1', 1, 0, 1, 0, 0, 1, 1, '2020-07-11 12:07:20', NULL, '2020-07-11 12:07:20'),
(2, '8-A Floor', '', '8-A Floor', '8-A Floor', 2, 1, 1, 0, 0, 1, 1, '2020-07-11 12:07:37', NULL, '2020-07-11 12:07:37'),
(3, '301', '', '301', '301', 3, 2, 1, 1, 1, 1, 1, '2020-07-11 12:08:07', NULL, '2020-07-11 12:08:07'),
(4, 'Room 8', '', 'Room 8', 'Room 8', 3, 2, 1, 1, 3, 1, 1, '2020-07-11 12:25:36', 1, '2020-07-11 12:25:36'),
(5, '302', '', '302', '302', 3, 2, 1, 1, 1, 1, 1, '2020-07-11 12:31:33', NULL, '2020-07-11 12:31:33');

-- --------------------------------------------------------

--
-- Table structure for table `hostel_room_type`
--

CREATE TABLE `hostel_room_type` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hostel_room_type`
--

INSERT INTO `hostel_room_type` (`id`, `name`, `name_in_malay`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Single Bed', 'Single Bed', 'SB', 1, 1, '2020-07-11 12:05:32', NULL, '2020-07-11 12:05:32');

-- --------------------------------------------------------

--
-- Table structure for table `intake`
--

CREATE TABLE `intake` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `year` int(5) DEFAULT NULL,
  `is_sibbling_discount` int(2) DEFAULT 0,
  `is_employee_discount` int(2) DEFAULT 0,
  `is_alumni_discount` int(2) DEFAULT 0,
  `is_temp_offer_letter` int(2) DEFAULT 1,
  `name_in_malay` varchar(200) DEFAULT NULL,
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `semester_sequence` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intake`
--

INSERT INTO `intake` (`id`, `name`, `year`, `is_sibbling_discount`, `is_employee_discount`, `is_alumni_discount`, `is_temp_offer_letter`, `name_in_malay`, `start_date`, `end_date`, `semester_sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'September 2020', 2020, 0, 0, 0, 1, 'September 2020', '2020-07-10 00:00:00', '2020-07-31 00:00:00', 'September', 1, NULL, '2020-07-10 23:58:44', NULL, '2020-07-10 23:58:44');

-- --------------------------------------------------------

--
-- Table structure for table `intake_has_programme`
--

CREATE TABLE `intake_has_programme` (
  `id` int(20) NOT NULL,
  `id_programme` int(20) DEFAULT NULL,
  `id_intake` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intake_has_programme`
--

INSERT INTO `intake_has_programme` (`id`, `id_programme`, `id_intake`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(23, 0, 1, 1, NULL, '2020-08-20 06:22:01', NULL, '2020-08-20 06:22:01'),
(24, 3, 1, 1, NULL, '2020-08-20 06:22:04', NULL, '2020-08-20 06:22:04'),
(25, 0, 1, 1, NULL, '2020-08-20 06:54:24', NULL, '2020-08-20 06:54:24'),
(26, 0, 1, 1, NULL, '2020-08-20 06:55:00', NULL, '2020-08-20 06:55:00');

-- --------------------------------------------------------

--
-- Table structure for table `internship_application`
--

CREATE TABLE `internship_application` (
  `id` int(20) NOT NULL,
  `application_number` varchar(50) DEFAULT '',
  `id_company_type` bigint(20) DEFAULT 0,
  `id_company` bigint(20) DEFAULT 0,
  `id_student` bigint(20) DEFAULT 0,
  `id_intake` bigint(20) DEFAULT 0,
  `id_program` bigint(20) DEFAULT 0,
  `description` varchar(500) DEFAULT '',
  `from_dt` datetime DEFAULT current_timestamp(),
  `to_dt` datetime DEFAULT NULL,
  `duration` varchar(50) DEFAULT '',
  `is_reporting` int(2) DEFAULT 0,
  `reason` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internship_application`
--

INSERT INTO `internship_application` (`id`, `application_number`, `id_company_type`, `id_company`, `id_student`, `id_intake`, `id_program`, `description`, `from_dt`, `to_dt`, `duration`, `is_reporting`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'INTR000001/2020', 1, 1, 4, 1, 1, '6 Months for Training', '2020-07-12 00:00:00', NULL, '6', 0, '', 1, NULL, '2020-07-11 13:17:49', NULL, '2020-07-11 13:17:49');

-- --------------------------------------------------------

--
-- Table structure for table `internship_company_has_program`
--

CREATE TABLE `internship_company_has_program` (
  `id` int(20) NOT NULL,
  `id_company` bigint(20) DEFAULT 0,
  `id_company_type` bigint(20) DEFAULT 0,
  `id_intake` bigint(20) DEFAULT 0,
  `id_program` bigint(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internship_company_has_program`
--

INSERT INTO `internship_company_has_program` (`id`, `id_company`, `id_company_type`, `id_intake`, `id_program`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 0, 0, 1, 0, NULL, '2020-07-11 13:17:08', NULL, '2020-07-11 13:17:08'),
(2, 1, 0, 0, 0, 0, NULL, '2020-08-10 06:39:31', NULL, '2020-08-10 06:39:31');

-- --------------------------------------------------------

--
-- Table structure for table `internship_company_registration`
--

CREATE TABLE `internship_company_registration` (
  `id` int(20) NOT NULL,
  `name` varchar(500) DEFAULT '',
  `registration_no` varchar(50) DEFAULT '',
  `person_in_charge` varchar(500) DEFAULT '',
  `email` varchar(500) DEFAULT '',
  `address` varchar(500) DEFAULT '',
  `contact_number` bigint(20) DEFAULT 0,
  `id_company_type` bigint(20) DEFAULT 0,
  `id_country` bigint(20) DEFAULT 0,
  `id_state` bigint(20) DEFAULT 0,
  `city` varchar(500) DEFAULT '',
  `zipcode` bigint(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internship_company_registration`
--

INSERT INTO `internship_company_registration` (`id`, `name`, `registration_no`, `person_in_charge`, `email`, `address`, `contact_number`, `id_company_type`, `id_country`, `id_state`, `city`, `zipcode`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Company one', 'Abc123', 'Abbhashah Chee Wee', 'azam@gmail.com', 'Addrss one', 2343423432, 1, 1, 1, 'City one', 23423, 1, 1, '2020-07-11 13:17:10', NULL, '2020-07-11 13:17:10');

-- --------------------------------------------------------

--
-- Table structure for table `internship_company_type`
--

CREATE TABLE `internship_company_type` (
  `id` int(20) NOT NULL,
  `name` varchar(500) DEFAULT '',
  `short_name` varchar(500) DEFAULT '',
  `name_in_malay` varchar(500) DEFAULT '',
  `code` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internship_company_type`
--

INSERT INTO `internship_company_type` (`id`, `name`, `short_name`, `name_in_malay`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'SAS', '', 'Software Services', 'Software Services', 1, 1, '2020-07-11 13:16:20', NULL, '2020-07-11 13:16:20');

-- --------------------------------------------------------

--
-- Table structure for table `internship_student_limit`
--

CREATE TABLE `internship_student_limit` (
  `id` bigint(20) NOT NULL,
  `max_limit` bigint(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `internship_student_limit`
--

INSERT INTO `internship_student_limit` (`id`, `max_limit`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 3, 1, NULL, '2020-07-11 13:05:53', NULL, '2020-07-11 13:05:53');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_allotment`
--

CREATE TABLE `inventory_allotment` (
  `id` int(20) NOT NULL,
  `level` int(20) DEFAULT 0,
  `id_room` int(20) DEFAULT 0,
  `id_inventory` int(20) DEFAULT 0,
  `quantity` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory_allotment`
--

INSERT INTO `inventory_allotment` (`id`, `level`, `id_room`, `id_inventory`, `quantity`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 0, 3, 1, 1, 0, NULL, '2020-07-11 12:07:59', NULL, '2020-07-11 12:07:59'),
(2, 0, 4, 1, 3, 0, NULL, '2020-07-11 12:23:07', NULL, '2020-07-11 12:23:07'),
(3, 0, 4, 0, 0, 0, NULL, '2020-07-11 12:25:43', NULL, '2020-07-11 12:25:43'),
(4, 0, 4, 2, 1, 0, NULL, '2020-07-11 12:26:02', NULL, '2020-07-11 12:26:02'),
(5, 0, 4, 0, 0, 0, NULL, '2020-07-11 12:26:11', NULL, '2020-07-11 12:26:11'),
(6, 0, 5, 1, 1, 0, NULL, '2020-07-11 12:31:24', NULL, '2020-07-11 12:31:24'),
(7, 0, 5, 2, 1, 0, NULL, '2020-07-11 12:31:30', NULL, '2020-07-11 12:31:30');

-- --------------------------------------------------------

--
-- Table structure for table `investment_application`
--

CREATE TABLE `investment_application` (
  `id` int(20) NOT NULL,
  `application_number` varchar(50) DEFAULT '',
  `description` varchar(580) DEFAULT '',
  `date_time` datetime DEFAULT current_timestamp(),
  `amount` float(20,2) DEFAULT NULL,
  `file_upload` varchar(500) DEFAULT NULL,
  `id_investment_bank` int(20) DEFAULT 0,
  `is_registered` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `investment_application_details`
--

CREATE TABLE `investment_application_details` (
  `id` int(20) NOT NULL,
  `id_application` varchar(50) DEFAULT '',
  `duration` int(20) DEFAULT 0,
  `duration_type` varchar(80) DEFAULT '',
  `total_amount` float(20,2) DEFAULT 0.00,
  `id_institution` int(20) DEFAULT 0,
  `id_investment_type` int(20) DEFAULT 0,
  `profit_rate` int(20) DEFAULT 0,
  `id_bank` int(20) DEFAULT 0,
  `bank_branch` int(20) DEFAULT 0,
  `maturity_date` datetime DEFAULT current_timestamp(),
  `remarks` varchar(580) DEFAULT '',
  `is_registered` int(2) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `investment_institution`
--

CREATE TABLE `investment_institution` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `contact_number` int(20) DEFAULT NULL,
  `id_bank` int(20) DEFAULT NULL,
  `contact_name` varchar(500) DEFAULT NULL,
  `address` varchar(580) DEFAULT '',
  `landmark` varchar(580) DEFAULT '',
  `id_state` int(20) DEFAULT 0,
  `id_country` int(20) DEFAULT 0,
  `city` varchar(580) DEFAULT '',
  `branch` varchar(580) DEFAULT '',
  `zipcode` int(10) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `investment_registration`
--

CREATE TABLE `investment_registration` (
  `id` int(20) NOT NULL,
  `id_application` int(20) DEFAULT 0,
  `id_application_detail` int(20) DEFAULT 0,
  `registration_number` varchar(50) DEFAULT '',
  `name` varchar(580) DEFAULT '',
  `amount` float(20,2) DEFAULT 0.00,
  `rate_of_interest` int(20) DEFAULT 0,
  `contact_person_one` varchar(500) DEFAULT '',
  `contact_email_one` varchar(580) DEFAULT '',
  `contact_person_two` varchar(500) DEFAULT '',
  `contact_email_two` varchar(580) DEFAULT '',
  `effective_date` datetime DEFAULT current_timestamp(),
  `maturity_date` datetime DEFAULT current_timestamp(),
  `id_investment_bank` int(20) DEFAULT 0,
  `id_institution` int(20) DEFAULT 0,
  `id_investment_type` int(20) DEFAULT 0,
  `id_bank` int(20) DEFAULT 0,
  `profit_rate` int(20) DEFAULT 0,
  `interest_to_bank` int(20) DEFAULT 0,
  `interest_day` int(20) DEFAULT 0,
  `account_code` varchar(50) DEFAULT '',
  `activity_code` varchar(50) DEFAULT '',
  `department_code` varchar(50) DEFAULT '',
  `fund_code` varchar(50) DEFAULT '',
  `file_upload` varchar(250) DEFAULT '',
  `duration` int(20) DEFAULT 0,
  `duration_type` varchar(50) DEFAULT '',
  `profit_amount` float(20,2) DEFAULT 0.00,
  `previous_status` int(20) DEFAULT 0,
  `previous_rate` int(20) DEFAULT 0,
  `is_reinvested` int(20) DEFAULT 0,
  `investment_status` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `investment_registration_withdraw`
--

CREATE TABLE `investment_registration_withdraw` (
  `id` int(20) NOT NULL,
  `id_application` int(20) DEFAULT 0,
  `id_application_detail` int(20) DEFAULT 0,
  `id_investment_registration` int(20) DEFAULT 0,
  `reference_number` varchar(50) DEFAULT '',
  `amount` float(20,2) DEFAULT 0.00,
  `withdraw_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `investment_type`
--

CREATE TABLE `investment_type` (
  `id` int(20) NOT NULL,
  `type` varchar(100) DEFAULT '',
  `contact_name` varchar(500) DEFAULT '',
  `contact_number` int(20) DEFAULT NULL,
  `account_code` varchar(50) DEFAULT '',
  `activity_code` varchar(50) DEFAULT '',
  `fund_code` varchar(50) DEFAULT '',
  `department_code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `is_scholarship_applicant_examination_details`
--

CREATE TABLE `is_scholarship_applicant_examination_details` (
  `id` int(20) NOT NULL,
  `id_scholarship_program` int(20) DEFAULT 0,
  `is_qualification_level` int(20) DEFAULT 1,
  `is_degree_awarded` int(20) DEFAULT 1,
  `is_specialization` int(20) DEFAULT 1,
  `is_class_degree` int(20) DEFAULT 1,
  `is_result` int(20) DEFAULT 1,
  `is_year` int(20) DEFAULT 1,
  `is_medium` int(20) DEFAULT 1,
  `is_college_country` int(20) DEFAULT 1,
  `is_college_name` int(20) DEFAULT 1,
  `is_certificate` int(20) DEFAULT 1,
  `is_transcript` int(20) DEFAULT 1,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `is_scholarship_applicant_examination_details`
--

INSERT INTO `is_scholarship_applicant_examination_details` (`id`, `id_scholarship_program`, `is_qualification_level`, `is_degree_awarded`, `is_specialization`, `is_class_degree`, `is_result`, `is_year`, `is_medium`, `is_college_country`, `is_college_name`, `is_certificate`, `is_transcript`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-07-22 02:12:09', NULL, '2020-07-22 02:12:09'),
(2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-07-24 13:19:07', NULL, '2020-07-24 13:19:07'),
(3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-07-24 23:42:06', NULL, '2020-07-24 23:42:06');

-- --------------------------------------------------------

--
-- Table structure for table `is_scholarship_applicant_personel_details`
--

CREATE TABLE `is_scholarship_applicant_personel_details` (
  `id` int(20) NOT NULL,
  `id_scholarship_program` int(20) DEFAULT 0,
  `is_year` int(20) DEFAULT 1,
  `is_id_intake` int(20) DEFAULT 1,
  `is_id_program` int(20) DEFAULT 1,
  `is_full_name` int(20) DEFAULT 1,
  `is_salutation` int(20) DEFAULT 1,
  `is_first_name` int(20) DEFAULT 1,
  `is_last_name` int(20) DEFAULT 1,
  `is_nric` int(20) DEFAULT 1,
  `is_passport` int(20) DEFAULT 1,
  `is_phone` int(20) DEFAULT 1,
  `is_email_id` int(20) DEFAULT 1,
  `is_password` int(20) DEFAULT 1,
  `is_passport_expiry_date` int(20) DEFAULT 1,
  `is_gender` int(20) DEFAULT 1,
  `is_date_of_birth` int(20) DEFAULT 1,
  `is_martial_status` int(20) DEFAULT 1,
  `is_religion` int(20) DEFAULT 1,
  `is_nationality` int(20) DEFAULT 1,
  `is_is_hostel` int(20) DEFAULT 1,
  `is_id_degree_type` int(20) DEFAULT 1,
  `is_id_race` int(20) DEFAULT 1,
  `is_mail_address1` int(20) DEFAULT 1,
  `is_mail_address2` int(20) DEFAULT 1,
  `is_mailing_country` int(20) DEFAULT 1,
  `is_mailing_state` int(20) DEFAULT 1,
  `is_mailing_city` int(20) DEFAULT 1,
  `is_mailing_zipcode` int(20) DEFAULT 1,
  `is_permanent_address1` int(20) DEFAULT 1,
  `is_permanent_address2` int(20) DEFAULT 1,
  `is_permanent_country` int(20) DEFAULT 1,
  `is_permanent_state` int(20) DEFAULT 1,
  `is_permanent_city` int(20) DEFAULT 1,
  `is_permanent_zipcode` int(20) DEFAULT 1,
  `is_sibbling_discount` int(20) DEFAULT 1,
  `is_employee_discount` int(20) DEFAULT 1,
  `is_applicant_status` int(20) DEFAULT 1,
  `is_is_sibbling_discount` int(20) DEFAULT 1,
  `is_is_employee_discount` int(20) DEFAULT 1,
  `is_id_type` int(20) DEFAULT 1,
  `is_passport_number` int(20) DEFAULT 1,
  `email_verified` int(2) DEFAULT 0,
  `is_submitted` int(2) DEFAULT 0,
  `is_updated` int(2) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `approved_by` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `is_scholarship_applicant_personel_details`
--

INSERT INTO `is_scholarship_applicant_personel_details` (`id`, `id_scholarship_program`, `is_year`, `is_id_intake`, `is_id_program`, `is_full_name`, `is_salutation`, `is_first_name`, `is_last_name`, `is_nric`, `is_passport`, `is_phone`, `is_email_id`, `is_password`, `is_passport_expiry_date`, `is_gender`, `is_date_of_birth`, `is_martial_status`, `is_religion`, `is_nationality`, `is_is_hostel`, `is_id_degree_type`, `is_id_race`, `is_mail_address1`, `is_mail_address2`, `is_mailing_country`, `is_mailing_state`, `is_mailing_city`, `is_mailing_zipcode`, `is_permanent_address1`, `is_permanent_address2`, `is_permanent_country`, `is_permanent_state`, `is_permanent_city`, `is_permanent_zipcode`, `is_sibbling_discount`, `is_employee_discount`, `is_applicant_status`, `is_is_sibbling_discount`, `is_is_employee_discount`, `is_id_type`, `is_passport_number`, `email_verified`, `is_submitted`, `is_updated`, `reason`, `status`, `approved_by`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', NULL, 0, NULL, '2020-07-22 02:12:09', NULL, '2020-07-22 02:12:09'),
(2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', NULL, 0, NULL, '2020-07-24 13:19:07', NULL, '2020-07-24 13:19:07'),
(3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', NULL, 0, NULL, '2020-07-24 23:42:06', NULL, '2020-07-24 23:42:06');

-- --------------------------------------------------------

--
-- Table structure for table `is_scholar_applicant_employment_status`
--

CREATE TABLE `is_scholar_applicant_employment_status` (
  `id` int(20) NOT NULL,
  `id_scholarship_program` int(10) DEFAULT 0,
  `is_company_name` int(20) DEFAULT 1,
  `is_company_address` int(20) DEFAULT 1,
  `is_telephone` int(20) DEFAULT 1,
  `is_fax_num` int(20) DEFAULT 1,
  `is_designation` int(20) DEFAULT 1,
  `is_position` int(20) DEFAULT 1,
  `is_service_year` int(20) DEFAULT 1,
  `is_industry` int(20) DEFAULT 1,
  `is_job_desc` int(20) DEFAULT 1,
  `is_employment_letter` int(20) DEFAULT 1,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `is_scholar_applicant_employment_status`
--

INSERT INTO `is_scholar_applicant_employment_status` (`id`, `id_scholarship_program`, `is_company_name`, `is_company_address`, `is_telephone`, `is_fax_num`, `is_designation`, `is_position`, `is_service_year`, `is_industry`, `is_job_desc`, `is_employment_letter`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-07-22 02:12:09', NULL, '2020-07-22 02:12:09'),
(2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-07-24 13:19:07', NULL, '2020-07-24 13:19:07'),
(3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-07-24 23:42:06', NULL, '2020-07-24 23:42:06');

-- --------------------------------------------------------

--
-- Table structure for table `is_scholar_applicant_family_details`
--

CREATE TABLE `is_scholar_applicant_family_details` (
  `id` int(20) NOT NULL,
  `id_scholarship_program` int(20) DEFAULT 0,
  `is_father_name` int(20) DEFAULT 1,
  `is_mother_name` int(20) DEFAULT 1,
  `is_father_deceased` int(20) DEFAULT 1,
  `is_father_occupation` int(20) DEFAULT 1,
  `is_no_siblings` int(20) DEFAULT 1,
  `is_est_fee` int(20) DEFAULT 1,
  `is_family_annual_income` int(20) DEFAULT 1,
  `reason` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `is_scholar_applicant_family_details`
--

INSERT INTO `is_scholar_applicant_family_details` (`id`, `id_scholarship_program`, `is_father_name`, `is_mother_name`, `is_father_deceased`, `is_father_occupation`, `is_no_siblings`, `is_est_fee`, `is_family_annual_income`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, '', 0, NULL, '2020-07-22 02:12:09', NULL, '2020-07-22 02:12:09'),
(2, 2, 1, 1, 1, 1, 1, 1, 1, '', 0, NULL, '2020-07-24 13:19:07', NULL, '2020-07-24 13:19:07'),
(3, 3, 1, 1, 1, 1, 1, 1, 1, '', 0, NULL, '2020-07-24 23:42:06', NULL, '2020-07-24 23:42:06');

-- --------------------------------------------------------

--
-- Table structure for table `journal`
--

CREATE TABLE `journal` (
  `id` int(20) NOT NULL,
  `journal_number` varchar(20) DEFAULT NULL,
  `date` datetime DEFAULT current_timestamp(),
  `reason` varchar(520) DEFAULT NULL,
  `total_amount` varchar(20) DEFAULT NULL,
  `id_financial_year` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp(),
  `approval_status` int(10) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `journal_details`
--

CREATE TABLE `journal_details` (
  `id` int(20) NOT NULL,
  `id_journal` int(20) DEFAULT NULL,
  `journal_type` varchar(50) DEFAULT '',
  `credit_amount` varchar(50) DEFAULT '',
  `debit_amount` varchar(50) DEFAULT '',
  `id_account_code` int(20) DEFAULT NULL,
  `id_activity_code` int(20) DEFAULT NULL,
  `id_department_code` int(20) DEFAULT NULL,
  `id_fund_code` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `landscape_course_type`
--

CREATE TABLE `landscape_course_type` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `landscape_course_type`
--

INSERT INTO `landscape_course_type` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'MPU', '', 1, NULL, '2020-07-19 09:21:49', NULL, '2020-07-19 09:21:49'),
(2, 'UCS', '', 1, NULL, '2020-07-19 09:22:08', NULL, '2020-07-19 09:22:08'),
(3, 'Common Core', '', 1, NULL, '2020-07-19 09:22:08', NULL, '2020-07-19 09:22:08'),
(4, 'Discipline Core', '', 1, NULL, '2020-07-19 09:22:08', NULL, '2020-07-19 09:22:08'),
(5, 'Elective', '', 1, NULL, '2020-07-19 09:22:08', NULL, '2020-07-19 09:22:08'),
(6, 'Industrial Training', '', 1, NULL, '2020-07-19 09:22:08', NULL, '2020-07-19 09:22:08');

-- --------------------------------------------------------

--
-- Table structure for table `late_registration`
--

CREATE TABLE `late_registration` (
  `id` int(20) NOT NULL,
  `semester_type` varchar(50) DEFAULT '',
  `effective_date` datetime DEFAULT current_timestamp(),
  `no_of_days` int(20) DEFAULT NULL,
  `penalty` varchar(20) DEFAULT '',
  `no_of_days_for_special_appeal` int(20) DEFAULT NULL,
  `special_appeal_penalty` varchar(20) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `main_invoice`
--

CREATE TABLE `main_invoice` (
  `id` int(20) NOT NULL,
  `type` varchar(50) DEFAULT '',
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `invoice_number` varchar(50) DEFAULT '',
  `type_of_invoice` varchar(50) DEFAULT '',
  `date_time` datetime DEFAULT current_timestamp(),
  `remarks` varchar(520) DEFAULT '',
  `id_application` int(10) DEFAULT NULL,
  `id_sponser` int(20) DEFAULT 0,
  `id_student` int(10) DEFAULT NULL,
  `currency` varchar(200) DEFAULT '',
  `total_amount` float(20,2) DEFAULT 0.00,
  `invoice_total` float(20,2) DEFAULT 0.00,
  `total_discount` float(20,2) DEFAULT 0.00,
  `balance_amount` float(20,2) DEFAULT 0.00,
  `paid_amount` float(20,2) DEFAULT 0.00,
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_invoice`
--

INSERT INTO `main_invoice` (`id`, `type`, `id_program`, `id_intake`, `invoice_number`, `type_of_invoice`, `date_time`, `remarks`, `id_application`, `id_sponser`, `id_student`, `currency`, `total_amount`, `invoice_total`, `total_discount`, `balance_amount`, `paid_amount`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(29, 'Applicant', 3, 1, 'INV000001/2020', '', '2020-08-20 07:09:45', 'Applicant Payable Amount', 1, 0, 28, 'MYR', 21780.00, 21780.00, 0.00, 1780.00, 20000.00, '', 1, 1, '2020-08-20 07:09:45', NULL, '2020-08-20 07:09:45'),
(30, 'Applicant', 3, 1, 'INV000002/2020', '', '2020-08-20 12:00:04', 'Applicant Payable Amount', 1, 0, 29, 'MYR', 300.00, 300.00, 0.00, 300.00, 0.00, '', 1, 1, '2020-08-20 12:00:04', NULL, '2020-08-20 12:00:04');

-- --------------------------------------------------------

--
-- Table structure for table `main_invoice_details`
--

CREATE TABLE `main_invoice_details` (
  `id` int(20) NOT NULL,
  `id_main_invoice` int(10) DEFAULT NULL,
  `id_fee_item` int(10) DEFAULT NULL,
  `amount` float(20,2) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `main_invoice_details`
--

INSERT INTO `main_invoice_details` (`id`, `id_main_invoice`, `id_fee_item`, `amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(26, 17, 3, 3333.00, 1, 1, '2020-08-17 12:38:03', NULL, '2020-08-17 12:38:03'),
(27, 17, 1, 2973.00, 1, 1, '2020-08-17 12:38:03', NULL, '2020-08-17 12:38:03'),
(28, 18, 3, 3333.00, 1, 1, '2020-08-17 12:47:45', NULL, '2020-08-17 12:47:45'),
(29, 18, 3, 3333.00, 1, 1, '2020-08-17 12:47:45', NULL, '2020-08-17 12:47:45'),
(30, 18, 1, 2973.00, 1, 1, '2020-08-17 12:47:45', NULL, '2020-08-17 12:47:45'),
(31, 19, 3, 3333.00, 1, 1, '2020-08-17 12:48:34', NULL, '2020-08-17 12:48:34'),
(32, 19, 3, 3333.00, 1, 1, '2020-08-17 12:48:34', NULL, '2020-08-17 12:48:34'),
(33, 19, 1, 2973.00, 1, 1, '2020-08-17 12:48:34', NULL, '2020-08-17 12:48:34'),
(34, 20, 2, 1000.00, 1, 1, '2020-08-17 12:50:39', NULL, '2020-08-17 12:50:39'),
(35, 21, NULL, NULL, 1, 1, '2020-08-17 14:28:16', NULL, '2020-08-17 14:28:16'),
(36, 22, 2, 170.00, 1, 1, '2020-08-17 14:31:32', NULL, '2020-08-17 14:31:32'),
(37, 23, 2, 1000.00, 1, 1, '2020-08-18 13:58:07', NULL, '2020-08-18 13:58:07'),
(38, 23, 3, 800.00, 1, 1, '2020-08-18 13:58:07', NULL, '2020-08-18 13:58:07'),
(39, 23, 1, 2000.00, 1, 1, '2020-08-18 13:58:07', NULL, '2020-08-18 13:58:07'),
(40, 24, 2, 1000.00, 1, 1, '2020-08-19 08:53:05', NULL, '2020-08-19 08:53:05'),
(41, 24, 3, 800.00, 1, 1, '2020-08-19 08:53:05', NULL, '2020-08-19 08:53:05'),
(42, 24, 1, 2000.00, 1, 1, '2020-08-19 08:53:05', NULL, '2020-08-19 08:53:05'),
(43, 25, 2, 1000.00, 1, 1, '2020-08-19 09:37:01', NULL, '2020-08-19 09:37:01'),
(44, 26, 2, 300.00, 1, 1, '2020-08-20 04:44:38', NULL, '2020-08-20 04:44:38'),
(45, 26, 3, 400.00, 1, 1, '2020-08-20 04:44:38', NULL, '2020-08-20 04:44:38'),
(46, 26, 1, 380.00, 1, 1, '2020-08-20 04:44:38', NULL, '2020-08-20 04:44:38'),
(47, 27, 2, 400.00, 1, 1, '2020-08-20 05:57:06', NULL, '2020-08-20 05:57:06'),
(48, 27, 1, 300.00, 1, 1, '2020-08-20 05:57:06', NULL, '2020-08-20 05:57:06'),
(49, 28, 3, 200.00, 1, 1, '2020-08-20 06:01:56', NULL, '2020-08-20 06:01:56'),
(50, 28, 1, 200.00, 1, 1, '2020-08-20 06:01:56', NULL, '2020-08-20 06:01:56'),
(51, 29, 6, 18000.00, 1, 1, '2020-08-20 07:09:45', NULL, '2020-08-20 07:09:45'),
(52, 29, 7, 1800.00, 1, 1, '2020-08-20 07:09:45', NULL, '2020-08-20 07:09:45'),
(53, 29, 8, 1680.00, 1, 1, '2020-08-20 07:09:45', NULL, '2020-08-20 07:09:45'),
(54, 29, 5, 300.00, 1, 1, '2020-08-20 07:09:45', NULL, '2020-08-20 07:09:45'),
(55, 30, 8, 300.00, 1, 1, '2020-08-20 12:00:04', NULL, '2020-08-20 12:00:04');

-- --------------------------------------------------------

--
-- Table structure for table `main_invoice_discount_details`
--

CREATE TABLE `main_invoice_discount_details` (
  `id` int(20) NOT NULL,
  `id_main_invoice` int(20) DEFAULT 0,
  `name` varchar(500) DEFAULT '',
  `amount` float(20,2) DEFAULT 0.00,
  `id_student` int(20) DEFAULT 0,
  `id_reference` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `marital_status_setup`
--

CREATE TABLE `marital_status_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `sequence` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marital_status_setup`
--

INSERT INTO `marital_status_setup` (`id`, `name`, `sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Single', 1, 1, NULL, '2020-07-22 01:31:35', NULL, '2020-07-22 01:31:35'),
(2, 'Married', 2, 1, NULL, '2020-07-22 02:29:21', NULL, '2020-07-22 02:29:21'),
(3, 'Divorced', 3, 0, NULL, '2020-07-22 02:29:35', NULL, '2020-07-22 02:29:35'),
(4, 'Widowed', 4, 1, NULL, '2020-07-22 02:29:52', NULL, '2020-07-22 02:29:52');

-- --------------------------------------------------------

--
-- Table structure for table `mark_distribution`
--

CREATE TABLE `mark_distribution` (
  `id` int(20) NOT NULL,
  `id_program` int(20) DEFAULT NULL,
  `id_intake` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT NULL,
  `id_course_registered` int(20) DEFAULT NULL COMMENT 'its a id_program_landscape_course typed wrongly',
  `status` int(2) DEFAULT NULL,
  `reason` varchar(1024) DEFAULT '',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mark_distribution`
--

INSERT INTO `mark_distribution` (`id`, `id_program`, `id_intake`, `id_course`, `id_course_registered`, `status`, `reason`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 3, 4, 1, '', 1, '2020-08-07 09:37:40', 1, '2020-08-07 09:37:40'),
(2, 1, 1, 3, 4, 1, '', 1, '2020-08-07 10:10:48', 1, '2020-08-07 10:10:48'),
(3, 1, 1, 1, 6, 1, '', 1, '2020-08-09 05:19:39', 1, '2020-08-09 05:19:39'),
(4, 1, 1, 3, 10, 1, '', 1, '2020-08-13 10:10:57', 1, '2020-08-13 10:10:57'),
(5, 1, 1, 3, 10, 1, '', 1, '2020-08-13 10:40:00', 1, '2020-08-13 10:40:00'),
(6, 1, 1, 2, 11, 1, '', 1, '2020-08-13 10:43:33', 1, '2020-08-13 10:43:33');

-- --------------------------------------------------------

--
-- Table structure for table `mark_distribution_details`
--

CREATE TABLE `mark_distribution_details` (
  `id` int(20) NOT NULL,
  `id_mark_distribution` int(20) DEFAULT NULL,
  `id_component` int(20) DEFAULT NULL,
  `is_pass_compulsary` int(2) DEFAULT NULL,
  `pass_marks` int(20) DEFAULT NULL,
  `max_marks` int(20) DEFAULT NULL,
  `weightage` int(20) DEFAULT NULL,
  `attendance_status` int(2) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mark_distribution_details`
--

INSERT INTO `mark_distribution_details` (`id`, `id_mark_distribution`, `id_component`, `is_pass_compulsary`, `pass_marks`, `max_marks`, `weightage`, `attendance_status`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, 40, 100, NULL, 1, NULL, NULL, '2020-08-07 09:37:58', NULL, '2020-08-07 09:37:58'),
(2, 2, 1, 1, 122, 12, NULL, 1, NULL, NULL, '2020-08-07 10:10:29', NULL, '2020-08-07 10:10:29'),
(3, 1, 2, 1, 25, 50, NULL, 1, NULL, NULL, '2020-08-07 14:22:14', NULL, '2020-08-07 14:22:14'),
(4, 1, 3, 1, 55, 150, NULL, 1, NULL, NULL, '2020-08-07 14:22:35', NULL, '2020-08-07 14:22:35'),
(5, 3, 1, 1, 20, 50, NULL, 1, NULL, NULL, '2020-08-09 05:19:24', NULL, '2020-08-09 05:19:24'),
(6, 3, 2, 1, 20, 50, NULL, 1, NULL, NULL, '2020-08-09 05:19:29', NULL, '2020-08-09 05:19:29'),
(7, 3, 3, 1, 40, 100, NULL, 1, NULL, NULL, '2020-08-09 05:19:38', NULL, '2020-08-09 05:19:38'),
(8, 4, 2, 1, 40, 100, NULL, 1, NULL, NULL, '2020-08-13 10:10:53', NULL, '2020-08-13 10:10:53'),
(9, 5, 1, 1, 40, 100, NULL, 1, NULL, NULL, '2020-08-13 10:39:46', NULL, '2020-08-13 10:39:46'),
(10, 5, 2, 1, 40, 100, NULL, 1, NULL, NULL, '2020-08-13 10:39:50', NULL, '2020-08-13 10:39:50'),
(11, 5, 3, 1, 80, 200, NULL, 1, NULL, NULL, '2020-08-13 10:39:59', NULL, '2020-08-13 10:39:59'),
(12, 6, 1, 1, 40, 100, NULL, 1, NULL, NULL, '2020-08-13 10:43:20', NULL, '2020-08-13 10:43:20'),
(13, 6, 2, 1, 40, 100, NULL, 1, NULL, NULL, '2020-08-13 10:43:24', NULL, '2020-08-13 10:43:24'),
(14, 6, 3, 1, 40, 100, NULL, 1, NULL, NULL, '2020-08-13 10:43:31', NULL, '2020-08-13 10:43:31');

-- --------------------------------------------------------

--
-- Table structure for table `mode_of_category`
--

CREATE TABLE `mode_of_category` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `category_code` varchar(120) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mode_of_program`
--

CREATE TABLE `mode_of_program` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nationality`
--

CREATE TABLE `nationality` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nonpo_detail`
--

CREATE TABLE `nonpo_detail` (
  `id` bigint(20) NOT NULL,
  `id_po_entry` bigint(20) DEFAULT NULL,
  `cr_account` varchar(20) DEFAULT NULL,
  `cr_activity` varchar(20) DEFAULT NULL,
  `cr_department` varchar(20) DEFAULT NULL,
  `cr_fund` varchar(20) DEFAULT NULL,
  `dt_account` varchar(20) DEFAULT NULL,
  `dt_activity` varchar(20) DEFAULT NULL,
  `dt_department` varchar(20) DEFAULT NULL,
  `dt_fund` varchar(20) DEFAULT NULL,
  `id_category` bigint(20) DEFAULT NULL,
  `id_sub_category` bigint(20) DEFAULT NULL,
  `id_item` bigint(20) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` float(20,2) DEFAULT NULL,
  `id_tax` float(10,2) DEFAULT NULL,
  `tax_price` float(10,2) DEFAULT NULL,
  `total_final` float(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nonpo_entry`
--

CREATE TABLE `nonpo_entry` (
  `id` bigint(20) NOT NULL,
  `id_financial_year` bigint(20) DEFAULT NULL,
  `type` varchar(100) DEFAULT '',
  `nonpo_entry_date` date DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `reason` varchar(500) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `created_date` datetime DEFAULT current_timestamp(),
  `nonpo_number` varchar(50) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `id_vendor` bigint(20) DEFAULT NULL,
  `id_pr` int(20) DEFAULT NULL,
  `id_department` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `organisation`
--

CREATE TABLE `organisation` (
  `id` int(20) NOT NULL,
  `name` varchar(500) DEFAULT '',
  `short_name` varchar(500) DEFAULT '',
  `name_in_malay` varchar(500) DEFAULT '',
  `url` varchar(500) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_registrar` int(20) DEFAULT 0,
  `date_time` datetime DEFAULT current_timestamp(),
  `contact_number` int(20) DEFAULT 0,
  `email` varchar(200) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `id_state` int(20) DEFAULT 0,
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organisation`
--

INSERT INTO `organisation` (`id`, `name`, `short_name`, `name_in_malay`, `url`, `id_country`, `id_registrar`, `date_time`, `contact_number`, `email`, `address1`, `address2`, `id_state`, `city`, `zipcode`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Asia E-University', 'AEU', '', 'https://aeu.edu.my/', 1, 1, '2020-07-10 00:00:00', 2147483647, 'conta@gmail.com', 'KL', '', 1, 'KL', 345465, 0, 1, '2020-07-10 23:25:36', NULL, '2020-07-10 23:25:36');

-- --------------------------------------------------------

--
-- Table structure for table `organisation_comitee`
--

CREATE TABLE `organisation_comitee` (
  `id` int(20) NOT NULL,
  `id_organisation` int(20) DEFAULT 0,
  `role` varchar(50) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `nric` varchar(100) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organisation_comitee`
--

INSERT INTO `organisation_comitee` (`id`, `id_organisation`, `role`, `name`, `nric`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Vice Chancellor', 'Emeritus Prof Datuk Dr Hassan Said', '740501026919', '2020-08-01 00:00:00', NULL, NULL, '2020-08-08 23:35:59', NULL, '2020-08-08 23:35:59');

-- --------------------------------------------------------

--
-- Table structure for table `organisation_has_training_center`
--

CREATE TABLE `organisation_has_training_center` (
  `id` int(20) NOT NULL,
  `id_organisation` int(20) DEFAULT 0,
  `name` varchar(500) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `complete_code` varchar(500) DEFAULT '',
  `id_contact_person` int(20) DEFAULT 0,
  `contact_number` int(20) DEFAULT 0,
  `email` varchar(200) DEFAULT '',
  `fax` varchar(200) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `location` varchar(512) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_state` int(20) DEFAULT 0,
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organisation_has_training_center`
--

INSERT INTO `organisation_has_training_center` (`id`, `id_organisation`, `name`, `code`, `complete_code`, `id_contact_person`, `contact_number`, `email`, `fax`, `address1`, `address2`, `location`, `id_country`, `id_state`, `city`, `zipcode`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(3, 1, 'KL', 'KL', '', 2, 123, 'as@cms.com', '123', 'KL', 'KL', 'KL', 1, 1, 'KL', 123, 1, NULL, '2020-08-20 07:07:01', NULL, '2020-08-20 07:07:01');

-- --------------------------------------------------------

--
-- Table structure for table `organisation_training_center_has_program`
--

CREATE TABLE `organisation_training_center_has_program` (
  `id` int(20) NOT NULL,
  `id_organisation` int(20) DEFAULT 0,
  `id_training_center` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organisation_training_center_has_program`
--

INSERT INTO `organisation_training_center_has_program` (`id`, `id_organisation`, `id_training_center`, `id_program`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 1, 1, 2, 0, NULL, '2020-08-18 08:18:26', NULL, '2020-08-18 08:18:26'),
(3, 1, 1, 1, 0, NULL, '2020-08-18 13:51:39', NULL, '2020-08-18 13:51:39'),
(4, 1, 2, 1, 0, NULL, '2020-08-19 13:10:33', NULL, '2020-08-19 13:10:33'),
(5, 1, 2, 2, 0, NULL, '2020-08-19 13:10:46', NULL, '2020-08-19 13:10:46'),
(6, 1, 3, 3, 0, NULL, '2020-08-20 07:07:12', NULL, '2020-08-20 07:07:12');

-- --------------------------------------------------------

--
-- Table structure for table `other_documents`
--

CREATE TABLE `other_documents` (
  `id` int(20) NOT NULL,
  `id_student` int(10) DEFAULT NULL,
  `doc_name` varchar(120) DEFAULT '',
  `doc_file` varchar(520) DEFAULT '',
  `remarks` varchar(120) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `other_documents`
--

INSERT INTO `other_documents` (`id`, `id_student`, `doc_name`, `doc_file`, `remarks`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 6, 'Other Documents', 'scholarship.jpeg', 'Remarks', NULL, NULL, '2020-08-12 10:07:24', NULL, '2020-08-12 10:07:24');

-- --------------------------------------------------------

--
-- Table structure for table `partner_category`
--

CREATE TABLE `partner_category` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partner_category`
--

INSERT INTO `partner_category` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Franchise', 'F', 1, NULL, '2020-07-13 04:46:25', NULL, '2020-07-13 04:46:25'),
(2, 'Joint Award', 'J', 1, NULL, '2020-07-13 04:46:44', NULL, '2020-07-13 04:46:44'),
(3, 'Learning Centre', 'LC', 1, NULL, '2020-08-08 23:38:31', NULL, '2020-08-08 23:38:31'),
(4, 'Dual Award', 'DUAL', 1, NULL, '2020-08-10 20:23:43', NULL, '2020-08-10 20:23:43');

-- --------------------------------------------------------

--
-- Table structure for table `partner_university`
--

CREATE TABLE `partner_university` (
  `id` int(20) NOT NULL,
  `name` varchar(500) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `id_partner_category` int(20) DEFAULT 0,
  `id_partner_university` int(20) DEFAULT 0,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `certificate` varchar(200) DEFAULT '',
  `short_name` varchar(500) DEFAULT '',
  `name_in_malay` varchar(500) DEFAULT '',
  `url` varchar(500) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_registrar` int(20) DEFAULT 0,
  `date_time` datetime DEFAULT current_timestamp(),
  `contact_number` int(20) DEFAULT 0,
  `email` varchar(200) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `location` varchar(512) DEFAULT '',
  `id_state` int(20) DEFAULT 0,
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partner_university`
--

INSERT INTO `partner_university` (`id`, `name`, `code`, `id_partner_category`, `id_partner_university`, `start_date`, `end_date`, `certificate`, `short_name`, `name_in_malay`, `url`, `id_country`, `id_registrar`, `date_time`, `contact_number`, `email`, `address1`, `address2`, `location`, `id_state`, `city`, `zipcode`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(4, 'AEU Petaling Jaya', 'AEUPJ', 3, 0, '1970-01-01 00:00:00', '1970-01-01 00:00:00', '13c19047d97c2536020d6126b3b60265.png', 'AEU', 'AEU', 'www.aeu.edu.my', 1, 0, '2020-07-14 06:58:11', 12345678, 'hazzna@gmail.com', 'KL', '', '', 1, 'KL', 234332, 1, 1, '2020-07-14 06:58:11', 1, '2020-07-14 06:58:11');

-- --------------------------------------------------------

--
-- Table structure for table `partner_university_comitee`
--

CREATE TABLE `partner_university_comitee` (
  `id` int(20) NOT NULL,
  `id_partner_university` int(20) DEFAULT 0,
  `role` varchar(50) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `nric` varchar(100) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partner_university_comitee`
--

INSERT INTO `partner_university_comitee` (`id`, `id_partner_university`, `role`, `name`, `nric`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Chancellor', 'ABC', '34234234', '2020-07-13 00:00:00', NULL, NULL, '2020-07-13 08:56:43', NULL, '2020-07-13 08:56:43'),
(2, 4, 'Chancellor', 'Shaheel', 'NR4432', '2020-07-01 00:00:00', NULL, NULL, '2020-07-14 06:58:44', NULL, '2020-07-14 06:58:44'),
(3, 4, 'Registrar', 'Bhasha Abdullah', '221278564', '2020-08-25 00:00:00', NULL, NULL, '2020-08-12 03:57:07', NULL, '2020-08-12 03:57:07');

-- --------------------------------------------------------

--
-- Table structure for table `payment_type`
--

CREATE TABLE `payment_type` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_type`
--

INSERT INTO `payment_type` (`id`, `name`, `code`, `name_in_malay`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Cash', 'Cash', '', 1, NULL, '2020-08-20 10:08:10', NULL, '2020-08-20 10:08:10'),
(2, 'Cash Deposit', 'Bank Transfer', '', 1, NULL, '2020-08-20 10:09:30', NULL, '2020-08-20 10:09:30'),
(3, 'Online Transfer', 'Bank Transfer', '', 1, NULL, '2020-08-20 10:11:17', NULL, '2020-08-20 10:11:17');

-- --------------------------------------------------------

--
-- Table structure for table `performa_invoice`
--

CREATE TABLE `performa_invoice` (
  `id` int(20) NOT NULL,
  `type_of_invoice` varchar(50) DEFAULT '',
  `performa_number` varchar(50) DEFAULT '',
  `total_amount` varchar(20) DEFAULT '',
  `balance_amount` varchar(20) DEFAULT '0',
  `paid_amount` varchar(20) DEFAULT '0',
  `date_time` datetime DEFAULT current_timestamp(),
  `remarks` varchar(520) DEFAULT NULL,
  `id_application` int(10) DEFAULT NULL,
  `id_student` int(10) DEFAULT NULL,
  `approval_status` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `performa_invoice_details`
--

CREATE TABLE `performa_invoice_details` (
  `id` int(20) NOT NULL,
  `id_performa` int(20) DEFAULT NULL,
  `id_fee_item` int(20) DEFAULT NULL,
  `amount` varchar(20) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `personal_billing_details`
--

CREATE TABLE `personal_billing_details` (
  `id` int(20) NOT NULL,
  `id_vendor` varchar(120) DEFAULT '',
  `name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `email` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `po_detail`
--

CREATE TABLE `po_detail` (
  `id` bigint(20) NOT NULL,
  `id_po_entry` bigint(20) DEFAULT NULL,
  `cr_account` varchar(20) DEFAULT NULL,
  `cr_activity` varchar(20) DEFAULT NULL,
  `cr_department` varchar(20) DEFAULT NULL,
  `cr_fund` varchar(20) DEFAULT NULL,
  `dt_account` varchar(20) DEFAULT NULL,
  `dt_activity` varchar(20) DEFAULT NULL,
  `dt_department` varchar(20) DEFAULT NULL,
  `dt_fund` varchar(20) DEFAULT NULL,
  `id_category` bigint(20) DEFAULT NULL,
  `id_sub_category` bigint(20) DEFAULT NULL,
  `id_item` bigint(20) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `balance_qty` int(20) DEFAULT NULL,
  `received_qty` int(20) DEFAULT 0,
  `price` float(20,2) DEFAULT NULL,
  `id_tax` float(10,2) DEFAULT NULL,
  `tax_price` float(10,2) DEFAULT NULL,
  `total_final` float(10,2) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `po_entry`
--

CREATE TABLE `po_entry` (
  `id` bigint(20) NOT NULL,
  `id_financial_year` bigint(20) DEFAULT NULL,
  `po_entry_date` date DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `type` varchar(100) DEFAULT '',
  `reason` varchar(500) DEFAULT NULL,
  `created_date` datetime DEFAULT current_timestamp(),
  `po_number` varchar(50) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `id_vendor` bigint(20) DEFAULT NULL,
  `id_pr` int(20) DEFAULT NULL,
  `id_department` bigint(20) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `procurement_category`
--

CREATE TABLE `procurement_category` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `description` varchar(580) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `procurement_item`
--

CREATE TABLE `procurement_item` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `description` varchar(580) DEFAULT '',
  `id_procurement_category` int(20) DEFAULT NULL,
  `id_procurement_sub_category` int(250) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `procurement_limit`
--

CREATE TABLE `procurement_limit` (
  `id` int(20) NOT NULL,
  `name` varchar(80) DEFAULT '',
  `from_limit` varchar(20) DEFAULT '',
  `to_limit` varchar(20) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `procurement_sub_category`
--

CREATE TABLE `procurement_sub_category` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `description` varchar(580) DEFAULT '',
  `id_procurement_category` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile_details`
--

CREATE TABLE `profile_details` (
  `id` int(20) NOT NULL,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `id_type` varchar(120) DEFAULT '',
  `passport_number` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `nationality_type` varchar(120) DEFAULT '',
  `id_race` int(10) DEFAULT NULL,
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `mailing_zipcode` varchar(120) DEFAULT '',
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `nric` varchar(50) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp(),
  `id_student` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_details`
--

INSERT INTO `profile_details` (`id`, `full_name`, `salutation`, `first_name`, `last_name`, `id_type`, `passport_number`, `phone`, `email_id`, `passport_expiry_date`, `gender`, `date_of_birth`, `martial_status`, `religion`, `nationality`, `nationality_type`, `id_race`, `mail_address1`, `mail_address2`, `mailing_country`, `mailing_state`, `mailing_city`, `mailing_zipcode`, `permanent_address1`, `permanent_address2`, `permanent_country`, `permanent_state`, `permanent_city`, `permanent_zipcode`, `nric`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`, `id_student`) VALUES
(1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-07-11 01:20:44', NULL, '2020-07-11 01:20:44', 1),
(2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-07-11 06:25:02', NULL, '2020-07-11 06:25:02', 1),
(3, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-07-11 11:53:53', NULL, '2020-07-11 11:53:53', 2),
(4, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-07-11 12:55:29', NULL, '2020-07-11 12:55:29', 3),
(5, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-07-11 13:15:32', NULL, '2020-07-11 13:15:32', 4),
(6, 'Miss. Anjuman Syed', 'Miss', 'Anjuman', 'Syed', '123', '123', '55678', 'as@cms.com', '08/19/2020', 'Female', '2020-07-24', 'Single', 'Hinduism', 'Malaysian', '', 0, 'KL', 'KL', 1, 1, 'KL', 'KL', '123, KL', 'KL', 1, 1, 'KL', '234567', '', NULL, NULL, '2020-08-04 14:28:09', NULL, '2020-08-04 14:28:09', 5),
(7, 'Mr. Nabasha Ali', 'Mr', 'Nabasha', 'Ali', 'Passport', '323213123', '78987856', 'nab@cms.com', '28-08-2020', 'Female', '1990-08-31', 'Married', 'Christianity', 'Malaysian', '', 0, 'Mohhamed Ali Street, ', 'KL', 1, 1, 'KL', '247827', 'Mohhamed Ali Street, ', 'KL', 2, 1, 'KL', '2123123', '', NULL, NULL, '2020-08-10 12:50:24', NULL, '2020-08-10 12:50:24', 6),
(8, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-08-10 14:27:11', NULL, '2020-08-10 14:27:11', 7),
(9, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-08-17 12:29:25', NULL, '2020-08-17 12:29:25', 8),
(10, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-08-19 08:58:41', NULL, '2020-08-19 08:58:41', 9),
(11, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-08-19 09:40:24', NULL, '2020-08-19 09:40:24', 10),
(12, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', NULL, NULL, '2020-08-20 05:22:04', NULL, '2020-08-20 05:22:04', 11);

-- --------------------------------------------------------

--
-- Table structure for table `programme`
--

CREATE TABLE `programme` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(250) DEFAULT '',
  `id_award` int(10) DEFAULT 0,
  `id_scheme` int(20) DEFAULT 0,
  `id_partner_category` int(20) DEFAULT 0,
  `id_partner_university` int(20) DEFAULT 0,
  `internal_external` varchar(50) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `total_cr_hrs` varchar(20) DEFAULT '',
  `graduate_studies` varchar(100) DEFAULT '',
  `foundation` varchar(50) DEFAULT '',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `certificate` varchar(200) DEFAULT '',
  `is_apel` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `programme`
--

INSERT INTO `programme` (`id`, `name`, `name_optional_language`, `id_award`, `id_scheme`, `id_partner_category`, `id_partner_university`, `internal_external`, `code`, `total_cr_hrs`, `graduate_studies`, `foundation`, `start_date`, `end_date`, `certificate`, `is_apel`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(3, 'Bachelor of Business Administration', 'Bachelor of Business Administration', 3, 4, 0, 0, 'Internal', 'BBA', '100', '', '', NULL, NULL, '', 1, 1, 1, '2020-08-20 06:18:19', 1, '2020-08-20 06:18:19');

-- --------------------------------------------------------

--
-- Table structure for table `programme_has_course`
--

CREATE TABLE `programme_has_course` (
  `id` int(20) NOT NULL,
  `id_programme` varchar(100) DEFAULT '',
  `id_programme_landscape` int(20) DEFAULT NULL,
  `id_course` int(10) DEFAULT NULL,
  `id_semester` int(20) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `programme_has_dean`
--

CREATE TABLE `programme_has_dean` (
  `id` int(20) NOT NULL,
  `id_programme` int(10) DEFAULT NULL,
  `id_staff` int(10) DEFAULT NULL,
  `effective_start_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `programme_has_dean`
--

INSERT INTO `programme_has_dean` (`id`, `id_programme`, `id_staff`, `effective_start_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 1, 1, '2020-07-10 00:00:00', NULL, NULL, '2020-07-10 23:51:06', NULL, '2020-07-10 23:51:06'),
(2, 1, 0, '1970-01-01 00:00:00', NULL, NULL, '2020-07-10 23:51:10', NULL, '2020-07-10 23:51:10'),
(3, 1, 0, '1970-01-01 00:00:00', NULL, NULL, '2020-07-11 00:08:33', NULL, '2020-07-11 00:08:33'),
(4, 1, 0, '1970-01-01 00:00:00', NULL, NULL, '2020-07-11 06:23:11', NULL, '2020-07-11 06:23:11'),
(5, 1, 0, '1970-01-01 00:00:00', NULL, NULL, '2020-07-12 18:29:50', NULL, '2020-07-12 18:29:50'),
(6, 1, 0, '1970-01-01 00:00:00', NULL, NULL, '2020-07-13 01:40:46', NULL, '2020-07-13 01:40:46'),
(7, 2, 1, '2021-08-31 00:00:00', NULL, NULL, '2020-08-17 12:31:04', NULL, '2020-08-17 12:31:04'),
(8, 2, 2, '2020-08-01 00:00:00', NULL, NULL, '2020-08-17 12:31:04', NULL, '2020-08-17 12:31:04'),
(9, 3, 2, '2020-08-01 00:00:00', NULL, NULL, '2020-08-20 06:18:19', NULL, '2020-08-20 06:18:19');

-- --------------------------------------------------------

--
-- Table structure for table `programme_has_fee_structure`
--

CREATE TABLE `programme_has_fee_structure` (
  `id` int(20) NOT NULL,
  `id_programme` int(20) DEFAULT NULL,
  `scheme` varchar(50) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `description` varchar(520) DEFAULT '',
  `id_intake` int(20) DEFAULT NULL,
  `id_semester` int(20) DEFAULT NULL,
  `effective_date` datetime DEFAULT NULL,
  `student_category` varchar(20) DEFAULT '',
  `currency` varchar(20) DEFAULT '',
  `estimated_total_fee` varchar(20) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `programme_has_scheme`
--

CREATE TABLE `programme_has_scheme` (
  `id` bigint(20) NOT NULL,
  `id_program` int(20) DEFAULT 0,
  `id_program_type` int(20) DEFAULT 0,
  `mode_of_program` varchar(100) DEFAULT '',
  `mode_of_study` varchar(100) DEFAULT '',
  `min_duration` int(20) DEFAULT 0,
  `max_duration` int(20) DEFAULT 0,
  `status` int(5) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `programme_has_scheme`
--

INSERT INTO `programme_has_scheme` (`id`, `id_program`, `id_program_type`, `mode_of_program`, `mode_of_study`, `min_duration`, `max_duration`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(8, 3, 2, 'Blended', 'Part Time', 24, 28, 0, NULL, '2020-08-20 06:18:51', NULL, '2020-08-20 06:18:51'),
(11, 3, 2, 'Face to Face', 'Full Time', 24, 28, 0, NULL, '2020-08-20 06:20:30', NULL, '2020-08-20 06:20:30'),
(12, 3, 2, 'Online', 'Part Time', 24, 28, 0, NULL, '2020-08-20 06:20:59', NULL, '2020-08-20 06:20:59'),
(13, 3, 1, 'Online', 'Full Time', 12, 133, 0, NULL, '2020-08-20 09:33:47', NULL, '2020-08-20 09:33:47');

-- --------------------------------------------------------

--
-- Table structure for table `programme_landscape`
--

CREATE TABLE `programme_landscape` (
  `id` int(20) NOT NULL,
  `name` varchar(220) DEFAULT '',
  `id_programme` int(10) DEFAULT NULL,
  `id_intake` int(10) DEFAULT NULL,
  `min_total_cr_hrs` varchar(20) DEFAULT '',
  `min_repeat_course` varchar(20) DEFAULT '',
  `max_repeat_exams` varchar(20) DEFAULT '',
  `total_semester` varchar(20) DEFAULT '',
  `program_scheme` varchar(50) DEFAULT '',
  `total_block` varchar(20) DEFAULT '',
  `total_level` varchar(20) DEFAULT '',
  `min_total_score` varchar(20) DEFAULT '',
  `min_pass_subject` varchar(20) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `programme_landscape`
--

INSERT INTO `programme_landscape` (`id`, `name`, `id_programme`, `id_intake`, `min_total_cr_hrs`, `min_repeat_course`, `max_repeat_exams`, `total_semester`, `program_scheme`, `total_block`, `total_level`, `min_total_score`, `min_pass_subject`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(5, 'Program Landscape One', 3, 1, '100', '100', '2', '14', '8', '14', '14', '40', '14', 1, NULL, '2020-08-20 06:23:39', NULL, '2020-08-20 06:23:39'),
(6, 'Program Landscape Two', 3, 1, '10', '10', '10', '14', '12', '10', '10', '40', '10', 1, NULL, '2020-08-20 06:25:04', NULL, '2020-08-20 06:25:04'),
(7, 'Program Landscape Three', 3, 1, '10', '10', '10', '9', '11', '12', '12', '40', '10', 1, NULL, '2020-08-20 06:26:11', NULL, '2020-08-20 06:26:11');

-- --------------------------------------------------------

--
-- Table structure for table `program_entry_requirement`
--

CREATE TABLE `program_entry_requirement` (
  `id` int(20) NOT NULL,
  `id_program` int(20) DEFAULT 0,
  `description` varchar(2048) DEFAULT '',
  `from_dt` datetime DEFAULT NULL,
  `to_dt` datetime DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program_entry_requirement`
--

INSERT INTO `program_entry_requirement` (`id`, `id_program`, `description`, `from_dt`, `to_dt`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Description', '2020-07-07 00:00:00', '2020-07-23 00:00:00', 1, 1, '2020-07-19 19:58:34', NULL, '2020-07-19 19:58:34'),
(2, 1, 'Description', '2020-07-20 00:00:00', '2020-07-16 00:00:00', 1, 1, '2020-07-19 20:00:50', NULL, '2020-07-19 20:00:50');

-- --------------------------------------------------------

--
-- Table structure for table `program_grade`
--

CREATE TABLE `program_grade` (
  `id` int(20) NOT NULL,
  `id_intake` int(10) DEFAULT NULL,
  `id_program` int(10) DEFAULT NULL,
  `id_semester` int(10) DEFAULT NULL,
  `id_grade` int(10) DEFAULT NULL,
  `description` varchar(250) DEFAULT '',
  `minmarks` varchar(20) DEFAULT '',
  `maxmarks` varchar(20) DEFAULT '',
  `result` varchar(50) DEFAULT '',
  `rank` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program_grade`
--

INSERT INTO `program_grade` (`id`, `id_intake`, `id_program`, `id_semester`, `id_grade`, `description`, `minmarks`, `maxmarks`, `result`, `rank`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 0, 1, 0, 2, 'Description', '90', '100', 'pass', '10', NULL, NULL, '2020-07-11 11:25:31', NULL, '2020-07-11 11:25:31');

-- --------------------------------------------------------

--
-- Table structure for table `program_has_acceredation_details`
--

CREATE TABLE `program_has_acceredation_details` (
  `id` int(20) NOT NULL,
  `id_program` int(20) DEFAULT 0,
  `category` varchar(1024) DEFAULT '',
  `type` varchar(50) DEFAULT '',
  `acceredation_dt` datetime DEFAULT NULL,
  `acceredation_number` varchar(50) DEFAULT '',
  `valid_from` datetime DEFAULT NULL,
  `valid_to` datetime DEFAULT NULL,
  `approval_dt` datetime DEFAULT NULL,
  `acceredation_reference` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program_has_acceredation_details`
--

INSERT INTO `program_has_acceredation_details` (`id`, `id_program`, `category`, `type`, `acceredation_dt`, `acceredation_number`, `valid_from`, `valid_to`, `approval_dt`, `acceredation_reference`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Board', 'Local', '2020-07-13 00:00:00', 'asdf', '2020-07-13 00:00:00', '2020-07-13 00:00:00', '2020-07-13 00:00:00', 'asdfsd', NULL, NULL, '2020-07-13 06:25:12', NULL, '2020-07-13 06:25:12');

-- --------------------------------------------------------

--
-- Table structure for table `program_has_concurrent_program`
--

CREATE TABLE `program_has_concurrent_program` (
  `id` int(20) NOT NULL,
  `id_program` int(20) DEFAULT 0,
  `id_concurrent_program` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `program_has_major_details`
--

CREATE TABLE `program_has_major_details` (
  `id` int(20) NOT NULL,
  `id_program` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_in_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program_has_major_details`
--

INSERT INTO `program_has_major_details` (`id`, `id_program`, `name`, `code`, `name_in_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Maths One', 'Maths 1', '', NULL, NULL, '2020-07-13 09:08:24', NULL, '2020-07-13 09:08:24'),
(2, 3, 'BBA', 'BBA', 'BBA', NULL, NULL, '2020-08-20 11:17:52', NULL, '2020-08-20 11:17:52');

-- --------------------------------------------------------

--
-- Table structure for table `program_has_minor_details`
--

CREATE TABLE `program_has_minor_details` (
  `id` int(20) NOT NULL,
  `id_program` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_in_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program_has_minor_details`
--

INSERT INTO `program_has_minor_details` (`id`, `id_program`, `name`, `code`, `name_in_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Environmental Science', 'ES', '', NULL, NULL, '2020-07-14 06:29:19', NULL, '2020-07-14 06:29:19');

-- --------------------------------------------------------

--
-- Table structure for table `program_landscape_requirement_details`
--

CREATE TABLE `program_landscape_requirement_details` (
  `id` int(20) NOT NULL,
  `id_program_landscape` int(20) DEFAULT NULL,
  `id_landscape_course_type` int(20) DEFAULT NULL,
  `requirement_type` varchar(50) DEFAULT '',
  `minimum_credit` int(20) DEFAULT NULL,
  `compulsary` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program_landscape_requirement_details`
--

INSERT INTO `program_landscape_requirement_details` (`id`, `id_program_landscape`, `id_landscape_course_type`, `requirement_type`, `minimum_credit`, `compulsary`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 'Credit Hours', 10, 1, NULL, NULL, '2020-07-19 09:31:08', NULL, '2020-07-19 09:31:08'),
(2, 1, 2, 'Papers', 5, 1, NULL, NULL, '2020-07-19 09:31:22', NULL, '2020-07-19 09:31:22');

-- --------------------------------------------------------

--
-- Table structure for table `program_landscape_requisite_details`
--

CREATE TABLE `program_landscape_requisite_details` (
  `id` int(20) NOT NULL,
  `id_program_landscape` int(20) DEFAULT 0,
  `id_compulsary_course` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `id_master_course` int(20) DEFAULT 0,
  `requisite_type` varchar(50) DEFAULT '',
  `min_pass_grade` int(20) DEFAULT 0,
  `min_total_credit` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program_landscape_requisite_details`
--

INSERT INTO `program_landscape_requisite_details` (`id`, `id_program_landscape`, `id_compulsary_course`, `id_course`, `id_intake`, `id_program`, `id_master_course`, `requisite_type`, `min_pass_grade`, `min_total_credit`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 1, 5, 2, 1, 1, 5, 'Co-requisite', 0, 0, 1, NULL, '2020-07-15 04:38:37', NULL, '2020-07-15 04:38:37'),
(3, 1, 5, 4, 1, 1, 5, 'Co-requisite', 0, 0, 1, NULL, '2020-07-15 04:38:47', NULL, '2020-07-15 04:38:47'),
(4, 1, 6, 0, 1, 1, 6, 'Pass with Grade', 5, 0, 1, NULL, '2020-07-19 00:31:44', NULL, '2020-07-19 00:31:44'),
(5, 1, 6, 2, 1, 1, 6, 'Co-requisite', 0, 0, 1, NULL, '2020-07-19 00:32:06', NULL, '2020-07-19 00:32:06');

-- --------------------------------------------------------

--
-- Table structure for table `program_landscape_semester_details`
--

CREATE TABLE `program_landscape_semester_details` (
  `id` int(20) NOT NULL,
  `id_program_landscape` int(20) DEFAULT NULL,
  `semester_type` varchar(50) DEFAULT '',
  `registration_rule` varchar(50) DEFAULT '',
  `minimum` int(20) DEFAULT NULL,
  `maximum` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program_landscape_semester_details`
--

INSERT INTO `program_landscape_semester_details` (`id`, `id_program_landscape`, `semester_type`, `registration_rule`, `minimum`, `maximum`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Short Semester', 'No Of Courses', 1, 5, NULL, NULL, '2020-07-19 09:30:09', NULL, '2020-07-19 09:30:09'),
(2, 2, 'Short Semester', 'No Of Courses', 40, 100, NULL, NULL, '2020-08-17 12:34:11', NULL, '2020-08-17 12:34:11'),
(3, 3, 'Short Semester', 'No Of Credit Hours', 40, 100, NULL, NULL, '2020-08-17 12:35:01', NULL, '2020-08-17 12:35:01'),
(4, 4, 'Short Semester', 'No Of Courses', 10, 100, NULL, NULL, '2020-08-19 21:57:59', NULL, '2020-08-19 21:57:59'),
(5, 5, 'Short Semester', 'No Of Courses', 10, 14, NULL, NULL, '2020-08-20 06:23:37', NULL, '2020-08-20 06:23:37'),
(6, 6, 'Short Semester', 'No Of Courses', 10, 14, NULL, NULL, '2020-08-20 06:25:02', NULL, '2020-08-20 06:25:02'),
(7, 7, 'Short Semester', 'No Of Courses', 4, 10, NULL, NULL, '2020-08-20 06:26:09', NULL, '2020-08-20 06:26:09');

-- --------------------------------------------------------

--
-- Table structure for table `program_type`
--

CREATE TABLE `program_type` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program_type`
--

INSERT INTO `program_type` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Full Course', 'Structure A', 1, NULL, '2020-07-13 04:47:07', NULL, '2020-07-13 04:47:07'),
(2, 'Course Work', 'Structure B', 1, NULL, '2020-07-13 09:09:49', NULL, '2020-07-13 09:09:49'),
(3, 'Mixed Mode', 'Structure C', 1, NULL, '2020-07-13 09:10:09', NULL, '2020-07-13 09:10:09');

-- --------------------------------------------------------

--
-- Table structure for table `project_report_submission`
--

CREATE TABLE `project_report_submission` (
  `id` int(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `description` varchar(8088) DEFAULT '',
  `id_program` int(20) DEFAULT 0,
  `id_qualification` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `from_dt` datetime DEFAULT current_timestamp(),
  `to_dt` datetime DEFAULT current_timestamp(),
  `duration` varchar(50) DEFAULT '',
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_report_submission`
--

INSERT INTO `project_report_submission` (`id`, `name`, `description`, `id_program`, `id_qualification`, `id_intake`, `id_student`, `from_dt`, `to_dt`, `duration`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'CMS', 'Campus Management System', 1, 1, 1, 4, '2020-07-12 00:00:00', '2020-07-12 00:00:00', '6', '', 1, NULL, '2020-07-11 13:18:40', NULL, '2020-07-11 13:18:40');

-- --------------------------------------------------------

--
-- Table structure for table `pr_entry`
--

CREATE TABLE `pr_entry` (
  `id` int(20) NOT NULL,
  `pr_number` varchar(50) DEFAULT NULL,
  `type_of_pr` varchar(50) DEFAULT '',
  `type` varchar(100) DEFAULT '',
  `pr_entry_date` datetime DEFAULT current_timestamp(),
  `id_financial_year` int(20) DEFAULT NULL,
  `id_department` int(20) DEFAULT NULL,
  `id_vendor` int(20) DEFAULT NULL,
  `description` varchar(580) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `is_po` int(10) DEFAULT 0,
  `reason` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pr_entry_details`
--

CREATE TABLE `pr_entry_details` (
  `id` int(20) NOT NULL,
  `id_pr_entry` int(200) DEFAULT NULL,
  `cr_account` int(200) DEFAULT NULL,
  `cr_activity` int(200) DEFAULT NULL,
  `cr_department` int(200) DEFAULT NULL,
  `cr_fund` int(200) DEFAULT NULL,
  `dt_account` int(200) DEFAULT NULL,
  `dt_activity` int(200) DEFAULT NULL,
  `dt_department` int(200) DEFAULT NULL,
  `dt_fund` int(200) DEFAULT NULL,
  `id_category` int(200) DEFAULT NULL,
  `id_sub_category` int(200) DEFAULT NULL,
  `id_item` int(200) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` float(20,2) DEFAULT NULL,
  `id_tax` float(10,2) DEFAULT NULL,
  `tax_price` float(10,2) DEFAULT NULL,
  `total_final` float(10,2) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `publish_assesment_result_date`
--

CREATE TABLE `publish_assesment_result_date` (
  `id` int(20) NOT NULL,
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_semester` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `date_time` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `publish_assesment_result_date`
--

INSERT INTO `publish_assesment_result_date` (`id`, `id_program`, `id_intake`, `id_semester`, `id_course`, `date_time`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, 3, '2020-08-04 00:00:00', 1, 1, '2020-08-16 23:30:23', NULL, '2020-08-16 23:30:23');

-- --------------------------------------------------------

--
-- Table structure for table `publish_exam`
--

CREATE TABLE `publish_exam` (
  `id` int(20) NOT NULL,
  `id_intake` varchar(20) DEFAULT NULL,
  `id_semester` varchar(20) DEFAULT NULL,
  `id_program` varchar(20) DEFAULT NULL,
  `exam_date` varchar(20) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `publish_exam`
--

INSERT INTO `publish_exam` (`id`, `id_intake`, `id_semester`, `id_program`, `exam_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, '1', '1', '1', '2020-07-22', 1, NULL, '2020-07-21 10:30:44', NULL, '2020-07-21 10:30:44'),
(2, '1', '1', '1', '2020-08-19', 1, NULL, '2020-08-16 23:29:33', NULL, '2020-08-16 23:29:33');

-- --------------------------------------------------------

--
-- Table structure for table `publish_exam_result_date`
--

CREATE TABLE `publish_exam_result_date` (
  `id` int(20) NOT NULL,
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_semester` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `date_time` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `publish_exam_result_date`
--

INSERT INTO `publish_exam_result_date` (`id`, `id_program`, `id_intake`, `id_semester`, `id_course`, `date_time`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, 3, '2020-08-17 00:00:00', 1, 1, '2020-08-16 23:29:58', NULL, '2020-08-16 23:29:58');

-- --------------------------------------------------------

--
-- Table structure for table `qualification_setup`
--

CREATE TABLE `qualification_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qualification_setup`
--

INSERT INTO `qualification_setup` (`id`, `name`, `name_in_malay`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Degree', 'Degree', 'Degree', 1, NULL, '2020-07-11 00:59:26', NULL, '2020-07-11 00:59:26');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` int(20) NOT NULL,
  `id_pool` int(20) DEFAULT 0,
  `question` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question_pool`
--

CREATE TABLE `question_pool` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `race_setup`
--

CREATE TABLE `race_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `race_setup`
--

INSERT INTO `race_setup` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Malays', '', 1, NULL, '2020-07-10 23:24:07', NULL, '2020-07-10 23:24:07'),
(2, 'Chinese', '', 1, NULL, '2020-07-10 23:24:14', NULL, '2020-07-10 23:24:14');

-- --------------------------------------------------------

--
-- Table structure for table `receipt`
--

CREATE TABLE `receipt` (
  `id` int(20) NOT NULL,
  `type` varchar(50) DEFAULT '',
  `receipt_number` varchar(50) DEFAULT '',
  `receipt_amount` float(20,2) DEFAULT NULL,
  `remarks` varchar(520) DEFAULT '',
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT NULL,
  `id_sponser` int(20) DEFAULT 0,
  `receipt_date` date DEFAULT NULL,
  `approval_status` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receipt`
--

INSERT INTO `receipt` (`id`, `type`, `receipt_number`, `receipt_amount`, `remarks`, `id_program`, `id_intake`, `id_student`, `id_sponser`, `receipt_date`, `approval_status`, `status`, `reason`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(16, 'Applicant', 'REC000002/2020', 20000.00, 'Rema', 3, 1, 28, 0, '2020-08-20', '', 0, '', NULL, '2020-08-20 07:33:07', NULL, '2020-08-20 07:33:07');

-- --------------------------------------------------------

--
-- Table structure for table `receipt_details`
--

CREATE TABLE `receipt_details` (
  `id` int(20) NOT NULL,
  `id_receipt` int(10) DEFAULT NULL,
  `id_main_invoice` int(10) DEFAULT NULL,
  `invoice_amount` float DEFAULT NULL,
  `paid_amount` float DEFAULT NULL,
  `approval_status` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receipt_details`
--

INSERT INTO `receipt_details` (`id`, `id_receipt`, `id_main_invoice`, `invoice_amount`, `paid_amount`, `approval_status`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(9, 16, 29, 0, 20000, NULL, 1, 1, '2020-08-20 07:33:07', NULL, '2020-08-20 07:33:07');

-- --------------------------------------------------------

--
-- Table structure for table `receipt_paid_details`
--

CREATE TABLE `receipt_paid_details` (
  `id` int(20) NOT NULL,
  `id_receipt` int(10) DEFAULT NULL,
  `id_payment_type` varchar(20) DEFAULT '',
  `payment_reference_number` varchar(50) DEFAULT '',
  `paid_amount` float(20,2) DEFAULT NULL,
  `approval_status` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receipt_paid_details`
--

INSERT INTO `receipt_paid_details` (`id`, `id_receipt`, `id_payment_type`, `payment_reference_number`, `paid_amount`, `approval_status`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(16, 15, 'Cash', 'CASH', 20000.00, NULL, 1, 1, '2020-08-20 07:32:22', NULL, '2020-08-20 07:32:22'),
(17, 16, 'Cash', 'Refere', 20000.00, NULL, 1, 1, '2020-08-20 07:33:07', NULL, '2020-08-20 07:33:07');

-- --------------------------------------------------------

--
-- Table structure for table `release`
--

CREATE TABLE `release` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT NULL,
  `id_barring_type` int(20) DEFAULT NULL,
  `id_semester` int(20) DEFAULT NULL,
  `reason` varchar(580) DEFAULT '',
  `release_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `release`
--

INSERT INTO `release` (`id`, `id_student`, `id_barring_type`, `id_semester`, `reason`, `release_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 6, 1, 1, 'Rebacked From Barring', '2020-08-10 00:00:00', 1, 1, '2020-08-10 13:53:03', NULL, '2020-08-10 13:53:03'),
(2, 7, 1, 1, 'Released By Giving Excuse', '2020-08-18 00:00:00', 1, 1, '2020-08-10 14:37:50', NULL, '2020-08-10 14:37:50');

-- --------------------------------------------------------

--
-- Table structure for table `religion_setup`
--

CREATE TABLE `religion_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `religion_setup`
--

INSERT INTO `religion_setup` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Islam', '', 1, NULL, '2020-07-10 23:24:44', NULL, '2020-07-10 23:24:44'),
(2, 'Buddhism', '', 1, NULL, '2020-07-10 23:24:56', NULL, '2020-07-10 23:24:56');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(20) NOT NULL,
  `role` varchar(250) DEFAULT '',
  `status` int(20) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `room_allotment`
--

CREATE TABLE `room_allotment` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT 0,
  `id_hostel` int(20) DEFAULT 0,
  `id_room` int(20) DEFAULT 0,
  `id_room_type` int(20) DEFAULT 0,
  `from_dt` date DEFAULT NULL,
  `to_dt` date DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_allotment`
--

INSERT INTO `room_allotment` (`id`, `id_student`, `id_hostel`, `id_room`, `id_room_type`, `from_dt`, `to_dt`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 4, 1, 5, 1, '2020-07-10', '2020-07-15', 1, 1, '2020-07-11 13:23:43', NULL, '2020-07-11 13:23:43');

-- --------------------------------------------------------

--
-- Table structure for table `salutation_setup`
--

CREATE TABLE `salutation_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `sequence` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salutation_setup`
--

INSERT INTO `salutation_setup` (`id`, `name`, `sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'AG', 1, 1, NULL, '2020-08-10 03:08:19', NULL, '2020-08-10 03:08:19'),
(2, 'DATIN', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(3, 'DATINDR', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(4, 'DATINPROFDR', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(5, 'DATINSERI', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(6, 'DATINSETIA', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(7, 'DATINSRI', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(8, 'DATO', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(9, 'DATODR', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(10, 'DATOHAJI', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(11, 'DATOSRI', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(12, 'DATUK', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(13, 'DATUKHAJI', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(14, 'DATUKSERI', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(15, 'DATUKSERIDR', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(16, 'DR', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(17, 'DRHAJI', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(18, 'EN', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(19, 'HAJI', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(20, 'HE', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(21, 'JENBTANSRIDATO', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(22, 'MDM', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(23, 'MR', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(24, 'MRS', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21'),
(25, 'MS', 1, 1, NULL, '2020-08-10 03:11:21', NULL, '2020-08-10 03:11:21');

-- --------------------------------------------------------

--
-- Table structure for table `scheme`
--

CREATE TABLE `scheme` (
  `id` int(20) NOT NULL,
  `description` varchar(580) DEFAULT '',
  `description_in_optional_language` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme`
--

INSERT INTO `scheme` (`id`, `description`, `description_in_optional_language`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Open Market', 'Open Market', 'OM', 1, 1, '2020-07-13 08:36:42', 1, '2020-07-13 08:36:42'),
(2, 'Corporate ', 'Corporate', 'COR', 1, 1, '2020-08-09 20:11:38', NULL, '2020-08-09 20:11:38'),
(3, 'MOE', 'MOE', 'MOE', 1, 1, '2020-08-09 20:12:00', NULL, '2020-08-09 20:12:00'),
(4, 'international', 'international', 'INT', 1, 1, '2020-08-10 20:26:02', NULL, '2020-08-10 20:26:02');

-- --------------------------------------------------------

--
-- Table structure for table `scheme_has_program`
--

CREATE TABLE `scheme_has_program` (
  `id` int(20) NOT NULL,
  `id_scholarship_scheme` bigint(20) DEFAULT 0,
  `id_program` bigint(20) DEFAULT 0,
  `id_sub_thrust` int(20) DEFAULT 0,
  `quota` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme_has_program`
--

INSERT INTO `scheme_has_program` (`id`, `id_scholarship_scheme`, `id_program`, `id_sub_thrust`, `quota`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 2, 1, 80, 0, NULL, '2020-07-29 12:41:24', NULL, '2020-07-29 12:41:24'),
(3, 1, 1, 1, 70, 0, NULL, '2020-07-29 12:42:32', NULL, '2020-07-29 12:42:32');

-- --------------------------------------------------------

--
-- Table structure for table `scholar`
--

CREATE TABLE `scholar` (
  `id` int(11) NOT NULL,
  `email` varchar(1024) NOT NULL COMMENT 'login email',
  `password` varchar(1024) NOT NULL COMMENT 'hashed login password',
  `salutation` varchar(50) DEFAULT '',
  `name` varchar(1024) DEFAULT NULL COMMENT 'full name of user',
  `first_name` varchar(1024) DEFAULT '',
  `last_name` varchar(1024) DEFAULT '',
  `mobile` varchar(20) DEFAULT NULL,
  `id_role` tinyint(4) NOT NULL,
  `status` int(20) DEFAULT 0,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scholar`
--

INSERT INTO `scholar` (`id`, `email`, `password`, `salutation`, `name`, `first_name`, `last_name`, `mobile`, `id_role`, `status`, `is_deleted`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'vk@cms.com', '7fef6171469e80d32c0559f88b377245', '1', 'Mr. Vinay Kiran', 'Vinay', 'Kiran', '8888888888', 1, 1, 0, 1, '2020-07-29 00:00:00', 1, '2020-07-23 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_accreditation_category`
--

CREATE TABLE `scholarship_accreditation_category` (
  `id` int(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_accreditation_category`
--

INSERT INTO `scholarship_accreditation_category` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Accreditation', 'Acc', '', 1, NULL, '2020-07-27 12:16:07', NULL, '2020-07-27 12:16:07');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_accreditation_type`
--

CREATE TABLE `scholarship_accreditation_type` (
  `id` int(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_accreditation_type`
--

INSERT INTO `scholarship_accreditation_type` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Local', 'LOC', '', 1, NULL, '2020-07-27 12:16:28', NULL, '2020-07-27 12:16:28'),
(2, 'International', 'INT', '', 1, NULL, '2020-07-27 12:16:47', NULL, '2020-07-27 12:16:47');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_amount_calculation_type`
--

CREATE TABLE `scholarship_amount_calculation_type` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_applicant`
--

CREATE TABLE `scholarship_applicant` (
  `id` int(20) NOT NULL,
  `id_scholarship_scheme` int(11) DEFAULT 0,
  `id_scholar_applicant` int(11) DEFAULT 0,
  `id_application` int(20) DEFAULT 0,
  `year` int(11) DEFAULT 0,
  `id_intake` int(11) DEFAULT NULL,
  `id_program` int(11) DEFAULT NULL,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `passport` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `is_hostel` int(20) DEFAULT 0,
  `id_degree_type` int(20) DEFAULT 0,
  `id_race` varchar(120) DEFAULT '',
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `mailing_zipcode` varchar(120) DEFAULT '',
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `sibbling_discount` varchar(50) DEFAULT '',
  `employee_discount` varchar(50) DEFAULT '',
  `applicant_status` varchar(50) DEFAULT 'Draft',
  `is_sibbling_discount` int(20) DEFAULT 3,
  `is_employee_discount` int(20) DEFAULT 3,
  `id_type` varchar(888) DEFAULT NULL,
  `passport_number` varchar(1024) DEFAULT NULL,
  `email_verified` int(2) DEFAULT 0,
  `is_submitted` int(2) DEFAULT 0,
  `is_updated` int(2) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `approved_by` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_application`
--

CREATE TABLE `scholarship_application` (
  `id` int(20) NOT NULL,
  `id_student` bigint(20) DEFAULT 0,
  `id_program` bigint(20) DEFAULT 0,
  `id_intake` bigint(20) DEFAULT 0,
  `id_qualification` bigint(20) DEFAULT 0,
  `application_number` varchar(80) DEFAULT '',
  `id_scholarship_scheme` int(20) DEFAULT 0,
  `id_scholar_applicant` int(20) DEFAULT 0,
  `father_name` varchar(1024) DEFAULT '',
  `mother_name` varchar(1024) DEFAULT '',
  `father_deceased` bigint(20) DEFAULT 0,
  `father_occupation` varchar(1024) DEFAULT '',
  `no_siblings` bigint(20) DEFAULT 0,
  `year` bigint(20) DEFAULT 0,
  `result_item` varchar(500) DEFAULT '',
  `est_fee` float(20,2) DEFAULT 0.00,
  `max_marks` bigint(20) DEFAULT 0,
  `obtained_marks` bigint(20) DEFAULT 0,
  `percentage` float(20,2) DEFAULT 0.00,
  `reason` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_assesment_method_setup`
--

CREATE TABLE `scholarship_assesment_method_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `type` float(20,2) DEFAULT 0.00,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_award_setup`
--

CREATE TABLE `scholarship_award_setup` (
  `id` int(20) NOT NULL,
  `code` varchar(200) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `level` int(20) DEFAULT NULL,
  `description` varchar(500) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_award_setup`
--

INSERT INTO `scholarship_award_setup` (`id`, `code`, `name`, `level`, `description`, `id_programme`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'COA', 'Certificate of Attendance', 1, 'Certificate of Attendance', NULL, 1, NULL, '2020-07-24 05:18:21', NULL, '2020-07-24 05:18:21'),
(2, 'CER', 'Certificate', 2, 'Certificate of Passing', NULL, 1, NULL, '2020-07-24 05:18:44', NULL, '2020-07-24 05:18:44'),
(3, 'DIP', 'Diploma', 3, 'Diploma', NULL, 1, NULL, '2020-07-24 05:19:08', NULL, '2020-07-24 05:19:08'),
(4, 'DEG', 'Degree', 4, 'Degree', NULL, 1, NULL, '2020-07-24 05:19:27', NULL, '2020-07-24 05:19:27'),
(5, 'PRO', 'Chartered', 5, 'Chartered', NULL, 1, NULL, '2020-07-24 05:20:46', NULL, '2020-07-24 05:20:46');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_bank_registration`
--

CREATE TABLE `scholarship_bank_registration` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `bank_id` varchar(50) DEFAULT '',
  `address` varchar(580) DEFAULT '',
  `landmark` varchar(580) DEFAULT '',
  `city` varchar(580) DEFAULT '',
  `id_state` int(20) DEFAULT NULL,
  `id_country` int(20) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `cr_fund` varchar(50) DEFAULT '',
  `cr_department` varchar(50) DEFAULT '',
  `cr_activity` varchar(50) DEFAULT '',
  `cr_account` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_country`
--

CREATE TABLE `scholarship_country` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(2) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(2) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_country`
--

INSERT INTO `scholarship_country` (`id`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Malaysia', 1, 0, '2020-07-22 01:33:31', 0, '2020-07-22 01:33:31');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_department`
--

CREATE TABLE `scholarship_department` (
  `id` int(20) NOT NULL,
  `name` varchar(200) DEFAULT '',
  `code` varchar(120) DEFAULT '',
  `description` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_department`
--

INSERT INTO `scholarship_department` (`id`, `name`, `code`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Batchelor Of Computer Science', 'B.CS', 'Batchelor Of Computer Science', 1, NULL, '2020-07-22 01:41:36', NULL, '2020-07-22 01:41:36');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_department_has_staff`
--

CREATE TABLE `scholarship_department_has_staff` (
  `id` int(20) NOT NULL,
  `id_department` int(20) DEFAULT 0,
  `id_staff` int(20) DEFAULT 0,
  `effective_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_department_has_staff`
--

INSERT INTO `scholarship_department_has_staff` (`id`, `id_department`, `id_staff`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, '2019-09-09 00:00:00', NULL, NULL, '2020-07-22 03:09:34', NULL, '2020-07-22 03:09:34');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_documents`
--

CREATE TABLE `scholarship_documents` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `description` varchar(500) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_documents_for_applicant`
--

CREATE TABLE `scholarship_documents_for_applicant` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `name_in_malay` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `id_scholarship_program` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_documents_program`
--

CREATE TABLE `scholarship_documents_program` (
  `id` int(20) NOT NULL,
  `id_document` int(20) DEFAULT 0,
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_documents_program_details`
--

CREATE TABLE `scholarship_documents_program_details` (
  `id` int(20) NOT NULL,
  `id_documents_program` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_education_level`
--

CREATE TABLE `scholarship_education_level` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `short_name` varchar(1024) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_education_level`
--

INSERT INTO `scholarship_education_level` (`id`, `name`, `short_name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Batchelors', 'B', '', 1, NULL, '2020-07-24 23:33:52', NULL, '2020-07-24 23:33:52');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_examination_details`
--

CREATE TABLE `scholarship_examination_details` (
  `id` int(20) NOT NULL,
  `id_scholar_applicant` int(20) DEFAULT NULL,
  `id_scholarship_scheme` int(20) DEFAULT 0,
  `id_application` int(20) DEFAULT 0,
  `qualification_level` varchar(120) DEFAULT '',
  `degree_awarded` varchar(120) DEFAULT '',
  `specialization` varchar(120) DEFAULT '',
  `class_degree` varchar(120) DEFAULT '',
  `result` varchar(120) DEFAULT '',
  `year` varchar(20) DEFAULT '',
  `medium` varchar(120) DEFAULT '',
  `college_country` varchar(120) DEFAULT '',
  `college_name` varchar(120) DEFAULT '',
  `certificate` varchar(520) DEFAULT '',
  `transcript` varchar(520) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_frequency_mode`
--

CREATE TABLE `scholarship_frequency_mode` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_individual_entry_requirement`
--

CREATE TABLE `scholarship_individual_entry_requirement` (
  `id` int(20) NOT NULL,
  `id_program` int(20) DEFAULT 0,
  `age` int(20) DEFAULT 0,
  `education` int(20) DEFAULT 0,
  `work_experience` int(20) DEFAULT 0,
  `other` int(20) DEFAULT 0,
  `min_age` int(20) DEFAULT 0,
  `max_age` int(20) DEFAULT 0,
  `id_education_qualification` int(20) DEFAULT 0,
  `education_description` varchar(2048) DEFAULT '',
  `min_work_experience` int(20) DEFAULT 0,
  `id_work_specialisation` int(20) DEFAULT 0,
  `work_description` varchar(2048) DEFAULT '',
  `other_description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_individual_entry_requirement`
--

INSERT INTO `scholarship_individual_entry_requirement` (`id`, `id_program`, `age`, `education`, `work_experience`, `other`, `min_age`, `max_age`, `id_education_qualification`, `education_description`, `min_work_experience`, `id_work_specialisation`, `work_description`, `other_description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(4, 1, 1, 0, 0, 0, 10, 88, 0, '', 0, 0, '', '', 1, NULL, '2020-07-30 12:54:23', NULL, '2020-07-30 12:54:23'),
(5, 1, 1, 1, 1, 1, 0, 0, 1, '', 0, 1, '', 'vxcvcv', 1, 1, '2020-08-18 10:34:45', NULL, '2020-08-18 10:34:45'),
(6, 1, 1, 0, 0, 1, 20, 30, 0, '', 0, 0, '', 'Must Graduated With Batchelor Degree .', 1, 1, '2020-08-18 11:43:54', NULL, '2020-08-18 11:43:54'),
(7, 2, 1, 1, 0, 0, 22, 29, 1, '', 0, 0, '', '', 1, 1, '2020-08-18 12:54:03', NULL, '2020-08-18 12:54:03'),
(8, 3, 1, 1, 1, 0, 19, 25, 1, '', 1, 1, '', '', 1, 1, '2020-08-20 06:52:58', NULL, '2020-08-20 06:52:58'),
(9, 3, 1, 1, 0, 0, 25, 29, 1, '', 0, 0, '', '', 1, 1, '2020-08-20 06:53:39', NULL, '2020-08-20 06:53:39');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_module_type_setup`
--

CREATE TABLE `scholarship_module_type_setup` (
  `id` int(20) NOT NULL,
  `code` varchar(200) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_module_type_setup`
--

INSERT INTO `scholarship_module_type_setup` (`id`, `code`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'COM', 'Compulsory', 1, NULL, '2020-07-23 07:09:30', NULL, '2020-07-23 07:09:30'),
(2, 'ELE', 'Elective', 1, NULL, '2020-07-23 18:09:01', NULL, '2020-07-23 18:09:01'),
(3, 'MQA', 'MQA', 1, NULL, '2020-07-23 18:09:15', NULL, '2020-07-23 18:09:15'),
(4, 'UNI', 'University Module', 1, NULL, '2020-07-23 18:09:30', NULL, '2020-07-23 18:09:30');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_organisation_has_department`
--

CREATE TABLE `scholarship_organisation_has_department` (
  `id` int(20) NOT NULL,
  `id_organisation` int(20) DEFAULT 0,
  `id_department` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_organisation_has_department`
--

INSERT INTO `scholarship_organisation_has_department` (`id`, `id_organisation`, `id_department`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 1, 1, NULL, NULL, '2020-07-30 11:03:25', NULL, '2020-07-30 11:03:25');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_organisation_setup`
--

CREATE TABLE `scholarship_organisation_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(500) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `short_name` varchar(500) DEFAULT '',
  `name_in_malay` varchar(500) DEFAULT '',
  `url` varchar(500) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_registrar` int(20) DEFAULT 0,
  `date_time` datetime DEFAULT current_timestamp(),
  `contact_number` int(20) DEFAULT 0,
  `email` varchar(200) DEFAULT '',
  `fax_number` varchar(50) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `id_state` int(20) DEFAULT 0,
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_organisation_setup`
--

INSERT INTO `scholarship_organisation_setup` (`id`, `name`, `code`, `short_name`, `name_in_malay`, `url`, `id_country`, `id_registrar`, `date_time`, `contact_number`, `email`, `fax_number`, `address1`, `address2`, `id_state`, `city`, `zipcode`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'College Management System', 'COD', 'CMS', 'College Management System', 'https://www.cms.com', 1, 0, '1970-01-01 00:00:00', 2147483647, 'email', '12121', 'KL', '9-2-353/1, KL', 1, 'sadsad', 22228888, 1, NULL, '2020-06-08 20:52:06', NULL, '2020-06-08 20:52:06');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_organisation_setup_comitee`
--

CREATE TABLE `scholarship_organisation_setup_comitee` (
  `id` int(20) NOT NULL,
  `id_organisation` int(20) DEFAULT 0,
  `role` varchar(50) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `nric` varchar(100) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_organisation_setup_comitee`
--

INSERT INTO `scholarship_organisation_setup_comitee` (`id`, `id_organisation`, `role`, `name`, `nric`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 1, 'Security Officer', 'Muzamil', 'NR888', '2020-07-24 00:00:00', NULL, NULL, '2020-07-22 01:40:53', NULL, '2020-07-22 01:40:53');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_partner_university`
--

CREATE TABLE `scholarship_partner_university` (
  `id` int(20) NOT NULL,
  `name` varchar(500) DEFAULT '',
  `short_name` varchar(500) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `certificate` varchar(200) DEFAULT '',
  `name_optional_language` varchar(500) DEFAULT '',
  `url` varchar(500) DEFAULT '',
  `id_registrar` int(20) DEFAULT 0,
  `id_contact_person` int(20) DEFAULT 0,
  `date_time` datetime DEFAULT current_timestamp(),
  `contact_number` int(20) DEFAULT 0,
  `email` varchar(200) DEFAULT '',
  `fax` varchar(200) DEFAULT '',
  `login_id` varchar(1048) DEFAULT '',
  `password` varchar(1048) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `location` varchar(512) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_state` int(20) DEFAULT 0,
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_partner_university`
--

INSERT INTO `scholarship_partner_university` (`id`, `name`, `short_name`, `code`, `start_date`, `end_date`, `certificate`, `name_optional_language`, `url`, `id_registrar`, `id_contact_person`, `date_time`, `contact_number`, `email`, `fax`, `login_id`, `password`, `address1`, `address2`, `location`, `id_country`, `id_state`, `city`, `zipcode`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 'Malaya University', 'Malaya UNi', 'MAL-UNI', '1970-01-01 00:00:00', '1970-01-01 00:00:00', '', '', 'www.malaya-uni.edu.my', 0, 0, '2020-07-22 10:32:16', 2147483647, 'vinaykiranp888@gmail.com', '5635623', '', '', 'KL', '', '', 1, 1, 'KL', 577522, 1, NULL, '2020-07-22 10:32:16', NULL, '2020-07-22 10:32:16'),
(3, 'Standford University', 'SU', 'Standford', '1970-01-01 00:00:00', '1970-01-01 00:00:00', '', '', 'su.edu.com', 0, 1, '2020-07-29 12:27:43', 2147483647, 'ubed@gmail.com', '1123', 'su', '123', '# 23, 23rd Main', 'KL', 'KL', 1, 2, 'KL', 223321, 1, NULL, '2020-07-29 12:27:43', NULL, '2020-07-29 12:27:43');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_partner_university_has_aggrement`
--

CREATE TABLE `scholarship_partner_university_has_aggrement` (
  `id` int(20) NOT NULL,
  `id_partner_university` int(20) DEFAULT 0,
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `file` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_partner_university_has_aggrement`
--

INSERT INTO `scholarship_partner_university_has_aggrement` (`id`, `id_partner_university`, `start_date`, `end_date`, `file`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 2, '2020-07-01 00:00:00', '2020-07-30 00:00:00', '24379fc1aaf4f02e2bd280b24bbb47da.jpeg', 0, NULL, '2020-07-26 23:22:14', NULL, '2020-07-26 23:22:14'),
(3, 2, '2020-07-01 00:00:00', '2020-07-31 00:00:00', 'aac48a05d9294d8a756297af28a339f3.pdf', 0, NULL, '2020-07-30 11:14:49', NULL, '2020-07-30 11:14:49'),
(5, 4, '2020-08-01 00:00:00', '2020-08-19 00:00:00', 'f7fc04d546f8c4861c6ca23a7724f0c4.png', 0, NULL, '2020-08-12 04:03:14', NULL, '2020-08-12 04:03:14');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_partner_university_has_program`
--

CREATE TABLE `scholarship_partner_university_has_program` (
  `id` int(20) NOT NULL,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `completion_criteria` int(20) DEFAULT NULL,
  `completion_criteria_type` varchar(1024) DEFAULT '',
  `assesment_method` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_partner_university_has_program`
--

INSERT INTO `scholarship_partner_university_has_program` (`id`, `id_partner_university`, `id_program`, `completion_criteria`, `completion_criteria_type`, `assesment_method`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 2, 1, 20, 'Credit Hours', 'CGPA', 1, NULL, '2020-07-23 04:14:13', NULL, '2020-07-23 04:14:13'),
(2, 3, 2, 30, 'Credit Hours', 'CGPA', 1, NULL, '2020-07-29 12:38:23', NULL, '2020-07-29 12:38:23'),
(3, 4, 1, 18, 'Credit Hours', 'CGPA', 1, NULL, '2020-08-12 03:59:43', NULL, '2020-08-12 03:59:43');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_partner_university_has_training_center`
--

CREATE TABLE `scholarship_partner_university_has_training_center` (
  `id` int(20) NOT NULL,
  `id_partner_university` int(20) DEFAULT 0,
  `name` varchar(500) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `complete_code` varchar(500) DEFAULT '',
  `id_contact_person` int(20) DEFAULT 0,
  `contact_number` int(20) DEFAULT 0,
  `email` varchar(200) DEFAULT '',
  `fax` varchar(200) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `location` varchar(512) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_state` int(20) DEFAULT 0,
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_partner_university_has_training_center`
--

INSERT INTO `scholarship_partner_university_has_training_center` (`id`, `id_partner_university`, `name`, `code`, `complete_code`, `id_contact_person`, `contact_number`, `email`, `fax`, `address1`, `address2`, `location`, `id_country`, `id_state`, `city`, `zipcode`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 2, 'Alamu 1', 'A1', '', 1, 2147483647, 'ubed@gmail.com', '323232', '# 23, 23rd Main', 'KL', 'Kalim Road', 1, 1, 'KL', 223321, 1, NULL, '2020-07-22 10:39:35', NULL, '2020-07-22 10:39:35'),
(3, 4, 'Training Center One', 'TRA1', 'AEUPJ_TRA1', 2, 1234322, 'contacttrainingcenter1@cms.com', '6767676', 'putra Jaya', 'putra Jaya', 'Near Middle City Forum', 1, 2, 'PJ', 565656, 1, NULL, '2020-08-12 03:59:17', NULL, '2020-08-12 03:59:17');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_partner_university_program_has_apprenticeship`
--

CREATE TABLE `scholarship_partner_university_program_has_apprenticeship` (
  `id` int(20) NOT NULL,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program_detail` int(20) DEFAULT NULL,
  `duration` int(20) DEFAULT NULL,
  `duration_type` varchar(200) DEFAULT '',
  `id_apprenticeship_center` int(20) DEFAULT NULL,
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_partner_university_program_has_apprenticeship`
--

INSERT INTO `scholarship_partner_university_program_has_apprenticeship` (`id`, `id_partner_university`, `id_program_detail`, `duration`, `duration_type`, `id_apprenticeship_center`, `start_date`, `end_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 2, 1, 1, 'Month', 2, '2020-07-07 00:00:00', '2020-07-25 00:00:00', NULL, NULL, '2020-07-23 07:35:19', NULL, '2020-07-23 07:35:19'),
(3, 4, 3, 2, 'Month', 3, '2020-08-26 00:00:00', '2020-12-25 00:00:00', NULL, NULL, '2020-08-12 04:01:21', NULL, '2020-08-12 04:01:21');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_partner_university_program_has_internship`
--

CREATE TABLE `scholarship_partner_university_program_has_internship` (
  `id` int(20) NOT NULL,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program_detail` int(20) DEFAULT NULL,
  `duration` int(20) DEFAULT NULL,
  `duration_type` varchar(200) DEFAULT '',
  `id_internship_center` int(20) DEFAULT NULL,
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_partner_university_program_has_internship`
--

INSERT INTO `scholarship_partner_university_program_has_internship` (`id`, `id_partner_university`, `id_program_detail`, `duration`, `duration_type`, `id_internship_center`, `start_date`, `end_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 4, 3, 4, 'Month', 3, '2020-08-15 00:00:00', '2020-12-14 00:00:00', NULL, NULL, '2020-08-12 04:00:47', NULL, '2020-08-12 04:00:47');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_partner_university_program_has_study_mode`
--

CREATE TABLE `scholarship_partner_university_program_has_study_mode` (
  `id` int(20) NOT NULL,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program_detail` int(20) DEFAULT NULL,
  `mode_of_study` varchar(200) DEFAULT '',
  `min_duration` int(20) DEFAULT NULL,
  `max_duration` int(20) DEFAULT NULL,
  `min_duration_type` varchar(200) DEFAULT '',
  `max_duration_type` varchar(200) DEFAULT '',
  `id_training_center` int(20) DEFAULT NULL,
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_partner_university_program_has_study_mode`
--

INSERT INTO `scholarship_partner_university_program_has_study_mode` (`id`, `id_partner_university`, `id_program_detail`, `mode_of_study`, `min_duration`, `max_duration`, `min_duration_type`, `max_duration_type`, `id_training_center`, `start_date`, `end_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 4, 3, 'Full Time', 1, 2, 'Month', 'Month', 3, '2020-08-01 00:00:00', '2020-08-31 00:00:00', NULL, NULL, '2020-08-12 04:00:14', NULL, '2020-08-12 04:00:14');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_partner_university_program_has_syllabus`
--

CREATE TABLE `scholarship_partner_university_program_has_syllabus` (
  `id` int(20) NOT NULL,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program_detail` int(20) DEFAULT NULL,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `credit` int(20) DEFAULT NULL,
  `credit_type` varchar(200) DEFAULT '',
  `id_module_type` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_partner_university_program_has_syllabus`
--

INSERT INTO `scholarship_partner_university_program_has_syllabus` (`id`, `id_partner_university`, `id_program_detail`, `name`, `code`, `credit`, `credit_type`, `id_module_type`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 4, 3, 'Module Syllabus One', 'MOD1', 1, 'Month', 2, NULL, NULL, '2020-08-12 04:01:51', NULL, '2020-08-12 04:01:51');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_payment_type`
--

CREATE TABLE `scholarship_payment_type` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_programme`
--

CREATE TABLE `scholarship_programme` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(250) DEFAULT '',
  `id_award` int(10) DEFAULT 0,
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `code` varchar(50) DEFAULT '',
  `total_cr_hrs` varchar(20) DEFAULT '',
  `graduate_studies` varchar(100) DEFAULT '',
  `foundation` varchar(50) DEFAULT '',
  `is_personel_details` int(11) DEFAULT 1,
  `is_education_details` int(11) DEFAULT 1,
  `is_family_details` int(11) DEFAULT 1,
  `is_financial_recoverable_details` int(11) DEFAULT 1,
  `is_file_upload_details` int(11) DEFAULT 1,
  `is_employment_details` int(11) DEFAULT 1,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_programme`
--

INSERT INTO `scholarship_programme` (`id`, `name`, `name_optional_language`, `id_award`, `start_date`, `end_date`, `code`, `total_cr_hrs`, `graduate_studies`, `foundation`, `is_personel_details`, `is_education_details`, `is_family_details`, `is_financial_recoverable_details`, `is_file_upload_details`, `is_employment_details`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Juruteknik Pemesinan CNC', 'Juruteknik Pemesinan CNC', 1, '2020-07-22 00:00:00', '2020-07-22 00:00:00', 'PS01', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, '2020-07-22 02:12:09', NULL, '2020-07-22 02:12:09'),
(2, 'Wiring Level 1', '', 2, '2020-07-01 00:00:00', '2020-12-31 00:00:00', 'WOR', '', '', '', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-07-24 13:19:07', NULL, '2020-07-24 13:19:07'),
(3, 'Prog', 'PP', 2, '2020-07-10 00:00:00', '2020-07-16 00:00:00', 'Program', '', '', '', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-07-24 23:42:06', NULL, '2020-07-24 23:42:06');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_program_accreditation`
--

CREATE TABLE `scholarship_program_accreditation` (
  `id` int(20) NOT NULL,
  `id_accreditation_category` int(20) DEFAULT NULL,
  `id_accreditation_type` int(20) DEFAULT NULL,
  `date_time` datetime DEFAULT current_timestamp(),
  `valid_from` datetime DEFAULT current_timestamp(),
  `valid_to` datetime DEFAULT current_timestamp(),
  `approval_date` datetime DEFAULT current_timestamp(),
  `accreditation_number` varchar(2048) DEFAULT '',
  `reference_number` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_program_accreditation`
--

INSERT INTO `scholarship_program_accreditation` (`id`, `id_accreditation_category`, `id_accreditation_type`, `date_time`, `valid_from`, `valid_to`, `approval_date`, `accreditation_number`, `reference_number`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 2, '2021-07-08 00:00:00', '2020-07-08 00:00:00', '2021-07-08 00:00:00', '2020-07-08 00:00:00', '123', 'REFE123', 1, NULL, '2020-07-27 12:27:46', NULL, '2020-07-27 12:27:46');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_program_partner`
--

CREATE TABLE `scholarship_program_partner` (
  `id` int(20) NOT NULL,
  `id_thrust` int(20) DEFAULT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_location` int(20) DEFAULT NULL,
  `internship` int(20) DEFAULT NULL,
  `apprenticeship` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_program_partner`
--

INSERT INTO `scholarship_program_partner` (`id`, `id_thrust`, `id_sub_thrust`, `id_program`, `id_partner_university`, `id_location`, `internship`, `apprenticeship`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, 2, 1, 6, 6, 1, NULL, '2020-07-27 12:17:38', NULL, '2020-07-27 12:17:38');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_program_syllabus`
--

CREATE TABLE `scholarship_program_syllabus` (
  `id` int(20) NOT NULL,
  `id_thrust` int(20) DEFAULT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_program_syllabus`
--

INSERT INTO `scholarship_program_syllabus` (`id`, `id_thrust`, `id_sub_thrust`, `id_program`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 3, 1, NULL, '2020-07-27 12:25:21', NULL, '2020-07-27 12:25:21');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_program_syllabus_has_module`
--

CREATE TABLE `scholarship_program_syllabus_has_module` (
  `id` int(20) NOT NULL,
  `id_program_syllabus` int(20) DEFAULT NULL,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `credit_hours` int(20) DEFAULT 0,
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_program_syllabus_has_module`
--

INSERT INTO `scholarship_program_syllabus_has_module` (`id`, `id_program_syllabus`, `name`, `code`, `credit_hours`, `type`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Module One', 'MOD-1', 28, 'Compulsary', NULL, NULL, '2020-07-27 12:25:45', NULL, '2020-07-27 12:25:45');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_race_setup`
--

CREATE TABLE `scholarship_race_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_race_setup`
--

INSERT INTO `scholarship_race_setup` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Bumiputera - Malay', 'BM', 1, NULL, '2020-07-21 23:48:32', NULL, '2020-07-21 23:48:32'),
(2, 'Bumiputera - Sabah', 'BS', 1, NULL, '2020-07-21 23:48:50', NULL, '2020-07-21 23:48:50'),
(3, 'Bumiputera - Sarawak', 'BSK', 1, NULL, '2020-07-21 23:49:12', NULL, '2020-07-21 23:49:12'),
(4, 'Bumiputera - Orang Asli', 'BOA', 1, NULL, '2020-07-21 23:49:26', NULL, '2020-07-21 23:49:26');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_religion_setup`
--

CREATE TABLE `scholarship_religion_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_religion_setup`
--

INSERT INTO `scholarship_religion_setup` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Islam', '', 1, NULL, '2020-07-21 23:49:53', NULL, '2020-07-21 23:49:53'),
(2, 'Christian', '', 1, NULL, '2020-07-21 23:50:05', NULL, '2020-07-21 23:50:05'),
(3, 'No Religion', '', 1, NULL, '2020-07-21 23:51:02', NULL, '2020-07-21 23:51:02');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_salutation_setup`
--

CREATE TABLE `scholarship_salutation_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `sequence` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_salutation_setup`
--

INSERT INTO `scholarship_salutation_setup` (`id`, `name`, `sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Mr', 1, 1, NULL, '2020-07-22 01:31:06', NULL, '2020-07-22 01:31:06');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_scheme`
--

CREATE TABLE `scholarship_scheme` (
  `id` int(20) NOT NULL,
  `name` varchar(1048) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `percentage` int(10) DEFAULT 0,
  `from_dt` date DEFAULT NULL,
  `to_dt` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_scheme`
--

INSERT INTO `scholarship_scheme` (`id`, `name`, `code`, `percentage`, `from_dt`, `to_dt`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'National Scholarship', 'NSP', 0, '2020-07-01', '2020-08-31', 1, NULL, '2020-07-29 12:41:35', NULL, '2020-07-29 12:41:35');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_selection_type`
--

CREATE TABLE `scholarship_selection_type` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `calculation_method` varchar(200) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_selection_type`
--

INSERT INTO `scholarship_selection_type` (`id`, `name`, `calculation_method`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Interview', 'Pass/Fail', '', 1, NULL, '2020-07-24 05:15:26', NULL, '2020-07-24 05:15:26'),
(2, 'Interview', 'Mark', '', 1, NULL, '2020-07-24 05:16:12', NULL, '2020-07-24 05:16:12');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_state`
--

CREATE TABLE `scholarship_state` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `id_country` int(10) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_state`
--

INSERT INTO `scholarship_state` (`id`, `name`, `id_country`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Kedah', 1, 1, 1, NULL, NULL, '2020-07-22 01:34:22'),
(2, 'KL', 1, 1, 1, NULL, NULL, '2020-07-22 01:35:37');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_sub_thrust`
--

CREATE TABLE `scholarship_sub_thrust` (
  `id` int(20) NOT NULL,
  `id_thrust` int(20) DEFAULT 0,
  `id_scholarship_manager` int(20) DEFAULT 0,
  `scholarship_name` varchar(1024) DEFAULT '',
  `scholarship_short_name` varchar(1024) DEFAULT '',
  `scholarship_code` varchar(200) DEFAULT '',
  `scholarship_name_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_sub_thrust`
--

INSERT INTO `scholarship_sub_thrust` (`id`, `id_thrust`, `id_scholarship_manager`, `scholarship_name`, `scholarship_short_name`, `scholarship_code`, `scholarship_name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 0, 'Scholarship', 'SCH', 'SCH', '', 1, NULL, '2020-07-24 09:31:18', NULL, '2020-07-24 09:31:18');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_sub_thrust_has_entry_requirement`
--

CREATE TABLE `scholarship_sub_thrust_has_entry_requirement` (
  `id` int(20) NOT NULL,
  `id_sub_thrust` int(20) DEFAULT 0,
  `nationality` varchar(200) DEFAULT '',
  `id_race` int(20) DEFAULT 0,
  `age` int(20) DEFAULT 0,
  `education` int(20) DEFAULT 0,
  `income` int(20) DEFAULT 0,
  `other` int(20) DEFAULT 0,
  `min_age` int(20) DEFAULT 0,
  `max_age` int(20) DEFAULT 0,
  `id_education_qualification` int(20) DEFAULT 0,
  `education_description` varchar(2048) DEFAULT '',
  `min_income` int(20) DEFAULT 0,
  `max_income` int(20) DEFAULT 0,
  `income_type` varchar(2048) DEFAULT '',
  `other_description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_sub_thrust_has_entry_requirement`
--

INSERT INTO `scholarship_sub_thrust_has_entry_requirement` (`id`, `id_sub_thrust`, `nationality`, `id_race`, `age`, `education`, `income`, `other`, `min_age`, `max_age`, `id_education_qualification`, `education_description`, `min_income`, `max_income`, `income_type`, `other_description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 1, 'Other', 2, 1, 1, 1, 0, 18, 21, 1, 'Compulsary Batchelor Degree', 20000, 28000, 'Personal Income', '', NULL, NULL, '2020-07-29 12:43:55', NULL, '2020-07-29 12:43:55'),
(3, 1, 'Malaysian', 1, 1, 1, 1, 1, 17, 25, 1, 'Compulsary Degree', 15000, 30000, 'Household Income', 'All Previous Documents Required', NULL, NULL, '2020-07-29 12:46:20', NULL, '2020-07-29 12:46:20'),
(4, 1, 'Malaysian', 1, 1, 0, 0, 0, 20, 28, 0, '', 0, 0, '', '', NULL, NULL, '2020-07-30 12:56:55', NULL, '2020-07-30 12:56:55');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_sub_thrust_has_program`
--

CREATE TABLE `scholarship_sub_thrust_has_program` (
  `id` int(20) NOT NULL,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_sub_thrust_has_program`
--

INSERT INTO `scholarship_sub_thrust_has_program` (`id`, `id_partner_university`, `id_program`, `id_sub_thrust`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(7, 2, 1, 1, NULL, NULL, '2020-07-26 18:40:01', NULL, '2020-07-26 18:40:01'),
(9, 3, 2, 1, NULL, NULL, '2020-07-29 12:39:33', NULL, '2020-07-29 12:39:33'),
(10, 2, 1, 1, NULL, NULL, '2020-07-29 12:39:57', NULL, '2020-07-29 12:39:57');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_sub_thrust_has_requirement`
--

CREATE TABLE `scholarship_sub_thrust_has_requirement` (
  `id` int(20) NOT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `requirement_entry` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_sub_thrust_has_requirement`
--

INSERT INTO `scholarship_sub_thrust_has_requirement` (`id`, `id_sub_thrust`, `id_program`, `requirement_entry`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 'Age', 1, NULL, '2020-07-24 12:18:47', NULL, '2020-07-24 12:18:47'),
(2, 1, 1, 'Education', 1, NULL, '2020-07-24 12:19:10', NULL, '2020-07-24 12:19:10'),
(3, 1, 1, 'Work Experience', 1, NULL, '2020-07-24 12:20:12', NULL, '2020-07-24 12:20:12'),
(4, 1, 1, 'Other Requirement', 1, NULL, '2020-07-24 12:20:49', NULL, '2020-07-24 12:20:49'),
(5, 1, 1, 'Age', 1, NULL, '2020-07-24 12:20:58', NULL, '2020-07-24 12:20:58'),
(6, 1, 1, 'Age', 1, NULL, '2020-07-24 13:20:46', NULL, '2020-07-24 13:20:46'),
(7, 1, 1, 'Education', 1, NULL, '2020-07-24 23:34:59', NULL, '2020-07-24 23:34:59'),
(8, 1, 2, 'Education', 1, NULL, '2020-07-24 23:34:59', NULL, '2020-07-24 23:34:59'),
(9, 1, 1, 'Work Experience', 1, NULL, '2020-07-24 23:35:48', NULL, '2020-07-24 23:35:48'),
(10, 1, 2, 'Work Experience', 1, NULL, '2020-07-24 23:35:48', NULL, '2020-07-24 23:35:48'),
(11, 1, 1, 'Age', 1, NULL, '2020-07-26 18:37:35', NULL, '2020-07-26 18:37:35'),
(12, 1, 3, 'Age', 1, NULL, '2020-07-26 18:37:35', NULL, '2020-07-26 18:37:35'),
(13, 1, 1, 'Education', 1, NULL, '2020-07-26 18:38:53', NULL, '2020-07-26 18:38:53'),
(14, 1, 3, 'Education', 1, NULL, '2020-07-26 18:38:53', NULL, '2020-07-26 18:38:53'),
(15, 1, 2, 'Education', 1, NULL, '2020-07-26 18:38:53', NULL, '2020-07-26 18:38:53');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_sub_thrust_requirement_has_age`
--

CREATE TABLE `scholarship_sub_thrust_requirement_has_age` (
  `id` int(20) NOT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_requirement` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `min_age` int(20) DEFAULT NULL,
  `max_age` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_sub_thrust_requirement_has_age`
--

INSERT INTO `scholarship_sub_thrust_requirement_has_age` (`id`, `id_sub_thrust`, `id_requirement`, `id_program`, `min_age`, `max_age`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(3, 1, 6, 1, 18, 22, NULL, NULL, '2020-07-24 13:20:46', NULL, '2020-07-24 13:20:46'),
(4, 1, 11, 1, 12, 12222, NULL, NULL, '2020-07-26 18:37:35', NULL, '2020-07-26 18:37:35'),
(5, 1, 12, 3, 12, 12222, NULL, NULL, '2020-07-26 18:37:35', NULL, '2020-07-26 18:37:35');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_sub_thrust_requirement_has_education`
--

CREATE TABLE `scholarship_sub_thrust_requirement_has_education` (
  `id` int(20) NOT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_requirement` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `id_qualification` int(20) DEFAULT NULL,
  `education_descrption` varchar(2048) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_sub_thrust_requirement_has_education`
--

INSERT INTO `scholarship_sub_thrust_requirement_has_education` (`id`, `id_sub_thrust`, `id_requirement`, `id_program`, `id_qualification`, `education_descrption`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 2, 1, 1, 'Education Compulsary', NULL, NULL, '2020-07-24 12:19:10', NULL, '2020-07-24 12:19:10'),
(2, 1, 7, 1, 1, 'Batchelor Degree Required', NULL, NULL, '2020-07-24 23:34:59', NULL, '2020-07-24 23:34:59'),
(3, 1, 8, 2, 1, 'Batchelor Degree Required', NULL, NULL, '2020-07-24 23:34:59', NULL, '2020-07-24 23:34:59'),
(4, 1, 13, 1, 1, '23', NULL, NULL, '2020-07-26 18:38:53', NULL, '2020-07-26 18:38:53'),
(5, 1, 14, 3, 1, '23', NULL, NULL, '2020-07-26 18:38:53', NULL, '2020-07-26 18:38:53'),
(6, 1, 15, 2, 1, '23', NULL, NULL, '2020-07-26 18:38:53', NULL, '2020-07-26 18:38:53');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_sub_thrust_requirement_has_other`
--

CREATE TABLE `scholarship_sub_thrust_requirement_has_other` (
  `id` int(20) NOT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_requirement` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `other_descrption` varchar(2048) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_sub_thrust_requirement_has_other`
--

INSERT INTO `scholarship_sub_thrust_requirement_has_other` (`id`, `id_sub_thrust`, `id_requirement`, `id_program`, `other_descrption`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 4, 1, 'Other Details i\'ll Be Updated Soon', NULL, NULL, '2020-07-24 12:20:49', NULL, '2020-07-24 12:20:49');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_sub_thrust_requirement_has_work_experience`
--

CREATE TABLE `scholarship_sub_thrust_requirement_has_work_experience` (
  `id` int(20) NOT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_requirement` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `min_experience` int(20) DEFAULT NULL,
  `id_specialisation` int(20) DEFAULT NULL,
  `experience_descrption` varchar(2048) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_sub_thrust_requirement_has_work_experience`
--

INSERT INTO `scholarship_sub_thrust_requirement_has_work_experience` (`id`, `id_sub_thrust`, `id_requirement`, `id_program`, `min_experience`, `id_specialisation`, `experience_descrption`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 3, 1, 1, 1, 'Work Experience Compulsary If U Select This Option', NULL, NULL, '2020-07-24 12:20:12', NULL, '2020-07-24 12:20:12'),
(2, 1, 9, 1, 1, 1, 'Work Experience Reuired If Applying Under Experience', NULL, NULL, '2020-07-24 23:35:48', NULL, '2020-07-24 23:35:48'),
(3, 1, 10, 2, 1, 1, 'Work Experience Reuired If Applying Under Experience', NULL, NULL, '2020-07-24 23:35:48', NULL, '2020-07-24 23:35:48');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_thrust`
--

CREATE TABLE `scholarship_thrust` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_thrust`
--

INSERT INTO `scholarship_thrust` (`id`, `name`, `code`, `name_optional_language`, `start_date`, `end_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Thrust Scholar', 'THR', '', '2020-07-13 00:00:00', '2020-07-03 00:00:00', 1, NULL, '2020-07-23 10:32:08', NULL, '2020-07-23 10:32:08');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_users`
--

CREATE TABLE `scholarship_users` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `role_id` tinyint(4) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_dt_tm` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scholarship_users`
--

INSERT INTO `scholarship_users` (`id`, `email`, `password`, `name`, `mobile`, `role_id`, `is_deleted`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'vk@cms.com', 'admin', 'Vinay', '08550078787', 1, 0, 1, '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_work_specialisation`
--

CREATE TABLE `scholarship_work_specialisation` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_work_specialisation`
--

INSERT INTO `scholarship_work_specialisation` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Work Specialisation', 'WS', '', 1, NULL, '2020-07-24 23:32:43', NULL, '2020-07-24 23:32:43');

-- --------------------------------------------------------

--
-- Table structure for table `scholar_applicant`
--

CREATE TABLE `scholar_applicant` (
  `id` int(20) NOT NULL,
  `id_intake` int(11) DEFAULT NULL,
  `id_program` int(11) DEFAULT NULL,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `passport` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `is_hostel` int(20) DEFAULT 0,
  `id_degree_type` int(20) DEFAULT 0,
  `id_race` varchar(120) DEFAULT '',
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `mailing_zipcode` varchar(120) DEFAULT '',
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `sibbling_discount` varchar(50) DEFAULT '',
  `employee_discount` varchar(50) DEFAULT '',
  `applicant_status` varchar(50) DEFAULT 'Draft',
  `is_sibbling_discount` int(20) DEFAULT 3,
  `is_employee_discount` int(20) DEFAULT 3,
  `id_type` varchar(888) DEFAULT NULL,
  `passport_number` varchar(1024) DEFAULT NULL,
  `email_verified` int(2) DEFAULT 0,
  `is_submitted` int(2) DEFAULT 0,
  `is_updated` int(2) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `approved_by` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholar_applicant`
--

INSERT INTO `scholar_applicant` (`id`, `id_intake`, `id_program`, `full_name`, `salutation`, `first_name`, `last_name`, `nric`, `passport`, `phone`, `email_id`, `password`, `passport_expiry_date`, `gender`, `date_of_birth`, `martial_status`, `religion`, `nationality`, `is_hostel`, `id_degree_type`, `id_race`, `mail_address1`, `mail_address2`, `mailing_country`, `mailing_state`, `mailing_city`, `mailing_zipcode`, `permanent_address1`, `permanent_address2`, `permanent_country`, `permanent_state`, `permanent_city`, `permanent_zipcode`, `sibbling_discount`, `employee_discount`, `applicant_status`, `is_sibbling_discount`, `is_employee_discount`, `id_type`, `passport_number`, `email_verified`, `is_submitted`, `is_updated`, `reason`, `status`, `approved_by`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, NULL, NULL, 'Miss. Sumangala K', 'Miss', 'Sumangala', 'K', 'NR333', '', '7878787878', 's@cms.com', '123', '', '', '', '', '', '', 0, 0, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', '', 'Draft', 3, 3, NULL, NULL, 1, 0, 0, '', NULL, 0, NULL, '2020-07-21 21:09:18', NULL, '2020-07-21 21:09:18');

-- --------------------------------------------------------

--
-- Table structure for table `scholar_applicant_employment_status`
--

CREATE TABLE `scholar_applicant_employment_status` (
  `id` int(20) NOT NULL,
  `id_scholar_applicant` int(10) DEFAULT NULL,
  `id_scholarship_scheme` int(20) DEFAULT 0,
  `year` int(20) DEFAULT 0,
  `id_application` int(20) DEFAULT 0,
  `company_name` varchar(120) DEFAULT '',
  `company_address` varchar(120) DEFAULT '',
  `telephone` varchar(120) DEFAULT '',
  `fax_num` varchar(120) DEFAULT '',
  `designation` varchar(120) DEFAULT '',
  `position` varchar(120) DEFAULT '',
  `service_year` varchar(120) DEFAULT '',
  `industry` varchar(120) DEFAULT '',
  `job_desc` varchar(520) DEFAULT '',
  `employment_letter` varchar(520) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholar_applicant_family_details`
--

CREATE TABLE `scholar_applicant_family_details` (
  `id` int(20) NOT NULL,
  `id_scholar_applicant` bigint(20) DEFAULT 0,
  `id_application` int(20) DEFAULT 0,
  `id_scholarship_scheme` int(20) DEFAULT 0,
  `year` int(20) DEFAULT 0,
  `father_name` varchar(1024) DEFAULT '',
  `mother_name` varchar(1024) DEFAULT '',
  `father_deceased` bigint(20) DEFAULT 0,
  `father_occupation` varchar(1024) DEFAULT '',
  `no_siblings` bigint(20) DEFAULT 0,
  `est_fee` bigint(20) DEFAULT 0,
  `family_annual_income` float(20,2) NOT NULL DEFAULT 0.00,
  `reason` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholar_applicant_family_details`
--

INSERT INTO `scholar_applicant_family_details` (`id`, `id_scholar_applicant`, `id_application`, `id_scholarship_scheme`, `year`, `father_name`, `mother_name`, `father_deceased`, `father_occupation`, `no_siblings`, `est_fee`, `family_annual_income`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 0, 0, 0, '', '', 0, '', 0, 0, 0.00, '', 0, NULL, '2020-07-21 21:09:18', NULL, '2020-07-21 21:09:18');

-- --------------------------------------------------------

--
-- Table structure for table `scholar_applicant_last_login`
--

CREATE TABLE `scholar_applicant_last_login` (
  `id` bigint(20) NOT NULL,
  `id_scholar_applicant` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scholar_applicant_last_login`
--

INSERT INTO `scholar_applicant_last_login` (`id`, `id_scholar_applicant`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(1, 1, '{\"scholar_applicant_name\":\"Miss. Sumangala K\",\"scholar_applicant_email_id\":\"s@cms.com\"}', '112.79.53.129', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-21 21:09:30');

-- --------------------------------------------------------

--
-- Table structure for table `scholar_last_login`
--

CREATE TABLE `scholar_last_login` (
  `id` bigint(20) NOT NULL,
  `id_scholar` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scholar_last_login`
--

INSERT INTO `scholar_last_login` (`id`, `id_scholar`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(1, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":null}', '112.79.53.129', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-21 21:05:01'),
(2, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-21 21:05:01\"}', '112.79.53.129', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-21 21:07:03'),
(3, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-21 21:07:03\"}', '112.79.53.68', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-21 21:44:20'),
(4, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-21 21:44:20\"}', '112.79.53.68', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-21 21:44:42'),
(5, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-21 21:44:42\"}', '112.79.53.68', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-21 21:45:19'),
(6, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-21 21:45:19\"}', '112.79.53.68', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-21 21:46:10'),
(7, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-21 21:46:10\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-21 21:47:46'),
(8, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-21 21:47:46\"}', '49.206.7.23', 'Safari 604.1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Mobile/15E148 Safari/604.1', 'iOS', '2020-07-21 21:47:56'),
(9, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-21 21:47:56\"}', '112.79.53.158', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-22 01:30:33'),
(10, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-22 01:30:33\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-22 06:50:16'),
(11, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-22 06:50:16\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-22 09:21:05'),
(12, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-22 09:21:05\"}', '112.79.54.84', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-22 10:29:20'),
(13, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-22 10:29:20\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-23 00:45:32'),
(14, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-23 00:45:32\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-23 03:02:55'),
(15, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-23 03:02:55\"}', '117.230.37.241', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-23 04:13:00'),
(16, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-23 04:13:00\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-23 05:34:26'),
(17, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-23 05:34:26\"}', '117.230.5.6', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-23 07:05:50'),
(18, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-23 07:05:50\"}', '112.79.48.147', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-23 10:25:22'),
(19, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-23 10:25:22\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-23 18:06:06'),
(20, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-23 18:06:06\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-23 22:55:31'),
(21, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-23 22:55:31\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-24 05:14:55'),
(22, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-24 05:14:55\"}', '112.79.48.230', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-24 09:30:15'),
(23, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-24 09:30:15\"}', '112.79.49.216', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-24 12:18:13'),
(24, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-24 12:18:13\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-24 13:10:20'),
(25, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-24 13:10:20\"}', '112.79.80.186', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-24 23:28:39'),
(26, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-24 23:28:39\"}', '112.79.51.20', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-25 07:42:04'),
(27, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-25 07:42:04\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-25 08:00:06'),
(28, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-25 08:00:06\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-07-26 18:34:58'),
(29, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-26 18:34:58\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-26 21:35:08'),
(30, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-26 21:35:08\"}', '112.79.49.125', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-26 22:49:02'),
(31, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-26 22:49:02\"}', '157.49.103.87', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-27 03:46:00'),
(32, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-27 03:46:00\"}', '112.79.49.29', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-27 12:15:51'),
(33, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-27 12:15:51\"}', '112.79.87.32', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-28 07:01:05'),
(34, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-28 07:01:05\"}', '112.79.51.233', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-29 07:16:40'),
(35, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-29 07:16:40\"}', '137.97.44.116', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-29 12:23:22'),
(36, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-29 12:23:22\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-07-30 10:53:17'),
(37, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-30 10:53:17\"}', '112.79.53.224', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-30 12:52:42'),
(38, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-30 12:52:42\"}', '42.190.15.202', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36', 'Windows 10', '2020-08-04 10:48:07'),
(39, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-08-04 10:48:07\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-11 21:23:59'),
(40, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-08-11 21:23:59\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-18 10:33:59');

-- --------------------------------------------------------

--
-- Table structure for table `scholar_permissions`
--

CREATE TABLE `scholar_permissions` (
  `id` bigint(20) NOT NULL,
  `code` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `status` int(2) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholar_permissions`
--

INSERT INTO `scholar_permissions` (`id`, `code`, `description`, `status`) VALUES
(1, 'SM', 'Scholar Manager', 1),
(2, 'sub_thrust.add', 'Sub Thrust Add', 1),
(3, 'application.approve', 'Application Aproval', 1);

-- --------------------------------------------------------

--
-- Table structure for table `scholar_roles`
--

CREATE TABLE `scholar_roles` (
  `id` tinyint(4) NOT NULL COMMENT 'role id',
  `role` varchar(50) NOT NULL COMMENT 'role text',
  `status` int(2) DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scholar_roles`
--

INSERT INTO `scholar_roles` (`id`, `role`, `status`) VALUES
(1, 'Scholarship Manager', 1),
(2, 'Programme Partner', 1),
(3, 'Verification Officer', 1);

-- --------------------------------------------------------

--
-- Table structure for table `scholar_role_permissions`
--

CREATE TABLE `scholar_role_permissions` (
  `id` bigint(20) NOT NULL,
  `id_role` bigint(20) DEFAULT NULL,
  `id_permission` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholar_role_permissions`
--

INSERT INTO `scholar_role_permissions` (`id`, `id_role`, `id_permission`) VALUES
(3, 2, 2),
(4, 1, 2),
(5, 1, 1),
(6, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `school_details`
--

CREATE TABLE `school_details` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `reg_no` varchar(20) DEFAULT '',
  `address_one` varchar(520) DEFAULT '',
  `address_two` varchar(520) DEFAULT '',
  `address_three` varchar(520) DEFAULT '',
  `phone_number` varchar(20) DEFAULT '',
  `mobile_number` varchar(20) DEFAULT '',
  `email` varchar(120) DEFAULT '',
  `fax` varchar(20) DEFAULT '',
  `pincode` int(10) DEFAULT NULL,
  `id_city` int(10) DEFAULT NULL,
  `id_state` int(10) DEFAULT NULL,
  `logo` varchar(120) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(20) DEFAULT '',
  `name_optional_language` varchar(200) DEFAULT '',
  `id_academic_year` int(10) DEFAULT NULL,
  `id_scheme` int(20) DEFAULT 0,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `semester_type` varchar(50) DEFAULT '',
  `is_countable` int(2) DEFAULT 0,
  `special_semester` int(2) DEFAULT 0,
  `semester_sequence` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`id`, `name`, `code`, `name_optional_language`, `id_academic_year`, `id_scheme`, `start_date`, `end_date`, `semester_type`, `is_countable`, `special_semester`, `semester_sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'First Semester', 'SEM-1', '', 1, 0, '2020-08-30 00:00:00', '2021-03-03 00:00:00', 'Long', 0, 0, 'September', 1, NULL, NULL, NULL, NULL),
(2, 'Second Semester', 'SEM-2', '', 1, 3, '2020-08-01 00:00:00', '2021-08-31 00:00:00', 'Long', 1, 0, 'January', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `semester_has_activity`
--

CREATE TABLE `semester_has_activity` (
  `id` int(20) NOT NULL,
  `id_semester` int(10) DEFAULT NULL,
  `id_activity` int(10) DEFAULT NULL,
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `set_form`
--

CREATE TABLE `set_form` (
  `id` int(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `id_pool` int(20) DEFAULT 0,
  `id_branch` int(20) DEFAULT 0,
  `id_semester` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `id_program_scheme` int(20) DEFAULT 0,
  `is_publish` int(20) DEFAULT 0,
  `published_by` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `set_form_has_question_pool`
--

CREATE TABLE `set_form_has_question_pool` (
  `id` int(20) NOT NULL,
  `id_set_form` int(20) DEFAULT 0,
  `id_pool` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sibbling_discount`
--

CREATE TABLE `sibbling_discount` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `amount` varchar(520) DEFAULT '',
  `currency` varchar(200) DEFAULT '',
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `sibbling_status` varchar(50) DEFAULT 'Pending',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sibbling_discount`
--

INSERT INTO `sibbling_discount` (`id`, `name`, `amount`, `currency`, `start_date`, `end_date`, `sibbling_status`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Sibbling Discount', '20', 'USD', '2020-07-11 00:00:00', '2020-07-11 00:00:00', 'Pending', 1, NULL, '2020-07-11 00:39:26', NULL, '2020-07-11 00:39:26'),
(2, 'Sibbling Discount', '80', 'MYR', '2020-08-01 00:00:00', '2020-08-31 00:00:00', 'Pending', 1, NULL, '2020-08-20 04:32:21', NULL, '2020-08-20 04:32:21');

-- --------------------------------------------------------

--
-- Table structure for table `specialization_setup`
--

CREATE TABLE `specialization_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `specialization_setup`
--

INSERT INTO `specialization_setup` (`id`, `name`, `name_in_malay`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Minimum 2 Principal Passes with a minimum grade C+ (CGPA 2.33)', '', 'M', 1, NULL, '2020-07-19 02:51:42', NULL, '2020-07-19 02:51:42');

-- --------------------------------------------------------

--
-- Table structure for table `sponser`
--

CREATE TABLE `sponser` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `mobile_number` varchar(20) DEFAULT '',
  `email` varchar(500) DEFAULT '',
  `fax` varchar(20) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `location` varchar(1024) DEFAULT '',
  `address` varchar(520) DEFAULT '',
  `id_state` int(20) DEFAULT NULL,
  `id_country` int(20) DEFAULT NULL,
  `zip_code` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sponser`
--

INSERT INTO `sponser` (`id`, `name`, `mobile_number`, `email`, `fax`, `code`, `address2`, `location`, `address`, `id_state`, `id_country`, `zip_code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Syed Nasrullah', '67564543', 'nasu@cms.com', '12322124', 'C000001/2020', 'KL', 'kl', 'KL', 1, 1, 566778, 1, NULL, '2020-08-08 07:33:13', NULL, '2020-08-08 07:33:13');

-- --------------------------------------------------------

--
-- Table structure for table `sponser_coordinator_details`
--

CREATE TABLE `sponser_coordinator_details` (
  `id` int(20) NOT NULL,
  `id_sponser` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `email` varchar(1024) DEFAULT '',
  `phone` int(20) DEFAULT 0,
  `mobile` int(20) DEFAULT 0,
  `fax` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sponser_coordinator_details`
--

INSERT INTO `sponser_coordinator_details` (`id`, `id_sponser`, `name`, `email`, `phone`, `mobile`, `fax`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Binti Ahmmed', 'bahm@cms.com', 12387654, 2147483647, 6788798, 1, NULL, '2020-08-08 07:35:11', NULL, '2020-08-08 07:35:11'),
(2, 1, '1', 'jgjg@HKHK.COM', 1, 1, 1, 1, NULL, '2020-08-08 07:36:15', NULL, '2020-08-08 07:36:15');

-- --------------------------------------------------------

--
-- Table structure for table `sponser_credit_note`
--

CREATE TABLE `sponser_credit_note` (
  `id` int(20) NOT NULL,
  `credit_note_number` varchar(50) DEFAULT '',
  `total_amount` varchar(20) DEFAULT '',
  `remarks` varchar(520) DEFAULT '',
  `approval_status` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sponser_credit_note_details`
--

CREATE TABLE `sponser_credit_note_details` (
  `id` int(20) NOT NULL,
  `id_sponser_credit_note` int(10) DEFAULT NULL,
  `id_sponser_main_invoice` int(20) DEFAULT 0,
  `invoice_amount` varchar(20) DEFAULT '',
  `credit_note_amount` varchar(20) DEFAULT '',
  `remarks` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sponser_fee_info_details`
--

CREATE TABLE `sponser_fee_info_details` (
  `id` int(20) NOT NULL,
  `id_sponser` int(20) DEFAULT 0,
  `id_fee_item` int(20) DEFAULT 0,
  `id_frequency_mode` int(20) DEFAULT 0,
  `calculation_mode` varchar(200) DEFAULT '',
  `repeat` int(20) DEFAULT 0,
  `max_repeat` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0.00,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sponser_fee_info_details`
--

INSERT INTO `sponser_fee_info_details` (`id`, `id_sponser`, `id_fee_item`, `id_frequency_mode`, `calculation_mode`, `repeat`, `max_repeat`, `amount`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, '2', 0, 1, 140.00, NULL, NULL, '2020-08-08 07:33:54', NULL, '2020-08-08 07:33:54');

-- --------------------------------------------------------

--
-- Table structure for table `sponser_has_students`
--

CREATE TABLE `sponser_has_students` (
  `id` int(20) NOT NULL,
  `id_sponser` int(10) DEFAULT NULL,
  `id_student` int(10) DEFAULT NULL,
  `amount` varchar(20) DEFAULT '',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `aggrement_no` varchar(200) DEFAULT '',
  `id_fee_item` int(20) DEFAULT 0,
  `calculation_mode` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sponser_has_students`
--

INSERT INTO `sponser_has_students` (`id`, `id_sponser`, `id_student`, `amount`, `start_date`, `end_date`, `aggrement_no`, `id_fee_item`, `calculation_mode`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(3, 1, 5, '100', '2020-08-01 00:00:00', '2020-08-31 00:00:00', '1234', 1, 'Amount', 1, 1, '2020-08-08 11:29:03', NULL, '2020-08-08 11:29:03'),
(4, 1, 6, '100', '2020-08-01 00:00:00', '2020-08-31 00:00:00', 'Agg123', 1, 'Amount', 1, 1, '2020-08-10 13:17:23', NULL, '2020-08-10 13:17:23');

-- --------------------------------------------------------

--
-- Table structure for table `sponser_main_invoice`
--

CREATE TABLE `sponser_main_invoice` (
  `id` int(20) NOT NULL,
  `invoice_number` varchar(50) DEFAULT '',
  `type_of_invoice` varchar(50) DEFAULT '',
  `date_time` datetime DEFAULT current_timestamp(),
  `remarks` varchar(520) DEFAULT '',
  `id_application` int(10) DEFAULT NULL,
  `id_sponser` int(10) DEFAULT 0,
  `total_amount` varchar(20) DEFAULT '',
  `balance_amount` varchar(20) DEFAULT '0',
  `paid_amount` varchar(20) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sponser_main_invoice_details`
--

CREATE TABLE `sponser_main_invoice_details` (
  `id` int(20) NOT NULL,
  `id_main_invoice` int(10) DEFAULT NULL,
  `id_fee_item` int(10) DEFAULT NULL,
  `amount` varchar(20) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sponser_receipt`
--

CREATE TABLE `sponser_receipt` (
  `id` int(20) NOT NULL,
  `receipt_number` varchar(50) DEFAULT '',
  `receipt_amount` varchar(20) DEFAULT '',
  `remarks` varchar(520) DEFAULT '',
  `approval_status` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sponser_receipt_details`
--

CREATE TABLE `sponser_receipt_details` (
  `id` int(20) NOT NULL,
  `id_sponser_receipt` int(10) DEFAULT NULL,
  `id_sponser_main_invoice` int(10) DEFAULT NULL,
  `invoice_amount` varchar(20) DEFAULT '',
  `paid_amount` varchar(20) DEFAULT '',
  `approval_status` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sponser_receipt_paid_details`
--

CREATE TABLE `sponser_receipt_paid_details` (
  `id` int(20) NOT NULL,
  `id_sponser_receipt` int(10) DEFAULT NULL,
  `id_payment_type` int(10) DEFAULT NULL,
  `paid_amount` varchar(20) DEFAULT '',
  `approval_status` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(20) NOT NULL,
  `salutation` varchar(20) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `first_name` varchar(2048) DEFAULT '',
  `last_name` varchar(2048) DEFAULT '',
  `ic_no` varchar(50) DEFAULT '',
  `dob` date DEFAULT NULL,
  `mobile_number` varchar(20) DEFAULT '',
  `phone_number` varchar(20) DEFAULT '',
  `id_country` int(20) DEFAULT NULL,
  `id_state` int(20) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `gender` varchar(20) DEFAULT '',
  `staff_id` varchar(50) DEFAULT '',
  `email` varchar(180) DEFAULT '',
  `address` varchar(520) DEFAULT '',
  `address_two` varchar(520) DEFAULT '',
  `job_type` varchar(20) DEFAULT '',
  `academic_type` varchar(20) DEFAULT '',
  `id_department` int(10) DEFAULT NULL,
  `id_faculty_program` int(20) DEFAULT 0 COMMENT 'as similar to department',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `salutation`, `name`, `first_name`, `last_name`, `ic_no`, `dob`, `mobile_number`, `phone_number`, `id_country`, `id_state`, `zipcode`, `gender`, `staff_id`, `email`, `address`, `address_two`, `job_type`, `academic_type`, `id_department`, `id_faculty_program`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, '1', 'AG. Muhammad Hakim', 'Muhammad', 'Hakim', '7612948373', '2020-07-11', '93873737373', '637363636', 1, 1, 573663, 'Male', '1234', 'mhd@g.co.my', 'Address one ', 'Address two', '1', '1', 1, 1, 1, NULL, '2020-07-10 23:42:07', NULL, '2020-07-10 23:42:07'),
(2, '1', 'AG. Asadullah Bag', 'Asadullah', 'Bag', 'IC88909', '1980-08-01', '89786756', '123', 1, 1, 23432, 'Male', 'F22312', 'asad@cms.com', 'KL', 'KL', '1', '1', 1, 1, 1, NULL, '2020-08-06 09:26:31', NULL, '2020-08-06 09:26:31');

-- --------------------------------------------------------

--
-- Table structure for table `staff_has_course`
--

CREATE TABLE `staff_has_course` (
  `id` int(20) NOT NULL,
  `id_staff` int(10) DEFAULT NULL,
  `id_course` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_has_course`
--

INSERT INTO `staff_has_course` (`id`, `id_staff`, `id_course`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 1, 1, NULL, NULL, '2020-07-10 23:42:07', NULL, '2020-07-10 23:42:07'),
(2, 1, 3, NULL, NULL, '2020-07-10 23:42:07', NULL, '2020-07-10 23:42:07'),
(3, 1, 0, NULL, NULL, '2020-07-13 09:04:43', NULL, '2020-07-13 09:04:43'),
(4, 1, 0, NULL, NULL, '2020-07-13 09:06:03', NULL, '2020-07-13 09:06:03'),
(5, 1, 0, NULL, NULL, '2020-07-13 09:06:51', NULL, '2020-07-13 09:06:51'),
(6, 1, 0, NULL, NULL, '2020-07-13 09:07:01', NULL, '2020-07-13 09:07:01'),
(7, 1, 0, NULL, NULL, '2020-07-13 10:02:31', NULL, '2020-07-13 10:02:31'),
(8, 1, 0, NULL, NULL, '2020-07-14 22:37:29', NULL, '2020-07-14 22:37:29'),
(9, 2, 0, NULL, NULL, '2020-08-09 19:43:19', NULL, '2020-08-09 19:43:19'),
(10, 2, 0, NULL, NULL, '2020-08-09 23:46:46', NULL, '2020-08-09 23:46:46'),
(11, 2, 0, NULL, NULL, '2020-08-10 00:25:09', NULL, '2020-08-10 00:25:09'),
(12, 1, 0, NULL, NULL, '2020-08-10 00:25:51', NULL, '2020-08-10 00:25:51'),
(13, 2, 0, NULL, NULL, '2020-08-10 01:42:09', NULL, '2020-08-10 01:42:09'),
(14, 2, 0, NULL, NULL, '2020-08-10 06:08:49', NULL, '2020-08-10 06:08:49'),
(15, 2, 0, NULL, NULL, '2020-08-10 06:09:25', NULL, '2020-08-10 06:09:25');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `id_country` int(10) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `name`, `id_country`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Kedah', 1, 1, NULL, NULL, NULL, '2020-07-10 23:22:53'),
(2, 'Perlis', 1, 1, NULL, NULL, NULL, '2020-07-10 23:23:18'),
(3, 'Adelaide', 2, 1, NULL, NULL, NULL, '2020-07-10 23:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(20) NOT NULL,
  `id_intake` int(11) DEFAULT NULL,
  `id_program` int(11) DEFAULT NULL,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `passport` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `contact_email` varchar(500) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `id_type` varchar(50) DEFAULT '',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `is_hostel` int(20) DEFAULT 0,
  `id_degree_type` int(20) DEFAULT 0,
  `id_race` varchar(120) DEFAULT '',
  `id_advisor` int(20) DEFAULT 0,
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `program_scheme` varchar(200) DEFAULT '',
  `id_program_scheme` int(20) DEFAULT 0,
  `id_program_requirement` int(20) DEFAULT 0,
  `id_branch` int(20) DEFAULT 0,
  `mailing_zipcode` varchar(120) DEFAULT '',
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `sibbling_discount` varchar(50) DEFAULT '',
  `employee_discount` varchar(50) DEFAULT '',
  `alumni_discount` varchar(50) DEFAULT '',
  `applicant_status` varchar(50) DEFAULT 'Draft',
  `passport_number` varchar(50) DEFAULT '',
  `id_applicant` int(20) DEFAULT 0,
  `pathway` varchar(200) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `approved_by` int(20) DEFAULT 0,
  `approved_dt_tm` datetime DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_emergency_contact_details`
--

CREATE TABLE `student_emergency_contact_details` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT 0,
  `relationship` varchar(10240) DEFAULT '',
  `relative_name` varchar(10240) DEFAULT '',
  `relative_address` varchar(10240) DEFAULT '',
  `relative_mobile` varchar(20) DEFAULT '',
  `relative_home_phone` varchar(20) DEFAULT '',
  `relative_office_phone` varchar(20) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_insurance_details`
--

CREATE TABLE `student_insurance_details` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT 0,
  `insurance_id_visa` int(20) DEFAULT 0,
  `insurance_insurance_type` varchar(200) DEFAULT '',
  `insurance_reference_no` varchar(200) DEFAULT '',
  `insurance_date_issue` datetime DEFAULT NULL,
  `insurance_expiry_date` datetime DEFAULT NULL,
  `insurance_reminder_type` varchar(200) DEFAULT '',
  `insurance_reminder_months_expiry` int(20) DEFAULT 0,
  `insurance_reminder_template` varchar(200) DEFAULT '0',
  `insurance_reminder_remarks` varchar(2048) DEFAULT '0',
  `insurance_cover_letter` varchar(200) DEFAULT '0',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_last_login`
--

CREATE TABLE `student_last_login` (
  `id` bigint(20) NOT NULL,
  `id_student` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_last_login`
--

INSERT INTO `student_last_login` (`id`, `id_student`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(1, 3, '{\"student_name\":\"Mr. Mohd Khairul\",\"email_id\":\"askiran123+3@gmail.com\",\"nric\":\"23424242324\",\"id_intake\":\"1\",\"id_program\":\"1\",\"id_qualification\":\"1\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-07-11 12:56:50'),
(2, 4, '{\"student_name\":\"Mr. Bhasha Zahir\",\"email_id\":\"bz@cms.com\",\"nric\":\"NR33445\",\"id_intake\":\"1\",\"id_program\":\"1\",\"id_qualification\":\"1\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-07-11 13:15:46'),
(3, 4, '{\"student_name\":\"Mr. Bhasha Zahir\",\"email_id\":\"bz@cms.com\",\"nric\":\"NR33445\",\"id_intake\":\"1\",\"id_program\":\"1\",\"id_qualification\":\"1\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-07-19 17:29:29'),
(4, 6, '{\"student_name\":\"Mr. Nabasha Ali\",\"email_id\":\"nab@cms.com\",\"nric\":\"5612345\",\"id_intake\":\"1\",\"id_program\":\"1\",\"id_qualification\":\"1\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-10 17:49:31'),
(5, 6, '{\"student_name\":\"Mr. Nabasha Ali\",\"email_id\":\"nab@cms.com\",\"nric\":\"5612345\",\"id_intake\":\"1\",\"id_program\":\"1\",\"id_qualification\":\"1\"}', '211.25.82.226', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '2020-08-10 20:03:00');

-- --------------------------------------------------------

--
-- Table structure for table `student_marks_entry`
--

CREATE TABLE `student_marks_entry` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_course_registered_landscape` int(20) DEFAULT 0,
  `id_course_registration` int(20) DEFAULT 0,
  `id_mark_distribution` int(20) DEFAULT 0,
  `total_course_marks` int(20) DEFAULT 0,
  `total_obtained_marks` int(20) DEFAULT 0,
  `total_min_pass_marks` int(20) DEFAULT 0,
  `result` varchar(500) DEFAULT '',
  `grade` varchar(512) DEFAULT '',
  `is_remarking` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_marks_entry`
--

INSERT INTO `student_marks_entry` (`id`, `id_student`, `id_program`, `id_intake`, `id_course_registered_landscape`, `id_course_registration`, `id_mark_distribution`, `total_course_marks`, `total_obtained_marks`, `total_min_pass_marks`, `result`, `grade`, `is_remarking`, `status`, `reason`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(3, 5, 1, 1, 6, 11, 3, 200, 148, 80, 'Pass', '', 3, 0, '', 1, '2020-08-09 05:31:10', NULL, '2020-08-09 05:31:10'),
(4, 3, 1, 1, 10, 19, 4, 100, 98, 40, 'Pass', 'A+', 0, 0, '', 1, '2020-08-13 10:25:59', NULL, '2020-08-13 10:25:59'),
(5, 6, 1, 1, 10, 20, 4, 100, 90, 40, 'Pass', 'A', 0, 0, '', 1, '2020-08-13 10:40:39', NULL, '2020-08-13 10:40:39'),
(6, 6, 1, 1, 11, 21, 6, 300, 220, 120, 'Pass', 'B', 2, 1, '', 1, '2020-08-13 10:44:12', 1, '2020-08-13 10:44:12');

-- --------------------------------------------------------

--
-- Table structure for table `student_marks_entry_details`
--

CREATE TABLE `student_marks_entry_details` (
  `id` int(20) NOT NULL,
  `id_student_marks_entry` int(20) DEFAULT 0,
  `id_component` int(20) DEFAULT 0,
  `id_marks_distribution_details` int(20) DEFAULT 0,
  `max_marks` int(20) DEFAULT 0,
  `obtained_marks` int(20) DEFAULT 0,
  `pass_marks` int(20) DEFAULT 0,
  `result` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_marks_entry_history`
--

CREATE TABLE `student_marks_entry_history` (
  `id` int(20) NOT NULL,
  `total_course_marks` int(20) DEFAULT 0,
  `total_obtained_marks` int(20) DEFAULT 0,
  `total_min_pass_marks` int(20) DEFAULT 0,
  `id_course_registration` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `id_student_marks_entry` int(20) DEFAULT 0,
  `id_student_remarks_entry` int(20) DEFAULT 0,
  `total_result` varchar(500) DEFAULT '',
  `grade` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_note`
--

CREATE TABLE `student_note` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT 0,
  `note` varchar(10240) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_note`
--

INSERT INTO `student_note` (`id`, `id_student`, `note`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 6, 'Fee Paymet Alert Need To Add', NULL, 1, '2020-08-10 13:49:18', NULL, '2020-08-10 13:49:18'),
(3, 4, 'Fee Paymet Alert Need To Add', NULL, 1, '2020-08-10 13:49:18', NULL, '2020-08-10 13:49:18'),
(4, 7, 'Payment Added & Migration As Student Done', NULL, 1, '2020-08-10 14:28:03', NULL, '2020-08-10 14:28:03');

-- --------------------------------------------------------

--
-- Table structure for table `student_passport_details`
--

CREATE TABLE `student_passport_details` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT 0,
  `passport_number` varchar(200) DEFAULT '',
  `passport_name` varchar(2048) DEFAULT '',
  `passport_country_of_issue` int(20) DEFAULT 0,
  `passport_date_issue` datetime DEFAULT NULL,
  `passport_expiry_date` datetime DEFAULT NULL,
  `passport_reminder_type` varchar(200) DEFAULT '',
  `passport_reminder_months_expiry` int(20) DEFAULT 0,
  `passport_reminder_template` varchar(200) DEFAULT '0',
  `passport_cover_letter` varchar(200) DEFAULT '',
  `passport_reminder_remarks` varchar(2048) DEFAULT '0',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_placement`
--

CREATE TABLE `student_placement` (
  `id` int(20) NOT NULL,
  `id_student` bigint(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `id_qualification` int(20) DEFAULT 0,
  `company_name` varchar(1024) DEFAULT '',
  `company_address` varchar(1024) DEFAULT '',
  `phone` bigint(20) DEFAULT 0,
  `email` varchar(1024) DEFAULT '',
  `id_country` bigint(20) DEFAULT 0,
  `id_state` bigint(20) DEFAULT 0,
  `city` varchar(1024) DEFAULT '',
  `zipcode` int(20) DEFAULT NULL,
  `designation` varchar(500) DEFAULT '',
  `joining_dt` datetime DEFAULT current_timestamp(),
  `reason` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_remarks_entry`
--

CREATE TABLE `student_remarks_entry` (
  `id` int(20) NOT NULL,
  `id_student_marks_entry` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `id_intake` int(20) DEFAULT 0,
  `id_course_registered_landscape` int(20) DEFAULT 0,
  `id_course_registration` int(20) DEFAULT 0,
  `id_mark_distribution` int(20) DEFAULT 0,
  `total_course_marks` int(20) DEFAULT 0,
  `total_obtained_marks` int(20) DEFAULT 0,
  `total_min_pass_marks` int(20) DEFAULT 0,
  `result` varchar(500) DEFAULT '',
  `grade` varchar(500) DEFAULT '',
  `announced` int(2) DEFAULT 0,
  `updated` int(2) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_remarks_entry_details`
--

CREATE TABLE `student_remarks_entry_details` (
  `id` int(20) NOT NULL,
  `id_student_remarks_entry` int(20) DEFAULT 0,
  `id_component` int(20) DEFAULT 0,
  `id_marks_distribution_details` int(20) DEFAULT 0,
  `max_marks` int(20) DEFAULT 0,
  `obtained_marks` int(20) DEFAULT 0,
  `pass_marks` int(20) DEFAULT 0,
  `result` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_remarks_entry_details`
--

INSERT INTO `student_remarks_entry_details` (`id`, `id_student_remarks_entry`, `id_component`, `id_marks_distribution_details`, `max_marks`, `obtained_marks`, `pass_marks`, `result`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 2, 1, 12, 100, 80, 40, 'Pass', 1, 1, '2020-08-14 14:59:41', NULL, '2020-08-14 14:59:41'),
(2, 2, 2, 13, 100, 80, 40, 'Pass', 1, 1, '2020-08-14 14:59:41', NULL, '2020-08-14 14:59:41'),
(3, 2, 3, 14, 100, 88, 40, 'Pass', 1, 1, '2020-08-14 14:59:41', NULL, '2020-08-14 14:59:41'),
(4, 3, 1, 12, 100, 120, 40, 'Pass', 1, 1, '2020-08-16 23:33:21', NULL, '2020-08-16 23:33:21'),
(5, 3, 2, 13, 100, 2, 40, 'Fail', 1, 1, '2020-08-16 23:33:21', NULL, '2020-08-16 23:33:21'),
(6, 3, 3, 14, 100, 33, 40, 'Fail', 1, 1, '2020-08-16 23:33:21', NULL, '2020-08-16 23:33:21');

-- --------------------------------------------------------

--
-- Table structure for table `student_semester_course_details`
--

CREATE TABLE `student_semester_course_details` (
  `id` int(20) NOT NULL,
  `id_student_result` int(20) DEFAULT NULL,
  `id_student_semester_result` int(20) DEFAULT 0,
  `id_exam_registration` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT NULL,
  `grade` varchar(100) DEFAULT '',
  `marks` int(20) DEFAULT NULL,
  `result` varchar(100) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_semester_course_details`
--

INSERT INTO `student_semester_course_details` (`id`, `id_student_result`, `id_student_semester_result`, `id_exam_registration`, `id_course`, `grade`, `marks`, `result`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, NULL, 1, 1, 3, '1', 82, 'Pass', 1, 1, '2020-07-11 11:27:03', NULL, '2020-07-11 11:27:03'),
(2, NULL, 1, 2, 1, '2', 91, '', 1, 1, '2020-07-11 11:27:03', NULL, '2020-07-11 11:27:03');

-- --------------------------------------------------------

--
-- Table structure for table `student_semester_result`
--

CREATE TABLE `student_semester_result` (
  `id` int(20) NOT NULL,
  `id_semester` int(20) DEFAULT NULL,
  `id_student` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `id_intake` int(20) DEFAULT NULL,
  `main_grade` varchar(100) DEFAULT '',
  `main_marks` int(20) DEFAULT NULL,
  `main_result` varchar(100) DEFAULT '',
  `result` varchar(50) DEFAULT '0',
  `marks` int(20) DEFAULT 0,
  `grade` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `reason` varchar(500) DEFAULT '',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_semester_result`
--

INSERT INTO `student_semester_result` (`id`, `id_semester`, `id_student`, `id_program`, `id_intake`, `main_grade`, `main_marks`, `main_result`, `result`, `marks`, `grade`, `status`, `reason`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, 1, '', NULL, '', 'Pass', 173, '1', 1, '', 1, '2020-07-11 11:27:03', NULL, '2020-07-11 11:27:03');

-- --------------------------------------------------------

--
-- Table structure for table `student_visa_details`
--

CREATE TABLE `student_visa_details` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT 0,
  `passport_no` varchar(200) DEFAULT '',
  `visa_type` varchar(200) DEFAULT '',
  `special_pass_type` varchar(200) DEFAULT '',
  `special_pass_remarks` varchar(2048) DEFAULT '',
  `visa_number` varchar(200) DEFAULT '',
  `date_issue` datetime DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `special_pass_date_issue` datetime DEFAULT NULL,
  `special_pass_expiry_date` datetime DEFAULT NULL,
  `id_program` int(20) DEFAULT 0,
  `insurance_coverage` varchar(200) DEFAULT '',
  `reminder_type` varchar(200) DEFAULT '',
  `reminder_months_expiry` int(20) DEFAULT 0,
  `reminder_template` varchar(200) DEFAULT '',
  `reminder_remarks` varchar(2048) DEFAULT '',
  `image_visa_1` varchar(200) DEFAULT '',
  `image_visa_2` varchar(200) DEFAULT '',
  `special_pass_number` varchar(200) DEFAULT '',
  `special_pass_image` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subject_details`
--

CREATE TABLE `subject_details` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `code` varchar(20) DEFAULT '',
  `status` int(1) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tax`
--

CREATE TABLE `tax` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `percentage` varchar(20) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_country`
--

CREATE TABLE `tbl_country` (
  `countryId` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_last_login`
--

CREATE TABLE `tbl_last_login` (
  `id` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `sessionData` varchar(2048) NOT NULL,
  `machineIp` varchar(1024) NOT NULL,
  `userAgent` varchar(128) NOT NULL,
  `agentString` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `createdDtm` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_last_login`
--

INSERT INTO `tbl_last_login` (`id`, `userId`, `sessionData`, `machineIp`, `userAgent`, `agentString`, `platform`, `createdDtm`) VALUES
(1, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(2, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(3, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(4, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '117.230.180.74', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(5, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(6, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(7, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.53.220', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(8, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(9, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(10, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.50.106', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(11, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(12, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.51.107', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(13, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(14, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.51.38', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(15, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.51.38', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(16, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.50.252', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(17, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(18, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.50.14', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(19, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.51.142', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(20, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(21, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.50.225', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(22, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(23, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.53.171', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(24, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(25, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.148', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '0000-00-00 00:00:00'),
(26, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.52.12', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(27, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.50.244', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(28, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(29, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(30, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.51.16', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(31, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(32, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '0000-00-00 00:00:00'),
(33, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.53.197', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(34, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.55.57', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(35, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.48.70', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(36, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.44', 'Windows 10', '0000-00-00 00:00:00'),
(37, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '14.98.22.14', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(38, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(39, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(40, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(41, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(42, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(43, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.15.79', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(44, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '117.230.160.152', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(45, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.15.79', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(46, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(47, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '124.123.104.139', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(48, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(49, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.80.205', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(50, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.90.175', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(51, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(52, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(53, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.88.194', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(54, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.52.202', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(55, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(56, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.169.254', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(57, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.51.4', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(58, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '124.123.104.139', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(59, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.170.225', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(60, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(61, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.191.89', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(62, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.49.44', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(63, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(64, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(65, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.133.61', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(66, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.133.61', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(67, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(68, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(69, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(70, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.51.100', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(71, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(72, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.15.202', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(73, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(74, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.133.58', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(75, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.49.45', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(76, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.56.252', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(77, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(78, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(79, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(80, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 'Windows 10', '0000-00-00 00:00:00'),
(81, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(82, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.80.180', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(83, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '106.197.221.48', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(84, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.88.170', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(85, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 79.0.3945.136', 'Mozilla/5.0 (Linux; Android 10; SAMSUNG SM-N960F) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/12.0 Chrome/79.0.3945.136 Mobile Safari/537.36', 'Android', '0000-00-00 00:00:00'),
(86, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.58', 'Windows 10', '0000-00-00 00:00:00'),
(87, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '106.197.199.72', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(88, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '106.197.199.72', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(89, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '117.230.26.220', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(90, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.172.59', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(91, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '42.190.31.233', 'Chrome 84.0.4147.105', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.58', 'Windows 10', '0000-00-00 00:00:00'),
(92, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.172.59', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(93, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.45.95.195', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(94, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '117.230.149.115', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(95, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Safari 604.1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_5 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/84.0.4147.71 Mobile/15E148 Safari/604.1', 'iOS', '0000-00-00 00:00:00'),
(96, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Safari 604.1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_5 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/84.0.4147.71 Mobile/15E148 Safari/604.1', 'iOS', '0000-00-00 00:00:00'),
(97, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.80.70', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(98, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(99, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(100, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(101, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(102, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '112.79.53.166', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(103, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(104, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(105, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.64.114', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(106, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '124.123.106.237', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(107, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '124.123.106.237', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(108, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.75.38', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(109, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '124.123.106.237', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(110, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.67.94', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(111, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(112, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(113, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.67.94', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(114, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(115, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(116, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(117, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.74.131', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(118, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(119, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(120, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.65.8', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(121, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(122, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '157.49.72.71', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(123, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(124, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '183.171.71.226', 'Chrome 84.0.4147.125', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(125, 1, '{\"role\":\"1\",\"roleText\":\"System Administrator\",\"name\":\"Administrator\"}', '183.171.71.226', 'Chrome 84.0.4147.125', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permissions`
--

CREATE TABLE `tbl_permissions` (
  `permissionId` bigint(20) NOT NULL,
  `code` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reset_password`
--

CREATE TABLE `tbl_reset_password` (
  `id` bigint(20) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activation_id` varchar(32) NOT NULL,
  `agent` varchar(512) NOT NULL,
  `client_ip` varchar(32) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT 0,
  `createdBy` bigint(20) NOT NULL DEFAULT 1,
  `createdDtm` datetime NOT NULL,
  `updatedBy` bigint(20) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_roles`
--

CREATE TABLE `tbl_roles` (
  `roleId` tinyint(4) NOT NULL COMMENT 'role id',
  `role` varchar(50) NOT NULL COMMENT 'role text'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_roles`
--

INSERT INTO `tbl_roles` (`roleId`, `role`) VALUES
(1, 'System Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role_permissions`
--

CREATE TABLE `tbl_role_permissions` (
  `id` bigint(20) NOT NULL,
  `roleId` bigint(20) DEFAULT NULL,
  `permissionId` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_state`
--

CREATE TABLE `tbl_state` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `id_country` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `userId` int(11) NOT NULL,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `roleId` tinyint(4) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT 0,
  `createdBy` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`userId`, `email`, `password`, `name`, `mobile`, `roleId`, `isDeleted`, `createdBy`, `createdDtm`, `updatedBy`, `updatedDtm`) VALUES
(1, 'admin@gmail.com', '$2y$10$YykrXgnhZ563BCiC6L2ee.Kktd/Vj/KncTnTeDZQXCWwAQ1xH4Req', 'Administrator', '9890098901', 1, 0, 0, '2015-07-01 18:56:49', 1, '2020-01-30 01:48:49');

-- --------------------------------------------------------

--
-- Table structure for table `temp_asset_disposal_detail`
--

CREATE TABLE `temp_asset_disposal_detail` (
  `id` int(20) NOT NULL,
  `id_session` varchar(100) DEFAULT '',
  `id_asset` int(20) DEFAULT NULL,
  `asset_code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_bank_details`
--

CREATE TABLE `temp_bank_details` (
  `id` int(20) NOT NULL,
  `id_bank` varchar(120) DEFAULT '',
  `id_session` varchar(120) DEFAULT '',
  `address` varchar(520) DEFAULT '',
  `country` varchar(120) DEFAULT '',
  `state` varchar(120) DEFAULT '',
  `city` varchar(120) DEFAULT '',
  `zipcode` varchar(120) DEFAULT '',
  `branch_no` varchar(120) DEFAULT '',
  `acc_no` varchar(120) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_billing_details`
--

CREATE TABLE `temp_billing_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(120) DEFAULT '',
  `name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `email` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_credit_note_details`
--

CREATE TABLE `temp_credit_note_details` (
  `id` int(20) NOT NULL,
  `id_main_invoice` int(20) NOT NULL,
  `invoice_amount` varchar(20) DEFAULT '',
  `credit_note_amount` varchar(20) DEFAULT '',
  `id_session` varchar(120) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_credit_transfer_details`
--

CREATE TABLE `temp_credit_transfer_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(500) DEFAULT '',
  `id_institution` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_grade` int(20) DEFAULT 0,
  `institution_type` varchar(200) DEFAULT '',
  `subject_course` varchar(2048) DEFAULT '',
  `id_equivalent_course` int(20) DEFAULT 0,
  `credit_hours` int(20) DEFAULT 0,
  `remarks` varchar(2048) DEFAULT '',
  `processing_fee` int(20) DEFAULT 0,
  `document_upload` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_documents_program_details`
--

CREATE TABLE `temp_documents_program_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(520) DEFAULT '',
  `id_program` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_entry_requirement_other_requirements`
--

CREATE TABLE `temp_entry_requirement_other_requirements` (
  `id` int(20) NOT NULL,
  `id_session` varchar(520) DEFAULT '',
  `id_group` int(20) DEFAULT 0,
  `condition` varchar(2048) DEFAULT '',
  `type` varchar(2048) DEFAULT '',
  `result_value` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_entry_requirement_requirements`
--

CREATE TABLE `temp_entry_requirement_requirements` (
  `id` int(20) NOT NULL,
  `id_session` varchar(520) DEFAULT '',
  `id_group` int(20) DEFAULT 0,
  `id_qualification` int(20) DEFAULT 0,
  `id_specialization` int(20) DEFAULT 0,
  `validity` int(20) DEFAULT 0,
  `result_item` varchar(2048) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `condition` varchar(2048) DEFAULT '',
  `result_value` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_entry_requirement_requirements`
--

INSERT INTO `temp_entry_requirement_requirements` (`id`, `id_session`, `id_group`, `id_qualification`, `id_specialization`, `validity`, `result_item`, `description`, `condition`, `result_value`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 'de7c41f8b3b85cdc623ca18ec0268d8e', 1, 1, 1, 1, 'CGPA', '', 'Greater Than Or Equal To', '2.33', NULL, NULL, '2020-07-19 02:54:49', NULL, '2020-07-19 02:54:49');

-- --------------------------------------------------------

--
-- Table structure for table `temp_equivalent_course_details`
--

CREATE TABLE `temp_equivalent_course_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(100) NOT NULL,
  `id_course` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_fee_structure_details`
--

CREATE TABLE `temp_fee_structure_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(100) DEFAULT NULL,
  `id_fee_item` int(20) DEFAULT NULL,
  `id_frequency_mode` int(20) DEFAULT NULL,
  `amount` float(20,2) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_gpa_cgpa_setup_details`
--

CREATE TABLE `temp_gpa_cgpa_setup_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(500) DEFAULT '',
  `min_grade_point` int(20) DEFAULT NULL,
  `max_grade_point` int(20) DEFAULT NULL,
  `description` varchar(5096) DEFAULT '',
  `status_optional_language` varchar(5096) DEFAULT '',
  `probation` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_grade_setup_details`
--

CREATE TABLE `temp_grade_setup_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(200) DEFAULT '',
  `id_grade` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `description` varchar(1024) DEFAULT '',
  `description_optional_language` varchar(1024) DEFAULT '',
  `point` int(20) DEFAULT 0,
  `min_marks` int(20) DEFAULT 0,
  `max_marks` int(20) DEFAULT 0,
  `pass` varchar(20) DEFAULT '',
  `rank` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_intake_has_programme`
--

CREATE TABLE `temp_intake_has_programme` (
  `id` int(20) NOT NULL,
  `id_session` varchar(100) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_internship_company_has_program`
--

CREATE TABLE `temp_internship_company_has_program` (
  `id` int(20) NOT NULL,
  `id_session` varchar(200) DEFAULT '',
  `id_company_type` bigint(20) DEFAULT 0,
  `id_intake` bigint(20) DEFAULT 0,
  `id_program` bigint(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_inventory_allotment`
--

CREATE TABLE `temp_inventory_allotment` (
  `id` int(20) NOT NULL,
  `level` int(20) DEFAULT 0,
  `id_session` varchar(1024) DEFAULT '',
  `id_inventory` int(20) DEFAULT 0,
  `quantity` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_investment_application_details`
--

CREATE TABLE `temp_investment_application_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(280) DEFAULT '',
  `duration` int(20) DEFAULT 0,
  `duration_type` varchar(80) DEFAULT '',
  `total_amount` float(20,2) DEFAULT 0.00,
  `id_institution` int(20) DEFAULT 0,
  `id_investment_type` int(20) DEFAULT 0,
  `profit_rate` int(20) DEFAULT 0,
  `id_bank` int(20) DEFAULT 0,
  `bank_branch` int(20) DEFAULT 0,
  `maturity_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_journal_details`
--

CREATE TABLE `temp_journal_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(50) DEFAULT '',
  `journal_type` varchar(50) DEFAULT '',
  `credit_amount` varchar(50) DEFAULT '',
  `debit_amount` varchar(50) DEFAULT '',
  `id_account_code` int(20) DEFAULT NULL,
  `id_activity_code` int(20) DEFAULT NULL,
  `id_department_code` int(20) DEFAULT NULL,
  `id_fund_code` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_main_invoice_details`
--

CREATE TABLE `temp_main_invoice_details` (
  `id` int(20) NOT NULL,
  `id_fee_item` int(10) DEFAULT NULL,
  `amount` varchar(20) DEFAULT '',
  `id_session` varchar(120) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_mark_distribution_details`
--

CREATE TABLE `temp_mark_distribution_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(500) DEFAULT '',
  `id_component` int(20) DEFAULT NULL,
  `is_pass_compulsary` int(2) DEFAULT NULL,
  `pass_marks` int(20) DEFAULT NULL,
  `max_marks` int(20) DEFAULT NULL,
  `weightage` int(20) DEFAULT NULL,
  `attendance_status` int(2) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_performa_invoice_details`
--

CREATE TABLE `temp_performa_invoice_details` (
  `id` int(20) NOT NULL,
  `id_fee_item` int(20) NOT NULL,
  `amount` varchar(20) DEFAULT '',
  `id_session` varchar(120) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_programme_has_course`
--

CREATE TABLE `temp_programme_has_course` (
  `id` int(20) NOT NULL,
  `id_session` varchar(100) DEFAULT '',
  `id_course` int(10) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_programme_has_dean`
--

CREATE TABLE `temp_programme_has_dean` (
  `id` int(20) NOT NULL,
  `id_session` varchar(100) DEFAULT NULL,
  `id_staff` int(10) DEFAULT NULL,
  `id_programme` int(20) DEFAULT NULL,
  `effective_start_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_programme_has_dean`
--

INSERT INTO `temp_programme_has_dean` (`id`, `id_session`, `id_staff`, `id_programme`, `effective_start_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(2, '2193a8dc78d2e9e091bb6e754a4708a4', 2, NULL, '2020-08-01 00:00:00', NULL, NULL, '2020-08-17 12:30:52', NULL, '2020-08-17 12:30:52'),
(3, '2193a8dc78d2e9e091bb6e754a4708a4', 1, NULL, '2021-08-31 00:00:00', NULL, NULL, '2020-08-17 12:31:02', NULL, '2020-08-17 12:31:02'),
(4, 'd1fb3f65ab1b36f72214785c0d20cb90', 2, NULL, '2020-08-01 00:00:00', NULL, NULL, '2020-08-20 06:18:17', NULL, '2020-08-20 06:18:17');

-- --------------------------------------------------------

--
-- Table structure for table `temp_programme_has_scheme`
--

CREATE TABLE `temp_programme_has_scheme` (
  `id` bigint(20) NOT NULL,
  `id_session` varchar(500) DEFAULT '',
  `id_scheme` int(20) DEFAULT 0,
  `status` int(5) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_program_landscape_semester_details`
--

CREATE TABLE `temp_program_landscape_semester_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(250) DEFAULT '',
  `semester_type` varchar(50) DEFAULT '',
  `registration_rule` varchar(50) DEFAULT '',
  `minimum` int(20) DEFAULT NULL,
  `maximum` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_pr_entry`
--

CREATE TABLE `temp_pr_entry` (
  `id` bigint(20) NOT NULL,
  `sessionid` varchar(100) DEFAULT NULL,
  `cr_account` varchar(20) DEFAULT NULL,
  `cr_activity` varchar(20) DEFAULT NULL,
  `cr_department` varchar(20) DEFAULT NULL,
  `cr_fund` varchar(20) DEFAULT NULL,
  `dt_account` varchar(20) DEFAULT NULL,
  `dt_activity` varchar(20) DEFAULT NULL,
  `dt_department` varchar(20) DEFAULT NULL,
  `dt_fund` varchar(20) DEFAULT NULL,
  `id_category` bigint(20) DEFAULT NULL,
  `id_sub_category` bigint(20) DEFAULT NULL,
  `id_item` bigint(20) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` float(20,2) DEFAULT NULL,
  `id_tax` float(10,2) DEFAULT NULL,
  `tax_price` float(10,2) DEFAULT NULL,
  `total_final` float(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_receipt_details`
--

CREATE TABLE `temp_receipt_details` (
  `id` int(20) NOT NULL,
  `id_payment_mode` varchar(20) DEFAULT '',
  `payment_reference_number` varchar(200) DEFAULT '',
  `payment_mode_amount` varchar(20) DEFAULT '',
  `id_session` varchar(120) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_scheme_has_program`
--

CREATE TABLE `temp_scheme_has_program` (
  `id` int(20) NOT NULL,
  `id_session` varchar(500) DEFAULT '',
  `id_program` bigint(20) DEFAULT 0,
  `id_sub_thrust` int(20) DEFAULT 0,
  `quota` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_scholarship_documents_program_details`
--

CREATE TABLE `temp_scholarship_documents_program_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(520) DEFAULT '',
  `id_program` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_sponser_credit_note_details`
--

CREATE TABLE `temp_sponser_credit_note_details` (
  `id` int(20) NOT NULL,
  `id_sponser_main_invoice` int(20) NOT NULL,
  `invoice_amount` varchar(20) DEFAULT '',
  `credit_note_amount` varchar(20) DEFAULT '',
  `id_session` varchar(120) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_sponser_main_invoice_details`
--

CREATE TABLE `temp_sponser_main_invoice_details` (
  `id` int(20) NOT NULL,
  `id_fee_item` int(10) DEFAULT NULL,
  `amount` varchar(20) DEFAULT '',
  `id_session` varchar(120) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_sponser_receipt_details`
--

CREATE TABLE `temp_sponser_receipt_details` (
  `id` int(20) NOT NULL,
  `id_sponser_main_invoice` int(20) NOT NULL,
  `invoice_amount` varchar(20) DEFAULT '',
  `paid_amount` varchar(20) DEFAULT '',
  `id_session` varchar(120) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_staff_has_course`
--

CREATE TABLE `temp_staff_has_course` (
  `id` int(20) NOT NULL,
  `id_session` varchar(100) DEFAULT NULL,
  `id_course` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_tender_commitee`
--

CREATE TABLE `temp_tender_commitee` (
  `id` bigint(20) NOT NULL,
  `id_session` varchar(100) DEFAULT '',
  `staff_type` varchar(50) DEFAULT '',
  `id_staff` bigint(20) DEFAULT NULL,
  `outside_staff_name` varchar(500) DEFAULT '',
  `comitee_description` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_tender_remarks`
--

CREATE TABLE `temp_tender_remarks` (
  `id` bigint(20) NOT NULL,
  `id_session` varchar(100) DEFAULT '',
  `remarks` varchar(500) DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tender_commitee`
--

CREATE TABLE `tender_commitee` (
  `id` bigint(20) NOT NULL,
  `id_tender` int(20) DEFAULT 0,
  `staff_type` varchar(50) DEFAULT '',
  `id_staff` bigint(20) DEFAULT NULL,
  `outside_staff_name` varchar(500) DEFAULT '',
  `comitee_description` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tender_quotation`
--

CREATE TABLE `tender_quotation` (
  `id` bigint(20) NOT NULL,
  `type` varchar(100) DEFAULT '',
  `id_financial_year` bigint(20) DEFAULT NULL,
  `id_vendor` bigint(20) DEFAULT NULL,
  `id_department` bigint(20) DEFAULT NULL,
  `id_pr` bigint(20) DEFAULT NULL,
  `quotation_number` varchar(50) DEFAULT NULL,
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `opening_date` datetime DEFAULT current_timestamp(),
  `description` varchar(500) DEFAULT NULL,
  `reason` varchar(500) DEFAULT NULL,
  `amount` float(10,2) DEFAULT NULL,
  `split` int(2) DEFAULT 0,
  `is_submitted` int(2) DEFAULT 0,
  `is_shortlisted` int(2) DEFAULT 0,
  `is_awarded` int(2) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tender_quotation_detail`
--

CREATE TABLE `tender_quotation_detail` (
  `id` bigint(20) NOT NULL,
  `id_tender_quotation` bigint(20) DEFAULT NULL,
  `cr_account` varchar(20) DEFAULT '',
  `cr_activity` varchar(20) DEFAULT '',
  `cr_department` varchar(20) DEFAULT '',
  `cr_fund` varchar(20) DEFAULT '',
  `dt_account` varchar(20) DEFAULT '',
  `dt_activity` varchar(20) DEFAULT '',
  `dt_department` varchar(20) DEFAULT '',
  `dt_fund` varchar(20) DEFAULT '',
  `period` int(20) DEFAULT NULL,
  `period_type` varchar(50) DEFAULT '',
  `id_category` bigint(20) DEFAULT NULL,
  `id_sub_category` bigint(20) DEFAULT NULL,
  `id_item` bigint(20) DEFAULT NULL,
  `type` varchar(20) DEFAULT '',
  `visit_location` varchar(200) DEFAULT '',
  `visit_date_time` datetime DEFAULT current_timestamp(),
  `quantity` int(20) DEFAULT NULL,
  `price` float(20,2) DEFAULT NULL,
  `id_tax` float(10,2) DEFAULT NULL,
  `tax_price` float(10,2) DEFAULT NULL,
  `total_final` float(10,2) DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tender_remarks`
--

CREATE TABLE `tender_remarks` (
  `id` bigint(20) NOT NULL,
  `id_tender` int(20) DEFAULT 0,
  `remarks` varchar(500) DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tender_submission_vendor`
--

CREATE TABLE `tender_submission_vendor` (
  `id` bigint(20) NOT NULL,
  `id_tender_quotation` int(20) DEFAULT 0,
  `id_vendor` int(20) DEFAULT 0,
  `description` varchar(500) DEFAULT NULL,
  `is_submitted` int(2) DEFAULT 0,
  `is_shortlisted` int(2) DEFAULT 0,
  `is_awarded` int(2) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `role_id` tinyint(4) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_dt_tm` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `mobile`, `role_id`, `is_deleted`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'admin@gmail.com', '$2y$10$YykrXgnhZ563BCiC6L2ee.Kktd/Vj/KncTnTeDZQXCWwAQ1xH4Req', 'Administrator', '9890098901', 1, 0, 0, '2015-07-01 18:56:49', 1, '2020-01-30 01:48:49');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_details`
--

CREATE TABLE `vendor_details` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `email` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `gender` varchar(20) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `mobile` varchar(20) DEFAULT '',
  `address_one` varchar(520) DEFAULT '',
  `address_two` varchar(520) DEFAULT '',
  `country` varchar(120) DEFAULT '',
  `state` varchar(120) DEFAULT '',
  `zipcode` varchar(120) DEFAULT '',
  `date_of_birth` varchar(20) DEFAULT '',
  `vendor_status` varchar(50) DEFAULT 'Draft',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `visa_details`
--

CREATE TABLE `visa_details` (
  `id` int(20) NOT NULL,
  `id_student` int(10) DEFAULT NULL,
  `malaysian_visa` varchar(120) DEFAULT '',
  `visa_number` varchar(200) DEFAULT '',
  `visa_expiry_date` varchar(120) DEFAULT '',
  `visa_status` varchar(120) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visa_details`
--

INSERT INTO `visa_details` (`id`, `id_student`, `malaysian_visa`, `visa_number`, `visa_expiry_date`, `visa_status`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 1, '', '', '', '', NULL, NULL, '2020-07-11 01:20:44', NULL, '2020-07-11 01:20:44'),
(2, 1, '', '', '', '', NULL, NULL, '2020-07-11 06:25:02', NULL, '2020-07-11 06:25:02'),
(3, 2, '', '', '', '', NULL, NULL, '2020-07-11 11:53:53', NULL, '2020-07-11 11:53:53'),
(4, 3, '', '', '', '', NULL, NULL, '2020-07-11 12:55:29', NULL, '2020-07-11 12:55:29'),
(5, 4, '', '', '', '', NULL, NULL, '2020-07-11 13:15:32', NULL, '2020-07-11 13:15:32'),
(7, 5, 'Yes', '128932726', '2020-08-29', 'Active Visa', NULL, NULL, '2020-08-06 07:56:06', NULL, '2020-08-06 07:56:06'),
(8, 5, '', '', '1970-01-01', 'Active Visa', NULL, NULL, '2020-08-06 07:59:36', NULL, '2020-08-06 07:59:36'),
(13, 6, 'Yes', 'w3432432', '2020-08-31', 'Active Visa', NULL, NULL, '2020-08-12 10:06:41', NULL, '2020-08-12 10:06:41'),
(14, 6, '', '', '1970-01-01', 'Active Visa', NULL, NULL, '2020-08-12 10:07:24', NULL, '2020-08-12 10:07:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academic_year`
--
ALTER TABLE `academic_year`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `account_code`
--
ALTER TABLE `account_code`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `activity_code`
--
ALTER TABLE `activity_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_details`
--
ALTER TABLE `activity_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `add_course_to_program_landscape`
--
ALTER TABLE `add_course_to_program_landscape`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `add_temp_receipt_paid_amount`
--
ALTER TABLE `add_temp_receipt_paid_amount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advisor_tagging`
--
ALTER TABLE `advisor_tagging`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alumni_discount`
--
ALTER TABLE `alumni_discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `amount_calculation_type`
--
ALTER TABLE `amount_calculation_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `apex_status`
--
ALTER TABLE `apex_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applicant`
--
ALTER TABLE `applicant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applicant_has_alumni_discount`
--
ALTER TABLE `applicant_has_alumni_discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applicant_has_document`
--
ALTER TABLE `applicant_has_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applicant_has_employee_discount`
--
ALTER TABLE `applicant_has_employee_discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applicant_has_sibbling_discount`
--
ALTER TABLE `applicant_has_sibbling_discount`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_applicant` (`id_applicant`);

--
-- Indexes for table `applicant_last_login`
--
ALTER TABLE `applicant_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applicant_status`
--
ALTER TABLE `applicant_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apply_change_programme`
--
ALTER TABLE `apply_change_programme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apply_change_scheme`
--
ALTER TABLE `apply_change_scheme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apply_change_status`
--
ALTER TABLE `apply_change_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_category`
--
ALTER TABLE `asset_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_disposal`
--
ALTER TABLE `asset_disposal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_disposal_detail`
--
ALTER TABLE `asset_disposal_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_item`
--
ALTER TABLE `asset_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_order`
--
ALTER TABLE `asset_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_pre_registration`
--
ALTER TABLE `asset_pre_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_registration`
--
ALTER TABLE `asset_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asset_sub_category`
--
ALTER TABLE `asset_sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assign_student_to_exam_center`
--
ALTER TABLE `assign_student_to_exam_center`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendence_setup`
--
ALTER TABLE `attendence_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `award`
--
ALTER TABLE `award`
  ADD PRIMARY KEY (`id`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `award_level`
--
ALTER TABLE `award_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_details`
--
ALTER TABLE `bank_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_registration`
--
ALTER TABLE `bank_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barring`
--
ALTER TABLE `barring`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barring_type`
--
ALTER TABLE `barring_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bulk_withdraw`
--
ALTER TABLE `bulk_withdraw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_type`
--
ALTER TABLE `category_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `change_branch`
--
ALTER TABLE `change_branch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `change_branch_history`
--
ALTER TABLE `change_branch_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `change_status`
--
ALTER TABLE `change_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_details`
--
ALTER TABLE `class_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `communication_group`
--
ALTER TABLE `communication_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_recepients`
--
ALTER TABLE `communication_group_recepients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_template`
--
ALTER TABLE `communication_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_details`
--
ALTER TABLE `company_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_vendor` (`id_vendor`);

--
-- Indexes for table `convocation`
--
ALTER TABLE `convocation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `convocation_details`
--
ALTER TABLE `convocation_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `updated_by` (`updated_by`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `id_staff_coordeinator` (`id_staff_coordinator`),
  ADD KEY `id_department` (`id_department`);

--
-- Indexes for table `courses_from_programme_landscape`
--
ALTER TABLE `courses_from_programme_landscape`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_grade`
--
ALTER TABLE `course_grade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_program` (`id_program`),
  ADD KEY `id_course` (`id_course`),
  ADD KEY `id_grade` (`id_grade`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `course_registration`
--
ALTER TABLE `course_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_requisite`
--
ALTER TABLE `course_requisite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_course` (`id_course`);

--
-- Indexes for table `course_type`
--
ALTER TABLE `course_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_withdraw`
--
ALTER TABLE `course_withdraw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_note`
--
ALTER TABLE `credit_note`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_note_details`
--
ALTER TABLE `credit_note_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_credit_note` (`id_credit_note`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `credit_note_type`
--
ALTER TABLE `credit_note_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_transfer`
--
ALTER TABLE `credit_transfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_transfer_details`
--
ALTER TABLE `credit_transfer_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_rate_setup`
--
ALTER TABLE `currency_rate_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_setup`
--
ALTER TABLE `currency_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `debit_note`
--
ALTER TABLE `debit_note`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `department_has_staff`
--
ALTER TABLE `department_has_staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `disposal_type`
--
ALTER TABLE `disposal_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents_program`
--
ALTER TABLE `documents_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents_program_details`
--
ALTER TABLE `documents_program_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_checklist`
--
ALTER TABLE `document_checklist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_discount`
--
ALTER TABLE `employee_discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employment_status`
--
ALTER TABLE `employment_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `english_proficiency_details`
--
ALTER TABLE `english_proficiency_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entry_requirement_other_requirements`
--
ALTER TABLE `entry_requirement_other_requirements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entry_requirement_requirements`
--
ALTER TABLE `entry_requirement_requirements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equivalent_course`
--
ALTER TABLE `equivalent_course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_course` (`id_course`),
  ADD KEY `id_equuivalent_course` (`id_equivalent_course`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `equivalent_course_details`
--
ALTER TABLE `equivalent_course_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `examination`
--
ALTER TABLE `examination`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `examination_assesment_type`
--
ALTER TABLE `examination_assesment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `examination_components`
--
ALTER TABLE `examination_components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `examination_details`
--
ALTER TABLE `examination_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_student` (`id_student`);

--
-- Indexes for table `exam_attendence`
--
ALTER TABLE `exam_attendence`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_attendence_details`
--
ALTER TABLE `exam_attendence_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_calender`
--
ALTER TABLE `exam_calender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_center`
--
ALTER TABLE `exam_center`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_center_location`
--
ALTER TABLE `exam_center_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_center_to_semester`
--
ALTER TABLE `exam_center_to_semester`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_configuration`
--
ALTER TABLE `exam_configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_event`
--
ALTER TABLE `exam_event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_register`
--
ALTER TABLE `exam_register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_registration`
--
ALTER TABLE `exam_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_program`
--
ALTER TABLE `faculty_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_program_has_staff`
--
ALTER TABLE `faculty_program_has_staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_category`
--
ALTER TABLE `fee_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `fee_setup`
--
ALTER TABLE `fee_setup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_fee_category` (`id_fee_category`),
  ADD KEY `id_amount_calculation_type` (`id_amount_calculation_type`),
  ADD KEY `id_frequency_mode` (`id_frequency_mode`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `fee_structure`
--
ALTER TABLE `fee_structure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_structure_activity`
--
ALTER TABLE `fee_structure_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_structure_details`
--
ALTER TABLE `fee_structure_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `financial_account_code`
--
ALTER TABLE `financial_account_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `financial_year`
--
ALTER TABLE `financial_year`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frequency_mode`
--
ALTER TABLE `frequency_mode`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `fund_code`
--
ALTER TABLE `fund_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gpa_cgpa_setup`
--
ALTER TABLE `gpa_cgpa_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gpa_cgpa_setup_details`
--
ALTER TABLE `gpa_cgpa_setup_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grade`
--
ALTER TABLE `grade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `grade_setup`
--
ALTER TABLE `grade_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grade_setup_details`
--
ALTER TABLE `grade_setup_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `graduation_details`
--
ALTER TABLE `graduation_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `graduation_list`
--
ALTER TABLE `graduation_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grn`
--
ALTER TABLE `grn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_setup`
--
ALTER TABLE `group_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guest`
--
ALTER TABLE `guest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hostel_inventory_registration`
--
ALTER TABLE `hostel_inventory_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hostel_item_registration`
--
ALTER TABLE `hostel_item_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hostel_registration`
--
ALTER TABLE `hostel_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hostel_room`
--
ALTER TABLE `hostel_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hostel_room_type`
--
ALTER TABLE `hostel_room_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `intake`
--
ALTER TABLE `intake`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `intake_has_programme`
--
ALTER TABLE `intake_has_programme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `internship_application`
--
ALTER TABLE `internship_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `internship_company_has_program`
--
ALTER TABLE `internship_company_has_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `internship_company_registration`
--
ALTER TABLE `internship_company_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `internship_company_type`
--
ALTER TABLE `internship_company_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `internship_student_limit`
--
ALTER TABLE `internship_student_limit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_allotment`
--
ALTER TABLE `inventory_allotment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investment_application`
--
ALTER TABLE `investment_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investment_application_details`
--
ALTER TABLE `investment_application_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investment_institution`
--
ALTER TABLE `investment_institution`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investment_registration`
--
ALTER TABLE `investment_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investment_registration_withdraw`
--
ALTER TABLE `investment_registration_withdraw`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investment_type`
--
ALTER TABLE `investment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `is_scholarship_applicant_examination_details`
--
ALTER TABLE `is_scholarship_applicant_examination_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `is_scholarship_applicant_personel_details`
--
ALTER TABLE `is_scholarship_applicant_personel_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `is_scholar_applicant_employment_status`
--
ALTER TABLE `is_scholar_applicant_employment_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `is_scholar_applicant_family_details`
--
ALTER TABLE `is_scholar_applicant_family_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `journal`
--
ALTER TABLE `journal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `journal_details`
--
ALTER TABLE `journal_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `landscape_course_type`
--
ALTER TABLE `landscape_course_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `late_registration`
--
ALTER TABLE `late_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_invoice`
--
ALTER TABLE `main_invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_application` (`id_application`),
  ADD KEY `id_student` (`id_student`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `main_invoice_details`
--
ALTER TABLE `main_invoice_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_main_invoice` (`id_main_invoice`),
  ADD KEY `id_fee_item` (`id_fee_item`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `main_invoice_discount_details`
--
ALTER TABLE `main_invoice_discount_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marital_status_setup`
--
ALTER TABLE `marital_status_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mark_distribution`
--
ALTER TABLE `mark_distribution`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mark_distribution_details`
--
ALTER TABLE `mark_distribution_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mode_of_category`
--
ALTER TABLE `mode_of_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mode_of_program`
--
ALTER TABLE `mode_of_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nationality`
--
ALTER TABLE `nationality`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nonpo_detail`
--
ALTER TABLE `nonpo_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nonpo_entry`
--
ALTER TABLE `nonpo_entry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organisation`
--
ALTER TABLE `organisation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organisation_comitee`
--
ALTER TABLE `organisation_comitee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organisation_has_training_center`
--
ALTER TABLE `organisation_has_training_center`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organisation_training_center_has_program`
--
ALTER TABLE `organisation_training_center_has_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `other_documents`
--
ALTER TABLE `other_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partner_category`
--
ALTER TABLE `partner_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partner_university`
--
ALTER TABLE `partner_university`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partner_university_comitee`
--
ALTER TABLE `partner_university_comitee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_type`
--
ALTER TABLE `payment_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `performa_invoice`
--
ALTER TABLE `performa_invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_application` (`id_application`),
  ADD KEY `id_student` (`id_student`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `performa_invoice_details`
--
ALTER TABLE `performa_invoice_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_performa` (`id_performa`),
  ADD KEY `id_fee_item` (`id_fee_item`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `personal_billing_details`
--
ALTER TABLE `personal_billing_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_vendor` (`id_vendor`);

--
-- Indexes for table `po_detail`
--
ALTER TABLE `po_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `po_entry`
--
ALTER TABLE `po_entry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procurement_category`
--
ALTER TABLE `procurement_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procurement_item`
--
ALTER TABLE `procurement_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procurement_limit`
--
ALTER TABLE `procurement_limit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procurement_sub_category`
--
ALTER TABLE `procurement_sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_details`
--
ALTER TABLE `profile_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `programme`
--
ALTER TABLE `programme`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `programme_has_course`
--
ALTER TABLE `programme_has_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `programme_has_dean`
--
ALTER TABLE `programme_has_dean`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_programme` (`id_programme`),
  ADD KEY `id_staff` (`id_staff`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `programme_has_fee_structure`
--
ALTER TABLE `programme_has_fee_structure`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_intake` (`id_intake`),
  ADD KEY `id_semester` (`id_semester`);

--
-- Indexes for table `programme_has_scheme`
--
ALTER TABLE `programme_has_scheme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `programme_landscape`
--
ALTER TABLE `programme_landscape`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_programme` (`id_programme`),
  ADD KEY `id_intake` (`id_intake`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `program_entry_requirement`
--
ALTER TABLE `program_entry_requirement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_grade`
--
ALTER TABLE `program_grade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_program` (`id_program`),
  ADD KEY `id_semester` (`id_semester`),
  ADD KEY `id_grade` (`id_grade`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `program_has_acceredation_details`
--
ALTER TABLE `program_has_acceredation_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_has_concurrent_program`
--
ALTER TABLE `program_has_concurrent_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_has_major_details`
--
ALTER TABLE `program_has_major_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_has_minor_details`
--
ALTER TABLE `program_has_minor_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_landscape_requirement_details`
--
ALTER TABLE `program_landscape_requirement_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_landscape_requisite_details`
--
ALTER TABLE `program_landscape_requisite_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_landscape_semester_details`
--
ALTER TABLE `program_landscape_semester_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_type`
--
ALTER TABLE `program_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_report_submission`
--
ALTER TABLE `project_report_submission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pr_entry`
--
ALTER TABLE `pr_entry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pr_entry_details`
--
ALTER TABLE `pr_entry_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publish_assesment_result_date`
--
ALTER TABLE `publish_assesment_result_date`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publish_exam`
--
ALTER TABLE `publish_exam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_intake` (`id_intake`),
  ADD KEY `id_semester` (`id_semester`),
  ADD KEY `id_program` (`id_program`);

--
-- Indexes for table `publish_exam_result_date`
--
ALTER TABLE `publish_exam_result_date`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qualification_setup`
--
ALTER TABLE `qualification_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_pool`
--
ALTER TABLE `question_pool`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `race_setup`
--
ALTER TABLE `race_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt`
--
ALTER TABLE `receipt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `receipt_details`
--
ALTER TABLE `receipt_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `id_receipt` (`id_receipt`),
  ADD KEY `id_main_invoice` (`id_main_invoice`);

--
-- Indexes for table `receipt_paid_details`
--
ALTER TABLE `receipt_paid_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_receipt` (`id_receipt`),
  ADD KEY `id_payment_type` (`id_payment_type`);

--
-- Indexes for table `release`
--
ALTER TABLE `release`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `religion_setup`
--
ALTER TABLE `religion_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room_allotment`
--
ALTER TABLE `room_allotment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salutation_setup`
--
ALTER TABLE `salutation_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scheme`
--
ALTER TABLE `scheme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scheme_has_program`
--
ALTER TABLE `scheme_has_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar`
--
ALTER TABLE `scholar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_accreditation_category`
--
ALTER TABLE `scholarship_accreditation_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_accreditation_type`
--
ALTER TABLE `scholarship_accreditation_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_amount_calculation_type`
--
ALTER TABLE `scholarship_amount_calculation_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_applicant`
--
ALTER TABLE `scholarship_applicant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_application`
--
ALTER TABLE `scholarship_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_assesment_method_setup`
--
ALTER TABLE `scholarship_assesment_method_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_award_setup`
--
ALTER TABLE `scholarship_award_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_bank_registration`
--
ALTER TABLE `scholarship_bank_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_country`
--
ALTER TABLE `scholarship_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_department`
--
ALTER TABLE `scholarship_department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_department_has_staff`
--
ALTER TABLE `scholarship_department_has_staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_documents`
--
ALTER TABLE `scholarship_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_documents_for_applicant`
--
ALTER TABLE `scholarship_documents_for_applicant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_documents_program`
--
ALTER TABLE `scholarship_documents_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_documents_program_details`
--
ALTER TABLE `scholarship_documents_program_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_education_level`
--
ALTER TABLE `scholarship_education_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_examination_details`
--
ALTER TABLE `scholarship_examination_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_individual_entry_requirement`
--
ALTER TABLE `scholarship_individual_entry_requirement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_module_type_setup`
--
ALTER TABLE `scholarship_module_type_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_organisation_has_department`
--
ALTER TABLE `scholarship_organisation_has_department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_organisation_setup`
--
ALTER TABLE `scholarship_organisation_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_organisation_setup_comitee`
--
ALTER TABLE `scholarship_organisation_setup_comitee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_partner_university`
--
ALTER TABLE `scholarship_partner_university`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_partner_university_has_aggrement`
--
ALTER TABLE `scholarship_partner_university_has_aggrement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_partner_university_has_program`
--
ALTER TABLE `scholarship_partner_university_has_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_partner_university_has_training_center`
--
ALTER TABLE `scholarship_partner_university_has_training_center`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_partner_university_program_has_apprenticeship`
--
ALTER TABLE `scholarship_partner_university_program_has_apprenticeship`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_partner_university_program_has_internship`
--
ALTER TABLE `scholarship_partner_university_program_has_internship`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_partner_university_program_has_study_mode`
--
ALTER TABLE `scholarship_partner_university_program_has_study_mode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_partner_university_program_has_syllabus`
--
ALTER TABLE `scholarship_partner_university_program_has_syllabus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_payment_type`
--
ALTER TABLE `scholarship_payment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_programme`
--
ALTER TABLE `scholarship_programme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_program_accreditation`
--
ALTER TABLE `scholarship_program_accreditation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_program_partner`
--
ALTER TABLE `scholarship_program_partner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_program_syllabus`
--
ALTER TABLE `scholarship_program_syllabus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_program_syllabus_has_module`
--
ALTER TABLE `scholarship_program_syllabus_has_module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_race_setup`
--
ALTER TABLE `scholarship_race_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_religion_setup`
--
ALTER TABLE `scholarship_religion_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_salutation_setup`
--
ALTER TABLE `scholarship_salutation_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_scheme`
--
ALTER TABLE `scholarship_scheme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_selection_type`
--
ALTER TABLE `scholarship_selection_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_state`
--
ALTER TABLE `scholarship_state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_sub_thrust`
--
ALTER TABLE `scholarship_sub_thrust`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_sub_thrust_has_entry_requirement`
--
ALTER TABLE `scholarship_sub_thrust_has_entry_requirement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_sub_thrust_has_program`
--
ALTER TABLE `scholarship_sub_thrust_has_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_sub_thrust_has_requirement`
--
ALTER TABLE `scholarship_sub_thrust_has_requirement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_sub_thrust_requirement_has_age`
--
ALTER TABLE `scholarship_sub_thrust_requirement_has_age`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_sub_thrust_requirement_has_education`
--
ALTER TABLE `scholarship_sub_thrust_requirement_has_education`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_sub_thrust_requirement_has_other`
--
ALTER TABLE `scholarship_sub_thrust_requirement_has_other`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_sub_thrust_requirement_has_work_experience`
--
ALTER TABLE `scholarship_sub_thrust_requirement_has_work_experience`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_thrust`
--
ALTER TABLE `scholarship_thrust`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_users`
--
ALTER TABLE `scholarship_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_work_specialisation`
--
ALTER TABLE `scholarship_work_specialisation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_applicant`
--
ALTER TABLE `scholar_applicant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_applicant_employment_status`
--
ALTER TABLE `scholar_applicant_employment_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_applicant_family_details`
--
ALTER TABLE `scholar_applicant_family_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_applicant_last_login`
--
ALTER TABLE `scholar_applicant_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_last_login`
--
ALTER TABLE `scholar_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_permissions`
--
ALTER TABLE `scholar_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_roles`
--
ALTER TABLE `scholar_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_role_permissions`
--
ALTER TABLE `scholar_role_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `school_details`
--
ALTER TABLE `school_details`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reg_no` (`reg_no`),
  ADD KEY `id_city` (`id_city`),
  ADD KEY `id_state` (`id_state`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `semester_has_activity`
--
ALTER TABLE `semester_has_activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_activity` (`id_activity`),
  ADD KEY `id_semester` (`id_semester`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `set_form`
--
ALTER TABLE `set_form`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `set_form_has_question_pool`
--
ALTER TABLE `set_form_has_question_pool`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sibbling_discount`
--
ALTER TABLE `sibbling_discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specialization_setup`
--
ALTER TABLE `specialization_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponser`
--
ALTER TABLE `sponser`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_city` (`id_state`),
  ADD KEY `id_state` (`id_country`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `sponser_coordinator_details`
--
ALTER TABLE `sponser_coordinator_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponser_credit_note`
--
ALTER TABLE `sponser_credit_note`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponser_credit_note_details`
--
ALTER TABLE `sponser_credit_note_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponser_fee_info_details`
--
ALTER TABLE `sponser_fee_info_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponser_has_students`
--
ALTER TABLE `sponser_has_students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_sponser` (`id_sponser`),
  ADD KEY `id_student` (`id_student`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `sponser_main_invoice`
--
ALTER TABLE `sponser_main_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponser_main_invoice_details`
--
ALTER TABLE `sponser_main_invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponser_receipt`
--
ALTER TABLE `sponser_receipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponser_receipt_details`
--
ALTER TABLE `sponser_receipt_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponser_receipt_paid_details`
--
ALTER TABLE `sponser_receipt_paid_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`ic_no`),
  ADD UNIQUE KEY `mobile_number` (`mobile_number`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `id_department` (`id_department`);

--
-- Indexes for table `staff_has_course`
--
ALTER TABLE `staff_has_course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_staff` (`id_staff`),
  ADD KEY `id_course` (`id_course`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_emergency_contact_details`
--
ALTER TABLE `student_emergency_contact_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_insurance_details`
--
ALTER TABLE `student_insurance_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_last_login`
--
ALTER TABLE `student_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_marks_entry`
--
ALTER TABLE `student_marks_entry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_marks_entry_details`
--
ALTER TABLE `student_marks_entry_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_marks_entry_history`
--
ALTER TABLE `student_marks_entry_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_note`
--
ALTER TABLE `student_note`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_passport_details`
--
ALTER TABLE `student_passport_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_placement`
--
ALTER TABLE `student_placement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_remarks_entry`
--
ALTER TABLE `student_remarks_entry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_remarks_entry_details`
--
ALTER TABLE `student_remarks_entry_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_semester_course_details`
--
ALTER TABLE `student_semester_course_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_semester_result`
--
ALTER TABLE `student_semester_result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_visa_details`
--
ALTER TABLE `student_visa_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject_details`
--
ALTER TABLE `subject_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tax`
--
ALTER TABLE `tax`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_country`
--
ALTER TABLE `tbl_country`
  ADD PRIMARY KEY (`countryId`);

--
-- Indexes for table `tbl_last_login`
--
ALTER TABLE `tbl_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_permissions`
--
ALTER TABLE `tbl_permissions`
  ADD PRIMARY KEY (`permissionId`);

--
-- Indexes for table `tbl_reset_password`
--
ALTER TABLE `tbl_reset_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  ADD PRIMARY KEY (`roleId`);

--
-- Indexes for table `tbl_role_permissions`
--
ALTER TABLE `tbl_role_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_state`
--
ALTER TABLE `tbl_state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `temp_asset_disposal_detail`
--
ALTER TABLE `temp_asset_disposal_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_bank_details`
--
ALTER TABLE `temp_bank_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_billing_details`
--
ALTER TABLE `temp_billing_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_credit_note_details`
--
ALTER TABLE `temp_credit_note_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_main_invoice` (`id_main_invoice`);

--
-- Indexes for table `temp_credit_transfer_details`
--
ALTER TABLE `temp_credit_transfer_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_documents_program_details`
--
ALTER TABLE `temp_documents_program_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_entry_requirement_other_requirements`
--
ALTER TABLE `temp_entry_requirement_other_requirements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_entry_requirement_requirements`
--
ALTER TABLE `temp_entry_requirement_requirements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_equivalent_course_details`
--
ALTER TABLE `temp_equivalent_course_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_fee_structure_details`
--
ALTER TABLE `temp_fee_structure_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_gpa_cgpa_setup_details`
--
ALTER TABLE `temp_gpa_cgpa_setup_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_grade_setup_details`
--
ALTER TABLE `temp_grade_setup_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_intake_has_programme`
--
ALTER TABLE `temp_intake_has_programme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_internship_company_has_program`
--
ALTER TABLE `temp_internship_company_has_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_inventory_allotment`
--
ALTER TABLE `temp_inventory_allotment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_investment_application_details`
--
ALTER TABLE `temp_investment_application_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_journal_details`
--
ALTER TABLE `temp_journal_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_main_invoice_details`
--
ALTER TABLE `temp_main_invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_mark_distribution_details`
--
ALTER TABLE `temp_mark_distribution_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_performa_invoice_details`
--
ALTER TABLE `temp_performa_invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_programme_has_course`
--
ALTER TABLE `temp_programme_has_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_programme_has_dean`
--
ALTER TABLE `temp_programme_has_dean`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_programme_has_scheme`
--
ALTER TABLE `temp_programme_has_scheme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_program_landscape_semester_details`
--
ALTER TABLE `temp_program_landscape_semester_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_pr_entry`
--
ALTER TABLE `temp_pr_entry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_receipt_details`
--
ALTER TABLE `temp_receipt_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_scheme_has_program`
--
ALTER TABLE `temp_scheme_has_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_scholarship_documents_program_details`
--
ALTER TABLE `temp_scholarship_documents_program_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_sponser_credit_note_details`
--
ALTER TABLE `temp_sponser_credit_note_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_sponser_main_invoice_details`
--
ALTER TABLE `temp_sponser_main_invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_sponser_receipt_details`
--
ALTER TABLE `temp_sponser_receipt_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_staff_has_course`
--
ALTER TABLE `temp_staff_has_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_tender_commitee`
--
ALTER TABLE `temp_tender_commitee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_tender_remarks`
--
ALTER TABLE `temp_tender_remarks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tender_commitee`
--
ALTER TABLE `tender_commitee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tender_quotation`
--
ALTER TABLE `tender_quotation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tender_quotation_detail`
--
ALTER TABLE `tender_quotation_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tender_remarks`
--
ALTER TABLE `tender_remarks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tender_submission_vendor`
--
ALTER TABLE `tender_submission_vendor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_details`
--
ALTER TABLE `vendor_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visa_details`
--
ALTER TABLE `visa_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academic_year`
--
ALTER TABLE `academic_year`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `account_code`
--
ALTER TABLE `account_code`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `activity_code`
--
ALTER TABLE `activity_code`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `activity_details`
--
ALTER TABLE `activity_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `add_course_to_program_landscape`
--
ALTER TABLE `add_course_to_program_landscape`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `add_temp_receipt_paid_amount`
--
ALTER TABLE `add_temp_receipt_paid_amount`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `advisor_tagging`
--
ALTER TABLE `advisor_tagging`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `alumni_discount`
--
ALTER TABLE `alumni_discount`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `amount_calculation_type`
--
ALTER TABLE `amount_calculation_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `apex_status`
--
ALTER TABLE `apex_status`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `applicant`
--
ALTER TABLE `applicant`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `applicant_has_alumni_discount`
--
ALTER TABLE `applicant_has_alumni_discount`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `applicant_has_document`
--
ALTER TABLE `applicant_has_document`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `applicant_has_employee_discount`
--
ALTER TABLE `applicant_has_employee_discount`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `applicant_has_sibbling_discount`
--
ALTER TABLE `applicant_has_sibbling_discount`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `applicant_last_login`
--
ALTER TABLE `applicant_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `applicant_status`
--
ALTER TABLE `applicant_status`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `apply_change_programme`
--
ALTER TABLE `apply_change_programme`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `apply_change_scheme`
--
ALTER TABLE `apply_change_scheme`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `apply_change_status`
--
ALTER TABLE `apply_change_status`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `asset_category`
--
ALTER TABLE `asset_category`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asset_disposal`
--
ALTER TABLE `asset_disposal`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asset_disposal_detail`
--
ALTER TABLE `asset_disposal_detail`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asset_item`
--
ALTER TABLE `asset_item`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asset_order`
--
ALTER TABLE `asset_order`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asset_pre_registration`
--
ALTER TABLE `asset_pre_registration`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asset_registration`
--
ALTER TABLE `asset_registration`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `asset_sub_category`
--
ALTER TABLE `asset_sub_category`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assign_student_to_exam_center`
--
ALTER TABLE `assign_student_to_exam_center`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attendence_setup`
--
ALTER TABLE `attendence_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `award`
--
ALTER TABLE `award`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `award_level`
--
ALTER TABLE `award_level`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bank_details`
--
ALTER TABLE `bank_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bank_registration`
--
ALTER TABLE `bank_registration`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `barring`
--
ALTER TABLE `barring`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `barring_type`
--
ALTER TABLE `barring_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bulk_withdraw`
--
ALTER TABLE `bulk_withdraw`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `category_type`
--
ALTER TABLE `category_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `change_branch`
--
ALTER TABLE `change_branch`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `change_branch_history`
--
ALTER TABLE `change_branch_history`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `change_status`
--
ALTER TABLE `change_status`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `class_details`
--
ALTER TABLE `class_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communication_group`
--
ALTER TABLE `communication_group`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communication_group_recepients`
--
ALTER TABLE `communication_group_recepients`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communication_template`
--
ALTER TABLE `communication_template`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `company_details`
--
ALTER TABLE `company_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `convocation`
--
ALTER TABLE `convocation`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `convocation_details`
--
ALTER TABLE `convocation_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `courses_from_programme_landscape`
--
ALTER TABLE `courses_from_programme_landscape`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `course_grade`
--
ALTER TABLE `course_grade`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `course_registration`
--
ALTER TABLE `course_registration`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `course_requisite`
--
ALTER TABLE `course_requisite`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `course_type`
--
ALTER TABLE `course_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `course_withdraw`
--
ALTER TABLE `course_withdraw`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `credit_note`
--
ALTER TABLE `credit_note`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `credit_note_details`
--
ALTER TABLE `credit_note_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `credit_note_type`
--
ALTER TABLE `credit_note_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `credit_transfer`
--
ALTER TABLE `credit_transfer`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `credit_transfer_details`
--
ALTER TABLE `credit_transfer_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currency_rate_setup`
--
ALTER TABLE `currency_rate_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `currency_setup`
--
ALTER TABLE `currency_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `debit_note`
--
ALTER TABLE `debit_note`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `department_has_staff`
--
ALTER TABLE `department_has_staff`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `disposal_type`
--
ALTER TABLE `disposal_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `documents_program`
--
ALTER TABLE `documents_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `documents_program_details`
--
ALTER TABLE `documents_program_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `document_checklist`
--
ALTER TABLE `document_checklist`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_discount`
--
ALTER TABLE `employee_discount`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employment_status`
--
ALTER TABLE `employment_status`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `english_proficiency_details`
--
ALTER TABLE `english_proficiency_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `entry_requirement_other_requirements`
--
ALTER TABLE `entry_requirement_other_requirements`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `entry_requirement_requirements`
--
ALTER TABLE `entry_requirement_requirements`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `equivalent_course`
--
ALTER TABLE `equivalent_course`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `equivalent_course_details`
--
ALTER TABLE `equivalent_course_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `examination`
--
ALTER TABLE `examination`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `examination_assesment_type`
--
ALTER TABLE `examination_assesment_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `examination_components`
--
ALTER TABLE `examination_components`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `examination_details`
--
ALTER TABLE `examination_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam_attendence`
--
ALTER TABLE `exam_attendence`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam_attendence_details`
--
ALTER TABLE `exam_attendence_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam_calender`
--
ALTER TABLE `exam_calender`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam_center`
--
ALTER TABLE `exam_center`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam_center_location`
--
ALTER TABLE `exam_center_location`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam_center_to_semester`
--
ALTER TABLE `exam_center_to_semester`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exam_configuration`
--
ALTER TABLE `exam_configuration`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam_event`
--
ALTER TABLE `exam_event`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam_register`
--
ALTER TABLE `exam_register`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `exam_registration`
--
ALTER TABLE `exam_registration`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `faculty_program`
--
ALTER TABLE `faculty_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `faculty_program_has_staff`
--
ALTER TABLE `faculty_program_has_staff`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fee_category`
--
ALTER TABLE `fee_category`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fee_setup`
--
ALTER TABLE `fee_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `fee_structure`
--
ALTER TABLE `fee_structure`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `fee_structure_activity`
--
ALTER TABLE `fee_structure_activity`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fee_structure_details`
--
ALTER TABLE `fee_structure_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `financial_account_code`
--
ALTER TABLE `financial_account_code`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `financial_year`
--
ALTER TABLE `financial_year`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `frequency_mode`
--
ALTER TABLE `frequency_mode`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `fund_code`
--
ALTER TABLE `fund_code`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gpa_cgpa_setup`
--
ALTER TABLE `gpa_cgpa_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gpa_cgpa_setup_details`
--
ALTER TABLE `gpa_cgpa_setup_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `grade`
--
ALTER TABLE `grade`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `grade_setup`
--
ALTER TABLE `grade_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `grade_setup_details`
--
ALTER TABLE `grade_setup_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `graduation_details`
--
ALTER TABLE `graduation_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `graduation_list`
--
ALTER TABLE `graduation_list`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `grn`
--
ALTER TABLE `grn`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `group_setup`
--
ALTER TABLE `group_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `guest`
--
ALTER TABLE `guest`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hostel_inventory_registration`
--
ALTER TABLE `hostel_inventory_registration`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `hostel_item_registration`
--
ALTER TABLE `hostel_item_registration`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hostel_registration`
--
ALTER TABLE `hostel_registration`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `hostel_room`
--
ALTER TABLE `hostel_room`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `hostel_room_type`
--
ALTER TABLE `hostel_room_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `intake`
--
ALTER TABLE `intake`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `intake_has_programme`
--
ALTER TABLE `intake_has_programme`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `internship_application`
--
ALTER TABLE `internship_application`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `internship_company_has_program`
--
ALTER TABLE `internship_company_has_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `internship_company_registration`
--
ALTER TABLE `internship_company_registration`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `internship_company_type`
--
ALTER TABLE `internship_company_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `internship_student_limit`
--
ALTER TABLE `internship_student_limit`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `inventory_allotment`
--
ALTER TABLE `inventory_allotment`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `investment_application`
--
ALTER TABLE `investment_application`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `investment_application_details`
--
ALTER TABLE `investment_application_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `investment_institution`
--
ALTER TABLE `investment_institution`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `investment_registration`
--
ALTER TABLE `investment_registration`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `investment_registration_withdraw`
--
ALTER TABLE `investment_registration_withdraw`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `investment_type`
--
ALTER TABLE `investment_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `is_scholarship_applicant_examination_details`
--
ALTER TABLE `is_scholarship_applicant_examination_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `is_scholarship_applicant_personel_details`
--
ALTER TABLE `is_scholarship_applicant_personel_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `is_scholar_applicant_employment_status`
--
ALTER TABLE `is_scholar_applicant_employment_status`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `is_scholar_applicant_family_details`
--
ALTER TABLE `is_scholar_applicant_family_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `journal`
--
ALTER TABLE `journal`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `journal_details`
--
ALTER TABLE `journal_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `landscape_course_type`
--
ALTER TABLE `landscape_course_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `late_registration`
--
ALTER TABLE `late_registration`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_invoice`
--
ALTER TABLE `main_invoice`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `main_invoice_details`
--
ALTER TABLE `main_invoice_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `main_invoice_discount_details`
--
ALTER TABLE `main_invoice_discount_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `marital_status_setup`
--
ALTER TABLE `marital_status_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mark_distribution`
--
ALTER TABLE `mark_distribution`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mark_distribution_details`
--
ALTER TABLE `mark_distribution_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `mode_of_category`
--
ALTER TABLE `mode_of_category`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mode_of_program`
--
ALTER TABLE `mode_of_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nationality`
--
ALTER TABLE `nationality`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nonpo_detail`
--
ALTER TABLE `nonpo_detail`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nonpo_entry`
--
ALTER TABLE `nonpo_entry`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `organisation`
--
ALTER TABLE `organisation`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `organisation_comitee`
--
ALTER TABLE `organisation_comitee`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `organisation_has_training_center`
--
ALTER TABLE `organisation_has_training_center`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `organisation_training_center_has_program`
--
ALTER TABLE `organisation_training_center_has_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `other_documents`
--
ALTER TABLE `other_documents`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `partner_category`
--
ALTER TABLE `partner_category`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `partner_university`
--
ALTER TABLE `partner_university`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `partner_university_comitee`
--
ALTER TABLE `partner_university_comitee`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_type`
--
ALTER TABLE `payment_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `performa_invoice`
--
ALTER TABLE `performa_invoice`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `performa_invoice_details`
--
ALTER TABLE `performa_invoice_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `personal_billing_details`
--
ALTER TABLE `personal_billing_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `po_detail`
--
ALTER TABLE `po_detail`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `po_entry`
--
ALTER TABLE `po_entry`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `procurement_category`
--
ALTER TABLE `procurement_category`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `procurement_item`
--
ALTER TABLE `procurement_item`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `procurement_limit`
--
ALTER TABLE `procurement_limit`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `procurement_sub_category`
--
ALTER TABLE `procurement_sub_category`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profile_details`
--
ALTER TABLE `profile_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `programme`
--
ALTER TABLE `programme`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `programme_has_course`
--
ALTER TABLE `programme_has_course`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `programme_has_dean`
--
ALTER TABLE `programme_has_dean`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `programme_has_fee_structure`
--
ALTER TABLE `programme_has_fee_structure`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `programme_has_scheme`
--
ALTER TABLE `programme_has_scheme`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `programme_landscape`
--
ALTER TABLE `programme_landscape`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `program_entry_requirement`
--
ALTER TABLE `program_entry_requirement`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `program_grade`
--
ALTER TABLE `program_grade`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `program_has_acceredation_details`
--
ALTER TABLE `program_has_acceredation_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `program_has_concurrent_program`
--
ALTER TABLE `program_has_concurrent_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `program_has_major_details`
--
ALTER TABLE `program_has_major_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `program_has_minor_details`
--
ALTER TABLE `program_has_minor_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `program_landscape_requirement_details`
--
ALTER TABLE `program_landscape_requirement_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `program_landscape_requisite_details`
--
ALTER TABLE `program_landscape_requisite_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `program_landscape_semester_details`
--
ALTER TABLE `program_landscape_semester_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `program_type`
--
ALTER TABLE `program_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `project_report_submission`
--
ALTER TABLE `project_report_submission`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pr_entry`
--
ALTER TABLE `pr_entry`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pr_entry_details`
--
ALTER TABLE `pr_entry_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `publish_assesment_result_date`
--
ALTER TABLE `publish_assesment_result_date`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `publish_exam`
--
ALTER TABLE `publish_exam`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `publish_exam_result_date`
--
ALTER TABLE `publish_exam_result_date`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `qualification_setup`
--
ALTER TABLE `qualification_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `question_pool`
--
ALTER TABLE `question_pool`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `race_setup`
--
ALTER TABLE `race_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `receipt`
--
ALTER TABLE `receipt`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `receipt_details`
--
ALTER TABLE `receipt_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `receipt_paid_details`
--
ALTER TABLE `receipt_paid_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `release`
--
ALTER TABLE `release`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `religion_setup`
--
ALTER TABLE `religion_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `room_allotment`
--
ALTER TABLE `room_allotment`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `salutation_setup`
--
ALTER TABLE `salutation_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `scheme`
--
ALTER TABLE `scheme`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `scheme_has_program`
--
ALTER TABLE `scheme_has_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholar`
--
ALTER TABLE `scholar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_accreditation_category`
--
ALTER TABLE `scholarship_accreditation_category`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_accreditation_type`
--
ALTER TABLE `scholarship_accreditation_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_amount_calculation_type`
--
ALTER TABLE `scholarship_amount_calculation_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scholarship_applicant`
--
ALTER TABLE `scholarship_applicant`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scholarship_application`
--
ALTER TABLE `scholarship_application`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scholarship_assesment_method_setup`
--
ALTER TABLE `scholarship_assesment_method_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scholarship_award_setup`
--
ALTER TABLE `scholarship_award_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `scholarship_bank_registration`
--
ALTER TABLE `scholarship_bank_registration`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scholarship_country`
--
ALTER TABLE `scholarship_country`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_department`
--
ALTER TABLE `scholarship_department`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_department_has_staff`
--
ALTER TABLE `scholarship_department_has_staff`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_documents`
--
ALTER TABLE `scholarship_documents`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scholarship_documents_for_applicant`
--
ALTER TABLE `scholarship_documents_for_applicant`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scholarship_documents_program`
--
ALTER TABLE `scholarship_documents_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scholarship_documents_program_details`
--
ALTER TABLE `scholarship_documents_program_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scholarship_education_level`
--
ALTER TABLE `scholarship_education_level`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_examination_details`
--
ALTER TABLE `scholarship_examination_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scholarship_individual_entry_requirement`
--
ALTER TABLE `scholarship_individual_entry_requirement`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `scholarship_module_type_setup`
--
ALTER TABLE `scholarship_module_type_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `scholarship_organisation_has_department`
--
ALTER TABLE `scholarship_organisation_has_department`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_organisation_setup`
--
ALTER TABLE `scholarship_organisation_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_organisation_setup_comitee`
--
ALTER TABLE `scholarship_organisation_setup_comitee`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_partner_university`
--
ALTER TABLE `scholarship_partner_university`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholarship_partner_university_has_aggrement`
--
ALTER TABLE `scholarship_partner_university_has_aggrement`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `scholarship_partner_university_has_program`
--
ALTER TABLE `scholarship_partner_university_has_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholarship_partner_university_has_training_center`
--
ALTER TABLE `scholarship_partner_university_has_training_center`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholarship_partner_university_program_has_apprenticeship`
--
ALTER TABLE `scholarship_partner_university_program_has_apprenticeship`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholarship_partner_university_program_has_internship`
--
ALTER TABLE `scholarship_partner_university_program_has_internship`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_partner_university_program_has_study_mode`
--
ALTER TABLE `scholarship_partner_university_program_has_study_mode`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_partner_university_program_has_syllabus`
--
ALTER TABLE `scholarship_partner_university_program_has_syllabus`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_payment_type`
--
ALTER TABLE `scholarship_payment_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scholarship_programme`
--
ALTER TABLE `scholarship_programme`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholarship_program_accreditation`
--
ALTER TABLE `scholarship_program_accreditation`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_program_partner`
--
ALTER TABLE `scholarship_program_partner`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_program_syllabus`
--
ALTER TABLE `scholarship_program_syllabus`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_program_syllabus_has_module`
--
ALTER TABLE `scholarship_program_syllabus_has_module`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_race_setup`
--
ALTER TABLE `scholarship_race_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `scholarship_religion_setup`
--
ALTER TABLE `scholarship_religion_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholarship_salutation_setup`
--
ALTER TABLE `scholarship_salutation_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_scheme`
--
ALTER TABLE `scholarship_scheme`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_selection_type`
--
ALTER TABLE `scholarship_selection_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_state`
--
ALTER TABLE `scholarship_state`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_sub_thrust`
--
ALTER TABLE `scholarship_sub_thrust`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_sub_thrust_has_entry_requirement`
--
ALTER TABLE `scholarship_sub_thrust_has_entry_requirement`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `scholarship_sub_thrust_has_program`
--
ALTER TABLE `scholarship_sub_thrust_has_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `scholarship_sub_thrust_has_requirement`
--
ALTER TABLE `scholarship_sub_thrust_has_requirement`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `scholarship_sub_thrust_requirement_has_age`
--
ALTER TABLE `scholarship_sub_thrust_requirement_has_age`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `scholarship_sub_thrust_requirement_has_education`
--
ALTER TABLE `scholarship_sub_thrust_requirement_has_education`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `scholarship_sub_thrust_requirement_has_other`
--
ALTER TABLE `scholarship_sub_thrust_requirement_has_other`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_sub_thrust_requirement_has_work_experience`
--
ALTER TABLE `scholarship_sub_thrust_requirement_has_work_experience`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholarship_thrust`
--
ALTER TABLE `scholarship_thrust`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_users`
--
ALTER TABLE `scholarship_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_work_specialisation`
--
ALTER TABLE `scholarship_work_specialisation`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholar_applicant`
--
ALTER TABLE `scholar_applicant`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholar_applicant_employment_status`
--
ALTER TABLE `scholar_applicant_employment_status`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scholar_applicant_family_details`
--
ALTER TABLE `scholar_applicant_family_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholar_applicant_last_login`
--
ALTER TABLE `scholar_applicant_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholar_last_login`
--
ALTER TABLE `scholar_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `scholar_permissions`
--
ALTER TABLE `scholar_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholar_roles`
--
ALTER TABLE `scholar_roles`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'role id', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholar_role_permissions`
--
ALTER TABLE `scholar_role_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `school_details`
--
ALTER TABLE `school_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `semester_has_activity`
--
ALTER TABLE `semester_has_activity`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `set_form`
--
ALTER TABLE `set_form`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `set_form_has_question_pool`
--
ALTER TABLE `set_form_has_question_pool`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sibbling_discount`
--
ALTER TABLE `sibbling_discount`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `specialization_setup`
--
ALTER TABLE `specialization_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sponser`
--
ALTER TABLE `sponser`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sponser_coordinator_details`
--
ALTER TABLE `sponser_coordinator_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sponser_credit_note`
--
ALTER TABLE `sponser_credit_note`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sponser_credit_note_details`
--
ALTER TABLE `sponser_credit_note_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sponser_fee_info_details`
--
ALTER TABLE `sponser_fee_info_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sponser_has_students`
--
ALTER TABLE `sponser_has_students`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sponser_main_invoice`
--
ALTER TABLE `sponser_main_invoice`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sponser_main_invoice_details`
--
ALTER TABLE `sponser_main_invoice_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sponser_receipt`
--
ALTER TABLE `sponser_receipt`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sponser_receipt_details`
--
ALTER TABLE `sponser_receipt_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sponser_receipt_paid_details`
--
ALTER TABLE `sponser_receipt_paid_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `staff_has_course`
--
ALTER TABLE `staff_has_course`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `student_emergency_contact_details`
--
ALTER TABLE `student_emergency_contact_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_insurance_details`
--
ALTER TABLE `student_insurance_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_last_login`
--
ALTER TABLE `student_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `student_marks_entry`
--
ALTER TABLE `student_marks_entry`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `student_marks_entry_details`
--
ALTER TABLE `student_marks_entry_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `student_marks_entry_history`
--
ALTER TABLE `student_marks_entry_history`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `student_note`
--
ALTER TABLE `student_note`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `student_passport_details`
--
ALTER TABLE `student_passport_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_placement`
--
ALTER TABLE `student_placement`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `student_remarks_entry`
--
ALTER TABLE `student_remarks_entry`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `student_remarks_entry_details`
--
ALTER TABLE `student_remarks_entry_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `student_semester_course_details`
--
ALTER TABLE `student_semester_course_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `student_semester_result`
--
ALTER TABLE `student_semester_result`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `student_visa_details`
--
ALTER TABLE `student_visa_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subject_details`
--
ALTER TABLE `subject_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tax`
--
ALTER TABLE `tax`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_country`
--
ALTER TABLE `tbl_country`
  MODIFY `countryId` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_last_login`
--
ALTER TABLE `tbl_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT for table `tbl_permissions`
--
ALTER TABLE `tbl_permissions`
  MODIFY `permissionId` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_reset_password`
--
ALTER TABLE `tbl_reset_password`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  MODIFY `roleId` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'role id', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_role_permissions`
--
ALTER TABLE `tbl_role_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_state`
--
ALTER TABLE `tbl_state`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `temp_asset_disposal_detail`
--
ALTER TABLE `temp_asset_disposal_detail`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_bank_details`
--
ALTER TABLE `temp_bank_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_billing_details`
--
ALTER TABLE `temp_billing_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_credit_note_details`
--
ALTER TABLE `temp_credit_note_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_credit_transfer_details`
--
ALTER TABLE `temp_credit_transfer_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `temp_documents_program_details`
--
ALTER TABLE `temp_documents_program_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `temp_entry_requirement_other_requirements`
--
ALTER TABLE `temp_entry_requirement_other_requirements`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_entry_requirement_requirements`
--
ALTER TABLE `temp_entry_requirement_requirements`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `temp_equivalent_course_details`
--
ALTER TABLE `temp_equivalent_course_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_fee_structure_details`
--
ALTER TABLE `temp_fee_structure_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_gpa_cgpa_setup_details`
--
ALTER TABLE `temp_gpa_cgpa_setup_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `temp_grade_setup_details`
--
ALTER TABLE `temp_grade_setup_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `temp_intake_has_programme`
--
ALTER TABLE `temp_intake_has_programme`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `temp_internship_company_has_program`
--
ALTER TABLE `temp_internship_company_has_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `temp_inventory_allotment`
--
ALTER TABLE `temp_inventory_allotment`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `temp_investment_application_details`
--
ALTER TABLE `temp_investment_application_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_journal_details`
--
ALTER TABLE `temp_journal_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_main_invoice_details`
--
ALTER TABLE `temp_main_invoice_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `temp_mark_distribution_details`
--
ALTER TABLE `temp_mark_distribution_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `temp_performa_invoice_details`
--
ALTER TABLE `temp_performa_invoice_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_programme_has_course`
--
ALTER TABLE `temp_programme_has_course`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_programme_has_dean`
--
ALTER TABLE `temp_programme_has_dean`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `temp_programme_has_scheme`
--
ALTER TABLE `temp_programme_has_scheme`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_program_landscape_semester_details`
--
ALTER TABLE `temp_program_landscape_semester_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `temp_pr_entry`
--
ALTER TABLE `temp_pr_entry`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_receipt_details`
--
ALTER TABLE `temp_receipt_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `temp_scheme_has_program`
--
ALTER TABLE `temp_scheme_has_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `temp_scholarship_documents_program_details`
--
ALTER TABLE `temp_scholarship_documents_program_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_sponser_credit_note_details`
--
ALTER TABLE `temp_sponser_credit_note_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_sponser_main_invoice_details`
--
ALTER TABLE `temp_sponser_main_invoice_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_sponser_receipt_details`
--
ALTER TABLE `temp_sponser_receipt_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_staff_has_course`
--
ALTER TABLE `temp_staff_has_course`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `temp_tender_commitee`
--
ALTER TABLE `temp_tender_commitee`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_tender_remarks`
--
ALTER TABLE `temp_tender_remarks`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tender_commitee`
--
ALTER TABLE `tender_commitee`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tender_quotation`
--
ALTER TABLE `tender_quotation`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tender_quotation_detail`
--
ALTER TABLE `tender_quotation_detail`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tender_remarks`
--
ALTER TABLE `tender_remarks`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tender_submission_vendor`
--
ALTER TABLE `tender_submission_vendor`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `vendor_details`
--
ALTER TABLE `vendor_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `visa_details`
--
ALTER TABLE `visa_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

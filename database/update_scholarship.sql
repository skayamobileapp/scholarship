scholar_applicant

applicant Registration Table














ALTER TABLE `scheme_has_program` ADD `budget` FLOAT(20,2) NULL DEFAULT '0' AFTER `quota`;

ALTER TABLE `temp_scheme_has_program` ADD `budget` FLOAT(20,2) NULL DEFAULT '0' AFTER `quota`

ALTER TABLE `scholarship_application` ADD `id_scholarship` INT(20) NULL DEFAULT '0' AFTER `id_program`;

ALTER TABLE `scholarship_application` CHANGE `id_scholarship_scheme` `id_scholarship_scheme` INT(20) NULL DEFAULT '0' COMMENT 'it\'s a id_cohert after flow change';

ALTER TABLE `scholarship_applicant` ADD `id_scholarship` INT(20) NULL DEFAULT '0' AFTER `id_scholar_applicant`;


CREATE TABLE `repayment` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(20) DEFAULT 0,
  `id_cohert` int(20) DEFAULT 0,
  `id_scholarship` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0,
  `currency` varchar(200) DEFAULT '',
  `payment_type` varchar(200) DEFAULT '',
  `installment_amount` float(20,2) DEFAULT 0,
  `day` int(20) DEFAULT 0,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `repayment_extension` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_repayment` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `additionnal_months` int(20) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `old_end_date` datetime DEFAULT NULL,
  `new_end_date` datetime DEFAULT NULL,
  `installment_amount` float(20,2) DEFAULT 0,
  `file_one` varchar(512) DEFAULT '',
  `file_two` varchar(512) DEFAULT '',
  `file_three` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `student_status_change_history` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(20) DEFAULT 0,
  `id_status` int(20) DEFAULT 0,
  `id_sub_status` int(20) DEFAULT 0,
  `effective_date` datetime DEFAULT NULL,
  `remarks` varchar(2048) DEFAULT '',
  `file` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `scholarship_partner_university_has_aggrement` ADD `id_program` INT(20) NULL DEFAULT '0' AFTER `end_date`, ADD `amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `id_program`;




CREATE TABLE `fee_structure_master` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_cohert` int(20) DEFAULT 0,
  `id_scholarship` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0,
  `currency` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `fee_structure` ADD `id_fee_structure` INT(20) NULL DEFAULT '0' AFTER `id`;

ALTER TABLE `student` ADD `id_fee_structure` INT(0) NULL DEFAULT '0' AFTER `id_program`;

ALTER TABLE `scholarship_application` ADD `id_fee_structure` INT(0) NULL DEFAULT '0' AFTER `id_scholarship_scheme`;


CREATE TABLE `main_invoice` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(50) DEFAULT '',
  `id_student` int(20) DEFAULT 0,
  `id_cohert` int(20) DEFAULT 0,
  `id_scholarship` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `invoice_number` varchar(50) DEFAULT '',
  `type_of_invoice` varchar(50) DEFAULT '',
  `date_time` datetime DEFAULT current_timestamp(),
  `remarks` varchar(520) DEFAULT '',
  `id_partner_university` int(20) DEFAULT 0,
  `currency` varchar(200) DEFAULT '',
  `total_amount` float(20,2) DEFAULT 0.00,
  `invoice_total` float(20,2) DEFAULT 0.00,
  `total_discount` float(20,2) DEFAULT 0.00,
  `balance_amount` float(20,2) DEFAULT 0.00,
  `paid_amount` float(20,2) DEFAULT 0.00,
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `main_invoice_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_main_invoice` int(20) DEFAULT NULL,
  `id_fee_item` int(20) DEFAULT NULL,
  `amount` float(20,2) DEFAULT NULL,
  `quantity` int(20) DEFAULT 0,
  `price` int(20) DEFAULT 0,
  `id_reference` int(20) DEFAULT 0,
  `description` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `temp_main_invoice_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(120) DEFAULT '',
  `id_fee_item` int(20) DEFAULT NULL,
  `amount` varchar(20) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

------------------ Update Here From Server---------------





-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 20, 2020 at 10:04 PM
-- Server version: 10.2.33-MariaDB-log
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `camsedu_scholarship`
--

-- --------------------------------------------------------

--
-- Table structure for table `academic_year`
--

CREATE TABLE `academic_year` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `from_year` varchar(20) DEFAULT '',
  `to_year` varchar(20) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `academic_year`
--

INSERT INTO `academic_year` (`id`, `name`, `start_date`, `end_date`, `from_year`, `to_year`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, '2020', '2020-01-01 00:00:00', '2020-12-31 00:00:00', '2020', '2020', 1, 1, '2020-07-10 23:37:30', NULL, '2020-07-10 23:37:30'),
(2, '2021', '2021-01-01 00:00:00', '2021-12-31 00:00:00', '', '', 1, 1, '2020-08-27 17:13:00', NULL, '2020-08-27 17:13:00'),
(3, '2014', '2020-09-21 00:00:00', '2014-12-31 00:00:00', '', '', 1, NULL, '2020-08-31 22:57:54', NULL, '2020-08-31 22:57:54'),
(4, '2016', '2016-01-01 00:00:00', '2016-12-31 00:00:00', '', '', 1, 1, '2020-08-31 23:01:07', NULL, '2020-08-31 23:01:07'),
(5, '2017', '2017-01-01 00:00:00', '2017-12-31 00:00:00', '', '', 1, 1, '2020-08-31 23:53:03', NULL, '2020-08-31 23:53:03');

-- --------------------------------------------------------

--
-- Table structure for table `applicantion_status_change_history`
--

CREATE TABLE `applicantion_status_change_history` (
  `id` int(20) NOT NULL,
  `id_application` int(20) DEFAULT 0,
  `id_applicant` int(20) DEFAULT 0,
  `id_status` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicantion_status_change_history`
--

INSERT INTO `applicantion_status_change_history` (`id`, `id_application`, `id_applicant`, `id_status`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(3, 1, 7, 2, NULL, 1, '2020-09-18 15:07:18', NULL, '2020-09-18 15:07:18'),
(4, 1, 7, 6, NULL, 1, '2020-09-18 15:07:31', NULL, '2020-09-18 15:07:31'),
(5, 1, 7, 7, NULL, 1, '2020-09-18 15:08:09', NULL, '2020-09-18 15:08:09'),
(6, 1, 7, 8, NULL, 1, '2020-09-18 15:08:56', NULL, '2020-09-18 15:08:56'),
(7, 2, 8, 2, NULL, 1, '2020-09-19 07:48:59', NULL, '2020-09-19 07:48:59');

-- --------------------------------------------------------

--
-- Table structure for table `apply_claim`
--

CREATE TABLE `apply_claim` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT 0,
  `type` varchar(1024) DEFAULT '',
  `id_program` int(20) DEFAULT 0,
  `claim_amount` float(20,2) DEFAULT 0.00,
  `reason` varchar(2048) DEFAULT '',
  `reference_number` varchar(1024) DEFAULT '',
  `paid_date` datetime DEFAULT NULL,
  `description` varchar(10240) DEFAULT '',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `amount` float(20,2) DEFAULT 0.00,
  `file_one` varchar(1024) DEFAULT '',
  `file_two` varchar(1024) DEFAULT '',
  `file_three` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `apply_claim`
--

INSERT INTO `apply_claim` (`id`, `id_student`, `type`, `id_program`, `claim_amount`, `reason`, `reference_number`, `paid_date`, `description`, `start_date`, `end_date`, `amount`, `file_one`, `file_two`, `file_three`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Training', 3, 1.00, '', 'Refer111', '2020-09-01 00:00:00', 'Des One', '2020-09-01 00:00:00', '2020-09-30 00:00:00', 1.00, '', '', '', 1, 0, '2020-09-17 16:24:53', 1, '2020-09-17 16:24:53'),
(2, 1, 'Miscellaneouss', 3, 0.00, '', '', NULL, 'Desc', '2020-09-01 00:00:00', '2020-09-30 00:00:00', 1000.00, '', '', '', 0, 0, '2020-09-17 16:25:09', NULL, '2020-09-17 16:25:09'),
(3, 1, 'Conferences', 3, 0.00, '', '', NULL, 'DDD', '2020-09-03 00:00:00', '2020-09-30 00:00:00', 123.00, '', '', '', 0, 0, '2020-09-17 16:26:29', NULL, '2020-09-17 16:26:29'),
(4, 5, 'Training', NULL, 1080.00, '', 'REFE231231', '2020-09-18 00:00:00', 'Training Data', '2020-09-01 00:00:00', '2020-09-30 00:00:00', 1300.00, '', '', '', 1, 0, '2020-09-18 15:13:51', 1, '2020-09-18 15:13:51'),
(5, 5, 'Conferences', NULL, 0.00, 'Issued For Training', 'NOT ISSUED', '2020-10-08 00:00:00', 'COnference Meeting', '2020-09-01 00:00:00', '2020-11-30 00:00:00', 1800.00, '', '', '', 2, 0, '2020-09-18 15:14:18', 1, '2020-09-18 15:14:18'),
(6, 5, 'Miscellaneouss', NULL, 1080.00, '', 'Scholarship Issued Partial', '2020-09-28 00:00:00', 'Misscellaneouss Activity', '2020-09-01 00:00:00', '2020-09-08 00:00:00', 1480.00, '', '', '', 1, 0, '2020-09-18 15:14:50', 1, '2020-09-18 15:14:50');

-- --------------------------------------------------------

--
-- Table structure for table `budget_allocation`
--

CREATE TABLE `budget_allocation` (
  `id` int(20) NOT NULL,
  `id_thrust` int(20) DEFAULT NULL,
  `id_academic_year` int(20) DEFAULT NULL,
  `amount` varchar(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `budget_allocation`
--

INSERT INTO `budget_allocation` (`id`, `id_thrust`, `id_academic_year`, `amount`, `status`) VALUES
(2, 3, 1, '4000000.00', 1),
(3, 3, 1, '2000000.00', 1),
(4, 1, 1, '2000000.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `budget_allocation_details`
--

CREATE TABLE `budget_allocation_details` (
  `id` int(20) NOT NULL,
  `id_budget_allocation` int(20) DEFAULT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `amount` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `budget_allocation_details`
--

INSERT INTO `budget_allocation_details` (`id`, `id_budget_allocation`, `id_sub_thrust`, `amount`) VALUES
(5, 2, 3, '678'),
(6, 2, 4, '3456');

-- --------------------------------------------------------

--
-- Table structure for table `cohort_activity`
--

CREATE TABLE `cohort_activity` (
  `id` bigint(20) NOT NULL,
  `id_scholarship_sub_thrust` int(20) DEFAULT NULL,
  `id_programme` int(20) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `activity_name` varchar(500) DEFAULT NULL,
  `id_communication_template` int(20) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `id_cohort` int(20) DEFAULT NULL,
  `variable` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cohort_activity`
--

INSERT INTO `cohort_activity` (`id`, `id_scholarship_sub_thrust`, `id_programme`, `start_date`, `end_date`, `activity_name`, `id_communication_template`, `status`, `id_cohort`, `variable`) VALUES
(2, 1, 3, '2020-09-18', '2020-09-19', 'Activitys', 1, 1, 2, 'Invoice');

-- --------------------------------------------------------

--
-- Table structure for table `communication_group`
--

CREATE TABLE `communication_group` (
  `id` int(20) NOT NULL,
  `id_template` int(20) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_recepients`
--

CREATE TABLE `communication_group_recepients` (
  `id` int(20) NOT NULL,
  `id_group` int(20) DEFAULT 0,
  `id_recepient` int(20) DEFAULT 0,
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `communication_template`
--

CREATE TABLE `communication_template` (
  `id` int(20) NOT NULL,
  `name` text DEFAULT NULL,
  `subject` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_template`
--

INSERT INTO `communication_template` (`id`, `name`, `subject`, `message`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'SCholrship', 'subject', '<p> </p>\r\n\r\n<p><img alt=\"Yayasan Peneraju Pendidikan Bumiputera\" src=\"https://yayasanpeneraju.com.my/wp-content/uploads/2017/10/logo.png\"></p>\r\n\r\n<p>Thank you for your application in our Scholarship Programmes.</p>\r\n\r\n<p>We are pleased to inform that you have been shortlisted to undergo our Stage 1 Assessment which will be administered online. You shall be notified on the details of the online assessment soon.</p>\r\n\r\n<p>We wish you all the best.</p>\r\n\r\n<p> </p>\r\n\r\n<p>Thank you.</p>\r\n\r\n<p> </p>\r\n\r\n<p>On Behalf of Yayasan Peneraju<br>\r\n<br>\r\n </p>\r\n', 1, NULL, '2020-09-16 11:59:06', NULL, '2020-09-16 11:59:06');

-- --------------------------------------------------------

--
-- Table structure for table `currency_rate_setup`
--

CREATE TABLE `currency_rate_setup` (
  `id` int(20) NOT NULL,
  `id_currency` int(20) DEFAULT 0,
  `exchange_rate` varchar(200) DEFAULT '',
  `min_rate` varchar(200) DEFAULT '',
  `max_rate` varchar(200) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency_rate_setup`
--

INSERT INTO `currency_rate_setup` (`id`, `id_currency`, `exchange_rate`, `min_rate`, `max_rate`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 1, '1', '1', '1', '2020-08-04 00:00:00', 1, 1, '2020-08-09 01:59:24', NULL, '2020-08-09 01:59:24');

-- --------------------------------------------------------

--
-- Table structure for table `currency_setup`
--

CREATE TABLE `currency_setup` (
  `id` int(20) NOT NULL,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `prefix` varchar(200) DEFAULT '',
  `suffix` varchar(200) DEFAULT '',
  `decimal_place` int(20) DEFAULT 0,
  `default` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency_setup`
--

INSERT INTO `currency_setup` (`id`, `code`, `name`, `name_optional_language`, `prefix`, `suffix`, `decimal_place`, `default`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'MYR', 'MYR', 'MYR', '', '', 2, 0, 1, NULL, '2020-09-18 18:35:09', NULL, '2020-09-18 18:35:09');

-- --------------------------------------------------------

--
-- Table structure for table `fee_category`
--

CREATE TABLE `fee_category` (
  `id` int(20) NOT NULL,
  `code` varchar(520) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `fee_group` varchar(200) DEFAULT '',
  `sequence` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_category`
--

INSERT INTO `fee_category` (`id`, `code`, `name`, `name_optional_language`, `fee_group`, `sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'TUTFEE', 'Tuition Fee', 'Tuition Fee', 'Statement Of Account', 1, 1, NULL, '2020-07-11 00:20:37', NULL, '2020-07-11 00:20:37'),
(2, 'MISC FEE', 'Miscellaneous Fee', 'MISC FEE', 'Statement Of Account', 2, 1, NULL, '2020-07-11 00:21:07', NULL, '2020-07-11 00:21:07');

-- --------------------------------------------------------

--
-- Table structure for table `fee_setup`
--

CREATE TABLE `fee_setup` (
  `id` int(20) NOT NULL,
  `code` varchar(50) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `id_fee_category` int(10) DEFAULT NULL,
  `id_amount_calculation_type` int(10) DEFAULT NULL,
  `id_frequency_mode` int(10) DEFAULT NULL,
  `account_code` varchar(50) DEFAULT '',
  `is_refundable` int(2) DEFAULT NULL,
  `is_non_invoice` int(2) DEFAULT NULL,
  `is_gst` int(2) DEFAULT NULL,
  `gst_tax` varchar(50) DEFAULT '',
  `effective_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp(),
  `recoverable_day` varchar(50) DEFAULT NULL,
  `recoverable_period` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_setup`
--

INSERT INTO `fee_setup` (`id`, `code`, `name`, `name_optional_language`, `id_fee_category`, `id_amount_calculation_type`, `id_frequency_mode`, `account_code`, `is_refundable`, `is_non_invoice`, `is_gst`, `gst_tax`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`, `recoverable_day`, `recoverable_period`) VALUES
(12, 'Traiining', 'Training Fees', 'Traiining', 1, 1, 0, '4001', 0, 1, 0, '0', '2020-09-17 00:00:00', 1, NULL, '2020-09-16 12:08:11', NULL, '2020-09-16 12:08:11', '5', 'Monthly'),
(13, 'Tuition & Certification Fee', 'Tuition & Certification Fee', 'Tuition & Certification Fee', 2, 1, 3, '4003', 0, 1, 0, '6', '2020-09-20 00:00:00', 1, NULL, '2020-09-20 03:35:10', NULL, '2020-09-20 03:35:10', '1', ''),
(14, 'Living Allowance', 'Living Allowance', 'Living Allowance', 1, 1, 1, '4004', 0, 0, 0, '0', '2020-09-20 00:00:00', 1, NULL, '2020-09-20 05:14:07', NULL, '2020-09-20 05:14:07', '', ''),
(15, 'Book Allowance', 'Book Allowance', 'Book Allowance', 1, 1, 2, '4005', 0, 0, 0, '0', '2020-09-20 00:00:00', 1, NULL, '2020-09-20 05:20:38', NULL, '2020-09-20 05:20:38', '', ''),
(16, 'Accommodation', 'Accommodation', 'Accommodation', 1, 1, 1, '4004', 0, 0, 0, '0', '2020-09-20 00:00:00', 1, NULL, '2020-09-20 05:24:09', NULL, '2020-09-20 05:24:09', '', ''),
(17, 'Meal Allowance', 'Meal Allowance', 'Meal Allowance', 1, 1, 1, '4005', 0, 0, 0, '0', '2020-09-20 00:00:00', 1, NULL, '2020-09-20 05:25:05', NULL, '2020-09-20 05:25:05', '', ''),
(18, 'Training', 'Training', 'Training', 1, 1, 2, '4006', 0, 1, 0, '0', '2020-09-20 00:00:00', 1, NULL, '2020-09-20 05:25:33', NULL, '2020-09-20 05:25:33', '1', ''),
(19, 'Transport', 'Transport', 'Transport', 1, 1, 1, '4003', 0, 0, 0, '0', '2020-09-20 00:00:00', 1, NULL, '2020-09-20 05:26:06', NULL, '2020-09-20 05:26:06', '1', ''),
(20, 'Insurance', 'Insurance', 'Insurance', 1, 1, 3, '4002', 0, 0, 0, '0', '2020-09-21 00:00:00', 1, NULL, '2020-09-20 18:37:02', NULL, '2020-09-20 18:37:02', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `fee_structure`
--

CREATE TABLE `fee_structure` (
  `id` int(20) NOT NULL,
  `id_fee_structure` int(20) DEFAULT 0,
  `id_fee_item` int(20) DEFAULT 0,
  `id_sub_thrust` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0.00,
  `name` varchar(1024) DEFAULT '',
  `currency` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp(),
  `funding_percentage` varchar(50) DEFAULT NULL,
  `recoverable_amount` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_structure`
--

INSERT INTO `fee_structure` (`id`, `id_fee_structure`, `id_fee_item`, `id_sub_thrust`, `amount`, `name`, `currency`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `funding_percentage`, `recoverable_amount`) VALUES
(1, 0, 12, 7, 1880.00, '', 'MYR', 1, NULL, '2020-09-16 14:32:37', NULL, '2020-09-16 14:32:37', NULL, NULL),
(2, 0, 12, 7, 880.00, '', 'USD', 1, NULL, '2020-09-16 14:32:46', NULL, '2020-09-16 14:32:46', NULL, NULL),
(3, 0, 0, 7, 0.00, '', 'USD', 1, NULL, '2020-09-16 14:34:51', NULL, '2020-09-16 14:34:51', NULL, NULL),
(4, 0, 0, 7, 0.00, '', 'USD', 1, NULL, '2020-09-16 14:34:51', NULL, '2020-09-16 14:34:51', NULL, NULL),
(5, 0, 0, 7, 0.00, '', 'MYR', 1, NULL, '2020-09-16 14:34:52', NULL, '2020-09-16 14:34:52', NULL, NULL),
(6, 2, 16, 0, 120.00, '', '', NULL, NULL, '2020-09-20 14:34:11', NULL, '2020-09-20 14:34:11', '80', '20'),
(7, 2, 14, 0, 1000.00, '', '', NULL, NULL, '2020-09-20 14:34:27', NULL, '2020-09-20 14:34:27', '30', '400'),
(8, 2, 18, 0, 1800.00, '', '', NULL, NULL, '2020-09-20 14:34:57', NULL, '2020-09-20 14:34:57', '30', '400'),
(9, 3, 13, 0, 5000.00, '', '', NULL, NULL, '2020-09-20 18:32:22', NULL, '2020-09-20 18:32:22', '50', '2500'),
(11, 3, 14, 0, 5000.00, '', '', NULL, NULL, '2020-09-20 18:33:01', NULL, '2020-09-20 18:33:01', '100', '0'),
(12, 3, 15, 0, 800.00, '', '', NULL, NULL, '2020-09-20 18:33:25', NULL, '2020-09-20 18:33:25', '100', '0'),
(13, 3, 16, 0, 5000.00, '', '', NULL, NULL, '2020-09-20 18:33:37', NULL, '2020-09-20 18:33:37', '100', '0'),
(14, 3, 17, 0, 6000.00, '', '', NULL, NULL, '2020-09-20 18:35:11', NULL, '2020-09-20 18:35:11', '100', '0'),
(16, 3, 18, 0, 5000.00, '', '', NULL, NULL, '2020-09-20 18:35:55', NULL, '2020-09-20 18:35:55', '75', '1250'),
(17, 3, 19, 0, 800.00, '', '', NULL, NULL, '2020-09-20 18:36:09', NULL, '2020-09-20 18:36:09', '100', '0'),
(18, 3, 20, 0, 1200.00, '', '', NULL, NULL, '2020-09-20 18:37:27', NULL, '2020-09-20 18:37:27', '100', '0');

-- --------------------------------------------------------

--
-- Table structure for table `fee_structure_master`
--

CREATE TABLE `fee_structure_master` (
  `id` int(20) NOT NULL,
  `id_cohert` int(20) DEFAULT 0,
  `id_scholarship` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0.00,
  `currency` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fee_structure_master`
--

INSERT INTO `fee_structure_master` (`id`, `id_cohert`, `id_scholarship`, `id_program`, `amount`, `currency`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 1, 1, 1, 0.00, '1', 1, NULL, '2020-09-20 14:32:42', NULL, '2020-09-20 14:32:42'),
(3, 2, 3, 1, 0.00, '1', 1, NULL, '2020-09-20 18:31:28', NULL, '2020-09-20 18:31:28');

-- --------------------------------------------------------

--
-- Table structure for table `financial_account_code`
--

CREATE TABLE `financial_account_code` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `short_code` varchar(50) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `level` int(2) DEFAULT 0,
  `id_parent` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `financial_account_code`
--

INSERT INTO `financial_account_code` (`id`, `name`, `name_optional_language`, `short_code`, `code`, `type`, `level`, `id_parent`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '', '', '', '4001', 'Tuition Fees', 0, 0, 1, 1, '2020-08-08 07:27:44', 1, '2020-08-08 07:27:44'),
(2, '', '', '', '4002', 'Assessment Fees', 0, 0, 1, 1, '2020-08-08 07:27:46', 1, '2020-08-08 07:27:46'),
(3, '', '', '', '4003', 'Registration Fees', 0, 0, 1, 1, '2020-08-20 09:50:05', NULL, '2020-08-20 09:50:05'),
(4, '', '', '', '4004', 'Recurring Fees', 0, 0, 1, 1, '2020-08-20 09:50:27', NULL, '2020-08-20 09:50:27'),
(5, '', '', '', '4005', 'Remarking Fees', 0, 0, 1, 1, '2020-08-20 09:52:05', NULL, '2020-08-20 09:52:05'),
(6, '', '', '', '4006', 'Application Fees', 0, 0, 1, 1, '2020-08-20 09:53:58', NULL, '2020-08-20 09:53:58'),
(7, '', '', '', '4007', 'Credit Transfer Fees', 0, 0, 1, 1, '2020-08-20 09:54:23', NULL, '2020-08-20 09:54:23'),
(8, '', '', '', '4008', 'Exemption Fees', 0, 0, 1, 1, '2020-08-20 09:54:40', NULL, '2020-08-20 09:54:40'),
(9, '', '', '', '4010', 'Penalty Fees', 0, 0, 1, 1, '2020-08-20 09:54:54', NULL, '2020-08-20 09:54:54'),
(10, '', '', '', '6009', 'GST', 0, 0, 1, 1, '2020-08-20 09:55:31', NULL, '2020-08-20 09:55:31'),
(11, '', '', '', '6008', 'Sibling Discount', 0, 0, 1, 1, '2020-08-20 09:56:37', NULL, '2020-08-20 09:56:37'),
(12, '', '', '', '6007', 'Alumni Discount', 0, 0, 1, 1, '2020-08-20 09:56:53', NULL, '2020-08-20 09:56:53'),
(13, '', '', '', '6006', 'Employee Discount', 0, 0, 1, 1, '2020-08-20 09:57:13', NULL, '2020-08-20 09:57:13'),
(14, '', '', '', '4011', 'Convocation Fees', 0, 0, 1, 1, '2020-08-20 09:57:36', NULL, '2020-08-20 09:57:36'),
(15, '', '', '', '2009', 'Student Deposit', 0, 0, 1, 1, '2020-08-20 09:58:36', NULL, '2020-08-20 09:58:36');

-- --------------------------------------------------------

--
-- Table structure for table `internship_company_has_program`
--

CREATE TABLE `internship_company_has_program` (
  `id` int(20) NOT NULL,
  `id_company` bigint(20) DEFAULT 0,
  `id_company_type` bigint(20) DEFAULT 0,
  `id_intake` bigint(20) DEFAULT 0,
  `id_program` bigint(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internship_company_has_program`
--

INSERT INTO `internship_company_has_program` (`id`, `id_company`, `id_company_type`, `id_intake`, `id_program`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 0, 0, 2, 0, NULL, '2020-08-01 11:55:02', NULL, '2020-08-01 11:55:02'),
(2, 1, 0, 0, 0, 0, NULL, '2020-08-01 11:55:24', NULL, '2020-08-01 11:55:24'),
(3, 1, 0, 0, 3, 0, NULL, '2020-08-01 11:55:30', NULL, '2020-08-01 11:55:30'),
(4, 1, 0, 0, 0, 0, NULL, '2020-08-01 11:55:40', NULL, '2020-08-01 11:55:40');

-- --------------------------------------------------------

--
-- Table structure for table `internship_company_registration`
--

CREATE TABLE `internship_company_registration` (
  `id` int(20) NOT NULL,
  `name` varchar(500) DEFAULT '',
  `registration_no` varchar(50) DEFAULT '',
  `person_in_charge` varchar(500) DEFAULT '',
  `email` varchar(500) DEFAULT '',
  `address` varchar(500) DEFAULT '',
  `contact_number` bigint(20) DEFAULT 0,
  `id_company_type` bigint(20) DEFAULT 0,
  `id_country` bigint(20) DEFAULT 0,
  `id_state` bigint(20) DEFAULT 0,
  `city` varchar(500) DEFAULT '',
  `zipcode` bigint(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internship_company_registration`
--

INSERT INTO `internship_company_registration` (`id`, `name`, `registration_no`, `person_in_charge`, `email`, `address`, `contact_number`, `id_company_type`, `id_country`, `id_state`, `city`, `zipcode`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Benzamin Liited', 'REG1243', 'Syed', 'syed@gmail.com', '#289,  uarters 123, Tower Road', 7892782459, 1, 1, 1, 'KL', 577522, 1, NULL, '2020-08-01 11:55:04', NULL, '2020-08-01 11:55:04');

-- --------------------------------------------------------

--
-- Table structure for table `internship_company_type`
--

CREATE TABLE `internship_company_type` (
  `id` int(20) NOT NULL,
  `name` varchar(500) DEFAULT '',
  `short_name` varchar(500) DEFAULT '',
  `name_in_malay` varchar(500) DEFAULT '',
  `code` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internship_company_type`
--

INSERT INTO `internship_company_type` (`id`, `name`, `short_name`, `name_in_malay`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Private Sector Company', '', 'Private Sector Company', 'PRI', 1, NULL, '2020-08-01 11:54:13', NULL, '2020-08-01 11:54:13');

-- --------------------------------------------------------

--
-- Table structure for table `internship_student_limit`
--

CREATE TABLE `internship_student_limit` (
  `id` bigint(20) NOT NULL,
  `max_limit` bigint(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `internship_student_limit`
--

INSERT INTO `internship_student_limit` (`id`, `max_limit`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 5, 1, NULL, '2020-08-01 11:52:28', NULL, '2020-08-02 12:23:52');

-- --------------------------------------------------------

--
-- Table structure for table `is_scholarship_applicant_examination_details`
--

CREATE TABLE `is_scholarship_applicant_examination_details` (
  `id` int(20) NOT NULL,
  `id_scholarship_program` int(20) DEFAULT 0,
  `is_qualification_level` int(20) DEFAULT 1,
  `is_degree_awarded` int(20) DEFAULT 1,
  `is_specialization` int(20) DEFAULT 1,
  `is_class_degree` int(20) DEFAULT 1,
  `is_result` int(20) DEFAULT 1,
  `is_year` int(20) DEFAULT 1,
  `is_medium` int(20) DEFAULT 1,
  `is_college_country` int(20) DEFAULT 1,
  `is_college_name` int(20) DEFAULT 1,
  `is_certificate` int(20) DEFAULT 1,
  `is_transcript` int(20) DEFAULT 1,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `is_scholarship_applicant_examination_details`
--

INSERT INTO `is_scholarship_applicant_examination_details` (`id`, `id_scholarship_program`, `is_qualification_level`, `is_degree_awarded`, `is_specialization`, `is_class_degree`, `is_result`, `is_year`, `is_medium`, `is_college_country`, `is_college_name`, `is_certificate`, `is_transcript`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-07-22 02:12:09', NULL, '2020-07-22 02:12:09'),
(2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-07-24 13:19:07', NULL, '2020-07-24 13:19:07'),
(3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-07-24 23:42:06', NULL, '2020-07-24 23:42:06'),
(4, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-09-16 10:29:40', NULL, '2020-09-16 10:29:40'),
(5, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-09-16 10:47:49', NULL, '2020-09-16 10:47:49'),
(6, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-09-16 10:49:41', NULL, '2020-09-16 10:49:41'),
(7, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-09-16 10:51:07', NULL, '2020-09-16 10:51:07'),
(8, 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-09-16 10:51:55', NULL, '2020-09-16 10:51:55'),
(9, 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-09-16 10:52:44', NULL, '2020-09-16 10:52:44'),
(10, 10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-09-16 10:58:50', NULL, '2020-09-16 10:58:50'),
(11, 11, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-09-16 11:00:27', NULL, '2020-09-16 11:00:27');

-- --------------------------------------------------------

--
-- Table structure for table `is_scholarship_applicant_personel_details`
--

CREATE TABLE `is_scholarship_applicant_personel_details` (
  `id` int(20) NOT NULL,
  `id_scholarship_program` int(20) DEFAULT 0,
  `is_year` int(20) DEFAULT 1,
  `is_id_intake` int(20) DEFAULT 1,
  `is_id_program` int(20) DEFAULT 1,
  `is_full_name` int(20) DEFAULT 1,
  `is_salutation` int(20) DEFAULT 1,
  `is_first_name` int(20) DEFAULT 1,
  `is_last_name` int(20) DEFAULT 1,
  `is_nric` int(20) DEFAULT 1,
  `is_passport` int(20) DEFAULT 1,
  `is_phone` int(20) DEFAULT 1,
  `is_email_id` int(20) DEFAULT 1,
  `is_password` int(20) DEFAULT 1,
  `is_passport_expiry_date` int(20) DEFAULT 1,
  `is_gender` int(20) DEFAULT 1,
  `is_date_of_birth` int(20) DEFAULT 1,
  `is_martial_status` int(20) DEFAULT 1,
  `is_religion` int(20) DEFAULT 1,
  `is_nationality` int(20) DEFAULT 1,
  `is_is_hostel` int(20) DEFAULT 1,
  `is_id_degree_type` int(20) DEFAULT 1,
  `is_id_race` int(20) DEFAULT 1,
  `is_mail_address1` int(20) DEFAULT 1,
  `is_mail_address2` int(20) DEFAULT 1,
  `is_mailing_country` int(20) DEFAULT 1,
  `is_mailing_state` int(20) DEFAULT 1,
  `is_mailing_city` int(20) DEFAULT 1,
  `is_mailing_zipcode` int(20) DEFAULT 1,
  `is_permanent_address1` int(20) DEFAULT 1,
  `is_permanent_address2` int(20) DEFAULT 1,
  `is_permanent_country` int(20) DEFAULT 1,
  `is_permanent_state` int(20) DEFAULT 1,
  `is_permanent_city` int(20) DEFAULT 1,
  `is_permanent_zipcode` int(20) DEFAULT 1,
  `is_sibbling_discount` int(20) DEFAULT 1,
  `is_employee_discount` int(20) DEFAULT 1,
  `is_applicant_status` int(20) DEFAULT 1,
  `is_is_sibbling_discount` int(20) DEFAULT 1,
  `is_is_employee_discount` int(20) DEFAULT 1,
  `is_id_type` int(20) DEFAULT 1,
  `is_passport_number` int(20) DEFAULT 1,
  `email_verified` int(2) DEFAULT 0,
  `is_submitted` int(2) DEFAULT 0,
  `is_updated` int(2) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `approved_by` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `is_scholarship_applicant_personel_details`
--

INSERT INTO `is_scholarship_applicant_personel_details` (`id`, `id_scholarship_program`, `is_year`, `is_id_intake`, `is_id_program`, `is_full_name`, `is_salutation`, `is_first_name`, `is_last_name`, `is_nric`, `is_passport`, `is_phone`, `is_email_id`, `is_password`, `is_passport_expiry_date`, `is_gender`, `is_date_of_birth`, `is_martial_status`, `is_religion`, `is_nationality`, `is_is_hostel`, `is_id_degree_type`, `is_id_race`, `is_mail_address1`, `is_mail_address2`, `is_mailing_country`, `is_mailing_state`, `is_mailing_city`, `is_mailing_zipcode`, `is_permanent_address1`, `is_permanent_address2`, `is_permanent_country`, `is_permanent_state`, `is_permanent_city`, `is_permanent_zipcode`, `is_sibbling_discount`, `is_employee_discount`, `is_applicant_status`, `is_is_sibbling_discount`, `is_is_employee_discount`, `is_id_type`, `is_passport_number`, `email_verified`, `is_submitted`, `is_updated`, `reason`, `status`, `approved_by`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', NULL, 0, NULL, '2020-07-22 02:12:09', NULL, '2020-07-22 02:12:09'),
(2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', NULL, 0, NULL, '2020-07-24 13:19:07', NULL, '2020-07-24 13:19:07'),
(3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', NULL, 0, NULL, '2020-07-24 23:42:06', NULL, '2020-07-24 23:42:06'),
(4, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', NULL, 0, NULL, '2020-09-16 10:29:40', NULL, '2020-09-16 10:29:40'),
(5, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', NULL, 0, NULL, '2020-09-16 10:47:49', NULL, '2020-09-16 10:47:49'),
(6, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', NULL, 0, NULL, '2020-09-16 10:49:41', NULL, '2020-09-16 10:49:41'),
(7, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', NULL, 0, NULL, '2020-09-16 10:51:07', NULL, '2020-09-16 10:51:07'),
(8, 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', NULL, 0, NULL, '2020-09-16 10:51:55', NULL, '2020-09-16 10:51:55'),
(9, 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', NULL, 0, NULL, '2020-09-16 10:52:44', NULL, '2020-09-16 10:52:44'),
(10, 10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', NULL, 0, NULL, '2020-09-16 10:58:50', NULL, '2020-09-16 10:58:50'),
(11, 11, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', NULL, 0, NULL, '2020-09-16 11:00:27', NULL, '2020-09-16 11:00:27');

-- --------------------------------------------------------

--
-- Table structure for table `is_scholar_applicant_employment_status`
--

CREATE TABLE `is_scholar_applicant_employment_status` (
  `id` int(20) NOT NULL,
  `id_scholarship_program` int(10) DEFAULT 0,
  `is_company_name` int(20) DEFAULT 1,
  `is_company_address` int(20) DEFAULT 1,
  `is_telephone` int(20) DEFAULT 1,
  `is_fax_num` int(20) DEFAULT 1,
  `is_designation` int(20) DEFAULT 1,
  `is_position` int(20) DEFAULT 1,
  `is_service_year` int(20) DEFAULT 1,
  `is_industry` int(20) DEFAULT 1,
  `is_job_desc` int(20) DEFAULT 1,
  `is_employment_letter` int(20) DEFAULT 1,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `is_scholar_applicant_employment_status`
--

INSERT INTO `is_scholar_applicant_employment_status` (`id`, `id_scholarship_program`, `is_company_name`, `is_company_address`, `is_telephone`, `is_fax_num`, `is_designation`, `is_position`, `is_service_year`, `is_industry`, `is_job_desc`, `is_employment_letter`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-07-22 02:12:09', NULL, '2020-07-22 02:12:09'),
(2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-07-24 13:19:07', NULL, '2020-07-24 13:19:07'),
(3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-07-24 23:42:06', NULL, '2020-07-24 23:42:06'),
(4, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-09-16 10:29:40', NULL, '2020-09-16 10:29:40'),
(5, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-09-16 10:47:49', NULL, '2020-09-16 10:47:49'),
(6, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-09-16 10:49:41', NULL, '2020-09-16 10:49:41'),
(7, 7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-09-16 10:51:07', NULL, '2020-09-16 10:51:07'),
(8, 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-09-16 10:51:55', NULL, '2020-09-16 10:51:55'),
(9, 9, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-09-16 10:52:44', NULL, '2020-09-16 10:52:44'),
(10, 10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-09-16 10:58:50', NULL, '2020-09-16 10:58:50'),
(11, 11, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, '2020-09-16 11:00:27', NULL, '2020-09-16 11:00:27');

-- --------------------------------------------------------

--
-- Table structure for table `is_scholar_applicant_family_details`
--

CREATE TABLE `is_scholar_applicant_family_details` (
  `id` int(20) NOT NULL,
  `id_scholarship_program` int(20) DEFAULT 0,
  `is_father_name` int(20) DEFAULT 1,
  `is_mother_name` int(20) DEFAULT 1,
  `is_father_deceased` int(20) DEFAULT 1,
  `is_father_occupation` int(20) DEFAULT 1,
  `is_no_siblings` int(20) DEFAULT 1,
  `is_est_fee` int(20) DEFAULT 1,
  `is_family_annual_income` int(20) DEFAULT 1,
  `reason` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `is_scholar_applicant_family_details`
--

INSERT INTO `is_scholar_applicant_family_details` (`id`, `id_scholarship_program`, `is_father_name`, `is_mother_name`, `is_father_deceased`, `is_father_occupation`, `is_no_siblings`, `is_est_fee`, `is_family_annual_income`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, '', 0, NULL, '2020-07-22 02:12:09', NULL, '2020-07-22 02:12:09'),
(2, 2, 1, 1, 1, 1, 1, 1, 1, '', 0, NULL, '2020-07-24 13:19:07', NULL, '2020-07-24 13:19:07'),
(3, 3, 1, 1, 1, 1, 1, 1, 1, '', 0, NULL, '2020-07-24 23:42:06', NULL, '2020-07-24 23:42:06'),
(4, 4, 1, 1, 1, 1, 1, 1, 1, '', 0, NULL, '2020-09-16 10:29:40', NULL, '2020-09-16 10:29:40'),
(5, 5, 1, 1, 1, 1, 1, 1, 1, '', 0, NULL, '2020-09-16 10:47:49', NULL, '2020-09-16 10:47:49'),
(6, 6, 1, 1, 1, 1, 1, 1, 1, '', 0, NULL, '2020-09-16 10:49:41', NULL, '2020-09-16 10:49:41'),
(7, 7, 1, 1, 1, 1, 1, 1, 1, '', 0, NULL, '2020-09-16 10:51:07', NULL, '2020-09-16 10:51:07'),
(8, 8, 1, 1, 1, 1, 1, 1, 1, '', 0, NULL, '2020-09-16 10:51:55', NULL, '2020-09-16 10:51:55'),
(9, 9, 1, 1, 1, 1, 1, 1, 1, '', 0, NULL, '2020-09-16 10:52:44', NULL, '2020-09-16 10:52:44'),
(10, 10, 1, 1, 1, 1, 1, 1, 1, '', 0, NULL, '2020-09-16 10:58:50', NULL, '2020-09-16 10:58:50'),
(11, 11, 1, 1, 1, 1, 1, 1, 1, '', 0, NULL, '2020-09-16 11:00:27', NULL, '2020-09-16 11:00:27');

-- --------------------------------------------------------

--
-- Table structure for table `main_invoice`
--

CREATE TABLE `main_invoice` (
  `id` int(20) NOT NULL,
  `type` varchar(50) DEFAULT '',
  `id_student` int(20) DEFAULT 0,
  `id_cohert` int(20) DEFAULT 0,
  `id_scholarship` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `invoice_number` varchar(50) DEFAULT '',
  `type_of_invoice` varchar(50) DEFAULT '',
  `date_time` datetime DEFAULT current_timestamp(),
  `remarks` varchar(520) DEFAULT '',
  `id_partner_university` int(20) DEFAULT 0,
  `currency` varchar(200) DEFAULT '',
  `total_amount` float(20,2) DEFAULT 0.00,
  `invoice_total` float(20,2) DEFAULT 0.00,
  `total_discount` float(20,2) DEFAULT 0.00,
  `balance_amount` float(20,2) DEFAULT 0.00,
  `paid_amount` float(20,2) DEFAULT 0.00,
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `main_invoice_details`
--

CREATE TABLE `main_invoice_details` (
  `id` int(20) NOT NULL,
  `id_main_invoice` int(20) DEFAULT NULL,
  `id_fee_item` int(20) DEFAULT NULL,
  `amount` float(20,2) DEFAULT NULL,
  `quantity` int(20) DEFAULT 0,
  `price` int(20) DEFAULT 0,
  `id_reference` int(20) DEFAULT 0,
  `description` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `marital_status_setup`
--

CREATE TABLE `marital_status_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `sequence` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marital_status_setup`
--

INSERT INTO `marital_status_setup` (`id`, `name`, `sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Married', 0, 1, NULL, '2020-09-15 08:13:43', NULL, '2020-09-15 08:13:43'),
(2, 'Single', 0, 1, NULL, '2020-09-16 09:25:07', NULL, '2020-09-16 09:25:07'),
(3, 'Divorced', 0, 1, NULL, '2020-09-16 09:25:16', NULL, '2020-09-16 09:25:16'),
(4, 'Single Parent', 0, 1, NULL, '2020-09-16 09:25:33', NULL, '2020-09-16 09:25:33');

-- --------------------------------------------------------

--
-- Table structure for table `program_has_certification`
--

CREATE TABLE `program_has_certification` (
  `id` int(20) NOT NULL,
  `id_program` int(20) DEFAULT NULL,
  `name` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program_has_certification`
--

INSERT INTO `program_has_certification` (`id`, `id_program`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 5, 'Certificate in ', NULL, NULL, '2020-09-17 02:31:58', NULL, '2020-09-17 02:31:58');

-- --------------------------------------------------------

--
-- Table structure for table `repayment`
--

CREATE TABLE `repayment` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT 0,
  `id_cohert` int(20) DEFAULT 0,
  `id_scholarship` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0.00,
  `currency` varchar(200) DEFAULT '',
  `payment_type` varchar(200) DEFAULT '',
  `installment_amount` float(20,2) DEFAULT 0.00,
  `day` int(20) DEFAULT 0,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `repayment_extension`
--

CREATE TABLE `repayment_extension` (
  `id` int(20) NOT NULL,
  `id_repayment` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT 0,
  `additionnal_months` int(20) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `old_end_date` datetime DEFAULT NULL,
  `new_end_date` datetime DEFAULT NULL,
  `installment_amount` float(20,2) DEFAULT 0.00,
  `file_one` varchar(512) DEFAULT '',
  `file_two` varchar(512) DEFAULT '',
  `file_three` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(20) NOT NULL,
  `role` varchar(250) DEFAULT '',
  `status` int(20) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salutation_setup`
--

CREATE TABLE `salutation_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `sequence` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scheme_has_program`
--

CREATE TABLE `scheme_has_program` (
  `id` int(20) NOT NULL,
  `id_scholarship_scheme` bigint(20) DEFAULT 0,
  `id_program` bigint(20) DEFAULT 0,
  `id_sub_thrust` int(20) DEFAULT 0,
  `quota` int(20) DEFAULT 0,
  `budget` float(20,2) DEFAULT 0.00,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scheme_has_program`
--

INSERT INTO `scheme_has_program` (`id`, `id_scholarship_scheme`, `id_program`, `id_sub_thrust`, `quota`, `budget`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(7, 2, 1, 3, 50, 1000000.00, 0, NULL, '2020-09-20 18:31:02', NULL, '2020-09-20 18:31:02');

-- --------------------------------------------------------

--
-- Table structure for table `scholar`
--

CREATE TABLE `scholar` (
  `id` int(11) NOT NULL,
  `email` varchar(1024) NOT NULL COMMENT 'login email',
  `password` varchar(1024) NOT NULL COMMENT 'hashed login password',
  `salutation` varchar(50) DEFAULT '',
  `name` varchar(1024) DEFAULT NULL COMMENT 'full name of user',
  `first_name` varchar(1024) DEFAULT '',
  `last_name` varchar(1024) DEFAULT '',
  `mobile` varchar(20) DEFAULT NULL,
  `id_role` tinyint(4) NOT NULL,
  `status` int(20) DEFAULT 0,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scholar`
--

INSERT INTO `scholar` (`id`, `email`, `password`, `salutation`, `name`, `first_name`, `last_name`, `mobile`, `id_role`, `status`, `is_deleted`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'vk@scholar.com', '7fef6171469e80d32c0559f88b377245', '1', 'Mr. Vinay Kiran', 'Vinay', 'Kiran', '8888888888', 1, 1, 0, 1, '2020-07-29 00:00:00', 1, '2020-07-23 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_accreditation_category`
--

CREATE TABLE `scholarship_accreditation_category` (
  `id` int(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_accreditation_category`
--

INSERT INTO `scholarship_accreditation_category` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Jabatan Perkhidmatan Awam', 'JPA', '', 1, NULL, '2020-07-27 12:16:07', NULL, '2020-07-27 12:16:07'),
(2, 'Professional Certification', 'PRO', '', 1, NULL, '2020-09-16 09:21:06', NULL, '2020-09-16 09:21:06');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_accreditation_type`
--

CREATE TABLE `scholarship_accreditation_type` (
  `id` int(20) NOT NULL,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_accreditation_type`
--

INSERT INTO `scholarship_accreditation_type` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Local', 'LOC', '', 1, NULL, '2020-07-27 12:16:28', NULL, '2020-07-27 12:16:28'),
(2, 'International', 'INT', '', 1, NULL, '2020-07-27 12:16:47', NULL, '2020-07-27 12:16:47');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_amount_calculation_type`
--

CREATE TABLE `scholarship_amount_calculation_type` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_amount_calculation_type`
--

INSERT INTO `scholarship_amount_calculation_type` (`id`, `name`, `name_optional_language`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'FIX AMOUNT', 'FIX AMOUNT', 'FIX AMOUNT', 1, NULL, '2020-08-04 02:31:54', NULL, '2020-08-04 02:31:54'),
(2, 'MONTHLY', 'MONTHLY', 'MONTHLY', 1, NULL, '2020-09-16 11:52:54', NULL, '2020-09-16 11:52:54');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_applicant`
--

CREATE TABLE `scholarship_applicant` (
  `id` int(20) NOT NULL,
  `id_scholarship_scheme` int(11) DEFAULT 0,
  `id_scholar_applicant` int(11) DEFAULT 0,
  `id_scholarship` int(20) DEFAULT 0,
  `id_application` int(20) DEFAULT 0,
  `year` int(11) DEFAULT 0,
  `id_intake` int(11) DEFAULT NULL,
  `id_program` int(11) DEFAULT NULL,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `passport` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `is_hostel` int(20) DEFAULT 0,
  `id_degree_type` int(20) DEFAULT 0,
  `id_race` varchar(120) DEFAULT '',
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `mailing_zipcode` varchar(120) DEFAULT '',
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `sibbling_discount` varchar(50) DEFAULT '',
  `employee_discount` varchar(50) DEFAULT '',
  `applicant_status` varchar(50) DEFAULT 'Draft',
  `is_sibbling_discount` int(20) DEFAULT 3,
  `is_employee_discount` int(20) DEFAULT 3,
  `id_type` varchar(888) DEFAULT NULL,
  `passport_number` varchar(1024) DEFAULT NULL,
  `email_verified` int(2) DEFAULT 0,
  `is_submitted` int(2) DEFAULT 0,
  `is_updated` int(2) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `approved_by` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_applicant`
--

INSERT INTO `scholarship_applicant` (`id`, `id_scholarship_scheme`, `id_scholar_applicant`, `id_scholarship`, `id_application`, `year`, `id_intake`, `id_program`, `full_name`, `salutation`, `first_name`, `last_name`, `nric`, `passport`, `phone`, `email_id`, `password`, `passport_expiry_date`, `gender`, `date_of_birth`, `martial_status`, `religion`, `nationality`, `is_hostel`, `id_degree_type`, `id_race`, `mail_address1`, `mail_address2`, `mailing_country`, `mailing_state`, `mailing_city`, `mailing_zipcode`, `permanent_address1`, `permanent_address2`, `permanent_country`, `permanent_state`, `permanent_city`, `permanent_zipcode`, `sibbling_discount`, `employee_discount`, `applicant_status`, `is_sibbling_discount`, `is_employee_discount`, `id_type`, `passport_number`, `email_verified`, `is_submitted`, `is_updated`, `reason`, `status`, `approved_by`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 3, 3, 0, 0, 2020, NULL, 1, 'Mr. Testing testing', 'Mr', 'Testing', 'testing', '11111111111', '', '1231313123', 'testing1@gmail.com', '', '17-09-2020', 'Male', '2020-09-17', 'Married', '1', 'Malaysian', 0, 0, '1', 'address', 'address', 1, 1, 'city', '1231313', 'address two', 'two', 1, 1, 'city', '12323123', '', '', 'Draft', 3, 3, 'sdf', '12312311212', 0, 0, 0, '', NULL, 0, NULL, '2020-09-16 20:28:12', NULL, '2020-09-16 20:28:12'),
(2, 2, 7, 0, 1, 2020, NULL, 4, 'Mr. Michel Adam', 'Mr', 'Michel', 'Adam', 'MA888', '', '738738921', 'ma@cms.com', '', '05-09-2020', 'Male', '2020-09-04', 'Married', '1', 'Malaysian', 0, 0, '4', 'Kl', 'KL', 1, 1, 'KL', '23131', 'KL', 'KL', 1, 1, 'KL', '123', '', '', 'Draft', 3, 3, 'Passport', 'KR12321', 0, 0, 0, '', NULL, 0, NULL, '2020-09-18 14:55:07', NULL, '2020-09-18 14:55:07'),
(3, 2, 8, 1, 2, 2020, NULL, 1, 'Mr. aeu1 AEU1', 'Mr', 'aeu1', 'AEU1', '777778993377', '', '3423423', 'ab@gmail.com', '', '19-09-2020', 'Male', '2020-09-19', 'Single', '1', 'Malaysian', 0, 0, '1', 'a', 'a', 1, 1, 'a', '560064', 'Anjana Nagar, Sunkadakatte, Bengaluru, Karnataka 560091', 'a', 1, 1, 'a', '234', '', '', 'Draft', 3, 3, 'sdf', 'asdfas', 0, 0, 0, '', NULL, 0, NULL, '2020-09-19 07:34:21', NULL, '2020-09-19 07:34:21'),
(4, 1, 7, 0, 3, 2020, NULL, 0, 'Mr. Michel Adam', 'Mr', 'Michel', 'Adam', 'MA888', '', '123231', 'ma@cms.com', '', '21-09-2020', 'Male', '1970-01-01', 'Married', '1', 'Other', 0, 0, '4', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', '', 'Draft', 3, 3, '8787878', '87878787', 0, 0, 0, '', NULL, 0, NULL, '2020-09-20 20:50:20', NULL, '2020-09-20 20:50:20');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_application`
--

CREATE TABLE `scholarship_application` (
  `id` int(20) NOT NULL,
  `id_student` bigint(20) DEFAULT 0,
  `id_program` bigint(20) DEFAULT 0,
  `id_scholarship` int(20) DEFAULT 0,
  `id_intake` bigint(20) DEFAULT 0,
  `id_qualification` bigint(20) DEFAULT 0,
  `application_number` varchar(80) DEFAULT '',
  `id_scholarship_scheme` int(20) DEFAULT 0 COMMENT 'it''s a id_cohert after flow change',
  `id_fee_structure` int(11) DEFAULT 0,
  `id_scholar_applicant` int(20) DEFAULT 0,
  `father_name` varchar(1024) DEFAULT '',
  `mother_name` varchar(1024) DEFAULT '',
  `father_deceased` bigint(20) DEFAULT 0,
  `father_occupation` varchar(1024) DEFAULT '',
  `no_siblings` bigint(20) DEFAULT 0,
  `year` bigint(20) DEFAULT 0,
  `result_item` varchar(500) DEFAULT '',
  `est_fee` float(20,2) DEFAULT 0.00,
  `max_marks` bigint(20) DEFAULT 0,
  `obtained_marks` bigint(20) DEFAULT 0,
  `percentage` float(20,2) DEFAULT 0.00,
  `reason` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `is_submitted` int(2) DEFAULT 0,
  `is_migrated` int(20) DEFAULT 0,
  `submitted_date` datetime DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_application`
--

INSERT INTO `scholarship_application` (`id`, `id_student`, `id_program`, `id_scholarship`, `id_intake`, `id_qualification`, `application_number`, `id_scholarship_scheme`, `id_fee_structure`, `id_scholar_applicant`, `father_name`, `mother_name`, `father_deceased`, `father_occupation`, `no_siblings`, `year`, `result_item`, `est_fee`, `max_marks`, `obtained_marks`, `percentage`, `reason`, `status`, `is_submitted`, `is_migrated`, `submitted_date`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 7, 4, 0, 0, 0, 'SCH000001/2020', 2, 0, 0, '', '', 0, '', 0, 2020, '', 0.00, 0, 0, 0.00, '', 8, 1, 5, '2020-09-19 03:34:16', 1, '2020-09-18 14:55:07', NULL, '2020-09-18 14:55:07'),
(2, 8, 1, 1, 0, 0, 'SCH000002/2020', 2, 0, 0, '', '', 0, '', 0, 2020, '', 0.00, 0, 0, 0.00, '', 2, 1, 0, '2020-09-19 08:18:01', 1, '2020-09-19 07:34:21', NULL, '2020-09-19 07:34:21'),
(3, 7, 0, 0, 0, 0, 'SCH000003/2020', 1, 0, 0, '', '', 0, '', 0, 2020, '', 0.00, 0, 0, 0.00, '', 0, 0, 0, NULL, NULL, '2020-09-20 20:50:20', NULL, '2020-09-20 20:50:20');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_assesment_method_setup`
--

CREATE TABLE `scholarship_assesment_method_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `type` float(20,2) DEFAULT 0.00,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_assesment_method_setup`
--

INSERT INTO `scholarship_assesment_method_setup` (`id`, `name`, `type`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Easy', 1.00, 1, NULL, '2020-09-15 08:23:19', NULL, '2020-09-15 08:23:19');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_award_setup`
--

CREATE TABLE `scholarship_award_setup` (
  `id` int(20) NOT NULL,
  `code` varchar(200) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `level` int(20) DEFAULT NULL,
  `description` varchar(500) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_award_setup`
--

INSERT INTO `scholarship_award_setup` (`id`, `code`, `name`, `level`, `description`, `id_programme`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'COA', 'Certificate of Attendance', 1, 'Certificate of Attendance', NULL, 1, NULL, '2020-07-24 05:18:21', NULL, '2020-07-24 05:18:21'),
(2, 'CER', 'Certificate', 2, 'Certificate of Passing', NULL, 1, NULL, '2020-07-24 05:18:44', NULL, '2020-07-24 05:18:44'),
(3, 'DIP', 'Diploma', 3, 'Diploma', NULL, 1, NULL, '2020-07-24 05:19:08', NULL, '2020-07-24 05:19:08'),
(4, 'DEG', 'Degree', 4, 'Degree', NULL, 1, NULL, '2020-07-24 05:19:27', NULL, '2020-07-24 05:19:27'),
(5, 'PRO', 'Chartered', 1, 'Chartered', NULL, 1, NULL, '2020-07-24 05:20:46', NULL, '2020-07-24 05:20:46'),
(6, 'CA', 'Certified Analyst', 2, 'Certified Analyst', NULL, 1, NULL, '2020-09-16 10:27:49', NULL, '2020-09-16 10:27:49');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_bank_registration`
--

CREATE TABLE `scholarship_bank_registration` (
  `id` int(20) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `bank_id` varchar(50) DEFAULT '',
  `address` varchar(580) DEFAULT '',
  `landmark` varchar(580) DEFAULT '',
  `city` varchar(580) DEFAULT '',
  `id_state` int(20) DEFAULT NULL,
  `id_country` int(20) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `cr_fund` varchar(50) DEFAULT '',
  `cr_department` varchar(50) DEFAULT '',
  `cr_activity` varchar(50) DEFAULT '',
  `cr_account` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_bank_registration`
--

INSERT INTO `scholarship_bank_registration` (`id`, `name`, `code`, `bank_id`, `address`, `landmark`, `city`, `id_state`, `id_country`, `zipcode`, `cr_fund`, `cr_department`, `cr_activity`, `cr_account`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'CIMB Bank', 'CIBBMYKLXXX', '32131', 'Jalan Raja Laut', 'KL', 'Kuala LUmpur', 2, 1, 50480, '', '', '', '', 1, NULL, '2020-09-15 08:52:05', NULL, '2020-09-15 08:52:05');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_country`
--

CREATE TABLE `scholarship_country` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(2) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(2) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_country`
--

INSERT INTO `scholarship_country` (`id`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Malaysia', 1, 0, '2020-07-22 01:33:31', 0, '2020-07-22 01:33:31');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_department`
--

CREATE TABLE `scholarship_department` (
  `id` int(20) NOT NULL,
  `name` varchar(200) DEFAULT '',
  `code` varchar(120) DEFAULT '',
  `description` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_department`
--

INSERT INTO `scholarship_department` (`id`, `name`, `code`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Finance Department', 'FIN', 'Finance Department', 1, NULL, '2020-07-22 01:41:36', NULL, '2020-07-22 01:41:36'),
(3, 'Programme and Scholarship Department', 'PSDM', 'Programme and Scholarship Department', 1, NULL, '2020-09-15 08:15:14', NULL, '2020-09-15 08:15:14'),
(4, 'Support Services', 'SUP', 'Support Services', 1, NULL, '2020-09-16 09:36:36', NULL, '2020-09-16 09:36:36');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_department_has_staff`
--

CREATE TABLE `scholarship_department_has_staff` (
  `id` int(20) NOT NULL,
  `id_department` int(20) DEFAULT 0,
  `id_staff` int(20) DEFAULT 0,
  `effective_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_department_has_staff`
--

INSERT INTO `scholarship_department_has_staff` (`id`, `id_department`, `id_staff`, `effective_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, '2019-09-09 00:00:00', NULL, NULL, '2020-07-22 03:09:34', NULL, '2020-07-22 03:09:34'),
(2, 3, 2, '2019-09-09 00:00:00', NULL, NULL, '2020-09-15 08:15:28', NULL, '2020-09-15 08:15:28'),
(3, 3, 2, '2020-09-01 00:00:00', NULL, NULL, '2020-09-16 09:34:50', NULL, '2020-09-16 09:34:50'),
(4, 3, 2, '2020-09-01 00:00:00', NULL, NULL, '2020-09-16 09:35:34', NULL, '2020-09-16 09:35:34');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_documents`
--

CREATE TABLE `scholarship_documents` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `description` varchar(500) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `id_programme` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_documents`
--

INSERT INTO `scholarship_documents` (`id`, `name`, `description`, `code`, `id_programme`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'NRIC', '', 'NRIC', NULL, 1, NULL, '2020-09-15 08:40:55', NULL, '2020-09-15 08:40:55');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_documents_for_applicant`
--

CREATE TABLE `scholarship_documents_for_applicant` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `name_in_malay` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `id_scholarship_program` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_documents_program`
--

CREATE TABLE `scholarship_documents_program` (
  `id` int(20) NOT NULL,
  `id_document` int(20) DEFAULT 0,
  `description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_documents_program`
--

INSERT INTO `scholarship_documents_program` (`id`, `id_document`, `description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Desc', 1, NULL, '2020-09-15 08:41:23', NULL, '2020-09-15 08:41:23');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_documents_program_details`
--

CREATE TABLE `scholarship_documents_program_details` (
  `id` int(20) NOT NULL,
  `id_documents_program` int(20) DEFAULT 0,
  `id_program` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_documents_program_details`
--

INSERT INTO `scholarship_documents_program_details` (`id`, `id_documents_program`, `id_program`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, NULL, NULL, '2020-09-15 08:41:22', NULL, '2020-09-15 08:41:22'),
(2, 1, 0, NULL, NULL, '2020-09-15 08:41:28', NULL, '2020-09-15 08:41:28'),
(3, 1, 0, NULL, NULL, '2020-09-17 03:50:34', NULL, '2020-09-17 03:50:34'),
(4, 1, 3, NULL, NULL, '2020-09-17 03:50:38', NULL, '2020-09-17 03:50:38');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_education_level`
--

CREATE TABLE `scholarship_education_level` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `short_name` varchar(1024) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_education_level`
--

INSERT INTO `scholarship_education_level` (`id`, `name`, `short_name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Ujian Pendidikan Sekolah Rendah', 'UPSR', '', 1, NULL, '2020-07-24 23:33:52', NULL, '2020-07-24 23:33:52'),
(2, 'Sijil Pelajaran Malaysia', 'SPM', '', 1, NULL, '2020-09-15 08:39:37', NULL, '2020-09-15 08:39:37'),
(3, 'Sijil Tinggi Pelajaran Malaysia', 'STPM', '', 1, NULL, '2020-09-16 09:53:53', NULL, '2020-09-16 09:53:53'),
(4, 'Asasi', 'Asasi', '', 1, NULL, '2020-09-16 09:54:17', NULL, '2020-09-16 09:54:17'),
(5, 'Matrikulasi', 'Matrikulasi', '', 1, NULL, '2020-09-16 09:54:34', NULL, '2020-09-16 09:54:34'),
(6, 'Diploma', 'Diploma', '', 1, NULL, '2020-09-16 09:54:57', NULL, '2020-09-16 09:54:57'),
(7, 'Tahun Pertama Diploma', 'Tahun Pertama Diploma', '', 1, NULL, '2020-09-16 09:55:23', NULL, '2020-09-16 09:55:23'),
(8, 'Tahun Pertama Ijazah Sarjana Muda', 'Tahun Pertama Ijazah Sarjana Muda', '', 1, NULL, '2020-09-16 09:55:54', NULL, '2020-09-16 09:55:54');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_examination_details`
--

CREATE TABLE `scholarship_examination_details` (
  `id` int(20) NOT NULL,
  `id_scholar_applicant` int(20) DEFAULT NULL,
  `id_scholarship_scheme` int(20) DEFAULT 0,
  `id_application` int(20) DEFAULT 0,
  `qualification_level` varchar(120) DEFAULT '',
  `degree_awarded` varchar(120) DEFAULT '',
  `specialization` varchar(120) DEFAULT '',
  `class_degree` varchar(120) DEFAULT '',
  `result` varchar(120) DEFAULT '',
  `year` varchar(20) DEFAULT '',
  `medium` varchar(120) DEFAULT '',
  `college_country` varchar(120) DEFAULT '',
  `college_name` varchar(120) DEFAULT '',
  `certificate` varchar(520) DEFAULT '',
  `transcript` varchar(520) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_examination_details`
--

INSERT INTO `scholarship_examination_details` (`id`, `id_scholar_applicant`, `id_scholarship_scheme`, `id_application`, `qualification_level`, `degree_awarded`, `specialization`, `class_degree`, `result`, `year`, `medium`, `college_country`, `college_name`, `certificate`, `transcript`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 3, 3, 0, 'asdf', 'asdf', 'asdf', 'asdf', '12', '2020', 'kj', 'kj', 'kj', NULL, NULL, NULL, NULL, '2020-09-16 20:29:02', NULL, '2020-09-16 20:29:02'),
(2, 7, 2, 1, 'Masters', 'Masters In Business Administration', 'Marketing', 'Ist Degree', '8.9', '2020', 'English', 'Malaysia', 'Malaya University', '983beaae9cec7e575e0d62e27741b174.png', '73c85ff3a85b1d8f90168b6ff28aee51.png', NULL, NULL, '2020-09-18 15:01:13', NULL, '2020-09-18 15:01:13');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_frequency_mode`
--

CREATE TABLE `scholarship_frequency_mode` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_frequency_mode`
--

INSERT INTO `scholarship_frequency_mode` (`id`, `name`, `name_optional_language`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Monthly', 'Monthly', 'M', 1, NULL, '2020-09-20 03:29:07', NULL, '2020-09-20 03:29:07'),
(2, 'ANNUALY', 'ANNUALY', 'A', 1, NULL, '2020-09-20 05:10:33', NULL, '2020-09-20 05:10:33'),
(3, 'ONCE', 'ONCE', 'o', 1, NULL, '2020-09-20 05:12:40', NULL, '2020-09-20 05:12:40');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_individual_entry_requirement`
--

CREATE TABLE `scholarship_individual_entry_requirement` (
  `id` int(20) NOT NULL,
  `id_program` int(20) DEFAULT 0,
  `age` int(20) DEFAULT 0,
  `education` int(20) DEFAULT 0,
  `work_experience` int(20) DEFAULT 0,
  `other` int(20) DEFAULT 0,
  `min_age` int(20) DEFAULT 0,
  `max_age` int(20) DEFAULT 0,
  `id_education_qualification` int(20) DEFAULT 0,
  `education_description` varchar(2048) DEFAULT '',
  `min_work_experience` int(20) DEFAULT 0,
  `id_work_specialisation` int(20) DEFAULT 0,
  `work_description` varchar(2048) DEFAULT '',
  `other_description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_individual_entry_requirement`
--

INSERT INTO `scholarship_individual_entry_requirement` (`id`, `id_program`, `age`, `education`, `work_experience`, `other`, `min_age`, `max_age`, `id_education_qualification`, `education_description`, `min_work_experience`, `id_work_specialisation`, `work_description`, `other_description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 0, 0, 1, 20, 30, 0, '', 0, 0, '', 'Must Graduated With Batchelor Degree', 1, NULL, '2020-07-28 07:07:24', NULL, '2020-07-28 07:07:24'),
(4, 1, 1, 0, 0, 0, 10, 88, 0, '', 0, 0, '', '', 1, NULL, '2020-07-30 12:54:23', NULL, '2020-07-30 12:54:23'),
(5, 4, 1, 1, 1, 0, 17, 40, 6, '', 2, 1, '', '', 1, NULL, '2020-09-16 10:34:50', NULL, '2020-09-16 10:34:50');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_module_type_setup`
--

CREATE TABLE `scholarship_module_type_setup` (
  `id` int(20) NOT NULL,
  `code` varchar(200) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_module_type_setup`
--

INSERT INTO `scholarship_module_type_setup` (`id`, `code`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'COM', 'Compulsory', 1, NULL, '2020-07-23 07:09:30', NULL, '2020-07-23 07:09:30'),
(2, 'ELE', 'Elective', 1, NULL, '2020-07-23 18:09:01', NULL, '2020-07-23 18:09:01'),
(3, 'MQA', 'MQA', 1, NULL, '2020-07-23 18:09:15', NULL, '2020-07-23 18:09:15'),
(4, 'UNI', 'University Module', 1, NULL, '2020-07-23 18:09:30', NULL, '2020-07-23 18:09:30');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_organisation_has_department`
--

CREATE TABLE `scholarship_organisation_has_department` (
  `id` int(20) NOT NULL,
  `id_organisation` int(20) DEFAULT 0,
  `id_department` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_organisation_has_department`
--

INSERT INTO `scholarship_organisation_has_department` (`id`, `id_organisation`, `id_department`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 1, 1, NULL, NULL, '2020-07-30 11:03:25', NULL, '2020-07-30 11:03:25'),
(3, 1, 3, NULL, NULL, '2020-09-15 08:17:54', NULL, '2020-09-15 08:17:54');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_organisation_setup`
--

CREATE TABLE `scholarship_organisation_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(500) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `short_name` varchar(500) DEFAULT '',
  `name_in_malay` varchar(500) DEFAULT '',
  `url` varchar(500) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_registrar` int(20) DEFAULT 0,
  `date_time` datetime DEFAULT current_timestamp(),
  `contact_number` int(20) DEFAULT 0,
  `email` varchar(200) DEFAULT '',
  `fax_number` varchar(50) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `id_state` int(20) DEFAULT 0,
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_organisation_setup`
--

INSERT INTO `scholarship_organisation_setup` (`id`, `name`, `code`, `short_name`, `name_in_malay`, `url`, `id_country`, `id_registrar`, `date_time`, `contact_number`, `email`, `fax_number`, `address1`, `address2`, `id_state`, `city`, `zipcode`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Yayasan Peneraju Pendidikan Bumiputera', 'YP', 'Yayasan Peneraju', 'Yayasan Peneraju', 'https://yayasanpeneraju.com.my/', 1, 0, '1970-01-01 00:00:00', 27279000, 'info@yayasanpeneraju.com.my', '12121', 'Tingkat 15-1, Mercu UEM', 'Jalan Stesen Sentral 5, KL Sentral', 2, 'Kuala Lumpur', 50470, 1, NULL, '2020-06-08 20:52:06', NULL, '2020-06-08 20:52:06');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_organisation_setup_comitee`
--

CREATE TABLE `scholarship_organisation_setup_comitee` (
  `id` int(20) NOT NULL,
  `id_organisation` int(20) DEFAULT 0,
  `role` varchar(50) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `nric` varchar(100) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_partner_university`
--

CREATE TABLE `scholarship_partner_university` (
  `id` int(20) NOT NULL,
  `name` varchar(500) DEFAULT '',
  `short_name` varchar(500) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `certificate` varchar(200) DEFAULT '',
  `name_optional_language` varchar(500) DEFAULT '',
  `url` varchar(500) DEFAULT '',
  `id_registrar` int(20) DEFAULT 0,
  `id_contact_person` int(20) DEFAULT 0,
  `date_time` datetime DEFAULT current_timestamp(),
  `contact_number` int(20) DEFAULT 0,
  `email` varchar(200) DEFAULT '',
  `fax` varchar(200) DEFAULT '',
  `login_id` varchar(1048) DEFAULT '',
  `password` varchar(1048) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `location` varchar(512) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_state` int(20) DEFAULT 0,
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_partner_university`
--

INSERT INTO `scholarship_partner_university` (`id`, `name`, `short_name`, `code`, `start_date`, `end_date`, `certificate`, `name_optional_language`, `url`, `id_registrar`, `id_contact_person`, `date_time`, `contact_number`, `email`, `fax`, `login_id`, `password`, `address1`, `address2`, `location`, `id_country`, `id_state`, `city`, `zipcode`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 'System & Skills Training Concept Sdn Bhd', 'SSTC', 'PP01', '1970-01-01 00:00:00', '1970-01-01 00:00:00', '', 'System & Skills Training Concept Sdn Bhd', 'https://yayasanpeneraju.com.my/peneraju-skil-iltizam-aat-advanced-diploma-in-accounting-with-cloud-accounting-certification/', 0, 2, '2020-07-22 10:32:16', 2147483647, 'vinaykiranp888@gmail.com', '5635623', 'SSTC', 'SSTC123', 'KL', '', 'Puchong', 1, 1, 'KL', 577522, 1, NULL, '2020-07-22 10:32:16', NULL, '2020-07-22 10:32:16'),
(3, 'Standford University', 'SU', 'Standford', '1970-01-01 00:00:00', '1970-01-01 00:00:00', '', '', 'su.edu.com', 0, 1, '2020-07-29 12:27:43', 2147483647, 'ubed@gmail.com', '1123', 'su', '123', '# 23, 23rd Main', 'KL', 'KL', 1, 2, 'KL', 223321, 1, NULL, '2020-07-29 12:27:43', NULL, '2020-07-29 12:27:43');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_partner_university_has_aggrement`
--

CREATE TABLE `scholarship_partner_university_has_aggrement` (
  `id` int(20) NOT NULL,
  `id_partner_university` int(20) DEFAULT 0,
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `id_program` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0.00,
  `file` varchar(500) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_partner_university_has_aggrement`
--

INSERT INTO `scholarship_partner_university_has_aggrement` (`id`, `id_partner_university`, `start_date`, `end_date`, `id_program`, `amount`, `file`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(4, 2, '2020-09-09 00:00:00', '2020-09-23 00:00:00', 0, 0.00, 'f212c1c3d85df98fce53c7b5f7e17428.png', 0, NULL, '2020-09-15 08:20:57', NULL, '2020-09-15 08:20:57'),
(6, 3, '2020-09-20 00:00:00', '2020-09-20 00:00:00', 2, 100.00, '0aa218ce84e260c8677fa714e17c1cd6.JPG', 0, NULL, '2020-09-20 03:19:46', NULL, '2020-09-20 03:19:46');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_partner_university_has_program`
--

CREATE TABLE `scholarship_partner_university_has_program` (
  `id` int(20) NOT NULL,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `completion_criteria` int(20) DEFAULT NULL,
  `completion_criteria_type` varchar(1024) DEFAULT '',
  `assesment_method` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_partner_university_has_program`
--

INSERT INTO `scholarship_partner_university_has_program` (`id`, `id_partner_university`, `id_program`, `completion_criteria`, `completion_criteria_type`, `assesment_method`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 2, 1, 20, 'Credit Hours', 'CGPA', 1, NULL, '2020-07-23 04:14:13', NULL, '2020-07-23 04:14:13'),
(2, 3, 2, 30, 'Credit Hours', 'CGPA', 1, NULL, '2020-07-29 12:38:23', NULL, '2020-07-29 12:38:23');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_partner_university_has_training_center`
--

CREATE TABLE `scholarship_partner_university_has_training_center` (
  `id` int(20) NOT NULL,
  `id_partner_university` int(20) DEFAULT 0,
  `name` varchar(500) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `complete_code` varchar(500) DEFAULT '',
  `id_contact_person` int(20) DEFAULT 0,
  `contact_number` int(20) DEFAULT 0,
  `email` varchar(200) DEFAULT '',
  `fax` varchar(200) DEFAULT '',
  `address1` varchar(1024) DEFAULT '',
  `address2` varchar(1024) DEFAULT '',
  `location` varchar(512) DEFAULT '',
  `id_country` int(20) DEFAULT 0,
  `id_state` int(20) DEFAULT 0,
  `city` varchar(512) DEFAULT '',
  `zipcode` int(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_partner_university_has_training_center`
--

INSERT INTO `scholarship_partner_university_has_training_center` (`id`, `id_partner_university`, `name`, `code`, `complete_code`, `id_contact_person`, `contact_number`, `email`, `fax`, `address1`, `address2`, `location`, `id_country`, `id_state`, `city`, `zipcode`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 2, 'Alamu 1', 'A1', '', 1, 2147483647, 'ubed@gmail.com', '323232', '# 23, 23rd Main', 'KL', 'Kalim Road', 1, 1, 'KL', 223321, 1, NULL, '2020-07-22 10:39:35', NULL, '2020-07-22 10:39:35');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_partner_university_program_has_apprenticeship`
--

CREATE TABLE `scholarship_partner_university_program_has_apprenticeship` (
  `id` int(20) NOT NULL,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program_detail` int(20) DEFAULT NULL,
  `duration` int(20) DEFAULT NULL,
  `duration_type` varchar(200) DEFAULT '',
  `id_apprenticeship_center` int(20) DEFAULT NULL,
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_partner_university_program_has_apprenticeship`
--

INSERT INTO `scholarship_partner_university_program_has_apprenticeship` (`id`, `id_partner_university`, `id_program_detail`, `duration`, `duration_type`, `id_apprenticeship_center`, `start_date`, `end_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 2, 1, 1, 'Month', 2, '2020-07-07 00:00:00', '2020-07-25 00:00:00', NULL, NULL, '2020-07-23 07:35:19', NULL, '2020-07-23 07:35:19');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_partner_university_program_has_internship`
--

CREATE TABLE `scholarship_partner_university_program_has_internship` (
  `id` int(20) NOT NULL,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program_detail` int(20) DEFAULT NULL,
  `duration` int(20) DEFAULT NULL,
  `duration_type` varchar(200) DEFAULT '',
  `id_internship_center` int(20) DEFAULT NULL,
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_partner_university_program_has_study_mode`
--

CREATE TABLE `scholarship_partner_university_program_has_study_mode` (
  `id` int(20) NOT NULL,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program_detail` int(20) DEFAULT NULL,
  `mode_of_study` varchar(200) DEFAULT '',
  `min_duration` int(20) DEFAULT NULL,
  `max_duration` int(20) DEFAULT NULL,
  `min_duration_type` varchar(200) DEFAULT '',
  `max_duration_type` varchar(200) DEFAULT '',
  `id_training_center` int(20) DEFAULT NULL,
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_partner_university_program_has_syllabus`
--

CREATE TABLE `scholarship_partner_university_program_has_syllabus` (
  `id` int(20) NOT NULL,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program_detail` int(20) DEFAULT NULL,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `credit` int(20) DEFAULT NULL,
  `credit_type` varchar(200) DEFAULT '',
  `id_module_type` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_payment_type`
--

CREATE TABLE `scholarship_payment_type` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_payment_type`
--

INSERT INTO `scholarship_payment_type` (`id`, `name`, `code`, `name_in_malay`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Cash', 'Cash', '', 1, NULL, '2020-09-16 11:54:23', NULL, '2020-09-16 11:54:23');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_programme`
--

CREATE TABLE `scholarship_programme` (
  `id` int(20) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(250) DEFAULT '',
  `id_award` int(10) DEFAULT 0,
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `code` varchar(50) DEFAULT '',
  `total_cr_hrs` varchar(20) DEFAULT '',
  `graduate_studies` varchar(100) DEFAULT '',
  `foundation` varchar(50) DEFAULT '',
  `is_personel_details` int(11) DEFAULT 1,
  `is_education_details` int(11) DEFAULT 1,
  `is_family_details` int(11) DEFAULT 1,
  `is_financial_recoverable_details` int(11) DEFAULT 1,
  `is_file_upload_details` int(11) DEFAULT 1,
  `is_employment_details` int(11) DEFAULT 1,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp(),
  `id_user` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_programme`
--

INSERT INTO `scholarship_programme` (`id`, `name`, `name_optional_language`, `id_award`, `start_date`, `end_date`, `code`, `total_cr_hrs`, `graduate_studies`, `foundation`, `is_personel_details`, `is_education_details`, `is_family_details`, `is_financial_recoverable_details`, `is_file_upload_details`, `is_employment_details`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `id_user`) VALUES
(1, 'Peneraju Skil Iltizam Fabrikasi Logam', 'Peneraju Skil Iltizam Fabrikasi Logam', 2, '2020-07-22 00:00:00', '2099-09-30 00:00:00', 'PSI01', '', '', '', 1, 1, 1, 1, 1, 1, 1, 1, '2020-07-22 02:12:09', NULL, '2020-07-22 02:12:09', NULL),
(2, 'Peneraju Skil Iltizam Juruteknik Jaminan Kualiti', 'Peneraju Skil Iltizam Juruteknik Jaminan Kualiti', 2, '2020-07-01 00:00:00', '2020-12-31 00:00:00', 'PSI02', '', '', '', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-07-24 13:19:07', NULL, '2020-07-24 13:19:07', NULL),
(3, 'Financial Risk Manager', 'Financial Risk Manager', 2, '2020-07-10 00:00:00', '2099-09-30 00:00:00', 'FRM', '', '', '', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-07-24 23:42:06', NULL, '2020-07-24 23:42:06', NULL),
(4, 'Kursus Analisis Big Data & Pembangunan Modal Insan', 'Kursus Analisis Big Data & Pembangunan Modal Insan', 6, '2020-09-01 00:00:00', '2099-09-30 00:00:00', 'PPDS01', '', '', '', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-09-16 10:29:40', NULL, '2020-09-16 10:29:40', NULL),
(5, 'Chartered Financial Analyst', 'Chartered Financial Analyst', 6, '2013-09-01 00:00:00', '2099-09-30 00:00:00', 'CFA', '', '', '', 0, 0, 1, 1, 1, 1, 1, NULL, '2020-09-16 10:47:49', NULL, '2020-09-16 10:47:49', 1),
(6, 'Peneraju Profesional Akauntan Muda Kategori A (CAT - ACCA)', 'Peneraju Profesional Akauntan Muda Kategori A (CAT - ACCA)', 2, '2020-09-01 00:00:00', '2099-09-30 00:00:00', 'CAT', '', '', '', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-09-16 10:49:41', NULL, '2020-09-16 10:49:41', NULL),
(7, 'Peneraju Profesional Akauntan Muda Kategori B (ACCA)', 'Peneraju Profesional Akauntan Muda Kategori B (ACCA)', 5, '2020-09-01 00:00:00', '2099-09-30 00:00:00', 'ACCA-B', '', '', '', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-09-16 10:51:07', NULL, '2020-09-16 10:51:07', NULL),
(8, 'Peneraju Profesional Akauntan Siswazah (ACCA)', 'Peneraju Profesional Akauntan Siswazah (ACCA)', 5, '2020-09-01 00:00:00', '2099-09-30 00:00:00', 'ACCA', '', '', '', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-09-16 10:51:55', NULL, '2020-09-16 10:51:55', NULL),
(9, 'Peneraju Profesional Akauntan Siswazah (MICPA - CAANZ)', 'Peneraju Profesional Akauntan Siswazah (MICPA - CAANZ)', 5, '2020-09-01 00:00:00', '2099-09-30 00:00:00', 'MICPA - CAANZ', '', '', '', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-09-16 10:52:44', NULL, '2020-09-16 10:52:44', NULL),
(10, 'Peneraju Skil Iltizam Automotive Electrician Technician', 'Peneraju Skil Iltizam Automotive Electrician Technician', 2, '2020-09-01 00:00:00', '2099-09-30 00:00:00', 'PSI03', '', '', '', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-09-16 10:58:50', NULL, '2020-09-16 10:58:50', NULL),
(11, 'Peneraju Skil Iltizam AAT Advanced Diploma In Accounting With Cloud Accounting Certification', 'Peneraju Skil Iltizam AAT Advanced Diploma In Accounting With Cloud Accounting Certification', 2, '2020-09-01 00:00:00', '2099-09-30 00:00:00', 'PSI04', '', '', '', 1, 1, 1, 1, 1, 1, 1, NULL, '2020-09-16 11:00:27', NULL, '2020-09-16 11:00:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_program_accreditation`
--

CREATE TABLE `scholarship_program_accreditation` (
  `id` int(20) NOT NULL,
  `id_accreditation_category` int(20) DEFAULT NULL,
  `id_accreditation_type` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT 0,
  `date_time` datetime DEFAULT current_timestamp(),
  `valid_from` datetime DEFAULT current_timestamp(),
  `valid_to` datetime DEFAULT current_timestamp(),
  `approval_date` datetime DEFAULT current_timestamp(),
  `accreditation_number` varchar(2048) DEFAULT '',
  `reference_number` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_program_accreditation`
--

INSERT INTO `scholarship_program_accreditation` (`id`, `id_accreditation_category`, `id_accreditation_type`, `id_program`, `date_time`, `valid_from`, `valid_to`, `approval_date`, `accreditation_number`, `reference_number`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 5, '2021-07-08 00:00:00', '2020-07-08 00:00:00', '2021-07-08 00:00:00', '2020-09-30 00:00:00', '123', 'REFE123', 1, NULL, '2020-07-27 12:27:46', NULL, '2020-07-27 12:27:46');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_program_partner`
--

CREATE TABLE `scholarship_program_partner` (
  `id` int(20) NOT NULL,
  `id_thrust` int(20) DEFAULT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_location` int(20) DEFAULT NULL,
  `internship` int(20) DEFAULT NULL,
  `apprenticeship` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_program_partner`
--

INSERT INTO `scholarship_program_partner` (`id`, `id_thrust`, `id_sub_thrust`, `id_program`, `id_partner_university`, `id_location`, `internship`, `apprenticeship`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 1, 3, 1, 6, 6, 1, NULL, '2020-07-27 12:17:38', NULL, '2020-07-27 12:17:38');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_program_syllabus`
--

CREATE TABLE `scholarship_program_syllabus` (
  `id` int(20) NOT NULL,
  `id_thrust` int(20) DEFAULT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_program_syllabus`
--

INSERT INTO `scholarship_program_syllabus` (`id`, `id_thrust`, `id_sub_thrust`, `id_program`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 3, 1, NULL, '2020-07-27 12:25:21', NULL, '2020-07-27 12:25:21');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_program_syllabus_has_module`
--

CREATE TABLE `scholarship_program_syllabus_has_module` (
  `id` int(20) NOT NULL,
  `id_program_syllabus` int(20) DEFAULT NULL,
  `name` varchar(2048) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `credit_hours` int(20) DEFAULT 0,
  `type` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_program_syllabus_has_module`
--

INSERT INTO `scholarship_program_syllabus_has_module` (`id`, `id_program_syllabus`, `name`, `code`, `credit_hours`, `type`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Module One', 'MOD-1', 28, 'Compulsary', NULL, NULL, '2020-07-27 12:25:45', NULL, '2020-07-27 12:25:45'),
(4, 1, 'Module Two', 'MOD-2', 12, 'Compulsary', NULL, NULL, '2020-09-15 08:37:26', NULL, '2020-09-15 08:37:26');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_race_setup`
--

CREATE TABLE `scholarship_race_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_race_setup`
--

INSERT INTO `scholarship_race_setup` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Bumiputera - Malay', 'BM', 1, NULL, '2020-07-21 23:48:32', NULL, '2020-07-21 23:48:32'),
(2, 'Bumiputera - Sabah', 'BS', 1, NULL, '2020-07-21 23:48:50', NULL, '2020-07-21 23:48:50'),
(3, 'Bumiputera - Sarawak', 'BSK', 1, NULL, '2020-07-21 23:49:12', NULL, '2020-07-21 23:49:12'),
(4, 'Bumiputera - Orang Asli', 'BOA', 1, NULL, '2020-07-21 23:49:26', NULL, '2020-07-21 23:49:26');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_religion_setup`
--

CREATE TABLE `scholarship_religion_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_religion_setup`
--

INSERT INTO `scholarship_religion_setup` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Islam', '', 1, NULL, '2020-07-21 23:49:53', NULL, '2020-07-21 23:49:53'),
(2, 'Christian', '', 1, NULL, '2020-07-21 23:50:05', NULL, '2020-07-21 23:50:05'),
(3, 'No Religion', '', 1, NULL, '2020-07-21 23:51:02', NULL, '2020-07-21 23:51:02');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_salutation_setup`
--

CREATE TABLE `scholarship_salutation_setup` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `sequence` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_salutation_setup`
--

INSERT INTO `scholarship_salutation_setup` (`id`, `name`, `sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Mr', 1, 1, NULL, '2020-07-22 01:31:06', NULL, '2020-07-22 01:31:06'),
(2, 'Ms.', 0, 1, NULL, '2020-09-15 07:51:00', NULL, '2020-09-15 07:51:00'),
(3, 'Mrs', 0, 1, NULL, '2020-09-16 09:19:55', NULL, '2020-09-16 09:19:55'),
(4, 'DR', 0, 1, NULL, '2020-09-19 07:32:00', NULL, '2020-09-19 07:32:00'),
(5, 'Datin', 0, 1, NULL, '2020-09-19 07:32:04', NULL, '2020-09-19 07:32:04');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_scheme`
--

CREATE TABLE `scholarship_scheme` (
  `id` int(20) NOT NULL,
  `name` varchar(1048) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `percentage` int(10) DEFAULT 0,
  `from_dt` date DEFAULT NULL,
  `to_dt` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_scheme`
--

INSERT INTO `scholarship_scheme` (`id`, `name`, `code`, `percentage`, `from_dt`, `to_dt`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'October 2020 Peneraju Skil', '2020-10-PS', 0, '2020-10-01', '2020-11-30', 1, NULL, '2020-07-29 12:41:35', NULL, '2020-07-29 12:41:35'),
(2, 'September 2020 Peneraju Professional', '2020-09-PP', 0, '2020-09-01', '2020-12-30', 1, NULL, '2020-09-15 08:42:02', NULL, '2020-09-15 08:42:02'),
(3, 'October 2020 Peneraju Skill Iltizam', '2020-10-PI', 0, '2020-10-01', '2020-12-30', 1, NULL, '2020-09-15 08:43:25', NULL, '2020-09-15 08:43:25'),
(4, 'March 2020 Peneraju TunasPotensi', '2020-03-PT', 0, '2020-03-01', '2020-05-30', 1, NULL, '2020-09-19 02:52:58', NULL, '2020-09-19 02:52:58');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_selection_type`
--

CREATE TABLE `scholarship_selection_type` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `calculation_method` varchar(200) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_selection_type`
--

INSERT INTO `scholarship_selection_type` (`id`, `name`, `calculation_method`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Interview', 'Pass/Fail', '', 1, NULL, '2020-07-24 05:15:26', NULL, '2020-07-24 05:15:26'),
(2, 'Interview', 'Mark', '', 1, NULL, '2020-07-24 05:16:12', NULL, '2020-07-24 05:16:12');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_state`
--

CREATE TABLE `scholarship_state` (
  `id` int(20) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `id_country` int(10) DEFAULT NULL,
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_state`
--

INSERT INTO `scholarship_state` (`id`, `name`, `id_country`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Kedah', 1, 1, 1, NULL, NULL, '2020-07-22 01:34:22'),
(2, 'KL', 1, 1, 1, NULL, NULL, '2020-07-22 01:35:37');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_sub_thrust`
--

CREATE TABLE `scholarship_sub_thrust` (
  `id` int(20) NOT NULL,
  `id_thrust` int(20) DEFAULT 0,
  `id_scholarship_manager` int(20) DEFAULT 0,
  `scholarship_name` varchar(1024) DEFAULT '',
  `scholarship_short_name` varchar(1024) DEFAULT '',
  `scholarship_code` varchar(200) DEFAULT '',
  `scholarship_name_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_sub_thrust`
--

INSERT INTO `scholarship_sub_thrust` (`id`, `id_thrust`, `id_scholarship_manager`, `scholarship_name`, `scholarship_short_name`, `scholarship_code`, `scholarship_name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 0, 'Peneraju Tunas Geliga', 'Peneraju Tunas Geliga', 'PTG', '', 1, NULL, '2020-07-24 09:31:18', NULL, '2020-07-24 09:31:18'),
(2, 1, 0, 'Peneraju Tunas Potensi', 'Peneraju Tunas Potensi', 'PTP', '', 1, NULL, '2020-09-16 10:06:20', NULL, '2020-09-16 10:06:20'),
(3, 2, 0, 'Peneraju Skil Iltizam', 'Peneraju Skil Iltizam', 'PSI', '', 1, NULL, '2020-09-16 10:20:52', NULL, '2020-09-16 10:20:52'),
(4, 2, 0, 'Peneraju Skil', 'Peneraju Skil', 'PS', '', 1, NULL, '2020-09-16 10:22:03', NULL, '2020-09-16 10:22:03'),
(5, 3, 0, 'Peneraju Profesional Data Scientist', 'Peneraju Profesional Data Scientist', 'PPDS', '', 1, NULL, '2020-09-16 10:22:43', NULL, '2020-09-16 10:22:43'),
(6, 3, 0, 'Peneraju Profesional Financial Risk Manager (FRM®)', 'Peneraju Profesional Financial Risk Manager (FRM®)', 'PPFRM', '', 1, NULL, '2020-09-16 10:23:38', NULL, '2020-09-16 10:23:38'),
(7, 3, 0, 'Peneraju Profesional Chartered Financial Analyst® (CFA®)', 'Peneraju Profesional Chartered Financial Analyst® (CFA®)', 'PPCFA', '', 1, NULL, '2020-09-16 10:24:21', NULL, '2020-09-16 10:24:21'),
(8, 3, 0, 'Program Pensijilan Profesional Perakaunan', 'Program Pensijilan Profesional Perakaunan', 'PPPP', '', 1, NULL, '2020-09-16 10:24:48', NULL, '2020-09-16 10:24:48');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_sub_thrust_has_entry_requirement`
--

CREATE TABLE `scholarship_sub_thrust_has_entry_requirement` (
  `id` int(20) NOT NULL,
  `id_sub_thrust` int(20) DEFAULT 0,
  `nationality` varchar(200) DEFAULT '',
  `id_race` text DEFAULT NULL,
  `age` int(20) DEFAULT 0,
  `education` int(20) DEFAULT 0,
  `income` int(20) DEFAULT 0,
  `other` int(20) DEFAULT 0,
  `min_age` int(20) DEFAULT 0,
  `max_age` int(20) DEFAULT 0,
  `id_education_qualification` int(20) DEFAULT 0,
  `education_description` varchar(2048) DEFAULT '',
  `min_income` int(20) DEFAULT 0,
  `max_income` int(20) DEFAULT 0,
  `income_type` varchar(2048) DEFAULT '',
  `other_description` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_sub_thrust_has_entry_requirement`
--

INSERT INTO `scholarship_sub_thrust_has_entry_requirement` (`id`, `id_sub_thrust`, `nationality`, `id_race`, `age`, `education`, `income`, `other`, `min_age`, `max_age`, `id_education_qualification`, `education_description`, `min_income`, `max_income`, `income_type`, `other_description`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(7, 1, 'Malaysian', '1', 1, 1, 0, 0, 11, 13, 1, '4A', 0, 0, '', '', NULL, NULL, '2020-09-16 09:59:30', NULL, '2020-09-16 09:59:30'),
(8, 2, 'Malaysian', '1', 1, 1, 1, 0, 17, 25, 2, '8A', 24000, 60000, 'Household Income', '', NULL, NULL, '2020-09-16 10:08:10', NULL, '2020-09-16 10:08:10'),
(9, 7, 'Malaysian', '1,2', 1, 1, 1, 0, 19, 20, 6, '', 1231321, 121212121, 'Household Income', '', NULL, NULL, '2020-09-16 11:51:15', NULL, '2020-09-16 11:51:15');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_sub_thrust_has_program`
--

CREATE TABLE `scholarship_sub_thrust_has_program` (
  `id` int(20) NOT NULL,
  `id_partner_university` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_sub_thrust_has_program`
--

INSERT INTO `scholarship_sub_thrust_has_program` (`id`, `id_partner_university`, `id_program`, `id_sub_thrust`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(7, 2, 1, 1, NULL, NULL, '2020-07-26 18:40:01', NULL, '2020-07-26 18:40:01'),
(9, 3, 2, 1, NULL, NULL, '2020-07-29 12:39:33', NULL, '2020-07-29 12:39:33'),
(10, 2, 1, 1, NULL, NULL, '2020-07-29 12:39:57', NULL, '2020-07-29 12:39:57'),
(12, 2, 1, 3, NULL, NULL, '2020-09-20 18:27:49', NULL, '2020-09-20 18:27:49');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_sub_thrust_has_requirement`
--

CREATE TABLE `scholarship_sub_thrust_has_requirement` (
  `id` int(20) NOT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `requirement_entry` varchar(200) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_sub_thrust_has_requirement`
--

INSERT INTO `scholarship_sub_thrust_has_requirement` (`id`, `id_sub_thrust`, `id_program`, `requirement_entry`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 'Age', 1, NULL, '2020-07-24 12:18:47', NULL, '2020-07-24 12:18:47'),
(2, 1, 1, 'Education', 1, NULL, '2020-07-24 12:19:10', NULL, '2020-07-24 12:19:10'),
(3, 1, 1, 'Work Experience', 1, NULL, '2020-07-24 12:20:12', NULL, '2020-07-24 12:20:12'),
(4, 1, 1, 'Other Requirement', 1, NULL, '2020-07-24 12:20:49', NULL, '2020-07-24 12:20:49'),
(5, 1, 1, 'Age', 1, NULL, '2020-07-24 12:20:58', NULL, '2020-07-24 12:20:58'),
(6, 1, 1, 'Age', 1, NULL, '2020-07-24 13:20:46', NULL, '2020-07-24 13:20:46'),
(7, 1, 1, 'Education', 1, NULL, '2020-07-24 23:34:59', NULL, '2020-07-24 23:34:59'),
(8, 1, 2, 'Education', 1, NULL, '2020-07-24 23:34:59', NULL, '2020-07-24 23:34:59'),
(9, 1, 1, 'Work Experience', 1, NULL, '2020-07-24 23:35:48', NULL, '2020-07-24 23:35:48'),
(10, 1, 2, 'Work Experience', 1, NULL, '2020-07-24 23:35:48', NULL, '2020-07-24 23:35:48'),
(11, 1, 1, 'Age', 1, NULL, '2020-07-26 18:37:35', NULL, '2020-07-26 18:37:35'),
(12, 1, 3, 'Age', 1, NULL, '2020-07-26 18:37:35', NULL, '2020-07-26 18:37:35'),
(13, 1, 1, 'Education', 1, NULL, '2020-07-26 18:38:53', NULL, '2020-07-26 18:38:53'),
(14, 1, 3, 'Education', 1, NULL, '2020-07-26 18:38:53', NULL, '2020-07-26 18:38:53'),
(15, 1, 2, 'Education', 1, NULL, '2020-07-26 18:38:53', NULL, '2020-07-26 18:38:53');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_sub_thrust_requirement_has_age`
--

CREATE TABLE `scholarship_sub_thrust_requirement_has_age` (
  `id` int(20) NOT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_requirement` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `min_age` int(20) DEFAULT NULL,
  `max_age` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_sub_thrust_requirement_has_age`
--

INSERT INTO `scholarship_sub_thrust_requirement_has_age` (`id`, `id_sub_thrust`, `id_requirement`, `id_program`, `min_age`, `max_age`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(3, 1, 6, 1, 18, 22, NULL, NULL, '2020-07-24 13:20:46', NULL, '2020-07-24 13:20:46'),
(4, 1, 11, 1, 12, 12222, NULL, NULL, '2020-07-26 18:37:35', NULL, '2020-07-26 18:37:35'),
(5, 1, 12, 3, 12, 12222, NULL, NULL, '2020-07-26 18:37:35', NULL, '2020-07-26 18:37:35');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_sub_thrust_requirement_has_education`
--

CREATE TABLE `scholarship_sub_thrust_requirement_has_education` (
  `id` int(20) NOT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_requirement` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `id_qualification` int(20) DEFAULT NULL,
  `education_descrption` varchar(2048) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_sub_thrust_requirement_has_education`
--

INSERT INTO `scholarship_sub_thrust_requirement_has_education` (`id`, `id_sub_thrust`, `id_requirement`, `id_program`, `id_qualification`, `education_descrption`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 2, 1, 1, 'Education Compulsary', NULL, NULL, '2020-07-24 12:19:10', NULL, '2020-07-24 12:19:10'),
(2, 1, 7, 1, 1, 'Batchelor Degree Required', NULL, NULL, '2020-07-24 23:34:59', NULL, '2020-07-24 23:34:59'),
(3, 1, 8, 2, 1, 'Batchelor Degree Required', NULL, NULL, '2020-07-24 23:34:59', NULL, '2020-07-24 23:34:59'),
(4, 1, 13, 1, 1, '23', NULL, NULL, '2020-07-26 18:38:53', NULL, '2020-07-26 18:38:53'),
(5, 1, 14, 3, 1, '23', NULL, NULL, '2020-07-26 18:38:53', NULL, '2020-07-26 18:38:53'),
(6, 1, 15, 2, 1, '23', NULL, NULL, '2020-07-26 18:38:53', NULL, '2020-07-26 18:38:53');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_sub_thrust_requirement_has_other`
--

CREATE TABLE `scholarship_sub_thrust_requirement_has_other` (
  `id` int(20) NOT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_requirement` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `other_descrption` varchar(2048) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_sub_thrust_requirement_has_other`
--

INSERT INTO `scholarship_sub_thrust_requirement_has_other` (`id`, `id_sub_thrust`, `id_requirement`, `id_program`, `other_descrption`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 4, 1, 'Other Details i\'ll Be Updated Soon', NULL, NULL, '2020-07-24 12:20:49', NULL, '2020-07-24 12:20:49');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_sub_thrust_requirement_has_work_experience`
--

CREATE TABLE `scholarship_sub_thrust_requirement_has_work_experience` (
  `id` int(20) NOT NULL,
  `id_sub_thrust` int(20) DEFAULT NULL,
  `id_requirement` int(20) DEFAULT NULL,
  `id_program` int(20) DEFAULT NULL,
  `min_experience` int(20) DEFAULT NULL,
  `id_specialisation` int(20) DEFAULT NULL,
  `experience_descrption` varchar(2048) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_sub_thrust_requirement_has_work_experience`
--

INSERT INTO `scholarship_sub_thrust_requirement_has_work_experience` (`id`, `id_sub_thrust`, `id_requirement`, `id_program`, `min_experience`, `id_specialisation`, `experience_descrption`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 3, 1, 1, 1, 'Work Experience Compulsary If U Select This Option', NULL, NULL, '2020-07-24 12:20:12', NULL, '2020-07-24 12:20:12'),
(2, 1, 9, 1, 1, 1, 'Work Experience Reuired If Applying Under Experience', NULL, NULL, '2020-07-24 23:35:48', NULL, '2020-07-24 23:35:48'),
(3, 1, 10, 2, 1, 1, 'Work Experience Reuired If Applying Under Experience', NULL, NULL, '2020-07-24 23:35:48', NULL, '2020-07-24 23:35:48');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_thrust`
--

CREATE TABLE `scholarship_thrust` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `start_date` datetime DEFAULT current_timestamp(),
  `end_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_thrust`
--

INSERT INTO `scholarship_thrust` (`id`, `name`, `code`, `name_optional_language`, `start_date`, `end_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Peneraju Tunas', 'Tunas', '', '2020-07-13 00:00:00', '2099-07-03 00:00:00', 1, NULL, '2020-07-23 10:32:08', NULL, '2020-07-23 10:32:08'),
(2, 'Peneraju Skil', 'Skil', '', '2020-09-01 00:00:00', '2099-09-30 00:00:00', 1, NULL, '2020-09-16 09:40:01', NULL, '2020-09-16 09:40:01'),
(3, ' Peneraju Profesional', 'Profesional', '', '2020-09-01 00:00:00', '2099-09-30 00:00:00', 1, NULL, '2020-09-16 09:43:23', NULL, '2020-09-16 09:43:23');

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_users`
--

CREATE TABLE `scholarship_users` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `role_id` tinyint(4) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_dt_tm` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scholarship_users`
--

INSERT INTO `scholarship_users` (`id`, `email`, `password`, `name`, `mobile`, `role_id`, `is_deleted`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'vk@cms.com', 'admin', 'Vinay', '08550078787', 1, 0, 1, '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_work_specialisation`
--

CREATE TABLE `scholarship_work_specialisation` (
  `id` int(20) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(200) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholarship_work_specialisation`
--

INSERT INTO `scholarship_work_specialisation` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Work Specialisation', 'WS 01', '', 1, NULL, '2020-07-24 23:32:43', NULL, '2020-07-24 23:32:43');

-- --------------------------------------------------------

--
-- Table structure for table `scholar_applicant`
--

CREATE TABLE `scholar_applicant` (
  `id` int(20) NOT NULL,
  `id_intake` int(11) DEFAULT NULL,
  `id_program` int(11) DEFAULT NULL,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `passport` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `is_hostel` int(20) DEFAULT 0,
  `id_degree_type` int(20) DEFAULT 0,
  `id_race` varchar(120) DEFAULT '',
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `mailing_zipcode` varchar(120) DEFAULT '',
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `sibbling_discount` varchar(50) DEFAULT '',
  `employee_discount` varchar(50) DEFAULT '',
  `applicant_status` varchar(50) DEFAULT 'Draft',
  `is_sibbling_discount` int(20) DEFAULT 3,
  `is_employee_discount` int(20) DEFAULT 3,
  `id_type` varchar(888) DEFAULT NULL,
  `passport_number` varchar(1024) DEFAULT NULL,
  `email_verified` int(2) DEFAULT 0,
  `is_submitted` int(2) DEFAULT 0,
  `is_updated` int(2) DEFAULT 0,
  `reason` varchar(2048) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `approved_by` int(20) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholar_applicant`
--

INSERT INTO `scholar_applicant` (`id`, `id_intake`, `id_program`, `full_name`, `salutation`, `first_name`, `last_name`, `nric`, `passport`, `phone`, `email_id`, `password`, `passport_expiry_date`, `gender`, `date_of_birth`, `martial_status`, `religion`, `nationality`, `is_hostel`, `id_degree_type`, `id_race`, `mail_address1`, `mail_address2`, `mailing_country`, `mailing_state`, `mailing_city`, `mailing_zipcode`, `permanent_address1`, `permanent_address2`, `permanent_country`, `permanent_state`, `permanent_city`, `permanent_zipcode`, `sibbling_discount`, `employee_discount`, `applicant_status`, `is_sibbling_discount`, `is_employee_discount`, `id_type`, `passport_number`, `email_verified`, `is_submitted`, `is_updated`, `reason`, `status`, `approved_by`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, NULL, NULL, 'Miss. Sumangala K', 'Miss', 'Sumangala', 'K', 'NR333', '', '7878787878', 's@cms.com', '123', '', '', '', '', '', '', 0, 0, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', '', 'Draft', 3, 3, NULL, NULL, 1, 0, 0, '', NULL, 0, NULL, '2020-07-21 21:09:18', NULL, '2020-07-21 21:09:18'),
(2, NULL, NULL, 'Mr. Testing  user', 'Mr', 'Testing ', 'user', '1111111122233', '', '123123123123', 'test1@gmail.com', 'test1', '', '', '', '', '', '', 0, 0, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', '', 'Draft', 3, 3, NULL, NULL, 1, 0, 0, '', NULL, 0, NULL, '2020-09-16 08:55:31', NULL, '2020-09-16 08:55:31'),
(3, NULL, NULL, 'Mr. Testing testing', 'Mr', 'Testing', 'testing', '11111111111', '', '1231313123', 'testing1@gmail.com', 'testing1', '', '', '', '', '', '', 0, 0, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', '', 'Draft', 3, 3, NULL, NULL, 1, 0, 0, '', NULL, 0, NULL, '2020-09-16 20:26:02', NULL, '2020-09-16 20:26:02'),
(4, NULL, NULL, 'Miss. app a', 'Miss', 'app', 'a', 'aasda', '', '32312', 'ap@cms.com', '123', '', '', '', '', '', '', 0, 0, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', '', 'Draft', 3, 3, NULL, NULL, 1, 0, 0, '', NULL, 0, NULL, '2020-09-17 03:57:41', NULL, '2020-09-17 03:57:41'),
(5, NULL, NULL, 'Mr. kiran a', 'Mr', 'kiran', 'a', '109873456', '', '123232876', 'a@a.com', 'a', '', '', '', '', '', '', 0, 0, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', '', 'Draft', 3, 3, NULL, NULL, 1, 0, 0, '', NULL, 0, NULL, '2020-09-17 18:44:25', NULL, '2020-09-17 18:44:25'),
(6, NULL, NULL, 'Miss. One 1', 'Miss', 'One', '1', '123', '', '12232', 'a1@cms.com', '123', '', '', '', '', '', '', 0, 0, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', '', 'Draft', 3, 3, NULL, NULL, 1, 0, 0, '', NULL, 0, NULL, '2020-09-18 00:44:36', NULL, '2020-09-18 00:44:36'),
(7, NULL, NULL, 'Mr. Michel Adam', 'Mr', 'Michel', 'Adam', 'MA888', '', '123231', 'ma@cms.com', '202cb962ac59075b964b07152d234b70', '', '', '', '', '', '', 0, 0, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', '', 'Draft', 3, 3, NULL, NULL, 1, 0, 0, '', NULL, 0, NULL, '2020-09-18 14:52:40', NULL, '2020-09-18 14:52:40'),
(8, NULL, NULL, 'Mr. aeu1 AEU1', 'Mr', 'aeu1', 'AEU1', '777778993377', '', '7378347834783', 'ab@gmail.com', '187ef4436122d1cc2f40dc2b92f0eba0', '', '', '', '', '', '', 0, 0, '', '', '', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', '', 'Draft', 3, 3, NULL, NULL, 1, 0, 0, '', NULL, 0, NULL, '2020-09-19 07:31:12', NULL, '2020-09-19 07:31:12');

-- --------------------------------------------------------

--
-- Table structure for table `scholar_applicant_employment_status`
--

CREATE TABLE `scholar_applicant_employment_status` (
  `id` int(20) NOT NULL,
  `id_scholar_applicant` int(10) DEFAULT NULL,
  `id_scholarship_scheme` int(20) DEFAULT 0,
  `year` int(20) DEFAULT 0,
  `id_application` int(20) DEFAULT 0,
  `company_name` varchar(120) DEFAULT '',
  `company_address` varchar(120) DEFAULT '',
  `telephone` varchar(120) DEFAULT '',
  `fax_num` varchar(120) DEFAULT '',
  `designation` varchar(120) DEFAULT '',
  `position` varchar(120) DEFAULT '',
  `service_year` varchar(120) DEFAULT '',
  `industry` varchar(120) DEFAULT '',
  `job_desc` varchar(520) DEFAULT '',
  `employment_letter` varchar(520) DEFAULT '',
  `status` int(20) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholar_applicant_employment_status`
--

INSERT INTO `scholar_applicant_employment_status` (`id`, `id_scholar_applicant`, `id_scholarship_scheme`, `year`, `id_application`, `company_name`, `company_address`, `telephone`, `fax_num`, `designation`, `position`, `service_year`, `industry`, `job_desc`, `employment_letter`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 7, 2, 2020, 1, ' Anabela Ceramics', 'KL', '233132', '434131', 'Marketing Lead', 'Senior Executive', '2', 'Marketing', 'Marketing Excecutive', '3d4976daf325e5fa9091533cd1feacf7.jpeg', NULL, NULL, '2020-09-18 15:03:55', NULL, '2020-09-18 15:03:55');

-- --------------------------------------------------------

--
-- Table structure for table `scholar_applicant_family_details`
--

CREATE TABLE `scholar_applicant_family_details` (
  `id` int(20) NOT NULL,
  `id_scholar_applicant` bigint(20) DEFAULT 0,
  `id_application` int(20) DEFAULT 0,
  `id_scholarship_scheme` int(20) DEFAULT 0,
  `year` int(20) DEFAULT 0,
  `father_name` varchar(1024) DEFAULT '',
  `mother_name` varchar(1024) DEFAULT '',
  `father_deceased` bigint(20) DEFAULT 0,
  `father_occupation` varchar(1024) DEFAULT '',
  `no_siblings` bigint(20) DEFAULT 0,
  `est_fee` bigint(20) DEFAULT 0,
  `family_annual_income` float(20,2) NOT NULL DEFAULT 0.00,
  `reason` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholar_applicant_family_details`
--

INSERT INTO `scholar_applicant_family_details` (`id`, `id_scholar_applicant`, `id_application`, `id_scholarship_scheme`, `year`, `father_name`, `mother_name`, `father_deceased`, `father_occupation`, `no_siblings`, `est_fee`, `family_annual_income`, `reason`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 0, 0, 0, '', '', 0, '', 0, 0, 0.00, '', 0, NULL, '2020-07-21 21:09:18', NULL, '2020-07-21 21:09:18'),
(2, 2, 0, 0, 0, '', '', 0, '', 0, 0, 0.00, '', 0, NULL, '2020-09-16 08:55:31', NULL, '2020-09-16 08:55:31'),
(3, 3, 0, 0, 0, '', '', 0, '', 0, 0, 0.00, '', 0, NULL, '2020-09-16 20:26:02', NULL, '2020-09-16 20:26:02'),
(4, 4, 0, 0, 0, '', '', 0, '', 0, 0, 0.00, '', 0, NULL, '2020-09-17 03:57:41', NULL, '2020-09-17 03:57:41'),
(5, 5, 0, 0, 0, '', '', 0, '', 0, 0, 0.00, '', 0, NULL, '2020-09-17 18:44:25', NULL, '2020-09-17 18:44:25'),
(6, 6, 0, 0, 0, '', '', 0, '', 0, 0, 0.00, '', 0, NULL, '2020-09-18 00:44:36', NULL, '2020-09-18 00:44:36'),
(7, 7, 0, 0, 0, '', '', 0, '', 0, 0, 0.00, '', 0, NULL, '2020-09-18 14:52:40', NULL, '2020-09-18 14:52:40'),
(8, 7, 1, 2, 2020, 'Deorge Adam', 'Stephanie Adam', 0, 'Business', 2, 10000, 20000.00, '', 0, NULL, '2020-09-18 15:02:34', NULL, '2020-09-18 15:02:34'),
(9, 8, 0, 0, 0, '', '', 0, '', 0, 0, 0.00, '', 0, NULL, '2020-09-19 07:31:12', NULL, '2020-09-19 07:31:12');

-- --------------------------------------------------------

--
-- Table structure for table `scholar_applicant_last_login`
--

CREATE TABLE `scholar_applicant_last_login` (
  `id` bigint(20) NOT NULL,
  `id_scholar_applicant` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scholar_applicant_last_login`
--

INSERT INTO `scholar_applicant_last_login` (`id`, `id_scholar_applicant`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(1, 1, '{\"scholar_applicant_name\":\"Miss. Sumangala K\",\"scholar_applicant_email_id\":\"s@cms.com\"}', '112.79.53.129', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-21 21:09:30'),
(2, 2, '{\"scholar_applicant_name\":\"Mr. Testing  user\",\"scholar_applicant_email_id\":\"test1@gmail.com\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-16 08:55:41'),
(3, 3, '{\"scholar_applicant_name\":\"Mr. Testing testing\",\"scholar_applicant_email_id\":\"testing1@gmail.com\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-16 20:26:14'),
(4, 3, '{\"scholar_applicant_name\":\"Mr. Testing testing\",\"scholar_applicant_email_id\":\"testing1@gmail.com\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-16 20:27:18'),
(5, 4, '{\"scholar_applicant_name\":\"Miss. app a\",\"scholar_applicant_email_id\":\"ap@cms.com\"}', '157.49.125.118', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-17 03:57:48'),
(6, 5, '{\"scholar_applicant_name\":\"Mr. kiran a\",\"scholar_applicant_email_id\":\"a@a.com\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-17 18:45:35'),
(7, 6, '{\"scholar_applicant_name\":\"Miss. One 1\",\"scholar_applicant_email_id\":\"a1@cms.com\"}', '157.49.100.192', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-18 00:52:43'),
(8, 6, '{\"scholar_applicant_name\":\"Miss. One 1\",\"scholar_applicant_email_id\":\"a1@cms.com\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-18 00:58:03'),
(9, 7, '{\"scholar_applicant_name\":\"Mr. Michel Adam\",\"scholar_applicant_email_id\":\"ma@cms.com\"}', '157.49.107.189', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-18 14:52:48'),
(10, 8, '{\"scholar_applicant_name\":\"Mr. aeu1 AEU1\",\"scholar_applicant_email_id\":\"ab@gmail.com\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-19 07:31:24'),
(11, 7, '{\"scholar_applicant_name\":\"Mr. Michel Adam\",\"scholar_applicant_email_id\":\"ma@cms.com\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-20 19:27:51'),
(12, 7, '{\"scholar_applicant_name\":\"Mr. Michel Adam\",\"scholar_applicant_email_id\":\"ma@cms.com\"}', '183.171.69.143', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '2020-09-20 19:39:07'),
(13, 7, '{\"scholar_applicant_name\":\"Mr. Michel Adam\",\"scholar_applicant_email_id\":\"ma@cms.com\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-20 20:49:59'),
(14, 7, '{\"scholar_applicant_name\":\"Mr. Michel Adam\",\"scholar_applicant_email_id\":\"ma@cms.com\"}', '183.171.69.143', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '2020-09-20 20:51:49');

-- --------------------------------------------------------

--
-- Table structure for table `scholar_last_login`
--

CREATE TABLE `scholar_last_login` (
  `id` bigint(20) NOT NULL,
  `id_scholar` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scholar_last_login`
--

INSERT INTO `scholar_last_login` (`id`, `id_scholar`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(1, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":null}', '112.79.53.129', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-21 21:05:01'),
(2, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-21 21:05:01\"}', '112.79.53.129', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-21 21:07:03'),
(3, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-21 21:07:03\"}', '112.79.53.68', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-21 21:44:20'),
(4, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-21 21:44:20\"}', '112.79.53.68', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-21 21:44:42'),
(5, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-21 21:44:42\"}', '112.79.53.68', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-21 21:45:19'),
(6, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-21 21:45:19\"}', '112.79.53.68', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-21 21:46:10'),
(7, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-21 21:46:10\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-21 21:47:46'),
(8, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-21 21:47:46\"}', '49.206.7.23', 'Safari 604.1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Mobile/15E148 Safari/604.1', 'iOS', '2020-07-21 21:47:56'),
(9, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-21 21:47:56\"}', '112.79.53.158', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-22 01:30:33'),
(10, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-22 01:30:33\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-22 06:50:16'),
(11, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-22 06:50:16\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-22 09:21:05'),
(12, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-22 09:21:05\"}', '112.79.54.84', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-22 10:29:20'),
(13, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-22 10:29:20\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-23 00:45:32'),
(14, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-23 00:45:32\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-23 03:02:55'),
(15, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-23 03:02:55\"}', '117.230.37.241', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-23 04:13:00'),
(16, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-23 04:13:00\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-23 05:34:26'),
(17, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-23 05:34:26\"}', '117.230.5.6', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-23 07:05:50'),
(18, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-23 07:05:50\"}', '112.79.48.147', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-23 10:25:22'),
(19, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-23 10:25:22\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-23 18:06:06'),
(20, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-23 18:06:06\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-23 22:55:31'),
(21, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-23 22:55:31\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-24 05:14:55'),
(22, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-24 05:14:55\"}', '112.79.48.230', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-24 09:30:15'),
(23, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-24 09:30:15\"}', '112.79.49.216', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-24 12:18:13'),
(24, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-24 12:18:13\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-24 13:10:20'),
(25, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-24 13:10:20\"}', '112.79.80.186', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-24 23:28:39'),
(26, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-24 23:28:39\"}', '112.79.51.20', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-25 07:42:04'),
(27, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-25 07:42:04\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-25 08:00:06'),
(28, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-25 08:00:06\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-07-26 18:34:58'),
(29, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-26 18:34:58\"}', '42.190.15.202', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 Edg/84.0.522.40', 'Windows 10', '2020-07-26 21:35:08'),
(30, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-26 21:35:08\"}', '112.79.49.125', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-26 22:49:02'),
(31, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-26 22:49:02\"}', '157.49.103.87', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-27 03:46:00'),
(32, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-27 03:46:00\"}', '112.79.49.29', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-27 12:15:51'),
(33, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-27 12:15:51\"}', '112.79.87.32', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-28 07:01:05'),
(34, 1, '{\"scholar_name\":\"vk\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-28 07:01:05\"}', '112.79.51.233', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-29 07:16:40'),
(35, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-29 07:16:40\"}', '137.97.44.116', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-29 12:23:22'),
(36, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-29 12:23:22\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-07-30 10:53:17'),
(37, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-30 10:53:17\"}', '112.79.53.224', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-07-30 12:52:42'),
(38, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-07-30 12:52:42\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-01 23:16:54'),
(39, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-08-01 23:16:54\"}', '157.45.19.112', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-01 11:09:36'),
(40, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-08-01 11:09:36\"}', '157.45.19.112', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-01 12:27:14'),
(41, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-08-01 12:27:14\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-08-03 19:59:20'),
(42, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-08-03 19:59:20\"}', '157.45.15.79', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '2020-08-04 02:33:00'),
(43, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-08-04 02:33:00\"}', '157.49.166.214', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-15 07:44:02'),
(44, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-15 07:44:02\"}', '1.39.136.212', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-15 22:53:53'),
(45, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-15 22:53:53\"}', '157.49.161.203', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-15 23:58:20'),
(46, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-15 23:58:20\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '2020-09-16 08:49:10'),
(47, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-16 08:49:10\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-16 09:04:15'),
(48, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-16 09:04:15\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-16 09:50:17'),
(49, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-16 09:50:17\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-16 11:08:10'),
(50, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-16 11:08:10\"}', '157.49.185.161', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-16 11:08:30'),
(51, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-16 11:08:30\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-16 11:49:14'),
(52, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-16 11:49:14\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-16 19:23:24'),
(53, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-16 19:23:24\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-16 19:23:55'),
(54, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-16 19:23:55\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-16 19:24:21'),
(55, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-16 19:24:21\"}', '183.171.79.216', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '2020-09-16 20:06:39'),
(56, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-16 20:06:39\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-16 20:23:50'),
(57, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-16 20:23:50\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '2020-09-16 23:42:47'),
(58, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-16 23:42:47\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-17 03:49:41'),
(59, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-17 03:49:41\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '2020-09-17 06:29:25'),
(60, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-17 06:29:25\"}', '157.49.168.169', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-17 13:22:29'),
(61, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-17 13:22:29\"}', '157.49.168.169', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-17 16:23:15'),
(62, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-17 16:23:15\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-17 18:39:46'),
(63, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-17 18:39:46\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '2020-09-17 21:01:28'),
(64, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-17 21:01:28\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-17 22:11:19'),
(65, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-17 22:11:19\"}', '49.206.13.223', 'Safari 604.1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/85.0.4183.109 Mobile/15E148 Safari/604.1', 'iOS', '2020-09-18 09:18:15'),
(66, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-18 09:18:15\"}', '49.206.13.223', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-18 11:18:05'),
(67, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-18 11:18:05\"}', '157.49.107.189', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-18 15:06:19'),
(68, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-18 15:06:19\"}', '1.39.145.182', 'Safari 604.1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/85.0.4183.109 Mobile/15E148 Safari/604.1', 'iOS', '2020-09-19 01:23:31'),
(69, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-19 01:23:31\"}', '157.49.126.194', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-19 01:32:57'),
(70, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-19 01:32:57\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-19 07:17:04'),
(71, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-19 07:17:04\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-19 07:28:49'),
(72, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-19 07:28:49\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '2020-09-19 08:21:51'),
(73, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-19 08:21:51\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-19 08:25:37'),
(74, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-19 08:25:37\"}', '117.230.151.152', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-19 12:52:38'),
(75, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-19 12:52:38\"}', '157.49.169.213', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-19 16:28:01'),
(76, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-19 16:28:01\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '2020-09-19 20:27:25'),
(77, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-19 20:27:25\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-19 20:36:13'),
(78, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-19 20:36:13\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-20 00:24:02'),
(79, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-20 00:24:02\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '2020-09-20 01:52:13'),
(80, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-20 01:52:13\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '2020-09-20 01:53:19'),
(81, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-20 01:53:19\"}', '157.49.120.76', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-20 09:15:41'),
(82, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-20 09:15:41\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-20 09:32:17'),
(83, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-20 09:32:17\"}', '157.49.169.213', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-20 11:56:30'),
(84, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-20 11:56:30\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '2020-09-20 14:26:40'),
(85, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-20 14:26:40\"}', '157.49.101.234', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-20 14:27:15'),
(86, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-20 14:27:15\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '2020-09-20 17:02:08'),
(87, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-20 17:02:08\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-20 18:19:44'),
(88, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-20 18:19:44\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-20 19:32:10'),
(89, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-20 19:32:10\"}', '183.171.69.143', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '2020-09-20 19:38:26'),
(90, 1, '{\"scholar_name\":\"Mr. Vinay Kiran\",\"scholar_role\":\"1\",\"scholarship_last_login\":\"2020-09-20 19:38:26\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-20 20:19:41');

-- --------------------------------------------------------

--
-- Table structure for table `scholar_permissions`
--

CREATE TABLE `scholar_permissions` (
  `id` bigint(20) NOT NULL,
  `code` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `status` int(2) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholar_permissions`
--

INSERT INTO `scholar_permissions` (`id`, `code`, `description`, `status`) VALUES
(1, 'SM', 'Scholar Manager', 1),
(2, 'sub_thrust.add', 'Sub Thrust Add', 1),
(3, 'application.approve', 'Application Aproval', 1);

-- --------------------------------------------------------

--
-- Table structure for table `scholar_roles`
--

CREATE TABLE `scholar_roles` (
  `id` tinyint(4) NOT NULL COMMENT 'role id',
  `role` varchar(50) NOT NULL COMMENT 'role text',
  `status` int(2) DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scholar_roles`
--

INSERT INTO `scholar_roles` (`id`, `role`, `status`) VALUES
(1, 'CEO', 1),
(2, 'COO', 1),
(3, 'HOD PSDM', 1),
(4, 'HOD Finance', 1),
(5, 'HOD Support Services', 1);

-- --------------------------------------------------------

--
-- Table structure for table `scholar_role_permissions`
--

CREATE TABLE `scholar_role_permissions` (
  `id` bigint(20) NOT NULL,
  `id_role` bigint(20) DEFAULT NULL,
  `id_permission` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scholar_role_permissions`
--

INSERT INTO `scholar_role_permissions` (`id`, `id_role`, `id_permission`) VALUES
(3, 2, 2),
(4, 1, 2),
(5, 1, 1),
(6, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(20) NOT NULL,
  `salutation` varchar(20) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `first_name` varchar(2048) DEFAULT '',
  `last_name` varchar(2048) DEFAULT '',
  `ic_no` varchar(50) DEFAULT '',
  `dob` date DEFAULT NULL,
  `mobile_number` varchar(20) DEFAULT '',
  `phone_number` varchar(20) DEFAULT '',
  `id_country` int(20) DEFAULT NULL,
  `id_state` int(20) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `gender` varchar(20) DEFAULT '',
  `staff_id` varchar(50) DEFAULT '',
  `email` varchar(180) DEFAULT '',
  `address` varchar(520) DEFAULT '',
  `address_two` varchar(520) DEFAULT '',
  `job_type` varchar(20) DEFAULT '',
  `academic_type` varchar(20) DEFAULT '',
  `id_department` int(10) DEFAULT NULL,
  `id_faculty_program` int(20) DEFAULT 0 COMMENT 'as similar to department',
  `status` int(2) DEFAULT 1,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `salutation`, `name`, `first_name`, `last_name`, `ic_no`, `dob`, `mobile_number`, `phone_number`, `id_country`, `id_state`, `zipcode`, `gender`, `staff_id`, `email`, `address`, `address_two`, `job_type`, `academic_type`, `id_department`, `id_faculty_program`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, '1', 'AG. Muhammad Hakim', 'Muhammad', 'Hakim', '7612948373', '2020-07-11', '93873737373', '637363636', 1, 1, 573663, 'Male', '1234', 'mhd@g.co.my', 'Address one ', 'Address two', '1', '1', 1, 1, 1, NULL, '2020-07-10 23:42:07', NULL, '2020-07-10 23:42:07'),
(2, '1', 'AG. Asadullah Bag', 'Asadullah', 'Bag', 'IC88909', '1980-08-01', '89786756', '123', 1, 1, 23432, 'Male', 'F22312', 'asad@cms.com', 'KL', 'KL', '1', '1', 1, 1, 1, NULL, '2020-08-06 09:26:31', NULL, '2020-08-06 09:26:31');

-- --------------------------------------------------------

--
-- Table structure for table `status_list`
--

CREATE TABLE `status_list` (
  `id` int(20) NOT NULL,
  `name` varchar(500) DEFAULT NULL,
  `id_status_master` int(20) DEFAULT 0,
  `migration` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_list`
--

INSERT INTO `status_list` (`id`, `name`, `id_status_master`, `migration`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 'Drafts', 1, 0, 1, NULL, '2020-09-17 22:27:42', NULL, '2020-09-17 22:27:42'),
(3, 'scholar - Drafts', 2, 0, 1, NULL, '2020-09-17 22:36:34', NULL, '2020-09-17 22:36:34'),
(4, 'Entry', 2, 0, 1, NULL, '2020-09-17 22:37:25', NULL, '2020-09-17 22:37:25'),
(5, 'Graduated', 3, 0, 1, NULL, '2020-09-17 22:41:34', NULL, '2020-09-17 22:41:34'),
(6, 'Waiting', 1, 0, 1, NULL, '2020-09-17 22:27:42', NULL, '2020-09-17 22:27:42'),
(7, 'Approved', 1, 0, 1, NULL, '2020-09-17 22:27:42', NULL, '2020-09-17 22:27:42'),
(8, 'Migrated', 1, 1, 1, NULL, '2020-09-17 22:27:42', NULL, '2020-09-17 22:27:42');

-- --------------------------------------------------------

--
-- Table structure for table `status_master`
--

CREATE TABLE `status_master` (
  `id` int(20) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `status` int(20) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_master`
--

INSERT INTO `status_master` (`id`, `name`, `status`) VALUES
(1, 'Applicant Status', 0),
(2, 'Scholars Status', 0),
(3, 'Alumni Status', 0);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(20) NOT NULL,
  `id_intake` int(11) DEFAULT NULL,
  `id_program` int(11) DEFAULT NULL,
  `id_fee_structure` int(11) DEFAULT 0,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `passport` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `contact_email` varchar(500) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `id_type` varchar(50) DEFAULT '',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `id_cohert` int(20) DEFAULT 0,
  `id_Scholarship` int(20) DEFAULT 0,
  `is_hostel` int(20) DEFAULT 0,
  `id_degree_type` int(20) DEFAULT 0,
  `id_race` varchar(120) DEFAULT '',
  `id_advisor` int(20) DEFAULT 0,
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `program_scheme` varchar(200) DEFAULT '',
  `id_learning_mode` int(20) DEFAULT 0,
  `id_program_scheme` int(20) DEFAULT 0,
  `id_program_landscape` int(20) DEFAULT 0,
  `id_program_requirement` int(20) DEFAULT 0,
  `id_university` int(20) DEFAULT 0,
  `id_branch` int(20) DEFAULT 0,
  `mailing_zipcode` varchar(120) DEFAULT '',
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `sibbling_discount` varchar(50) DEFAULT '',
  `employee_discount` varchar(50) DEFAULT '',
  `alumni_discount` varchar(50) DEFAULT '',
  `applicant_status` varchar(50) DEFAULT 'Draft',
  `passport_number` varchar(50) DEFAULT '',
  `id_applicant` int(20) DEFAULT 0,
  `pathway` varchar(200) DEFAULT '',
  `added_by_partner_university` int(2) DEFAULT 0,
  `status` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `approved_by` int(20) DEFAULT 0,
  `approved_dt_tm` datetime DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `id_intake`, `id_program`, `id_fee_structure`, `full_name`, `salutation`, `first_name`, `last_name`, `nric`, `passport`, `phone`, `email_id`, `contact_email`, `password`, `id_type`, `passport_expiry_date`, `gender`, `date_of_birth`, `martial_status`, `religion`, `nationality`, `id_cohert`, `id_Scholarship`, `is_hostel`, `id_degree_type`, `id_race`, `id_advisor`, `mail_address1`, `mail_address2`, `mailing_country`, `mailing_state`, `mailing_city`, `program_scheme`, `id_learning_mode`, `id_program_scheme`, `id_program_landscape`, `id_program_requirement`, `id_university`, `id_branch`, `mailing_zipcode`, `permanent_address1`, `permanent_address2`, `permanent_country`, `permanent_state`, `permanent_city`, `permanent_zipcode`, `sibbling_discount`, `employee_discount`, `alumni_discount`, `applicant_status`, `passport_number`, `id_applicant`, `pathway`, `added_by_partner_university`, `status`, `reason`, `approved_by`, `approved_dt_tm`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 1, 1, 0, 'MR Azwan Ahmad', '2', 'Azwan', 'Ahmad', '200502025681', '', '123', 'tesing@gmail.com', '', '202cb962ac59075b964b07152d234b70', '', '', 'Male', '2002-09-08', 'Single', '2', 'Malaysian', 0, 0, 1, 1, '2', 1, 'KL', 'KL', 110, 1, 'KL', 'Blended - Part Time', 4, 14, 10, 1, 4, 5, '133211', 'KL', 'KL', 110, 1, 'KL', '1232131', '', '', '', 'Approved', '', 7, 'APEL', 4, NULL, '', 0, NULL, NULL, '2020-09-10 14:13:10', 1, '2020-09-10 14:13:10'),
(2, 1, 4, 0, 'DATIN. Sadhullah Beeg', '2', 'Sadhullah', 'Beeg', 'WQEQW', '', '323137', 'sb@cms.com', '', '202cb962ac59075b964b07152d234b70', '', '', 'Male', '2020-09-12', 'Single', '2', 'Malaysian', 0, 0, 1, 7, '3', 1, 'KL', '', 110, 1, 'KL', 'Blended - Part Time', 1, 15, 18, 1, 4, 5, '31231', 'kl', '', 110, 1, 'KL', '2312312', '', 'Yes', '', 'Approved', '', 48, 'APEL', 0, 1, '', 0, '2020-09-14 03:05:00', NULL, '2020-09-13 14:20:20', 1, '2020-09-13 14:20:20'),
(3, 1, 3, 0, 'DATINPROFDR. Abdul Rehman', '4', 'Abdul', 'Rehman', '132123', '', '231231', 'abr@cms.com', '', '202cb962ac59075b964b07152d234b70', '', '', 'Male', '2020-09-17', 'Single', '2', 'Malaysian', 0, 0, 1, 7, '3', 1, 'KL', '', 110, 1, 'KL', 'Blended - Part Time', 4, 14, NULL, 1, 4, 5, '3231231', 'KL', '', 110, 1, 'KL', '12112', '', '', '', 'Approved', '', 0, 'APEL', 4, NULL, '', 0, NULL, NULL, '2020-09-15 04:29:08', 1, '2020-09-15 04:29:08'),
(4, 1, 3, 0, 'DATINDR. Syed Thousif', '3', 'Syed', 'Thousif', '4342', '', '321312', 'test@cms.com', '', '202cb962ac59075b964b07152d234b70', '', '', 'Male', '2020-09-19', 'Single', '2', 'Malaysian', 0, 0, 1, 7, '2', 0, 'KKL', '', 110, 1, 'KL', 'Blended - Part Time', 4, 14, NULL, 8, 4, 5, '231312', 'KL', '', 110, 1, 'KL', '122131', '', '', '', 'Approved', '', 49, 'DIRECT ENTRY', 0, 1, '', 0, '2020-09-15 17:35:00', NULL, '2020-09-15 05:05:22', NULL, '2020-09-15 05:01:45'),
(5, NULL, NULL, 0, 'Mr. Michel Adam', 'Mr', 'Michel', 'Adam', 'MA888', '', '123231', 'ma@cms.com', '', '202cb962ac59075b964b07152d234b70', NULL, '', '', '', '', '', '', 0, 0, 0, 0, '', 0, '', '', NULL, NULL, '', '', 0, 0, 0, 0, 0, 0, '', '', '', NULL, NULL, '', '', '', '', '', 'Draft', NULL, 7, '', 0, NULL, '', 0, NULL, NULL, '2020-09-18 14:52:40', NULL, '2020-09-18 15:08:56');

-- --------------------------------------------------------

--
-- Table structure for table `student_last_login`
--

CREATE TABLE `student_last_login` (
  `id` bigint(20) NOT NULL,
  `id_student` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_last_login`
--

INSERT INTO `student_last_login` (`id`, `id_student`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(1, 1, '{\"student_name\":\"DATIN. Osomania Azarul\",\"email_id\":\"oa@cms.com\",\"nric\":\"NR213312\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_program_landscape\":\"10\",\"id_qualification\":\"1\",\"id_program_scheme\":\"14\",\"student_education_level\":\"Ujian Pendidikan Sekolah Rendah\"}', '157.49.168.169', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-17 16:23:52'),
(2, 1, '{\"student_name\":\"DATIN. Osomania Azarul\",\"email_id\":\"oa@cms.com\",\"nric\":\"NR213312\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_program_landscape\":\"10\",\"id_qualification\":\"1\",\"id_program_scheme\":\"14\",\"student_education_level\":\"Ujian Pendidikan Sekolah Rendah\"}', '157.49.168.169', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-17 16:29:01'),
(3, 1, '{\"student_name\":\"DATIN. Osomania Azarul\",\"email_id\":\"oa@cms.com\",\"nric\":\"NR213312\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_program_landscape\":\"10\",\"id_qualification\":\"1\",\"id_program_scheme\":\"14\",\"student_education_level\":\"Ujian Pendidikan Sekolah Rendah\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-17 18:34:05'),
(4, 1, '{\"student_name\":\"DATIN. Osomania Azarul\",\"email_id\":\"oa@cms.com\",\"nric\":\"NR213312\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_program_landscape\":\"10\",\"id_qualification\":\"1\",\"id_program_scheme\":\"14\",\"student_education_level\":\"Ujian Pendidikan Sekolah Rendah\"}', '42.190.31.233', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '2020-09-17 19:47:16'),
(5, 5, '{\"student_name\":\"Mr. Michel Adam\",\"email_id\":\"ma@cms.com\",\"nric\":\"MA888\",\"id_intake\":null,\"id_program\":null,\"id_program_landscape\":\"0\",\"id_qualification\":\"0\",\"id_applicant\":\"7\",\"id_program_scheme\":\"0\",\"student_education_level\":null}', '157.49.107.189', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '2020-09-18 15:13:08'),
(6, 4, '{\"student_name\":\"DATINDR. Syed Thousif\",\"email_id\":\"syf@cms.com\",\"nric\":\"4342\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_program_landscape\":null,\"id_qualification\":\"7\",\"id_applicant\":\"49\",\"id_program_scheme\":\"14\",\"student_education_level\":\"Tahun Pertama Diploma\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2020-09-20 19:29:26'),
(7, 4, '{\"student_name\":\"DATINDR. Syed Thousif\",\"email_id\":\"test@cms.com\",\"nric\":\"4342\",\"id_intake\":\"1\",\"id_program\":\"3\",\"id_program_landscape\":null,\"id_qualification\":\"7\",\"id_applicant\":\"49\",\"id_program_scheme\":\"14\",\"student_education_level\":\"Tahun Pertama Diploma\"}', '183.171.69.143', 'Chrome 85.0.4183.83', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36', 'Windows 10', '2020-09-20 19:39:42');

-- --------------------------------------------------------

--
-- Table structure for table `student_status_change_history`
--

CREATE TABLE `student_status_change_history` (
  `id` int(20) NOT NULL,
  `id_student` int(20) DEFAULT 0,
  `id_status` int(20) DEFAULT 0,
  `id_sub_status` int(20) DEFAULT 0,
  `effective_date` datetime DEFAULT NULL,
  `remarks` varchar(2048) DEFAULT '',
  `file` varchar(512) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_status_change_history`
--

INSERT INTO `student_status_change_history` (`id`, `id_student`, `id_status`, `id_sub_status`, `effective_date`, `remarks`, `file`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 4, 2, 3, '2020-09-24 00:00:00', 'Rema', 'd2d91d335d6cf52b3420aa526edd6edb.png', NULL, NULL, '2020-09-19 16:30:35', NULL, '2020-09-19 16:30:35');

-- --------------------------------------------------------

--
-- Table structure for table `temp_internship_company_has_program`
--

CREATE TABLE `temp_internship_company_has_program` (
  `id` int(20) NOT NULL,
  `id_session` varchar(200) DEFAULT '',
  `id_company_type` bigint(20) DEFAULT 0,
  `id_intake` bigint(20) DEFAULT 0,
  `id_program` bigint(20) DEFAULT 0,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_main_invoice_details`
--

CREATE TABLE `temp_main_invoice_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(120) DEFAULT '',
  `id_fee_item` int(20) DEFAULT NULL,
  `amount` varchar(20) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_scheme_has_program`
--

CREATE TABLE `temp_scheme_has_program` (
  `id` int(20) NOT NULL,
  `id_session` varchar(500) DEFAULT '',
  `id_program` bigint(20) DEFAULT 0,
  `id_sub_thrust` int(20) DEFAULT 0,
  `quota` int(20) DEFAULT 0,
  `budget` float(20,2) DEFAULT 0.00,
  `status` int(2) DEFAULT 0,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_scholarship_documents_program_details`
--

CREATE TABLE `temp_scholarship_documents_program_details` (
  `id` int(20) NOT NULL,
  `id_session` varchar(520) DEFAULT '',
  `id_program` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academic_year`
--
ALTER TABLE `academic_year`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `applicantion_status_change_history`
--
ALTER TABLE `applicantion_status_change_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apply_claim`
--
ALTER TABLE `apply_claim`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `budget_allocation`
--
ALTER TABLE `budget_allocation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `budget_allocation_details`
--
ALTER TABLE `budget_allocation_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cohort_activity`
--
ALTER TABLE `cohort_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group`
--
ALTER TABLE `communication_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_recepients`
--
ALTER TABLE `communication_group_recepients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_template`
--
ALTER TABLE `communication_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_rate_setup`
--
ALTER TABLE `currency_rate_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_setup`
--
ALTER TABLE `currency_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_category`
--
ALTER TABLE `fee_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_setup`
--
ALTER TABLE `fee_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_structure`
--
ALTER TABLE `fee_structure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_structure_master`
--
ALTER TABLE `fee_structure_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `financial_account_code`
--
ALTER TABLE `financial_account_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `internship_company_has_program`
--
ALTER TABLE `internship_company_has_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `internship_company_registration`
--
ALTER TABLE `internship_company_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `internship_company_type`
--
ALTER TABLE `internship_company_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `internship_student_limit`
--
ALTER TABLE `internship_student_limit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `is_scholarship_applicant_examination_details`
--
ALTER TABLE `is_scholarship_applicant_examination_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `is_scholarship_applicant_personel_details`
--
ALTER TABLE `is_scholarship_applicant_personel_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `is_scholar_applicant_employment_status`
--
ALTER TABLE `is_scholar_applicant_employment_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `is_scholar_applicant_family_details`
--
ALTER TABLE `is_scholar_applicant_family_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_invoice`
--
ALTER TABLE `main_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_invoice_details`
--
ALTER TABLE `main_invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marital_status_setup`
--
ALTER TABLE `marital_status_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_has_certification`
--
ALTER TABLE `program_has_certification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `repayment`
--
ALTER TABLE `repayment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `repayment_extension`
--
ALTER TABLE `repayment_extension`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salutation_setup`
--
ALTER TABLE `salutation_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scheme_has_program`
--
ALTER TABLE `scheme_has_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar`
--
ALTER TABLE `scholar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_accreditation_category`
--
ALTER TABLE `scholarship_accreditation_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_accreditation_type`
--
ALTER TABLE `scholarship_accreditation_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_amount_calculation_type`
--
ALTER TABLE `scholarship_amount_calculation_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_applicant`
--
ALTER TABLE `scholarship_applicant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_application`
--
ALTER TABLE `scholarship_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_assesment_method_setup`
--
ALTER TABLE `scholarship_assesment_method_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_award_setup`
--
ALTER TABLE `scholarship_award_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_bank_registration`
--
ALTER TABLE `scholarship_bank_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_country`
--
ALTER TABLE `scholarship_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_department`
--
ALTER TABLE `scholarship_department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_department_has_staff`
--
ALTER TABLE `scholarship_department_has_staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_documents`
--
ALTER TABLE `scholarship_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_documents_for_applicant`
--
ALTER TABLE `scholarship_documents_for_applicant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_documents_program`
--
ALTER TABLE `scholarship_documents_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_documents_program_details`
--
ALTER TABLE `scholarship_documents_program_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_education_level`
--
ALTER TABLE `scholarship_education_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_examination_details`
--
ALTER TABLE `scholarship_examination_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_frequency_mode`
--
ALTER TABLE `scholarship_frequency_mode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_individual_entry_requirement`
--
ALTER TABLE `scholarship_individual_entry_requirement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_module_type_setup`
--
ALTER TABLE `scholarship_module_type_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_organisation_has_department`
--
ALTER TABLE `scholarship_organisation_has_department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_organisation_setup`
--
ALTER TABLE `scholarship_organisation_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_organisation_setup_comitee`
--
ALTER TABLE `scholarship_organisation_setup_comitee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_partner_university`
--
ALTER TABLE `scholarship_partner_university`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_partner_university_has_aggrement`
--
ALTER TABLE `scholarship_partner_university_has_aggrement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_partner_university_has_program`
--
ALTER TABLE `scholarship_partner_university_has_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_partner_university_has_training_center`
--
ALTER TABLE `scholarship_partner_university_has_training_center`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_partner_university_program_has_apprenticeship`
--
ALTER TABLE `scholarship_partner_university_program_has_apprenticeship`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_partner_university_program_has_internship`
--
ALTER TABLE `scholarship_partner_university_program_has_internship`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_partner_university_program_has_study_mode`
--
ALTER TABLE `scholarship_partner_university_program_has_study_mode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_partner_university_program_has_syllabus`
--
ALTER TABLE `scholarship_partner_university_program_has_syllabus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_payment_type`
--
ALTER TABLE `scholarship_payment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_programme`
--
ALTER TABLE `scholarship_programme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_program_accreditation`
--
ALTER TABLE `scholarship_program_accreditation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_program_partner`
--
ALTER TABLE `scholarship_program_partner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_program_syllabus`
--
ALTER TABLE `scholarship_program_syllabus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_program_syllabus_has_module`
--
ALTER TABLE `scholarship_program_syllabus_has_module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_race_setup`
--
ALTER TABLE `scholarship_race_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_religion_setup`
--
ALTER TABLE `scholarship_religion_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_salutation_setup`
--
ALTER TABLE `scholarship_salutation_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_scheme`
--
ALTER TABLE `scholarship_scheme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_selection_type`
--
ALTER TABLE `scholarship_selection_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_state`
--
ALTER TABLE `scholarship_state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_sub_thrust`
--
ALTER TABLE `scholarship_sub_thrust`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_sub_thrust_has_entry_requirement`
--
ALTER TABLE `scholarship_sub_thrust_has_entry_requirement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_sub_thrust_has_program`
--
ALTER TABLE `scholarship_sub_thrust_has_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_sub_thrust_has_requirement`
--
ALTER TABLE `scholarship_sub_thrust_has_requirement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_sub_thrust_requirement_has_age`
--
ALTER TABLE `scholarship_sub_thrust_requirement_has_age`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_sub_thrust_requirement_has_education`
--
ALTER TABLE `scholarship_sub_thrust_requirement_has_education`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_sub_thrust_requirement_has_other`
--
ALTER TABLE `scholarship_sub_thrust_requirement_has_other`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_sub_thrust_requirement_has_work_experience`
--
ALTER TABLE `scholarship_sub_thrust_requirement_has_work_experience`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_thrust`
--
ALTER TABLE `scholarship_thrust`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_users`
--
ALTER TABLE `scholarship_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholarship_work_specialisation`
--
ALTER TABLE `scholarship_work_specialisation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_applicant`
--
ALTER TABLE `scholar_applicant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_applicant_employment_status`
--
ALTER TABLE `scholar_applicant_employment_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_applicant_family_details`
--
ALTER TABLE `scholar_applicant_family_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_applicant_last_login`
--
ALTER TABLE `scholar_applicant_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_last_login`
--
ALTER TABLE `scholar_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_permissions`
--
ALTER TABLE `scholar_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_roles`
--
ALTER TABLE `scholar_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_role_permissions`
--
ALTER TABLE `scholar_role_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_list`
--
ALTER TABLE `status_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_master`
--
ALTER TABLE `status_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_last_login`
--
ALTER TABLE `student_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_status_change_history`
--
ALTER TABLE `student_status_change_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_internship_company_has_program`
--
ALTER TABLE `temp_internship_company_has_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_main_invoice_details`
--
ALTER TABLE `temp_main_invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_scheme_has_program`
--
ALTER TABLE `temp_scheme_has_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_scholarship_documents_program_details`
--
ALTER TABLE `temp_scholarship_documents_program_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academic_year`
--
ALTER TABLE `academic_year`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `applicantion_status_change_history`
--
ALTER TABLE `applicantion_status_change_history`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `apply_claim`
--
ALTER TABLE `apply_claim`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `budget_allocation`
--
ALTER TABLE `budget_allocation`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `budget_allocation_details`
--
ALTER TABLE `budget_allocation_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cohort_activity`
--
ALTER TABLE `cohort_activity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `communication_group`
--
ALTER TABLE `communication_group`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communication_group_recepients`
--
ALTER TABLE `communication_group_recepients`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communication_template`
--
ALTER TABLE `communication_template`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `currency_rate_setup`
--
ALTER TABLE `currency_rate_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `currency_setup`
--
ALTER TABLE `currency_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fee_category`
--
ALTER TABLE `fee_category`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fee_setup`
--
ALTER TABLE `fee_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `fee_structure`
--
ALTER TABLE `fee_structure`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `fee_structure_master`
--
ALTER TABLE `fee_structure_master`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `financial_account_code`
--
ALTER TABLE `financial_account_code`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `internship_company_has_program`
--
ALTER TABLE `internship_company_has_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `internship_company_registration`
--
ALTER TABLE `internship_company_registration`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `internship_company_type`
--
ALTER TABLE `internship_company_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `internship_student_limit`
--
ALTER TABLE `internship_student_limit`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `is_scholarship_applicant_examination_details`
--
ALTER TABLE `is_scholarship_applicant_examination_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `is_scholarship_applicant_personel_details`
--
ALTER TABLE `is_scholarship_applicant_personel_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `is_scholar_applicant_employment_status`
--
ALTER TABLE `is_scholar_applicant_employment_status`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `is_scholar_applicant_family_details`
--
ALTER TABLE `is_scholar_applicant_family_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `main_invoice`
--
ALTER TABLE `main_invoice`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_invoice_details`
--
ALTER TABLE `main_invoice_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `marital_status_setup`
--
ALTER TABLE `marital_status_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `program_has_certification`
--
ALTER TABLE `program_has_certification`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `repayment`
--
ALTER TABLE `repayment`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `repayment_extension`
--
ALTER TABLE `repayment_extension`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salutation_setup`
--
ALTER TABLE `salutation_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scheme_has_program`
--
ALTER TABLE `scheme_has_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `scholar`
--
ALTER TABLE `scholar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_accreditation_category`
--
ALTER TABLE `scholarship_accreditation_category`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_accreditation_type`
--
ALTER TABLE `scholarship_accreditation_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_amount_calculation_type`
--
ALTER TABLE `scholarship_amount_calculation_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_applicant`
--
ALTER TABLE `scholarship_applicant`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `scholarship_application`
--
ALTER TABLE `scholarship_application`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholarship_assesment_method_setup`
--
ALTER TABLE `scholarship_assesment_method_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_award_setup`
--
ALTER TABLE `scholarship_award_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `scholarship_bank_registration`
--
ALTER TABLE `scholarship_bank_registration`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_country`
--
ALTER TABLE `scholarship_country`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_department`
--
ALTER TABLE `scholarship_department`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `scholarship_department_has_staff`
--
ALTER TABLE `scholarship_department_has_staff`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `scholarship_documents`
--
ALTER TABLE `scholarship_documents`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_documents_for_applicant`
--
ALTER TABLE `scholarship_documents_for_applicant`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scholarship_documents_program`
--
ALTER TABLE `scholarship_documents_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_documents_program_details`
--
ALTER TABLE `scholarship_documents_program_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `scholarship_education_level`
--
ALTER TABLE `scholarship_education_level`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `scholarship_examination_details`
--
ALTER TABLE `scholarship_examination_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_frequency_mode`
--
ALTER TABLE `scholarship_frequency_mode`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholarship_individual_entry_requirement`
--
ALTER TABLE `scholarship_individual_entry_requirement`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `scholarship_module_type_setup`
--
ALTER TABLE `scholarship_module_type_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `scholarship_organisation_has_department`
--
ALTER TABLE `scholarship_organisation_has_department`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholarship_organisation_setup`
--
ALTER TABLE `scholarship_organisation_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_organisation_setup_comitee`
--
ALTER TABLE `scholarship_organisation_setup_comitee`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `scholarship_partner_university`
--
ALTER TABLE `scholarship_partner_university`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholarship_partner_university_has_aggrement`
--
ALTER TABLE `scholarship_partner_university_has_aggrement`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `scholarship_partner_university_has_program`
--
ALTER TABLE `scholarship_partner_university_has_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_partner_university_has_training_center`
--
ALTER TABLE `scholarship_partner_university_has_training_center`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_partner_university_program_has_apprenticeship`
--
ALTER TABLE `scholarship_partner_university_program_has_apprenticeship`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_partner_university_program_has_internship`
--
ALTER TABLE `scholarship_partner_university_program_has_internship`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_partner_university_program_has_study_mode`
--
ALTER TABLE `scholarship_partner_university_program_has_study_mode`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_partner_university_program_has_syllabus`
--
ALTER TABLE `scholarship_partner_university_program_has_syllabus`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_payment_type`
--
ALTER TABLE `scholarship_payment_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_programme`
--
ALTER TABLE `scholarship_programme`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `scholarship_program_accreditation`
--
ALTER TABLE `scholarship_program_accreditation`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_program_partner`
--
ALTER TABLE `scholarship_program_partner`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_program_syllabus`
--
ALTER TABLE `scholarship_program_syllabus`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_program_syllabus_has_module`
--
ALTER TABLE `scholarship_program_syllabus_has_module`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `scholarship_race_setup`
--
ALTER TABLE `scholarship_race_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `scholarship_religion_setup`
--
ALTER TABLE `scholarship_religion_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholarship_salutation_setup`
--
ALTER TABLE `scholarship_salutation_setup`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `scholarship_scheme`
--
ALTER TABLE `scholarship_scheme`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `scholarship_selection_type`
--
ALTER TABLE `scholarship_selection_type`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_state`
--
ALTER TABLE `scholarship_state`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `scholarship_sub_thrust`
--
ALTER TABLE `scholarship_sub_thrust`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `scholarship_sub_thrust_has_entry_requirement`
--
ALTER TABLE `scholarship_sub_thrust_has_entry_requirement`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `scholarship_sub_thrust_has_program`
--
ALTER TABLE `scholarship_sub_thrust_has_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `scholarship_sub_thrust_has_requirement`
--
ALTER TABLE `scholarship_sub_thrust_has_requirement`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `scholarship_sub_thrust_requirement_has_age`
--
ALTER TABLE `scholarship_sub_thrust_requirement_has_age`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `scholarship_sub_thrust_requirement_has_education`
--
ALTER TABLE `scholarship_sub_thrust_requirement_has_education`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `scholarship_sub_thrust_requirement_has_other`
--
ALTER TABLE `scholarship_sub_thrust_requirement_has_other`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_sub_thrust_requirement_has_work_experience`
--
ALTER TABLE `scholarship_sub_thrust_requirement_has_work_experience`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholarship_thrust`
--
ALTER TABLE `scholarship_thrust`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholarship_users`
--
ALTER TABLE `scholarship_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholarship_work_specialisation`
--
ALTER TABLE `scholarship_work_specialisation`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholar_applicant`
--
ALTER TABLE `scholar_applicant`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `scholar_applicant_employment_status`
--
ALTER TABLE `scholar_applicant_employment_status`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `scholar_applicant_family_details`
--
ALTER TABLE `scholar_applicant_family_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `scholar_applicant_last_login`
--
ALTER TABLE `scholar_applicant_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `scholar_last_login`
--
ALTER TABLE `scholar_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `scholar_permissions`
--
ALTER TABLE `scholar_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `scholar_roles`
--
ALTER TABLE `scholar_roles`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'role id', AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `scholar_role_permissions`
--
ALTER TABLE `scholar_role_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `status_list`
--
ALTER TABLE `status_list`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `status_master`
--
ALTER TABLE `status_master`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `student_last_login`
--
ALTER TABLE `student_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `student_status_change_history`
--
ALTER TABLE `student_status_change_history`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `temp_internship_company_has_program`
--
ALTER TABLE `temp_internship_company_has_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `temp_main_invoice_details`
--
ALTER TABLE `temp_main_invoice_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_scheme_has_program`
--
ALTER TABLE `temp_scheme_has_program`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `temp_scholarship_documents_program_details`
--
ALTER TABLE `temp_scholarship_documents_program_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

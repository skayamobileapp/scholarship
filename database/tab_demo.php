<?php $this->load->helper("form"); ?>
<div class="container-fluid page-wrapper">
    <div class="main-container clearfix">

        <div class="page-title clearfix">
            <h3>Edit Partner University </h3>
        </div>



            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">

                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#tab_program_one" class="nav-link border rounded text-center"
                            aria-controls="tab_program_one" aria-selected="true"
                            role="tab" data-toggle="tab">Study Mode</a>
                    </li>

                    <li role="presentation"><a href="#tab_program_two" class="nav-link border rounded text-center"
                            aria-controls="tab_program_two" role="tab" data-toggle="tab">Internship</a>
                    </li>

                    <li role="presentation"><a href="#tab_program_three" class="nav-link border rounded text-center"
                            aria-controls="tab_program_three" role="tab" data-toggle="tab">Apprenticeship</a>
                    </li>

                    <li role="presentation"><a href="#tab_program_four" class="nav-link border rounded text-center"
                            aria-controls="tab_program_four" role="tab" data-toggle="tab">Syllabus</a>
                    </li>
                    
                </ul>

                
                <div class="tab-content offers-tab-content">




                    <div role="tabpanel" class="tab-pane active" id="tab_program_one">
                        <div class="col-12 mt-4">




                        <form id="form_program_one" action="" method="post" enctype="multipart/form-data">


                        </form>


                        <div class="button-block clearfix">
                            <div class="bttn-group">
                                <button type="button" class="btn btn-primary btn-lg" onclick="addProgramStudyModeData()">Save</button>
                                <a href="../list" class="btn btn-link">Back</a>
                            </div>
                        </div>

                                 
                        </div> 
                    
                    </div>




                    <div role="tabpanel" class="tab-pane" id="tab_program_two">
                        <div class="mt-4">


                        <form id="form_program_two" action="" method="post">

                        </form>

                        <div class="button-block clearfix">
                            <div class="bttn-group">
                                <button type="button" class="btn btn-primary btn-lg" onclick="addProgramInternshipData()">Save</button>
                                <a href="../list" class="btn btn-link">Back</a>
                            </div>
                        </div>


                        </div>
                    
                    </div>






                    <div role="tabpanel" class="tab-pane" id="tab_program_three">
                        <div class="mt-4">


                        <form id="form_program_three" action="" method="post">


                        </form>

                        <div class="button-block clearfix">
                            <div class="bttn-group">
                                <button type="button" class="btn btn-primary btn-lg" onclick="addProgramApprenticeData()">Save</button>
                                <a href="../list" class="btn btn-link">Back</a>
                            </div>
                        </div>


                        </div>
                    
                    </div>





                    <div role="tabpanel" class="tab-pane" id="tab_program_four">
                        <div class="mt-4">


                        <form id="form_program_one" action="" method="post">

                        </form>

                        <div class="button-block clearfix">
                            <div class="bttn-group">
                                <button type="button" class="btn btn-primary btn-lg" onclick="addProgramSyllabusData()">Save</button>
                                <a href="../list" class="btn btn-link">Back</a>
                            </div>
                        </div>


                        </div>
                    
                    </div>





            </div>

        </div>












        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer>
    </div>
</div>
<script>
